﻿using System;
using System.Runtime.InteropServices;

namespace AutoPrinter_SN
{
    class RawPrinterHelper
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DocInfoA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string PDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string POutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string PDataType;
        }

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DocInfoA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount, string docTitle)
        {
            IntPtr hPrinter;
            var di = new DocInfoA();
            var bSuccess = false;

            di.PDocName = "CSIMES RAW Document Title : " + docTitle + " Time : " +
                          DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            di.PDataType = "RAW";

            // Open the printer
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Start the document
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Start a print
                    if (StartPagePrinter(hPrinter))
                    {
                        // Print
                        int dwWritten;
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }

            return bSuccess;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString, string docTitle)
        {
            //Length of the string
            var dwCount = szString.Length;
            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            var pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            var retValue = SendBytesToPrinter(szPrinterName, pBytes, dwCount, docTitle);
            Marshal.FreeCoTaskMem(pBytes);
            return retValue;
        }

        public RawPrinterHelper()
        {
        }
    }
}
