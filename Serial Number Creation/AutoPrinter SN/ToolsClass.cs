﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace AutoPrinter_SN
{
    class ToolsClass
    {
        public ToolsClass()
        {

        }
        public static string GetConnection()
        {
            return "Data Source= 10.127.34.15; Initial Catalog= LabelPrintDB;User ID=fastengine;Password=Csi456";
        }


        public static DataTable PullData(string query, string connString = "")
        {
            if (string.IsNullOrEmpty(connString))
                connString = GetConnection();

            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();

                // create data adapter
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                conn.Close();
                da.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }


        }
        public static int PutsData(string query, string connString = "")
        {
            if (string.IsNullOrEmpty(connString))
                connString = GetConnection();
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand myCommand = new SqlCommand(query, conn);

                int i = myCommand.ExecuteNonQuery();
                conn.Close();
                return i;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }
        public static void saveXMLNode(string snode, string value = "", bool isAttribute = false, string sAttributeName = "", string fileName = "")
        {
            string xmlFile = "";
            string ParentNode = "";
            if (fileName == "" || fileName == null)
            {
                xmlFile = Application.StartupPath + "\\config.xml";
                ParentNode = "config";
            }
            else
            {
                xmlFile = Application.StartupPath + "\\" + fileName;
                ParentNode = "config";
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode(ParentNode).ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn;
                if (xe.Name == snode)
                {
                    if (isAttribute)
                    {
                        xe.SetAttribute(sAttributeName, value);
                        break;
                    }
                    else
                    {
                        xe.InnerText = value;
                        break;
                    }
                }
            }
            try
            {
                xmlDoc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static String getColumnNameFromIndex(int column)
        {
            column--;
            String col = Convert.ToString((char)('A' + (column % 26)));
            while (column >= 26)
            {
                column = (column / 26) - 1;
                col = Convert.ToString((char)('A' + (column % 26))) + col;
            }
            return col;
        }
        public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int 
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "config.xml";
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {
                return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
        

    }
}
