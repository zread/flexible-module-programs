﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 27-08-15
 * Time: 10:29
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Diagnostics;
using System.Net.Mail;

namespace AutoPrinter_SN
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		private static string ZPL;
        private static string Printer;  
        private static string PrinterA;        
        private static string PrinterB;
        private static string PrinterC;
        private static string PrinterD;
        private static string ManualA;
        private static string ManualB;
        private static string ManualC;
        private static string ManualD;        
        private static string User;
		
		public MainForm(String username)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
            CbPrint.Enabled = false;
            CbPrint.SelectedIndex = 1;
            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
            User = username;
			InitializeZPL();
			#region London
//			CbPrint.SelectedIndex = 1;
//			comboBox1.SelectedIndex = 2;
//			CbPrint.Enabled = false;
//			comboBox1.Enabled = false;
			#endregion
		}
		
		public static void connectToSql(String Sn_F, String Sn_L){
			string connectionString = getSQLString();
    		String sql = "INSERT INTO SapAutoSerialNumbers VALUES(@firstSN, @SecondSN, @Date, @User)";
			
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
    				cmd.Parameters.AddWithValue("@firstSN", Sn_F);
	                cmd.Parameters.AddWithValue("@SecondSN", Sn_L);
	                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
	                cmd.Parameters.AddWithValue("@User", User.ToLower());
	    			cmd.ExecuteNonQuery();
    			}                
    			}                
    		}
				

		void Button1Click(object sender, System.EventArgs e)
		{

			
			int Ln = 0;	
			int Qty = Convert.ToInt32(textBox2.Text.ToString().Trim());
			string SqlSN="";
			int F_Sn = 1,L_Sn;
			string F_SnS = "0001",L_SnS = "0000";			
			string year = DateTime.Today.ToString("yy");
			string month = DateTime.Today.Month.ToString("d2");
            string date = DateTime.Today.Day.ToString("d2");
            string wattage = "000";
			string Sn_F = "", Sn_L = "", Po = "";
            string FlexibleSn_F = "", FlexibleSn_L = "";


            #region Checking inputs
            if (comboBox1.Text.Length < 1 ||  textBox1.Text.Length < 1 || textBox2.Text.Length < 1 )
			{
				MessageBox.Show(" Please fill out all text entries. ");
				return;
			}
            
			
			if( textBox1.Text.Length != 3 )
			{
				MessageBox.Show(" Please re-enter 3 digits autoincreamenting Production order ");
				return;
			}
			

			if(  textBox2.Text.Length > 4)
			{
				MessageBox.Show(" Serial number exceeds maximum capacity, please re-enter ");
				return;				
			}


            if(string.IsNullOrEmpty(tBWattage.Text))
            {
                MessageBox.Show(" Wattage needs to be entered ");
                return;
            }

            if (tBVol.Text.Length != 2)
            {
                MessageBox.Show(" Voltage needs to be entered correctly ");
                return;
            }
            #endregion
            wattage = tBWattage.Text.Trim();
			Po = textBox1.Text.Trim();
			if(comboBox1.Text.ToString() == "A" )
				Ln = 40;
			if(comboBox1.Text.ToString() == "B" )
				Ln = 42;
			if(comboBox1.Text.ToString() == "C" )
				Ln = 44;
			
			
			bool check = true;
       		int constantLn =Ln+1;
			//Check if we have same serial numbers
			
			string connectionString = MainForm.getSQLString();
			string sql ="";
			#region check where Last SN prints to
			while (check)
			{
				sql= @"SELECT TOP 1 * FROM SapAutoSerialNumbers WHERE FirstSN like '" + "_"+ year + month + Ln.ToString() + Po + "____' order by EntryOn desc ";
				SqlDataReader reader = GetDataReader(sql,connectionString);
				if (reader.Read() && reader != null) {
					SqlSN = reader.GetString(2).Substring(10,4);
					F_Sn = Convert.ToInt32(SqlSN);
					if(F_Sn == 9999)
					{
						Ln++;
						F_Sn = 1;
					}
					else
					{
						F_Sn = F_Sn + 1;
						F_SnS = F_Sn.ToString("D" + 4);
		    			reader.Close();
						check = false;
					}
				}
				else
				break;
				if(Ln > constantLn )
				{
					MessageBox.Show("SN out of range, please contact administrator!");
					return;
				}
			}
			#endregion

			//Naming Serial numbers	
			if(F_Sn + Qty <= 10000)
			{
				Sn_F = "1"+ year + month + Ln + Po + F_SnS;
                int FirstFlexSn = LastFlexSN(year,month,date,wattage) + 1;
                FlexibleSn_F = "eNow-" + tBVol.Text + "-GC" + wattage + year + month + date + FirstFlexSn.ToString("d3");

                L_Sn =F_Sn + Qty-1;
				L_SnS = L_Sn.ToString("D" + 4);
				Sn_L = "1"+ year + month + Ln + Po + L_SnS;
                FlexibleSn_L = "eNow-" + tBVol.Text + "-GC" + wattage + year + month + date + (FirstFlexSn + Qty - 1).ToString("d3");
                Label lb = new Label();
				lb.sn = Sn_F;
				lb.Qty = Qty;


               

				
				if (MessageBox.Show("Are you sure you want to print the following serial number range: \r\n" +  Sn_F + " : " + Sn_L + "\r\nA total of \r\n" + lb.Qty + "\r\nlabels?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if(CbPrint.SelectedIndex == 0)
					{
						if(comboBox1.Text.ToString() == "A")
							Printer = PrinterA;
						else if(comboBox1.Text.ToString() == "B")
							Printer = PrinterB;	
						else if(comboBox1.Text.ToString() == "C")
							Printer = PrinterC;	
					}
					else if(CbPrint.SelectedIndex == 1)
					{
						if(comboBox1.Text.ToString() == "A")
							Printer = ManualA;
						else if(comboBox1.Text.ToString() == "B")
							Printer = ManualB;	
						else if(comboBox1.Text.ToString() == "C")
							Printer = ManualC;		
					}

                    sqlCsitoFlexMapping(comboBox1.Text, textBox1.Text, F_Sn, wattage, FirstFlexSn, Qty);
                    lb.sn = FlexibleSn_F;
                    


                    for(int i = 0; i < Qty; i++ )
                    {
                        string current_Sn = (Convert.ToInt16(FirstFlexSn) + i).ToString("D" + 3);
                        lb.sn = "eNow-" + tBVol.Text + "-GC" + wattage + year + month + date + current_Sn;
                        lb.Qty = 1;
                        PrintLabel.print(lb, ZPL, Printer);
                    }
                   


                    connectToSql(Sn_F,Sn_L);
                    		  	
					textBox1.Clear();
					textBox2.Clear();				
				}
			}
			else
			{
				if(Ln.ToString().Substring(1,1) == "9" )
				{
					MessageBox.Show("SN out of range, Please contact administrator!");
					return;
				}
				Sn_F = "1"+ year + month + Ln + Po + F_SnS;			
				Sn_L = "1"+ year + month + Ln + Po + "9999";
				string Sn_F2 = "3"+ year + month + Convert.ToString(Ln+1) + Po +"0001";
				string Sn_L2 = "3"+ year + month + Convert.ToString(Ln+1) + Po + (F_Sn + Qty - 10000).ToString("D" + 4);
				Label lb1 = new Label();
				lb1.sn = Sn_F;
				lb1.Qty = 9999-F_Sn +1;
				
				Label lb2 = new Label();
				lb2.sn = Sn_F2 = "1"+ year + month + Convert.ToString(Ln+1) + Po +"0001";
				lb2.Qty = Qty - lb1.Qty;
		
				if (MessageBox.Show("Are you sure you want to print the following serial number range: \r\n" +  Sn_F + " : " + Sn_L2 + "\r\nA total of \r\n" + Qty + "\r\nlabels?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if(CbPrint.SelectedIndex == 0)
					{
						if(comboBox1.Text.ToString() == "A")
							Printer = PrinterA;
						else if(comboBox1.Text.ToString() == "B")
							Printer = PrinterB;	
						else if(comboBox1.Text.ToString() == "C")
							Printer = PrinterC;					
						
					}
					else if(CbPrint.SelectedIndex == 1)
					{
						if(comboBox1.Text.ToString() == "A")
							Printer = ManualA;
						else if(comboBox1.Text.ToString() == "B")
							Printer = ManualB;	
						else if(comboBox1.Text.ToString() == "C")
							Printer = ManualC;					
						
					}
				
					PrintLabel.print(lb1, ZPL, Printer);
					PrintLabel.print(lb2, ZPL, Printer);
					connectToSql(Sn_F,Sn_L );	
					connectToSql(Sn_F2,Sn_L2 );					
					textBox1.Clear();
					textBox2.Clear();				
				}
				
			}
		}



        int LastFlexSN(string yy, string MM, string dd, string wtg)
        {
            string sql = @"select top 1 FlexSN from CsiToFlexSN  where FlexSN like 'eNow-" + tBVol.Text + "-GC' + '{0}%' order by InterID desc";
            sql = string.Format(sql, wtg + yy + MM + dd);
            DataTable dt =  ToolsClass.PullData(sql);
            if(dt!=null && dt.Rows.Count > 0)
            {
                return Convert.ToInt16(dt.Rows[0][0].ToString().Substring(19,3));
            }

            return 0;
        }
        private void sqlCsitoFlexMapping(string line, string MO, int F_Sns,string wtg, int FlexF_Sns, int Qty)
        {
           
            string Ln = "0";
            string year = DateTime.Today.ToString("yy");
            string month = DateTime.Today.Month.ToString("d2");
            string date = DateTime.Today.Day.ToString("d2");

            if (line == "A")
                Ln = "40";
            if (line == "B")
                Ln = "42";
            if (line == "C")
                Ln = "44";           
            

            for (int i = 0; i < Qty; i++)
            {
                string Sn_F = "1" + year + month + Ln + MO + (F_Sns + i).ToString("D" + 4);
                string FlexSn_F = "eNow-" + tBVol.Text + "-GC" + wtg + year + month + date + (FlexF_Sns + i).ToString("d3");

                string connectionString = getSQLString();
                string sql = "INSERT INTO CsiToFlexSN VALUES(@firstSN, @SecondSN, getdate())";

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@firstSN", Sn_F);
                        cmd.Parameters.AddWithValue("@SecondSN", FlexSn_F);
                        cmd.Parameters.AddWithValue("@Date", DateTime.Now);                     
                        cmd.ExecuteNonQuery();
                    }
                }
            }

        }
        public static void InitializeZPL()
        {
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\Config.xml";

            XmlReader reader = XmlReader.Create(xmlFile);
            reader.ReadToFollowing("ZPL");
            ZPL = reader.ReadElementContentAsString();
            reader.Read();
            PrinterA = reader.ReadElementContentAsString();
            reader.Read();
            PrinterB = reader.ReadElementContentAsString();
            reader.Read();
            PrinterC = reader.ReadElementContentAsString();
            reader.Read();
            PrinterD = reader.ReadElementContentAsString();
            reader.Read();
           	ManualA = reader.ReadElementContentAsString();
           	reader.Read();
           	ManualB = reader.ReadElementContentAsString();            
           	reader.Read();
           	ManualC = reader.ReadElementContentAsString();            
           	reader.Read();
           	ManualD = reader.ReadElementContentAsString();
            
           	
            
            reader.Close();
        }
		
		public static string getSQLString()
		{			
			return "Data Source= 10.127.34.15; Initial Catalog= LabelPrintDB;User ID=fastengine;Password=Csi456";
		}
		 
		public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";       
            
                ConnStr = sDBConn;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                    dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }

      
    }
	}
