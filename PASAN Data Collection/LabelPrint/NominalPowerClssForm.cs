﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/9/2016
 * Time: 1:57 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;



namespace LabelPrint
{
	/// <summary>
	/// Description of NominalPowerClssForm.
	/// </summary>
	public partial class NominalPowerClssForm : Form
	{
		public NominalPowerClssForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			ShowList();
		}
		
		void ShowList()
		{
			string sql = @"select distinct PMAXID from Schemeentry";
			DataTable dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count< 1)
				return;
			foreach (DataRow row in dt.Rows) {
				string temp = row[0].ToString();
				LBClass.Items.AddRange(new object[] {temp});
			
			}
			
		}
		void BtExitClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void BtAddClick(object sender, EventArgs e)
		{
				AddNominalPowerForm frm = new AddNominalPowerForm();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				LBClass.Items.Clear();
				ShowList();
		}
		
		void BtEditClick(object sender, EventArgs e)
		{
			if(LBClass.SelectedIndex < 0)
				return;
			string sql =@" select top 1 PmaxNM from schemeentry where PmaxID ='"+ LBClass.Text.ToString()+"'";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			string pmaNm = "";
			if(rd.Read() && rd != null)
			{
				pmaNm = rd.GetString(0);
			}
			EditNominalPowerForm frm = new EditNominalPowerForm(LBClass.Text.ToString(),rd.GetString(0));
				frm.StartPosition = FormStartPosition.CenterScreen;
				LBClass.Items.Clear();
				frm.ShowDialog();
				LBClass.Items.Clear();					
				ShowList();
				
		}
		
		void BtDeleteClick(object sender, EventArgs e)
		{
			if(LBClass.SelectedIndex < 0)
				return;
			DialogResult dialogResult = MessageBox.Show("Are you sure to delete this record?","Attention", MessageBoxButtons.YesNo);
			if(dialogResult== DialogResult.Yes)
			{
				string sql =@"select top 1 PmaxNM from schemeentry where PmaxID ='"+ LBClass.Text.ToString()+"'";
				SqlDataReader rd = ToolsClass.GetDataReader(sql);
				string pmaNm = "";
				if(rd.Read() && rd != null)
				{
					pmaNm = rd.GetString(0);
				}
				sql = @"delete from schemeentry where PmaxID ='{0}' and PmaxNM = '{1}'";
				sql = string.Format(sql,LBClass.Text.ToString(),pmaNm);
				ToolsClass.PutsData(sql);
				LBClass.Items.Clear();
				ShowList();
			}
		}
	}
}
