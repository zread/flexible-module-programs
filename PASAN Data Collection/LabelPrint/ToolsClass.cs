﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 1:17 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Xml;
using System.Windows.Forms;




namespace LabelPrint
{
	/// <summary>
	/// Description of ToolsClass.
	/// </summary>
	public partial class ToolsClass : Form
	{
		public ToolsClass()
		{
		}
		
		 public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";
            if(string.IsNullOrEmpty(sDBConn))
            	ConnStr = ToolsClass.getConfig ("Conn",false,"","Config.xml");	
            else
            ConnStr = sDBConn;
         
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                     dr = cmd.ExecuteReader();//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
                finally
                {
                	              	
                }
               
            }
            return dr;
        }

		 
	

		
		 
		 public static DataTable PullData(string query,string connString = "")
    	{
		 	if(string.IsNullOrEmpty(connString))
		 	connString = ToolsClass.getConfig ("Conn",false,"","config.xml");
		 			DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				
		 	
		 	try{
		    		conn.Open();
       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
       			 	da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		WriteErrorLog("Exception caught when query: ",query,e.Message);
		    		return null;
		    	}
		    	finally
		    	{
		    		if(conn != null && conn.State != ConnectionState.Closed)
		    		conn.Close();
		    	}
				
    	}
		 public static int PutsData(string query,string connString = "")
   		{
		    if(string.IsNullOrEmpty(connString))
		 		connString = ToolsClass.getConfig ("Conn",false,"","config.xml");
					SqlConnection conn = new SqlConnection(connString);
		    		conn.Open();	
		    		int i = 0;
		 	try{
		    		
   					SqlCommand myCommand = new SqlCommand(query,conn);

   					 i = myCommand.ExecuteNonQuery();  
   					 return i;
		    	}
		    	catch (Exception ex)
		    	{
		    		WriteErrorLog("Exception caught when query:",query,ex.Message);
		    		return 0;
		    	}
		    	finally
		    	{
		    		if(conn != null && conn.State != ConnectionState.Closed)
		    		conn.Close();
		    		
		    	}
		    	
		    	
		
    	}
		 public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "config.xml";
            string xmlFile = Application.StartupPath + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {    
            	return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
              	System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            	return "";
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
		 public static void setConfig(string sData, string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode,int LabelFormat,string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule,string MixChecked,string CheckNOChecked)
        {
		  	 string xmlFile = "";
		  	if (configFile == "" || configFile == null)
		  		xmlFile = Application.StartupPath + "\\config.xml";
		  	else 
		  		xmlFile = Application.StartupPath + "\\" + configFile;
		  	
            string[] sDataSet = sData.Split('|');

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode parentsNode = null;
            doc.AppendChild(docNode);
			 if (configFile == "" || configFile == null)
           	 	parentsNode = doc.CreateElement("config");
	
            doc.AppendChild(parentsNode);

            subNode(doc, parentsNode, "Conn", sDataSet[0]);
           
            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
		 public static XmlNode subNode(XmlDocument doc, XmlNode ParentNode, string sNodeName, string sValue, string sAttributeName = "",string sAttribute = "")
        {
            XmlNode subNode = doc.CreateElement(sNodeName);
            if (sAttribute != "")
            {
                XmlAttribute subAttribute = doc.CreateAttribute(sAttributeName);
                subAttribute.Value = sAttribute;
                subNode.Attributes.Append(subAttribute);
            }
            if (sValue != "")
                subNode.AppendChild(doc.CreateTextNode(sValue));
            ParentNode.AppendChild(subNode);
            return subNode;
        }
		 
		 public static string[] ReadFile(string FilePath = "")
		 {
		 	if(FilePath == "")
		 		FilePath = getConfig("FilePath",false);
		 	string[] Lines = null;
		 	Lines = System.IO.File.ReadAllLines(FilePath);
		 	return Lines;
		 }
		 
		  public static string ReadFileReverse(string FilePath = "")
		 {
		 	if(FilePath == "")
		 		FilePath = getConfig("FilePath",false);
		 	string Lines = string.Empty;		 	
			Lines = File.ReadAllText(FilePath);
			string line = Lines.Substring(Lines.LastIndexOf('&')+1, Lines.Length - Lines.LastIndexOf('&')-1);
			return line;
		 }
		  public static void WritetoFile(string C, string FilePath = "")
		  {
		  	if(FilePath == "")
		  		FilePath = getConfig("FilePath",false);
		  	try 
		  	{
		  		File.AppendAllText(FilePath,  C + Environment.NewLine);
		  	} 
		  	catch (Exception e) 
		  	{
		  		WriteErrorLog("Exception Caught when Write to file",e.Message,"");
		  	}
		  	
		  }
		   public static void WriteErrorLog(string SN,string Msg,string error)
		  {
	  		StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + SN + "	" + Msg + "	" + error);
	                sw.Flush();
	                sw.Close();
	            }
	            catch (Exception e)
	            {
	            	WriteErrorLog("Exception Caught when Write to file",e.Message,"");
	            }
		  
		  }
		  
		   
	        public static void WriteToLog(string SN,string Msg = "")
	        {
	            
	        	StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory +"\\Log.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + SN + "	" + Msg);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
	        }
	    public static void SendEmail(string body)
        {
           	
        	try
            {
        		string host = getConfig("smtphost",false,"","Config.xml");
        		string port =  getConfig("smtpport",false,"","Config.xml");
        		string MailFrom = getConfig("EmailFrom",false,"","Config.xml");
        		string MailTo = getConfig("EmailTo",false,"","Config.xml");
        		string subject = getConfig("Subject",false,"","Config.xml");
        		string userNM = getConfig("userNM",false,"","Config.xml");
        		string PsW = getConfig("PassWord",false,"","Config.xml");
        		string[] Receipts = MailTo.Split(';');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);

                mail.From = new MailAddress(MailFrom);
                foreach (string r in Receipts)
                mail.To.Add(r);
                mail.Subject = subject;
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(port);   
               	SmtpServer.Credentials = new System.Net.NetworkCredential(userNM, PsW);          
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }     
	        
	        
			 public static String GetTimestamp(DateTime value)
	        {
	            return value.ToString("dd'/'MM'/'yy HH:mm:ss:ffff");
	        }

	}
}
