﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 2:58 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class EditSchemeForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditSchemeForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.TbCode = new System.Windows.Forms.TextBox();
			this.TbName = new System.Windows.Forms.TextBox();
			this.CbPower = new System.Windows.Forms.ComboBox();
			this.CbColor = new System.Windows.Forms.ComboBox();
			this.CbModel = new System.Windows.Forms.ComboBox();
			this.CbNominal = new System.Windows.Forms.ComboBox();
			this.CbPattern = new System.Windows.Forms.ComboBox();
			this.CbLabel = new System.Windows.Forms.ComboBox();
			this.CbSize = new System.Windows.Forms.ComboBox();
			this.CbCellType = new System.Windows.Forms.ComboBox();
			this.CbDecimal = new System.Windows.Forms.ComboBox();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.BtCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(33, 70);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Scheme Code";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(33, 100);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(78, 25);
			this.label2.TabIndex = 1;
			this.label2.Text = "Power";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(33, 130);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(78, 25);
			this.label3.TabIndex = 2;
			this.label3.Text = "Color";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(33, 160);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(78, 25);
			this.label4.TabIndex = 3;
			this.label4.Text = "product Model";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(33, 190);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(78, 25);
			this.label5.TabIndex = 4;
			this.label5.Text = "Nominal Power";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(33, 220);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(78, 25);
			this.label6.TabIndex = 5;
			this.label6.Text = "Pattern";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(419, 74);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(78, 25);
			this.label7.TabIndex = 6;
			this.label7.Text = "label7";
			// 
			// label8
			// 
			this.label8.BackColor = System.Drawing.Color.Silver;
			this.label8.Location = new System.Drawing.Point(383, 70);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(78, 25);
			this.label8.TabIndex = 7;
			this.label8.Text = "Scheme Name";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(383, 100);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(78, 25);
			this.label9.TabIndex = 8;
			this.label9.Text = "label";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(383, 130);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(78, 25);
			this.label10.TabIndex = 9;
			this.label10.Text = "Size(MM)";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(383, 160);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(78, 25);
			this.label11.TabIndex = 10;
			this.label11.Text = "Cell Type";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(383, 190);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(78, 25);
			this.label12.TabIndex = 11;
			this.label12.Text = "Decimal Digits";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TbCode
			// 
			this.TbCode.Location = new System.Drawing.Point(120, 70);
			this.TbCode.Name = "TbCode";
			this.TbCode.Size = new System.Drawing.Size(213, 20);
			this.TbCode.TabIndex = 13;
			// 
			// TbName
			// 
			this.TbName.Location = new System.Drawing.Point(470, 70);
			this.TbName.Name = "TbName";
			this.TbName.Size = new System.Drawing.Size(213, 20);
			this.TbName.TabIndex = 14;
			// 
			// CbPower
			// 
			this.CbPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbPower.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbPower.FormattingEnabled = true;
			this.CbPower.Items.AddRange(new object[] {
			""});
			this.CbPower.Location = new System.Drawing.Point(120, 100);
			this.CbPower.Name = "CbPower";
			this.CbPower.Size = new System.Drawing.Size(211, 21);
			this.CbPower.TabIndex = 15;
			// 
			// CbColor
			// 
			this.CbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbColor.FormattingEnabled = true;
			this.CbColor.Items.AddRange(new object[] {
			""});
			this.CbColor.Location = new System.Drawing.Point(120, 130);
			this.CbColor.Name = "CbColor";
			this.CbColor.Size = new System.Drawing.Size(211, 21);
			this.CbColor.TabIndex = 16;
			// 
			// CbModel
			// 
			this.CbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbModel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbModel.FormattingEnabled = true;
			this.CbModel.Items.AddRange(new object[] {
			""});
			this.CbModel.Location = new System.Drawing.Point(120, 160);
			this.CbModel.Name = "CbModel";
			this.CbModel.Size = new System.Drawing.Size(211, 21);
			this.CbModel.TabIndex = 17;
			// 
			// CbNominal
			// 
			this.CbNominal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbNominal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbNominal.FormattingEnabled = true;
			this.CbNominal.Items.AddRange(new object[] {
			""});
			this.CbNominal.Location = new System.Drawing.Point(120, 190);
			this.CbNominal.Name = "CbNominal";
			this.CbNominal.Size = new System.Drawing.Size(211, 21);
			this.CbNominal.TabIndex = 18;
			// 
			// CbPattern
			// 
			this.CbPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbPattern.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbPattern.FormattingEnabled = true;
			this.CbPattern.Items.AddRange(new object[] {
			""});
			this.CbPattern.Location = new System.Drawing.Point(122, 224);
			this.CbPattern.Name = "CbPattern";
			this.CbPattern.Size = new System.Drawing.Size(211, 21);
			this.CbPattern.TabIndex = 1;
			// 
			// CbLabel
			// 
			this.CbLabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbLabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbLabel.ForeColor = System.Drawing.SystemColors.WindowText;
			this.CbLabel.FormattingEnabled = true;
			this.CbLabel.Items.AddRange(new object[] {
			""});
			this.CbLabel.Location = new System.Drawing.Point(470, 100);
			this.CbLabel.Name = "CbLabel";
			this.CbLabel.Size = new System.Drawing.Size(211, 21);
			this.CbLabel.TabIndex = 20;
			// 
			// CbSize
			// 
			this.CbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbSize.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbSize.FormattingEnabled = true;
			this.CbSize.Items.AddRange(new object[] {
			""});
			this.CbSize.Location = new System.Drawing.Point(470, 130);
			this.CbSize.Name = "CbSize";
			this.CbSize.Size = new System.Drawing.Size(211, 21);
			this.CbSize.TabIndex = 21;
			// 
			// CbCellType
			// 
			this.CbCellType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCellType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbCellType.FormattingEnabled = true;
			this.CbCellType.Items.AddRange(new object[] {
			""});
			this.CbCellType.Location = new System.Drawing.Point(470, 160);
			this.CbCellType.Name = "CbCellType";
			this.CbCellType.Size = new System.Drawing.Size(211, 21);
			this.CbCellType.TabIndex = 22;
			// 
			// CbDecimal
			// 
			this.CbDecimal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDecimal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbDecimal.FormattingEnabled = true;
			this.CbDecimal.Items.AddRange(new object[] {
			""});
			this.CbDecimal.Location = new System.Drawing.Point(470, 190);
			this.CbDecimal.Name = "CbDecimal";
			this.CbDecimal.Size = new System.Drawing.Size(211, 21);
			this.CbDecimal.TabIndex = 23;
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(183, 282);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(100, 26);
			this.BtSubmit.TabIndex = 24;
			this.BtSubmit.Text = "Sumbit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// BtCancel
			// 
			this.BtCancel.Location = new System.Drawing.Point(469, 282);
			this.BtCancel.Name = "BtCancel";
			this.BtCancel.Size = new System.Drawing.Size(100, 26);
			this.BtCancel.TabIndex = 25;
			this.BtCancel.Text = "Cancel";
			this.BtCancel.UseVisualStyleBackColor = true;
			this.BtCancel.Click += new System.EventHandler(this.BtCancelClick);
			// 
			// EditSchemeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Silver;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(825, 380);
			this.Controls.Add(this.BtCancel);
			this.Controls.Add(this.BtSubmit);
			this.Controls.Add(this.CbDecimal);
			this.Controls.Add(this.CbCellType);
			this.Controls.Add(this.CbSize);
			this.Controls.Add(this.CbLabel);
			this.Controls.Add(this.CbPattern);
			this.Controls.Add(this.CbNominal);
			this.Controls.Add(this.CbModel);
			this.Controls.Add(this.CbColor);
			this.Controls.Add(this.CbPower);
			this.Controls.Add(this.TbName);
			this.Controls.Add(this.TbCode);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "EditSchemeForm";
			this.Text = "EditSchemeForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button BtCancel;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.ComboBox CbDecimal;
		private System.Windows.Forms.ComboBox CbCellType;
		private System.Windows.Forms.ComboBox CbSize;
		private System.Windows.Forms.ComboBox CbLabel;
		private System.Windows.Forms.ComboBox CbPattern;
		private System.Windows.Forms.ComboBox CbNominal;
		private System.Windows.Forms.ComboBox CbModel;
		private System.Windows.Forms.ComboBox CbColor;
		private System.Windows.Forms.ComboBox CbPower;
		private System.Windows.Forms.TextBox TbName;
		private System.Windows.Forms.TextBox TbCode;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		
		
	}
}
