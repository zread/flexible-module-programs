﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 4/22/2016
 * Time: 3:09 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class SchemeRunForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchemeRunForm));
			this.CbScheme = new System.Windows.Forms.ComboBox();
			this.BtRun = new System.Windows.Forms.Button();
			this.BtStop = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// CbScheme
			// 
			this.CbScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbScheme.FormattingEnabled = true;
			this.CbScheme.Location = new System.Drawing.Point(105, 80);
			this.CbScheme.Name = "CbScheme";
			this.CbScheme.Size = new System.Drawing.Size(152, 21);
			this.CbScheme.TabIndex = 0;
			this.CbScheme.SelectedIndexChanged += new System.EventHandler(this.CbSchemeSelectedIndexChanged);
			// 
			// BtRun
			// 
			this.BtRun.Location = new System.Drawing.Point(63, 137);
			this.BtRun.Name = "BtRun";
			this.BtRun.Size = new System.Drawing.Size(78, 28);
			this.BtRun.TabIndex = 1;
			this.BtRun.Text = "Run";
			this.BtRun.UseVisualStyleBackColor = true;
			this.BtRun.Click += new System.EventHandler(this.BtRunClick);
			// 
			// BtStop
			// 
			this.BtStop.Location = new System.Drawing.Point(179, 137);
			this.BtStop.Name = "BtStop";
			this.BtStop.Size = new System.Drawing.Size(78, 28);
			this.BtStop.TabIndex = 2;
			this.BtStop.Text = "Stop";
			this.BtStop.UseVisualStyleBackColor = true;
			this.BtStop.Click += new System.EventHandler(this.BtStopClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(27, 77);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 24);
			this.label1.TabIndex = 3;
			this.label1.Text = "Scheme";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// SchemeRunForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(363, 197);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.BtStop);
			this.Controls.Add(this.BtRun);
			this.Controls.Add(this.CbScheme);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SchemeRunForm";
			this.Text = "SchemeRunForm";
			this.Load += new System.EventHandler(this.SchemeRunFormLoad);
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button BtStop;
		private System.Windows.Forms.Button BtRun;
		private System.Windows.Forms.ComboBox CbScheme;
	}
}
