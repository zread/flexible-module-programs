﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 11:52 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;


namespace LabelPrint
{
	/// <summary>
	/// Description of EditDecimalForm.
	/// </summary>
	public partial class EditDecimalForm : Form
	{
		bool Add;
		public EditDecimalForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			TbCode.Enabled = true;	
			Add = true;		
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		public EditDecimalForm(string []items)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			Add = false;
			InitializeComponent();
			TbCode.Enabled = false;	
			InitializingValues(items);			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void InitializingValues(string []items)
		{
			TbCode.Text = items[0];
			TbPmpp.Text = items[1];
			TbVmpp.Text = items[2];
			TbImpp.Text = items[3];
			TbVoc.Text = items[4];
			TbIsc.Text =items[5];
		}
		
		void BtCancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			if(!VarifyData())
			{
				MessageBox.Show("please fill all required filed!");
				return;
			}
			if(!VarifyNumber())
			{
				MessageBox.Show("Only Integer allowed");
				return;
			}
			
			int interid	 = 0;
			int pmpp = Convert.ToInt32(TbPmpp.Text.ToString());
			int impp = Convert.ToInt32(TbImpp.Text.ToString());
			int vmpp = Convert.ToInt32(TbVmpp.Text.ToString());
			int isc = Convert.ToInt32(TbIsc.Text.ToString());
			int voc = Convert.ToInt32(TbVoc.Text.ToString());
			if(Add)
			{
				string sql = @"select * from point where PointID = '"+ TbCode.Text.ToString() +"'";
				SqlDataReader rd = ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
				{
					MessageBox.Show("Already exist, please go to edit!");
					this.Close();
					return;
				}
					rd.Close();
					sql= @"select top 1 interid from point order by interid desc";
					rd = ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
				{
					interid = Convert.ToInt32(rd.GetValue(0));					
				}
				sql = @"insert into point values({0},'{1}',{2},{3},{4},{5},{6},getdate(),{7},null,null)";
				sql = string.Format(sql,interid+1,TbCode.Text.ToString(),pmpp,vmpp,impp,voc,isc,LoginFrm.userID);
				if(ToolsClass.PutsData(sql)!= 1)
				{
					MessageBox.Show("Error!");
					return;
				}
				this.Close();
				return;
			}
			else
			{
				//edit
				string sql = @"update point set Pmpp = {0}, Vmpp ={1},Impp ={2},Voc = {3},Isc ={4},ModifyDate= getdate(), modifyuser = {5} where PointID = '{6}'";
				sql = string.Format(sql,pmpp,vmpp,impp,voc,isc,LoginFrm.userID,TbCode.Text.ToString());
				if(ToolsClass.PutsData(sql)!= 1)
				{
					MessageBox.Show("Selected doesnt exist!");
					return;
				}
				this.Close();
				return;
			}
		}
		private bool VarifyNumber()
		{
			int value = 0;
			if(!int.TryParse(TbPmpp.Text.ToString(), out value))
				return false;
			if(!int.TryParse(TbVmpp.Text.ToString(), out value))
				return false;
			if(!int.TryParse(TbImpp.Text.ToString(), out value))
				return false;
			if(!int.TryParse(TbVoc.Text.ToString(), out value))
				return false;
			if(!int.TryParse(TbIsc.Text.ToString(), out value))
				return false;
			
			return true;			
		}
		private bool VarifyData()
		{
			foreach (Control ctl in Controls) {
				if(string.IsNullOrEmpty(ctl.Text.ToString()))
					return false;
			}
			return true;
		}
	}
}
