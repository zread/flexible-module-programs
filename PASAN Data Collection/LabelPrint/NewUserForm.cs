﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 2:10 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace LabelPrint
{
	/// <summary>
	/// Description of NewUserForm.
	/// </summary>
	public partial class NewUserForm : Form
	{
		string UserName = "";
		string PassWord = "";
		string Specification = "";
		string Cpw = "";
		string ConnDB = "";
		public NewUserForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			loadParameters();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		
		void Bt_OkClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(Tb_UN.Text.ToString()))
				return;
			if(string.IsNullOrEmpty(Tb_PW.Text.ToString()))
				return;
			if(string.IsNullOrEmpty(Tb_CFPW.Text.ToString()))
				return;
			if(!string.IsNullOrEmpty(Tb_SP.Text.ToString()))
				Specification = Tb_SP.Text.ToString();
				UserName = Tb_UN.Text.ToString();
				PassWord = Tb_PW.Text.ToString();
				Cpw = Tb_CFPW.Text.ToString();
			if(!string.Equals(PassWord,Cpw))
			{
				MessageBox.Show("Confirm pass word!");
				return;
			}
				string sql = @"select * from [LabelPrintDB].[dbo].[UserLogin] where userNM = '{0}'";
				string.Format(sql,UserName);
				DataTable dt = ToolsClass.PullData(sql,ConnDB);
				if(dt.Rows.Count > 0)
				{
					MessageBox.Show("User already exist!");
				}
				else
				{
					sql = @" select InterID from [LabelPrintDB].[dbo].[UserLogin] order by interid desc";
					dt = ToolsClass.PullData(sql,ConnDB);
					
					string ID = dt.Rows[0]["InterId"].ToString();
					int InterID = Convert.ToInt16(ID) + 1;
		            
		            
					sql = @" Insert into [LabelPrintDB].[dbo].[UserLogin] values('{0}','{1}','{2}','{3}',NULL,NULL,NULL,NULL,1,16)";
					sql = string.Format(sql, InterID,UserName,PassWord,Specification);
					ToolsClass.PutsData(sql,ConnDB);
					this.Close();
				
					
				}
		}
		private void loadParameters()
		{
			ConnDB = ToolsClass.getConfig ("Conn",false,"","Config.xml");			
		}
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
