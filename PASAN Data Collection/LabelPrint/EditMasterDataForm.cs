﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/16/2016
 * Time: 11:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace LabelPrint
{
	/// <summary>
	/// Description of EditMasterDataForm.
	/// </summary>
	public partial class EditMasterDataForm : Form
	{
		private int ClassId = 0;
		private int ItemId = 0;
		public EditMasterDataForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		public EditMasterDataForm(int Class_id, int Item_id)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			ClassId = Class_id;
			ItemId = Item_id;
			
			ListMaterDataCode();
		}
		
		void ListMaterDataCode()
		{
			string ItemValue = "";
			string sql = @" select ItemID, ItemValue from [LabelPrintDB].[dbo].[Item] where ItemClassInterID = {0} and ItemId = {1}";
			int i = -1;
			sql = string.Format(sql,ClassId,ItemId);
			DataTable dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count > 0)
			{
				i = Convert.ToInt32 (dt.Rows[0][0]);
				ItemValue = dt.Rows[0][1].ToString();
			}
			else
			{
				MessageBox.Show("Item Deleted, please refresh!");
			}
		
			Tb_Code.Text = Convert.ToString(i);
			Tb_Name.Text = ItemValue;		
				
		}
		
		
		void Bt_SubClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(Tb_Name.Text.ToString()))
			{
				MessageBox.Show("Name of Master Data cannot be empty! ");
				return;
			}
			string sql = @"select * from [LabelPrintDB].[dbo].[Item] where ItemClassInterID = {0} and ItemId = {1}";
			sql = string.Format(sql, ClassId, ItemId);
			DataTable dt = ToolsClass.PullData(sql);
			if (dt.Rows.Count  < 1) 
			{
				MessageBox.Show("Data has been deleted, please refresh!");
				return;
			}
			sql = @"update item set ItemValue = '{0}' where ItemClassInterID = {1} and ItemID = {2}";
			sql = string.Format(sql,Tb_Name.Text.ToString(),ClassId,ItemId);
			ToolsClass.PutsData(sql);
			
			this.Close();
			
			
		}
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
