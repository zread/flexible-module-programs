﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/10/2016
 * Time: 4:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class EditNominalPowerForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditNominalPowerForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.TbCode = new System.Windows.Forms.TextBox();
			this.TbName = new System.Windows.Forms.TextBox();
			this.TbValue1 = new System.Windows.Forms.TextBox();
			this.TbValue2 = new System.Windows.Forms.TextBox();
			this.TbPower1 = new System.Windows.Forms.TextBox();
			this.TbPower2 = new System.Windows.Forms.TextBox();
			this.CbOpr1 = new System.Windows.Forms.ComboBox();
			this.CbOpr2 = new System.Windows.Forms.ComboBox();
			this.CbCtl = new System.Windows.Forms.ComboBox();
			this.CbDisplay = new System.Windows.Forms.ComboBox();
			this.CbNominalPower = new System.Windows.Forms.ComboBox();
			this.BtAdd = new System.Windows.Forms.Button();
			this.BtEdit = new System.Windows.Forms.Button();
			this.BtDelete = new System.Windows.Forms.Button();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.BtQuit = new System.Windows.Forms.Button();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.PmaxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.PmaxNM = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Pmax = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.MinValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Ctl = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.MaxValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Resv1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Resv2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Resv3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(33, 73);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Code";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(33, 113);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 22);
			this.label2.TabIndex = 1;
			this.label2.Text = "Nominal Power";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(402, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(67, 22);
			this.label3.TabIndex = 2;
			this.label3.Text = "Name";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(286, 114);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(67, 22);
			this.label4.TabIndex = 3;
			this.label4.Text = "Mesured";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(286, 150);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(67, 22);
			this.label5.TabIndex = 4;
			this.label5.Text = "Mesured";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(240, 183);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(90, 22);
			this.label6.TabIndex = 5;
			this.label6.Text = "Power Grade 1";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(240, 221);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(90, 22);
			this.label7.TabIndex = 6;
			this.label7.Text = "Power Grade 2";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(508, 183);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(67, 22);
			this.label8.TabIndex = 7;
			this.label8.Text = "Display";
			// 
			// TbCode
			// 
			this.TbCode.Location = new System.Drawing.Point(120, 73);
			this.TbCode.Name = "TbCode";
			this.TbCode.Size = new System.Drawing.Size(89, 20);
			this.TbCode.TabIndex = 8;
			// 
			// TbName
			// 
			this.TbName.Location = new System.Drawing.Point(486, 77);
			this.TbName.Name = "TbName";
			this.TbName.Size = new System.Drawing.Size(89, 20);
			this.TbName.TabIndex = 10;
			// 
			// TbValue1
			// 
			this.TbValue1.Location = new System.Drawing.Point(442, 114);
			this.TbValue1.Name = "TbValue1";
			this.TbValue1.Size = new System.Drawing.Size(89, 20);
			this.TbValue1.TabIndex = 11;
			// 
			// TbValue2
			// 
			this.TbValue2.Location = new System.Drawing.Point(442, 147);
			this.TbValue2.Name = "TbValue2";
			this.TbValue2.Size = new System.Drawing.Size(89, 20);
			this.TbValue2.TabIndex = 11;
			// 
			// TbPower1
			// 
			this.TbPower1.Location = new System.Drawing.Point(336, 183);
			this.TbPower1.Name = "TbPower1";
			this.TbPower1.Size = new System.Drawing.Size(89, 20);
			this.TbPower1.TabIndex = 101;
			// 
			// TbPower2
			// 
			this.TbPower2.Location = new System.Drawing.Point(336, 221);
			this.TbPower2.Name = "TbPower2";
			this.TbPower2.Size = new System.Drawing.Size(89, 20);
			this.TbPower2.TabIndex = 102;
			// 
			// CbOpr1
			// 
			this.CbOpr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbOpr1.FormattingEnabled = true;
			this.CbOpr1.Items.AddRange(new object[] {
			"",
			"=",
			">",
			">="});
			this.CbOpr1.Location = new System.Drawing.Point(336, 111);
			this.CbOpr1.Name = "CbOpr1";
			this.CbOpr1.Size = new System.Drawing.Size(84, 21);
			this.CbOpr1.TabIndex = 14;
			// 
			// CbOpr2
			// 
			this.CbOpr2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbOpr2.FormattingEnabled = true;
			this.CbOpr2.Items.AddRange(new object[] {
			"",
			"=",
			"<",
			"<="});
			this.CbOpr2.Location = new System.Drawing.Point(336, 146);
			this.CbOpr2.Name = "CbOpr2";
			this.CbOpr2.Size = new System.Drawing.Size(84, 21);
			this.CbOpr2.TabIndex = 15;
			// 
			// CbCtl
			// 
			this.CbCtl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCtl.FormattingEnabled = true;
			this.CbCtl.Items.AddRange(new object[] {
			"And",
			"Or"});
			this.CbCtl.Location = new System.Drawing.Point(562, 111);
			this.CbCtl.Name = "CbCtl";
			this.CbCtl.Size = new System.Drawing.Size(84, 21);
			this.CbCtl.TabIndex = 16;
			// 
			// CbDisplay
			// 
			this.CbDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDisplay.FormattingEnabled = true;
			this.CbDisplay.Items.AddRange(new object[] {
			"",
			"Y",
			"N"});
			this.CbDisplay.Location = new System.Drawing.Point(562, 180);
			this.CbDisplay.Name = "CbDisplay";
			this.CbDisplay.Size = new System.Drawing.Size(84, 21);
			this.CbDisplay.TabIndex = 103;
			// 
			// CbNominalPower
			// 
			this.CbNominalPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbNominalPower.FormattingEnabled = true;
			this.CbNominalPower.Location = new System.Drawing.Point(120, 110);
			this.CbNominalPower.Name = "CbNominalPower";
			this.CbNominalPower.Size = new System.Drawing.Size(84, 21);
			this.CbNominalPower.TabIndex = 18;
			// 
			// BtAdd
			// 
			this.BtAdd.Location = new System.Drawing.Point(25, 216);
			this.BtAdd.Name = "BtAdd";
			this.BtAdd.Size = new System.Drawing.Size(59, 23);
			this.BtAdd.TabIndex = 20;
			this.BtAdd.Text = "Add";
			this.BtAdd.UseVisualStyleBackColor = true;
			this.BtAdd.Click += new System.EventHandler(this.BtAddClick);
			// 
			// BtEdit
			// 
			this.BtEdit.Location = new System.Drawing.Point(90, 216);
			this.BtEdit.Name = "BtEdit";
			this.BtEdit.Size = new System.Drawing.Size(59, 23);
			this.BtEdit.TabIndex = 20;
			this.BtEdit.Text = "Edit";
			this.BtEdit.UseVisualStyleBackColor = true;
			this.BtEdit.Click += new System.EventHandler(this.BtEditClick);
			// 
			// BtDelete
			// 
			this.BtDelete.Location = new System.Drawing.Point(155, 216);
			this.BtDelete.Name = "BtDelete";
			this.BtDelete.Size = new System.Drawing.Size(59, 23);
			this.BtDelete.TabIndex = 21;
			this.BtDelete.Text = "Delete";
			this.BtDelete.UseVisualStyleBackColor = true;
			this.BtDelete.Click += new System.EventHandler(this.BtDeleteClick);
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(155, 421);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(75, 23);
			this.BtSubmit.TabIndex = 21;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// BtQuit
			// 
			this.BtQuit.Location = new System.Drawing.Point(417, 421);
			this.BtQuit.Name = "BtQuit";
			this.BtQuit.Size = new System.Drawing.Size(75, 23);
			this.BtQuit.TabIndex = 22;
			this.BtQuit.Text = "Quit";
			this.BtQuit.UseVisualStyleBackColor = true;
			this.BtQuit.Click += new System.EventHandler(this.BtQuitClick);
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToResizeColumns = false;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.ColumnHeadersVisible = false;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.ID,
			this.PmaxID,
			this.PmaxNM,
			this.Pmax,
			this.MinValue,
			this.Ctl,
			this.MaxValue,
			this.Resv1,
			this.Resv2,
			this.Resv3});
			this.dataGridView1.GridColor = System.Drawing.Color.White;
			this.dataGridView1.Location = new System.Drawing.Point(42, 259);
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(603, 145);
			this.dataGridView1.TabIndex = 104;
			this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
			// 
			// ID
			// 
			this.ID.HeaderText = "ID";
			this.ID.Name = "ID";
			this.ID.ReadOnly = true;
			this.ID.Width = 50;
			// 
			// PmaxID
			// 
			this.PmaxID.HeaderText = "PmaxID";
			this.PmaxID.Name = "PmaxID";
			this.PmaxID.ReadOnly = true;
			// 
			// PmaxNM
			// 
			this.PmaxNM.HeaderText = "PmaxNM";
			this.PmaxNM.Name = "PmaxNM";
			this.PmaxNM.ReadOnly = true;
			// 
			// Pmax
			// 
			this.Pmax.HeaderText = "Pmax";
			this.Pmax.Name = "Pmax";
			this.Pmax.ReadOnly = true;
			// 
			// MinValue
			// 
			this.MinValue.HeaderText = "MinValue";
			this.MinValue.Name = "MinValue";
			this.MinValue.ReadOnly = true;
			this.MinValue.Width = 150;
			// 
			// Ctl
			// 
			this.Ctl.HeaderText = "Ctl";
			this.Ctl.Name = "Ctl";
			this.Ctl.ReadOnly = true;
			// 
			// MaxValue
			// 
			this.MaxValue.HeaderText = "MaxValue";
			this.MaxValue.Name = "MaxValue";
			this.MaxValue.ReadOnly = true;
			this.MaxValue.Width = 150;
			// 
			// Resv1
			// 
			this.Resv1.HeaderText = "PowerGrade1";
			this.Resv1.Name = "Resv1";
			this.Resv1.ReadOnly = true;
			// 
			// Resv2
			// 
			this.Resv2.HeaderText = "Display";
			this.Resv2.Name = "Resv2";
			this.Resv2.ReadOnly = true;
			// 
			// Resv3
			// 
			this.Resv3.HeaderText = "PowerGrade2";
			this.Resv3.Name = "Resv3";
			this.Resv3.ReadOnly = true;
			// 
			// EditNominalPowerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(708, 456);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.BtQuit);
			this.Controls.Add(this.BtSubmit);
			this.Controls.Add(this.BtDelete);
			this.Controls.Add(this.BtAdd);
			this.Controls.Add(this.BtEdit);
			this.Controls.Add(this.CbNominalPower);
			this.Controls.Add(this.CbDisplay);
			this.Controls.Add(this.CbCtl);
			this.Controls.Add(this.CbOpr2);
			this.Controls.Add(this.CbOpr1);
			this.Controls.Add(this.TbPower2);
			this.Controls.Add(this.TbPower1);
			this.Controls.Add(this.TbValue2);
			this.Controls.Add(this.TbValue1);
			this.Controls.Add(this.TbName);
			this.Controls.Add(this.TbCode);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "EditNominalPowerForm";
			this.Text = "EditNominalPowerForm";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.DataGridViewTextBoxColumn Resv3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Resv2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Resv1;
		private System.Windows.Forms.DataGridViewTextBoxColumn MaxValue;
		private System.Windows.Forms.DataGridViewTextBoxColumn Ctl;
		private System.Windows.Forms.DataGridViewTextBoxColumn MinValue;
		private System.Windows.Forms.DataGridViewTextBoxColumn Pmax;
		private System.Windows.Forms.DataGridViewTextBoxColumn PmaxNM;
		private System.Windows.Forms.DataGridViewTextBoxColumn PmaxID;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button BtQuit;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.Button BtDelete;
		private System.Windows.Forms.Button BtEdit;
		private System.Windows.Forms.Button BtAdd;
		private System.Windows.Forms.ComboBox CbNominalPower;
		private System.Windows.Forms.ComboBox CbDisplay;
		private System.Windows.Forms.ComboBox CbCtl;
		private System.Windows.Forms.ComboBox CbOpr2;
		private System.Windows.Forms.ComboBox CbOpr1;
		private System.Windows.Forms.TextBox TbPower2;
		private System.Windows.Forms.TextBox TbPower1;
		private System.Windows.Forms.TextBox TbValue2;
		private System.Windows.Forms.TextBox TbValue1;
		private System.Windows.Forms.TextBox TbName;
		private System.Windows.Forms.TextBox TbCode;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
