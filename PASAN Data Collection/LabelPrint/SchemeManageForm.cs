﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 2:36 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of SchemeManageForm.
	/// </summary>
	public partial class SchemeManageForm : Form
	{
		public SchemeManageForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ShowRecord();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
			void ShowRecord()
			{
				string sql = @"select  [SchemeID] ,[SchemeNM] From Scheme";				
				DataTable dt = ToolsClass.PullData(sql);
				
				foreach ( DataRow  row  in dt.Rows) {
					string[]name = {row[0].ToString(),row[1].ToString()};
							dataGridView1.Rows.Add(name);
					}
			}
	
		
		void BtAddClick(object sender, EventArgs e)
		{
			EditSchemeForm frm = new EditSchemeForm();
			frm.StartPosition = FormStartPosition.CenterScreen;
			frm.ShowDialog();
			dataGridView1.Rows.Clear();
			ShowRecord();
				
		}
		
		void BtEditClick(object sender, EventArgs e)
		{
			
			if(dataGridView1.SelectedRows.Count != 1)
			{
				MessageBox.Show("Please select one row!");
				return;
			}
			EditSchemeForm frm = new EditSchemeForm(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
			frm.StartPosition = FormStartPosition.CenterScreen;
			frm.ShowDialog();
			dataGridView1.Rows.Clear();
			ShowRecord();
		}
		
		void BtDeleteClick(object sender, EventArgs e)
		{
			if(dataGridView1.SelectedRows.Count != 1)
			{
				MessageBox.Show("Please select one row!");
				return;
			}
			DialogResult result1 = MessageBox.Show("Do you want to delete scheme: "+dataGridView1.SelectedRows[0].Cells[0].Value.ToString() +"?","Attention",  MessageBoxButtons.YesNo);
			if(result1 == DialogResult.Yes)
			{
				string sql = @"delete from Scheme where [SchemeID] = '"+dataGridView1.SelectedRows[0].Cells[0].Value.ToString()+"'";
				int i = ToolsClass.PutsData(sql);
				if(i == 1)				
					MessageBox.Show("Deleting Successful!");
				else
					MessageBox.Show("Deleting failed!");				
			}
			dataGridView1.Rows.Clear();
			ShowRecord();
			
				return;
			
		}
		
		void BtQuitClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
