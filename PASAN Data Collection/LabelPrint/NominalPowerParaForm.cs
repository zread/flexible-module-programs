﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 8:50 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;


namespace LabelPrint
{
	/// <summary>
	/// Description of NominalPowerParaForm.
	/// </summary>
	public partial class NominalPowerParaForm : Form
	{
		public NominalPowerParaForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ShowList();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}		
			void ShowList()
		{
				string sql = @"select  modeltypeid from modeltype ";
				DataTable dt = ToolsClass.PullData(sql);
				if(dt.Rows.Count< 1)
				return;
				foreach (DataRow row in dt.Rows) {
				string temp = row[0].ToString();
				LBModel.Items.AddRange(new object[] {temp});
			
			}			
		}
			
			
			
		
		void BtEditClick(object sender, EventArgs e)
		{
			if(LBModel.SelectedIndex < 0)
			return;
			EditNominalParam frm = new EditNominalParam(LBModel.Text.ToString());
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				LBModel.Items.Clear();
				ShowList();
		}
		
		void BtAddClick(object sender, EventArgs e)
		{
			EditNominalParam frm = new EditNominalParam();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				LBModel.Items.Clear();
				ShowList();
		}
		
		void BtDeleteClick(object sender, EventArgs e)
		{
			if(LBModel.SelectedIndex < 0)
			return;
			string sql = @" delete from ModelType where ModelTypeID = '"+ LBModel.Text.ToString() + "'";
			if(ToolsClass.PutsData(sql) > 0 )
			{
				MessageBox.Show("Deleting successful!");
				LBModel.Items.Clear();
				ShowList();
				return;
			}
			else
			{
				MessageBox.Show("Not Deleted!");
				return;
			}
		}
		
		void BtQuitClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
