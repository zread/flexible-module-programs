﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/16/2016
 * Time: 11:29 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;


namespace LabelPrint
{
	/// <summary>
	/// Description of AddMasterDataForm.
	/// </summary>
	public partial class AddMasterDataForm : Form
	{
		private int ClassId = 0;
		
		public AddMasterDataForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//		
			
						
		}
		
		 public AddMasterDataForm(int index)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ClassId = index;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			ListMaterDataCode(index);
		}
		
		void ListMaterDataCode(int index)
		{
			string sql = @" select ItemID from [LabelPrintDB].[dbo].[Item] where ItemClassInterID = '{0}' order by InterId desc  ";
			int i = -1;
			sql = string.Format(sql,index);
			DataTable dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count > 0)
				i = Convert.ToInt32 (dt.Rows[0][0]);
			else
				i = 0;
		
			Tb_Code.Text = Convert.ToString(i+1);
				
		}
		void Bt_SubClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(Tb_Name.Text.ToString()))
			{
				MessageBox.Show("Please enter Name of Master Data! ");
				return;
			}
			
			string sql = @"select * from [LabelPrintDB].[dbo].[Item] where ItemClassInterId = {0} and ItemValue = '{1}' ";
			sql = string.Format(sql,ClassId,Tb_Name.Text.ToString());
			DataTable dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count > 0)
			{
				MessageBox.Show("Item already exist!");
				return;
			}
			sql = @"select top 1 InterID from [LabelPrintDB].[dbo].[Item] order by CreateDate desc";
			dt = ToolsClass.PullData(sql);
			
			int InterId = Convert.ToInt32(dt.Rows[0][0]) + 1;
				
				
			sql = @"insert into [LabelPrintDB].[dbo].[Item] Values ({0},'{1}',{2},'{3}',getdate(),0,null,null)";
			sql = string.Format(sql,InterId,ClassId,Convert.ToInt32(Tb_Code.Text.ToString()),Tb_Name.Text.ToString());
			ToolsClass.PutsData(sql);
			this.Close();
			
		}
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
