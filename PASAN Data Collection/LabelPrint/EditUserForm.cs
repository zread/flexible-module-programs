﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 3:45 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of EditUserForm.
	/// </summary>
	public partial class EditUserForm : Form
	{
		string UserName = "";
		string PassWord = "";
		string Specification = "";
		string Cpw = "";		
		string ConnDB = "";
		
		public EditUserForm(string UserNM)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			loadParameters();
			string sql = @"Select UserNote, UserPW from [LabelPrintDB].[dbo].[UserLogin] where UserNM = '{0}'";
			sql = string.Format(sql,UserNM);
			DataTable dt = ToolsClass.PullData(sql,ConnDB);			
			foreach (DataRow row in dt.Rows) {
				
				Specification = row[0].ToString();
				PassWord = row[1].ToString();
				Cpw = row[1].ToString();
				
			}	
			
			Tb_UN.Text = UserNM;			
			Tb_SP.Text = Specification;
			Tb_PW.Text = PassWord;
			Tb_CFPW.Text = Cpw;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		private void loadParameters()
		{
			ConnDB = ToolsClass.getConfig ("Conn",false,"","Config.xml");			
		}
		
		void Bt_OkClick(object sender, EventArgs e)
		{
			
			if(string.IsNullOrEmpty(Tb_UN.Text.ToString()))
				return;
			if(string.IsNullOrEmpty(Tb_PW.Text.ToString()))
				return;
			if(string.IsNullOrEmpty(Tb_CFPW.Text.ToString()))
				return;
			if(!string.IsNullOrEmpty(Tb_SP.Text.ToString()))
				Specification = Tb_SP.Text.ToString();
				UserName = Tb_UN.Text.ToString();
				PassWord = Tb_PW.Text.ToString();
				Cpw = Tb_CFPW.Text.ToString();
			if(!string.Equals(PassWord,Cpw))
			{
				MessageBox.Show("Confirm pass word!");
				return;
			}	
			
			string sql = @" update UserLogin set Usernote = '{0}', UserPW = '{1}'  where UserNM = '{2}'";
			sql = string.Format(sql,Specification,PassWord,UserName);
					ToolsClass.PutsData(sql,ConnDB);
					this.Close();
				
					
				
		}
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		
		
	}
}
