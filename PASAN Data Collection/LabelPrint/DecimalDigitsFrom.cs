﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 11:31 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of DecimalDigitsFrom.
	/// </summary>
	public partial class DecimalDigitsFrom : Form
	{
		public DecimalDigitsFrom()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			ShowList();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void ShowList()
		{
			string sql = @"Select [PointID]
		      ,[Pmpp]
		      ,[Vmpp]
		      ,[Impp]
		      ,[Voc]
		      ,[Isc]From Point";
			DataTable dt = ToolsClass.PullData(sql);
			foreach (DataRow row  in dt.Rows) {
					string[]name = {row[0].ToString(),row[1].ToString(),row[2].ToString(),row[3].ToString(),row[4].ToString(),row[5].ToString()};
					dataGridView1.Rows.Add(name);	
			}
		}
		
		void BtAddClick(object sender, EventArgs e)
		{
			EditDecimalForm frm = new EditDecimalForm();
			frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				dataGridView1.Rows.Clear();
				ShowList();
				
		}
		
		void BtEditClick(object sender, EventArgs e)
		{
			if(dataGridView1.CurrentRow.Index < 0)
					return;
			int rowindex = dataGridView1.CurrentRow.Index;
			
			string [] item = new string[6];
			item[0] = dataGridView1.Rows[rowindex].Cells["Code"].Value.ToString();
			item[1] = dataGridView1.Rows[rowindex].Cells["Pmpp"].Value.ToString();
			item[2] = dataGridView1.Rows[rowindex].Cells["Vmpp"].Value.ToString();
			item[3] = dataGridView1.Rows[rowindex].Cells["Impp"].Value.ToString();
			item[4] = dataGridView1.Rows[rowindex].Cells["Voc"].Value.ToString();
			item[5] = dataGridView1.Rows[rowindex].Cells["Isc"].Value.ToString();
			EditDecimalForm frm = new EditDecimalForm(item);
			frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
				dataGridView1.Rows.Clear();
				ShowList();
		}
		
		void BtDeleteClick(object sender, EventArgs e)
		{
			string sql = @"delete from point where pointid = '" + dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells["Code"].Value.ToString() +"'";
			if(ToolsClass.PutsData(sql)!= 1)
				MessageBox.Show("Selected does not exist!");
			dataGridView1.Rows.Clear();
				ShowList();
		}
		
		void BtQuitClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
