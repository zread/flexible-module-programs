﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/15/2016
 * Time: 12:19 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace LabelPrint
{
	/// <summary>
	/// Description of ListClass.
	/// </summary>
	public partial class ListClass 
	{
		public ListClass()
		{
			
		}
		private string _PMaxID = null;
		private string _PMaxNM = null;
		private string _PMaxNum = "1";
		private string _PMax = null;
		private string _MinValue = null;
		private string _CtlValue = null;
		private string _MaxValue = null;
		private string _CreateDate = null;
		private string _CreateUser = null;
      	private string _ModifyDate = null;
      	private string _ModifyUser = null;
      	private string _RESV1 = "";
      	private string _RESV2 = "";
      	private string _RESV3 = "";
		
        public string PMaxID
        {
            get { return _PMaxID; }
            set { _PMaxID = value; }
        }
        public string PMaxNM
        {
            get { return _PMaxNM; }
            set { _PMaxNM = value; }
        }
        public string PMaxNum
        {
            get { return _PMaxNum; }
            set { _PMaxNum = value; }
        }
        public string PMax
        {
            get { return _PMax; }
            set { _PMax = value; }
        }
        public string MinValue
        {
            get { return _MinValue; }
            set { _MinValue = value; }
        }
        public string CtlValue
        {
            get { return _CtlValue; }
            set { _CtlValue = value; }
        }
        public string MaxValue
        {
            get { return _MaxValue; }
            set { _MaxValue = value; }
        }
        public string CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }
        public string CreateUser
        {
            get { return _CreateUser; }
            set { _CreateUser = value; }
        }
        public string ModifyDate
        {
            get { return _ModifyDate; }
            set { _ModifyDate = value; }
        }
        public string ModifyUser
        {
            get { return _ModifyUser; }
            set { _ModifyUser = value; }
        }
        public string RESV1
        {
            get { return _RESV1; }
            set { _RESV1 = value; }
        }
        public string RESV2
        {
            get { return _RESV2; }
            set { _RESV2 = value; }
        }
        public string RESV3
        {
            get { return _RESV3; }
            set { _RESV3 = value; }
        }
      
	}
}
