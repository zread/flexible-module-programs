﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 9:01 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace LabelPrint
{
	/// <summary>
	/// Description of EditNominalParam.
	/// </summary>
	public partial class EditNominalParam : Form
	{
		string _Code = "";
		public EditNominalParam()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			SetupAllSelection();
			CbModelType.Enabled = true;
			CbCellType.Enabled = true;
			CbMaxPower.Enabled = true;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		public EditNominalParam(string Code)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			SetupAllSelection();
			CbModelType.Enabled = false;
			CbCellType.Enabled = false;
			CbMaxPower.Enabled = false;
			_Code = Code;
			Showparameter();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void Showparameter()
		{
			TbCode.Text = _Code;
			string sql = @"select [ItemModelInterID],[ItemPmaxInterID],[celltype],[NOValue]
					      ,[OCVValue]
					      ,[SCCValue]
					      ,[VPPValue]
					      ,[CPPValue]
					      ,[RMPValue] from ModelType where ModelTypeID = '"+ _Code+ "'";
			DataTable dt = ToolsClass.PullData(sql);
			int PowerID = 0, ModelID = 0;
			ModelID = Convert.ToInt32(dt.Rows[0][0].ToString());
			PowerID = Convert.ToInt32(dt.Rows[0][1].ToString());
			CbCellType.Text = dt.Rows[0][2].ToString();
			TbArt.Text = dt.Rows[0][3].ToString();
			TbVoc.Text = dt.Rows[0][4].ToString();
			TbIsc.Text = dt.Rows[0][5].ToString();
			TbVmpp.Text = dt.Rows[0][6].ToString();
			TbImpp.Text = dt.Rows[0][7].ToString();
			TbPmpp.Text = dt.Rows[0][8].ToString();
			sql = @"select itemvalue from item where interid = '" +ModelID + "'";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read()&&rd!=null)
			CbModelType.Text = rd.GetString(0);
			sql = @"select itemvalue from item where interid = '" +PowerID + "'";
			rd.Close();
			rd = ToolsClass.GetDataReader(sql);	
			if(rd.Read()&&rd!=null)
			CbMaxPower.Text = rd.GetString(0);	
			return;			
		}
		void SetupAllSelection()
		{
			string sql = "";			
			//Modeltyp
			sql = @" select ItemValue from item where ItemClassInterID = 1";
			DataTable dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count < 1)
			{
				this.Close();
				return;
			}
			CbModelType.Items.AddRange(new object[]{""});
			foreach (DataRow row in dt.Rows) {
				string temp = row[0].ToString();
				CbModelType.Items.AddRange(new object[]{temp});
			}
			
			//Pmax
			sql = @" select ItemValue from item where ItemClassInterID = 2";
			dt.Rows.Clear();
			dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count < 1)
			{
				this.Close();
				return;
			}
			CbMaxPower.Items.AddRange(new object[]{""});
			foreach (DataRow row in dt.Rows) {
				string temp = row[0].ToString();
				CbMaxPower.Items.AddRange(new object[]{temp});
			}
			
			//celltype
			sql = @"select ItemValue from item where ItemClassInterID = 3";
			dt.Rows.Clear();
			dt = ToolsClass.PullData(sql);
			if(dt.Rows.Count < 1)
			{
				this.Close();
				return;
			}
			CbCellType.Items.AddRange(new object[]{""});
			foreach (DataRow row in dt.Rows) {
				string temp = row[0].ToString();
				CbCellType.Items.AddRange(new object[]{temp});
			}
		
		}
		
		void CbModelTypeSelectedIndexChanged(object sender, EventArgs e)
		{
			TbCode.Text = CbModelType.Text.ToString() + "-" + CbMaxPower.Text.ToString() + CbCellType.Text.ToString();
		}
		
		void CbMaxPowerSelectedIndexChanged(object sender, EventArgs e)
		{
			TbCode.Text = CbModelType.Text.ToString() + "-" + CbMaxPower.Text.ToString() + CbCellType.Text.ToString();
		}
		
		void CbCellTypeSelectedIndexChanged(object sender, EventArgs e)
		{
			TbCode.Text = CbModelType.Text.ToString() + "-" + CbMaxPower.Text.ToString() + CbCellType.Text.ToString();
		}
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			if(!VarifyData())
				return;
			if(string.IsNullOrEmpty(_Code))
			{
				string sqlcheck =@"select * from ModelType where modeltypeid ='" + TbCode.Text.ToString() +"'";
				SqlDataReader rdcheck = ToolsClass.GetDataReader(sqlcheck);
				if(rdcheck.Read() && rdcheck!= null)
				{
					MessageBox.Show("Already exist, please go to edit mode!");
					this.Close();
					return;
				}
			}
			int interid = 0;
			string sql = @"select top 1 interID from ModelType order by interID desc ";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
			{
				interid = Convert.ToInt32(rd.GetValue(0));
			}
			rd.Close();
			sql = @"select * from ModelType where ModelTypeID = '"+ TbCode.Text.ToString() +"'";		
			rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
			{
				sql = "update ModelType set NOValue = '{0}', OCVValue = '{1}', SCCValue = '{2}',VPPValue ='{3}'," +
				"CPPValue = '{4}', RMPValue = '{5}', ModifyDate = getdate(), ModifyUserID = {6},celltype = '{7}' where ModelTypeID = '{8}'";
				sql = string.Format(sql,TbArt.Text.ToString(),TbVoc.Text.ToString(),TbIsc.Text.ToString(),
				                    TbVmpp.Text.ToString(),TbImpp.Text.ToString(),TbPmpp.Text.ToString(),
				                    LoginFrm.userID,CbCellType.Text.ToString(),TbCode.Text.ToString());
				
				if(ToolsClass.PutsData(sql) == 1)
				{
					MessageBox.Show("Successful!");
					this.Close();
				}
				else
				{
					MessageBox.Show("Error");
					return;
				}
			}
			else
			{
				int pmaxID= 0, modeltypeID = 0;
				sql = @" select InterID from Item where ItemClassInterID =2 and ItemValue = '"+ CbMaxPower.Text.ToString()+"'";
				rd.Close();
				rd = ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
					pmaxID = Convert.ToInt32(rd.GetValue(0));
				sql = @"select InterID  from Item where ItemClassInterID = 1 and ItemValue = '"+ CbModelType.Text.ToString()+"'";
				rd.Close();
				rd = ToolsClass.GetDataReader(sql);
				if(rd.Read() && rd!=null)
					modeltypeID = Convert.ToInt32(rd.GetValue(0));
				
				if(pmaxID == 0 || modeltypeID == 0)
				{
					MessageBox.Show("please maintain power range or Model type!");
						return;
				}
				sql  = (string.IsNullOrEmpty(CbCellType.Text.ToString()))?"insert into ModelType values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',getdate(),null,'{10}',null,'{11}')":"insert into ModelType values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',getdate(),null,'',null,null)";
				sql = string.Format(sql,interid++,TbCode.Text.ToString(),modeltypeID,pmaxID,TbArt.Text.ToString(),TbVoc.Text.ToString(),TbIsc.Text.ToString(),
				                    TbVmpp.Text.ToString(),TbImpp.Text.ToString(),TbPmpp.Text.ToString(),LoginFrm.userID,CbCellType.Text.ToString());
					if(ToolsClass.PutsData(sql) == 1)
				{
					MessageBox.Show("Successful!");
					this.Close();
				}
				else
				{
					MessageBox.Show("Error");
					return;
				}
				
			}
			
		}
		
		private bool VarifyData()
		{
			bool check = true;
			
			foreach (Control ctl in Controls) 
			{
				if(ctl.TabIndex > 99)
					continue;				
				if(string.IsNullOrEmpty(ctl.Text.ToString()))
					check = false;
			}
			return check;
		}
		
		void BtCancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
