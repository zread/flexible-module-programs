﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 4/22/2016
 * Time: 3:09 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Linq;
//using System.Threading.Tasks;

namespace LabelPrint
{
	/// <summary>
	/// Description of SchemeRunForm.
	/// </summary>
	public partial class SchemeRunForm : Form
	{
		int SchemeInterID = 0;
		bool isrunning = false;
		System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
		int Delay = Convert.ToInt32(ToolsClass.getConfig("Period",false,"","config.xml"));
		string ModelType = "", CellSpec = "", CellType = "", CellColor ="", Pattern = "";
		string connPLC = ToolsClass.getConfig("connPLC",false,"","config.xml");
		string Line = ToolsClass.getConfig("Line",false,"","config.xml");
		string SCM = "";
		private Thread workerThread = null;
		//private delegate void UpdateStatusDelegate();
		//private UpdateStatusDelegate updateStatusDelegate = null;
		
		public SchemeRunForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			//this.updateStatusDelegate = new UpdateStatusDelegate(this.listSelection);
			
			listSelection();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
		    base.OnFormClosing(e);
		
		    if (e.CloseReason == CloseReason.WindowsShutDown) return;
		
		    // Confirm user wants to close		 
		    if(isrunning)
		    { 
		    	MessageBox.Show("Please stop the program first.");
		    	e.Cancel = true;
		    }
		    	
		}
		
		
		
		void listSelection()
		{
            string sql = @"select [SchemeID],interID from [Scheme] where InterID not in (select distinct schemeinterid from SchemeHidden where SchemeStatus = 0)";
            DataTable dt = ToolsClass.PullData(sql);
			foreach (DataRow row in dt.Rows) {
				CbScheme.Items.Add(row[0].ToString());
			}
			
		}
		
		void BtRunClick(object sender, EventArgs e)
		{
			
			if(string.IsNullOrEmpty(CbScheme.Text.ToString()))
				return;	
			SCM = CbScheme.Text.ToString();
			
			
			
			DialogResult dialogResult = MessageBox.Show("Sure To run scheme: "+CbScheme.Text.ToString()+"?", "Admin", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.No)
				return;	
			bool isok = SetValues();
			
			Line = ToolsClass.getConfig("Line",false,"","config.xml");
        	string sql = @"select interid from scheme where schemeID = '"+SCM+"'";
		    //SqlDataReader rd = ToolsClass.GetDataReader(sql);
		    
		    DataTable DtCheck = ToolsClass.PullData(sql);
			if( DtCheck!=null && DtCheck.Rows.Count > 0)	
			{
				SchemeInterID = Convert.ToInt32(DtCheck.Rows[0][0].ToString());
				DtCheck.Rows.Clear();
			}
			
			
			ToolsClass.WriteToLog("Program running on Line "+ Line, "Scheme "+ CbScheme.Text.ToString());
			workerThread = null;
			CbScheme.Enabled = false;
			BtRun.Enabled = false;
			isrunning = true;
			
	            timer.Tick += new EventHandler(timer_Tick); // Everytime timer ticks, timer_Tick will be called
	            timer.Interval = (1000) * (Delay);             // Timer will tick evert 10 seconds
	            timer.Enabled = true;                       // Enable the timer
	            timer.Start();  
		}
		
		
		bool SetValues()
		{
			ToolsClass.WriteToLog("InSetValues");
			string sql = @" select [ItemModelID],[ItemSizeID],[ItemMPID],[ItemColorID],[ItemPatternID] from scheme where [SchemeID] = '{0}'";
			sql = string.Format(sql, CbScheme.Text.ToString());
			DataTable DtCheck = ToolsClass.PullData(sql);
			//SqlDataReader rd = ToolsClass.GetDataReader(sql);	
			
			if(DtCheck!=null && DtCheck.Rows.Count > 0)
			{
				sql = @"select itemvalue,ItemClassInterID from [Item] where interid in ({0},{1},{2},{3},{4})";
				sql = string.Format(sql,Convert.ToInt32(DtCheck.Rows[0][0].ToString()),Convert.ToInt32(DtCheck.Rows[0][1].ToString()),Convert.ToInt32(DtCheck.Rows[0][2].ToString()),Convert.ToInt32(DtCheck.Rows[0][3].ToString()),Convert.ToInt32(DtCheck.Rows[0][4].ToString()));
				
				DataTable dt = ToolsClass.PullData(sql);
				foreach(DataRow row in dt.Rows)
				{
					switch(row[1].ToString())
					{
						case "1":
							ModelType = row[0].ToString();
							break;
						case "3":
							CellType = row[0].ToString();
							break;
						case "6":
							CellColor = row[0].ToString();
							break;
						case "7":
							CellSpec = row[0].ToString();
							break;
						case "8":
							Pattern= row[0].ToString();
							break;							
							
					}	
				}	
				DtCheck.Rows.Clear();
			}
			
			return true;
		}

        void timer_Tick(object sender, EventArgs e)
        {
        	if(!isrunning)
        	{
        		timer.Stop();
        		return;
        	}
        	if(workerThread != null)
        	{
        		//ToolsClass.WriteToLog(workerThread.ThreadState.ToString());
        		if(workerThread.ThreadState.ToString() == "Running")
        		{
        			ToolsClass.WriteToLog("Program still Running");
        			return;
        		}
        		workerThread.Abort();    		
        		
        	}
        	
        	this.workerThread = new Thread(new ThreadStart(Insertion));
		    this.workerThread.Start();
        }
        
        
        void Insertion()
        {     	
        	if(!isrunning)
        		return;
			
        	#region readFile
			 string sql ="";
			 string line = "";
			 try {
			 	 line=ToolsClass.ReadFileReverse();	
			 }
			 catch (Exception ex) {
			 	ToolsClass.WriteErrorLog("Reading File Error",ex.Message,"");
			 	line = "";
			 }
			
			 if(string.IsNullOrEmpty(line))
			 {
			 	ToolsClass.WriteToLog("Nothing In The Text File");
			 	//insertion2();
			 	return;
			 }
			 string[] Lines = line.Split('\n');
			 if ( Lines == null || !Lines.Any() )
			 {	
			 	ToolsClass.WriteToLog("Nothing In The Text File");
			 	//insertion2();
			 	return;
			 }
			 
			 
			 int count = 0;
		
			 for( int i= 0; i<Lines.Length;i++)
			 {
			 	Lines[i] = Lines[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
			 	string[] Elements = Lines[i].Split('\t');
			 	
			 	if( Elements.Length != 15 )
			 	{
			 		ToolsClass.WriteErrorLog("SN = ",Elements[0]," Number of Item: "+Elements.Length.ToString());
			 		continue;
			 	}
			 	if( Elements[0].Length != 22 )
			 	{
			 		ToolsClass.WriteErrorLog(Elements[0],"SerialNumber Wrong Format","");
			 		continue;
			 	}
//			 	if( !Elements[0].Contains("316")&&!Elements[0].Contains("C"))
//			 	{
//			 		ToolsClass.WriteErrorLog(Elements[0],"SerialNumber Wrong Format","");
//			 		continue;
//			 	}
			 	if(CheckIfPacked(Elements[0]))
			 	{
					ToolsClass.WriteErrorLog(Elements[0],"SerialNumber Packed","");
			 		//continue;
			 	}

                sql = @"select CsiSN from CsiToFlexSN where FlexSN = '" + Elements[0]+ "' ";
                
                DataTable dtSN = ToolsClass.PullData(sql);
                if(dtSN != null && dtSN.Rows.Count > 0)
                {
                    Elements[0] = dtSN.Rows[0][0].ToString();
                }
                DBModification(Elements);
			 	
				count++;			 	 
			 }
			 if(count > 0)
			 ToolsClass.WritetoFile("&");			 
		          #endregion
		    //Verification      
		    //insertion2();
        }
        
        void insertion2()
        {
        	#region readDB  //			0			1	   2   3   4     5   6    7    8       9       10      11                            12             13   14
			string sql = @"select top 30 SerialNumber,MTemp,VOC,ISC,Pmax,VMax,CMax, FF,CellEff,Rseries,Rshunt, convert(time,TestTime) as H ,TestTime as D,LineKUKA, 1 as FNO from CTMMasterData where Packaging = 0 and LineKUKA = '"+ Line +"'";
			DataTable Dt47 = ToolsClass.PullData(sql,connPLC);
			if(Dt47== null|| Dt47.Rows.Count < 1)
				return;
			
			foreach(DataRow row in Dt47.Rows) {
				
				if(CheckInElecparatest(row[0].ToString()))
				{
					sql = @"update [CTMMasterData] set [Packaging] = 1 where SerialNumber = '"+row[0].ToString()+"'";
					ToolsClass.PutsData(sql,connPLC);
						continue;
				}
				if(CheckIfPacked(row[0].ToString()))
				{
					ToolsClass.WriteErrorLog(row[0].ToString(),"SerialNumber Packed","");
					//continue;
				}
				
				//Thread.Sleep(50);
				bool isNull = false;
				string[] Elements =new string[15];
				double Rshunt = 0;
				try {
				
				for (int i = 0; i<15; i++)
				{
					if ((string.IsNullOrEmpty(row[i].ToString()) || row[i].ToString() == "0" ) && i != 8)
					{
						ToolsClass.WriteErrorLog(Elements[0],"Column "+ i.ToString()+" is empty!","" );
						sql = @"UPDATE [CTMMasterData] set [Packaging] = null where [SerialNumber] = '"+Elements[0]+"'";
						ToolsClass.PutsData(sql,connPLC);
						isNull = true;
						break;
					}			
					switch (i){
						case 7:
							Elements[i] = (Math.Round(Convert.ToDouble(row[i].ToString())/100,3)).ToString();
							break;
						case 8:
							Elements[i] = (!string.IsNullOrEmpty(row[i].ToString()))?(Convert.ToDouble(row[i].ToString())/100).ToString():CalculateCellEff(Convert.ToDouble(Elements[4])).ToString();
							string check = row[i].ToString();
							break;
						case 10:							
							Elements[i] = (Line == "A")?(Convert.ToDouble(row[i].ToString())/10).ToString():row[i].ToString();
							Rshunt = Convert.ToDouble(Elements[i]);
							Rshunt = Math.Round(Rshunt,1);
							Elements[i] = Rshunt.ToString();
							break;
						case 11:
							Elements[i] = row[i].ToString();
							break;
						case 12:
							Elements[i] = Convert.ToDateTime(row[i].ToString()).ToString("dd-MM-yy");
							break;
						default:
							Elements[i] = row[i].ToString();
							break;
					}
				}
				
					
				} 
				catch (Exception e ) 
				{
					ToolsClass.WriteErrorLog("Exception Caught on aissgning value",e.Message,Elements[0]);
				}
				
				if(isNull)
					continue;
				
				 DBModification(Elements);
			 	 sql = @"update [CTMMasterData] set [Packaging] = 1 where SerialNumber = '"+Elements[0]+"'";
			 	 
			 	 if(ToolsClass.PutsData(sql,connPLC) < 1)
			 	 {
			 	 	ToolsClass.WriteErrorLog(Elements[0],"Inserting failed","CTMMasterData");
					continue;
			 	 }
				
			}
       
      
			
			#endregion 
        }
     
		
        private void DBModification(string[] Ele)
        {
        						
				int result = 0;  
	        	//check if tested in ElecParaTest
			 	string sql = @"select * from [ElecParaTest] with (nolock) where Fnumber = '"+ Ele[0] +"'";
	        	DataTable dt = ToolsClass.PullData(sql);
	        	if(dt!= null && dt.Rows.Count > 0)
	        	{
	        		ToolsClass.WriteErrorLog(Ele[0],"Alreayd Exists","ElecParaTest");
	        		sql =@"INSERT INTO ElecParaTest_detail SELECT * FROM ElecParaTest with (nolock) where Fnumber = '"+ Ele[0] +"'";
	        		result = ToolsClass.PutsData(sql);
	        		if (result == 1) {
	        			sql = @" delete from ElecParaTest where Fnumber = '"+ Ele[0] +"'";
	        			ToolsClass.PutsData(sql);
	        		}
	        	dt.Rows.Clear();	
	        	}
	        	
	        	
	        
	        	 //insert into ElecParaTest when no duplicate.	//SN	TEMP	
				 sql = @"insert into [ElecParaTest] values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',getdate(),'{15}','{16}',{17})";
				 sql = string.Format(sql,Ele[0],Ele[1],Ele[2],Ele[3],Ele[4],Ele[5],Ele[6],Ele[7],Ele[8],Ele[9],Ele[10],Ele[11],Ele[12],Ele[13],SchemeInterID,Ele[14],Ele[12].Substring(3,2)+"-"+Ele[12].Substring(0,3)+Ele[12].Substring(6,2)+" "+Ele[11],1);
			 	 ToolsClass.PutsData(sql);
			 	 
			 	 sql = @"select [InterID] from [ElecParaTest] with (nolock) where Fnumber = '"+ Ele[0] +"'";
			 	
			 	 dt = ToolsClass.PullData(sql);
			 	 int interIDelec = 0;
			 	 
			 	 if(dt!=null &&dt.Rows.Count > 0)
			 	 {
			 	 	interIDelec = Convert.ToInt32(dt.Rows[0][0].ToString());
			 	 	dt.Rows.Clear();
			 	 }
	        	
			 	 int interIDProd = 0;
	        	result = 0; 
				//check if record in product
				sql =@"Select InterId from product with (nolock) where SN = '"+ Ele[0] +"'";
				dt = ToolsClass.PullData(sql);
				if(dt!= null && dt.Rows.Count > 0)
	        	{
					interIDProd = Convert.ToInt32(dt.Rows[0][0].ToString());
					dt.Rows.Clear();
					ToolsClass.WriteErrorLog(Ele[0],"Alreayd Exists","product");
					sql =@"INSERT INTO Product_detail SELECT * FROM product where SN = '"+ Ele[0] +"'";
	        		result = ToolsClass.PutsData(sql);
	        		if(result == 1) {
	        			sql = @"insert into [Product] select [SN]
							      ,'{2}'
							      ,'{3}'
							      ,'{4}'
							      ,[CellDegree]
							      ,[CellProvider]
							      ,'{5}'
							      ,[Process]
							      ,[ProcessPass]
							      ,[ModelTypeLabel]
							      ,[Degree]
							      ,[BoxID]
							      ,[Mix]
							      ,GETDATE()
							      ,{0}
							      ,[CTMGrade]
							      ,[ModuleClass]
							      ,[OperatorID]
							      ,[Art_No]
							      ,[Pattern]
							      ,[Flag]
							      ,[StdPower]
							      ,[Pmax]
							      ,[StorageDate]
							      ,[StorageOperator]
							      ,[SAPStorageOperator]
							      ,[SAPStorageDate]
							      ,[PackOperator]
							      ,[PackDate]
							      ,[WorkOrder]
							      ,[ReWorkOrder]
							      ,[UnpackOperator]
							      ,[UnpackDate]
							      ,[CancelStorageDate]
							      ,[CancelStorageOperator]
							      ,[Cust_BoxID]
							      ,[Print_BoxID]
							      ,[ReworkDate]
							      ,[Reworker]
							      ,[LocalFlag]
								  from [Product] where InterId = {1}
								delete from Product where interID = {1} ";
	        			
	        			sql = string.Format(sql,interIDelec,interIDProd,ModelType+"-"+ CellType,CellSpec,CellType,CellColor);
	        			ToolsClass.PutsData(sql);
	        		}
	        	}
				else
				{
					 sql = @"insert into [product] ( [SN],[ModelType],[CellSpec],[CellType],[CellProvider],[CellColor],[Process],[BoxID],[ProcessDateTime],[InterNewTempTableID],[CTMGrade],[ModuleClass],[OperatorID],[Pattern],[Flag],[LocalFlag])
								 			VALUES('{0}','{1}','{2}','{3}','/','{4}','T3','','{5}','{6}','W','A','','{7}','0','0')";
			 	 sql = string.Format(sql,Ele[0],ModelType+"-"+ CellType,CellSpec,CellType,CellColor,Ele[12].Substring(3,2)+"-"+Ele[12].Substring(0,3)+Ele[12].Substring(6,2)+" "+Ele[11], interIDelec,Pattern);
			 	 ToolsClass.PutsData(sql);
				}
				ToolsClass.WriteToLog(Ele[0],"Inserted");
			 	
        }
        private double CalculateCellEff(double Pmax)
        {
        	int cellperModule = (ModelType == "CS6X")?72:60;
        	return Math.Round(Pmax/(0.156*0.156 * 1000* cellperModule),2);
        }
        bool CheckIfPacked(string sn)
        {
        	bool ret = false;
        	string sql = @"select sn from product with (nolock) where sn = '"+sn+"' and process = 'T5'";
        	DataTable dtCheck = ToolsClass.PullData(sql);
        	if(dtCheck != null && dtCheck.Rows.Count > 0)
        	{
        		ret = true;
        		//ToolsClass.SendEmail("Serial Number: "+sn+" has been packed.");
        	}
        	return ret;
        }
        bool CheckInElecparatest(string sn)
        {
        	bool ret = false;
        	string sql = @"select FNumber from ElecParaTest with (nolock) where FNumber = '"+sn+"'";
			DataTable dtCheck = ToolsClass.PullData(sql);
        	if(dtCheck != null && dtCheck.Rows.Count > 0)
       			ret = true;
        	//dtCheck.Rows.Clear();
        	return ret;
        }
		void BtStopClick(object sender, EventArgs e)
		{
			ToolsClass.WriteToLog("Program Stopped");
			if(workerThread!=null)
				workerThread.Abort();
			
			
			CbScheme.Enabled = true;
			BtRun.Enabled = true;
			isrunning = false;	
			this.Close();
			
		}
		void CbSchemeSelectedIndexChanged(object sender, EventArgs e)
		{
			SCM = CbScheme.Text.ToString();
		}
		void SchemeRunFormLoad(object sender, EventArgs e)
		{
			
		}
	}
}
