﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 9:01 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class EditNominalParam
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditNominalParam));
			this.TbCode = new System.Windows.Forms.TextBox();
			this.TbArt = new System.Windows.Forms.TextBox();
			this.TbVoc = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.CbModelType = new System.Windows.Forms.ComboBox();
			this.CbMaxPower = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.CbCellType = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.TbIsc = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.TbImpp = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.TbVmpp = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.TbPmpp = new System.Windows.Forms.TextBox();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.BtCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TbCode
			// 
			this.TbCode.Enabled = false;
			this.TbCode.Location = new System.Drawing.Point(190, 60);
			this.TbCode.Name = "TbCode";
			this.TbCode.Size = new System.Drawing.Size(170, 20);
			this.TbCode.TabIndex = 0;
			// 
			// TbArt
			// 
			this.TbArt.Location = new System.Drawing.Point(190, 150);
			this.TbArt.Name = "TbArt";
			this.TbArt.Size = new System.Drawing.Size(170, 20);
			this.TbArt.TabIndex = 100;
			// 
			// TbVoc
			// 
			this.TbVoc.Location = new System.Drawing.Point(190, 190);
			this.TbVoc.Name = "TbVoc";
			this.TbVoc.Size = new System.Drawing.Size(170, 20);
			this.TbVoc.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(170, 20);
			this.label1.TabIndex = 7;
			this.label1.Text = "Nominal Power Parameter code";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(10, 90);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(170, 20);
			this.label2.TabIndex = 8;
			this.label2.Text = "Nominal Power Parameter";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// CbModelType
			// 
			this.CbModelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbModelType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbModelType.FormattingEnabled = true;
			this.CbModelType.Location = new System.Drawing.Point(190, 90);
			this.CbModelType.Name = "CbModelType";
			this.CbModelType.Size = new System.Drawing.Size(72, 21);
			this.CbModelType.TabIndex = 9;
			this.CbModelType.SelectedIndexChanged += new System.EventHandler(this.CbModelTypeSelectedIndexChanged);
			// 
			// CbMaxPower
			// 
			this.CbMaxPower.BackColor = System.Drawing.SystemColors.Window;
			this.CbMaxPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbMaxPower.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbMaxPower.FormattingEnabled = true;
			this.CbMaxPower.Location = new System.Drawing.Point(289, 91);
			this.CbMaxPower.Name = "CbMaxPower";
			this.CbMaxPower.Size = new System.Drawing.Size(72, 21);
			this.CbMaxPower.TabIndex = 10;
			this.CbMaxPower.SelectedIndexChanged += new System.EventHandler(this.CbMaxPowerSelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(270, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(19, 22);
			this.label3.TabIndex = 11;
			this.label3.Text = "_";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(10, 120);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(170, 20);
			this.label4.TabIndex = 12;
			this.label4.Text = "Cell Type";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// CbCellType
			// 
			this.CbCellType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbCellType.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.CbCellType.FormattingEnabled = true;
			this.CbCellType.Location = new System.Drawing.Point(190, 120);
			this.CbCellType.Name = "CbCellType";
			this.CbCellType.Size = new System.Drawing.Size(72, 21);
			this.CbCellType.TabIndex = 101;
			this.CbCellType.SelectedIndexChanged += new System.EventHandler(this.CbCellTypeSelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(10, 150);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(170, 20);
			this.label5.TabIndex = 14;
			this.label5.Text = "ART-NO";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(10, 190);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(170, 20);
			this.label6.TabIndex = 15;
			this.label6.Text = "Open Circuit Voltage (Voc)";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TbIsc
			// 
			this.TbIsc.Location = new System.Drawing.Point(190, 220);
			this.TbIsc.Name = "TbIsc";
			this.TbIsc.Size = new System.Drawing.Size(170, 20);
			this.TbIsc.TabIndex = 16;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(10, 220);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(170, 20);
			this.label7.TabIndex = 17;
			this.label7.Text = "Short Circuit Current (Isc)";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(10, 280);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(170, 20);
			this.label8.TabIndex = 21;
			this.label8.Text = "Current @ Peak Power (Impp)";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TbImpp
			// 
			this.TbImpp.Location = new System.Drawing.Point(190, 280);
			this.TbImpp.Name = "TbImpp";
			this.TbImpp.Size = new System.Drawing.Size(170, 20);
			this.TbImpp.TabIndex = 20;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(10, 250);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(170, 20);
			this.label9.TabIndex = 19;
			this.label9.Text = "Voltage @ Peak Power(Vmpp)";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TbVmpp
			// 
			this.TbVmpp.Location = new System.Drawing.Point(190, 250);
			this.TbVmpp.Name = "TbVmpp";
			this.TbVmpp.Size = new System.Drawing.Size(170, 20);
			this.TbVmpp.TabIndex = 18;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(10, 310);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(170, 20);
			this.label10.TabIndex = 23;
			this.label10.Text = "Rated Maximum Power (Pmpp)";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// TbPmpp
			// 
			this.TbPmpp.Location = new System.Drawing.Point(190, 310);
			this.TbPmpp.Name = "TbPmpp";
			this.TbPmpp.Size = new System.Drawing.Size(170, 20);
			this.TbPmpp.TabIndex = 22;
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(73, 364);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(90, 22);
			this.BtSubmit.TabIndex = 24;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// BtCancel
			// 
			this.BtCancel.Location = new System.Drawing.Point(236, 364);
			this.BtCancel.Name = "BtCancel";
			this.BtCancel.Size = new System.Drawing.Size(90, 22);
			this.BtCancel.TabIndex = 25;
			this.BtCancel.Text = "Cancel";
			this.BtCancel.UseVisualStyleBackColor = true;
			this.BtCancel.Click += new System.EventHandler(this.BtCancelClick);
			// 
			// EditNominalParam
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(408, 437);
			this.Controls.Add(this.BtCancel);
			this.Controls.Add(this.BtSubmit);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.TbPmpp);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.TbImpp);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.TbVmpp);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.TbIsc);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.CbCellType);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.CbMaxPower);
			this.Controls.Add(this.CbModelType);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.TbVoc);
			this.Controls.Add(this.TbArt);
			this.Controls.Add(this.TbCode);
			this.DoubleBuffered = true;
			this.Name = "EditNominalParam";
			this.Text = "EditNominalParam";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button BtCancel;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.TextBox TbPmpp;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox TbVmpp;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox TbImpp;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox TbIsc;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox CbCellType;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox CbMaxPower;
		private System.Windows.Forms.ComboBox CbModelType;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox TbVoc;
		private System.Windows.Forms.TextBox TbArt;
		private System.Windows.Forms.TextBox TbCode;
	}
}
