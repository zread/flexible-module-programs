﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 11:31 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class DecimalDigitsFrom
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecimalDigitsFrom));
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Pmpp = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Vmpp = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Impp = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Voc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Isc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BtAdd = new System.Windows.Forms.Button();
			this.BtEdit = new System.Windows.Forms.Button();
			this.BtDelete = new System.Windows.Forms.Button();
			this.BtQuit = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToResizeColumns = false;
			this.dataGridView1.AllowUserToResizeRows = false;
			this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Code,
			this.Pmpp,
			this.Vmpp,
			this.Impp,
			this.Voc,
			this.Isc});
			this.dataGridView1.GridColor = System.Drawing.Color.White;
			this.dataGridView1.Location = new System.Drawing.Point(29, 64);
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(269, 334);
			this.dataGridView1.TabIndex = 105;
			// 
			// Code
			// 
			this.Code.HeaderText = "Code";
			this.Code.Name = "Code";
			this.Code.ReadOnly = true;
			// 
			// Pmpp
			// 
			this.Pmpp.HeaderText = "Pmpp";
			this.Pmpp.Name = "Pmpp";
			this.Pmpp.ReadOnly = true;
			// 
			// Vmpp
			// 
			this.Vmpp.HeaderText = "Vmpp";
			this.Vmpp.Name = "Vmpp";
			this.Vmpp.ReadOnly = true;
			// 
			// Impp
			// 
			this.Impp.HeaderText = "Impp";
			this.Impp.Name = "Impp";
			this.Impp.ReadOnly = true;
			// 
			// Voc
			// 
			this.Voc.HeaderText = "Voc";
			this.Voc.Name = "Voc";
			this.Voc.ReadOnly = true;
			// 
			// Isc
			// 
			this.Isc.HeaderText = "Isc";
			this.Isc.Name = "Isc";
			this.Isc.ReadOnly = true;
			// 
			// BtAdd
			// 
			this.BtAdd.Location = new System.Drawing.Point(355, 79);
			this.BtAdd.Name = "BtAdd";
			this.BtAdd.Size = new System.Drawing.Size(82, 25);
			this.BtAdd.TabIndex = 106;
			this.BtAdd.Text = "Add";
			this.BtAdd.UseVisualStyleBackColor = true;
			this.BtAdd.Click += new System.EventHandler(this.BtAddClick);
			// 
			// BtEdit
			// 
			this.BtEdit.Location = new System.Drawing.Point(355, 119);
			this.BtEdit.Name = "BtEdit";
			this.BtEdit.Size = new System.Drawing.Size(82, 25);
			this.BtEdit.TabIndex = 106;
			this.BtEdit.Text = "Edit";
			this.BtEdit.UseVisualStyleBackColor = true;
			this.BtEdit.Click += new System.EventHandler(this.BtEditClick);
			// 
			// BtDelete
			// 
			this.BtDelete.Location = new System.Drawing.Point(355, 170);
			this.BtDelete.Name = "BtDelete";
			this.BtDelete.Size = new System.Drawing.Size(82, 25);
			this.BtDelete.TabIndex = 107;
			this.BtDelete.Text = "Delete";
			this.BtDelete.UseVisualStyleBackColor = true;
			this.BtDelete.Click += new System.EventHandler(this.BtDeleteClick);
			// 
			// BtQuit
			// 
			this.BtQuit.Location = new System.Drawing.Point(355, 373);
			this.BtQuit.Name = "BtQuit";
			this.BtQuit.Size = new System.Drawing.Size(82, 25);
			this.BtQuit.TabIndex = 108;
			this.BtQuit.Text = "Quit";
			this.BtQuit.UseVisualStyleBackColor = true;
			this.BtQuit.Click += new System.EventHandler(this.BtQuitClick);
			// 
			// DecimalDigitsFrom
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(483, 441);
			this.Controls.Add(this.BtQuit);
			this.Controls.Add(this.BtDelete);
			this.Controls.Add(this.BtEdit);
			this.Controls.Add(this.BtAdd);
			this.Controls.Add(this.dataGridView1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "DecimalDigitsFrom";
			this.Text = "DecimalDigitsFrom";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button BtQuit;
		private System.Windows.Forms.Button BtDelete;
		private System.Windows.Forms.Button BtEdit;
		private System.Windows.Forms.Button BtAdd;
		private System.Windows.Forms.DataGridViewTextBoxColumn Isc;
		private System.Windows.Forms.DataGridViewTextBoxColumn Voc;
		private System.Windows.Forms.DataGridViewTextBoxColumn Impp;
		private System.Windows.Forms.DataGridViewTextBoxColumn Vmpp;
		private System.Windows.Forms.DataGridViewTextBoxColumn Pmpp;
		private System.Windows.Forms.DataGridViewTextBoxColumn Code;
		private System.Windows.Forms.DataGridView dataGridView1;
	}
}
