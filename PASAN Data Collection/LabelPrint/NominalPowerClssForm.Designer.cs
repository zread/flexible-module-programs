﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/9/2016
 * Time: 1:57 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class NominalPowerClssForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NominalPowerClssForm));
			this.LBClass = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.BtAdd = new System.Windows.Forms.Button();
			this.BtEdit = new System.Windows.Forms.Button();
			this.BtDelete = new System.Windows.Forms.Button();
			this.BtExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// LBClass
			// 
			this.LBClass.FormattingEnabled = true;
			this.LBClass.Location = new System.Drawing.Point(32, 82);
			this.LBClass.Name = "LBClass";
			this.LBClass.Size = new System.Drawing.Size(216, 251);
			this.LBClass.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(32, 54);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(216, 25);
			this.label1.TabIndex = 1;
			this.label1.Text = "Nominal Power setting List";
			// 
			// BtAdd
			// 
			this.BtAdd.Location = new System.Drawing.Point(275, 97);
			this.BtAdd.Name = "BtAdd";
			this.BtAdd.Size = new System.Drawing.Size(72, 29);
			this.BtAdd.TabIndex = 2;
			this.BtAdd.Text = "Add";
			this.BtAdd.UseVisualStyleBackColor = true;
			this.BtAdd.Click += new System.EventHandler(this.BtAddClick);
			// 
			// BtEdit
			// 
			this.BtEdit.Location = new System.Drawing.Point(275, 151);
			this.BtEdit.Name = "BtEdit";
			this.BtEdit.Size = new System.Drawing.Size(72, 29);
			this.BtEdit.TabIndex = 3;
			this.BtEdit.Text = "Edit";
			this.BtEdit.UseVisualStyleBackColor = true;
			this.BtEdit.Click += new System.EventHandler(this.BtEditClick);
			// 
			// BtDelete
			// 
			this.BtDelete.Location = new System.Drawing.Point(275, 201);
			this.BtDelete.Name = "BtDelete";
			this.BtDelete.Size = new System.Drawing.Size(72, 29);
			this.BtDelete.TabIndex = 4;
			this.BtDelete.Text = "Delete";
			this.BtDelete.UseVisualStyleBackColor = true;
			this.BtDelete.Click += new System.EventHandler(this.BtDeleteClick);
			// 
			// BtExit
			// 
			this.BtExit.Location = new System.Drawing.Point(275, 304);
			this.BtExit.Name = "BtExit";
			this.BtExit.Size = new System.Drawing.Size(72, 29);
			this.BtExit.TabIndex = 5;
			this.BtExit.Text = "Exit";
			this.BtExit.UseVisualStyleBackColor = true;
			this.BtExit.Click += new System.EventHandler(this.BtExitClick);
			// 
			// NominalPowerClssForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(377, 395);
			this.Controls.Add(this.BtExit);
			this.Controls.Add(this.BtDelete);
			this.Controls.Add(this.BtEdit);
			this.Controls.Add(this.BtAdd);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.LBClass);
			this.Name = "NominalPowerClssForm";
			this.Text = "NominalPowerClssForm";
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Button BtExit;
		private System.Windows.Forms.Button BtDelete;
		private System.Windows.Forms.Button BtEdit;
		private System.Windows.Forms.Button BtAdd;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox LBClass;
	}
}
