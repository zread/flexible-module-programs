﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 3/16/2016
 * Time: 11:52 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace LabelPrint
{
	partial class EditDecimalForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditDecimalForm));
			this.label1 = new System.Windows.Forms.Label();
			this.TbCode = new System.Windows.Forms.TextBox();
			this.TbPmpp = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.TbVmpp = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.TbImpp = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.TbVoc = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.TbIsc = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.BtCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(59, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Code";
			// 
			// TbCode
			// 
			this.TbCode.Location = new System.Drawing.Point(120, 60);
			this.TbCode.Name = "TbCode";
			this.TbCode.Size = new System.Drawing.Size(119, 20);
			this.TbCode.TabIndex = 1;
			// 
			// TbPmpp
			// 
			this.TbPmpp.Location = new System.Drawing.Point(120, 90);
			this.TbPmpp.Name = "TbPmpp";
			this.TbPmpp.Size = new System.Drawing.Size(119, 20);
			this.TbPmpp.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 90);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(59, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "Pmpp";
			// 
			// TbVmpp
			// 
			this.TbVmpp.Location = new System.Drawing.Point(120, 120);
			this.TbVmpp.Name = "TbVmpp";
			this.TbVmpp.Size = new System.Drawing.Size(119, 20);
			this.TbVmpp.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(40, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(59, 23);
			this.label3.TabIndex = 4;
			this.label3.Text = "Vmpp";
			// 
			// TbImpp
			// 
			this.TbImpp.Location = new System.Drawing.Point(120, 150);
			this.TbImpp.Name = "TbImpp";
			this.TbImpp.Size = new System.Drawing.Size(119, 20);
			this.TbImpp.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(40, 150);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(59, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "Impp";
			// 
			// TbVoc
			// 
			this.TbVoc.Location = new System.Drawing.Point(120, 180);
			this.TbVoc.Name = "TbVoc";
			this.TbVoc.Size = new System.Drawing.Size(119, 20);
			this.TbVoc.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(40, 180);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(59, 23);
			this.label5.TabIndex = 8;
			this.label5.Text = "Voc";
			// 
			// TbIsc
			// 
			this.TbIsc.Location = new System.Drawing.Point(120, 210);
			this.TbIsc.Name = "TbIsc";
			this.TbIsc.Size = new System.Drawing.Size(119, 20);
			this.TbIsc.TabIndex = 11;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(40, 210);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(59, 23);
			this.label6.TabIndex = 10;
			this.label6.Text = "Isc";
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(39, 247);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(89, 26);
			this.BtSubmit.TabIndex = 12;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// BtCancel
			// 
			this.BtCancel.Location = new System.Drawing.Point(162, 247);
			this.BtCancel.Name = "BtCancel";
			this.BtCancel.Size = new System.Drawing.Size(89, 26);
			this.BtCancel.TabIndex = 13;
			this.BtCancel.Text = "Cancel";
			this.BtCancel.UseVisualStyleBackColor = true;
			this.BtCancel.Click += new System.EventHandler(this.BtCancelClick);
			// 
			// EditDecimalForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(284, 297);
			this.Controls.Add(this.BtCancel);
			this.Controls.Add(this.BtSubmit);
			this.Controls.Add(this.TbIsc);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.TbVoc);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.TbImpp);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.TbVmpp);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.TbPmpp);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.TbCode);
			this.Controls.Add(this.label1);
			this.Name = "EditDecimalForm";
			this.Text = "EditDecimalForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Button BtCancel;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox TbIsc;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox TbVoc;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TbImpp;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox TbVmpp;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox TbPmpp;
		private System.Windows.Forms.TextBox TbCode;
		private System.Windows.Forms.Label label1;
	}
}
