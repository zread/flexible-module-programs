﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 2/2/2016
 * Time: 4:50 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace LabelPrint
{
	/// <summary>
	/// Description of MasterDataForm.
	/// </summary>
	public partial class MasterDataForm : Form
	{
		public MasterDataForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			ListItemClass();		
		}
		void ListItemClass()
		{
			string sql = @"select ItemClassId,ItemClassNM from [LabelPrintDB].[dbo].[ItemClass]";
				DataTable dt = ToolsClass.PullData(sql);
				{
						foreach (DataRow row in dt.Rows) {				
						string temp = row[0]+ " " + row[1];
						Cb_ItemCLass.Items.AddRange(new object[] {temp});			
				}
				}
		}
		
		void Cb_ItemCLassSelectedIndexChanged(object sender, EventArgs e)
		{
			AutoPopulate();	
			
		}
		void AutoPopulate()
		{
			Cb_Item.Items.Clear();
			string sql = @"select ItemClassInterID,ItemID,ItemValue from [LabelPrintDB].[dbo].[Item] where ItemClassInterID = '{0}' order by itemid";
			sql = string.Format(sql, Cb_ItemCLass.SelectedIndex + 1 );
			
			if(Cb_ItemCLass.SelectedIndex != -1)
			{
				DataTable dt = ToolsClass.PullData(sql);
				{
					
					foreach (DataRow row in dt.Rows) {
						string temp = row[1] + " " + row[2];
						Cb_Item.Items.AddRange(new object[] {temp});
					}
					
				}
			}
		}
		
		
		

		
		void AddToolStripMenuItemClick(object sender, EventArgs e)
		{
		
				//AddMasterDataForm frm = new AddMasterDataForm();
				
				AddMasterDataForm frm = new AddMasterDataForm(this.Cb_ItemCLass.SelectedIndex + 1);
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
		}
		
		
		void EditToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(Cb_Item.SelectedIndex == -1)
			{
				MessageBox.Show("Please select item to be modified!");
				return;
			}
				EditMasterDataForm frm = new EditMasterDataForm(this.Cb_ItemCLass.SelectedIndex + 1,this.Cb_Item.SelectedIndex + 1);
				//EditMasterDataForm frm = new EditMasterDataForm(this.Cb_ItemCLass.SelectedIndex + 1,Cb_Item.SelectedItem.ToString().Substring(0,1));
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();
		}
		
		void DeleteToolStripMenuItemClick(object sender, EventArgs e)
		{
			
			if(Cb_Item.SelectedIndex == -1)
			{
				MessageBox.Show("Please select item to be modified!");
				return;
			}
			string sql = @"delete from item where ItemClassInterID = {0} and ItemId = {1}";
			sql = string.Format(sql,this.Cb_ItemCLass.SelectedIndex + 1,this.Cb_Item.SelectedIndex + 1);
			ToolsClass.PutsData(sql);
			
			
		}
	}
}
