﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 5/3/2012
 * Time: 10:16 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of whitelabel.
	/// </summary>
	public class Whitelabel
	{
		public Whitelabel()
		{
		}
		
		public string model = "";
		public string sn = "";
		public string size = "";
		public string color = "";
		public string type = "";
	}
}
