﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 5/3/2012
 * Time: 10:17 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using LabelManager2;
using System.Collections;
using System.Diagnostics;
using 条码打印测试;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of PrintWhiteLabel.
	/// </summary>
	public class PrintWhiteLabel
	{
		public PrintWhiteLabel()
		{
		}
		
		public static string print(Whitelabel lb, string labFilePath)
		{
			LabelManager2.ApplicationClass lbl = new ApplicationClass();
        	try
        	{
				lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
				Document doc = lbl.ActiveDocument;
				int VarCnt = lbl.ActiveDocument.Variables.Count;

				for (int i = 1; i <= VarCnt; i++)
				{
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "model")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.model;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "mp")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.type;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "color")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.color;
						continue;
					}
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "size")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.size;
						continue;
					}					
					
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "sn")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = lb.sn;
						continue;
					}											
				}
				doc.PrintLabel(1, 1, 1, 1); //打印数量
				doc.PrintDocument(0);
				lbl.Quit();  //退出
				Process[] killbyname = Process.GetProcessesByName("lppa");
				for (int i = 0; i < killbyname.Length; i++)
				{
					killbyname[i].Kill();
				}
        	}
       		catch (Exception ex)
            {
                return ex.Message;
            }
       		return "";
		}
		
			public static bool print3(Whitelabel lb, string ZPL, string printer)
		{
			ZPL = ZPL.Replace("^FDCS6P-255P^FS","^FD"+lb.model+"^FS");
			ZPL = ZPL.Replace("^FDC915231011111^FS","^FD"+lb.sn+"^FS");
			ZPL = ZPL.Replace("^FDSerial No. C915231011111^FS","^FDSerial No. "+lb.sn+"^FS");		
			ZPL = ZPL.Replace("^FDMM/P^FS","^FD"+lb.type+"^FS");
			ZPL = ZPL.Replace("^FDCColor^FS","^FD"+lb.color+"^FS");
			ZPL = ZPL.Replace("^FDSSize(mm)^FS","^FD"+lb.size+"^FS");
		
			return RawPrinterHelper.SendStringToPrinter(printer, ZPL, "");
		}
	}
}
