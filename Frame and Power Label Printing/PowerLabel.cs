﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 3/30/2012
 * Time: 10:27 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace QualityLabelPrinting
{
	/// <summary>
	/// Description of Label.
	/// </summary>
	public class PowerLabel
	{
		public PowerLabel()
		{
			
		}
		
		public string model = "";
		public string pmpp = "";
		public string vmpp = "";
		public string impp = "";
		public string voc = "";
		public string isc = "";
		public string sn = "";
		public string moduleclass = "";
		public string voltage = "";
	}
}
