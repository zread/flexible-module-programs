﻿namespace CSICPR
{
    partial class FormGlass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.line = new System.Windows.Forms.Label();
        	this.lineicon = new System.Windows.Forms.Label();
        	this.MOLabel = new System.Windows.Forms.Label();
        	this.MO = new System.Windows.Forms.ComboBox();
        	this.Material = new System.Windows.Forms.Label();
        	this.Mtrlcode = new System.Windows.Forms.Label();
        	this.Materialcode = new System.Windows.Forms.TextBox();
        	this.Batch = new System.Windows.Forms.Label();
        	this.Batchnum = new System.Windows.Forms.TextBox();
        	this.SU = new System.Windows.Forms.Label();
        	this.SUNum = new System.Windows.Forms.TextBox();
        	this.quantity = new System.Windows.Forms.Label();
        	this.Qty = new System.Windows.Forms.TextBox();
        	this.Stockin = new System.Windows.Forms.Button();
        	this.Modify = new System.Windows.Forms.Button();
        	this.Stockout = new System.Windows.Forms.Button();
        	this.Review = new System.Windows.Forms.Button();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.pcs = new System.Windows.Forms.Label();
        	this.ProdWarehouse = new System.Windows.Forms.Label();
        	this.Recieved = new System.Windows.Forms.Label();
        	this.Mosize = new System.Windows.Forms.Label();
        	this.WHR = new System.Windows.Forms.Button();
        	this.QR = new System.Windows.Forms.Button();
        	this.label1 = new System.Windows.Forms.Label();
        	this.CbShift = new System.Windows.Forms.ComboBox();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// line
        	// 
        	this.line.AutoSize = true;
        	this.line.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.line.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.line.Location = new System.Drawing.Point(589, 22);
        	this.line.Name = "line";
        	this.line.Size = new System.Drawing.Size(41, 39);
        	this.line.TabIndex = 4;
        	this.line.Text = "X";
        	// 
        	// lineicon
        	// 
        	this.lineicon.AutoSize = true;
        	this.lineicon.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.lineicon.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.lineicon.Location = new System.Drawing.Point(471, 22);
        	this.lineicon.Name = "lineicon";
        	this.lineicon.Size = new System.Drawing.Size(107, 39);
        	this.lineicon.TabIndex = 3;
        	this.lineicon.Text = "Line :";
        	this.lineicon.Click += new System.EventHandler(this.label1_Click);
        	// 
        	// MOLabel
        	// 
        	this.MOLabel.AutoSize = true;
        	this.MOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.MOLabel.ForeColor = System.Drawing.SystemColors.Control;
        	this.MOLabel.Location = new System.Drawing.Point(73, 95);
        	this.MOLabel.Name = "MOLabel";
        	this.MOLabel.Size = new System.Drawing.Size(75, 31);
        	this.MOLabel.TabIndex = 5;
        	this.MOLabel.Text = "MO#";
        	// 
        	// MO
        	// 
        	this.MO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.MO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.MO.FormattingEnabled = true;
        	this.MO.Location = new System.Drawing.Point(152, 95);
        	this.MO.Name = "MO";
        	this.MO.Size = new System.Drawing.Size(202, 33);
        	this.MO.TabIndex = 1;
        	// 
        	// Material
        	// 
        	this.Material.AutoSize = true;
        	this.Material.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Material.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.Material.Location = new System.Drawing.Point(116, 22);
        	this.Material.Name = "Material";
        	this.Material.Size = new System.Drawing.Size(149, 39);
        	this.Material.TabIndex = 4;
        	this.Material.Text = "Material";
        	// 
        	// Mtrlcode
        	// 
        	this.Mtrlcode.AutoSize = true;
        	this.Mtrlcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Mtrlcode.ForeColor = System.Drawing.SystemColors.Control;
        	this.Mtrlcode.Location = new System.Drawing.Point(8, 159);
        	this.Mtrlcode.Name = "Mtrlcode";
        	this.Mtrlcode.Size = new System.Drawing.Size(140, 31);
        	this.Mtrlcode.TabIndex = 5;
        	this.Mtrlcode.Text = "Mtrl Code";
        	this.Mtrlcode.Click += new System.EventHandler(this.label1_Click_1);
        	// 
        	// Materialcode
        	// 
        	this.Materialcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
        	this.Materialcode.Location = new System.Drawing.Point(153, 159);
        	this.Materialcode.MaxLength = 8;
        	this.Materialcode.Name = "Materialcode";
        	this.Materialcode.Size = new System.Drawing.Size(202, 31);
        	this.Materialcode.TabIndex = 2;
        	this.Materialcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Materialcode_keyDown);
        	// 
        	// Batch
        	// 
        	this.Batch.AutoSize = true;
        	this.Batch.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Batch.ForeColor = System.Drawing.SystemColors.Control;
        	this.Batch.Location = new System.Drawing.Point(43, 221);
        	this.Batch.Name = "Batch";
        	this.Batch.Size = new System.Drawing.Size(105, 31);
        	this.Batch.TabIndex = 5;
        	this.Batch.Text = "Batch#";
        	this.Batch.Click += new System.EventHandler(this.label1_Click_1);
        	// 
        	// Batchnum
        	// 
        	this.Batchnum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
        	this.Batchnum.Location = new System.Drawing.Point(153, 221);
        	this.Batchnum.MaxLength = 20;
        	this.Batchnum.Name = "Batchnum";
        	this.Batchnum.Size = new System.Drawing.Size(202, 31);
        	this.Batchnum.TabIndex = 3;
        	this.Batchnum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Batchnum_keyDown);
        	// 
        	// SU
        	// 
        	this.SU.AutoSize = true;
        	this.SU.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.SU.ForeColor = System.Drawing.SystemColors.Control;
        	this.SU.Location = new System.Drawing.Point(78, 283);
        	this.SU.Name = "SU";
        	this.SU.Size = new System.Drawing.Size(70, 31);
        	this.SU.TabIndex = 5;
        	this.SU.Text = "SU#";
        	this.SU.Click += new System.EventHandler(this.label1_Click_1);
        	// 
        	// SUNum
        	// 
        	this.SUNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
        	this.SUNum.Location = new System.Drawing.Point(153, 283);
        	this.SUNum.MaxLength = 40;
        	this.SUNum.Name = "SUNum";
        	this.SUNum.Size = new System.Drawing.Size(202, 31);
        	this.SUNum.TabIndex = 4;
        	this.SUNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.sunum_keyDown);
        	// 
        	// quantity
        	// 
        	this.quantity.AutoSize = true;
        	this.quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.quantity.ForeColor = System.Drawing.SystemColors.Control;
        	this.quantity.Location = new System.Drawing.Point(79, 345);
        	this.quantity.Name = "quantity";
        	this.quantity.Size = new System.Drawing.Size(69, 31);
        	this.quantity.TabIndex = 5;
        	this.quantity.Text = "Qty.";
        	this.quantity.Click += new System.EventHandler(this.label1_Click_1);
        	// 
        	// Qty
        	// 
        	this.Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
        	this.Qty.Location = new System.Drawing.Point(153, 345);
        	this.Qty.Name = "Qty";
        	this.Qty.Size = new System.Drawing.Size(202, 31);
        	this.Qty.TabIndex = 5;
        	// 
        	// Stockin
        	// 
        	this.Stockin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Stockin.Location = new System.Drawing.Point(79, 392);
        	this.Stockin.Name = "Stockin";
        	this.Stockin.Size = new System.Drawing.Size(118, 32);
        	this.Stockin.TabIndex = 8;
        	this.Stockin.Text = "Stock In";
        	this.Stockin.UseVisualStyleBackColor = true;
        	this.Stockin.Click += new System.EventHandler(this.Stockin_Click);
        	// 
        	// Modify
        	// 
        	this.Modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Modify.Location = new System.Drawing.Point(478, 392);
        	this.Modify.Name = "Modify";
        	this.Modify.Size = new System.Drawing.Size(118, 32);
        	this.Modify.TabIndex = 8;
        	this.Modify.Text = "Modify";
        	this.Modify.UseVisualStyleBackColor = true;
        	this.Modify.Click += new System.EventHandler(this.Modify_Click);
        	// 
        	// Stockout
        	// 
        	this.Stockout.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Stockout.Location = new System.Drawing.Point(289, 392);
        	this.Stockout.Name = "Stockout";
        	this.Stockout.Size = new System.Drawing.Size(118, 32);
        	this.Stockout.TabIndex = 8;
        	this.Stockout.Text = "Stock Out";
        	this.Stockout.UseVisualStyleBackColor = true;
        	this.Stockout.Click += new System.EventHandler(this.Stockout_Click);
        	// 
        	// Review
        	// 
        	this.Review.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Review.Location = new System.Drawing.Point(478, 97);
        	this.Review.Name = "Review";
        	this.Review.Size = new System.Drawing.Size(118, 32);
        	this.Review.TabIndex = 8;
        	this.Review.Text = "Review";
        	this.Review.UseVisualStyleBackColor = true;
        	this.Review.Click += new System.EventHandler(this.Review_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Location = new System.Drawing.Point(478, 135);
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.Size = new System.Drawing.Size(1087, 251);
        	this.dataGridView1.TabIndex = 9;
        	// 
        	// pcs
        	// 
        	this.pcs.AutoSize = true;
        	this.pcs.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.pcs.ForeColor = System.Drawing.SystemColors.Control;
        	this.pcs.Location = new System.Drawing.Point(358, 342);
        	this.pcs.Name = "pcs";
        	this.pcs.Size = new System.Drawing.Size(60, 31);
        	this.pcs.TabIndex = 5;
        	this.pcs.Text = "pcs";
        	this.pcs.Click += new System.EventHandler(this.label1_Click_1);
        	// 
        	// ProdWarehouse
        	// 
        	this.ProdWarehouse.AutoSize = true;
        	this.ProdWarehouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.ProdWarehouse.ForeColor = System.Drawing.SystemColors.HighlightText;
        	this.ProdWarehouse.Location = new System.Drawing.Point(80, 534);
        	this.ProdWarehouse.Name = "ProdWarehouse";
        	this.ProdWarehouse.Size = new System.Drawing.Size(53, 29);
        	this.ProdWarehouse.TabIndex = 25;
        	this.ProdWarehouse.Text = "PW";
        	this.ProdWarehouse.Visible = false;
        	// 
        	// Recieved
        	// 
        	this.Recieved.AutoSize = true;
        	this.Recieved.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Recieved.ForeColor = System.Drawing.SystemColors.HighlightText;
        	this.Recieved.Location = new System.Drawing.Point(80, 484);
        	this.Recieved.Name = "Recieved";
        	this.Recieved.Size = new System.Drawing.Size(59, 29);
        	this.Recieved.TabIndex = 26;
        	this.Recieved.Text = "Rec";
        	this.Recieved.Visible = false;
        	// 
        	// Mosize
        	// 
        	this.Mosize.AutoSize = true;
        	this.Mosize.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Mosize.ForeColor = System.Drawing.SystemColors.HighlightText;
        	this.Mosize.Location = new System.Drawing.Point(80, 434);
        	this.Mosize.Name = "Mosize";
        	this.Mosize.Size = new System.Drawing.Size(49, 29);
        	this.Mosize.TabIndex = 27;
        	this.Mosize.Text = "Mo";
        	this.Mosize.Visible = false;
        	// 
        	// WHR
        	// 
        	this.WHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.WHR.Location = new System.Drawing.Point(910, 392);
        	this.WHR.Name = "WHR";
        	this.WHR.Size = new System.Drawing.Size(164, 32);
        	this.WHR.TabIndex = 50;
        	this.WHR.Text = "WH Return";
        	this.WHR.UseVisualStyleBackColor = true;
        	this.WHR.Click += new System.EventHandler(this.WHRClick);
        	// 
        	// QR
        	// 
        	this.QR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.QR.Location = new System.Drawing.Point(723, 392);
        	this.QR.Name = "QR";
        	this.QR.Size = new System.Drawing.Size(164, 32);
        	this.QR.TabIndex = 49;
        	this.QR.Text = "Quality Reject";
        	this.QR.UseVisualStyleBackColor = true;
        	this.QR.Click += new System.EventHandler(this.QRClick);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.label1.ForeColor = System.Drawing.SystemColors.Control;
        	this.label1.Location = new System.Drawing.Point(667, 95);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(74, 31);
        	this.label1.TabIndex = 52;
        	this.label1.Text = "Shift";
        	// 
        	// CbShift
        	// 
        	this.CbShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.CbShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.CbShift.FormattingEnabled = true;
        	this.CbShift.Items.AddRange(new object[] {
			"D1",
			"D2",
			"N1",
			"N2"});
        	this.CbShift.Location = new System.Drawing.Point(747, 96);
        	this.CbShift.Name = "CbShift";
        	this.CbShift.Size = new System.Drawing.Size(202, 33);
        	this.CbShift.TabIndex = 53;
        	// 
        	// FormGlass
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.SystemColors.ActiveBorder;
        	this.ClientSize = new System.Drawing.Size(1475, 570);
        	this.Controls.Add(this.CbShift);
        	this.Controls.Add(this.label1);
        	this.Controls.Add(this.WHR);
        	this.Controls.Add(this.QR);
        	this.Controls.Add(this.ProdWarehouse);
        	this.Controls.Add(this.Recieved);
        	this.Controls.Add(this.Mosize);
        	this.Controls.Add(this.dataGridView1);
        	this.Controls.Add(this.Review);
        	this.Controls.Add(this.Stockout);
        	this.Controls.Add(this.Modify);
        	this.Controls.Add(this.Stockin);
        	this.Controls.Add(this.Qty);
        	this.Controls.Add(this.pcs);
        	this.Controls.Add(this.quantity);
        	this.Controls.Add(this.SUNum);
        	this.Controls.Add(this.SU);
        	this.Controls.Add(this.Batchnum);
        	this.Controls.Add(this.Batch);
        	this.Controls.Add(this.Materialcode);
        	this.Controls.Add(this.Mtrlcode);
        	this.Controls.Add(this.MOLabel);
        	this.Controls.Add(this.MO);
        	this.Controls.Add(this.Material);
        	this.Controls.Add(this.line);
        	this.Controls.Add(this.lineicon);
        	this.Name = "FormGlass";
        	this.Text = "FormGlass";
        	this.Load += new System.EventHandler(this.FormGlass_Load);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Label lineicon;
        private System.Windows.Forms.Label MOLabel;
        private System.Windows.Forms.ComboBox MO;
        private System.Windows.Forms.Label Material;
        private System.Windows.Forms.Label Mtrlcode;
        private System.Windows.Forms.TextBox Materialcode;
        private System.Windows.Forms.Label Batch;
        private System.Windows.Forms.TextBox Batchnum;
        private System.Windows.Forms.Label SU;
        private System.Windows.Forms.TextBox SUNum;
        private System.Windows.Forms.Label quantity;
        private System.Windows.Forms.TextBox Qty;
        private System.Windows.Forms.Button Stockin;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.Button Stockout;
        private System.Windows.Forms.Button Review;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label pcs;
        private System.Windows.Forms.Label ProdWarehouse;
        private System.Windows.Forms.Label Recieved;
        private System.Windows.Forms.Label Mosize;
        private System.Windows.Forms.Button WHR;
        private System.Windows.Forms.Button QR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CbShift;
    }
}