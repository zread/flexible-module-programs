﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormStorageSN : Form
    {
        /// <summary>
        /// 记录验证sap入库成功的托号
        /// </summary>
        //private List<string> _ListCartonSucess = new List<string>();

        private static List<LanguageItemModel> LanMessList;//定义语言集
        public FormStorageSN()
        {
            InitializeComponent();
        }

        private void SNStorage_Load(object sender, EventArgs e)
        {
            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, "普通生产工单");
            this.ddlWoType.Items.Insert(2, "返工生产工单");
            this.ddlWoType.SelectedIndex = 0;

            //提示信息
            this.lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            //界面Grid列头禁止排序
            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            //工单状态
            this.ddlWostatus.Items.Insert(0, "");
            this.ddlWostatus.Items.Insert(1, "完工");
            this.ddlWostatus.Items.Insert(2, "强制结单");
            this.ddlWostatus.SelectedIndex = 0;

            //电池分档
            this.ddlByIm.Items.Insert(0, "NA");
            //this.ddlByIm.Items.Insert(1, "是");
            //this.ddlByIm.Items.Insert(2, "否");
            this.ddlByIm.SelectedIndex = 0;
            this.ddlByIm.Enabled = false;

            //网板
            this.ddlCellNetBoard.Items.Insert(0, "");
            this.ddlCellNetBoard.Items.Insert(1, "C0");
            this.ddlCellNetBoard.Items.Insert(2, "C1");
            this.ddlCellNetBoard.Items.Insert(3, "C2");
            this.ddlCellNetBoard.Items.Insert(4, "C3");
            this.ddlCellNetBoard.Items.Insert(5, "C4");
            this.ddlCellNetBoard.Items.Insert(6, "C5");
            this.ddlCellNetBoard.SelectedIndex = 0;

            //玻璃厚度
            this.ddlGlassLength.Items.Insert(0, "");
            this.ddlGlassLength.Items.Insert(1, "3.2mm");
            this.ddlGlassLength.Items.Insert(2, "4mm");
            this.ddlGlassLength.Items.Insert(3, "Other");
            this.ddlGlassLength.SelectedIndex = 0;

            //包装方式
            this.ddlPackingPattern.Items.Insert(0, "");
            this.ddlPackingPattern.Items.Insert(1, "横包装");
            this.ddlPackingPattern.Items.Insert(2, "竖包装");
            this.ddlPackingPattern.Items.Insert(3, "双件装");
            this.ddlPackingPattern.Items.Insert(4, "单件装");
            this.ddlPackingPattern.Items.Insert(5, "其它");
            this.ddlPackingPattern.SelectedIndex = 0;

            //撤销入库
            this.ddlCancelStorageFlag.Items.Insert(0, "");
            this.ddlCancelStorageFlag.Items.Insert(1, "正常");
            this.ddlCancelStorageFlag.Items.Insert(2, "撤销");
            this.ddlCancelStorageFlag.SelectedIndex = 0;

            //是否拼托
            this.ddlIsOnlyPacking.Items.Insert(0, "");
            this.ddlIsOnlyPacking.Items.Insert(1, "是");
            this.ddlIsOnlyPacking.Items.Insert(2, "否");
            this.ddlIsOnlyPacking.SelectedIndex = 0;

            //界面Gird列头添加checkbox按钮
            HeadCheckBox.datagridviewCheckboxHeaderCell ch = new HeadCheckBox.datagridviewCheckboxHeaderCell();
            ch.OnCheckBoxClicked += new HeadCheckBox.datagridviewCheckboxHeaderCell.HeaderEventHander(ch_OnCheckBoxClicked);
            DataGridViewCheckBoxColumn checkboxCol = this.dataGridView1.Columns[0] as DataGridViewCheckBoxColumn;
            checkboxCol.HeaderCell = ch;
            checkboxCol.HeaderCell.Value = string.Empty;

            this.txtCarton.Focus();

            this.checkBox2.Checked = false;
            this.txtWO1.Enabled = false;
            this.pictureBox2.Enabled = false;
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlWoType);
            LanguageHelper.GetCombomBox(this, this.ddlWostatus);
            LanguageHelper.GetCombomBox(this, this.ddlByIm);
            LanguageHelper.GetCombomBox(this, this.ddlPackingPattern);
            LanguageHelper.GetCombomBox(this, this.ddlCancelStorageFlag);
            LanguageHelper.GetCombomBox(this, this.ddlIsOnlyPacking);
            # endregion

        }

        #region 公有变量
        private static FormStorageSN theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormStorageSN();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        /// <summary>
        /// 工单类型
        /// </summary>
        private string WoType = "";
        /// <summary>
        /// 工单状态
        /// </summary>
        private string WoStatus = "";
        /// <summary>
        /// 记录选中的记录
        /// </summary>
        private Dictionary<int, string> dgvIndex = new Dictionary<int, string>();
        /// <summary>
        /// 设置：记录选中的组件号
        /// </summary>
        private List<string> SNArrayS = new List<string>();
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        /// <summary>
        /// 电流分档
        /// </summary>
        private string ByIm_flag = "";
        /// <summary>
        /// 撤销入库标识
        /// </summary>
        private string _CancelStorageFlag = "";
        /// <summary>
        /// 是否拼托
        /// </summary>
        private string _IsOnlyPacking = "";
        /// <summary>
        /// 包装方式
        /// </summary>
        private string PackingPatternValue = "";
        #endregion

        #region 私有方法
        /// <summary>
        /// 检查是否为空
        /// </summary>
        /// <returns></returns>
        private bool InputDataIsNotNull()
        {
            #region
            if (this.ddlByIm.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电流分档不能为空", "ABNORMAL", lstView);
                this.ddlByIm.Focus();
                return false;
            }

            if (this.ddlPackingPattern.Text.Trim().Equals(""))
            {
                ToolsClass.Log("包装方式不能为空", "ABNORMAL", lstView);
                this.ddlPackingPattern.Focus();
                return false;
            }

            #region 电池片的输入格式
            if (!string.IsNullOrEmpty(this.ddlCellTransfer.Text.Trim()))
            {
                int aa = this.ddlCellTransfer.Text.Trim().ToString().Length;
                if (this.ddlCellTransfer.Text.Trim().ToString().Length != 6)
                {
                    ToolsClass.Log("电池片转换效率格式不对! 正确格式例如15.171", "ABNORMAL", lstView);
                    this.ddlCellTransfer.Focus();
                    return false;
                }
                else
                {
                    string CellTransfer = this.ddlCellTransfer.Text.Trim().ToString();
                    string flag = "Y";
                    foreach (char c in CellTransfer)
                    {
                        if ((!char.IsNumber(c)))
                        {
                            char a = '.';
                            if (c.Equals(a))
                                continue;
                            else
                            {
                                flag = "N";
                                break;
                            }
                        }
                    }
                    if (flag.Equals("N"))
                    {
                        ToolsClass.Log("电池片转换效率格式不对! 正确格式例如15.171", "ABNORMAL", lstView);
                        this.ddlCellTransfer.Focus();
                        return false;
                    }
                }
            }
            else
            {
                //ToolsClass.Log("电池片转换效率不能为空", "ABNORMAL", lstView);
                //this.ddlCellTransfer.Focus();
                //return false;
            }
            #endregion

            if (this.ddlCellBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电池片批次不能为空", "ABNORMAL", lstView);
                this.ddlCellBatch.Focus();
                return false;
            }

            if (this.ddltxtCell.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电池片不能为空", "ABNORMAL", lstView);
                this.ddlCellBatch.Focus();
                return false;
            }

            if (this.ddlGlassBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("玻璃批次不能为空", "ABNORMAL", lstView);
                this.ddlGlassBatch.Focus();
                return false;
            }

            if (this.ddltxtGlassCode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("玻璃不能为空", "ABNORMAL", lstView);
                this.ddlGlassBatch.Focus();
                return false;
            }
            if (this.ddlEVABatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("EVA 批次不能为空", "ABNORMAL", lstView);
                this.ddlEVABatch.Focus();
                return false;
            }

            if (this.ddltxtEVACode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("EVA 物料不能为空", "ABNORMAL", lstView);
                this.ddlEVABatch.Focus();
                return false;
            }

            if (this.ddlTPTBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("背板批次不能为空", "ABNORMAL", lstView);
                this.ddlTPTBatch.Focus();
                return false;
            }

            if (this.ddltxtTPTCode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("背板物料不能为空", "ABNORMAL", lstView);
                this.ddlTPTBatch.Focus();
                return false;
            }

            if (this.ddlConBoxBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("接线盒批次不能为空", "ABNORMAL", lstView);
                this.ddlConBoxBatch.Focus();
                return false;
            }

            if (this.ddltxtConBoxCod.Text.Trim().Equals(""))
            {
                ToolsClass.Log("接线盒物料不能为空", "ABNORMAL", lstView);
                this.ddlConBoxBatch.Focus();
                return false;
            }
            if (!FormCover.WOTypeCode.Trim().ToUpper().Equals("ZP09"))
            {
                if (this.ddlShortAIFrameBatch.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("短边框批次不能为空", "ABNORMAL", lstView);
                    this.ddlShortAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddltxtShortAIFrameCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("短边框物料不能为空", "ABNORMAL", lstView);
                    this.ddlShortAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddlAIFrameBatch.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("长边框批次不能为空", "ABNORMAL", lstView);
                    this.ddlAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddltxtAIFrameCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("长边框物料不能为空", "ABNORMAL", lstView);
                    this.ddlAIFrameBatch.Focus();
                    return false;
                }
            }
            return true;
            #endregion
        }
        /// <summary>
        /// 工单：检查输入的内容是否合法
        /// </summary>
        private bool CheckInput()
        {
            if (WoType.Equals("ZP11")||WoType.Equals("ZPA9"))
            {
                if (!CheckNewSnIsNull(SNArrayS))
                    return false;
            }
            else
            {
                if (!InputDataIsNotNull())
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 工单：初始化界面
        /// </summary>
        private void ClearData(bool flag)
        {
            this.txtWo.Clear();
            this.txtFactory.Clear();
            this.txtMitemCode.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();

            this.ddltxtGlassCode.Items.Clear();
            this.ddltxtGlassCode.Text = "";
            this.ddltxtGlassCode.SelectedIndex = -1;

            this.ddltxtEVACode.Items.Clear();
            this.ddltxtEVACode.Text = "";
            this.ddltxtEVACode.SelectedIndex = -1;


            this.ddltxtTPTCode.Items.Clear();
            this.ddltxtTPTCode.Text = "";
            this.ddltxtTPTCode.SelectedIndex = -1;

            this.ddltxtConBoxCod.Items.Clear();
            this.ddltxtConBoxCod.Text = "";
            this.ddltxtConBoxCod.SelectedIndex = -1;

            this.ddltxtAIFrameCode.Items.Clear();
            this.ddltxtAIFrameCode.Text = "";
            this.ddltxtAIFrameCode.SelectedIndex = -1;

            this.ddltxtCell.Items.Clear();
            this.ddltxtCell.Text = "";
            this.ddltxtCell.SelectedIndex = -1;

            this.ddltxtShortAIFrameCode.Items.Clear();
            this.ddltxtShortAIFrameCode.Text = "";
            this.ddltxtShortAIFrameCode.SelectedIndex = -1;

            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();

            if (flag)
                this.ddlWoType.SelectedIndex = -1;

            this.ddlWostatus.SelectedIndex = -1;

            this.ddlByIm.SelectedIndex = -1;

            this.ddlGlassLength.SelectedIndex = -1;

            this.ddlPackingPattern.SelectedIndex = -1;

            this.ddlCancelStorageFlag.SelectedIndex = -1;

            this.ddlIsOnlyPacking.SelectedIndex = -1;

            this.ddlCellBatch.Items.Clear();
            this.ddlCellBatch.Text = "";
            this.ddlCellBatch.SelectedIndex = -1;


            this.ddlShortAIFrameBatch.Items.Clear();
            this.ddlShortAIFrameBatch.Text = "";
            this.ddlShortAIFrameBatch.SelectedIndex = -1;

            this.ddlCellNetBoard.SelectedIndex = -1;

            this.ddlCellTransfer.Items.Clear();
            this.ddlCellTransfer.Text = "";
            this.ddlCellTransfer.SelectedIndex = -1;

            this.ddlGlassBatch.Items.Clear();
            this.ddlGlassBatch.Text = "";
            this.ddlGlassBatch.SelectedIndex = -1;

            this.ddlEVABatch.Items.Clear();
            this.ddlEVABatch.Text = "";
            this.ddlEVABatch.SelectedIndex = -1;

            this.ddlTPTBatch.Items.Clear();
            this.ddlTPTBatch.Text = "";
            this.ddlTPTBatch.SelectedIndex = -1;


            this.ddlConBoxBatch.Items.Clear();
            this.ddlConBoxBatch.Text = "";
            this.ddlConBoxBatch.SelectedIndex = -1;

            this.ddlAIFrameBatch.Items.Clear();
            this.ddlAIFrameBatch.Text = "";
            this.ddlAIFrameBatch.SelectedIndex = -1;

            this.checkBox2.Checked = false;
            this.txtWO1.Clear();
            this.txtWO1.Enabled = false;
            this.pictureBox1.Enabled = false;
        }

        /// <summary>
        /// 工单:初始化样式
        /// </summary>
        private void ResetFormat(bool flag)
        {
            if (flag)
            {
                this.ddlCellBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlCellBatch.ForeColor = Color.Black;
                this.lblCellBatch.ForeColor = Color.Black;
                this.lblCell.ForeColor = Color.Black;
                this.ddltxtCell.ForeColor = Color.Black;
                this.PicBoxCellBatch.Enabled = true;

                this.ddlGlassBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlGlassBatch.ForeColor = Color.Black;
                this.lblGlassBatch.ForeColor = Color.Black;
                this.lblGlass.ForeColor = Color.Black;
                this.ddltxtGlassCode.ForeColor = Color.Black;
                this.PicBoxGlassBatch.Enabled = true;

                this.ddlEVABatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlEVABatch.ForeColor = Color.Black;
                this.lblEVABatch.ForeColor = Color.Black;
                this.lblEVA.ForeColor = Color.Black;
                this.ddltxtEVACode.ForeColor = Color.Black;
                this.PicBoxEVABatch.Enabled = true;

                this.ddlTPTBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlTPTBatch.ForeColor = Color.Black;
                this.lblTPTBatch.ForeColor = Color.Black;
                this.lblTPT.ForeColor = Color.Black;
                this.ddltxtTPTCode.ForeColor = Color.Black;
                this.PicBoxTPTBatch.Enabled = true;

                this.ddlAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrame.ForeColor = Color.Black;
                this.ddltxtAIFrameCode.ForeColor = Color.Black;
                this.PicBoxAIFrameBatch.Enabled = true;


                this.ddlShortAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrame.ForeColor = Color.Black;
                this.ddltxtShortAIFrameCode.ForeColor = Color.Black;
                this.PicBoxShortAIFrameBatch.Enabled = true;

                this.ddlConBoxBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlConBoxBatch.ForeColor = Color.Black;
                this.lblConBoxBatch.ForeColor = Color.Black;
                this.lblConBox.ForeColor = Color.Black;
                this.ddltxtConBoxCod.ForeColor = Color.Black;
                this.PicBoxConBoxBatch.Enabled = true;
            }
            else
            {
                this.ddlCellBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlCellBatch.ForeColor = Color.Black;
                this.lblCellBatch.ForeColor = Color.Black;
                this.lblCell.ForeColor = Color.Black;
                this.ddltxtCell.ForeColor = Color.Black;
                this.PicBoxCellBatch.Enabled = true;

                this.ddlGlassBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlGlassBatch.ForeColor = Color.Black;
                this.lblGlassBatch.ForeColor = Color.Black;
                this.lblGlass.ForeColor = Color.Black;
                this.ddltxtGlassCode.ForeColor = Color.Black;
                this.PicBoxGlassBatch.Enabled = true;

                this.ddlEVABatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlEVABatch.ForeColor = Color.Black;
                this.lblEVABatch.ForeColor = Color.Black;
                this.lblEVA.ForeColor = Color.Black;
                this.ddltxtEVACode.ForeColor = Color.Black;
                this.PicBoxEVABatch.Enabled = true;

                this.ddlTPTBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlTPTBatch.ForeColor = Color.Black;
                this.lblTPTBatch.ForeColor = Color.Black;
                this.lblTPT.ForeColor = Color.Black;
                this.ddltxtTPTCode.ForeColor = Color.Black;
                this.PicBoxTPTBatch.Enabled = true;

                this.ddlAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrame.ForeColor = Color.Black;
                this.ddltxtAIFrameCode.ForeColor = Color.Black;
                this.PicBoxAIFrameBatch.Enabled = true;


                this.ddlShortAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrame.ForeColor = Color.Black;
                this.ddltxtShortAIFrameCode.ForeColor = Color.Black;
                this.PicBoxShortAIFrameBatch.Enabled = true;

                this.ddlConBoxBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlConBoxBatch.ForeColor = Color.Black;
                this.lblConBoxBatch.ForeColor = Color.Black;
                this.lblConBox.ForeColor = Color.Black;
                this.ddltxtConBoxCod.ForeColor = Color.Black;
                this.PicBoxConBoxBatch.Enabled = true;
            }
        }
        #endregion

        #region 托号查询
        /// <summary>
        /// 托号查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CartonQuery()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            string CartonNo = Convert.ToString(this.txtCarton.Text.Trim());
            if (CartonNo.Equals(""))
            {
                ToolsClass.Log("托号不能为空!", "ABNORMAL", lstView);
                this.txtCarton.SelectAll();
                this.txtCarton.Focus();
                return;
            }
            else
            {
                DataTable dt = ProductStorageDAL.GetCartonInfo(CartonNo);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (this.dataGridView1.Rows.Count > 0)
                        this.dataGridView1.Rows.Clear();
                    int RowNo = 0;
                    int RowIndex = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[RowNo].Cells["ModuleSN"].Value = Convert.ToString(row["SN"]);
                        if (Convert.ToString(row["flag"]).Equals("0"))
                            this.dataGridView1.Rows[RowNo].Cells["SnStatus"].Value = "已测试";
                        else if (Convert.ToString(row["flag"]).Equals("1"))
                            this.dataGridView1.Rows[RowNo].Cells["SnStatus"].Value = "已包装";
                        else if (Convert.ToString(row["flag"]).Equals("2"))
                            this.dataGridView1.Rows[RowNo].Cells["SnStatus"].Value = "已入库";
                        else
                            this.dataGridView1.Rows[RowNo].Cells["SnStatus"].Value = Convert.ToString(row["flag"]);
                        this.dataGridView1.Rows[RowNo].Cells["CartonNo"].Value = Convert.ToString(row["BoxID"]);
                        this.dataGridView1.Rows[RowNo].Cells["TestPower"].Value = Convert.ToString(row["Pmax"]);
                        this.dataGridView1.Rows[RowNo].Cells["StdPower"].Value = Convert.ToString(row["stdPower"]);
                        this.dataGridView1.Rows[RowNo].Cells["ModuleGrade"].Value = Convert.ToString(row["ModuleClass"]);
                        this.dataGridView1.Rows[RowNo].Cells["FinishedOn"].Value = Convert.ToString(row["ProcessDateTime"]);
                        this.dataGridView1.Rows[RowNo].Cells["CustomerCartonNo"].Value = Convert.ToString(row["Cust_BoxID"]);
                        this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                        RowNo++;
                        RowIndex++;
                    }
                    this.dataGridView1.CurrentCell = this.dataGridView1.Rows[0].Cells[3];
                    this.txtCarton.Clear();
                }
                else
                {
                    ToolsClass.Log("托号不存在或者已经入过库!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
            }
        }
        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                CartonQuery();
            }
        }

        private void txtCarton_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114)
            {
                pictureBox1_Click(sender, e);
            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var frm = new FormCartonInfo(this.txtCarton.Text.Trim());
            frm.ShowDialog();
            this.txtCarton.Text = frm.Carton;
            if (frm != null)
                frm.Dispose();
            CartonQuery();
        }
        #endregion

        #region 页面事件

        private void CartonReset_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            this.txtCarton.Text = "";
            this.txtCarton.Focus();

        }

        private void SetIsEnable(bool flag)
        {
            if (flag)
            {
                CartonQueryList.Enabled = true;
                this.pictureBox1.Enabled = true;
                this.CartonReset.Enabled = true;
                this.dataGridView1.Enabled = true;
                this.splitContainer3.Panel1.Enabled = true;
                this.Set.Enabled = true;
                this.Reset.Enabled = true;
                this.SapSave.Enabled = true;

            }
            else
            {
                CartonQueryList.Enabled = false;
                this.pictureBox1.Enabled = false;
                this.CartonReset.Enabled = false;
                this.dataGridView1.Enabled = false;
                this.splitContainer3.Panel1.Enabled = false;
                this.Set.Enabled = false;
                this.Reset.Enabled = false;
                this.SapSave.Enabled = false;
            }
        }
        /// <summary>
        /// sap入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SapSave_Click(object sender, EventArgs e)
        {
            try
            {
                SetIsEnable(false);

                #region
                if (this.dataGridView1.Rows.Count < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有要入库的数据!", "ABNORMAL", lstView);
                    return;
                }

                if (_ListSapWo.Count > 0)
                    _ListSapWo.Clear();

                string flag = "N";
                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if (dgvRow.Cells[2].Value.Equals(""))
                    {
                        flag = "Y";
                        break;
                    }
                }
                if (flag.Equals("Y"))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("组件信息没有维护完成!", "ABNORMAL", lstView);
                    return;
                }

                string carton = "";
                string wo = "";
                DateTime CurrentTime = DateTime.Now;
                string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期
                string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码

                var tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
                var createdOn = DT.DateTime().LongDateTime;
                var createdBy = FormCover.CurrUserName;
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
                DataTable cartonTable = null;
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {                
                    if (carton.Equals(""))
                        carton = Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonNo"].Value).Trim();

                    //wo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value).Trim();

                    SapWoModule SapWo = new SapWoModule();
                    SapWo.ActionCode = "I";
                    SapWo.SysId = Convert.ToString(this.dataGridView1.Rows[i].Cells["ModuleSN"].Value).Trim();

                    if (Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo1"].Value).Trim().Equals(""))
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value);//外协后工单
                    else
                    {
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo1"].Value);//外协后工单
                        SapWo.RESV01 = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value);//外协前工单
                    }

                    SapWo.ProductCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ProductCode"].Value).Trim();
                    SapWo.PostedOn = PostedOn;
                    SapWo.FinishedOn = Convert.ToDateTime(this.dataGridView1.Rows[i].Cells["FinishedOn"].Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
                    SapWo.Unit = "PC";
                    SapWo.Factory = Convert.ToString(this.dataGridView1.Rows[i].Cells["Factory"].Value).Trim();
                    SapWo.Workshop = Convert.ToString(this.dataGridView1.Rows[i].Cells["Workshop"].Value).Trim();
                    SapWo.PackingLocation = Convert.ToString(this.dataGridView1.Rows[i].Cells["PackingLocation"].Value).Trim();
                    SapWo.CellEff = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellEff"].Value).Trim();
                    SapWo.ModuleSN = Convert.ToString(this.dataGridView1.Rows[i].Cells["ModuleSN"].Value).Trim();
                    SapWo.CartonNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonNo"].Value).Trim();
                    SapWo.TestPower = Convert.ToString(this.dataGridView1.Rows[i].Cells["TestPower"].Value).Trim();
                    SapWo.StdPower = Convert.ToString(this.dataGridView1.Rows[i].Cells["StdPower"].Value).Trim();
                    SapWo.OrderStatus = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderStatus"].Value).Trim();
                    SapWo.PostKey = PostKey;
                    SapWo.ModuleGrade = Convert.ToString(this.dataGridView1.Rows[i].Cells["ModuleGrade"].Value);
                    SapWo.ByIm = Convert.ToString(this.dataGridView1.Rows[i].Cells["ByIm"].Value).Trim();
                    SapWo.CellCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellCode"].Value).Trim();
                    SapWo.CellBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellBatch"].Value).Trim();
                    SapWo.CellPrintMode = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellPrintMode"].Value).Trim();
                    SapWo.GlassCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassCode"].Value).Trim();
                    SapWo.GlassBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassBatch"].Value).Trim();
                    SapWo.EvaCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["EvaCode"].Value).Trim();
                    SapWo.EvaBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["EvaBatch"].Value).Trim();
                    SapWo.TptCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["TptCode"].Value).Trim();
                    SapWo.TptBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["TptBatch"].Value).Trim();
                    SapWo.ConBoxCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ConBoxCode"].Value).Trim();
                    SapWo.ConBoxBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["ConBoxBatch"].Value).Trim();
                    SapWo.LongFrameCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["LongFrameCode"].Value);
                    SapWo.LongFrameBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["LongFrameBatch"].Value);
                    SapWo.SalesItemNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesItemNo"].Value);
                    SapWo.SalesOrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesOrderNo"].Value);
                    SapWo.ShortFrameBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["ShortFrameBatch"].Value);
                    SapWo.ShortFrameCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ShortFrameCode"].Value);
                    SapWo.PackingMode = Convert.ToString(this.dataGridView1.Rows[i].Cells["PackingMode"].Value);
                    SapWo.GlassThickness = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassThickness"].Value);
                    SapWo.IsCancelPacking = "1";
                    SapWo.IsOnlyPacking = Convert.ToString(this.dataGridView1.Rows[i].Cells["IsOnlyPacking"].Value);
                    SapWo.CustomerCartonNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["CustomerCartonNo"].Value);
                    SapWo.InnerJobNo = "";
                    _ListSapWo.Add(SapWo);

                    var moduleColor = string.Empty;
                    var tolerance = string.Empty;
                    var isRework = "N";
                    var stdPowerLevel = string.Empty;
                    if (cartonTable == null)
                    {
                        cartonTable = ProductStorageDAL.GetSAPCartonStorageInfo(SapWo.CartonNo);
                    }
                    if (cartonTable != null && cartonTable.Rows != null && cartonTable.Rows.Count > 0)
                    {
                        var rows = cartonTable.Select(string.Format("[组件序列号]='{0}'", SapWo.ModuleSN));
                        if (rows.Length > 0)
                        {
                            moduleColor = Convert.ToString(rows[0]["组件颜色"]);
                            tolerance = Convert.ToString(rows[0]["公差"]);
                            isRework = Convert.ToString(rows[0]["是否重工"]);
                            stdPowerLevel = Convert.ToString(rows[0]["StdPowerLevel"]);
                        }
                    }
                    var tsapReceiptUploadModule = new TsapReceiptUploadModule
                    {
                        Sysid = Guid.NewGuid().ToString(""),
                        CreatedOn = createdOn,
                        CreatedBy = createdBy,
                        GroupHistKey = groupHistKey,
                        CartonNo = SapWo.CartonNo,
                        CustomerCartonNo = SapWo.CustomerCartonNo,
                        FinishedOn = SapWo.FinishedOn,
                        ModuleColor = moduleColor,
                        ModuleSn = SapWo.ModuleSN,
                        OrderNo = SapWo.OrderNo,
                        SalesOrderNo = SapWo.SalesOrderNo,
                        SalesItemNo = SapWo.SalesItemNo,
                        OrderStatus = SapWo.OrderStatus,
                        ProductCode = SapWo.ProductCode,
                        Unit = SapWo.Unit,
                        Factory = SapWo.Factory,
                        Workshop = SapWo.Workshop,
                        PackingLocation = SapWo.PackingLocation,
                        PackingMode = SapWo.PackingMode,
                        CellEff = SapWo.CellEff,
                        TestPower = SapWo.TestPower,
                        StdPower = SapWo.StdPower,
                        ModuleGrade = SapWo.ModuleGrade,
                        ByIm = SapWo.ByIm,
                        Tolerance = tolerance,
                        CellCode = SapWo.CellCode,
                        CellBatch = SapWo.CellBatch,
                        CellPrintMode = SapWo.CellPrintMode,
                        GlassCode = SapWo.GlassCode,
                        GlassBatch = SapWo.GlassBatch,
                        EvaCode = SapWo.EvaCode,
                        EvaBatch = SapWo.EvaBatch,
                        TptCode = SapWo.TptCode,
                        TptBatch = SapWo.TptBatch,
                        ConboxCode = SapWo.ConBoxCode,
                        ConboxBatch = SapWo.ConBoxBatch,
                        ConboxType = string.Empty,
                        LongFrameCode = SapWo.LongFrameCode,
                        LongFrameBatch = SapWo.LongFrameBatch,
                        ShortFrameCode = SapWo.ShortFrameCode,
                        ShortFrameBatch = SapWo.ShortFrameBatch,
                        GlassThickness = SapWo.GlassThickness,
                        IsRework = isRework,
                        IsByProduction = "false",
                        Resv01 = stdPowerLevel
                    };
                    if (tsapReceiptUploadModule.OrderNo.StartsWith("111") || tsapReceiptUploadModule.OrderNo.StartsWith("333"))
                        tsapReceiptUploadModule.IsRework = "Y";
                    tsapReceiptUploadModules.Add(tsapReceiptUploadModule);
                }

                if (_ListSapWo.Count > 0)
                {
                    string msg = "";
                    if (!ProductStorageDAL.CheckWoStorage(_ListSapWo, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    if (!DataAccess.QueryReworkOrgBatch(tsapReceiptUploadModules, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }

                    //var findAll =
                    //    tsapReceiptUploadModules.FindAll(
                    //        p =>
                    //        string.IsNullOrEmpty(p.ProductCode) || string.IsNullOrEmpty(p.OrderNo) ||
                    //        string.IsNullOrEmpty(p.CellCode) || string.IsNullOrEmpty(p.CellBatch) ||
                    //        string.IsNullOrEmpty(p.GlassCode) || string.IsNullOrEmpty(p.GlassBatch) ||
                    //        string.IsNullOrEmpty(p.EvaCode) || string.IsNullOrEmpty(p.EvaBatch) ||
                    //        string.IsNullOrEmpty(p.TptCode) || string.IsNullOrEmpty(p.TptBatch) ||
                    //        string.IsNullOrEmpty(p.ConboxCode) || string.IsNullOrEmpty(p.ConboxBatch) ||
                    //        string.IsNullOrEmpty(p.LongFrameCode) || string.IsNullOrEmpty(p.LongFrameBatch) ||
                    //        string.IsNullOrEmpty(p.ShortFrameCode) || string.IsNullOrEmpty(p.ShortFrameBatch) ||
                    //        string.IsNullOrEmpty(p.PackingMode) || string.IsNullOrEmpty(p.ModuleGrade) ||
                    //        string.IsNullOrEmpty(p.ByIm) || string.IsNullOrEmpty(p.StdPower) ||
                    //        string.IsNullOrEmpty(p.ModuleColor) || string.IsNullOrEmpty(p.Tolerance));
                    //if (findAll.Count > 0)
                    //{
                    //    const string errorMsg =
                    //        "SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、电流分档、标称功率、组件颜色、公差";
                    //    SetIsEnable(true);
                    //    ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                    //    return;
                    //}


                    List<string> listMessages = SapMessages(tsapReceiptUploadModules);
                    if (listMessages.Count > 0)
                    {
                        SetIsEnable(true);
                        foreach (var errorMsg in listMessages)
                        {
                            ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        }
                        return;
                    }

                    if (!DataAccess.IsSapInventoryUseManulUpload(FormCover.CurrentFactory.Trim(), FormCover.InterfaceConnString))
                    {
                        SaveXmlFormat(_ListSapWo, carton, wo);
                    }
                    else
                    {
                        //保存数据供手动上传使用
                        SavePreUploadData(_ListSapWo, carton, tsapReceiptUploadModules);
                    }
                }

                SetIsEnable(true);
                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常(处理包装数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }
        private List<string> SapMessages(List<TsapReceiptUploadModule> sapReceipt)
        {
            List<string> messages = new List<string>();
            try
            {

                //"SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、
                //玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、
                //接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、
                //电流分档、标称功率、组件颜色、公差";

                if (sapReceipt != null && sapReceipt.Count > 0)
                {
                    StringBuilder strtmp;
                    int i = 1;
                    foreach (TsapReceiptUploadModule sapModule in sapReceipt)
                    {
                        strtmp = new StringBuilder();
                        bool blntmp = true;

                        if (!string.IsNullOrEmpty(sapModule.OrderNo))
                        {
                            strtmp.Append("工单号:" + sapModule.OrderNo + "--");
                        }
                        else
                        {
                            strtmp.Append("【第 " + i.ToString() + "行工单号为空】");
                            blntmp = false;
                        }
                        if (!string.IsNullOrEmpty(sapModule.ModuleSn))
                        {
                            strtmp.Append("序列号:" + sapModule.ModuleSn + "   缺失的值【");

                        }
                        if (string.IsNullOrEmpty(sapModule.ProductCode))
                        {
                            strtmp.Append("产品物料代码、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellCode))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellBatch))
                        {
                            strtmp.Append("电池片批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassCode))
                        {
                            strtmp.Append("玻璃批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassBatch))
                        {
                            strtmp.Append("EVA物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaCode))
                        {
                            strtmp.Append("EVA批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaBatch))
                        {
                            strtmp.Append("背板物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptCode))
                        {
                            strtmp.Append("背板批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptBatch))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxCode))
                        {
                            strtmp.Append("接线盒物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxBatch))
                        {
                            strtmp.Append("接线盒批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameCode))
                        {
                            strtmp.Append("长边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameBatch))
                        {
                            strtmp.Append("长边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameCode))
                        {
                            strtmp.Append("短边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameBatch))
                        {
                            strtmp.Append("短边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.PackingMode))
                        {
                            strtmp.Append("包装方式、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleGrade))
                        {
                            strtmp.Append("组件等级、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ByIm))
                        {
                            strtmp.Append("电流分档、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.StdPower))
                        {
                            strtmp.Append("标称功率、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleColor))
                        {
                            strtmp.Append("组件颜色、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.Tolerance))
                        {
                            strtmp.Append("公差、");
                            blntmp = false;
                        }
                        if (strtmp != null && !string.IsNullOrEmpty(strtmp.ToString()) && blntmp == false)
                        {
                            messages.Add(strtmp + "】 【第 " + i.ToString() + " 行】");
                        }
                        i = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add("SAP 数据检查异常:" + ex.Message.ToString());
            }
            return messages;
        }
        /// <summary>
        /// 上传SAP
        /// </summary>
        /// <returns></returns>
        private void SaveXmlFormat(List<SapWoModule> listWo, string carton, string wo)
        {
            try
            {
                #region
                string flag = "Y";
                string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(listWo, Encoding.UTF8);
                SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");

                ToolsClass.Log("内部托号：" + Convert.ToString(this.dataGridView1.Rows[0].Cells["CartonNo"].Value).Trim() + "入库数据保存");
                ToolsClass.Log(SapStorageString);
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                string aa = service.packingDataMesToSap(SapStorageString);
                string bb;
                DataTable dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
                if (!listWo.Count.Equals(dt.Rows.Count))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("上传SAP和SAP返回的总数据不一致", "ABNORMAL", lstView);
                    return;
                }
                string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                if (dt != null && dt.Rows.Count > 0)
                {
                    //if (_ListCartonSucess.Count  > 0)
                    //    _ListCartonSucess.Clear();

                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["Result"]).Equals("0"))
                        {
                            SetIsEnable(true);
                            ToolsClass.Log("上传SAP保存失败,原因:" + row["Message"] + "", "ABNORMAL", lstView);
                            flag = "N";
                            break;
                        }
                    }
                    //更新数据库
                    if (flag.Equals("Y"))
                    {
                        if (ProductStorageDAL.updatepackingdatabySn(_ListSapWo, joinid, posttime))
                        {
                            //if (!_ListCartonSucess.Contains(carton))
                            //    _ListCartonSucess.Add(carton);
                            //#region 写入库交易记录

                            //if (_ListCartonSucess.Count > 0)
                            //{
                            //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "Invertory");
                            //}
                            //#endregion
                            ProductStorageDAL.SaveStorageInfoLog(_ListSapWo);

                            SetIsEnable(true);
                            ToolsClass.Log("托号：" + carton + " 上传SAP保存成功", "NORMAL", lstView);

                            if (this.dataGridView1.Rows.Count > 0)
                                this.dataGridView1.Rows.Clear();
                        }
                        else
                        {
                            SetIsEnable(true);
                            ToolsClass.Log("托号：" + carton + " 上传SAP保存失败(更新包装数据库失败)", "ABNORMAL", lstView);
                        }
                    }
                }
                else
                {
                    SetIsEnable(true);
                    ToolsClass.Log("上传SA保存返回数据错误", "ABNORMAL", lstView);
                    return;
                }

                #region
                //StringBuilder code = new StringBuilder();
                //code.Append("<?xml version='1.0' encoding='utf-8' ?>");
                //code.Append("<MesPackingData>");
                //foreach (SapWoModule wo in listWo)
                //{
                //    code.Append("<PackingData>");
                //    code.Append(string.Format("<ActionCode>{0}</ActionCode>", wo.OrderNo));
                //    code.Append(string.Format("<SysId>{0}</SysId>", wo.SysId));
                //    code.Append(string.Format("<OrderNo>{0}</OrderNo>", wo.OrderNo));
                //    code.Append(string.Format("<ProductCode>{0}</ProductCode>", wo.ProductCode));
                //    code.Append(string.Format("<FinishedOn>{0}</FinishedOn>", wo.FinishedOn));
                //    code.Append(string.Format("<PostedOn>{0}</PostedOn>", wo.PostedOn));
                //    code.Append(string.Format("<Unit>{0}</Unit>", wo.Unit));
                //    code.Append(string.Format("<Factory>{0}</Factory>", wo.Factory));
                //    code.Append(string.Format("<Workshop>{0}</Workshop>", wo.Workshop));
                //    code.Append(string.Format("<PackingLocation>{0}</PackingLocation>", wo.PackingLocation));
                //    code.Append(string.Format("<CellEff>{0}</CellEff>", wo.CellEff));
                //    code.Append(string.Format("<ModuleSN>{0}</ModuleSN>", wo.ModuleSN));
                //    code.Append(string.Format("<CartonNo>{0}</CartonNo>", wo.CartonNo));
                //    code.Append(string.Format("<TestPower>{0}</TestPower>", wo.TestPower));
                //    code.Append(string.Format("<StdPower>{0}</StdPower>", wo.StdPower));
                //    code.Append(string.Format("<OrderStatus>{0}</OrderStatus>", wo.OrderStatus));
                //    code.Append(string.Format("<PostKey>{0}</PostKey>", wo.PostKey));
                //    code.Append(string.Format("<ModuleGrade>{0}</ModuleGrade>", wo.ModuleGrade));
                //    code.Append(string.Format("<ByIm>{0}</ByIm>", wo.ByIm));
                //    code.Append(string.Format("<CellCode>{0}</CellCode>", wo.CellCode));
                //    code.Append(string.Format("<CellBatch>{0}</CellBatch>", wo.CellBatch));
                //    code.Append(string.Format("<CellPrintMode>{0}</CellPrintMode>", wo.CellPrintMode));
                //    code.Append(string.Format("<GlassCode>{0}</GlassCode>", wo.GlassCode));
                //    code.Append(string.Format("<GlassBatch>{0}</GlassBatch>", wo.GlassBatch));
                //    code.Append(string.Format("<EvaCode>{0}</EvaCode>", wo.EvaCode));
                //    code.Append(string.Format("<EvaBatch>{0}</EvaBatch>", wo.EvaBatch));
                //    code.Append(string.Format("<TptCode>{0}</TptCode>", wo.TptCode));
                //    code.Append(string.Format("<TptBatch>{0}</TptBatch>", wo.TptBatch));
                //    code.Append(string.Format("<ConBoxCode>{0}</ConBoxCode>", wo.ConBoxCode));
                //    code.Append(string.Format("<ConBoxBatch>{0}</ConBoxBatch>", wo.ConBoxBatch));
                //    code.Append(string.Format("<FrameCode>{0}</FrameCode>", wo.FrameCode));
                //    code.Append(string.Format("<FrameBatch>{0}</FrameBatch>", wo.FrameBatch));
                //    code.Append("</PackingData>");
                //}
                //code.ToString();
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("上传SAP保存时发生异常(处理SAP返回数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        private void SavePreUploadData(List<SapWoModule> listWo, string carton, List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            try
            {
                #region

                ToolsClass.Log("内部托号：" + Convert.ToString(this.dataGridView1.Rows[0].Cells["CartonNo"].Value).Trim() + "入库数据保存");

                string joinid = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                if (ProductStorageDAL.updatepackingdatabySn(tsapReceiptUploadModules, listWo, joinid, posttime))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("托号：" + carton + "待上传SAP数据保存成功", "NORMAL", lstView);

                    if (this.dataGridView1.Rows.Count > 0)
                        this.dataGridView1.Rows.Clear();
                }
                else
                {
                    SetIsEnable(true);
                    ToolsClass.Log("托号：" + carton + "待上传SAP数据保存失败(更新包装数据库失败)", "ABNORMAL", lstView);
                }

                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        /// <summary>
        /// 工单重置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, EventArgs e)
        {
            if (dgvIndex.Count > 0)
                dgvIndex.Clear();
            ClearData(true);
            ResetFormat(false);
            this.txtWoOrder.SelectAll();
            this.txtWoOrder.Focus();
            return;
        }

        /// <summary>
        /// 工单类型选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlWoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearData(false);

            if (this.ddlWoType.SelectedIndex == 0)
            {
                WoType = "";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
            }
            else if (this.ddlWoType.SelectedIndex == 1)
            {
                if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M01"))
                {
                    WoType = "ZP01";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M07"))
                {
                    WoType = "ZP07";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN01"))
                {
                    WoType = "ZPA1";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN02"))
                {
                    WoType = "ZPA2";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }

                else
                {
                    WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                    WoType = WoType.Replace("M", "");
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
            }
            else if (this.ddlWoType.SelectedIndex == 2)
            {
                //WoType = "ZP11";
                switch (FormCover.PlanCode)
                {
                    case "CS":
                        WoType = "ZP11";
                        break;
                    case "VNSM":
                        WoType = "ZPA9";
                        break;
                    default:
                        WoType = "";
                        break;
                }

                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
            }
        }

        /// <summary>
        /// 电池分档选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlByIm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlByIm.SelectedIndex == 0)
                ByIm_flag = "";
            else if (this.ddlByIm.SelectedIndex == 1)
                ByIm_flag = "Y";
            else if (this.ddlByIm.SelectedIndex == 2)
                ByIm_flag = "N";
        }

        /// <summary>
        /// 工单生产状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlWostatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlWostatus.SelectedIndex == 0)
                WoStatus = "";
            else if (this.ddlWostatus.SelectedIndex == 1)
                WoStatus = "1";
            else if (this.ddlWostatus.SelectedIndex == 1)
                WoStatus = "2";
        }

        /// <summary>
        /// 对于重工工单，如果托里混有新组建，必须要输入物料特性
        /// </summary>
        /// <param name="Cartons">托号清单</param>
        private bool CheckNewSnIsNull(List<string> SN)
        {
            try
            {
                foreach (string serialnumber in SN)
                {
                    #region
                    DataTable dtnew = ProductStorageDAL.GetSNInfoFromInterface(serialnumber);
                    //SAP已经入库过
                    if ((dtnew != null) && (dtnew.Rows.Count > 0))
                        continue;
                    else //重工时新加组件
                        if (!InputDataIsNotNull())
                        {
                            ToolsClass.Log("组件：" + serialnumber + " 为新组件，物料信息不能为空", "ABNORMAL", lstView);
                            return false;
                        }
                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                ToolsClass.Log("检查组件物料完整性时发生了异常 " + ex.Message + "", "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 给拖号设置工单信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Set_Click(object sender, EventArgs e)
        {
            try
            {
                #region
                if (this.dataGridView1.Rows.Count > 0)
                {
                    if (dgvIndex.Count > 0)
                    {
                        dgvIndex.Clear();
                    }
                    if (SNArrayS.Count > 0)
                    {
                        SNArrayS.Clear();
                    }
                    for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                    {
                        if (this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString().ToUpper() == "TRUE")
                        {
                            dgvIndex.Add(i, this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString());
                            SNArrayS.Add(this.dataGridView1.Rows[i].Cells[3].EditedFormattedValue.ToString());
                        }
                    }
                    if (dgvIndex.Count < 1)
                    {
                        ToolsClass.Log("没有选中要设置的数据,请确认!", "ABNORMAL", lstView);
                        return;
                    }
                }
                else
                {
                    ToolsClass.Log("没有要设置的数据,请确认!", "ABNORMAL", lstView);
                    return;
                }

                if (!CheckInput())
                    return;

                foreach (int RowIndex in dgvIndex.Keys)
                {
                    this.dataGridView1.Rows[RowIndex].Cells["SnStatus"].Value = "待入库";
                    this.dataGridView1.Rows[RowIndex].Cells["SetFlag"].Value = "是";
                    this.dataGridView1.Rows[RowIndex].Cells["OrderNo"].Value = Convert.ToString(this.txtWo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["ProductCode"].Value = Convert.ToString(this.txtMitemCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["Factory"].Value = Convert.ToString(this.txtPlanCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["Workshop"].Value = Convert.ToString(this.txtFactory.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["PackingLocation"].Value = Convert.ToString(this.txtlocation.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["CellEff"].Value = this.ddlCellTransfer.Text.Trim();
                    this.dataGridView1.Rows[RowIndex].Cells["OrderStatus"].Value = "1";
                    this.dataGridView1.Rows[RowIndex].Cells["ByIm"].Value = ByIm_flag;
                    this.dataGridView1.Rows[RowIndex].Cells["CellCode"].Value = this.ddltxtCell.Text.Trim();
                    this.dataGridView1.Rows[RowIndex].Cells["CellBatch"].Value = this.ddlCellBatch.Text.Trim();
                    this.dataGridView1.Rows[RowIndex].Cells["CellPrintMode"].Value = "";// Convert.ToString(this.ddlCellNetBoard.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["GlassCode"].Value = this.ddltxtGlassCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["GlassBatch"].Value = this.ddlGlassBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["EvaCode"].Value = this.ddltxtEVACode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["EvaBatch"].Value = this.ddlEVABatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["TptCode"].Value = this.ddltxtTPTCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["TptBatch"].Value = this.ddlTPTBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["ConBoxCode"].Value = this.ddltxtConBoxCod.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["ConBoxBatch"].Value = this.ddlConBoxBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["LongFrameCode"].Value = this.ddltxtAIFrameCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["LongFrameBatch"].Value = this.ddlAIFrameBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["GlassThickness"].Value = "";// Convert.ToString(this.ddlGlassLength.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["PackingMode"].Value = PackingPatternValue;
                    this.dataGridView1.Rows[RowIndex].Cells["ShortFrameBatch"].Value = Convert.ToString(this.ddlShortAIFrameBatch.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["ShortFrameCode"].Value = Convert.ToString(this.ddltxtShortAIFrameCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["IsCancelPacking"].Value = "1";
                    this.dataGridView1.Rows[RowIndex].Cells["SalesOrderNo"].Value = Convert.ToString(this.txtSalesOrderNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["SalesItemNo"].Value = Convert.ToString(this.txtSalesItemNo.Text.Trim());
                    //默认值
                    this.dataGridView1.Rows[RowIndex].Cells["IsOnlyPacking"].Value = "1";
                    //外协后工单
                    this.dataGridView1.Rows[RowIndex].Cells["OrderNo1"].Value = this.txtWO1.Text.Trim();
                }
                ToolsClass.Log("设置成功!", "NORMAL", lstView);
                Reset_Click(null, null);
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("设置数据时发生异常：" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }
        /// <summary>
        /// 单击Grid列标题中的 CheckBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ch_OnCheckBoxClicked(object sender, HeadCheckBox.datagridviewCheckboxHeaderEventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = e.CheckedState;
            }
        }
        #endregion

        #region 物料查询

        #region 电池片
        private void ddlCellBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void ddlCellBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void PicBoxCellBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlCellBatch.Text.Trim().ToString(), "Cell", this.ddlCellBatch, this.ddltxtCell);
        }

        private void ddlCellBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlCellBatch.Text.Trim(), this.ddltxtCell, true, "Cell");
        }

        private void ddlCellBatch_Leave(object sender, EventArgs e)
        {

        }

        #endregion

        #region GLASS
        private void ddlGlassBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void ddlGlassBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void PicBoxGlassBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlGlassBatch.Text.Trim().ToString(), "GLASS", this.ddlGlassBatch, this.ddltxtGlassCode);
        }

        private void ddlGlassBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlGlassBatch.Text.Trim(), this.ddltxtGlassCode, true, "GLASS");
        }
        private void ddlGlassBatch_Leave(object sender, EventArgs e)
        {

        }
        #endregion

        #region EVA
        private void ddlEVABatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ddlEVABatch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void PicBoxEVABatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlEVABatch.Text.Trim().ToString(), "EVA", this.ddlEVABatch, this.ddltxtEVACode);
        }

        private void ddlEVABatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlEVABatch.Text.Trim(), this.ddltxtEVACode, true, "EVA");
        }

        private void ddlEVABatch_Leave(object sender, EventArgs e)
        {

        }
        #endregion

        #region TPT
        private void ddlTPTBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void ddlTPTBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private void PicBoxTPTBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlTPTBatch.Text.Trim().ToString(), "TPT", this.ddlTPTBatch, this.ddltxtTPTCode);
        }
        private void ddlTPTBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlTPTBatch.Text.Trim(), this.ddltxtTPTCode, true, "TPT");
        }
        private void ddlTPTBatch_Leave(object sender, EventArgs e)
        {

        }
        #endregion

        #region 接线盒
        private void ddlConBoxBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void ddlConBoxBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void PicBoxConBoxBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlConBoxBatch.Text.Trim().ToString(), "CONBOX", this.ddlConBoxBatch, this.ddltxtConBoxCod);
        }

        private void ddlConBoxBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlConBoxBatch.Text.Trim(), this.ddltxtConBoxCod, true, "CONBOX");
        }
        private void ddlConBoxBatch_Leave(object sender, EventArgs e)
        {

        }
        #endregion

        #region 长边框
        private void ddlAIFrameBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void ddlAIFrameBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        private void PicBoxAIFrameBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlAIFrameBatch.Text.Trim().ToString(), "AIFRAME-LONG", this.ddlAIFrameBatch, this.ddltxtAIFrameCode);
        }

        private void ddlAIFrameBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlAIFrameBatch.Text.Trim(), this.ddltxtAIFrameCode, true, "AIFRAME-LONG");
        }
        private void ddlAIFrameBatch_Leave(object sender, EventArgs e)
        {

        }
        #endregion

        #region 短边框
        private void ddlShortAIFrameBatch_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void ddlShortAIFrameBatch_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ddlShortAIFrameBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlShortAIFrameBatch.Text.Trim(), this.ddltxtShortAIFrameCode, false, "AIFRAME-SHORT");
            if (this.ddltxtShortAIFrameCode.Text.Equals(""))
                this.ddlShortAIFrameBatch.Text = "";
        }

        private void ddlShortAIFrameBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlShortAIFrameBatch.Text.Trim(), this.ddltxtShortAIFrameCode, true, "AIFRAME-SHORT");
        }

        private void PicBoxShortAIFrameBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlShortAIFrameBatch.Text.Trim().ToString(), "AIFRAME-SHORT", this.ddlShortAIFrameBatch, this.ddltxtShortAIFrameCode);
        }
        #endregion

        private void ShowBatchInfo(string _BatchNo, string MitemType, object ddl, object txt, string _wo)
        {
            Control ddl_text = ddl as Control;
            Control text = txt as Control;
            var frm = new FormMitemBatch(_BatchNo, MitemType, _wo, WoType);
            frm.ShowDialog();
            ddl_text.Text = frm.Batch;
            text.Text = frm.MitemCode;
            if (frm != null)
                frm.Dispose();
        }
        private void setMitemCode(string batchno, object label, bool flag, string MitemType)
        {
            if (this.txtWo.Text.Trim().Equals(""))
            {
                ToolsClass.Log("请先选择工单!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }

            ComboBox c = label as ComboBox;
            c.Items.Clear();

            if (!batchno.Equals(""))
            {
                DataTable MitemCode = null;
                if (!string.IsNullOrEmpty(ELPS_WOTYE))
                    WoType = ELPS_WOTYE;

                if (ProductStorageDAL.GetMulMitemCode(batchno, MitemType, this.txtWo.Text.Trim(), WoType, out MitemCode) != null)
                {
                    foreach (DataRow row in MitemCode.Rows)
                    {
                        if (!c.Items.Contains(Convert.ToString(row["MaterialCode"]).Trim()))
                            c.Items.Add(Convert.ToString(row["MaterialCode"]).Trim());
                    }
                }
                else
                {
                    //MessageBox.Show("此批次:" + batchno + " 没找到所对应的物料，请确认！", MitemType);
                    //c.Text = "";
                    return;
                }
            }
            else
            {
                if (flag)
                {
                    MessageBox.Show("输入的批次号不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }
        private void SetMitemBatch(string batch, string type, object ddl, object txt)
        {
            if (!this.txtWo.Text.Equals(""))
                ShowBatchInfo(batch, type, ddl, txt, this.txtWo.Text.Trim());
            else
            {
                ToolsClass.Log("请先选择工单!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }
        }
        #endregion

        #region 工单查询
        private void txtWoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请先选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    this.ddlWoType.SelectAll();
                    this.txtWoOrder.Clear();
                    return;
                }

                if (string.IsNullOrEmpty(Convert.ToString(this.txtWoOrder.Text.Trim())))
                {
                    ToolsClass.Log("输入的工单不能为空,请重新输入!", "ABNORMAL", lstView);
                    this.txtWoOrder.Focus();
                    return;
                }

                if (dgvIndex.Count > 0)
                    dgvIndex.Clear();

                ClearData(false);

                //ResetFormat();

                SetWOData(true);
            }
        }
        private void txtWoOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    return;
                }
                PicBoxWO_Click(null, null);
            }

        }
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                this.ddlWoType.Focus();
                return;
            }

            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), WoType);
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
        }
        private void SetWOData(bool flag)
        {
            ClearData(false);

            if (Convert.ToString(this.txtWoOrder.Text.Trim()).Equals(""))
            {

                ToolsClass.Log("输入的工单为空，请确认！", "ABNORMAL", lstView);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }
            DataSet wo = ProductStorageDAL.GetWoInfo(Convert.ToString(this.txtWoOrder.Text.Trim()), WoType);
            if (wo != null && wo.Tables.Count > 0)
            {
                SetWoInfo(wo, WoType);
            }
            else
            {
                if (flag)
                {
                    ToolsClass.Log("工单：" + Convert.ToString(this.txtWoOrder.Text.Trim()) + " 没有从SAP下载或工单类型不一致,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
                else
                {
                    ToolsClass.Log("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "在SAP没有做物料转储,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
            }
            this.txtWoOrder.Clear();
        }
        private string ELPS_WOTYE = "";//elps的标志
        private void SetWoInfo(DataSet wo, string _Wotype)
        {
            DataTable dtWo = wo.Tables[0];
            if (dtWo.Rows.Count == 0)
            {
                ToolsClass.Log("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "不存在,请确认!", "ABNORMAL", lstView);
                return;
            }

            string NewWoType = "";
            DataRow row = dtWo.Rows[0];
            this.txtWo.Text = Convert.ToString(row["TWO_NO"]);
            this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
            this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
            this.txtlocation.Text = Convert.ToString(row["RESV04"]);
            this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
            this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
            this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
            NewWoType = Convert.ToString(row["RESV05"]).Trim();
            if (Convert.ToString(row["RESV05"]).Trim().Equals("ZP13"))
                ELPS_WOTYE = "ZP13";

            #region
            //电池片
            foreach (DataRow rows in wo.Tables[1].Rows)
            {
                if (!this.ddlCellBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlCellBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //玻璃
            foreach (DataRow rows in wo.Tables[2].Rows)
            {
                if (!this.ddlGlassBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlGlassBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //EVN
            foreach (DataRow rows in wo.Tables[3].Rows)
            {
                if (!this.ddlEVABatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlEVABatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //TPT
            foreach (DataRow rows in wo.Tables[4].Rows)
            {
                if (!this.ddlTPTBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlTPTBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //CONBOX
            foreach (DataRow rows in wo.Tables[5].Rows)
            {
                if (!this.ddlConBoxBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlConBoxBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //AIFRAME 1 长边框
            foreach (DataRow rows in wo.Tables[6].Rows)
            {
                if (!this.ddlAIFrameBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlAIFrameBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //AIFRAME 2 短边框
            foreach (DataRow rows in wo.Tables[7].Rows)
            {
                if (!this.ddlShortAIFrameBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlShortAIFrameBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            #endregion

            #region 小组件工单如果没有发料，给一个默认值
            if (NewWoType.Equals("ZP09"))
            {
                //电池片
                if (this.ddlCellBatch.Items.Count < 1)
                {
                    DataTable dtCell = ProductStorageDAL.GetStorageMitemCodeInfo("CELL");
                    if (dtCell != null)
                    {
                        DataRow rowCell = dtCell.Rows[0];
                        this.ddlCellBatch.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_02"]));
                        this.ddlCellBatch.SelectedIndex = 0;
                        this.ddlCellBatch.Enabled = false;

                        this.ddltxtCell.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_03"]));
                        this.ddltxtCell.SelectedIndex = 0;
                        this.ddltxtCell.Enabled = false;
                    }
                }
                //玻璃
                if (this.ddlGlassBatch.Items.Count < 1)
                {
                    DataTable dtGLASS = ProductStorageDAL.GetStorageMitemCodeInfo("GLASS");
                    if (dtGLASS != null)
                    {
                        DataRow rowGLASS = dtGLASS.Rows[0];
                        this.ddlGlassBatch.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_02"]));
                        this.ddlGlassBatch.SelectedIndex = 0;
                        this.ddlGlassBatch.Enabled = false;

                        this.ddltxtGlassCode.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_03"]));
                        this.ddltxtGlassCode.SelectedIndex = 0;
                        this.ddltxtGlassCode.Enabled = false;
                    }
                }
                // EVA
                if (this.ddlEVABatch.Items.Count < 1)
                {
                    DataTable dtEVA = ProductStorageDAL.GetStorageMitemCodeInfo("EVA");
                    if (dtEVA != null)
                    {
                        DataRow rowEVA = dtEVA.Rows[0];
                        this.ddlEVABatch.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_02"]));
                        this.ddlEVABatch.SelectedIndex = 0;
                        this.ddlEVABatch.Enabled = false;

                        this.ddltxtEVACode.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_03"]));
                        this.ddltxtEVACode.SelectedIndex = 0;
                        this.ddltxtEVACode.Enabled = false;
                    }
                }
                // TPT
                if (this.ddlTPTBatch.Items.Count < 1)
                {
                    DataTable dtTPT = ProductStorageDAL.GetStorageMitemCodeInfo("TPT");
                    if (dtTPT != null)
                    {
                        DataRow rowTPT = dtTPT.Rows[0];
                        this.ddlTPTBatch.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_02"]));
                        this.ddlTPTBatch.SelectedIndex = 0;
                        this.ddlTPTBatch.Enabled = false;

                        this.ddltxtTPTCode.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_03"]));
                        this.ddltxtTPTCode.SelectedIndex = 0;
                        this.ddltxtTPTCode.Enabled = false;
                    }
                }
                //长边框
                if (this.ddlAIFrameBatch.Items.Count < 1)
                {
                    DataTable dtAIFRAMELONG = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-LONG");
                    if (dtAIFRAMELONG != null)
                    {
                        DataRow rowAIFRAMELONG = dtAIFRAMELONG.Rows[0];
                        this.ddlAIFrameBatch.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_02"]));
                        this.ddlAIFrameBatch.SelectedIndex = 0;
                        this.ddlAIFrameBatch.Enabled = false;

                        this.ddltxtAIFrameCode.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_03"]));
                        this.ddltxtAIFrameCode.SelectedIndex = 0;
                        this.ddltxtAIFrameCode.Enabled = false;
                    }
                }
                //短边框
                if (this.ddlShortAIFrameBatch.Items.Count < 1)
                {
                    DataTable dtAIFRAMESHORT = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-SHORT");
                    if (dtAIFRAMESHORT != null)
                    {
                        DataRow rowAIFRAMESHORT = dtAIFRAMESHORT.Rows[0];
                        this.ddlShortAIFrameBatch.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_02"]));
                        this.ddlShortAIFrameBatch.SelectedIndex = 0;
                        this.ddlShortAIFrameBatch.Enabled = false;

                        this.ddltxtShortAIFrameCode.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_03"]));
                        this.ddltxtShortAIFrameCode.SelectedIndex = 0;
                        this.ddltxtShortAIFrameCode.Enabled = false;
                    }
                }
                //接线盒
                if (this.ddlConBoxBatch.Items.Count < 1)
                {
                    DataTable dtCONBOX = ProductStorageDAL.GetStorageMitemCodeInfo("CONBOX");
                    if (dtCONBOX != null)
                    {
                        DataRow rowCONBOX = dtCONBOX.Rows[0];
                        this.ddlConBoxBatch.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_02"]));
                        this.ddlConBoxBatch.SelectedIndex = 0;
                        this.ddlConBoxBatch.Enabled = false;

                        this.ddltxtConBoxCod.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_03"]));
                        this.ddltxtConBoxCod.SelectedIndex = 0;
                        this.ddltxtConBoxCod.Enabled = false;
                    }
                }
            }
            #endregion

            #region 双玻组件的虚拟物料指定批次
            var bom = wo.Tables[8];
            if (bom != null && bom.Rows != null && bom.Rows.Count > 0)
            {
                //电池片
                if (this.ddlCellBatch.Items.Count < 1)
                {
                    var cellRows = bom.Select("MaterialCategory='CELL' AND Resv04='1'");
                    if (cellRows != null && cellRows.Length > 0)
                    {
                        var cellRow = cellRows[0];
                        this.ddlCellBatch.Items.Add(Convert.ToString(cellRow["Resv05"]));
                        this.ddlCellBatch.SelectedIndex = 0;
                        this.ddlCellBatch.Enabled = false;

                        this.ddltxtCell.Items.Add(Convert.ToString(cellRow["MaterialCode"]));
                        this.ddltxtCell.SelectedIndex = 0;
                        this.ddltxtCell.Enabled = false;
                    }
                }
                //玻璃
                if (this.ddlGlassBatch.Items.Count < 1)
                {
                    var glassRows = bom.Select("MaterialCategory='GLASS' AND Resv04='1'");
                    if (glassRows != null && glassRows.Length > 0)
                    {
                        var glassRow = glassRows[0];
                        this.ddlGlassBatch.Items.Add(Convert.ToString(glassRow["Resv05"]));
                        this.ddlGlassBatch.SelectedIndex = 0;
                        this.ddlGlassBatch.Enabled = false;

                        this.ddltxtGlassCode.Items.Add(Convert.ToString(glassRow["MaterialCode"]));
                        this.ddltxtGlassCode.SelectedIndex = 0;
                        this.ddltxtGlassCode.Enabled = false;
                    }
                }
                // EVA
                if (this.ddlEVABatch.Items.Count < 1)
                {
                    var evaRows = bom.Select("MaterialCategory='EVA' AND Resv04='1'");
                    if (evaRows != null && evaRows.Length > 0)
                    {
                        var evaRow = evaRows[0];
                        this.ddlEVABatch.Items.Add(Convert.ToString(evaRow["Resv05"]));
                        this.ddlEVABatch.SelectedIndex = 0;
                        this.ddlEVABatch.Enabled = false;

                        this.ddltxtEVACode.Items.Add(Convert.ToString(evaRow["MaterialCode"]));
                        this.ddltxtEVACode.SelectedIndex = 0;
                        this.ddltxtEVACode.Enabled = false;
                    }
                }
                // TPT
                if (this.ddlTPTBatch.Items.Count < 1)
                {
                    var tptRows = bom.Select("MaterialCategory='TPT' AND Resv04='1'");
                    if (tptRows != null && tptRows.Length > 0)
                    {
                        var tptRow = tptRows[0];
                        this.ddlTPTBatch.Items.Add(Convert.ToString(tptRow["Resv05"]));
                        this.ddlTPTBatch.SelectedIndex = 0;
                        this.ddlTPTBatch.Enabled = false;

                        this.ddltxtTPTCode.Items.Add(Convert.ToString(tptRow["MaterialCode"]));
                        this.ddltxtTPTCode.SelectedIndex = 0;
                        this.ddltxtTPTCode.Enabled = false;
                    }
                }
            }
            #endregion
        }
        #endregion

        private void ddlPackingPattern_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPackingPattern.SelectedIndex == 0)
                PackingPatternValue = "";
            else if (this.ddlPackingPattern.SelectedIndex == 1)
                PackingPatternValue = "A";
            else if (this.ddlPackingPattern.SelectedIndex == 2)
                PackingPatternValue = "B";
            else if (this.ddlPackingPattern.SelectedIndex == 3)
                PackingPatternValue = "C";
            else if (this.ddlPackingPattern.SelectedIndex == 4)
                PackingPatternValue = "D";
            else if (this.ddlPackingPattern.SelectedIndex == 5)
                PackingPatternValue = "E";
        }

        private void ddlIsOnlyPacking_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlIsOnlyPacking.SelectedIndex == 0)
                _IsOnlyPacking = "";
            else if (this.ddlIsOnlyPacking.SelectedIndex == 1)
                _IsOnlyPacking = "2";
            else if (this.ddlIsOnlyPacking.SelectedIndex == 2)
                _IsOnlyPacking = "1";
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        #region
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.txtWO1.Enabled = this.checkBox2.Checked;
            this.pictureBox2.Enabled = this.checkBox2.Checked;
            this.txtWO1.Clear();
            this.txtWO1.Focus();
        }

        private void txtWO1_Leave(object sender, EventArgs e)
        {
            //判断是否有外协工单
            if (!this.txtWO1.Text.Trim().ToString().Equals(""))
            {
                DataTable dt = ProductStorageDAL.GetWoMasterIno(this.txtWO1.Text.Trim());
                if (dt == null || dt.Rows.Count == 0)
                {
                    if (this.checkBox2.Checked)
                    {
                        ToolsClass.Log("输入的外协工单：" + this.txtWO1.Text.Trim() + " 没有从SAP下载", "ABNORMAL", lstView);
                        this.txtWO1.Clear();
                        this.txtWO1.Focus();
                        return;
                    }
                }
                else
                {
                    DataRow row = dt.Rows[0];
                    this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
                    this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.txtlocation.Text = Convert.ToString(row["RESV04"]);
                    this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
                    this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
                    this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
                }
            }
            else
            {
                if (this.checkBox2.Checked)
                {
                    ToolsClass.Log("请输入外协工单号码", "ABNORMAL", lstView);
                    this.txtWO1.Focus();
                }
            }
        }

        private void txtWO1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                pictureBox2_Click(null, null);
            }
        }

        private void txtWO1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                txtWO1_Leave(null, null);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), "");
            frm.ShowDialog();
            this.txtWO1.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
        }
        #endregion

    }
}
