﻿namespace CSICPR
{
    partial class FormWoQueryReport    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_ORDER_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_PRODUCT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_WO_QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_PLANSTART_DATETIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_PLANCOMPLETE_DATETIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TWO_WORKSHOP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnQuery = new System.Windows.Forms.Button();
            this.ddlQueryType = new System.Windows.Forms.ComboBox();
            this.lblCartonQuery = new System.Windows.Forms.Label();
            this.PalWoDate = new System.Windows.Forms.Panel();
            this.lblPackingFrom = new System.Windows.Forms.Label();
            this.dtpStorageDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblPackingTo = new System.Windows.Forms.Label();
            this.dtpStorageDateTo = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.PalWo = new System.Windows.Forms.Panel();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PalWoDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PalWo.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowIndex,
            this.TWO_NO,
            this.TWO_ORDER_NO,
            this.TWO_ORDER_ITEM,
            this.TWO_PRODUCT_NAME,
            this.TWO_WO_QTY,
            this.TWO_PLANSTART_DATETIME,
            this.TWO_PLANCOMPLETE_DATETIME,
            this.TWO_WORKSHOP});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 88);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 16;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1004, 484);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // RowIndex
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RowIndex.DefaultCellStyle = dataGridViewCellStyle3;
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 50;
            // 
            // TWO_NO
            // 
            this.TWO_NO.HeaderText = "工单号";
            this.TWO_NO.Name = "TWO_NO";
            this.TWO_NO.ReadOnly = true;
            // 
            // TWO_ORDER_NO
            // 
            this.TWO_ORDER_NO.HeaderText = "销售订单号";
            this.TWO_ORDER_NO.Name = "TWO_ORDER_NO";
            this.TWO_ORDER_NO.ReadOnly = true;
            // 
            // TWO_ORDER_ITEM
            // 
            this.TWO_ORDER_ITEM.HeaderText = "销售订单项目";
            this.TWO_ORDER_ITEM.Name = "TWO_ORDER_ITEM";
            this.TWO_ORDER_ITEM.ReadOnly = true;
            // 
            // TWO_PRODUCT_NAME
            // 
            this.TWO_PRODUCT_NAME.HeaderText = "产品代码";
            this.TWO_PRODUCT_NAME.Name = "TWO_PRODUCT_NAME";
            this.TWO_PRODUCT_NAME.ReadOnly = true;
            // 
            // TWO_WO_QTY
            // 
            this.TWO_WO_QTY.HeaderText = "工单数量";
            this.TWO_WO_QTY.Name = "TWO_WO_QTY";
            this.TWO_WO_QTY.ReadOnly = true;
            this.TWO_WO_QTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TWO_WO_QTY.Width = 160;
            // 
            // TWO_PLANSTART_DATETIME
            // 
            this.TWO_PLANSTART_DATETIME.HeaderText = "计划开始日期";
            this.TWO_PLANSTART_DATETIME.Name = "TWO_PLANSTART_DATETIME";
            this.TWO_PLANSTART_DATETIME.ReadOnly = true;
            // 
            // TWO_PLANCOMPLETE_DATETIME
            // 
            this.TWO_PLANCOMPLETE_DATETIME.HeaderText = "计划结束日期";
            this.TWO_PLANCOMPLETE_DATETIME.Name = "TWO_PLANCOMPLETE_DATETIME";
            this.TWO_PLANCOMPLETE_DATETIME.ReadOnly = true;
            this.TWO_PLANCOMPLETE_DATETIME.Width = 160;
            // 
            // TWO_WORKSHOP
            // 
            this.TWO_WORKSHOP.HeaderText = "车间";
            this.TWO_WORKSHOP.Name = "TWO_WORKSHOP";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(284, 50);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 22);
            this.btnQuery.TabIndex = 168;
            this.btnQuery.Tag = "button1";
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // ddlQueryType
            // 
            this.ddlQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlQueryType.FormattingEnabled = true;
            this.ddlQueryType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ddlQueryType.Location = new System.Drawing.Point(93, 17);
            this.ddlQueryType.Name = "ddlQueryType";
            this.ddlQueryType.Size = new System.Drawing.Size(148, 20);
            this.ddlQueryType.TabIndex = 169;
            this.ddlQueryType.SelectedIndexChanged += new System.EventHandler(this.ddlQueryType_SelectedIndexChanged);
            // 
            // lblCartonQuery
            // 
            this.lblCartonQuery.AutoSize = true;
            this.lblCartonQuery.Location = new System.Drawing.Point(16, 20);
            this.lblCartonQuery.Name = "lblCartonQuery";
            this.lblCartonQuery.Size = new System.Drawing.Size(71, 12);
            this.lblCartonQuery.TabIndex = 170;
            this.lblCartonQuery.Text = "查 询 类 型";
            // 
            // PalWoDate
            // 
            this.PalWoDate.Controls.Add(this.lblPackingFrom);
            this.PalWoDate.Controls.Add(this.dtpStorageDateFrom);
            this.PalWoDate.Controls.Add(this.lblPackingTo);
            this.PalWoDate.Controls.Add(this.dtpStorageDateTo);
            this.PalWoDate.Location = new System.Drawing.Point(11, 46);
            this.PalWoDate.Name = "PalWoDate";
            this.PalWoDate.Size = new System.Drawing.Size(261, 36);
            this.PalWoDate.TabIndex = 171;
            // 
            // lblPackingFrom
            // 
            this.lblPackingFrom.AutoSize = true;
            this.lblPackingFrom.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingFrom.Location = new System.Drawing.Point(2, 6);
            this.lblPackingFrom.Name = "lblPackingFrom";
            this.lblPackingFrom.Size = new System.Drawing.Size(21, 14);
            this.lblPackingFrom.TabIndex = 0;
            this.lblPackingFrom.Text = "从";
            // 
            // dtpStorageDateFrom
            // 
            this.dtpStorageDateFrom.CustomFormat = "yyyy-MM-dd";
            this.dtpStorageDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStorageDateFrom.Location = new System.Drawing.Point(24, 3);
            this.dtpStorageDateFrom.Name = "dtpStorageDateFrom";
            this.dtpStorageDateFrom.Size = new System.Drawing.Size(100, 21);
            this.dtpStorageDateFrom.TabIndex = 5;
            // 
            // lblPackingTo
            // 
            this.lblPackingTo.AutoSize = true;
            this.lblPackingTo.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingTo.Location = new System.Drawing.Point(133, 7);
            this.lblPackingTo.Name = "lblPackingTo";
            this.lblPackingTo.Size = new System.Drawing.Size(21, 14);
            this.lblPackingTo.TabIndex = 6;
            this.lblPackingTo.Text = "到";
            // 
            // dtpStorageDateTo
            // 
            this.dtpStorageDateTo.CustomFormat = "yyyy-MM-dd";
            this.dtpStorageDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStorageDateTo.Location = new System.Drawing.Point(156, 4);
            this.dtpStorageDateTo.Name = "dtpStorageDateTo";
            this.dtpStorageDateTo.Size = new System.Drawing.Size(104, 21);
            this.dtpStorageDateTo.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.ddlQueryType);
            this.panel1.Controls.Add(this.lblCartonQuery);
            this.panel1.Controls.Add(this.PalWoDate);
            this.panel1.Controls.Add(this.PalWo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 88);
            this.panel1.TabIndex = 174;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(367, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 22);
            this.button2.TabIndex = 178;
            this.button2.Tag = "button1";
            this.button2.Text = "清空";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(465, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 12);
            this.label4.TabIndex = 177;
            // 
            // PalWo
            // 
            this.PalWo.Controls.Add(this.txtWo);
            this.PalWo.Controls.Add(this.label1);
            this.PalWo.Location = new System.Drawing.Point(15, 48);
            this.PalWo.Name = "PalWo";
            this.PalWo.Size = new System.Drawing.Size(259, 32);
            this.PalWo.TabIndex = 173;
            // 
            // txtWo
            // 
            this.txtWo.Location = new System.Drawing.Point(79, 3);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(148, 21);
            this.txtWo.TabIndex = 94;
            this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 94;
            this.label1.Text = "工   单  号";
            // 
            // FormWoQueryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 572);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormWoQueryReport";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0002";
            this.Text = "工单查询";
            this.Load += new System.EventHandler(this.FormWoQueryReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PalWoDate.ResumeLayout(false);
            this.PalWoDate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PalWo.ResumeLayout(false);
            this.PalWo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox ddlQueryType;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalWoDate;
        private System.Windows.Forms.Label lblPackingFrom;
        private System.Windows.Forms.DateTimePicker dtpStorageDateFrom;
        private System.Windows.Forms.Label lblPackingTo;
        private System.Windows.Forms.DateTimePicker dtpStorageDateTo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PalWo;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_ORDER_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_PRODUCT_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_WO_QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_PLANSTART_DATETIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_PLANCOMPLETE_DATETIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn TWO_WORKSHOP;
    }
}