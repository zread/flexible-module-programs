﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormStorageReport : Form
    {
        #region 私有变量
        /// <summary>
        /// 查询类型
        /// </summary>
        private string QueryType = "";
        /// <summary>
        /// 工单号
        /// </summary>
        private string Wo = "";
        /// <summary>
        /// 托号
        /// </summary>
        private string Carton = "";
        /// <summary>
        /// 入库数据上传开始日期
        /// </summary>
        private DateTime StorageDateFrom;
        /// <summary>
        /// 入库数据上传结束日期
        /// </summary>
        private DateTime StorageDateTo;
        #endregion

        private static List<LanguageItemModel> LanMessList;
        private static FormStorageReport theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormStorageReport();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }

        #region 私有方法
        private void ClearDataGridViewData()
        {
            this.dataGridView2.DataSource = null;

            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            
        }
        private void SetDataGridViewData(string type)
        {
            if (QueryType.Equals("Carton"))
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    MessageBox.Show("托号不能为空,请输入！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.txtCarton.Focus();
                    return;
                }
                else
                    Carton = Convert.ToString(this.txtCarton.Text.Trim());
            }
            else if (QueryType.Equals("Wo"))
            {
                if (Convert.ToString(this.txtWo.Text.Trim()).Equals(""))
                {
                    MessageBox.Show("工单不能为空,请输入！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.txtWo.Focus();
                    return;
                }
                else
                    Wo = Convert.ToString(this.txtWo.Text.Trim());
            }
            else if (QueryType.Equals("StorageDate"))
            {
                StorageDateFrom = Convert.ToDateTime(this.dtpStorageDateFrom.Value);
                StorageDateTo = Convert.ToDateTime(this.dtpStorageDateTo.Value);
            }
				
//            string mysql = "select order_no as OrderNo,carton_no as CartonNo,carton_no as CustomerCartonNo, last_update_date as PostedOn , process_flag as IsCancelPacking, lid as Operator,create_date as OperatorDate,market as Workshop from stockin where carton_no ='164120400684'"
//            DataTable dt = 
            DataTable dt = ProductStorageDAL.GetStorageInfo(QueryType, Carton, StorageDateFrom, StorageDateTo, Wo);
            DataTable dt_detail = ProductStorageDAL.GetStorageDetailLogInfo(QueryType, Carton, StorageDateFrom, StorageDateTo, Wo);
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["OrderNo"].Value = Convert.ToString(row["OrderNo"]);
                    this.dataGridView1.Rows[RowNo].Cells["CartonNo"].Value = Convert.ToString(row["CartonNo"]);
                    this.dataGridView1.Rows[RowNo].Cells["CustomerCartonNo"].Value = Convert.ToString(row["CustomerCartonNo"]);
                    this.dataGridView1.Rows[RowNo].Cells["PostedOn"].Value = Convert.ToString(row["PostedOn"]);
                    if (Convert.ToString(row["IsCancelPacking"]).Equals("1"))
                        this.dataGridView1.Rows[RowNo].Cells["IsCancelPacking"].Value = "正常入库";
                    else if (Convert.ToString(row["IsCancelPacking"]).Equals("2"))
                        this.dataGridView1.Rows[RowNo].Cells["IsCancelPacking"].Value = "撤销入库";
                    this.dataGridView1.Rows[RowNo].Cells["Operator"].Value = Convert.ToString(row["Operator"]);
                    this.dataGridView1.Rows[RowNo].Cells["OperatorDate"].Value = Convert.ToString(row["OperatorDate"]);
                    this.dataGridView1.Rows[RowNo].Cells["Workshop"].Value = Convert.ToString(row["Workshop"]);
                    RowNo++;
                    RowIndex++;
                }
                // this.label4.Text = "共查询到 " + (dt.Rows.Count) + " 笔数据";
                this.label4.Text = string.Format(LanguageHelper.GetMessage(LanMessList, "Message2", "共查询到{0}笔数据"),dt.Rows.Count);
                   
               
                 if (QueryType.Equals("Carton"))
                 {
                     this.txtCarton.Clear();
                     this.txtCarton.Focus();
                 }
                 else if (QueryType.Equals("Wo"))
                 {
                     this.txtWo.Clear();
                     this.txtWo.Focus();
                 }
                 else if (QueryType.Equals("StorageDate"))
                 {
                     this.dtpStorageDateFrom.Focus();
                 }
                 if (dt_detail != null && dt_detail.Rows.Count > 0)
                     this.dataGridView2.DataSource = dt_detail;
            }
            else
            {
                 MessageBox.Show("没有查询到数据！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 if (QueryType.Equals("Carton"))
                     this.txtCarton.SelectAll();
                 else if (QueryType.Equals("Wo"))
                     this.txtWo.SelectAll();
                 else if (QueryType.Equals("StorageDate"))
                     this.dtpStorageDateFrom.Focus();
                // this.label4.Text = "共查询到 0 笔数据";
                 this.label4.Text = LanguageHelper.GetMessage(LanMessList, "Message1", "共查询到0笔数据");
            }
        }
        #endregion

        #region 窗体事件
        public FormStorageReport()
        {
            InitializeComponent();
        }
        private void FormCartonInfoQuery_Load(object sender, EventArgs e)
        {
            //查询类型
            this.ddlQueryType.Items.Insert(0, "入库上传日期");
            this.ddlQueryType.Items.Insert(1, "内部托号");
            this.ddlQueryType.Items.Insert(2, "工单号");
            //默认查询类型
            this.ddlQueryType.SelectedIndex = 1;
            this.PalStorageDate.Visible = false;
            this.PalCarton.Visible = true;
            QueryType = "Carton";
            this.txtCarton.Focus();
            //this.label4.Text = "共查询到 0 笔数据";
          

            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            this.label4.Text = LanguageHelper.GetMessage(LanMessList, "Message1", "共查询到0笔数据");
            LanguageHelper.getNames(this);
            //cbQueryTerm 多语言
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);

            # endregion
        }

        #endregion

        #region 窗体页面事件
        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearDataGridViewData();

            if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalStorageDate.Visible = true;
                this.PalCarton.Visible = false;
                this.PalWo.Visible = false;
                this.dtpStorageDateFrom.Focus();
                QueryType = "StorageDate";
            }
            else if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalStorageDate.Visible = false;
                this.PalCarton.Visible = true;
                this.PalWo.Visible = false;
                this.txtCarton.Clear();
                this.txtCarton.Focus();
                QueryType = "Carton";
            }
            else
            {
                this.PalStorageDate.Visible = false;
                this.PalCarton.Visible = false;
                this.PalWo.Visible = true;
                this.txtWo.Clear();
                this.txtWo.Focus();
                QueryType = "Wo";
            }
        }
       
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData(QueryType);
        }
        #endregion

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 2))
            {
                string OrderNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["OrderNo"].Value);
                string CartonNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CartonNo"].Value);
                string IsCancelPacking = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["IsCancelPacking"].Value);
                string CustomerCartonNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CustomerCartonNo"].Value);
                string Workshop = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["Workshop"].Value);
                string PostedOn = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["PostedOn"].Value);
                string Operator = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["Operator"].Value);
                string OperatorDate = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["OperatorDate"].Value);
                if (IsCancelPacking.Equals("正常入库"))
                    IsCancelPacking = "1";
                else if (IsCancelPacking.Equals("撤销入库"))
                    IsCancelPacking = "2";

                DataTable dt = ProductStorageDAL.GetStorageDetailInfo(OrderNo, CartonNo, IsCancelPacking,
                    CustomerCartonNo, Workshop, PostedOn, Operator, OperatorDate);
                if (dt!=null && dt.Rows.Count>0)
                {
                    int count = dt.Rows.Count;
                    var frm = new FormStorageReportDetail(dt,count);
                    if (frm.ShowDialog() != DialogResult.No)
                            frm.Show();
                    if (frm != null)
                    {
                        frm.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("没有获取到组件信息","信息提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ToolsClass.DataToExcel(this.dataGridView2, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            this.label4.Text = "共查询到 0 笔数据";
        }            
    }
}
