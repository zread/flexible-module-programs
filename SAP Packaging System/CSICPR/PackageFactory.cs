﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using LablePLibrary;

namespace CSICPR
{
    #region"abstract class"

    abstract class absPackageFactory
    {
        abstract public absCAMOPackage CreateCAMOPackage();
        abstract public absChangShuPackage CreateChangShuPackage();
    }

    abstract class absCAMOPackage
    {
        abstract public string getBoxID(string sStationNo,string sCellColor,string sYear="",string sDayOfYear="");
        abstract public bool checkCartonClass(string sModuleClass);
        abstract public string largeLabelCodeSoft(string labFilePath, LargeLabel largelabel, int pnum);
    }

    abstract class absChangShuPackage
    {
        abstract public string getBoxID(string sFirstCode, string sSerialCode, string sThirdCode, int iSerialCount);
        abstract public bool checkCTM(string sModuleNo);
        abstract public string largeLabelExcel(string sModuleNo);
    }

    #endregion

    #region"Real Class"

    class CAMOPackage : absCAMOPackage
    {
        private object lockGenerateBoxID;
        /// <summary>
        /// getBoxID,Generate BoxID
        /// </summary>
        /// <param name="sStationNo"></param>
        /// <param name="sCellColor"></param>
        /// <param name="sYear"></param>
        /// <param name="sDayOfYear"></param>
        /// <returns>BoxID</returns>
        public override string getBoxID(string sStationNo, string sCellColor, string sYear = "", string sDayOfYear = "")
        {
            string sboxid = "",strsql="select * from BoxCodeList where BoxID='{0}' and IsUsed<>'N'";
            bool isUse = true;
            int iSerial = int.Parse(ToolsClass.getConfig("SerialCode"));
            try
            {
                DateTime dt = Convert.ToDateTime(ToolsClass.getConfig("SystemDate"));
                if (ToolsClass.DateDiff(ToolsClass.DateInterval.Day, dt, DateTime.Now.Date) > 0)
                {
                    ToolsClass.saveXMLNode("SystemDate", DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss"));
                    iSerial = 1;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Get System Date Fail:"+e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            lockGenerateBoxID = new object();
            if(sYear.Equals(""))
                sYear = DateTime.Now.Year.ToString().Substring(2,2);
            if (sDayOfYear.Equals(""))
                sDayOfYear = DateTime.Now.DayOfYear.ToString().PadLeft(ToolsClass.iDate_Length, '0');

            while (isUse)
            {
                if (iSerial > 999)
                {
                    MessageBox.Show("Carton Serial Number Has Run Out!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return "";
                }
                sboxid = ToolsClass.WorkShop + sStationNo + sYear + sDayOfYear + iSerial.ToString().PadLeft(ToolsClass.iSerial_Length, '0') + sCellColor;
                if (ToolsClass.ExecuteQuery(string.Format(strsql, sboxid)) > 0)
                    isUse = true;
                else
                    isUse = false;
                iSerial++;
            }
            ToolsClass.saveXMLNode("SerialCode", iSerial.ToString());
            lock (lockGenerateBoxID)//Lock
            {
                ToolsClass.saveBoxCodeList(sboxid);
                strsql = "update BoxCodeList set IsUsed='W' where BoxID='" + sboxid + "'";
                ToolsClass.ExecuteNonQuery(strsql);
            }
            return sboxid;
        }

        /// <summary>
        /// checkModuleClass,verify module grade
        /// </summary>
        /// <param name="sModuleNo"></param>
        /// <returns>True:Grade(A),False:Grade(A1,A2)</returns>
        public override bool checkCartonClass(string sModuleClass)
        {
            throw new NotImplementedException();
        }

        public override string largeLabelCodeSoft(string labFilePath, LargeLabel largelabel, int pnum)
        {
            //throw new NotImplementedException();
            return LablePrint.LargeLabelPrint(labFilePath, largelabel, pnum);
        }
    }

    class ChangShuPackage : absChangShuPackage
    {
        private object lockGenerateBoxID;

        public override bool checkCTM(string sModuleNo)
        {
            throw new NotImplementedException();
        }

        public override string getBoxID(string sFirstCode, string sSerialCode, string sThirdCode, int iSerialCount)
        {
            string sboxid = "", strsql = "select * from BoxCodeList where BoxID='{0}' and IsUsed<>'N'";
            bool isUse = true;
            string sInitSerial = ToolsClass.getConfig("SerialCode", true, "initCnt").Trim();
            int iSerialCode = int.Parse(sInitSerial);
            int iSerial = int.Parse(ToolsClass.getConfig("SerialCode"));

            lockGenerateBoxID = new object();

            while (isUse)
            {
                if (iSerial - iSerialCode > iSerialCount)
                {
                    MessageBox.Show("Carton Serial Number Has Run Out!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return "";
                }
                sboxid = sFirstCode + iSerial.ToString().PadLeft(sInitSerial.Length, '0') + sThirdCode;
                if (ToolsClass.ExecuteQuery(string.Format(strsql, sboxid)) > 0)
                    isUse = true;
                else
                    isUse = false;
                iSerial++;
            }

            ToolsClass.saveXMLNode("SerialCode", iSerial.ToString().PadLeft(sInitSerial.Length, '0'));
            lock (lockGenerateBoxID)//Lock
            {
                ToolsClass.saveBoxCodeList(sboxid);
                strsql = "update BoxCodeList set IsUsed='W' where BoxID='" + sboxid + "'";
                ToolsClass.ExecuteNonQuery(strsql);
            }
            return sboxid;
        }

        public override string largeLabelExcel(string sModuleNo)
        {
            throw new NotImplementedException();
        }
    }

    class clsPackage : absPackageFactory
    {
        public override absCAMOPackage CreateCAMOPackage()
        {
            return new CAMOPackage();
            //throw new NotImplementedException();
        }

        public override absChangShuPackage CreateChangShuPackage()
        {
            return new ChangShuPackage();
            //throw new NotImplementedException();
        }
    }

    class Package
    {
        private absCAMOPackage camo;
        private absChangShuPackage cs;

        public Package(absPackageFactory factory)
        {
            camo = factory.CreateCAMOPackage();
            cs = factory.CreateChangShuPackage();
        }

        public object returnObject(int i)
        {
            object obj = new object();
            switch (i)
            {
                case 1: 
                    obj= camo;
                    break;
                case 2:
                    obj= cs;
                    break;
            }
            return obj;
        }
    }
    #endregion

}
