﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormWoQueryReport : Form
    {
        #region 私有变量
        /// <summary>
        /// 查询类型
        /// </summary>
        private string QueryType = "";
        /// <summary>
        /// 工单号
        /// </summary>
        private string Wo = "";
        /// <summary>
        /// 托号
        /// </summary>
        private string Carton = "";
        /// <summary>
        /// 工单创建开始日期
        /// </summary>
        private DateTime WoDateFrom;
        /// <summary>
        /// 工单创建结束日期
        /// </summary>
        private DateTime WoDateTo;
        #endregion
        private static List<LanguageItemModel> LanMessList;
        private static FormWoQueryReport theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormWoQueryReport();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }

        #region 私有方法
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            
        }
        private void SetDataGridViewData(string type)
        {
            if (QueryType.Equals("WO"))
            {
                if (Convert.ToString(this.txtWo.Text.Trim()).Equals(""))
                {
                    MessageBox.Show("工单不能为空,请输入！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.txtWo.Focus();
                    return;
                }
                else
                    Wo = Convert.ToString(this.txtWo.Text.Trim());
            }
            else if (QueryType.Equals("WOCreateDate"))
            {
                WoDateFrom = Convert.ToDateTime(this.dtpStorageDateFrom.Value);
                WoDateTo = Convert.ToDateTime(this.dtpStorageDateTo.Value);
            }

            DataTable dt = ProductStorageDAL.GetWoInfoFromSAPInFo(QueryType, Wo, WoDateFrom, WoDateTo);
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["TWO_NO"].Value = Convert.ToString(row["TWO_NO"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_ORDER_NO"].Value = Convert.ToString(row["TWO_ORDER_NO"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_ORDER_ITEM"].Value = Convert.ToString(row["TWO_ORDER_ITEM"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_PRODUCT_NAME"].Value = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_WO_QTY"].Value = Convert.ToString(row["TWO_WO_QTY"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_PLANSTART_DATETIME"].Value = Convert.ToString(row["TWO_PLANSTART_DATETIME"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_PLANCOMPLETE_DATETIME"].Value = Convert.ToString(row["TWO_PLANCOMPLETE_DATETIME"]);
                    this.dataGridView1.Rows[RowNo].Cells["TWO_WORKSHOP"].Value = Convert.ToString(row["TWO_WORKSHOP"]);
                    RowNo++;
                    RowIndex++;
                }
                 //this.label4.Text = "共查询到 " + (dt.Rows.Count) + " 笔数据";
                this.label4.Text = string.Format(LanguageHelper.GetMessage(LanMessList, "Message2", "共查到{0}笔数据"), dt.Rows.Count);
                 if (QueryType.Equals("Wo"))
                 {
                     this.txtWo.Clear();
                     this.txtWo.Focus();
                 }
                 else if (QueryType.Equals("WOCreateDate"))
                 {
                     this.dtpStorageDateFrom.Focus();
                 }
            }
            else
            {
                 MessageBox.Show("没有查询到数据！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 if (QueryType.Equals("Wo"))
                     this.txtWo.SelectAll();
                 else if (QueryType.Equals("StorageDate"))
                     this.dtpStorageDateFrom.Focus();
                // this.label4.Text = "共查询到 0 笔数据";
                 this.label4.Text = LanguageHelper.GetMessage(LanMessList, "Message1", "共查询到 0 笔数据");
            }
        }
        #endregion

        #region 窗体事件
        public FormWoQueryReport()
        {
            InitializeComponent();
        }
        private void FormWoQueryReport_Load(object sender, EventArgs e)
        {
            //查询类型
            this.ddlQueryType.Items.Insert(0, "工单创建日期");
            this.ddlQueryType.Items.Insert(1, "工单号");
            //默认查询类型
            this.ddlQueryType.SelectedIndex = 1;
            this.PalWoDate.Visible = false;
            this.PalWo.Visible = true;
            QueryType = "WO";
            //this.label4.Text = "共查询到 0 笔数据";
          


            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            this.label4.Text = LanguageHelper.GetMessage(LanMessList, "Message1", "共查询到 0 笔数据");
            LanguageHelper.getNames(this);
            //cbQueryTerm 多语言
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);
            # endregion
        }

        #endregion

        #region 窗体页面事件
        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearDataGridViewData();

            if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalWoDate.Visible = true;
                this.PalWo.Visible = false;
                this.dtpStorageDateFrom.Focus();
                QueryType = "WOCreateDate";
            }
            else if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalWoDate.Visible = false;
                this.PalWo.Visible = true;
                this.txtWo.Clear();
                this.txtWo.Focus();
                QueryType = "WO";
            }
        }
       
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData(QueryType);
        }
        #endregion

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 2))
            {
                string OrderNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["OrderNo"].Value);
                string CartonNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CartonNo"].Value);
                string IsCancelPacking = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["IsCancelPacking"].Value);
                string CustomerCartonNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CustomerCartonNo"].Value);
                string Workshop = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["Workshop"].Value);
                string PostedOn = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["PostedOn"].Value);
                string Operator = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["Operator"].Value);
                string OperatorDate = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["OperatorDate"].Value);
                if (IsCancelPacking.Equals("正常入库"))
                    IsCancelPacking = "1";
                else if (IsCancelPacking.Equals("撤销入库"))
                    IsCancelPacking = "2";

                DataTable dt = ProductStorageDAL.GetStorageDetailInfo(OrderNo, CartonNo, IsCancelPacking,
                    CustomerCartonNo, Workshop, PostedOn, Operator, OperatorDate);
                if (dt!=null && dt.Rows.Count>0)
                {
                    int count = dt.Rows.Count;
                    var frm = new FormStorageReportDetail(dt,count);
                    if (frm.ShowDialog() != DialogResult.No)
                            frm.Show();
                    if (frm != null)
                    {
                        frm.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("没有获取到组件信息","信息提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            //this.label4.Text = "共查询到 0 笔数据";
            this.label4.Text = LanguageHelper.GetMessage(LanMessList, "Message1", "共查询到 0 笔数据");
        }            
    }
}
