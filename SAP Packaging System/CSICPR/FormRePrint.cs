﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormRePrint : Form
    {
        private bool isModel, isTrueP, hasPar2;
        private FormPacking fp;
        public FormRePrint(FormPacking fm, bool Par2, bool md, bool tp,string sn="")
        {
            InitializeComponent();
            this.hasPar2 = Par2;
            this.fp = fm;
            this.isModel = md;
            this.isTrueP = tp;

            if (sn != "")
            {
                textBox1.Text = sn;
                comboBox1.SelectedIndex = 1;
            }
        }

        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        private void label14_DoubleClick(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btQuery_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex< 0)
            {
                FormMain.showTipMSG("Please Select Query Option!", this.comboBox1, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            if (textBox1.Text.Trim().Length < 8)
            {
                FormMain.showTipMSG("Please input correct data", this.textBox1, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            
            //清除当前显示内容
            dataGridView1.Rows.Clear();
            if (comboBox1.SelectedIndex == 0)
            {
                button1.Enabled = true;
            }
            //显示数据
            using (SqlDataReader read = ToolsClass.GetDataReader(string.Format(tbBoxHandingQuerySQL.Text, textBox1.Text)))
            {
                if (read != null)
                {
                    string sn="",temp="";
                    string nPower = "0";
                    while (read.Read())
                    {
                        if (this.comboBox1.SelectedIndex == 0)
                        {
                            temp = read.GetString(0);
                            if (sn == temp)
                                continue;
                            else
                                sn = temp;
                        }
                        nPower = ToolsClass.getIdealPower(read.GetDouble(2), read.GetString(0).Trim()); 
                        dataGridView1.Rows.Add(read.GetString(0), read.GetString(1), "", read.GetDouble(2), nPower, read.GetString(7), read.GetString(8));
                    }
                    button1.Enabled = true;
                }
            }
            if (dataGridView1.Rows.Count == 0)
                MessageBox.Show("No Data to be query!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); ;
            
        }
       

        private void btSaveSQL_Click(object sender, EventArgs e)
        {
            //保存组件装箱报表sql语句 tbCPTBoxSQL
            ToolsClass.saveSQL(tbBoxHandingQuerySQL);
            splitContainer1.Panel2Collapsed = true;
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbBoxHandingQuerySQL.Undo();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
                tbBoxHandingQuerySQL.Name = "tbBoxHandingQuerySQL";
            else
                tbBoxHandingQuerySQL.Name = "tbCPTHandingQuerySQL";
            tbBoxHandingQuerySQL.Text = ToolsClass.getSQL(tbBoxHandingQuerySQL.Name);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0 && textBox1.TextLength < 8)
            {
                FormMain.showTipMSG("No Data can be print", this.label1, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            string[] sModelTypeSet=null;
            string[] sModelType = null;
            string[,] codeArr = null;
            string model = "";
            if (dataGridView1.RowCount > 0)
            {
                
                 codeArr = new string[dataGridView1.RowCount, 2];
                 sModelTypeSet = new string[dataGridView1.RowCount];
                 sModelType = new string[dataGridView1.RowCount];
                 int i = 0, location;
                 foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        codeArr[i, 0] = row.Cells[0].Value.ToString();
                        sModelType[i] = dataGridView1[1, i].Value.ToString();
                        sModelTypeSet[i] = dataGridView1[1, i].Value.ToString().Insert(dataGridView1[1, i].Value.ToString().IndexOf('-') + 1,dataGridView1[4, i].Value.ToString());
                        if (this.hasPar2)
                        {
                            if (this.isModel)
                            {
                                model = row.Cells[1].Value.ToString();
                                location = model.IndexOf('-');
                                if (location == -1)
                                {
                                    model = model + "-" + row.Cells[4].Value.ToString();// +"P";
                                }
                                else
                                {
                                    model = model.Insert(location + 1, row.Cells[4].Value.ToString());
                                }

                            }
                            else if (this.isTrueP)
                            {
                                model = row.Cells[3].Value.ToString();
                            }
                            else
                            {
                                model = row.Cells[4].Value.ToString();
                            }
                        }
                        codeArr[i, 1] = model;
                        i++;
                    }
            }

            model = "";
            if (comboBox1.SelectedIndex == 0 && textBox1.TextLength > 7)
            {
                model = textBox1.Text;
            }
            model = fp.RePrint(codeArr, model, sModelTypeSet, sModelType);
            if (model.Length > 0)
            {
                MessageBox.Show("Print Label Exception: " + model, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

    }
}
