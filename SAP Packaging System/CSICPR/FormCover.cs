﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Collections.Generic;
using Microsoft.Win32;
using System.Threading;
using System.Data;

namespace CSICPR
{
    public partial class FormCover : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        /// <summary>
        /// 语言
        /// </summary>
        private static string vlanguage = "";
        public static string Language
        {
            get { return vlanguage; }
        }
        
        #region xml里配置的数据,以及数据库连接字符串
        /// <summary>
        /// xml里配置的数据库连接字符串
        /// </summary>
        public static string connStringBase;
        /// <summary>
        /// xml里配置的DataSource
        /// </summary>
        /// 
        public static string connStringOldPack;
        /// <summary>
        /// xml里配置的DataSource
        /// </summary>
        /// 
        private static string dbServer;
        /// <summary>
        /// xml里配置的DBName
        /// </summary>
        private static string dbName;
        /// <summary>
        /// xml里配置的UserID
        /// </summary>
        private static string dbUser;
        /// <summary>
        /// xml里配置的Password
        /// </summary>
        private static string dbPwd;
        /// <summary>
        /// 厂区代码
        /// </summary>
        public static string PlanCode = "";
        private static string currentFactory;
        /// <summary>
        /// xml里配置的车间名称
        /// </summary>
        public static string CurrentFactory
        {
            get { return currentFactory; }
            set { currentFactory = value; }
        }
        #endregion

        #region 集中数据库连接字符串以及服务器名字
        private static string _CenterDBConnString = "";
        /// <summary>
        /// 集中数据库连接字符串
        /// </summary>
        public static string CenterDBConnString
        {
            set { _CenterDBConnString = value; }
            get { return _CenterDBConnString; }
        }
        private static string _CenterDBName = "";
        /// <summary>
        /// 集中数据库所在服务器的名称
        /// </summary>
        public static string CenterDBName
        {
            set { _CenterDBName = value; }
            get { return _CenterDBName; }
        }
        #endregion

        #region ESB数据库连接字符串
        /// <summary>
        /// ESB数据库连接字符串
        /// </summary>
        public static string EsbConnString { get; set; }
        #endregion

        #region 接口数据库连接字符串以及服务器名字
        private static string _InterfaceConnString = "";
        /// <summary>
        /// 接口数据库连接字符串
        /// </summary>
        public static string InterfaceConnString
        {
            set { _InterfaceConnString = value; }
            get { return _InterfaceConnString; }
        }
        private static string _SAPDBName = "";
        /// <summary>
        /// 接口数据库服务器名字
        /// </summary>
        public static string SAPDBName
        {
            get { return _SAPDBName; }
            set { _SAPDBName = value; }
        }

        #endregion

        #region 包装数据库连接字符串以及名字
        /// <summary>
        /// 包装数据库连接字符串
        /// </summary>
        public static string connectionBase
        {
            get { return connStringBase; }
            set { connStringBase = value; }
        }
        public static string ConnectionOldPackaging
        {
        	get { return connStringOldPack; }
            set { connStringOldPack = value; }
        }
        private static string _PackingDBName = "";
        /// <summary>
        /// 包装数据库DB名字
        /// </summary>
        public static string PackingDBName
        {
            get { return _PackingDBName; }
            set { _PackingDBName = value; }
        }
        #endregion

        #region webservice address
        private static string _WebServiceAddress = "";
        /// <summary>
        /// 入库webservice地址
        /// </summary>
        public static string WebServiceAddress
        {
            get { return _WebServiceAddress; }
            set { _WebServiceAddress = value; }
        }
        #endregion

        private static string _ShowInputStorageFlag = "";
        /// <summary>
        /// 是否显示手动入库功能
        /// </summary>
        public static string ShowInputStorageFlag
        {
            get { return _ShowInputStorageFlag; }
            set { _ShowInputStorageFlag = value; }
        }

        private static string _WOTypeCode = "";
        /// <summary>
        /// 工单类型代码
        /// </summary>
        public static string WOTypeCode
        {
            get { return _WOTypeCode; }
            set { _WOTypeCode = value; }
        }


        #region 私有变量
        /// <summary>
        /// 窗体本身
        /// </summary>
        private FormLogin fLogin;
        private static Image bgImage;
        private bool bExit = true;

        public static string currUserName;
        private static string currUserWorkID;
        private static List<string> allRight;
        private static List<string> CtlTab;
        private static bool canUse;
        public static bool isManager;
        private static string curPWD;
        /// <summary>
        /// 权限检查
        /// </summary>
        /// <param name="excName">操作名称</param>
        /// <returns>bool 是否有权限</returns>
        public static bool HasPower(string excName, string sname = "")
        {
            if (allRight.Contains(excName)) return true;
            else
            {
               // MessageBox.Show("对不起，你没有权限使用“" + sName + "”!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (sname == "") {
                		MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "对不起，你没有权限使用") +"”!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               }
               else
               	MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "对不起，你没有权限使用")+ "”!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               
                return false;
            }
        }
         public static bool HasPowerControl(string excName, string sname = "")
        {
            if (CtlTab.Contains(excName)) return true;
            else
            {
            	//MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "对不起，你没有权限使用")+ "”!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
        /// <summary>
        /// 用户名称
        /// </summary>
        public static string CurrUserName
        {
            get { return currUserName; }
        }
        /// <summary>
        /// 用户工号
        /// </summary>
        public static string CurrUserWorkID
        {
            get { return currUserWorkID; }
        }
        /// <summary>
        /// 数据库服务器
        /// </summary>
        public static string DBServer
        {
            set { dbServer = value; }
            get { return dbServer; }
        }
        /// <summary>
        /// 数据库账号
        /// </summary>
        public static string DBUser
        {
            set { dbUser = value; }
            get { return dbUser; }
        }
        /// <summary>
        /// 数据库名
        /// </summary>
        public static string DBName
        {
            set { dbName = value; }
            get { return dbName; }
        }
        /// <summary>
        /// 数据库密码
        /// </summary>
        public static string DBPWD
        {
            set { dbPwd = value; }
            get { return dbPwd; }
        }

        /// <summary>
        /// MD5 32位加密
        /// </summary>
        public static string GetMd5Str(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str));
            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
        public static Image loginBackgroundImage
        {
            get { return bgImage; }
        }
        private void FormCover_Shown(object sender, EventArgs e)
        {
            fLogin.ShowDialog();
        }
        #endregion

        #region 私有方法
        private static bool verifyUser(string user, string pwd)
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    conn.Open();
                    currUserName = "";
                    canUse = false;
                    isManager = false;
                    cmd.CommandText = string.Format("SELECT [UserNM],[Status],[UserGroup] FROM [UserLogin] WHERE [UserNM]='{0}' AND [UserPW]='{1}'", user, pwd);//SELECT [Name],[Manager] FROM [Operator] WHERE [OperatorID]='{0}' AND [Password]='{1}'
                    System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader();
                    if (read.Read())
                    {
                        allRight = new List<string>();
                        CtlTab = new List<string>();
                        if (read.GetBoolean(1))
                        {
                            allRight.Add("btColose");
                            allRight.Add("Account Manager");
                            allRight.Add("packageConfigToolStripMenuItem");
                            allRight.Add("MenuItemBase");
                            allRight.Add("btTodayPackged");
                            //CtlTab.Add("BtReprintCartonLabel");
                            if (read.GetInt32(2) == 9)
                                allRight.Add("tssbCTM");
                            
                            
                        }
                        if (read.GetInt32(2) == 9)//admin
                        {
                            allRight.Add("tssbCTM");
                            allRight.Add("accountManagerToolStripMenuItem");
                            //Packing
                            allRight.Add("SNPackingToolStripMenuItem");
                            allRight.Add("btnBatchMode");
                            allRight.Add("btColose");
                            //downloading
                            allRight.Add("ToolStripMenuItemMaterial");
                            allRight.Add("ToolStripMenuItemOrderno");
                            allRight.Add("ToolStripMenuItemStorage");
                            //Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //material template
                            allRight.Add("MaterialTemplateToolStripMenuItem");
                            allRight.Add("MaterialToolStripMenuItem");    
                            //Registering rework
                            allRight.Add("toolStripMenuItemReworkRegister");
                            allRight.Add("ToolStripQualityInspection");
							//Reprint Label
							CtlTab.Add("BtReprintCartonLabel");
							//Change carton number
							CtlTab.Add("Change Carton Number");
							//Cell Tracibility
							allRight.Add("toolStripSplitButton4");
                            //Modify permission
                            CtlTab.Add("FormLineModify");
                            CtlTab.Add("TECO");
                            //Upload Retry
                            CtlTab.Add("BtRetry");
                        }
                        if(read.GetInt32(2) == 10)// supervisor
                        {
                        	//packing                        	
                        	allRight.Add("SNPackingToolStripMenuItem");
                            allRight.Add("btnBatchMode");
                            allRight.Add("btColose");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");                       	
                            //allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //Link to template
                            allRight.Add("MaterialToolStripMenuItem"); 
							//Reprint Label
							CtlTab.Add("BtReprintCartonLabel");	
							//Change carton number
							CtlTab.Add("Change Carton Number");							
                            
                        }
                        if(read.GetInt32(2) == 11)//comman user
                        {
                        	//packing
                        	allRight.Add("SNPackingToolStripMenuItem");
                          	//stock in
                            allRight.Add("NormalToolStripMenuItemm");                            
                            allRight.Add("toolStripMenuItemUploadSap");
                            //Link to template
                            allRight.Add("MaterialToolStripMenuItem");
                        
                        }
                        if(read.GetInt32(2) == 12)// MCT
                        {
                        	//downloading
                            allRight.Add("ToolStripMenuItemMaterial");
                            allRight.Add("ToolStripMenuItemOrderno");
                            allRight.Add("ToolStripMenuItemStorage");
                            //Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //material template
                            allRight.Add("MaterialTemplateToolStripMenuItem");
                            allRight.Add("MaterialToolStripMenuItem");    
							//Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");                            
                        	//new form cell tracibility
                        	allRight.Add("toolStripSplitButton4");
                            //Modify permission                          
                        	
                        }
                        if(read.GetInt32(2) == 13)//RWK
                        {
                        	//Packing
                            allRight.Add("SNPackingToolStripMenuItem");
                            allRight.Add("btnBatchMode");
                            allRight.Add("btColose");
                            //downloading
                            allRight.Add("ToolStripMenuItemMaterial");
                            allRight.Add("ToolStripMenuItemOrderno");
                            allRight.Add("ToolStripMenuItemStorage");
                            //Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //material template
                            allRight.Add("MaterialTemplateToolStripMenuItem");
                            allRight.Add("MaterialToolStripMenuItem");    
                            //Registering rework
                            allRight.Add("toolStripMenuItemReworkRegister");
                            allRight.Add("ToolStripQualityInspection"); 
							//Reprint Label
							CtlTab.Add("BtReprintCartonLabel");	
							// Rework Template
							CtlTab.Add("Rework");
							//new form cell tracibility
                        	allRight.Add("toolStripSplitButton4");
                            //Modify permission
                            CtlTab.Add("FormLineModify");
							
                        }                  
                         if(read.GetInt32(2) == 14)// MCT leader
                        {
                     		//downloading
                            allRight.Add("ToolStripMenuItemMaterial");
                            allRight.Add("ToolStripMenuItemOrderno");
                            allRight.Add("ToolStripMenuItemStorage");
                            //Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //material template
                            allRight.Add("MaterialTemplateToolStripMenuItem");
                            allRight.Add("MaterialToolStripMenuItem");    
							//Maintain work ordr
                            allRight.Add("ToolStripMenuItemMatainOrder");                            
                        	//new form cell tracibility
                        	allRight.Add("toolStripSplitButton4");
                            //Modify permission
                            CtlTab.Add("FormLineModify");
                           
                        }
                         if(read.GetInt32(2) == 16)// Packaging leader
                        {
                        	//packing
                        	allRight.Add("SNPackingToolStripMenuItem");
                            allRight.Add("btnBatchMode");
                            allRight.Add("btColose");
                            //stock in
                            allRight.Add("NormalToolStripMenuItemm");
                            allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");                       	
                            //allRight.Add("toolStripMenuItemCancel");
                            allRight.Add("toolStripMenuItemUploadSap");
                            //Link to template
                            allRight.Add("MaterialToolStripMenuItem"); 
                           
                        }
                        
                        
                        currUserName = read.GetString(0);
                        currUserWorkID = user;
                        curPWD = pwd;

                        //Todo 检查用户所有权限
                        if (read.GetInt32(2) == 9)
                            isManager = true;

                        read.Close();
                    }

                    if (currUserName == "")
                        return false;
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
        }

        public static bool checkUser(string user, string pwd)
        {
            if (curPWD == pwd && currUserWorkID == user)
                return true;
            else
                return false;
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
        /// <summary>
        /// 初始化参数
        /// </summary>
        public static bool getRegistryKeys()
        {
            //从XML里读取
            dbServer = ToolsClass.getConfig("DataSource", false, "", "config.xml");
            dbName = ToolsClass.getConfig("DBName", false, "", "config.xml");
            dbUser = ToolsClass.getConfig("UserID", false, "", "config.xml");
            dbPwd = ToolsClass.getConfig("Password", false, "", "config.xml");
            currentFactory = ToolsClass.getConfig("WorkShop", false, "", "config.xml");
            PlanCode = ToolsClass.getConfig("PlanCode", false, "", "config.xml");
            string cadbName = ToolsClass.getConfig("caDBName", false, "", "config.xml");
			string cadbUser = ToolsClass.getConfig("caUserID", false, "", "config.xml");
			string cadbPwd = ToolsClass.getConfig("caPassword", false, "", "config.xml");
            if (dbServer.Equals(""))
            {
               // MessageBox.Show("连接数据库的DataSource没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message6", "连接数据库的DataSource没有配置"));
                return false;
            }
            if (dbName.Equals(""))
            {
               // MessageBox.Show("连接数据库的DBName没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message7", "连接数据库的DBName没有配置"));
                return false;
            }
            if (dbUser.Equals(""))
            {
               // MessageBox.Show("连接数据库的UserID没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message8", "连接数据库的UserID没有配置"));
                return false;
            }
            if (dbPwd.Equals(""))
            {
                //MessageBox.Show("连接数据库的Password没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message9", "连接数据库的Password没有配置"));
                return false;
            }
            if (currentFactory.Equals(""))
            {
               // MessageBox.Show("连接数据库的WorkShop没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message10", "连接数据库的WorkShop没有配置"));
                return false;
            }
            if (PlanCode.Equals(""))
            {
                //MessageBox.Show("连接数据库的PlanCode没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message11", "连接数据库的PlanCode没有配置"));
                return false;
            }
            connStringBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
            ConnectionOldPackaging = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, cadbName, cadbUser, cadbPwd);
            return true;
        }

        public static bool saveRegistryKeys(string[,] keyValueArr)
        {
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"Software\SCI_solar\CPMSystem", RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                try
                {
                    for (int i = 0; i < keyValueArr.GetLength(0); i++)
                    {
                        Key.SetValue(keyValueArr[i, 0], keyValueArr[i, 1], RegistryValueKind.String);
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
            }

        }

        public static bool WriteValue(string ValueName, object ValueData)
        {
            if (ValueName.Length == 0) return false;

            try
            {
                RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"Software\SCI_solar\CPMSystem", RegistryKeyPermissionCheck.ReadWriteSubTree);
                Key.SetValue(ValueName, ValueData, RegistryValueKind.String);
                Key.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string ReadValue(string ValueName)
        {

            if (ValueName.Length == 0) return "";//[ERROR]
            try
            {
                RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"Software\SCI_solar\CPMSystem", false);
                string strKey = Key.GetValue(ValueName, "").ToString();
                Key.Close();
                return strKey;
            }
            catch
            {
                return "";//[ERROR]
            }
        }
        #endregion

        #region 窗体初始化界面
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="isLoading"></param>
        /// <param name="isLock"></param>
        public FormCover(bool isLoading, bool isLock = false)
        {
            InitializeComponent();
            fLogin = new FormLogin(isLock);
            fLogin.Owner = this;

            this.label2.Visible = isLoading;
            this.label3.Visible = isLoading;
            this.btLogin.Visible = isLoading;
            this.btQuit.Visible = isLoading;
            this.tbName.Visible = isLoading;
            this.tbPWD.Visible = isLoading;

            this.comboLangua.Text = ToolsClass.getConfig("Language", false, "", "");

            if (isLoading)
            {
                if (bgImage == null && System.IO.File.Exists("loginBG.png"))
                {
                    bgImage = Image.FromFile("loginBG.png");
                }
                if (bgImage != null) this.BackgroundImage = bgImage; ;
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                this.btQuit.Click += new System.EventHandler(this.btQuit_Click);
                this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
                this.tbPWD.Enter += new System.EventHandler(this.textBox_Enter);
                this.tbPWD.Leave += new System.EventHandler(this.textBox_Leave);
                this.tbName.Enter += new System.EventHandler(this.textBox_Enter);
                this.tbName.Leave += new System.EventHandler(this.textBox_Leave);
                this.TopMost = true;
                //getRegistryKeys();
            }
            else
            {
                this.Opacity = 0.7;
                this.BackColor = System.Drawing.SystemColors.GrayText;
                this.Shown += new System.EventHandler(this.FormCover_Shown);
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            }


        }

        public Mutex objMutex;
        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCover_Load(object sender, EventArgs e)
        {
            try
            {
               vlanguage= ToolsClass.getConfig("Language", false, "", "").ToUpper();
               if (vlanguage.Trim().Equals(""))
                   return;
                getRegistryKeys();
                # region 获取本车间库的连接
                ConfigString cfg = ProductStorageDAL.GetPackingSystemConfigInfo(PlanCode, currentFactory);
                if (cfg != null)
                {
                    dbServer = cfg.DataSource;
                    dbName = cfg.DatabaseName;
                    dbUser = cfg.LoginUserName;
                    dbPwd = cfg.LoginPassword;
                    _PackingDBName = cfg.DbLinkName;
                }
                //重新赋值
                connectionBase  = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
                # endregion

                # region 多语言
                LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
                LanguageHelper.getNames(this);
                # endregion

                #region 一个实际的车间对应多个车间
                getRegistryKeys();
                DataTable dt = ProductStorageDAL.GetMultiWorkShopMapping("MULTIWORKSHOP", currentFactory);
                if (dt != null && dt.Rows.Count == 1)
                {
                    DataRow row = dt.Rows[0];
                    if ((Convert.ToString(row["MAPPING_KEY_02"]).Equals("Y")))
                    {
                        string multiworkshop = Convert.ToString(row["MAPPING_KEY_03"]);
                        string[] array = multiworkshop.Split('-');
                        if (array.Length > 1)
                        {
                            for (int i = 0; i < array.Length; i++)
                            {
                                this.ddlPlantCode.Items.Insert(i, array[i]);
                            }
                        }
                    }
                    else
                    {
                        this.ddlPlantCode.Enabled = false;
                    }
                }
                else
                {
                    this.ddlPlantCode.Enabled = false;
                }
                #endregion

                ToolsClass.sSite = ToolsClass.getSQL("WorkShop", "config.xml").ToUpper();//2011-08-10|add by alex.dong|Get Real WorkShop

                objMutex = new Mutex(false, this.Text + "_Mutex");

                if (objMutex.WaitOne(0, false) == false)
                {
                    objMutex.Close();
                    objMutex = null;
                    //MessageBox.Show("一次只能打开一个包装系统!", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message12", "一次只能打开一个包装系统!"));
                    System.Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message) ;
                MessageBox.Show(ex.Message);
                this.Close();
               
            }
        }

        /// <summary>
        /// 窗体关闭时发生
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCover_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (bExit)
                System.Environment.Exit(0);
        }
        #endregion

        #region 界面事件
        /// <summary>
        /// 登陆按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btLogin_Click(object sender, EventArgs e)
        {
            if (!vlanguage.Equals(this.comboLangua.Text.Trim()))
            {
                //MessageBox.Show("你已更改语言,请重新打开程序!");
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message5", "你已更改语言,请重新打开程序!"));
                vlanguage = this.comboLangua.Text.Trim();
                ToolsClass.saveXMLNode("Language", this.comboLangua.Text.Trim(), false, "", "");

                Close();
            }          

            string user, pwd;
            user = this.tbName.Text.Trim();
            if (user.Length == 0)
            {
                //FormMain.showTipMSG("帐号不能为空!", this.tbName, "Prompting Message", ToolTipIcon.Warning);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message1","账号不能为空"));
                return;
            }
            bExit = false;
            pwd = this.tbPWD.Text.Trim();
            if (pwd.Length == 0)
            {
                //FormMain.showTipMSG("密码不能为空!", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message2", "密码不能为空"));
                return;
            }

            if ((this.ddlPlantCode.Enabled) && (this.ddlPlantCode.Text.Trim().Equals("")))
            {
                //MessageBox.Show("车间不能为空，请选择", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "车间不能为空"));
                return;
            }
            //根据用户选择，更新xml里的workshop

            if (!this.ddlPlantCode.Text.Trim().Equals(""))
            {
                ToolsClass.saveXMLNode("WorkShop", this.ddlPlantCode.Text.Trim().ToUpper(), false, "", "Config.xml");
            }

            //初始化参数
            if (!getRegistryKeys())
                return;

            //集中数据库连接字符串已经服务器的名字



            ConfigString cfg = ProductStorageDAL.GetCenterDBConfigInfo(PlanCode);
            if (cfg != null)
            {
                _CenterDBName = cfg.DbLinkName;
                _CenterDBConnString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", cfg.DataSource, cfg.DatabaseName, cfg.LoginUserName, cfg.LoginPassword);
            }

            cfg = ProductStorageDAL.GetEsbConfigInfo(PlanCode);
            if (cfg != null)
            {
                EsbConnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};charset=utf8;Allow User Variables=True;", cfg.DataSource, cfg.DatabaseName, cfg.LoginUserName, cfg.LoginPassword);
            }

            //接口数据库连接字符串已经服务器的名字
            cfg = ProductStorageDAL.GetInterfaceConfigInfo(PlanCode);
            if (cfg != null)
            {
                _SAPDBName = cfg.DbLinkName;
                _InterfaceConnString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", cfg.DataSource, cfg.DatabaseName, cfg.LoginUserName, cfg.LoginPassword);
            }



            if (!(ProductStorageDAL.GetSysMapping("PackingSystemStorageInterfaceService", currentFactory).Equals("")))
                _WebServiceAddress = ProductStorageDAL.GetSysMapping("PackingSystemStorageInterfaceService", currentFactory).Trim();
            else
            {
               // MessageBox.Show("获取入库的Webservice地址错误", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message13", "获取入库的Webservice地址错误"));
                return;
            }

            //是否显示手动入库功能
            if (!(ProductStorageDAL.GetSysMapping("SHOW-INPUT-STORAGE-FLAG", currentFactory).Equals("")))
                _ShowInputStorageFlag = ProductStorageDAL.GetSysMapping("SHOW-INPUT-STORAGE-FLAG", currentFactory).Trim();
            else
            {
               // MessageBox.Show("获取是否显示手动入库功能错误", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message14", "获取是否显示手动入库功能错误"));
                return;
            }
            # region
            //包装数据库连接字符串已经服务器的名字
            cfg = ProductStorageDAL.GetPackingSystemConfigInfo(PlanCode, currentFactory);
            if (cfg != null)
            {
                dbServer = cfg.DataSource;
                dbName = cfg.DatabaseName;
                dbUser = cfg.LoginUserName;
                dbPwd = cfg.LoginPassword;
                _PackingDBName = cfg.DbLinkName;
                //重新赋值
                connectionBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
            }
            # endregion
            string WoType;
            if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN01"))
                WoType = "ZPA1";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN02"))
                WoType = "ZPA2";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("ZPA1"))
                WoType = "ZPA1";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("ZPA2"))
                WoType = "ZPA2";
            else
                WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
            WOTypeCode = WoType.Replace("M", "");

            bool isOK = verifyUser(user, pwd);

            if (isOK)
                this.DialogResult = DialogResult.OK;
            else
            {
               // FormMain.showTipMSG("账号或者密码不对!", this.tbPWD, "Prompting Message", ToolTipIcon.Warning);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message15", "账号或者密码不对!"));
                tbPWD.Text = "";
                tbPWD.Focus();
            }
        }

        /// <summary>
        /// 设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSetUp_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormAccount().ShowDialog();
            this.Show();
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btQuit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }
        private void tbPWD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btLogin_Click(null, null);
        }
        private void tbName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.tbPWD.Focus();
        }
        #endregion

        #region Quality_Condition //Jacky 2015/11/27
        public bool IsEN()
        {
            bool B = false;
            if (vlanguage == "EN")
                B = true;
            return B;

        }
		void BtQuitClick(object sender, EventArgs e)
		{
			
		}

        #endregion





        
      
    }
}
