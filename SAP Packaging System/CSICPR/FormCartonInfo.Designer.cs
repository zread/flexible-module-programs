﻿namespace CSICPR
{
    partial class FormCartonInfo    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cust_BoxID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnQuery = new System.Windows.Forms.Button();
            this.ddlQueryType = new System.Windows.Forms.ComboBox();
            this.lblCartonQuery = new System.Windows.Forms.Label();
            this.PalPackingDate = new System.Windows.Forms.Panel();
            this.lblPackingFrom = new System.Windows.Forms.Label();
            this.dtpPackingDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblPackingTo = new System.Windows.Forms.Label();
            this.dtpPackingDateTo = new System.Windows.Forms.DateTimePicker();
            this.PalCarton = new System.Windows.Forms.Panel();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.PalConter = new System.Windows.Forms.Panel();
            this.txtConter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PalPackingDate.SuspendLayout();
            this.PalCarton.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PalConter.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowIndex,
            this.CartonID,
            this.CartonStatus,
            this.Cust_BoxID,
            this.SNQTY});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 75);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 16;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(453, 318);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // RowIndex
            // 
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 50;
            // 
            // CartonID
            // 
            this.CartonID.HeaderText = "内部托号";
            this.CartonID.Name = "CartonID";
            this.CartonID.ReadOnly = true;
            this.CartonID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CartonID.Width = 120;
            // 
            // CartonStatus
            // 
            this.CartonStatus.HeaderText = "托号状态";
            this.CartonStatus.Name = "CartonStatus";
            this.CartonStatus.ReadOnly = true;
            this.CartonStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Cust_BoxID
            // 
            this.Cust_BoxID.HeaderText = "客户托号";
            this.Cust_BoxID.Name = "Cust_BoxID";
            // 
            // SNQTY
            // 
            this.SNQTY.HeaderText = "组件数量";
            this.SNQTY.Name = "SNQTY";
            this.SNQTY.ReadOnly = true;
            this.SNQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SNQTY.Width = 80;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(275, 40);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 22);
            this.btnQuery.TabIndex = 168;
            this.btnQuery.Tag = "button1";
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // ddlQueryType
            // 
            this.ddlQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlQueryType.FormattingEnabled = true;
            this.ddlQueryType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ddlQueryType.Location = new System.Drawing.Point(93, 7);
            this.ddlQueryType.Name = "ddlQueryType";
            this.ddlQueryType.Size = new System.Drawing.Size(148, 20);
            this.ddlQueryType.TabIndex = 169;
            this.ddlQueryType.SelectedIndexChanged += new System.EventHandler(this.ddlQueryType_SelectedIndexChanged);
            // 
            // lblCartonQuery
            // 
            this.lblCartonQuery.AutoSize = true;
            this.lblCartonQuery.Location = new System.Drawing.Point(16, 10);
            this.lblCartonQuery.Name = "lblCartonQuery";
            this.lblCartonQuery.Size = new System.Drawing.Size(71, 12);
            this.lblCartonQuery.TabIndex = 170;
            this.lblCartonQuery.Text = "查 询 类 型";
            // 
            // PalPackingDate
            // 
            this.PalPackingDate.Controls.Add(this.lblPackingFrom);
            this.PalPackingDate.Controls.Add(this.dtpPackingDateFrom);
            this.PalPackingDate.Controls.Add(this.lblPackingTo);
            this.PalPackingDate.Controls.Add(this.dtpPackingDateTo);
            this.PalPackingDate.Location = new System.Drawing.Point(11, 36);
            this.PalPackingDate.Name = "PalPackingDate";
            this.PalPackingDate.Size = new System.Drawing.Size(261, 36);
            this.PalPackingDate.TabIndex = 171;
            // 
            // lblPackingFrom
            // 
            this.lblPackingFrom.AutoSize = true;
            this.lblPackingFrom.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingFrom.Location = new System.Drawing.Point(2, 6);
            this.lblPackingFrom.Name = "lblPackingFrom";
            this.lblPackingFrom.Size = new System.Drawing.Size(21, 14);
            this.lblPackingFrom.TabIndex = 0;
            this.lblPackingFrom.Text = "从";
            // 
            // dtpPackingDateFrom
            // 
            this.dtpPackingDateFrom.CustomFormat = "yyyy-MM-dd";
            this.dtpPackingDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPackingDateFrom.Location = new System.Drawing.Point(24, 3);
            this.dtpPackingDateFrom.Name = "dtpPackingDateFrom";
            this.dtpPackingDateFrom.Size = new System.Drawing.Size(100, 21);
            this.dtpPackingDateFrom.TabIndex = 5;
            // 
            // lblPackingTo
            // 
            this.lblPackingTo.AutoSize = true;
            this.lblPackingTo.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingTo.Location = new System.Drawing.Point(133, 7);
            this.lblPackingTo.Name = "lblPackingTo";
            this.lblPackingTo.Size = new System.Drawing.Size(21, 14);
            this.lblPackingTo.TabIndex = 6;
            this.lblPackingTo.Text = "到";
            // 
            // dtpPackingDateTo
            // 
            this.dtpPackingDateTo.CustomFormat = "yyyy-MM-dd";
            this.dtpPackingDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPackingDateTo.Location = new System.Drawing.Point(156, 4);
            this.dtpPackingDateTo.Name = "dtpPackingDateTo";
            this.dtpPackingDateTo.Size = new System.Drawing.Size(104, 21);
            this.dtpPackingDateTo.TabIndex = 7;
            // 
            // PalCarton
            // 
            this.PalCarton.Controls.Add(this.txtCarton);
            this.PalCarton.Controls.Add(this.label2);
            this.PalCarton.Location = new System.Drawing.Point(13, 38);
            this.PalCarton.Name = "PalCarton";
            this.PalCarton.Size = new System.Drawing.Size(259, 32);
            this.PalCarton.TabIndex = 172;
            // 
            // txtCarton
            // 
            this.txtCarton.Location = new System.Drawing.Point(79, 3);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.Size = new System.Drawing.Size(148, 21);
            this.txtCarton.TabIndex = 94;
            this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "内 部 托 号";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.ddlQueryType);
            this.panel1.Controls.Add(this.lblCartonQuery);
            this.panel1.Controls.Add(this.PalCarton);
            this.panel1.Controls.Add(this.PalPackingDate);
            this.panel1.Controls.Add(this.PalConter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(453, 75);
            this.panel1.TabIndex = 174;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(362, 40);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 22);
            this.btnSave.TabIndex = 174;
            this.btnSave.Tag = "button1";
            this.btnSave.Text = "确定";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // PalConter
            // 
            this.PalConter.Controls.Add(this.txtConter);
            this.PalConter.Controls.Add(this.label1);
            this.PalConter.Location = new System.Drawing.Point(15, 38);
            this.PalConter.Name = "PalConter";
            this.PalConter.Size = new System.Drawing.Size(259, 32);
            this.PalConter.TabIndex = 173;
            // 
            // txtConter
            // 
            this.txtConter.Location = new System.Drawing.Point(79, 3);
            this.txtConter.Name = "txtConter";
            this.txtConter.Size = new System.Drawing.Size(148, 21);
            this.txtConter.TabIndex = 94;
            this.txtConter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConter_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 94;
            this.label1.Text = "货   柜   号";
            // 
            // FormCartonInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 393);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCartonInfo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0002";
            this.Load += new System.EventHandler(this.FormCartonInfoQuery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PalPackingDate.ResumeLayout(false);
            this.PalPackingDate.PerformLayout();
            this.PalCarton.ResumeLayout(false);
            this.PalCarton.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PalConter.ResumeLayout(false);
            this.PalConter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox ddlQueryType;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalPackingDate;
        private System.Windows.Forms.Label lblPackingFrom;
        private System.Windows.Forms.DateTimePicker dtpPackingDateFrom;
        private System.Windows.Forms.Label lblPackingTo;
        private System.Windows.Forms.DateTimePicker dtpPackingDateTo;
        private System.Windows.Forms.Panel PalCarton;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PalConter;
        private System.Windows.Forms.TextBox txtConter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cust_BoxID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNQTY;
    }
}