﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormScrap  : Form
    {
        public FormScrap(string pa, string ma, string unit, string qty)
        {
            InitializeComponent();
            line.Text = pa;
            Material.Text = ma;
            pcs.Text = unit;
            Qty.Text = qty;
            Conversion.Visible = false;

            List<string> returnList = new List<string>();
            SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB;User ID = reader; Password=12345");
            string sql = "select top 4 ProductionOrder FROM[LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule]  where SUBSTRING(ProductionOrder, 2, 1) = '" + line.Text + "' order by ID desc";

            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    returnList.Add(sdr[0].ToString());

                }
                foreach (string value in returnList)
                {
                    MO.Items.Add(value);


                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        private void FormScrap_Load(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

     

      
        private void Shift_keyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Qty.Focus();
        }
       
        private void sunum_keyDown(object sender, KeyEventArgs e)
        {

        }
        private int Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                int a = (int)Mycommand.ExecuteNonQuery();
                conn.Close();
                return a;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

     

       
         private string getShift(DateTime dt)
        {
        	int[] S2 = {0,1,4,5,6,9,10};
        	string OneTwo = "";
        	string DN = "";
        	
        	
        	string date = dt.ToString("HH:mm:ss");
        	if(Convert.ToInt16(date.Substring(0,2)) >= 7 && Convert.ToInt16(date.Substring(0,2)) <19)
        		DN = "D";
        	else
        	{
        		DN = "N";
        		if (Convert.ToInt16(date.Substring(0,2)) < 7) {        			
        			dt = dt.AddDays(-1);
        		}        		
        	}
        	
        	var Days = (dt.Date- Convert.ToDateTime("2016-08-01")).TotalDays;
        	int Mod = Convert.ToInt16(Days) % 14;
        	if (S2.Contains(Mod)) 
        		OneTwo = "2";
        	else 
        		OneTwo = "1";
        	
        	return DN+OneTwo;
        	
        }
        
        private string getDate(string sft)
        {
        	bool Finish = false;
        	DateTime dt = DateTime.Now;
        	while(!Finish)
        	{
        		if(getShift(dt) == sft)
        			Finish = true;
        		else
        		{
        			dt = dt.AddHours(-12);
        		}
        	}
        	return dt.ToString("yyyy-MM-dd HH:mm:ss.000");
        }

        private void Scrap_Click(object sender, EventArgs e)
        {
            Scrap.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString())  || string.IsNullOrEmpty(Shift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[Scrap] values('{0}','{1}','{2}','{3}',null,null,'{4}',null)";
                if (Material.Text == "406 EVA" || Material.Text == "806 EVA")
                {
                	sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 180 / 60,getDate(Shift.Text));
                    Conversion.Text = "EVA: 60Kg = 180m²";
                    Conversion.Visible = true;
                }
                if (Material.Text == "Backsheet")
                {
                	sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 400 / 173,getDate(Shift.Text));
                    Conversion.Text = "Backsheet: 173Kg = 400m²";
                    Conversion.Visible = true;
                }
                else
                {
                	sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text),getDate(Shift.Text));
                }
                Executenonequery(sql);

                string sqlselect = "SELECT  [ID],[RM],[MO],[Shift],[ScrapQty],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[Scrap] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  shift = '" + Shift.Text + "' and CreatedDate >= GETDATE()-1";
                DataTable dt = RetrieveData(sqlselect);
                MessageBox.Show(Material.Text + " is scraped");                          
                dataGridView1.DataSource = dt;
            }
            Scrap.Enabled = true;
        }

        private void Review_Click_1(object sender, EventArgs e)
        {
            Review.Enabled = false;
            string sqlselect = "SELECT  [ID],[RM],[MO],[Shift],[ScrapQty],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[Scrap] where RM='" + Material.Text + "' and mo='" + MO.Text + "'and createddate>=getdate()-1 ";
            DataTable dt = RetrieveData(sqlselect);
            dataGridView1.DataSource = dt;
            Review.Enabled = true;
        }

        private void Modify_Click_1(object sender, EventArgs e)
        {
            Modify.Enabled = false;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
            string sql = "Update  [CAPA01DB01].[dbo].[Scrap] set  mo = '" + selectedRow.Cells["mo"].Value.ToString() + "',shift = '" + selectedRow.Cells["shift"].Value.ToString() + "',scrapqty = '" + selectedRow.Cells["scrapqty"].Value + "',QRQty = '" + selectedRow.Cells["QRQty"].Value + "',WHRQty = '" + selectedRow.Cells["WHRQty"].Value + "',ModifiedDate = GETDATE()  where  ID = '" + selectedRow.Cells["ID"].Value + "'";
            int a = Executenonequery(sql);
            string sqlselect = "SELECT  [ID],[RM],[MO],[Shift],[ScrapQty],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[Scrap] where ID = '" + selectedRow.Cells["ID"].Value + "'";
            DataTable dt = RetrieveData(sqlselect);
            if (a > 0)
            {
                MessageBox.Show("This row is Modified.");

                
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("This row can not be modified.");
            }
            Modify.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void QR_Click(object sender, EventArgs e)
        {
            QR.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(Shift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[Scrap] values('{0}','{1}','{2}',null,'{3}',null,getdate(),null)";
                if (Material.Text == "406 EVA" || Material.Text == "806 EVA")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 180 / 60);
                    Conversion.Text = "EVA: 60Kg = 180m²";
                    Conversion.Visible = true;
                }
                if (Material.Text == "Backsheet")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 400 / 173);
                    Conversion.Text = "Backsheet: 173Kg = 400m²";
                    Conversion.Visible = true;
                }
                else
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text));
                }
                Executenonequery(sql);

                string sqlselect = "SELECT  [ID],[RM],[MO],[Shift],[ScrapQty],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[Scrap] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  shift = '" + Shift.Text + "' and CreatedDate >= GETDATE()-1";
                DataTable dt = RetrieveData(sqlselect);
                MessageBox.Show(Material.Text + " is rejected by Quality");
                dataGridView1.DataSource = dt;
            }
            QR.Enabled = true;
        }

        private void WHR_Click(object sender, EventArgs e)
        {
            WHR.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(Shift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[Scrap] values('{0}','{1}','{2}',null,null,'{3}',getdate(),null)";
                if (Material.Text == "406 EVA" || Material.Text == "806 EVA")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 180 / 60);
                    Conversion.Text = "EVA: 60Kg = 180m²";
                    Conversion.Visible = true;
                }
                if (Material.Text == "Backsheet")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text) * 400 / 173);
                    Conversion.Text = "Backsheet: 173Kg = 400m²";
                    Conversion.Visible = true;
                }
                else
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Shift.Text.ToString(), Convert.ToDouble(Qty.Text));
                }
                Executenonequery(sql);

                string sqlselect = "SELECT  [ID],[RM],[MO],[Shift],[ScrapQty],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[Scrap] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  shift = '" + Shift.Text + "' and CreatedDate >= GETDATE()-1";
                DataTable dt = RetrieveData(sqlselect);
                MessageBox.Show(Material.Text + " Return to Warehouse");
                dataGridView1.DataSource = dt;
            }
            WHR.Enabled = true;
        }
    }
}
