﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormAnalyse : Form
    {
        DataTable prodDateSource;

        //reportFlag=1:Report by date,=2:report by carton,=3:report by module
        private int reportFlag = -1;
        private string sCellType = "";

        public FormAnalyse(Form fm, int iReportFormat)
        {
            InitializeComponent();
            this.reportFlag = iReportFormat;

            prodDateSource = new DataTable();
            upSourcd();

            this.MdiParent = fm;
            this.WindowState = FormWindowState.Maximized;
            this.Show();

            if (iReportFormat == ToolsClass.iDateReport)
            {
                tbCPTPackingQuerySQL.Name = "tbCPTPackingQuerySQL";
                this.Text = "Report By Date";
            }
            else if (iReportFormat == ToolsClass.iCartonReport)
            {
                tbCPTPackingQuerySQL.Name = "tbCPTPackingQuerySQLCarton";
                lblfrom.Text = "Carton Number:";
                txtCartonNo.Visible = true;
                lblto.Visible = false;
                dateTo.Visible = false;
                dateFrom.Visible = false;
                this.Text = "Report By Carton";
            }
            else
            {
                tbCPTPackingQuerySQL.Name = "tbCPTPackingQuerySQLModule";
                lblfrom.Text = "Module Number:";
                txtCartonNo.Visible = true;
                lblto.Visible = false;
                dateTo.Visible = false;
                dateFrom.Visible = false;
                this.Text = "Report By Module";
            }
            tbCPTPackingQuerySQL.Text = ToolsClass.getSQL(tbCPTPackingQuerySQL.Name);
           
        }
        
        private void FormAnalyse_Load(object sender, EventArgs e)
        {
            if (this.reportFlag == ToolsClass.iDateReport)
            {
                this.dateFrom.Value = DateTime.Today;
                this.dateTo.Value = DateTime.Today.AddDays(1);
                tbCPTPackingQuerySQL.Text = ToolsClass.getSQL("tbCPTPackingQuerySQL");
                btQuery_Click(sender, e);
            }

        }
        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        private void upSourcd()
        {
            //string strTemp = ":";
            //清除当前显示内容
            dataGridView1.Rows.Clear();

            int iClassIndex = 0;
            //显示数据
            string boxID = "", boxIDnew = "", pdcSN = "", pdcSNnew = "";
            int rowNum = 0, reNum = 0;
            DataRow lastrow = null;
            foreach (DataRow row in prodDateSource.Rows)
            {	
            	pdcSNnew = row[2].ToString();
                if (pdcSN == pdcSNnew)
                {
                    reNum++;
                    continue;
                }
                else
                    pdcSN = pdcSNnew;
                boxIDnew = row[0].ToString();
                if (boxID == boxIDnew)
                {
                    iClassIndex++;
                    if (iClassIndex <= 5)
                    {
                    	rowNum = dataGridView1.Rows.Count - 6 + iClassIndex;
                    }
                    else
                    {
                    	rowNum = dataGridView1.Rows.Add();
                    }
                	dataGridView1.Rows[rowNum].Cells[2].Value = row[2];
                    dataGridView1.Rows[rowNum].Cells[3].Value = row[3];
                    dataGridView1.Rows[rowNum].Cells[4].Value = row[5];
                    dataGridView1.Rows[rowNum].Cells[5].Value = row[6];
                    dataGridView1.Rows[rowNum].Cells[6].Value = row[7];
                    dataGridView1.Rows[rowNum].Cells[7].Value = row[8];
                    dataGridView1.Rows[rowNum].Cells[8].Value = row[9];
                }
                else 
                {
                	//strTemp = ":";
                    rowNum = dataGridView1.Rows.Add();
                    boxID = boxIDnew;
                    dataGridView1.Rows[rowNum].Cells[0].Value = row[0];
                    dataGridView1.Rows[rowNum].Cells[1].Value = "NominalPower: "+ToolsClass.getIdealPower((double)row[3], row[2].ToString()).ToString();
                    dataGridView1.Rows[rowNum].Cells[2].Value = row[2];
                    dataGridView1.Rows[rowNum].Cells[3].Value = row[3];
                    dataGridView1.Rows[rowNum].Cells[4].Value = row[5];
                    dataGridView1.Rows[rowNum].Cells[5].Value = row[6];
                    dataGridView1.Rows[rowNum].Cells[6].Value = row[7];
                    dataGridView1.Rows[rowNum].Cells[7].Value = row[8];
                    dataGridView1.Rows[rowNum].Cells[8].Value = row[9];
                    iClassIndex = 0;
                    for(int i = 1; i <= 5; i++)
                    {
                    	rowNum = dataGridView1.Rows.Add();
                    	switch (i)
                        {
                            case 1:
                                dataGridView1.Rows[rowNum].Cells[1].Value = "Model: " + row[4].ToString();
                                break;
                            case 2:
                                if (row[4].ToString().Trim().Substring(row[4].ToString().Trim().Length - 1).ToUpper().Equals("M"))
                                    sCellType = "Mono";
                                else if (row[4].ToString().Trim().Substring(row[4].ToString().Trim().Length - 1).ToUpper().Equals("P"))
                                    sCellType = "Poly";
                                else
                                    sCellType = row[11].ToString();
                                dataGridView1.Rows[rowNum].Cells[1].Value = "CellType: " + sCellType;
                                break;
                            case 3:
                                dataGridView1.Rows[rowNum].Cells[1].Value = "CellColor: " + row[10].ToString();
                                break;
                            case 4:
                                dataGridView1.Rows[rowNum].Cells[1].Value = "CartonClass: " + row[12].ToString();
                                break;
                            case 5:
                                dataGridView1.Rows[rowNum].Cells[1].Value = "PackingDate: " + row[13].ToString();
                                break;
                        }
                    }
                }

            }
            this.Cursor = System.Windows.Forms.Cursors.Default;
            //label2.Text = "Module，" + "Total:" + (prodDateSource.Rows.Count - reNum) + " Module";
            label2.Text = "Total:" + (prodDateSource.Rows.Count - reNum) + " Module";
        }

        private void btOutpu_Click(object sender, EventArgs e)
        {
            ToolsClass.DataToExcel(dataGridView1, true);
            //ToolsClass.DataToNotepad(dataGridView1);
        }

        private void label14_DoubleClick(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        private void btQuery_Click(object sender, EventArgs e)
        {


            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            string sqlStr = "";
            if (this.reportFlag == ToolsClass.iDateReport)
            {
                string ToStr = dateTo.Value.ToString("yyyy-MM-dd HH:mm:ss");
                ToStr = ToStr.Remove(ToStr.LastIndexOf(':'));
                string fromStr = dateFrom.Value.ToString("yyyy-MM-dd HH:mm:ss");
                fromStr = fromStr.Remove(fromStr.LastIndexOf(':'));
                //MessageBox.Show(fromStr);
                sqlStr = string.Format(tbCPTPackingQuerySQL.Text, fromStr, ToStr);
                ToolsClass.GetDataTable(sqlStr, prodDateSource);
            }
            else
            {
                sqlStr = string.Format(tbCPTPackingQuerySQL.Text, txtCartonNo.Text.Trim());
                if (!txtCartonNo.Text.Trim().Equals(""))
                    ToolsClass.GetDataTable(sqlStr, prodDateSource);
            }
            
            //更新显示
            upSourcd();
        }

        private void btSaveSQL_Click(object sender, EventArgs e)
        {
            //保存组件装箱报表sql语句 tbCPTBoxSQL
            ToolsClass.saveSQL(tbCPTPackingQuerySQL);
            splitContainer1.Panel2Collapsed = true;
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbCPTPackingQuerySQL.Undo();
        }

        private void FormAnalyse_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

    }
}