﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormCurb : Form
    {
        private static int len = 0;//编号长度
        private static int end = 0;//转换为整数的最大值
        private static string temp = "";//零散编码
        private static int loc = 0;//需检验的字符在一个编号中的起始位置
        private static int sta = 0;//转换为整数的起始编号的待检验位
        private static string frist = "";//起始编号
        public FormCurb()
        {
            InitializeComponent();
        }
        public static bool isIn(string code)
        {
            if (code.Length == len)
            {
                if (frist == code.Substring(0, loc))
                {
                    int tmp = 0;
                    if (int.TryParse(code.Substring(loc), out  tmp))
                    {
                        if (tmp >= sta && tmp < end)
                            return true;
                    }
                }
            }
            return temp.Contains(code);
        }
        
        private void btOK_Click(object sender, EventArgs e)
        {
            try
            {
                len = this.textBox1.TextLength;
                loc = len - numericUpDown1.Value.ToString().Length;
                frist = this.textBox1.Text.Substring(0, loc);
                sta = int.Parse(this.textBox1.Text.Substring(loc)); //int.TryParse(this.textBox1.Text.Substring(loc), out  sta);
                end = sta + (int)numericUpDown1.Value;
                temp = textBox2.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
