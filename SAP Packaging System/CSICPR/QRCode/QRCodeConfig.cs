﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR.QRCode
{
    public partial class QRCodeConfig : Form
    {
        private string xmlFile = Application.StartupPath + "\\QRCodeConfig.xml";

        private static string SoftwareAddress;//软件安装地址
        private static string PrintCount;//打印张数
        private static string strModel;
        private static string strPMax;
        private static string strVmp;
        private static string strImp;
        private static string strVoc;
        private static string strIsc;
        private static string strBrandPrintName;
        private static string strSidePrintName;

        public bool isChange = false;

        public QRCodeConfig()
        {
            InitializeComponent();
            loadXML();
        }

        private void loadXML()
        {
            if (System.IO.File.Exists(xmlFile))
            {
                txtAddress.Text = BaseQRCodeHelper.getConfig("SoftwareAddress");
                nudPrintNum.Value = int.Parse(BaseQRCodeHelper.getConfig("PrintCount"));
                txtModel.Text = BaseQRCodeHelper.getConfig("Model");
                txtPMax.Text = BaseQRCodeHelper.getConfig("PMax");
                txtVmp.Text = BaseQRCodeHelper.getConfig("Vmp");
                txtImp.Text = BaseQRCodeHelper.getConfig("Imp");
                txtVoc.Text = BaseQRCodeHelper.getConfig("Voc");
                txtIsc.Text = BaseQRCodeHelper.getConfig("Isc");
                txtBrandPrintName.Text = BaseQRCodeHelper.getConfig("BrandPrintName");
                txtSidePrintName.Text = BaseQRCodeHelper.getConfig("SidePrintName");  
            }
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            isChange = true;
            if (!System.IO.File.Exists(xmlFile))
            {
                string sDataSet = "";
                SoftwareAddress = txtAddress.Text.Trim();
                PrintCount = nudPrintNum.Value.ToString();
                strModel = txtModel.Text.Trim();
                strPMax = txtPMax.Text.Trim();
                strVmp = txtVmp.Text.Trim();
                strImp = txtImp.Text.Trim();
                strVoc = txtVoc.Text.Trim();
                strIsc = txtIsc.Text.Trim();
                strBrandPrintName = txtBrandPrintName.Text.Trim();
                strSidePrintName = txtSidePrintName.Text.Trim();

                sDataSet = SoftwareAddress + "|" + PrintCount + "|" + strModel + "|" + strPMax + "|" + strVmp + "|" + strImp + "|" + strVoc + "|" + strIsc + "|" + strBrandPrintName + "|" + strSidePrintName;
                BaseQRCodeHelper.setConfig(sDataSet);
            }
            else
                isSaveChange();
            this.Close();
        }

        private void isSaveChange()
        {
            if (isChange)
            {
                #region
                if (System.IO.File.Exists(xmlFile))
                {
                    if (MessageBox.Show("确认保存以下修改内容吗?", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {

                        ToolsClass.saveXMLNode("SoftwareAddress", SoftwareAddress);
                        ToolsClass.saveXMLNode("PrintCount", PrintCount);
                        ToolsClass.saveXMLNode("Model", strModel);
                        ToolsClass.saveXMLNode("PMax", strVmp);
                        ToolsClass.saveXMLNode("Vmp", strVmp);
                        ToolsClass.saveXMLNode("Imp", strImp);
                        ToolsClass.saveXMLNode("Voc", strVoc);
                        ToolsClass.saveXMLNode("Isc", strIsc);

                        ToolsClass.saveXMLNode("BrandPrintName", strBrandPrintName);
                        ToolsClass.saveXMLNode("SidePrintName", strSidePrintName);
             
                 
                        isChange = false;
                    }
                }
                #endregion
            }
        }
    }
}
