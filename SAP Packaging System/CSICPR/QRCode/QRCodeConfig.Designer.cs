﻿namespace CSICPR.QRCode
{
    partial class QRCodeConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSidePrintName = new System.Windows.Forms.TextBox();
            this.txtBrandPrintName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nudPrintNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtIsc = new System.Windows.Forms.TextBox();
            this.txtImp = new System.Windows.Forms.TextBox();
            this.txtVoc = new System.Windows.Forms.TextBox();
            this.txtVmp = new System.Windows.Forms.TextBox();
            this.txtPMax = new System.Windows.Forms.TextBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Submit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintNum)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSidePrintName);
            this.groupBox1.Controls.Add(this.txtBrandPrintName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nudPrintNum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(452, 162);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "基础数据设定";
            // 
            // txtSidePrintName
            // 
            this.txtSidePrintName.Location = new System.Drawing.Point(119, 90);
            this.txtSidePrintName.Name = "txtSidePrintName";
            this.txtSidePrintName.ReadOnly = true;
            this.txtSidePrintName.Size = new System.Drawing.Size(322, 21);
            this.txtSidePrintName.TabIndex = 17;
            // 
            // txtBrandPrintName
            // 
            this.txtBrandPrintName.Location = new System.Drawing.Point(119, 54);
            this.txtBrandPrintName.Name = "txtBrandPrintName";
            this.txtBrandPrintName.ReadOnly = true;
            this.txtBrandPrintName.Size = new System.Drawing.Size(322, 21);
            this.txtBrandPrintName.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 12);
            this.label10.TabIndex = 15;
            this.label10.Text = "侧边框打印机名称:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 63);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "商标打印机名称:";
            // 
            // nudPrintNum
            // 
            this.nudPrintNum.Location = new System.Drawing.Point(119, 127);
            this.nudPrintNum.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPrintNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrintNum.Name = "nudPrintNum";
            this.nudPrintNum.Size = new System.Drawing.Size(38, 21);
            this.nudPrintNum.TabIndex = 13;
            this.nudPrintNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPrintNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "打印张数:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(66, 20);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.ReadOnly = true;
            this.txtAddress.Size = new System.Drawing.Size(375, 21);
            this.txtAddress.TabIndex = 1;
            this.txtAddress.Text = "C:\\Program Files (x86)\\Seagull\\BarTender Suite\\bartend.exe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "安装地址:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtIsc);
            this.groupBox2.Controls.Add(this.txtImp);
            this.groupBox2.Controls.Add(this.txtVoc);
            this.groupBox2.Controls.Add(this.txtVmp);
            this.groupBox2.Controls.Add(this.txtPMax);
            this.groupBox2.Controls.Add(this.txtModel);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(2, 178);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(452, 154);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "二维码参数设定";
            // 
            // txtIsc
            // 
            this.txtIsc.Location = new System.Drawing.Point(247, 108);
            this.txtIsc.Name = "txtIsc";
            this.txtIsc.Size = new System.Drawing.Size(94, 21);
            this.txtIsc.TabIndex = 12;
            this.txtIsc.Text = "1.15";
            // 
            // txtImp
            // 
            this.txtImp.Location = new System.Drawing.Point(247, 71);
            this.txtImp.Name = "txtImp";
            this.txtImp.Size = new System.Drawing.Size(94, 21);
            this.txtImp.TabIndex = 11;
            this.txtImp.Text = "1.13";
            // 
            // txtVoc
            // 
            this.txtVoc.Location = new System.Drawing.Point(72, 108);
            this.txtVoc.Name = "txtVoc";
            this.txtVoc.Size = new System.Drawing.Size(94, 21);
            this.txtVoc.TabIndex = 10;
            this.txtVoc.Text = "1.14";
            // 
            // txtVmp
            // 
            this.txtVmp.Location = new System.Drawing.Point(72, 71);
            this.txtVmp.Name = "txtVmp";
            this.txtVmp.Size = new System.Drawing.Size(94, 21);
            this.txtVmp.TabIndex = 9;
            this.txtVmp.Text = "1.12";
            // 
            // txtPMax
            // 
            this.txtPMax.Location = new System.Drawing.Point(247, 32);
            this.txtPMax.Name = "txtPMax";
            this.txtPMax.Size = new System.Drawing.Size(94, 21);
            this.txtPMax.TabIndex = 8;
            this.txtPMax.Text = "1.11";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(72, 32);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(94, 21);
            this.txtModel.TabIndex = 7;
            this.txtModel.Text = "CS6P-P";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(206, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "PMax:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "Vmp:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(206, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "Imp:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "Voc:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(206, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "Isc:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "Model:";
            // 
            // Submit
            // 
            this.Submit.Location = new System.Drawing.Point(133, 352);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(172, 23);
            this.Submit.TabIndex = 2;
            this.Submit.Text = "返    回";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // QRCodeConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 391);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "QRCodeConfig";
            this.Text = "二维码打印参数配置";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintNum)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudPrintNum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPMax;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtIsc;
        private System.Windows.Forms.TextBox txtImp;
        private System.Windows.Forms.TextBox txtVoc;
        private System.Windows.Forms.TextBox txtVmp;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.TextBox txtSidePrintName;
        private System.Windows.Forms.TextBox txtBrandPrintName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}