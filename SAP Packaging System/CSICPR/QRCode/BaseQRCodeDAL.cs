﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CSICPR.QRCode
{
    public class BaseQRCodeDAL
    {
        public static bool InsertBaseQRCodeModel(BaseQRCodeModel baseQRCodeModel, string conn)
        {
            const string sql = @"
INSERT INTO BaseQRCode(SupplierCode,BSII,Revision,BrandName,SideBarcodeName,CreateOn,CreateBy,Resv01,Resv02,Resv03,Resv04,Resv05,Resv06,Resv07)
VALUES(@SupplierCode,@BSII,@Revision,@BrandName,@SideBarcodeName,@CreateOn,@CreateBy,@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07)";

            return Dapper.Save(baseQRCodeModel, sql, conn);
        }

        public static bool UpdateBaseQRCodeModelByID(BaseQRCodeModel baseQRCodeModel, string conn)
        {
            const string sql = @"
UPDATE BaseQRCode SET Resv01='T' where InterID=@InterID";

            return Dapper.Save(baseQRCodeModel, sql, conn);
        }

        public static bool UpdateBaseQRCodeModelByStatus(BaseQRCodeModel baseQRCodeModel, string conn)
        {
            const string sql = @"
UPDATE BaseQRCode SET Resv01='' where Resv01='T'";

            return Dapper.Save(baseQRCodeModel, sql, conn);
        }

        public static bool DeleteBaseQRCodeModel(BaseQRCodeModel baseQRCodeModel, string conn)
        {
            const string sql = @"
DELETE FROM BaseQRCode  where InterID=@InterID";

            return Dapper.Save(baseQRCodeModel, sql, conn);
        }

        public static List<BaseQRCodeModel> QueryBaseQRCodeModel(string conn)
        {
            const string sql = @"
SELECT [InterID] ,[SupplierCode],[BSII] ,[Revision],[BrandName]
      ,[SideBarcodeName],[CreateOn],[CreateBy],[Resv01]
      ,[Resv02],[Resv03],[Resv04],[Resv05],[Resv06],[Resv07]
  FROM BaseQRCode nolock";

            return Dapper.Query<BaseQRCodeModel>(sql,null,conn);
        }

        public static BaseQRCodeModel QueryBaseQRCodeModel(string strRev,string conn)
        {
            const string sql = @"
SELECT [InterID] ,[SupplierCode],[BSII] ,[Revision],[BrandName]
      ,[SideBarcodeName],[CreateOn],[CreateBy],[Resv01]
      ,[Resv02],[Resv03],[Resv04],[Resv05],[Resv06],[Resv07]
  FROM BaseQRCode nolock where Resv01=@Resv01";

            return Dapper.QuerySingle<BaseQRCodeModel>(sql, new { Resv01 = strRev }, conn);
        }
    }
}
