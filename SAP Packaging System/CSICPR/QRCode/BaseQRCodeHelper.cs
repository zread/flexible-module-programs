﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace CSICPR.QRCode
{
    public class BaseQRCodeHelper
    {
        public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "QRCodeConfig.xml";
            string xmlFile = Application.StartupPath + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {
                MessageBox.Show("Please Inform Manager to Config Package Parameters Before Packing", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }

        public static void setConfig(string sData)//(int CartonQty, int IdealPower, int MixCount, int PrintMode,int LabelFormat,string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule,string MixChecked,string CheckNOChecked)
        {
            string xmlFile = Application.StartupPath + "\\QRCodeConfig.xml";
            string[] sDataSet = sData.Split('|');

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode parentsNode = doc.CreateElement("Parameters");
            doc.AppendChild(parentsNode);

            subNode(doc, parentsNode, "SoftwareAddress", sDataSet[0]);
            subNode(doc, parentsNode, "PrintCount", sDataSet[1]);
            subNode(doc, parentsNode, "Model", sDataSet[2]);
            subNode(doc, parentsNode, "PMax", sDataSet[3]);
            subNode(doc, parentsNode, "Vmp", sDataSet[4]);
            subNode(doc, parentsNode, "Imp", sDataSet[5]);
            subNode(doc, parentsNode, "Voc", sDataSet[6]);
            subNode(doc, parentsNode, "Isc", sDataSet[7]);
            subNode(doc, parentsNode, "BrandPrintName", sDataSet[8]);
            subNode(doc, parentsNode, "SidePrintName", sDataSet[9]);
          
            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XmlNode subNode(XmlDocument doc, XmlNode ParentNode, string sNodeName, string sValue, string sAttributeName = "", string sAttribute = "")
        {
            XmlNode subNode = doc.CreateElement(sNodeName);
            if (sAttribute != "")
            {
                XmlAttribute subAttribute = doc.CreateAttribute(sAttributeName);
                subAttribute.Value = sAttribute;
                subNode.Attributes.Append(subAttribute);
            }
            if (sValue != "")
                subNode.AppendChild(doc.CreateTextNode(sValue));
            ParentNode.AppendChild(subNode);
            return subNode;
        }

        public static void saveXMLNode(string snode, string value = "", bool isAttribute = false, string sAttributeName = "", string fileName = "")
        {
            string xmlFile = "";
            string ParentNode = "";
            if (fileName == "" || fileName == null)
            {
                xmlFile = Application.StartupPath + "\\QRCodeConfig.xml";
                ParentNode = "Parameters";
            }
            else
            {
                xmlFile = Application.StartupPath + "\\" + fileName;
                ParentNode = "appSQL";
            }

            #region 在xml文件里添加一个节点
            if (snode.Trim().Equals("CheckModuleClass"))
            {
                XmlDocument newxmlDoc = new XmlDocument();
                newxmlDoc.Load(xmlFile);
                XmlNodeList newnodeList = newxmlDoc.SelectSingleNode(ParentNode).ChildNodes;
                string existsFlag = "N";

                foreach (XmlNode newxn in newnodeList)
                {
                    XmlElement xe = (XmlElement)newxn;
                    if (xe.Name == snode)
                    {
                        existsFlag = "Y";
                        break;
                    }
                }

                if (existsFlag.Equals("N"))
                {
                    XmlNode root = newxmlDoc.SelectSingleNode(ParentNode);
                    XmlElement node = newxmlDoc.CreateElement("CheckModuleClass");
                    root.AppendChild(node);
                    newxmlDoc.Save(xmlFile);
                }
            }
            #endregion

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode(ParentNode).ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn;
                if (xe.Name == snode)
                {
                    if (isAttribute)
                    {
                        xe.SetAttribute(sAttributeName, value);
                        break;
                    }
                    else
                    {
                        xe.InnerText = value;
                        break;
                    }
                }
            }
            try
            {
                xmlDoc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
