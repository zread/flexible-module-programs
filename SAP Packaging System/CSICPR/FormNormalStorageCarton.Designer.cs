﻿namespace CSICPR
{
    partial class FormNormalStorageCarton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNormalStorageCarton));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.GpbCartonQuery = new System.Windows.Forms.GroupBox();
            this.chbSelected = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonID = new System.Windows.Forms.DataGridViewLinkColumn();
            this.CartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormalWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReworkWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WaiXieWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Workshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackingLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.CartonQueryList = new System.Windows.Forms.Panel();
            this.PicBoxCarton = new System.Windows.Forms.PictureBox();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.GpbWoQuery = new System.Windows.Forms.GroupBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtSalesItemNo = new System.Windows.Forms.TextBox();
            this.txtSalesOrderNo = new System.Windows.Forms.TextBox();
            this.PicBoxWO = new System.Windows.Forms.PictureBox();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddlWoType = new System.Windows.Forms.ComboBox();
            this.txtWoOrder = new System.Windows.Forms.TextBox();
            this.LblWo = new System.Windows.Forms.Label();
            this.txtMitemCode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblWoType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPlanCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtWO1 = new System.Windows.Forms.TextBox();
            this.txtlocation = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFactory = new System.Windows.Forms.TextBox();
            this.Reset = new System.Windows.Forms.Button();
            this.Set = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SapSave = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.lstView = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.GpbCartonQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel4.SuspendLayout();
            this.CartonQueryList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxCarton)).BeginInit();
            this.GpbWoQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 16);
            this.panel1.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1020, 548);
            this.panel3.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.GpbCartonQuery);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.GpbWoQuery);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 548);
            this.splitContainer1.SplitterDistance = 727;
            this.splitContainer1.TabIndex = 0;
            // 
            // GpbCartonQuery
            // 
            this.GpbCartonQuery.Controls.Add(this.chbSelected);
            this.GpbCartonQuery.Controls.Add(this.dataGridView1);
            this.GpbCartonQuery.Controls.Add(this.panel4);
            this.GpbCartonQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbCartonQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbCartonQuery.Name = "GpbCartonQuery";
            this.GpbCartonQuery.Size = new System.Drawing.Size(727, 548);
            this.GpbCartonQuery.TabIndex = 2;
            this.GpbCartonQuery.TabStop = false;
            this.GpbCartonQuery.Text = "托号查询";
            // 
            // chbSelected
            // 
            this.chbSelected.AutoSize = true;
            this.chbSelected.BackColor = System.Drawing.SystemColors.Control;
            this.chbSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbSelected.Location = new System.Drawing.Point(18, 63);
            this.chbSelected.Name = "chbSelected";
            this.chbSelected.Size = new System.Drawing.Size(15, 14);
            this.chbSelected.TabIndex = 49;
            this.chbSelected.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.chbSelected.UseVisualStyleBackColor = false;
            this.chbSelected.Click += new System.EventHandler(this.chbSelected_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selected,
            this.RowIndex,
            this.CartonID,
            this.CartonStatus,
            this.CustomerCartonNo,
            this.SNQTY,
            this.NormalWo,
            this.ReworkWo,
            this.WaiXieWo,
            this.ProductCode,
            this.SalesOrderNo,
            this.SalesItemNo,
            this.Workshop,
            this.PackingLocation,
            this.Factory});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 59);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(721, 486);
            this.dataGridView1.TabIndex = 47;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dataGridView1_Scroll);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // Selected
            // 
            this.Selected.HeaderText = "";
            this.Selected.Name = "Selected";
            this.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Selected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Selected.Width = 40;
            // 
            // RowIndex
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RowIndex.DefaultCellStyle = dataGridViewCellStyle2;
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 35;
            // 
            // CartonID
            // 
            this.CartonID.HeaderText = "内部托号";
            this.CartonID.Name = "CartonID";
            this.CartonID.ReadOnly = true;
            this.CartonID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CartonID.Width = 120;
            // 
            // CartonStatus
            // 
            this.CartonStatus.HeaderText = "托号状态";
            this.CartonStatus.Name = "CartonStatus";
            this.CartonStatus.ReadOnly = true;
            this.CartonStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CartonStatus.Width = 60;
            // 
            // CustomerCartonNo
            // 
            this.CustomerCartonNo.HeaderText = "客户托号";
            this.CustomerCartonNo.Name = "CustomerCartonNo";
            this.CustomerCartonNo.Width = 120;
            // 
            // SNQTY
            // 
            this.SNQTY.HeaderText = "组件数量";
            this.SNQTY.Name = "SNQTY";
            this.SNQTY.ReadOnly = true;
            this.SNQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SNQTY.Width = 60;
            // 
            // NormalWo
            // 
            this.NormalWo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NormalWo.HeaderText = "转常规工单";
            this.NormalWo.Name = "NormalWo";
            // 
            // ReworkWo
            // 
            this.ReworkWo.HeaderText = "返工工单";
            this.ReworkWo.Name = "ReworkWo";
            // 
            // WaiXieWo
            // 
            this.WaiXieWo.HeaderText = "外协后工单";
            this.WaiXieWo.Name = "WaiXieWo";
            // 
            // ProductCode
            // 
            this.ProductCode.HeaderText = "产品物料编码";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.Width = 110;
            // 
            // SalesOrderNo
            // 
            this.SalesOrderNo.HeaderText = "销售订单";
            this.SalesOrderNo.Name = "SalesOrderNo";
            // 
            // SalesItemNo
            // 
            this.SalesItemNo.HeaderText = "销售订单项目";
            this.SalesItemNo.Name = "SalesItemNo";
            // 
            // Workshop
            // 
            this.Workshop.HeaderText = "车间";
            this.Workshop.Name = "Workshop";
            // 
            // PackingLocation
            // 
            this.PackingLocation.HeaderText = "入库地点";
            this.PackingLocation.Name = "PackingLocation";
            // 
            // Factory
            // 
            this.Factory.HeaderText = "工厂";
            this.Factory.Name = "Factory";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button1);
            this.panel4.Controls.Add(this.CartonQueryList);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 17);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(721, 42);
            this.panel4.TabIndex = 48;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(283, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 24);
            this.button1.TabIndex = 164;
            this.button1.Tag = "button1";
            this.button1.Text = "重置";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CartonQueryList
            // 
            this.CartonQueryList.Controls.Add(this.PicBoxCarton);
            this.CartonQueryList.Controls.Add(this.txtCarton);
            this.CartonQueryList.Controls.Add(this.label2);
            this.CartonQueryList.Location = new System.Drawing.Point(18, 5);
            this.CartonQueryList.Name = "CartonQueryList";
            this.CartonQueryList.Size = new System.Drawing.Size(259, 32);
            this.CartonQueryList.TabIndex = 93;
            // 
            // PicBoxCarton
            // 
            this.PicBoxCarton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCarton.BackgroundImage")));
            this.PicBoxCarton.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCarton.InitialImage")));
            this.PicBoxCarton.Location = new System.Drawing.Point(227, 4);
            this.PicBoxCarton.Name = "PicBoxCarton";
            this.PicBoxCarton.Size = new System.Drawing.Size(27, 18);
            this.PicBoxCarton.TabIndex = 167;
            this.PicBoxCarton.TabStop = false;
            this.PicBoxCarton.Click += new System.EventHandler(this.PicBoxCarton_Click);
            // 
            // txtCarton
            // 
            this.txtCarton.Location = new System.Drawing.Point(79, 3);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.Size = new System.Drawing.Size(148, 21);
            this.txtCarton.TabIndex = 94;
            this.txtCarton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCartonQuery_KeyDown);
            this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCartonQuery_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "内 部 托 号";
            // 
            // GpbWoQuery
            // 
            this.GpbWoQuery.Controls.Add(this.splitContainer3);
            this.GpbWoQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbWoQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbWoQuery.Name = "GpbWoQuery";
            this.GpbWoQuery.Size = new System.Drawing.Size(289, 548);
            this.GpbWoQuery.TabIndex = 0;
            this.GpbWoQuery.TabStop = false;
            this.GpbWoQuery.Text = "工单查询";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(3, 17);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label6);
            this.splitContainer3.Panel1.Controls.Add(this.label5);
            this.splitContainer3.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer3.Panel1.Controls.Add(this.checkBox2);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesItemNo);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesOrderNo);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxWO);
            this.splitContainer3.Panel1.Controls.Add(this.txtWo);
            this.splitContainer3.Panel1.Controls.Add(this.label3);
            this.splitContainer3.Panel1.Controls.Add(this.ddlWoType);
            this.splitContainer3.Panel1.Controls.Add(this.txtWoOrder);
            this.splitContainer3.Panel1.Controls.Add(this.LblWo);
            this.splitContainer3.Panel1.Controls.Add(this.txtMitemCode);
            this.splitContainer3.Panel1.Controls.Add(this.label14);
            this.splitContainer3.Panel1.Controls.Add(this.lblWoType);
            this.splitContainer3.Panel1.Controls.Add(this.label10);
            this.splitContainer3.Panel1.Controls.Add(this.txtPlanCode);
            this.splitContainer3.Panel1.Controls.Add(this.label9);
            this.splitContainer3.Panel1.Controls.Add(this.txtWO1);
            this.splitContainer3.Panel1.Controls.Add(this.txtlocation);
            this.splitContainer3.Panel1.Controls.Add(this.label4);
            this.splitContainer3.Panel1.Controls.Add(this.txtFactory);
            this.splitContainer3.Panel1.Controls.Add(this.Reset);
            this.splitContainer3.Panel1.Controls.Add(this.Set);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer3.Panel2.Controls.Add(this.Save);
            this.splitContainer3.Size = new System.Drawing.Size(283, 528);
            this.splitContainer3.SplitterDistance = 457;
            this.splitContainer3.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 267;
            this.label6.Text = "销 售 订 单";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 266;
            this.label5.Text = "销售订单项目";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(252, 373);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 18);
            this.pictureBox1.TabIndex = 265;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.PicBoxWO_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(5, 374);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(90, 16);
            this.checkBox2.TabIndex = 264;
            this.checkBox2.Text = "外协后 工单";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            this.checkBox2.CheckStateChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // txtSalesItemNo
            // 
            this.txtSalesItemNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesItemNo.Enabled = false;
            this.txtSalesItemNo.Location = new System.Drawing.Point(104, 218);
            this.txtSalesItemNo.Name = "txtSalesItemNo";
            this.txtSalesItemNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesItemNo.TabIndex = 262;
            // 
            // txtSalesOrderNo
            // 
            this.txtSalesOrderNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesOrderNo.Enabled = false;
            this.txtSalesOrderNo.Location = new System.Drawing.Point(104, 176);
            this.txtSalesOrderNo.Name = "txtSalesOrderNo";
            this.txtSalesOrderNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesOrderNo.TabIndex = 261;
            // 
            // PicBoxWO
            // 
            this.PicBoxWO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.BackgroundImage")));
            this.PicBoxWO.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.InitialImage")));
            this.PicBoxWO.Location = new System.Drawing.Point(253, 51);
            this.PicBoxWO.Name = "PicBoxWO";
            this.PicBoxWO.Size = new System.Drawing.Size(27, 18);
            this.PicBoxWO.TabIndex = 260;
            this.PicBoxWO.TabStop = false;
            this.PicBoxWO.Click += new System.EventHandler(this.PicBoxWO_Click);
            // 
            // txtWo
            // 
            this.txtWo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWo.Enabled = false;
            this.txtWo.Location = new System.Drawing.Point(103, 91);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(150, 21);
            this.txtWo.TabIndex = 255;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 254;
            this.label3.Text = "外协前 工单";
            // 
            // ddlWoType
            // 
            this.ddlWoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlWoType.FormattingEnabled = true;
            this.ddlWoType.Location = new System.Drawing.Point(104, 10);
            this.ddlWoType.Name = "ddlWoType";
            this.ddlWoType.Size = new System.Drawing.Size(150, 20);
            this.ddlWoType.TabIndex = 252;
            this.ddlWoType.SelectedIndexChanged += new System.EventHandler(this.ddlWoType_SelectedIndexChanged);
            // 
            // txtWoOrder
            // 
            this.txtWoOrder.Location = new System.Drawing.Point(102, 50);
            this.txtWoOrder.Name = "txtWoOrder";
            this.txtWoOrder.Size = new System.Drawing.Size(150, 21);
            this.txtWoOrder.TabIndex = 251;
            this.txtWoOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWoOrder_KeyDown);
            this.txtWoOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWoOrder_KeyPress);
            // 
            // LblWo
            // 
            this.LblWo.AutoSize = true;
            this.LblWo.Location = new System.Drawing.Point(17, 56);
            this.LblWo.Name = "LblWo";
            this.LblWo.Size = new System.Drawing.Size(71, 12);
            this.LblWo.TabIndex = 250;
            this.LblWo.Text = "工       单";
            // 
            // txtMitemCode
            // 
            this.txtMitemCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtMitemCode.Enabled = false;
            this.txtMitemCode.Location = new System.Drawing.Point(104, 135);
            this.txtMitemCode.Name = "txtMitemCode";
            this.txtMitemCode.Size = new System.Drawing.Size(150, 21);
            this.txtMitemCode.TabIndex = 249;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 139);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 248;
            this.label14.Text = "产品物料编码";
            // 
            // lblWoType
            // 
            this.lblWoType.AutoSize = true;
            this.lblWoType.Location = new System.Drawing.Point(16, 12);
            this.lblWoType.Name = "lblWoType";
            this.lblWoType.Size = new System.Drawing.Size(71, 12);
            this.lblWoType.TabIndex = 246;
            this.lblWoType.Text = "工 单 类 型";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 335);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 258;
            this.label10.Text = "工        厂";
            // 
            // txtPlanCode
            // 
            this.txtPlanCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtPlanCode.Enabled = false;
            this.txtPlanCode.Location = new System.Drawing.Point(104, 332);
            this.txtPlanCode.Name = "txtPlanCode";
            this.txtPlanCode.Size = new System.Drawing.Size(150, 21);
            this.txtPlanCode.TabIndex = 259;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 295);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 247;
            this.label9.Text = "入 库 地 点";
            // 
            // txtWO1
            // 
            this.txtWO1.Location = new System.Drawing.Point(101, 372);
            this.txtWO1.Name = "txtWO1";
            this.txtWO1.Size = new System.Drawing.Size(150, 21);
            this.txtWO1.TabIndex = 263;
            this.txtWO1.Visible = false;
            // 
            // txtlocation
            // 
            this.txtlocation.BackColor = System.Drawing.SystemColors.Menu;
            this.txtlocation.Enabled = false;
            this.txtlocation.Location = new System.Drawing.Point(104, 293);
            this.txtlocation.Name = "txtlocation";
            this.txtlocation.Size = new System.Drawing.Size(150, 21);
            this.txtlocation.TabIndex = 253;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 256;
            this.label4.Text = "车       间";
            // 
            // txtFactory
            // 
            this.txtFactory.BackColor = System.Drawing.SystemColors.Menu;
            this.txtFactory.Enabled = false;
            this.txtFactory.Location = new System.Drawing.Point(104, 251);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.Size = new System.Drawing.Size(150, 21);
            this.txtFactory.TabIndex = 257;
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(179, 400);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(72, 24);
            this.Reset.TabIndex = 245;
            this.Reset.Tag = "button1";
            this.Reset.Text = "重置";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Set
            // 
            this.Set.Location = new System.Drawing.Point(94, 399);
            this.Set.Name = "Set";
            this.Set.Size = new System.Drawing.Size(72, 24);
            this.Set.TabIndex = 244;
            this.Set.Tag = "button1";
            this.Set.Text = "设置";
            this.Set.UseVisualStyleBackColor = true;
            this.Set.Click += new System.EventHandler(this.Set_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SapSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 67);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            // 
            // SapSave
            // 
            this.SapSave.Location = new System.Drawing.Point(170, 20);
            this.SapSave.Name = "SapSave";
            this.SapSave.Size = new System.Drawing.Size(83, 32);
            this.SapSave.TabIndex = 51;
            this.SapSave.Tag = "button1";
            this.SapSave.Text = "入库";
            this.SapSave.UseVisualStyleBackColor = true;
            this.SapSave.Click += new System.EventHandler(this.Save_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(534, 3);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(89, 32);
            this.Save.TabIndex = 50;
            this.Save.Tag = "button1";
            this.Save.Text = "入库";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // lstView
            // 
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 0);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(1020, 72);
            this.lstView.TabIndex = 1;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 564);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1020, 72);
            this.panel2.TabIndex = 5;
            // 
            // FormNormalStorageCarton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 636);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FormNormalStorageCarton";
            this.Text = "批量整托入库";
            this.Load += new System.EventHandler(this.ProductStorage_Load);
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.GpbCartonQuery.ResumeLayout(false);
            this.GpbCartonQuery.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.CartonQueryList.ResumeLayout(false);
            this.CartonQueryList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxCarton)).EndInit();
            this.GpbWoQuery.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox GpbCartonQuery;
        private System.Windows.Forms.GroupBox GpbWoQuery;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chbSelected;
        private System.Windows.Forms.PictureBox PicBoxCarton;
        private System.Windows.Forms.Panel CartonQueryList;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox txtSalesItemNo;
        private System.Windows.Forms.TextBox txtSalesOrderNo;
        private System.Windows.Forms.PictureBox PicBoxWO;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ddlWoType;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Label LblWo;
        private System.Windows.Forms.TextBox txtMitemCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblWoType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPlanCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWO1;
        private System.Windows.Forms.TextBox txtlocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFactory;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button Set;
        private System.Windows.Forms.Button SapSave;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewLinkColumn CartonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn NormalWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReworkWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn WaiXieWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Workshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackingLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}