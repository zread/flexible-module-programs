﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class frmMaterial : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private static frmMaterial theSingleton = null;
        private static Form parentfm = null;
        private const int London = 9;
		private const int Guelph = 10;
		private static string CentrDB = FormCover.CenterDBConnString;
		private static string CSIDB = FormCover.InterfaceConnString;
        public static void Instance(Form fm)
        {
            parentfm = fm;
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new frmMaterial();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        public frmMaterial()
        {
            InitializeComponent();
        }

        #region 私有变量

        // 工单类型
        private string WoType = "";
       
        // 全局变量记录白色，绿色数据行
        private List<string> lstWhite = new List<string>();        
        private List<string> lstGreen = new List<string>(); 
        private List<string> lstSn = new List<string>();
       

        #endregion

        //page_load事件
        private void frmMaterial_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            
            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, LanguageHelper.GetMessage(LanMessList, "Message1", "普通生产工单"));
            this.ddlWoType.Items.Insert(2,LanguageHelper.GetMessage(LanMessList, "Message2", "返工生产工单"));
            this.ddlWoType.SelectedIndex = 0;

            //gridview2设置选择格式
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        #region 工单查询

        //点击查询菜单按钮
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            //if (this.ddlWoType.SelectedIndex == 0)
            //{
            //    MessageBox.Show("请选择工单类型!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    this.ddlWoType.Focus();
            //    return;
            //}

            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), FormCover.WOTypeCode.Trim());
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
        }

        //把查询出的菜单显示在界面上
        private void SetWOData(bool flag)
        {
            ClearData(false);

            if (Convert.ToString(this.txtWoOrder.Text.Trim()).Equals(""))
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "输入的工单为空，请确认！"),"Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }

            //DataTable wo = ProductStorageDAL.GetWoInfo(Convert.ToString(this.txtWoOrder.Text.Trim()), WoType);
            //if (wo != null && wo.Rows.Count > 0)
            //{
            //    //SetWoInfo(wo, WoType);
            //}
            //else
            //{
            //    if (flag)
            //    {
            //        MessageBox.Show("工单：" + Convert.ToString(this.txtWoOrder.Text.Trim()) + " 没有从SAP下载或工单类型不一致,请确认!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //         this.txtWoOrder.SelectAll();
            //        this.txtWoOrder.Focus();
            //        return;
            //    }
            //    else
            //    {
            //        MessageBox.Show("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "在SAP没有做物料转储,请确认!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        this.txtWoOrder.SelectAll();
            //        this.txtWoOrder.Focus();
            //        return;
            //    }
            //}
            //this.txtWoOrder.Clear();
        }
        

        // 初始化界面：工单信息维护
        private void ClearData(bool flag)
        {
            if (flag)
                this.ddlWoType.SelectedIndex = -1;
        }

        #endregion
//
//        private void txtWoOrder_TextChanged(object sender, EventArgs e)
//        {
//            txtModulePrefix.Text = txtWoOrder.Text;
//        }

//        private void chbModulePrefix_CheckedChanged(object sender, EventArgs e)
//        {
//            if (chbModulePrefix.Checked == true)
//                txtModulePrefix.Enabled = true;
//            else
//                txtModulePrefix.Enabled = false;
//
//        }

        //清空dataGridView1,lstWhite,lstGreen
        private void ClearDataGridViewData_1()
        {
            lstWhite.Clear();
            lstGreen.Clear();
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }

        //清空dataGridView2
        private void ClearDataGridViewData_2()
        {
            if (this.dataGridView2.Rows.Count > 0)
                this.dataGridView2.Rows.Clear();
        }



        private void btnSelect_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData_2();
            string QueryItem = (string.IsNullOrEmpty(this.CbLinenumber.Text.Trim().ToString()))?"":"C"+this.CbLinenumber.Text.Trim().ToString();
            SetDataGridViewData_2(QueryItem);
        }
        
        private void TxtWoOrderTextChanged(object sender, EventArgs e)
        {
        	ClearDataGridViewData_2();        	
        	
        }
        
         private void txtwoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
         	 if (e.KeyChar == '\r' && this.txtWoOrder.Text.Trim().ToString().Length >= 2 )
             {
         	 	SetDataGridViewData_2(this.txtWoOrder.Text.Trim().ToString().Substring(0,2));
         	 }
        }

        //填充dataGridView2
        private void SetDataGridViewData_2(string templateName)
        {
            DataTable dt = ProductStorageDAL.GetMaterialTemplateInfo(templateName);

            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                	//Jacky
                	if(ProductStorageDAL.GetQtyLeftInTemplate(row[0].ToString()) == 0)
                	  continue;
                	this.dataGridView2.Rows.Add();
                    this.dataGridView2.Rows[RowNo].Cells["name"].Value = Convert.ToString(row[0]);
                    this.dataGridView2.Rows[RowNo].Cells["desc"].Value = Convert.ToString(row[1]);
                    this.dataGridView2.Rows[RowNo].Cells["sysID"].Value = Convert.ToString(row[2]);
                    this.dataGridView2.Rows[RowNo].Cells["QtyAvailable"].Value = Convert.ToString(ProductStorageDAL.GetQtyLeftInTemplate(row[0].ToString()));
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message9", "没有查询到数据！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
		private string SqlCheck ="";
        private void btnBind_Click(object sender, EventArgs e)
        {
        	List<string> inlstwhite = new List<string>();
        	List<string> outLstwhite = new List<string>();
            if (this.dataGridView2.SelectedRows.Count <= 0)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message10", "请先选择物料模板！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return; 
            }

            string templateSysID = this.dataGridView2.SelectedRows[0].Cells["sysID"].Value.ToString();
			
            if (lstWhite.Count <= 0)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message11", "请先选择白色行(未设定模板的组件)！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
				
            
            
            	string Porder = "";
            	string workorder = "";
            
            try {
					SqlCheck = @"select OrderNo From [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] where [SYSID] = '"+ templateSysID +"'";
					DataTable dtCheck = ToolsClass.GetDataTable(SqlCheck);
					if(dtCheck != null && dtCheck.Rows.Count > 0)
						workorder = dtCheck.Rows[0][0].ToString();
					else
					{
						MessageBox.Show("No Parent order found with selected template!");
            			return;
					}
					SqlCheck = @"select resv05 FROM [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where orderNo = '{0}' order by CreatedOn";
	           		SqlCheck = string.Format(SqlCheck,workorder);
	           		dtCheck.Rows.Clear();
	           		dtCheck = ToolsClass.GetDataTable(SqlCheck);
	           		if(dtCheck != null && dtCheck.Rows.Count > 0)
	           			Porder = dtCheck.Rows[0][0].ToString();
					#region commented for connection leak
//					SqlDataReader rd = ToolsClass.GetDataReader(SqlCheck);
//					if(rd!=null&&rd.Read())
//						workorder = rd.GetString(0);
//            		else
//            		{
//            			MessageBox.Show("No Parent order found with selected template!");
//            			return;
//            		}
//            		SqlCheck = @" select resv05 FROM [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where orderNo = '{0}' order by CreatedOn";
//	           		SqlCheck = string.Format(SqlCheck,workorder);
//	           		rd = ToolsClass.GetDataReader(SqlCheck);
//	           		while(rd.Read())
//	           		Porder = rd.GetString(0);
					#endregion
				} catch (Exception E) {
					
					throw E;
				}
				if(Porder.Length != 9)
				{
					MessageBox.Show("Cannot find corresponding parent porduction order, please maintain in SAP!");
					return;
				}
					
           		foreach(string s in lstWhite)
           		{
           			string PO = GetPONumber(s);        	
           			string sequence = PO.Substring(6,3);
           			string Line = PO.Substring(1,1);
           			
           			if (sequence.Equals(Porder.Substring(Porder.Length - 3,3))&& Line.Equals(Porder.Substring(1,1))&&Porder.Substring(2,2).Equals(DateTime.Now.Year.ToString().Substring(2,2))) {
		        		inlstwhite.Add(s);
		        	} 
           			else
           				outLstwhite.Add(s);
           		}
           		if(outLstwhite.Count > 0)
           		MessageBox.Show("List of "+ outLstwhite.Count.ToString() + " serial numbers not under parent order: "+ Porder);
            int retval =0;
            //retval = ProductStorageDAL.AddSNandMaterialTemplateLink(templateSysID, lstWhite);
            retval = ProductStorageDAL.AddSNandMaterialTemplateLink(templateSysID, inlstwhite);
            if (retval > 0)
            {
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message12", "设定物料模板成功！新增了数据条数：") + retval.ToString() + "", 
                    "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Gen_SNClick(sender,e);
            	// btnGenerateSN_Click(sender,e);
                return;
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message13", "设定物料模板失败！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 return;
            }
        }

        private void btnDefine_Click(object sender, EventArgs e)
        {
            frmMaterialTemplate.Instance(parentfm);
        }

        private void btnUnbind_Click(object sender, EventArgs e)
        {
            //string templateSysID = this.dataGridView2.SelectedRows[0].Cells["sysID"].Value.ToString();

            if (lstGreen.Count <= 0)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message14", "请先选择绿色行(已设定模板，尚未入库)！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 return;
            }

            int retval = 0;
            retval = ProductStorageDAL.DeleteSNandMaterialTemplateLink(lstGreen);
            if (retval > 0)
            {
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message15", "删除物料模板成功！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 Gen_SNClick(sender,e);
                //btnGenerateSN_Click(sender, e);
                return;
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message16", "删除物料模板失败！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void SelectGridRowsbyColor(string rowcolor)
        {
            lstWhite.Clear();
            lstGreen.Clear();

            if (dataGridView1.Rows.Count>0)
            {
                int RowNo = 0;
                while (RowNo < dataGridView1.Rows.Count)
                {
                    if (this.dataGridView1.Rows[RowNo].Cells["rowcolor"].Value.ToString() == rowcolor)
                    {
                        this.dataGridView1.Rows[RowNo].Cells["Selected"].Value = true;
                        if (rowcolor == "White")
                        {
                            lstWhite.Add(dataGridView1.Rows[RowNo].Cells["SN"].Value.ToString());
                        }
                        else if (rowcolor == "Green")
                        {
                            lstGreen.Add(dataGridView1.Rows[RowNo].Cells["SN"].Value.ToString());
                        }
                    }
                    else
                    {
                        this.dataGridView1.Rows[RowNo].Cells["Selected"].Value = false;
                    }
                    RowNo++;
                }
                dataGridView1.Refresh();
            }
        }
        private void radioButtonWhite_Click(object sender, EventArgs e)
        {
            //if(radioButtonWhite.Checked == true)
            if (dataGridView1.Rows.Count <= 0)
            {
                radioButtonWhite.Checked = false;
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message17", "没有白色行(未设定模板的组件)！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 return;
            }
            SelectGridRowsbyColor("White");
        }

        private void radioButtonGreen_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count <= 0)
            {
                radioButtonGreen.Checked = false;
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message18", "没有绿色行(已设定模板，尚未入库)！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 return;
            }
            SelectGridRowsbyColor("Green");
        }

        private void btnUpdateBind_Click(object sender, EventArgs e)
        {
			List<string> inlstwhite = new List<string>();
        	List<string> outLstwhite = new List<string>();           
        	if (this.dataGridView2.SelectedRows.Count <= 0)
            {
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message10", "请先选择物料模板！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 return;
            }

            string templateSysID = this.dataGridView2.SelectedRows[0].Cells["sysID"].Value.ToString();

            if (lstGreen.Count <= 0)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message14", "请先选择绿色行(已设定模板，尚未入库)！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            	
            	string Porder = "";
            	string workorder = "";
            
            try {
					SqlCheck = @"select OrderNo From [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] where [SYSID] = '"+ templateSysID +"'";
					DataTable dtCheck = ToolsClass.GetDataTable(SqlCheck);
					if(dtCheck != null && dtCheck.Rows.Count > 0)
						workorder = dtCheck.Rows[0][0].ToString();
					else
					{
						MessageBox.Show("No Parent order found with selected template!");
            			return;
					}
					SqlCheck = @"select resv05 FROM [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where orderNo = '{0}' order by CreatedOn";
	           		SqlCheck = string.Format(SqlCheck,workorder);
	           		dtCheck.Rows.Clear();
	           		dtCheck = ToolsClass.GetDataTable(SqlCheck);
	           		if(dtCheck != null && dtCheck.Rows.Count > 0)
	           			Porder = dtCheck.Rows[0][0].ToString();
					
					
					#region commented for connection leak
//					SqlDataReader rd = ToolsClass.GetDataReader(SqlCheck);
//					if(rd!=null&&rd.Read())
//						workorder = rd.GetString(0);
//            		else
//            		{
//            			MessageBox.Show("No Parent order found with selected template!");
//            			return;
//            		}
//            		SqlCheck = @" select resv05 FROM [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where orderNo = '{0}' order by CreatedOn";
//	           		SqlCheck = string.Format(SqlCheck,workorder);
//	           		rd = ToolsClass.GetDataReader(SqlCheck);
//	           		while(rd.Read())
//	           		Porder = rd.GetString(0);
	           		#endregion
	           		
				} catch (Exception E) {
					
					throw E;
				}
				if(Porder.Length != 9)
				{
					MessageBox.Show("Cannot find corresponding parent porduction order, please maintain in SAP!");
					return;
				}
					foreach(string s in lstGreen)
           		{
           			string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+s+"')";
        			string sequence = "";
        			string Line = "";
        			DataTable dtCheck = ToolsClass.GetDataTable(sql);
        			if(dtCheck != null && dtCheck.Rows.Count > 0)
        			{
        				sequence = dtCheck.Rows[0][0].ToString().Substring(6,3);
        				Line = dtCheck.Rows[0][0].ToString().Substring(1,1);
        			}
        			else
		        	{
		        		sequence = s.Substring(7,3);		        	
			        	int lineNumber = Convert.ToInt32(s.Substring(5,2));
			        	if(lineNumber > 9 && lineNumber < 20 )
			        		Line = "A";
			        	else if(lineNumber > 19 && lineNumber < 30 )
			        		Line = "B";
			        	else if(lineNumber > 29 && lineNumber < 40 )
			        		Line = "C";
			        	else if(lineNumber > 39 && lineNumber < 50 )
			        		Line = "D";
		        	}
        			
        			#region commented for connection leak
//        			SqlDataReader rd = ToolsClass.GetDataReader(sql);
//        			if(rd!=null && rd.Read())
//		        	{
//		        		sequence = rd.GetString(0).Substring(6,3);
//		        		Line = rd.GetString(0).Substring(1,1);
//		        	}
//		        	else
//		        	{
//		        		sequence = s.Substring(7,3);		        	
//			        	int lineNumber = Convert.ToInt32(s.Substring(5,2));
//			        	if(lineNumber > 9 && lineNumber < 20 )
//			        		Line = "A";
//			        	else if(lineNumber > 19 && lineNumber < 30 )
//			        		Line = "B";
//			        	else if(lineNumber > 29 && lineNumber < 40 )
//			        		Line = "C";
//			        	else if(lineNumber > 39 && lineNumber < 50 )
//			        		Line = "D";
//		        	}
		        	#endregion
		     
           			if (sequence.Equals(Porder.Substring(Porder.Length - 3,3))&& Line.Equals(Porder.Substring(1,1))&&Porder.Substring(2,2).Equals(DateTime.Now.Year.ToString().Substring(2,2))) {
		        		inlstwhite.Add(s);
		        	} 
           			else
           				outLstwhite.Add(s);
           		}
			if(outLstwhite.Count > 0)
           		MessageBox.Show("List of "+ outLstwhite.Count.ToString() + " serial numbers not under parent order: "+ Porder);
            int retval = 0;
            retval = ProductStorageDAL.UpdateSNandMaterialTemplateLink(templateSysID, inlstwhite);
            //retval = ProductStorageDAL.UpdateSNandMaterialTemplateLink(templateSysID, lstGreen);
            if (retval > 0)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message19", "设定物料模板成功！新增了数据条数:") + retval.ToString(),
                    "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
               Gen_SNClick(sender,e);
            	//btnGenerateSN_Click(sender, e);
                return;
            }
            else
            {
               MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message20", "设定物料模板失败！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {          
               if (e.ColumnIndex == 0 && e .RowIndex != -1)            
               {   //获取控件的值
                   string currentSN = dataGridView1.Rows[e.RowIndex].Cells["SN"].Value.ToString();
                   string currentColor = dataGridView1.Rows[e.RowIndex].Cells["rowcolor"].Value.ToString(); 
                   bool currentSelected =  Convert.ToBoolean(this.dataGridView1.Rows[e.RowIndex].Cells[0].EditedFormattedValue.ToString());

                   if (currentSelected && currentColor == "White")
                   {
                       if (!lstWhite.Contains(currentSN))
                           lstWhite.Add(currentSN);
                   }
                   else if (!currentSelected && currentColor == "White")
                   {
                       if (lstWhite.Contains(currentSN))
                           lstWhite.Remove(currentSN);
                   }
                   else if (currentSelected && currentColor == "Green")
                   {
                       if (!lstGreen.Contains(currentSN))
                           lstGreen.Add(currentSN);
                   }
                   else if (!currentSelected && currentColor == "Green")
                   {
                       if (lstGreen.Contains(currentSN))
                           lstGreen.Remove(currentSN);
                   }

               }
        }

        private void radioButtonNothing_Click(object sender, EventArgs e)
        {
            lstGreen.Clear();
            lstWhite.Clear();

            if (dataGridView1.Rows.Count > 0)
            {
                int RowNo = 0;
                while (RowNo < dataGridView1.Rows.Count)
                {
                    this.dataGridView1.Rows[RowNo].Cells["Selected"].Value = false;
                    RowNo++;
                }
                dataGridView1.Refresh();
            }

        }
        
        #region Change
                
    
			
        
           private void SetDataGridViewData_1(List<string> temp)
        {
           

            int RowNo = 0;
            int RowIndex = 1;

            try
            {
                DataTable dt = ProductStorageDAL.GetSnInfoUsingSetofSn(temp);

                #region 1:处理数据库中已经存在的
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[RowNo].Cells["SN"].Value = Convert.ToString(row[0]);
                        this.dataGridView1.Rows[RowNo].Cells["MaterialTemplateID"].Value = Convert.ToString(row[1]);
                        this.dataGridView1.Rows[RowNo].Cells["PostedOn"].Value = Convert.ToString(row[2]);
                        this.dataGridView1.Rows[RowNo].Cells["LastUpdateUser"].Value = Convert.ToString(row[3]);
                        this.dataGridView1.Rows[RowNo].Cells["LastUpdateTime"].Value = Convert.ToString(row[4]);
                        this.dataGridView1.Rows[RowNo].Cells["MaterialTemplateName"].Value = Convert.ToString(row[5]);

                        if (!string.IsNullOrEmpty(Convert.ToString(row[2])))//如果此字段非空，表示已入库，为红色，不能修改
                        {
                            this.dataGridView1.Rows[RowNo].Cells["rowcolor"].Value = "Red";

                            this.dataGridView1.Rows[RowNo].Cells["Selected"].ReadOnly = true;

                            this.dataGridView1.Rows[RowNo].DefaultCellStyle.BackColor = Color.Red;
                        }
                        else
                        {
                            this.dataGridView1.Rows[RowNo].Cells["rowcolor"].Value = "Green";
                            this.dataGridView1.Rows[RowNo].DefaultCellStyle.BackColor = Color.Green;
                        }
                        RowNo++;
                        RowIndex++;

                        temp.Remove(Convert.ToString(row[0]));
                    }

                }
                #endregion

                #region 2:处理数据库中不存在的
                if (lstSn.Count > 0)
                {
                    foreach (string s in temp)
                    {
                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[RowNo].Cells["SN"].Value = s;
                        this.dataGridView1.Rows[RowNo].Cells["MaterialTemplateID"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["PostedOn"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["LastUpdateUser"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["LastUpdateTime"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["rowcolor"].Value = "White";

                        this.dataGridView1.Rows[RowNo].DefaultCellStyle.BackColor = Color.White;
                        RowNo++;
                        RowIndex++;
                    }

                }
                #endregion

                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
      

        void Gen_SNClick(object sender, EventArgs e)
        {
        	
        	      	
        			lstSn.Clear();
        			ClearDataGridViewData_1();
        			

        			Thread.Sleep(50);
        			
//        			if(txtWoOrder.Text.ToString().Trim().Length <=0)
//            		{
//              			 MessageBox.Show("Please select Production order！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
//               			 txtWoOrder.Focus();
//               			 return;
//           			}
//        	  
           
                    string strTemp = CartonNo.Text;                    
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                        DataTable dt = null; 
                        dt = ProductStorageDAL.GetSNStroageBC(sDataSet[i]);
                        
                       
                        if(dt != null)
                        {
                        	foreach (DataRow row in dt.Rows)
                   		 {                                       
                        	lstSn.Add(Convert.ToString(row[0]));                                                
                         }                        	
                        
                        }
                    }
                     
                    try
         		   {
                     SetDataGridViewData_1(lstSn);               		 
            		}
           			 catch (Exception ex)
            		{
               		 MessageBox.Show(ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
           			 }
       			
        	
        }
		void BtLinkClick(object sender, EventArgs e)
		{
			string strTemp = CartonNo.Text; 
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                        if(sDataSet[i] == "" || string.IsNullOrEmpty(sDataSet[i]))
                        	continue;
                        if(sDataSet[i].Length != 12)
                        	continue;
                        if(IsStockin(sDataSet[i]))
                        	continue;
                        
                        string sql = @"Select SN from product where boxid = '{0}'";
                        sql = string.Format(sql,sDataSet[i]);
                        DataTable dt = ToolsClass.GetDataTable(sql);
                        foreach ( DataRow row in dt.Rows) {
                        	LinkToTemplate(row[0].ToString());
                        }
                    }
                    Gen_SNClick(sender,e);
		}
		
		bool IsStockin(string cartonno)
		{
			string sql = @"select * from TSAP_RECEIPT_UPLOAD_MODULE where CARTON_NO = '"+ cartonno +"' and UPLOAD_STATUS <>'Canceled'";
			DataTable dt = ToolsClass.GetDataTable(sql);
			if(dt!= null && dt.Rows.Count > 0)
			{
				MessageBox.Show("Carton "+cartonno+" already stocked in. cannot re-link!");
				return true;
			}
			return false;
		}
		void LinkToTemplate(string SN)
		{
			string POsequence = "";
			string Line = "";
			string ParentPo = "";
			string syr =SN.Substring(1,2);
			string templateName = "";
			string tempSYSID = "";
//			
			
			string TEMPPO = GetPONumber(SN);
			POsequence = TEMPPO.Substring(6,3);
			Line = TEMPPO.Substring(1,1);
			
			ParentPo = "C"+Line+syr+"__"+POsequence;
			if(ExistTracebility(ParentPo))
			{
				MessageBox.Show("Exist in Tracebility,please contact engineering.");				
				return;
			}
			string sql = @"select distinct orderno from [TSAP_WORKORDER_SALES_REQUEST] with(nolock) where resv05 like '{0}' and LEN(OrderNo) = {1} ";
			int orderLen = (Line == "C")?London:Guelph;			
			sql =string.Format(sql,ParentPo,orderLen);
			DataTable dt = ToolsClass.GetDataTable1(sql,CSIDB);
			if(dt.Rows.Count < 1)
			{
				ToolsClass.SendEmail(SN+ " Production order not download!");
				MessageBox.Show("Production order not download,please contact engineering.");
				sql = @" insert into [AutoLinkTracebility] values('{0}','Production order not download', getdate(), 1)";
				sql = string.Format(sql,ParentPo);
				ToolsClass.ExecuteNonQuery(sql);
				return;
			}
			string[] ChildPo = new string[dt.Rows.Count];
			int i = 0;
			foreach (DataRow row in dt.Rows) {
				ChildPo[i] = row[0].ToString();
				i++;
			}
			sql = @"  select MaterialTemplateName,OrderNo,SYSID from
				(select ROW_NUMBER()over(partition by orderno order by operatorDate ) as r, orderno,MaterialTemplateName,SYSID,operatorDate from [T_PACK_MATERIAL_TEMPLATE] with(nolock) where  OrderNo in ('{0}')
				)a where r = 1 order by OperatorDate";
			sql = string.Format(sql, string.Join("','", ChildPo));
			dt.Clear();
			dt = ToolsClass.GetDataTable1(sql,CentrDB);
			
			foreach (DataRow row in dt.Rows) {
				int Qty = ProductStorageDAL.GetQtyLeftInTemplate(row[0].ToString());
				if(Qty != 0)
				{
					templateName = row[0].ToString();
					tempSYSID = row[2].ToString();
					break;
				}				
			}
			
			if(string.IsNullOrEmpty(tempSYSID) || tempSYSID == "")
			{				
				ToolsClass.SendEmail(SN + " Template not found or full!");
				sql = @" insert into [AutoLinkTracebility] values('{0}','Template not found or full', getdate(), 1)";
				sql = string.Format(sql,ParentPo);
				ToolsClass.ExecuteNonQuery(sql);
				return;
			}
			int rev = 0;
			if(SNinTemplate(SN))
				rev = UpdateSNandMaterialTemplateLink(tempSYSID, SN);
			else
				rev = AddSNandMaterialTemplateLink(tempSYSID, SN);			
		}
	
			private bool SNinTemplate(string SN)
		{
			string sql =  @"select * from [T_PACK_MATERIAL_TEMPLATE_MODULE_LK] where [MODULE_SN] = '{0}'";
			sql = string.Format(sql,SN);
			SqlDataReader rd = ToolsClass.GetDataReader(sql,CentrDB);
			if(rd!=null && rd.Read())
				return true;
			return false;		
			
		}
		
		private string getPOsequence( string serial, out string line)
        {
        
        				string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+serial+"')";
			        	string sequence = "";
			        	line = "";
			        	SqlDataReader rd = ToolsClass.GetDataReader(sql);
			        	if(rd!=null && rd.Read())
			        	{
			        		sequence = rd.GetString(0).Substring(6,3);
			        		line = rd.GetString(0).Substring(1,1);
			        	}
			        	else 
			        	{
			        		sequence = serial.Substring(7,3);
			        		int lineNumber = Convert.ToInt32(serial.ToString().Substring(5,2));
							if(lineNumber > 9 && lineNumber < 20 )
								line = "A";
							else if(lineNumber > 19 && lineNumber < 30 )
								line = "B";
							else if(lineNumber > 29 && lineNumber < 40 )
								line = "C";
							else if(lineNumber > 39 && lineNumber < 50 )
								line = "D";
			        	}
        	return sequence;
        }
		
		private bool InReworkOrder(string SN)
		{
			string sql = @"select * from T_REG_MODULE_REWORK WHERE MODULE_SN = '{0}'";
			sql  = string.Format(sql,SN);
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
				return true;
			return false;
			
		}
		private bool ExistTracebility(string Po)
		{
			string sql = @"select * from [AutoLinkTracebility] with(nolock) where PO = '"+Po+"' and Status = 1";
			SqlDataReader rd = ToolsClass.GetDataReader(sql);
			if(rd.Read() && rd != null)
				return true;
			return false;
		}
		
		
			private int AddSNandMaterialTemplateLink(string TemplateSysID, string SN)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(CSIDB);
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

            try
            {
                    string strSqlInsert = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                        ([MODULE_SN],[PACK_MA_SYSID],[LASTUPDATETIME],[LASTUPDATEUSER])
                        VALUES('{0}','{1}','{2}','{3}')";
                    strSqlInsert = string.Format(strSqlInsert, SN, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "SYSTEM");

                    cmd.CommandText = strSqlInsert;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
             		sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }
        }
		      
		private int UpdateSNandMaterialTemplateLink(string TemplateSysID, string SN)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(CSIDB);
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

  
            try
            {
            	
                    string strSqlUpdate = @"UPDATE [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                                        SET [PACK_MA_SYSID] = '{0}',[LASTUPDATETIME]='{1}',[LASTUPDATEUSER]='{2}'
                                        WHERE [MODULE_SN] = '{3}'";
                    strSqlUpdate = string.Format(strSqlUpdate, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "SYSTEM", SN);

                    cmd.CommandText = strSqlUpdate;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
                sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }


        }
		   private string GetPONumber(string SN)
        {
        	if (string.IsNullOrEmpty(SN)) {
        		MessageBox.Show("Serial number: "+SN+" does not have a matched PO, please check!");
        		return "";
        	}
        	
        	string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+SN+"')";
        	string sequence = "";
        	string Line = "";
        	
        	string syr = SN.Substring(1,2);
        	string sMonth = SN.Substring(3,2);
        	SqlDataReader rd = ToolsClass.GetDataReader(sql);
        	if(rd!=null && rd.Read())
        	{
        		sequence = rd.GetString(0).Substring(6,3);
		        Line = rd.GetString(0).Substring(1,1);
        	}
        	else
        	{
        		sequence = SN.Substring(7,3);
	        	int lineNumber = Convert.ToInt32(SN.Substring(5,2));


                //Changeover on 2016/10/01
                if ((Convert.ToInt16(syr) == 16 && Convert.ToInt16(sMonth) > 9) || (Convert.ToInt16(syr) > 16))
                {
		        	switch (lineNumber) {
		        			case 40:
		        					Line = "A";
		        					break;
		        				case 41:
		        					Line = "A";
		        					break;
		        				case 42:
		        					Line = "B";
		        					break;
		        				case 43:
		        					Line = "B";
		        					break;
		        				case 44:
		        					Line = "C";
		        					break;
		        				case 45:
		        					return GetPONumber(getOriginalSn(SN));
		        					break;
		        	}
	        	}
	        	else
	        	{
		        	if(lineNumber > 9 && lineNumber < 20 )
		        		Line = "A";
		        	else if(lineNumber > 19 && lineNumber < 30 )
		        		Line = "B";
		        	else if(lineNumber > 29 && lineNumber < 40 )
		        		Line = "C";
		        	else if(lineNumber > 39 && lineNumber < 50 )
		        		Line = "D";
	        	}
        	}
        	
        	return "C"+Line+syr+sMonth+sequence;
        	
        	
        }
        
        private string getOriginalSn(string SN)
        {
        	string sql = @" select DuplicatedSN from SapDuplicationSN where NewSN = '{0}'";
	        		sql = string.Format(sql,SN);
	        		DataTable OriginalSn = ToolsClass.GetDataTable1(sql);
	        		if (OriginalSn != null && OriginalSn.Rows.Count > 0 ) {
	        			return OriginalSn.Rows[0][0].ToString();
	        		}
	        		return "";
        }
		
        #endregion      
       
    }
}
