﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    /// <summary>
    /// v2.2
    /// 常熟厂区Report Format
    /// </summary>
    public partial class FormAnalyseCS : Form
    {

        private static List<LanguageItemModel> LanMessList;
        DataTable prodDateSource;//存储报表查询结果
        private int reportFlag = -1;
        private bool JobNoIsEmpty = false;

        private FormAnalyseCS()
        {
            InitializeComponent();

            prodDateSource = new DataTable();
            upSourcd();
            tbCPTPackingQuerySQL.Name = "tbCPTCSPackingQuerySQL";
            tbCPTPackingQuerySQL.Text = ToolsClass.getSQL(tbCPTPackingQuerySQL.Name);
            dateFrom.Value = DateTime.Today;
            dateTo.Value = DateTime.Today.AddDays(1);
        }

        public FormAnalyseCS(Form fm, int iReportFormat)
        {
            InitializeComponent();


            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
           
            this.reportFlag = iReportFormat;

            prodDateSource = new DataTable();
            upSourcd();

            this.MdiParent = fm;
            this.WindowState = FormWindowState.Maximized;
            this.Show();

            if (iReportFormat == ToolsClass.iDateReport)
            {
                tbCPTPackingQuerySQL.Name = "tbCPTCSPackingQuerySQL";
                this.Text = "Report By Date";
                this.btnQueryByCartonSpan.Visible = true;
                this.txtCartonFrom.Visible = true;
                this.txtCartonTo.Visible = true;
                this.txtCartonNo.Visible = false;
            }
            else if (iReportFormat == ToolsClass.iCartonReport)
            {
                tbCPTPackingQuerySQL.Name = "tbCPTPackingQuerySQLCarton";
                //lblfrom.Text = "内部托号：";

                txtCartonNo.Visible = true;
                lblto.Visible = false;
                dateTo.Visible = false;
                dateFrom.Visible = false; 
                this.Text = "Report By Carton";
            }
            else
            {
                tbCPTPackingQuerySQL.Name = "tbCPTPackingQuerySQLModule";
                lblfrom.Text = "Module Number:";
                txtCartonNo.Visible = true;
                lblto.Visible = false;
                dateTo.Visible = false;
                dateFrom.Visible = false;
                //this.Text = "Report By Module";
                this.Text = "Report By Module";
            }
            if (iReportFormat != ToolsClass.iDateReport)
            {
                tbCPTPackingQuerySQL.Text = ToolsClass.getSQL(tbCPTPackingQuerySQL.Name);
            }
            else
            {
                tbCPTPackingQuerySQL.Text = sqlByDate;
            }         
        }

        private static FormAnalyseCS theSingleton = null;
        public static void Instance(Form fm,bool isDlg)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormAnalyseCS();
                if (isDlg)
                {
                    theSingleton.ShowInTaskbar = false;
                    theSingleton.StartPosition = FormStartPosition.CenterScreen;
                    theSingleton.Shown += new System.EventHandler(theSingleton.FormAnalyseCS_Load);
                    theSingleton.ShowDialog();
                    theSingleton.Dispose();
                }
                else
                {
                    theSingleton.MdiParent = fm;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }
            }
            else
                theSingleton.Activate();
        }
        public static FormAnalyseCS TheSingleton
        {
            get 
            {
                if (theSingleton == null)
                    theSingleton = new FormAnalyseCS();

                return theSingleton;

            }
        }

        private void FormAnalyseCS_Load(object sender, EventArgs e)
        {
            //theSingleton.dateFrom.Value = DateTime.Today;
            //theSingleton.dateTo.Value = DateTime.Today.AddDays(1);
            //btQuery_Click(sender, e);
            if (this.reportFlag == ToolsClass.iDateReport)
            {
                this.dateFrom.Value = DateTime.Today.AddDays(1);
                this.dateTo.Value = DateTime.Today.AddDays(1);
                //注释 By Ning.Yu 2012-03-13
                //string sql = ToolsClass.getSQL("tbCPTCSPackingQuerySQL");

                tbCPTPackingQuerySQL.Text = sqlByDate;
                btQuery_Click(sender, e);


                //this.groupBox1.Text = "按时间区间查询";
                LanguageHelper.GetMessage(LanMessList, "Message7", "按时间区间查询");
                this.groupBox2.Visible = true;
                //this.groupBox3.Text = "以客户托号区间查询";
                LanguageHelper.GetMessage(LanMessList, "Message8", "以客户托号区间查询");
            }
            else
            {
               // this.groupBox1.Text = "按内部托号查询";
                LanguageHelper.GetMessage(LanMessList, "Message9", "按内部托号查询");

                this.groupBox3.Visible = false;

               // this.groupBox2.Text = "以客户托号查询";
                LanguageHelper.GetMessage(LanMessList, "Message10", "以客户托号查询");
               // this.label4.Text = "箱号";
               LanguageHelper.GetMessage(LanMessList, "Message11", "箱号");

                this.label5.Visible = false;
                this.txtCartonFrom.Visible = false;
                this.txtCartonTo.Visible = false;

                this.btnCustCartonQuery.Visible = true;
            }

            dataGridView1.KeyDown += new KeyEventHandler(dataGridView_KeyDown);//进制dataGridView复制
        }

        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
                e.Handled = true;
        }

        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }
        
        /// <summary>
        /// 更新datagridview显示
        /// </summary>
        private void upSourcd()
        {
            int iflag = 0;
            //清除当前显示内容
            int iClassIndex = 0;
            dataGridView1.Rows.Clear();
            //调整表头结构
           // int cols = (int)numericUpDown1.Value - ((dataGridView1.ColumnCount - 7) / 3) - 1;
            int cols = (int)numericUpDown1.Value - ((dataGridView1.ColumnCount - 8) / 4) - 1;
            if (cols > 0)
            {
                DataGridViewColumn col, col2, col3, col4;
                for (int i = 0; i < cols; i++)
                {
                    //add
                    col3 = new DataGridViewColumn(dataGridView1.Columns[3].CellTemplate);
                    col3.HeaderText = dataGridView1.Columns[3].HeaderText;
                    col3.Width = dataGridView1.Columns[3].Width;
                    col3.DefaultCellStyle = dataGridView1.Columns[3].DefaultCellStyle;

                    col = new DataGridViewColumn(dataGridView1.Columns[4].CellTemplate);
                    col.HeaderText = dataGridView1.Columns[4].HeaderText;
                    col.Width = dataGridView1.Columns[5].Width;
                    col.DefaultCellStyle = dataGridView1.Columns[4].DefaultCellStyle;

                    col2 = new DataGridViewColumn(dataGridView1.Columns[5].CellTemplate);   
                    col2.HeaderText = dataGridView1.Columns[5].HeaderText;
                    col2.Width = dataGridView1.Columns[5].Width;
                    col2.DefaultCellStyle = dataGridView1.Columns[5].DefaultCellStyle;


                    //by hexing 增加组件等级
                    col4 = new DataGridViewColumn(dataGridView1.Columns[6].CellTemplate);
                    col4.HeaderText = dataGridView1.Columns[6].HeaderText;
                    col4.Width = dataGridView1.Columns[6].Width;
                    col4.DefaultCellStyle = dataGridView1.Columns[6].DefaultCellStyle;


                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col3); //add
                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col);
                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col2);
                    dataGridView1.Columns.Insert(dataGridView1.ColumnCount - 1, col4);//by hexing

                    
                }
            }
            else if (cols < 0)
            {
                for (int i = 0; i > cols; i--)
                {
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);
                    dataGridView1.Columns.RemoveAt(dataGridView1.ColumnCount - 2);//by hexing

                }
            }
            //显示数据
            string boxID = "", boxIDnew = "", pdcSN = "", pdcSNnew = "";
            int rowNum = 0,reNum=0,rowNumT=0;
            foreach (DataRow row in prodDateSource.Rows)
            {
                pdcSNnew = row[2].ToString();
                if (pdcSN == pdcSNnew)
                {
                    reNum++;
                    continue;
                }
                else
                    pdcSN = pdcSNnew;
                boxIDnew = row[0].ToString();
                if (boxID == boxIDnew)
                {
                    if (cols == numericUpDown1.Value)
                    {
                        iClassIndex++;
                        rowNum = dataGridView1.Rows.Add();
                        dataGridView1.Rows[rowNum].Cells[3].Value = row["StdPower"];
                        dataGridView1.Rows[rowNum].Cells[4].Value = row[2];
                        dataGridView1.Rows[rowNum].Cells[5].Value = ProductStorageDAL.GetDecimalString(Convert.ToString(row[3]), 2);

                        dataGridView1.Rows[rowNum].Cells[6].Value = row["ModuleClass"];//by hexing 显示组件等级
                        //dataGridView1.Rows[rowNum].Cells[6].Value = row[7];//add by alex.dong|2012-03-20
                        cols = 1;
                        if (iClassIndex == 1)
                        {
                            dataGridView1.Rows[rowNum].Cells[2].Value = "打包时间：" + row[5];
                        }
                        if(iClassIndex==2)
                            dataGridView1.Rows[rowNum].Cells[2].Value = "Art No：" + row[7];
                    }
                    else
                    {
                        dataGridView1.Rows[rowNum].Cells[3 + cols * 4].Value = row["StdPower"];
                        dataGridView1.Rows[rowNum].Cells[4 + cols * 4].Value = row[2];
                        dataGridView1.Rows[rowNum].Cells[5 + cols * 4].Value = ProductStorageDAL.GetDecimalString(Convert.ToString(row[3]), 2);
                        dataGridView1.Rows[rowNum].Cells[6 + cols * 4].Value = row["ModuleClass"];
                        //dataGridView1.Rows[rowNum].Cells[6 + cols * 2].Value = row[7];//add by alex.dong|2012-03-20
                        cols++; 
                    }
                }
                else
                {
                    rowNum = dataGridView1.Rows.Add();
                    boxID = boxIDnew;
                    iClassIndex = 0;
                    dataGridView1.Rows[rowNum].Cells[0].Value = row[0];
                    dataGridView1.Rows[rowNum].Cells[1].Value = row[1];// row[6] JobNo查询
                    dataGridView1.Rows[rowNum].Cells[2].Value = "打包人员："+row[4];
                    //dataGridView1.Rows[rowNum].Cells[3].Value = row["StdPower"];//ToolsClass.getNominalPower((double)row[3]);
                    dataGridView1.Rows[rowNum].Cells[3].Value = row["StdPower"]; 
                    dataGridView1.Rows[rowNum].Cells[4].Value = row[2];
                    dataGridView1.Rows[rowNum].Cells[5].Value = ProductStorageDAL.GetDecimalString(Convert.ToString(row[3]), 2);
                    dataGridView1.Rows[rowNum].Cells[6].Value = row["ModuleClass"];

                    //if (string.IsNullOrEmpty(row[6].ToString()))
                    //{
                    //    JobNoIsEmpty = true;
                    //    ToolsClass.message = "JobNo不全，不允许导出，请先维护JobNo!";
                    //    iflag = 1;
                    //}
                    //dataGridView1.Rows[rowNum].Cells[6].Value = row[7];//add by alex.dong|2012-03-20
                    ////for (int i = 1; i <= 1; i++)
                    ////{
                    ////    rowNum = dataGridView1.Rows.Add();
                    ////    switch (i)
                    ////    {
                    ////        case 1:
                    ////            dataGridView1.Rows[rowNum].Cells[1].Value = "打包时间：" + row[5];
                    ////            break;
                    ////    }
                    ////}
                    cols = 1;
                }
            }
            if (iflag == 0)
            {
                JobNoIsEmpty = false;
                ToolsClass.message = "";
            }
            this.Cursor = System.Windows.Forms.Cursors.Default;
           // label2.Text = "共查询到:" + (prodDateSource.Rows.Count - reNum) + " 个组件";
            label2.Text = string.Format(LanguageHelper.GetMessage(LanMessList, "Message2", "共查询到:{0}个组件!"), prodDateSource.Rows.Count - reNum);
        }
        
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //tbSQL.Text = numericUpDown1.Value.ToString();
            if (this.Cursor == System.Windows.Forms.Cursors.WaitCursor)
            {
                MessageBox.Show("Program in processing,Please wait!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                upSourcd();
            }
        }

        private void btOutpu_Click(object sender, EventArgs e)
        {
            //去掉是因为JOBNO的维护，要在SAP维护
            //if (JobNoIsEmpty)
            //{
            //    btnJobNoInput.PerformClick();
            //}
            //else
                ToolsClass.DataToExcel(dataGridView1, true);
        }

        #region SqlQueryCondition
        //以日期查询
        private const string sqlByDate = @"
                            SELECT 
	                            pd2.[BoxID],
	                            pd2.[Cust_BoxID],
	                            pd2.[SN],
	                            [ElecParaTest].[Pmax],
	                            [Box].[PackOperator],
	                            CONVERT(varchar(19),Box.[PackDate],120),
	                            [Box].JobNo,
                                pd2.[Art_No],
                                pd2.[StdPower],
                                pd2.[ModuleClass]

                            FROM 
	                            (
		                            SELECT StdPower, [BoxID],Cust_BoxID,[SN],[InterID],[InterNewTempTableID],[OperatorID],[ProcessDateTime],[Art_No],[ModuleClass]
		                            FROM [Product] 
		                            WHERE [BoxID] in 
		                            (
			                            SELECT distinct [BoxID] 
			                            FROM [Product] 
			                            WHERE len([BoxID]) > 0 
			                            AND [PackDate]>'{0}' 
			                            AND [PackDate]<'{1}'
		                            )
	                            ) AS pd2 
                            inner join [ElecParaTest] ON 
	                            [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] 
                            inner join Box on 
	                            pd2.BoxID=Box.BoxID 
                            order by pd2.[BoxID],pd2.[SN],pd2.[InterID] DESC
";

        private const string sqlByJobNo = @"
                            SELECT 
	                            pd2.[BoxID],
	                            0,
	                            pd2.[SN],
	                            [ElecParaTest].[Pmax],
	                            [Box].[PackOperator],
	                            CONVERT(varchar(19),Box.[PackDate],120),
	                            [Box].JobNo,
                                pd2.[Art_No],
                                pd2.[StdPower],
                               pd2.[ModuleClass]
                            FROM 
	                            (
		                            SELECT [StdPower],[BoxID],[SN],[InterID],[InterNewTempTableID],[OperatorID],[ProcessDateTime],[Art_No],[ModuleClass]
		                            FROM [Product] 
		                            WHERE [BoxID] in 
		                            (
			                            SELECT distinct [BoxID] 
			                            FROM [Product] 
			                            WHERE len([BoxID]) > 0 
		                            )
	                            ) AS pd2 
                            inner join [ElecParaTest] ON 
	                            [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] 
                            inner join Box on 
	                            pd2.BoxID=Box.BoxID 
                            WHERE 1=1 
	                            AND Box.JobNo LIKE '%{0}%' 
                            order by pd2.[BoxID],pd2.[SN],pd2.[InterID] DESC
                            ";
        //以内部托号查询
        private const string sqlByCartonSpan = @"
                        SELECT 
	                        pd2.[BoxID],
	                        pd2.[Cust_BoxID],
	                        pd2.[SN],
	                        [ElecParaTest].[Pmax],
	                        [Box].[PackOperator],
	                        CONVERT(varchar(19),Box.[PackDate],120),
	                        [Box].JobNo,
                            pd2.[Art_No],
                            pd2.StdPower,
                        pd2.[ModuleClass]

                        FROM 
	                        (
		                        SELECT [BoxID],[Cust_BoxID],[SN],[InterID],[InterNewTempTableID],[OperatorID],[ProcessDateTime],[Art_No],StdPower,[ModuleClass]
		                        FROM [Product] 
		                        WHERE [BoxID] in 
		                        (
			                        SELECT distinct [BoxID] 
			                        FROM [Product] 
			                        WHERE len([BoxID]) > 0 
		                        )
	                        ) AS pd2 
                        inner join [ElecParaTest] ON 
	                        [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] 
                        inner join Box on 
	                        pd2.BoxID=Box.BoxID 
                        WHERE 1=1 
                        ";

        //以客户托号查询
        private const string sqlByCustCartonSpan = @"
                        SELECT 
	                        pd2.[BoxID],
	                        pd2.[Cust_BoxID],
	                        pd2.[SN],
	                        [ElecParaTest].[Pmax] AS 'Pmax',
	                        [Box].[PackOperator],
	                        CONVERT(varchar(19),Box.[PackDate],120),
	                        [Box].JobNo,
                            pd2.[Art_No],
                            pd2.StdPower,
                              pd2.[ModuleClass]
                        FROM 
	                        (
		                        SELECT [StdPower],[BoxID],[Cust_BoxID],[SN],[InterID],[InterNewTempTableID],[OperatorID],[ProcessDateTime],[Art_No],[ModuleClass]
		                        FROM [Product] 
		                        WHERE [Cust_BoxID] in 
		                        (
			                        SELECT distinct [Cust_BoxID] 
			                        FROM [Product] 
			                        WHERE len([Cust_BoxID]) > 0 
		                        )
	                        ) AS pd2 
                        inner join [ElecParaTest] ON 
	                        [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] 
                        inner join Box on 
	                        pd2.Cust_BoxID=Box.Cust_BoxID 
                        WHERE 1=1 
                        ";
        #endregion

        private const string sqlOrderBy = @" order by pd2.[BoxID],pd2.[SN],pd2.[InterID] DESC";
        private QueryType _QueryType = QueryType.ByDate;

        /// <summary>
        /// Query by Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btQuery_Click(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            string sqlStr = "";
            if (this.reportFlag == ToolsClass.iDateReport)
            {
                string toTime = dateTo.Value.ToString("yyyy-MM-dd HH:mm");
                string fromTime = dateFrom.Value.ToString("yyyy-MM-dd HH:mm");
                sqlStr = string.Format(tbCPTPackingQuerySQL.Text, fromTime, toTime);
                ToolsClass.GetDataTable(sqlStr, prodDateSource);

                _QueryType = QueryType.ByDate;

            }
            else
            {
                sqlStr = string.Format(tbCPTPackingQuerySQL.Text, txtCartonNo.Text.Trim());
                if (!txtCartonNo.Text.Trim().Equals(""))
                    ToolsClass.GetDataTable(sqlStr, prodDateSource);
                _QueryType = QueryType.ByCarton;

            }

            //更新显示
            upSourcd();
            
        }

        private void btSaveSQL_Click(object sender, EventArgs e)
        {
            //保存组件装箱报表sql语句 tbCPTBoxSQL
            ToolsClass.saveSQL(tbCPTPackingQuerySQL);
            splitContainer1.Panel2Collapsed = true;
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbCPTPackingQuerySQL.Undo();
        }

        private void btnJobNoInput_Click(object sender, EventArgs e)
        {
            string fromTime = dateFrom.Value.ToString("yyyy-MM-dd HH:mm");
            string toTime = dateTo.Value.ToString("yyyy-MM-dd HH:mm");
                
            string jobNo = this.txtJobNo.Text.Trim().ToUpper();
            string boxID = this.txtCartonNo.Text.Trim().ToUpper();

            string fromCarton = this.txtCartonFrom.Text.Trim().ToUpper();
            string toCarton = this.txtCartonTo.Text.Trim().ToUpper();

            FrmJobNoInput frm = new FrmJobNoInput(fromTime, toTime, jobNo, boxID,fromCarton,toCarton, _QueryType);
            frm.Show();
        }
         

        private void btnQueryByJobNo_Click(object sender, EventArgs e)
        {
            //string jobNo = this.txtJobNo.Text.Trim();
            //if (string.IsNullOrEmpty(jobNo))
            //{
            //    MessageBox.Show("请输入 JobNo", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}

            string fromCarton = this.txtCustCartonFrom.Text.Trim();
            string toCarton = this.txtCustCartonTo.Text.Trim();

            if (string.IsNullOrEmpty(fromCarton) &&
                string.IsNullOrEmpty(toCarton))
            {
                MessageBox.Show("起始箱号与截止箱号至少输入一个", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string sql = sqlByCustCartonSpan;

            if (!string.IsNullOrEmpty(fromCarton))
                sql += @" AND Box.Cust_BoxID >= '" + fromCarton + "'";

            if (!string.IsNullOrEmpty(toCarton))
                sql += @" AND Box.Cust_BoxID <= '" + toCarton + "'";

            sql += sqlOrderBy;

            ToolsClass.GetDataTable(sql, prodDateSource);
            //string sql = string.Format(sqlByCustCartonSpan, jobNo);
            //ToolsClass.GetDataTable(sql, prodDateSource);
            _QueryType = QueryType.ByCustCarton;
            //更新显示
            upSourcd();
        }

        private void btnQueryByCartonSpan_Click(object sender, EventArgs e)
        {
            string fromCarton = this.txtCartonFrom.Text.Trim();
            string toCarton = this.txtCartonTo.Text.Trim();

            if (string.IsNullOrEmpty(fromCarton) &&
                string.IsNullOrEmpty(toCarton))
            {
                MessageBox.Show("起始箱号与截止箱号至少输入一个", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string sql = sqlByCartonSpan;

            if (!string.IsNullOrEmpty(fromCarton))
                sql += @" AND Box.BoxID >= '"+ fromCarton + "'";

            if (!string.IsNullOrEmpty(toCarton))
                sql += @" AND Box.BoxID <= '" + toCarton + "'";

            sql += sqlOrderBy;
            ToolsClass.GetDataTable(sql, prodDateSource);

            _QueryType = QueryType.ByCartonSpan;
            //更新显示
            upSourcd();
        }

        private void btnCustCartonQuery_Click(object sender, EventArgs e)
        {
            string Carton = this.txtCustCartonNo.Text.Trim();
            if (string.IsNullOrEmpty(Carton))
            {
                MessageBox.Show("请输入客户箱号", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string sql = sqlByCustCartonSpan;

            if (!string.IsNullOrEmpty(Carton))
                sql += @" AND Box.Cust_BoxID = '" + Carton + "'";

            sql += sqlOrderBy;

            ToolsClass.GetDataTable(sql, prodDateSource);
            _QueryType = QueryType.ByCustCarton;
            //更新显示
            upSourcd();
        }

        private void txtCartonFrom_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCustCartonFrom_TextChanged(object sender, EventArgs e)
        {

        } 
    }

    /// <summary>
    /// 查询方式
    /// </summary>
    public enum QueryType
    {
        ByDate,
        ByCarton,
        ByJobNo,
        ByCartonSpan,
        ByCustCarton
    }
}
