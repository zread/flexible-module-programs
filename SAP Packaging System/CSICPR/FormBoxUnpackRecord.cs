﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormBoxUnpackRecord : Form
    {
        private static List<LanguageItemModel> LanMessList;
        public FormBoxUnpackRecord()
        {
            InitializeComponent();
            dateFrom.Value = DateTime.Today;
            dateTo.Value = DateTime.Today.AddDays(1);
            cbCondition.SelectedIndex = 0;
            tbBoxID.Visible = false;
        }

        private void btQuery_Click(object sender, EventArgs e)
        {
            string ToStr = dateTo.Value.ToString("yyyy-MM-dd HH:mm:ss");
            string fromStr = dateFrom.Value.ToString("yyyy-MM-dd HH:mm:ss");

            string sql = "";
            if (cbCondition.SelectedIndex == 0)
                sql = "select BoxID,UnpackOperator,UnpackDate from Box where UnpackDate>='" + fromStr + "' and UnpackDate<='" + ToStr + "'";
            else if (cbCondition.SelectedIndex == 1)
                sql = "select BoxID,UnpackOperator,UnpackDate from Box where BoxID='" + tbBoxID.Text.Trim() + "'";

            dGridView.Rows.Clear();
            using (SqlDataReader read = ToolsClass.GetDataReader(sql))
            {
                if (read != null)
                {
                    while (read.Read())
                    {
                        dGridView.Rows.Add(read.GetString(0), read.GetString(1), read.GetDateTime(2).ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                }
            }
        }

        private void cbCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCondition.SelectedIndex == 0)
            {
                tbBoxID.Visible = false;
                lblfrom.Visible = true;
                lblto.Visible = true;
                dateFrom.Visible = true;
                dateTo.Visible = true;
                dateFrom.Value = DateTime.Today;
                dateTo.Value = DateTime.Today.AddDays(1);
            }
            else if (cbCondition.SelectedIndex == 1)
            {
                tbBoxID.Visible = true;
                tbBoxID.Text = "";
                lblfrom.Visible = false;
                lblto.Visible = false;
                dateFrom.Visible = false;
                dateTo.Visible = false;
            }
        }

        private void FormBoxUnpackRecord_Load(object sender, EventArgs e)
        {
            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);

            LanguageHelper.GetCombomBox(this, cbCondition);
            # endregion
        }

    }
}
