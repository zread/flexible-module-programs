﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;

namespace CSICPR
{
    public partial class FrmCancelInventory : Form
    {
        public FrmCancelInventory()
        {
            InitializeComponent();
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FrmCancelInventory _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmCancelInventory
                {
                    MdiParent = fm, 
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        private List<TsapReceiptUploadModule> _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();

        private void FormCancelStorge_Load(object sender, EventArgs e)
        {
            PlCarton.Visible = true;

           

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
             lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1","提示信息"), 630, HorizontalAlignment.Left);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            var carton = Convert.ToString(txtCarton.Text.Trim());
            if (!checkrepeat(carton))
                return;
            var tsapCancelInventoryModules = DataAccess.QueryCancelInventoryModuleByCartonNo(carton, FormCover.connectionBase);
            if (tsapCancelInventoryModules == null || tsapCancelInventoryModules.Count <= 0)
            {
            	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message2","托号：{0}入库数据不存在"), carton), "ABNORMAL", lstView);
                return;
            }
            var findAll = tsapCancelInventoryModules.FindAll(p => p.UploadStatus.ToLower() == "Finished".ToLower());
            if (findAll.Count > 0)
            {
            	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message3","托号：{0}已入库上传"),carton) , "ABNORMAL", lstView);
                return;
            }
            findAll = tsapCancelInventoryModules.FindAll(p => p.UploadStatus.ToLower() == "Canceled".ToLower());
            if (findAll.Count > 0)
            {
            	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message4","托号：{0}已取消入库"), carton ), "ABNORMAL", lstView);
                return;
            }
            findAll = tsapCancelInventoryModules.FindAll(p => p.UploadStatus.ToLower() == "Ready".ToLower());
            if (findAll.Count <= 0)
            {
            	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message5","托号：{0}状态异常"), carton),"ABNORMAL", lstView);
                return;
            }
            if (_tsapReceiptUploadModules == null)
                _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
            tsapCancelInventoryModules.ForEach(p => p.CommandName = "移除");
            _tsapReceiptUploadModules.AddRange(tsapCancelInventoryModules);
            dgvData.DataSource = null;
            dgvData.DataSource = _tsapReceiptUploadModules;
            txtCarton.Clear();
            txtCarton.Focus();
        }

        private bool checkrepeat(string carton)
        {
            foreach (DataGridViewRow datarow in dgvData.Rows)
            {
                if (datarow.Cells[2].Value != null && datarow.Cells[2].Value.Equals(carton))
                {
                	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message6","托号：{0}已在列表第 {1} 行" ),carton ,datarow.Index+1),"ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 取消入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                btnQuery.Enabled = false;
                button1.Enabled = false;
                if (_tsapReceiptUploadModules == null || _tsapReceiptUploadModules.Count <= 0)
                {
                    btnQuery.Enabled = true;
                    button1.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message7","没有要取消入库的数据,请确认"), "ABNORMAL", lstView);
                    return;
                }
                string joinid = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                var cartonNos = _tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
                foreach (var tempCartonNo in cartonNos)
                {
                    string msg;
                    var cartonNo = tempCartonNo;
                    var tsapReceiptUploadModules = _tsapReceiptUploadModules.FindAll(p => p.CartonNo == cartonNo);
                    tsapReceiptUploadModules.ForEach(p => p.UploadStatus = "Canceled");
                    if (ProductStorageDAL.UpdateCancelStorageInfo(cartonNo, out msg, joinid, posttime, tsapReceiptUploadModules))
                    {
                        tsapReceiptUploadModules.ForEach(p => _tsapReceiptUploadModules.Remove(p));
                        ToolsClass.Log("Carton: "+ cartonNo + " Undo stock in successful!","NORMAL",lstView);
                    }
                    else
                    {
                    	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message8","托号:{0}取消入库失败,原因:"), cartonNo ,msg ), "ABNORMAL", lstView);
                    }
                }
                dgvData.DataSource = null;
                dgvData.DataSource = _tsapReceiptUploadModules;
                btnQuery.Enabled = true;
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                btnQuery.Enabled = true;
                button1.Enabled = true;
                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message9","取消入库时发生了异常:{0}"),ex.Message), "ABNORMAL", lstView);
            }
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString(CultureInfo.InvariantCulture).Equals("\r"))
            {
                btnQuery_Click(null, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
            dgvData.DataSource = null;
            dgvData.DataSource = _tsapReceiptUploadModules;
            txtCarton.SelectAll();
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }
    }
}
