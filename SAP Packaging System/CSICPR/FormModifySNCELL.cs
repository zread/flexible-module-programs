﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormModifySNCELL : Form
    {
        public FormModifySNCELL()
        {
            InitializeComponent();
        }

        private string CellCode = "";
        private string CellBatch = "";
        private string CartonNo = "";
        private string Wo = "";
        public List<CellSerialNumberTemp> _list = new List<CellSerialNumberTemp>();
        private List<string> WOCellCode = new List<string>();
        private List<string> WOCellBatch = new List<string>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CartonNo">托号</param>
        /// <param name="OldCellCode">电池片物料号</param>
        /// <param name="OldCellBatch">电池片批次号</param>
        /// <param name="_ListPackingSN">暂存组件电池片信息</param>
        public FormModifySNCELL(string OldCartonNo, string OldCellCode, string OldCellBatch, string OldWo, List<CellSerialNumberTemp> _ListPackingSN)
        {
            InitializeComponent();
            this.Text = "托号:" + OldCartonNo;
            _list = _ListPackingSN;
            CartonNo = OldCartonNo;
            CellBatch = OldCellBatch;
            CellCode = OldCellCode;
            Wo = OldWo;
        }

        private void FormModifySNCELL_Load(object sender, EventArgs e)
        {
            DataTable dt = ProductStorageDAL.GetCartonInfo(CartonNo);
            if (dt != null && dt.Rows.Count > 0)
            {
                if (this.dataGridView1.Rows.Count > 0)
                    this.dataGridView1.Rows.Clear();
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["SN"].Value = Convert.ToString(row["SN"]);
                    this.dataGridView1.Rows[RowNo].Cells["OrderNo"].Value = Wo;

                    for (int i = 0; i < _list.Count; i++)
                    {
                        if (Convert.ToString(row["SN"]) == _list[i].SN)
                        {
                            this.dataGridView1.Rows[RowNo].Cells["Cell"].Value = _list[i].CellCode;
                            this.dataGridView1.Rows[RowNo].Cells["CellBatchNo"].Value = _list[i].CellBatch;
                            this.dataGridView1.Rows[RowNo].Cells["flag"].Value = "是";
                            break;
                        }
                    }

                    if ((Convert.ToString(this.dataGridView1.Rows[RowNo].Cells["Cell"].Value).Equals("")) ||
                        (Convert.ToString(this.dataGridView1.Rows[RowNo].Cells["CellBatchNo"].Value).Equals("")))
                    {
                        this.dataGridView1.Rows[RowNo].Cells["Cell"].Value = CellCode;
                        this.dataGridView1.Rows[RowNo].Cells["CellBatchNo"].Value = CellBatch;
                    }
                    #region
                    //DataGridViewComboBoxCell Cell = dataGridView1.Rows[RowNo].Cells["Cell"] as DataGridViewComboBoxCell;
                    //foreach(string RowCellCode in WOCellCode)
                    //{
                    //    Cell.Items.Add(RowCellCode);
                    //}
                    //Cell.Value = CellCode;

                    //DataGridViewComboBoxCell CellBatchNo = dataGridView1.Rows[RowNo].Cells["CellBatchNo"] as DataGridViewComboBoxCell;
                    //foreach(string RowCellBatch in WOCellBatch)
                    //{
                    //    CellBatchNo.Items.Add(RowCellBatch);
                    //}
                    //CellBatchNo.Value = CellBatch;
                    #endregion
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show("获取组件信息失败");
                return;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 5)
            {
                #region
                //string sn = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["SN"].Value);
                //CellSerialNumberTemp CellTemp = new CellSerialNumberTemp();
                //CellTemp.CartonNo = CartonNo;
                //CellTemp.SN = sn;
                //CellTemp.CellCode = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["Cell"].Value);
                //CellTemp.CellBatch = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CellBatchNo"].Value);
                //for (int i = 0; i < _list.Count; i++)
                //{
                //    if (sn == _list[i].SN)
                //    {
                //        _list.RemoveAt(i);
                //    }
                //}
                //_list.Add(CellTemp);

                //MessageBox.Show("保存成功");
                #endregion
            }
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (((e.KeyValue == 114) || (e.KeyValue == 13)) && (this.dataGridView1.CurrentCell.ColumnIndex == 3))
            {
                var frm = new FormMitemBatch(this.dataGridView1.CurrentCell.Value.ToString(), "Cell","","");
                frm.ShowDialog();
                if (!frm.MitemCode.Equals("") && !frm.Batch.Equals(""))
                {
                    this.dataGridView1.CurrentCell.Value = frm.Batch;
                    this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[this.dataGridView1.CurrentCell.ColumnIndex + 1].Value = frm.MitemCode;
                    this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[this.dataGridView1.CurrentCell.ColumnIndex + 2].Value = "是";
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView1.CurrentCell.ColumnIndex == 3)
            {
                var frm = new FormMitemBatch(this.dataGridView1.CurrentCell.Value.ToString(), "Cell","","");
                frm.ShowDialog();
                if (!frm.MitemCode.Equals("") && !frm.Batch.Equals(""))
                {
                    this.dataGridView1.CurrentCell.Value = frm.Batch;
                    this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[this.dataGridView1.CurrentCell.ColumnIndex + 1].Value = frm.MitemCode;
                    this.dataGridView1.Rows[this.dataGridView1.CurrentCell.RowIndex].Cells[this.dataGridView1.CurrentCell.ColumnIndex + 2].Value = "是";
                }
            }
        }

        private void FormModifySNCELL_FormClosing(object sender, FormClosingEventArgs e)
        {
            string sn = "";
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                if (!Convert.ToString(dgvRow.Cells["flag"].Value).Equals(""))
                {
                    sn = Convert.ToString(dgvRow.Cells["SN"].Value);

                    for (int i = 0; i < _list.Count; i++)
                    {
                        if (sn == _list[i].SN)
                        {
                            _list.RemoveAt(i);
                        }
                    }

                    CellSerialNumberTemp CellTemp = new CellSerialNumberTemp();
                    CellTemp.CartonNo = CartonNo;
                    CellTemp.SN = sn;
                    CellTemp.CellCode = Convert.ToString(dgvRow.Cells["Cell"].Value);
                    CellTemp.CellBatch = Convert.ToString(dgvRow.Cells["CellBatchNo"].Value);
                    _list.Add(CellTemp);
                }
            }
        }
    }
}
