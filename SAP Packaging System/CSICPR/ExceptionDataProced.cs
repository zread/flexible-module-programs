﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class ExceptionDataProced : Form
    {
        private List<MODULE> _ListModule = new List<MODULE>();//CenterDB
        private List<MODULE_TEST> _ListModuleTest = new List<MODULE_TEST>();//CenterDB
        private string _CartonPower = "";//CenterDB 标称功率
        private string Work_Shop = "";
        private string ProductSql = "";
        private string ProductDB = "";
        private string CenterDB = "";
        //private string
        public ExceptionDataProced()
        {
            InitializeComponent();
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            lstLog.Items.Add("----------------------------数据同步开始，箱号：" + tbCarton.Text.Trim());
            btnSync.Enabled = false;
            _CartonPower = "";
            if (!CheckItem())
                return;
            try
            {
                ProductDB = ToolsClass.getConfig(cmbWorkShop.Text + "DB", false, "", "config.xml");
                Work_Shop = cmbWorkShop.Text;

                if (!CheckCartonExist(tbCarton.Text.Trim()))
                {
                    ProductSql = @"select P.SN,P.ModelType,P.Art_No,E.Temp,E.VOC,E.ISC,E.Pmax,E.Vm,E.Im,E.FF,E.Eff,E.TestDateTime,E.SchemeInterID,E.InterID from Product P
                                      inner join ElecParaTest E
                                      on P.InterNewTempTableID=E.InterID
                                      where P.BoxID='{0}'";
                    ProductSql = string.Format(ProductSql, tbCarton.Text.Trim());
                    DataSet ds = DbHelperSQL.Query(ProductDB, ProductSql, null);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        //bool check = true;
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            #region"Mark"
                            //if (check)
                            //{
                            //    check = false;
                            //    if (CheckModuleExist(row["SN"].ToString()))
                            //    {
                            //        lstLog.Items.Add("数据同步失败，箱号：" + tbCarton.Text.Trim() + "被拆包，请用包装系统重新打包");
                            //        btnSync.Enabled = true;
                            //        return;
                            //    }
                            //}
                            #endregion
                            string isExist = new MODULEDAL().CheckRepeat(row["SN"].ToString());
                            if (isExist == "0")
                            {
                                #region"No exist"
                                MODULE mo = new MODULE();
                                mo.SYSID = Guid.NewGuid().ToString();
                                if (Work_Shop.ToUpper().Equals("M08"))
                                    mo.SYSID = mo.SYSID + "-08";
                                mo.MODULE_SN = row["SN"].ToString();
                                mo.BARCODE = row["SN"].ToString();
                                mo.WORKSHOP = Work_Shop;
                                mo.WORK_ORDER = row["SN"].ToString().Substring(0, 9);
                                mo.MODULE_TYPE = row["ModelType"].ToString();
                                mo.CREATED_ON = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                mo.CREATED_BY = Work_Shop + "-SYNC";
                                mo.MODIFIED_BY = Work_Shop + "-SYNC";
                                mo.MODIFIED_ON = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                mo.RESV01 = row["Art_No"].ToString();
                                _ListModule.Add(mo);

                                MODULE_TEST moTest = new MODULE_TEST();
                                moTest.SYSID = Guid.NewGuid().ToString();
                                if (Work_Shop.ToUpper().Equals("M08"))
                                    moTest.SYSID = moTest.SYSID + "-08";
                                moTest.MODULE_SN = row["SN"].ToString();
                                moTest.TEMP = row["Temp"].ToString();
                                moTest.ISC = row["ISC"].ToString();
                                moTest.VOC = row["VOC"].ToString();
                                moTest.IMP = row["Im"].ToString();
                                moTest.VMP = row["Vm"].ToString();
                                moTest.FF = row["FF"].ToString();
                                moTest.EFF = row["Eff"].ToString();
                                moTest.PMAX = row["Pmax"].ToString();
                                moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.CREATED_BY = Work_Shop + "-SYNC";
                                moTest.MODIFIED_ON = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.MODIFIED_BY = Work_Shop + "-SYNC";
                                _ListModuleTest.Add(moTest);
                                #endregion
                            }
                            
                            if (isExist == "2")
                            {
                                #region"Exist"
                                MODULE_TEST moTest = new MODULE_TEST();
                                moTest.SYSID = Guid.NewGuid().ToString();
                                if (Work_Shop.ToUpper().Equals("M08"))
                                    moTest.SYSID = moTest.SYSID + "-08";
                                moTest.MODULE_SN = row["SN"].ToString();
                                moTest.TEMP = row["Temp"].ToString();
                                moTest.ISC = row["ISC"].ToString();
                                moTest.VOC = row["VOC"].ToString();
                                moTest.IMP = row["Im"].ToString();
                                moTest.VMP = row["Vm"].ToString();
                                moTest.FF = row["FF"].ToString();
                                moTest.EFF = row["Eff"].ToString();
                                moTest.PMAX = row["Pmax"].ToString();
                                moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.CREATED_BY = Work_Shop + "-SYNC";
                                moTest.MODIFIED_ON = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                moTest.MODIFIED_BY = Work_Shop + "-SYNC";
                                _ListModuleTest.Add(moTest);

                                string sqlQuerySNId = string.Format("SELECT SYSID FROM T_MODULE where MODULE_SN='{0}'", row["SN"].ToString());
                                object obj = DbHelperSQL.ExecuteScalar(sqlQuerySNId, null);
                                MODULE mo = new MODULE();
                                if (obj != null && obj != DBNull.Value)
                                {
                                    mo.SYSID = obj.ToString();
                                    mo.REMARK = "UnPacking";
                                    _ListModule.Add(mo);
                                }
                                else
                                {
                                    MessageBox.Show("组件未找到：" + row["SN"].ToString(), "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                #endregion
                            }

                            if (isExist == "1")
                            {
                                lstLog.Items.Add("箱号：" + tbCarton.Text.Trim() + " 组件：" + row["SN"].ToString() + " 已经被打包");
                                ClearItem();
                                btnSync.Enabled = true;
                                return;
                            }

                            if (string.IsNullOrEmpty(_CartonPower))
                            {
                                _CartonPower = ToolsClass.getIdealPower(double.Parse(row["Pmax"].ToString()), "", int.Parse(row["SchemeInterID"].ToString()),ProductDB).ToString();
                            }
                        }
                        
                        if (_ListModule != null && _ListModule.Count > 0
                            && _ListModuleTest != null && _ListModuleTest.Count > 0)
                        {
                           
                            foreach (MODULE_TEST test in _ListModuleTest)
                            {
                                new MODULE_TESTDAL().Add(test);
                            }

                            MODULE_CARTON moCarton = new MODULE_CARTON();
                            moCarton.SYSID = Guid.NewGuid().ToString();
                            if (Work_Shop.ToUpper().Equals("M08"))//add by alex.dong|2012-06-07|v2.0.6|For M08
                                moCarton.SYSID = moCarton.SYSID + "-08";
                            moCarton.STD_POWER_LEVEL = _CartonPower;
                            moCarton.CARTON_NO = tbCarton.Text.Trim();
                            moCarton.SHIP_JOB_NO = tbJobNo.Text.Trim();
                            moCarton.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            moCarton.CREATED_BY = Work_Shop + "-SYNC";
                            moCarton.MODIFIED_BY = Work_Shop + "-SYNC";
                            moCarton.MODIFIED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                            new MODULE_CARTONDAL().Add(moCarton);

                            StringBuilder log = new StringBuilder();
                            foreach (MODULE mo in _ListModule)
                            {
                                if (mo.REMARK != "UnPacking")
                                {
                                    new MODULEDAL().Add(mo);
                                }

                                MODULE_PACKING_LIST molk = new MODULE_PACKING_LIST();
                                molk.MODULE_CARTON_SYSID = moCarton.SYSID;
                                molk.MODULE_SYSID = mo.SYSID;
                                new MODULE_PACKING_LISTDAL().Add(molk);

                                log.AppendLine(string.Format("PackingList.CartonId:{0} ModuleSysId:{1}", moCarton.SYSID, mo.SYSID));
                            }

                            lstLog.Items.Add(log);
                            _ListModule = new List<MODULE>();//CenterDB
                            _ListModuleTest = new List<MODULE_TEST>();//CenterDB
                        }
                        
                        lstLog.Items.Add("箱号：" + tbCarton.Text.Trim() + " 数据同步完成");
                    }
                    else
                    {
                        lstLog.Items.Add("箱号：" + tbCarton.Text.Trim() + " 不存在于车间：" + Work_Shop);
                    }
                }
                else
                {
                    lstLog.Items.Add("箱号：" + tbCarton.Text.Trim() + " 已经存在于中央数据库");
                }

                ClearItem();
                
            }
            catch(Exception ex)
            {
                ClearItem();
                _ListModule = new List<MODULE>();//CenterDB
                _ListModuleTest = new List<MODULE_TEST>();//CenterDB
                lstLog.Items.Add("数据同步失败，原因："+ex.ToString());
            }
            lstLog.Items.Add("----------------------------数据同步结束，箱号：" + tbCarton.Text.Trim());
            btnSync.Enabled = true;
        }

        private void ExceptionDataProced_Load(object sender, EventArgs e)
        {
            lstLog.Items.Clear();
            CenterDB = ToolsClass.getConfig("CenterDB", false, "", "config.xml");
            DbHelperSQL.sqlConn = CenterDB;
        }

        private bool CheckItem()
        {
            bool chkFlag = true;
            if (tbCarton.Text.Trim().Length == 0)
            {
                MessageBox.Show("箱号不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkFlag = false;
            }
            if (tbJobNo.Text.Trim().Length == 0)
            {
                MessageBox.Show("货柜号不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkFlag = false;
            }
            if (cmbWorkShop.Text.Trim().Length == 0)
            {
                MessageBox.Show("车间不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chkFlag = false;
            }
            return chkFlag;
        }

        private void ClearItem()
        {
            tbCarton.Text = "";
            tbJobNo.Text = "";
            if (lstLog.Items.Count > 1000)
                lstLog.Items.Clear();
        }

        private bool CheckCartonExist(string CartonNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE_CARTON");
            strSql.Append(" where CARTON_NO=@CARTON_NO ");
            SqlParameter[] parameters = {
                    new SqlParameter("@CARTON_NO", SqlDbType.NVarChar,50)};
            parameters[0].Value = CartonNo;

            bool snIsHave = DbHelperSQL.Exists(strSql.ToString(), parameters);
            return snIsHave;
        }

        private bool CheckModuleExist(string Module_SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE");
            strSql.Append(" where MODULE_SN=@MODULE_SN ");
            SqlParameter[] parameters = {
                    new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50)};
            parameters[0].Value = Module_SN;

            bool snIsHave = DbHelperSQL.Exists(strSql.ToString(), parameters);
            return snIsHave;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            lstLog.Items.Clear();
            string sn = tbSN.Text.Trim();
            string artno = tbArtNo.Text.Trim();
            if (string.IsNullOrEmpty(sn))
                return;
            //if(string.IsNullOrEmpty(artno))
            if (updateArtNo(sn, artno) > 0)
                lstLog.Items.Add("更新成功");
            else
                lstLog.Items.Add("更新失败，中央数据库无此序列号：" + sn);

            tbSN.Text = "";
        }

        private int updateArtNo(string sn, string artno)
        {
            string sql = "";
            sql = "update t_module set RESV01='{0}' where MODULE_SN='{1}'";
            sql = string.Format(sql, artno, sn);
            return ToolsClass.ExecuteNonQuery(sql, CenterDB);
        }
    }
}
