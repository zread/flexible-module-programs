﻿namespace CSICPR
{
    partial class FormReWorkOrder    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReWorkOrder));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnQuery = new System.Windows.Forms.Button();
            this.PalWO = new System.Windows.Forms.Panel();
            this.PicBoxWo = new System.Windows.Forms.PictureBox();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RowIndex = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ReworkOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonStatusCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FactoryCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SAPCartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SAPCartonStatusCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chbSelected = new System.Windows.Forms.CheckBox();
            this.PalWO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWo)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(286, 18);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 22);
            this.btnQuery.TabIndex = 168;
            this.btnQuery.Tag = "button1";
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // PalWO
            // 
            this.PalWO.Controls.Add(this.PicBoxWo);
            this.PalWO.Controls.Add(this.txtWo);
            this.PalWO.Controls.Add(this.label2);
            this.PalWO.Location = new System.Drawing.Point(20, 15);
            this.PalWO.Name = "PalWO";
            this.PalWO.Size = new System.Drawing.Size(259, 32);
            this.PalWO.TabIndex = 172;
            // 
            // PicBoxWo
            // 
            this.PicBoxWo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWo.BackgroundImage")));
            this.PicBoxWo.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWo.InitialImage")));
            this.PicBoxWo.Location = new System.Drawing.Point(228, 4);
            this.PicBoxWo.Name = "PicBoxWo";
            this.PicBoxWo.Size = new System.Drawing.Size(27, 18);
            this.PicBoxWo.TabIndex = 175;
            this.PicBoxWo.TabStop = false;
            this.PicBoxWo.Click += new System.EventHandler(this.PicBoxWo_Click);
            // 
            // txtWo
            // 
            this.txtWo.Location = new System.Drawing.Point(79, 3);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(148, 21);
            this.txtWo.TabIndex = 94;
            this.txtWo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWo_KeyDown);
            this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "工       单";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.PalWO);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1003, 57);
            this.panel1.TabIndex = 174;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(433, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 22);
            this.button1.TabIndex = 175;
            this.button1.Tag = "button1";
            this.button1.Text = "重置";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(361, 18);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 22);
            this.btnSave.TabIndex = 174;
            this.btnSave.Tag = "button1";
            this.btnSave.Text = "确定";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowIndex,
            this.ReworkOrder,
            this.PostCode,
            this.Factory,
            this.CartonNo,
            this.CartonStatus,
            this.CartonStatusCode,
            this.FactoryCode,
            this.SAPCartonStatus,
            this.SAPCartonStatusCode});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1003, 515);
            this.dataGridView1.TabIndex = 175;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // RowIndex
            // 
            this.RowIndex.FillWeight = 50F;
            this.RowIndex.HeaderText = "";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RowIndex.Width = 45;
            // 
            // ReworkOrder
            // 
            this.ReworkOrder.FillWeight = 120F;
            this.ReworkOrder.HeaderText = "重工工单";
            this.ReworkOrder.Name = "ReworkOrder";
            this.ReworkOrder.ReadOnly = true;
            // 
            // PostCode
            // 
            this.PostCode.HeaderText = "凭证号";
            this.PostCode.Name = "PostCode";
            // 
            // Factory
            // 
            this.Factory.HeaderText = "原车间";
            this.Factory.Name = "Factory";
            // 
            // CartonNo
            // 
            this.CartonNo.FillWeight = 150F;
            this.CartonNo.HeaderText = "托号";
            this.CartonNo.Name = "CartonNo";
            this.CartonNo.ReadOnly = true;
            this.CartonNo.Width = 150;
            // 
            // CartonStatus
            // 
            this.CartonStatus.HeaderText = "托号状态";
            this.CartonStatus.Name = "CartonStatus";
            this.CartonStatus.ReadOnly = true;
            // 
            // CartonStatusCode
            // 
            this.CartonStatusCode.HeaderText = "托号状态代码";
            this.CartonStatusCode.Name = "CartonStatusCode";
            // 
            // FactoryCode
            // 
            this.FactoryCode.HeaderText = "原车间代码";
            this.FactoryCode.Name = "FactoryCode";
            // 
            // SAPCartonStatus
            // 
            this.SAPCartonStatus.HeaderText = "SAP托号状态";
            this.SAPCartonStatus.Name = "SAPCartonStatus";
            // 
            // SAPCartonStatusCode
            // 
            this.SAPCartonStatusCode.HeaderText = "SAP托号状态代码";
            this.SAPCartonStatusCode.Name = "SAPCartonStatusCode";
            this.SAPCartonStatusCode.Width = 120;
            // 
            // chbSelected
            // 
            this.chbSelected.AutoSize = true;
            this.chbSelected.BackColor = System.Drawing.SystemColors.Control;
            this.chbSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chbSelected.Location = new System.Drawing.Point(16, 60);
            this.chbSelected.Name = "chbSelected";
            this.chbSelected.Size = new System.Drawing.Size(15, 14);
            this.chbSelected.TabIndex = 176;
            this.chbSelected.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.chbSelected.UseVisualStyleBackColor = false;
            this.chbSelected.Click += new System.EventHandler(this.chbSelected_Click);
            // 
            // FormReWorkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 572);
            this.Controls.Add(this.chbSelected);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "FormReWorkOrder";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0002";
            this.Text = "重工工单确认";
            this.Load += new System.EventHandler(this.FormReWorkOrder_Load);
            this.PalWO.ResumeLayout(false);
            this.PalWO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWo)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Panel PalWO;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox PicBoxWo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chbSelected;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReworkOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatusCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn FactoryCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn SAPCartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn SAPCartonStatusCode;
    }
}