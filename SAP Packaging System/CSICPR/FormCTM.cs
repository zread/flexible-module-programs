﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CSICPR
{
    /// <summary>
    /// V2.2
    /// 手工录入CTM结果
    /// </summary>
    public partial class FormCTM : Form
    {
        private static List<LanguageItemModel> LanMessList;
        private DataTable dtCTM;

        public FormCTM()
        {
            InitializeComponent();
        }

        private void tbBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                Thread.Sleep(100);
                if (cbCTMList.Text.Trim().ToUpper().Equals(""))
                {
                    MessageBox.Show("请选择组件的CTM等级!!!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                if (cbCTMList.Text.Trim().ToUpper().Equals("NG"))
                {
                    if (MessageBox.Show("确定此批组件CTM判定NG？", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                    }
                    else
                    {
                        return;
                    }
                }
                string strTemp = tbBarCode.Text;
                string[] sDataSet = strTemp.Split('\n');
                for (int i = 0; i < sDataSet.Length; i++)
                {
                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    if (sDataSet[i] != null && sDataSet[i] != "")
                        updateModuleCTM(sDataSet[i], cbCTMList.Text.Trim());
                }
                tbBarCode.Clear();
            }
        }

        private void FormCTM_Load(object sender, EventArgs e)
        {
            controlIsShow(0, 1,0);
            lblQueryCnt.Visible = false;
            dtCTM = new DataTable();

            cbCTMList.Items.Add("OK");
            cbCTMList.Items.Add("NG");

            # region
            //lstViewCTM.View = View.Details;
            //lstViewCTM.Columns.Add("组件序列号", 120, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("温度", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("VOC", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("ISC", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("Pmax(W)", 70, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("VM", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("IM", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("FF", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("EFF", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("Rs", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("Rsh", 60, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("CTM", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("测试员", 80, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("测试时间", 120, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("CTM判等人员", 120, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("组件类型", 50, HorizontalAlignment.Center);
            //lstViewCTM.Columns.Add("组件颜色", 50, HorizontalAlignment.Center);
            //lstViewCTM.FullRowSelect = true;
            # endregion
            # region 新方法
            lstViewCTM.View = View.Details;
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "组件序列号", Width = 120, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader1" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "温度", Width = 80, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader2" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "VOC", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader3" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "ISC", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader4" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "Pmax", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader5" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "VM", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader6" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "IM", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader7" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "FF", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader8" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "EFF", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader9" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "Rs", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader10" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "Rsh", Width = 60, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader11" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "CTM", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader12" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "测试员", Width = 80, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader13" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "测试时间", Width = 120, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader14" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "CTM判等人员", Width = 120, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader15" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "组件类型", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader16" });
            lstViewCTM.Columns.Add(new ColumnHeader() { Text = "组件颜色", Width = 50, TextAlign = HorizontalAlignment.Center, Name = "ColumnHeader17" });
            # endregion

            this.dateFrom.Value = DateTime.Today;
            this.dateTo.Value = DateTime.Today.AddDays(1);
            this.cbQueryTerm.SelectedIndex = 0;

            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            //cbQueryTerm 多语言
            LanguageHelper.GetCombomBox(this, this.cbQueryTerm);




            # endregion
        }

        private void updateModuleCTM(string sModuleNo,string sCTMValue)
        {
            if (sModuleNo == "" || sCTMValue == "")
                return;

            string sql = "update product set CTMGrade='{0}',OperatorID='" + FormCover.CurrUserWorkID + "' where SN='{1}' and BoxID='' and InterID=(select max(InterID) from product where SN='{1}')";
            sql = string.Format(sql, sCTMValue, sModuleNo);
            ToolsClass.ExecuteNonQuery(sql);
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            string tmp = "", tmp2 = "";
            string sqlQuery = @"select pd1.FNumber,pd1.Temp,pd1.VOC,pd1.ISC,pd1.Pmax,pd1.Vm,
                                       pd1.Im,pd1.FF,pd1.Eff,pd1.Serial,
                                       pd1.Shunt,pd2.CTMGrade as CTMGrade,
                                       pd1.Operator,CONVERT(varchar(19),
                                       pd1.TestDateTime,120),pd2.OperatorID as OperatorID,
                                       pd2.ModelType as ModelType,pd2.CellColor as CellColor 
                                from ((select FNumber,Temp,VOC,ISC,Pmax,Vm,Im,FF,Eff,Serial,Shunt,InterID,Operator,TestDateTime 
                                       from ElecParaTest a with(nolock)
                                       where {0} and not exists(select 1 from ElecParaTest with(nolock) 
                                                                where FNumber=a.FNumber and InterID>a.InterID)) as pd1 
                                                                inner join(select CTMGrade,InterNewTempTableID,OperatorID,ModelType,CellColor 
                        from Product with(nolock) where {1}) as pd2 on pd2.InterNewTempTableID=pd1.InterID )order by pd1.InterID asc";

            lstViewCTM.Items.Clear();
            if (cbQueryTerm.Text.Trim().Equals(""))
            {
                MessageBox.Show("请选择查询条件!!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (cbQueryTerm.SelectedIndex == 0)
            {
                tmp = "TestDateTime>='" + dateFrom.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and TestDateTime<='" + dateTo.Value.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                tmp2 = "CTMGrade='W'";
                sqlQuery = string.Format(sqlQuery, tmp,tmp2);
            }
            else if (cbQueryTerm.SelectedIndex == 1)
            {
                tmp = "Operator='" + txtLine.Text.Trim() + "'";
                tmp2 = "CTMGrade='W'";
                sqlQuery = string.Format(sqlQuery, tmp, tmp2);
            }
            else if (cbQueryTerm.SelectedIndex == 2)
            {
                tmp = "FNumber='" + txtNo.Text.Trim() + "'";
                tmp2 = "CTMGrade<>''";
                sqlQuery = string.Format(sqlQuery, tmp, tmp2);
            }

            ToolsClass.GetDataTable(sqlQuery,dtCTM);

            foreach (DataRow DRkeys in dtCTM.Rows)
            {
                ListViewItem lItem = new ListViewItem();
                lItem.Text = DRkeys[0].ToString();
                for (int i = 1; i < dtCTM.Columns.Count; i++)
                {
                    lItem.SubItems.Add(DRkeys[i].ToString());
                }
                this.lstViewCTM.Items.Add(lItem);
            }
            lblQueryCnt.Visible = true;
            lblQueryCnt.Text = "共查询到:" + dtCTM.Rows.Count.ToString() + "笔数据";
        }

        private void btOutpu_Click(object sender, EventArgs e)
        {
            ToolsClass.DataToExcel(null, true, lstViewCTM);
        }

        private void cbQueryTerm_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cbQueryTerm.SelectedIndex)
            {
                case 0:
                    controlIsShow(0, 1, 0);
                    break;
                case 1:
                    controlIsShow(1, 0, 0);
                    break;
                case 2:
                    controlIsShow(0, 0, 1);
                    break;
            }
        }

        private void controlIsShow(int i,int j,int m)
        {
            if (i == 0)
            {
                lbltmp.Visible = false;
                txtLine.Visible = false;
            }
            else
            {
                txtLine.Visible = true;
                lbltmp.Visible = true;
            }

            if (j == 0)
            {
                lblFrom.Visible = false;
                lblTo.Visible = false;
                dateFrom.Visible = false;
                dateTo.Visible = false;
            }
            else
            {
                lblFrom.Visible = true;
                lblTo.Visible = true;
                dateFrom.Visible = true;
                dateTo.Visible = true;
            }

            if (m == 0)
            {
                lblNo.Visible = false;
                txtNo.Visible = false;
            }
            else
            {
                lblNo.Visible = true;
                txtNo.Visible = true;
            }
        }

        private void FormCTM_FormClosing(object sender, FormClosingEventArgs e)
        {
            dtCTM = null;
        }
    }
}
