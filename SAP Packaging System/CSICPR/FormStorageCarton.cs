﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;
using System.Xml;
using System.IO;

namespace CSICPR
{
    public partial class FormStorageCarton : Form
    {
        /// <summary>
        /// 保险数据
        /// </summary>
        // private List<string> _ListCartonSucess = new List<string>();
        /// <summary>
        ///私有变量
        /// </summary>
        /// 

        private static List<LanguageItemModel> LanMessList;//定义语言集
        public FormStorageCarton()
        {
            InitializeComponent();
        }

        private void ProductStorage_Load(object sender, EventArgs e)
        {
            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, "普通生产工单");
            this.ddlWoType.Items.Insert(2, "返工生产工单");
            this.ddlWoType.SelectedIndex = 0;
           

            //提示信息
            this.lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            //界面Grid列头禁止排序
            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            //工单状态
            this.ddlWostatus.Items.Insert(0, "");
            this.ddlWostatus.Items.Insert(1, "完工");
            this.ddlWostatus.Items.Insert(2, "强制结单");
            this.ddlWostatus.SelectedIndex = 0;

            //电池分档
            this.ddlByIm.Items.Insert(0, "NA");
            //this.ddlByIm.Items.Insert(1, "是");
            //this.ddlByIm.Items.Insert(2, "否");
            this.ddlByIm.SelectedIndex = 0;
            this.ddlByIm.Enabled = false;

            //网板
            this.ddlCellNetBoard.Items.Insert(0, "");
            this.ddlCellNetBoard.Items.Insert(1, "C0");
            this.ddlCellNetBoard.Items.Insert(2, "C1");
            this.ddlCellNetBoard.Items.Insert(3, "C2");
            this.ddlCellNetBoard.Items.Insert(4, "C3");
            this.ddlCellNetBoard.Items.Insert(5, "C4");
            this.ddlCellNetBoard.Items.Insert(6, "C5");
            this.ddlCellNetBoard.SelectedIndex = 0;

            //玻璃厚度
            this.ddlGlassLength.Items.Insert(0, "");
            this.ddlGlassLength.Items.Insert(1, "3.2mm");
            this.ddlGlassLength.Items.Insert(2, "4mm");
            this.ddlGlassLength.Items.Insert(3, "Other");
            this.ddlGlassLength.SelectedIndex = 0;

            //包装方式
            this.ddlPackingPattern.Items.Insert(0, "");
            this.ddlPackingPattern.Items.Insert(1, "横包装");
            this.ddlPackingPattern.Items.Insert(2, "竖包装");
            this.ddlPackingPattern.Items.Insert(3, "双件装");
            this.ddlPackingPattern.Items.Insert(4, "单件装");
            this.ddlPackingPattern.Items.Insert(5, "其它");
            this.ddlPackingPattern.SelectedIndex = 0;

            //撤销入库
            this.ddlCancelStorageFlag.Items.Insert(0, "");
            this.ddlCancelStorageFlag.Items.Insert(1, "正常");
            this.ddlCancelStorageFlag.Items.Insert(2, "撤销");
            this.ddlCancelStorageFlag.SelectedIndex = 0;

            //是否拼托
            this.ddlIsOnlyPacking.Items.Insert(0, "");
            this.ddlIsOnlyPacking.Items.Insert(1, "是");
            this.ddlIsOnlyPacking.Items.Insert(2, "否");
            this.ddlIsOnlyPacking.SelectedIndex = 1;

            this.txtCarton.Focus();

            this.checkBox2.Checked = false;
            this.txtWO1.Enabled = false;
            this.pictureBox1.Enabled = false;

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlWoType);
            LanguageHelper.GetCombomBox(this, this.ddlWostatus);
            LanguageHelper.GetCombomBox(this, this.ddlByIm);
            LanguageHelper.GetCombomBox(this, this.ddlPackingPattern);
            LanguageHelper.GetCombomBox(this, this.ddlCancelStorageFlag);
            LanguageHelper.GetCombomBox(this, this.ddlIsOnlyPacking);
            # endregion
        }

        #region 公有变量
        private static FormStorageCarton theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormStorageCarton();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        /// <summary>
        /// 工单类型
        /// </summary>
        private string WoType = "";
        /// <summary>
        /// 工单状态
        /// </summary>
        private string WoStatus = "";
        /// <summary>
        /// 包装方式
        /// </summary>
        private string PackingPatternValue = "";
        /// <summary>
        /// 设置：记录选中的记录
        /// </summary>
        private Dictionary<int, string> dgvIndex = new Dictionary<int, string>();
        /// <summary>
        /// 设置：记录选中的托号码
        /// </summary>
        private List<string> CartonArrayS = new List<string>();
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        /// <summary>
        /// 暂存修改过的组件电池片信息
        /// </summary>
        private List<CellSerialNumberTemp> _ListPackingSNCellInfo = new List<CellSerialNumberTemp>();
        /// <summary>
        /// 电流分档
        /// </summary>
        private string ByIm_flag = "";

        /// <summary>
        /// 撤销入库标识
        /// </summary>
        private string _CancelStorageFlag = "";
        /// <summary>
        /// 是否拼托
        /// </summary>
        private string _IsOnlyPacking = "";
        /// <summary>
        ///记录上传入库成功的做记录
        /// </summary>
        private List<SapWoModule> _ListSapWoLog = new List<SapWoModule>();
        #endregion

        /// <summary>
        /// 对于重工工单，如果托里混有新组建，必须要输入物料特性
        /// </summary>
        /// <param name="Cartons">托号清单</param>
        private bool CheckNewSnIsNull(List<string> Cartons)
        {
            foreach (string carton in Cartons)
            {
                DataTable dt = ProductStorageDAL.GetSNInfoFromPacking(carton);
                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    #region
                    foreach (DataRow row in dt.Rows)
                    {
                        string sn = Convert.ToString(row["SN"]).Trim();
                        if (!sn.Equals(""))
                        {
                            DataTable dtnew = ProductStorageDAL.GetSNInfoFromInterface(sn);
                            //SAP已经入库过
                            if ((dtnew != null) && (dtnew.Rows.Count > 0))
                                continue;
                            else //重工时新加组件
                                if (!InputDataIsNotNull())
                                {
                                    ToolsClass.Log("组件：" + sn + " 为新组件，物料信息不能为空", "ABNORMAL", lstView);
                                    return false;
                                }
                        }
                    }
                    #endregion
                }
                else
                {
                    ToolsClass.Log("托号：" + carton + " 从包装系统获取组件信息失败", "ABNORMAL", lstView);
                    break;
                }
            }
            return true;
        }
        #region 私有方法
        /// <summary>
        /// 检查是否为空
        /// </summary>
        /// <returns></returns>
        private bool InputDataIsNotNull()
        {
            #region
            if (this.ddlByIm.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电流分档不能为空", "ABNORMAL", lstView);
                this.ddlByIm.Focus();
                return false;
            }

            if (this.ddlPackingPattern.Text.Trim().Equals(""))
            {
                ToolsClass.Log("包装方式不能为空", "ABNORMAL", lstView);
                this.ddlPackingPattern.Focus();
                return false;
            }

            #region 电池片的输入格式
            if (!string.IsNullOrEmpty(this.ddlCellTransfer.Text.Trim()))
            {
                int aa = this.ddlCellTransfer.Text.Trim().ToString().Length;
                if (this.ddlCellTransfer.Text.Trim().ToString().Length != 6)
                {
                    ToolsClass.Log("电池片转换效率格式不对! 正确格式例如15.171", "ABNORMAL", lstView);
                    this.ddlCellTransfer.Focus();
                    return false;
                }
                else
                {
                    string CellTransfer = this.ddlCellTransfer.Text.Trim().ToString();
                    string flag = "Y";
                    foreach (char c in CellTransfer)
                    {
                        if ((!char.IsNumber(c)))
                        {
                            char a = '.';
                            if (c.Equals(a))
                                continue;
                            else
                            {
                                flag = "N";
                                break;
                            }
                        }
                    }
                    if (flag.Equals("N"))
                    {
                        ToolsClass.Log("电池片转换效率格式不对! 正确格式例如15.171", "ABNORMAL", lstView);
                        this.ddlCellTransfer.Focus();
                        return false;
                    }
                }
            }
            else
            {
                //ToolsClass.Log("电池片转换效率不能为空", "ABNORMAL", lstView);
                //this.ddlCellTransfer.Focus();
                //return false;
            }
            #endregion

            if (this.ddlCellBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电池片批次不能为空", "ABNORMAL", lstView);
                this.ddlCellBatch.Focus();
                return false;
            }

            if (this.ddltxtCell.Text.Trim().Equals(""))
            {
                ToolsClass.Log("电池片不能为空", "ABNORMAL", lstView);
                this.ddlCellBatch.Focus();
                return false;
            }

            if (this.ddlGlassBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("玻璃批次不能为空", "ABNORMAL", lstView);
                this.ddlGlassBatch.Focus();
                return false;
            }

            if (this.ddltxtGlassCode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("玻璃不能为空", "ABNORMAL", lstView);
                this.ddlGlassBatch.Focus();
                return false;
            }
            if (this.ddlEVABatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("EVA 批次不能为空", "ABNORMAL", lstView);
                this.ddlEVABatch.Focus();
                return false;
            }

            if (this.ddltxtEVACode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("EVA 物料不能为空", "ABNORMAL", lstView);
                this.ddlEVABatch.Focus();
                return false;
            }

            if (this.ddlTPTBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("背板批次不能为空", "ABNORMAL", lstView);
                this.ddlTPTBatch.Focus();
                return false;
            }

            if (this.ddltxtTPTCode.Text.Trim().Equals(""))
            {
                ToolsClass.Log("背板物料不能为空", "ABNORMAL", lstView);
                this.ddlTPTBatch.Focus();
                return false;
            }

            if (this.ddlConBoxBatch.Text.Trim().Equals(""))
            {
                ToolsClass.Log("接线盒批次不能为空", "ABNORMAL", lstView);
                this.ddlConBoxBatch.Focus();
                return false;
            }

            if (this.ddltxtConBoxCod.Text.Trim().Equals(""))
            {
                ToolsClass.Log("接线盒物料不能为空", "ABNORMAL", lstView);
                this.ddlConBoxBatch.Focus();
                return false;
            }

            if (!FormCover.WOTypeCode.Trim().ToUpper().Equals("ZP09"))
            {
                if (this.ddlShortAIFrameBatch.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("短边框批次不能为空", "ABNORMAL", lstView);
                    this.ddlShortAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddltxtShortAIFrameCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("短边框物料不能为空", "ABNORMAL", lstView);
                    this.ddlShortAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddlAIFrameBatch.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("长边框批次不能为空", "ABNORMAL", lstView);
                    this.ddlAIFrameBatch.Focus();
                    return false;
                }

                if (this.ddltxtAIFrameCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("长边框物料不能为空", "ABNORMAL", lstView);
                    this.ddlAIFrameBatch.Focus();
                    return false;
                }
            }
            return true;
            #endregion
        }
        /// <summary>
        /// 工单：检查输入的内容是否合法
        /// </summary>
        private bool CheckInput()
        {
            if (WoType.Equals("ZCRO"))
            {
                if (!CheckNewSnIsNull(CartonArrayS))
                    return false;
            }
            else
            {
                if (!InputDataIsNotNull())
                    return false;
            }
            return true;
        }
        /// <summary>
        /// 初始化界面：工单信息维护
        /// </summary>
        private void ClearData(bool flag)
        {
            this.txtWo.Clear();
            this.txtFactory.Clear();
            this.txtMitemCode.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();

            this.ddltxtGlassCode.Items.Clear();
            this.ddltxtGlassCode.Text = "";
            this.ddltxtGlassCode.SelectedIndex = -1;

            this.ddltxtEVACode.Items.Clear();
            this.ddltxtEVACode.Text = "";
            this.ddltxtEVACode.SelectedIndex = -1;


            this.ddltxtTPTCode.Items.Clear();
            this.ddltxtTPTCode.Text = "";
            this.ddltxtTPTCode.SelectedIndex = -1;

            this.ddltxtConBoxCod.Items.Clear();
            this.ddltxtConBoxCod.Text = "";
            this.ddltxtConBoxCod.SelectedIndex = -1;

            this.ddltxtAIFrameCode.Items.Clear();
            this.ddltxtAIFrameCode.Text = "";
            this.ddltxtAIFrameCode.SelectedIndex = -1;

            this.ddltxtCell.Items.Clear();
            this.ddltxtCell.Text = "";
            this.ddltxtCell.SelectedIndex = -1;

            this.ddltxtShortAIFrameCode.Items.Clear();
            this.ddltxtShortAIFrameCode.Text = "";
            this.ddltxtShortAIFrameCode.SelectedIndex = -1;

            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();

            if (flag)
                this.ddlWoType.SelectedIndex = -1;

            this.ddlWostatus.SelectedIndex = -1;

            this.ddlByIm.SelectedIndex = -1;

            this.ddlGlassLength.SelectedIndex = -1;

            this.ddlPackingPattern.SelectedIndex = -1;

            this.ddlCancelStorageFlag.SelectedIndex = -1;

            this.ddlIsOnlyPacking.SelectedIndex = -1;

            this.ddlCellBatch.Items.Clear();
            this.ddlCellBatch.Text = "";
            this.ddlCellBatch.SelectedIndex = -1;


            this.ddlShortAIFrameBatch.Items.Clear();
            this.ddlShortAIFrameBatch.Text = "";
            this.ddlShortAIFrameBatch.SelectedIndex = -1;

            this.ddlCellNetBoard.SelectedIndex = -1;

            this.ddlCellTransfer.Items.Clear();
            this.ddlCellTransfer.Text = "";
            this.ddlCellTransfer.SelectedIndex = -1;

            this.ddlGlassBatch.Items.Clear();
            this.ddlGlassBatch.Text = "";
            this.ddlGlassBatch.SelectedIndex = -1;

            this.ddlEVABatch.Items.Clear();
            this.ddlEVABatch.Text = "";
            this.ddlEVABatch.SelectedIndex = -1;

            this.ddlTPTBatch.Items.Clear();
            this.ddlTPTBatch.Text = "";
            this.ddlTPTBatch.SelectedIndex = -1;


            this.ddlConBoxBatch.Items.Clear();
            this.ddlConBoxBatch.Text = "";
            this.ddlConBoxBatch.SelectedIndex = -1;

            this.ddlAIFrameBatch.Items.Clear();
            this.ddlAIFrameBatch.Text = "";
            this.ddlAIFrameBatch.SelectedIndex = -1;

            this.checkBox2.Checked = false;
            this.txtWO1.Clear();
            this.txtWO1.Enabled = false;
            this.pictureBox1.Enabled = false;


            this.ddlCellBatch.Enabled = true;
            this.ddltxtCell.Enabled = true;

            this.ddlGlassBatch.Enabled = true;
            this.ddltxtGlassCode.Enabled = true;

            this.ddlEVABatch.Enabled = true;
            this.ddltxtEVACode.Enabled = true;

            this.ddlTPTBatch.Enabled = true;
            this.ddltxtTPTCode.Enabled = true;

            this.ddlConBoxBatch.Enabled = true;
            this.ddltxtConBoxCod.Enabled = true;

            this.ddlShortAIFrameBatch.Enabled = true;
            this.ddltxtShortAIFrameCode.Enabled = true;

            this.ddlAIFrameBatch.Enabled = true;
            this.ddltxtAIFrameCode.Enabled = true;
        }

        private void ResetFormat(bool flag)
        {
            if (flag)
            {
                this.ddlCellBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlCellBatch.ForeColor = Color.Black;
                this.lblCellBatch.ForeColor = Color.Black;
                this.lblCell.ForeColor = Color.Black;
                this.ddltxtCell.ForeColor = Color.Black;
                this.PicBoxCellBatch.Enabled = true;

                this.ddlGlassBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlGlassBatch.ForeColor = Color.Black;
                this.lblGlassBatch.ForeColor = Color.Black;
                this.lblGlass.ForeColor = Color.Black;
                this.ddltxtGlassCode.ForeColor = Color.Black;
                this.PicBoxGlassBatch.Enabled = true;

                this.ddlEVABatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlEVABatch.ForeColor = Color.Black;
                this.lblEVABatch.ForeColor = Color.Black;
                this.lblEVA.ForeColor = Color.Black;
                this.ddltxtEVACode.ForeColor = Color.Black;
                this.PicBoxEVABatch.Enabled = true;

                this.ddlTPTBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlTPTBatch.ForeColor = Color.Black;
                this.lblTPTBatch.ForeColor = Color.Black;
                this.lblTPT.ForeColor = Color.Black;
                this.ddltxtTPTCode.ForeColor = Color.Black;
                this.PicBoxTPTBatch.Enabled = true;

                this.ddlAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrame.ForeColor = Color.Black;
                this.ddltxtAIFrameCode.ForeColor = Color.Black;
                this.PicBoxAIFrameBatch.Enabled = true;


                this.ddlShortAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrame.ForeColor = Color.Black;
                this.ddltxtShortAIFrameCode.ForeColor = Color.Black;
                this.PicBoxShortAIFrameBatch.Enabled = true;

                this.ddlConBoxBatch.DropDownStyle = ComboBoxStyle.DropDown;
                this.ddlConBoxBatch.ForeColor = Color.Black;
                this.lblConBoxBatch.ForeColor = Color.Black;
                this.lblConBox.ForeColor = Color.Black;
                this.ddltxtConBoxCod.ForeColor = Color.Black;
                this.PicBoxConBoxBatch.Enabled = true;
            }
            else
            {
                this.ddlCellBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlCellBatch.ForeColor = Color.Black;
                this.lblCellBatch.ForeColor = Color.Black;
                this.lblCell.ForeColor = Color.Black;
                this.ddltxtCell.ForeColor = Color.Black;
                this.PicBoxCellBatch.Enabled = true;

                this.ddlGlassBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlGlassBatch.ForeColor = Color.Black;
                this.lblGlassBatch.ForeColor = Color.Black;
                this.lblGlass.ForeColor = Color.Black;
                this.ddltxtGlassCode.ForeColor = Color.Black;
                this.PicBoxGlassBatch.Enabled = true;

                this.ddlEVABatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlEVABatch.ForeColor = Color.Black;
                this.lblEVABatch.ForeColor = Color.Black;
                this.lblEVA.ForeColor = Color.Black;
                this.ddltxtEVACode.ForeColor = Color.Black;
                this.PicBoxEVABatch.Enabled = true;

                this.ddlTPTBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlTPTBatch.ForeColor = Color.Black;
                this.lblTPTBatch.ForeColor = Color.Black;
                this.lblTPT.ForeColor = Color.Black;
                this.ddltxtTPTCode.ForeColor = Color.Black;
                this.PicBoxTPTBatch.Enabled = true;

                this.ddlAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrameBatch.ForeColor = Color.Black;
                this.lblAIFrame.ForeColor = Color.Black;
                this.ddltxtAIFrameCode.ForeColor = Color.Black;
                this.PicBoxAIFrameBatch.Enabled = true;


                this.ddlShortAIFrameBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrameBatch.ForeColor = Color.Black;
                this.lblShortAIFrame.ForeColor = Color.Black;
                this.ddltxtShortAIFrameCode.ForeColor = Color.Black;
                this.PicBoxShortAIFrameBatch.Enabled = true;

                this.ddlConBoxBatch.DropDownStyle = ComboBoxStyle.DropDownList;
                this.ddlConBoxBatch.ForeColor = Color.Black;
                this.lblConBoxBatch.ForeColor = Color.Black;
                this.lblConBox.ForeColor = Color.Black;
                this.ddltxtConBoxCod.ForeColor = Color.Black;
                this.PicBoxConBoxBatch.Enabled = true;
            }
        }
        #endregion

        #region 工单查询
        private void txtWoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请先选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    this.ddlWoType.SelectAll();
                    this.txtWoOrder.Clear();
                    return;
                }

                if (string.IsNullOrEmpty(Convert.ToString(this.txtWoOrder.Text.Trim())))
                {
                    ToolsClass.Log("输入的工单不能为空,请重新输入!", "ABNORMAL", lstView);
                    this.txtWoOrder.Focus();
                    this.txtWoOrder.SelectAll();
                    return;
                }

                SetWOData(true);
            }
        }
        private void txtWoOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    return;
                }
                PicBoxWO_Click(null, null);
            }

        }
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                this.ddlWoType.Focus();
                return;
            }

            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), WoType);
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
        }

        private void SetWOData(bool flag)
        {
            ClearData(false);

            if (Convert.ToString(this.txtWoOrder.Text.Trim()).Equals(""))
            {
                ToolsClass.Log("输入的工单为空，请确认！", "ABNORMAL", lstView);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }

            
            
            DataSet wo = ProductStorageDAL.GetWoInfo(Convert.ToString(this.txtWoOrder.Text.Trim()), WoType);
            if (wo != null && wo.Tables.Count > 0)
            {
                SetWoInfo(wo, WoType);
            }
            else
            {
                if (flag)
                {
                    ToolsClass.Log("工单：" + Convert.ToString(this.txtWoOrder.Text.Trim()) + " 没有从SAP下载或工单类型不一致,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
                else
                {
                    ToolsClass.Log("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "在SAP没有做物料转储,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
            }
            this.txtWoOrder.Clear();
        }
        private string ELPS_WOTYE = "";//elps的标志
        private void SetWoInfo(DataSet wo, string _Wotype)
        {
            DataTable dtWo = wo.Tables[0];
            if (dtWo.Rows.Count == 0)
            {
                ToolsClass.Log("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "不存在,请确认!", "ABNORMAL", lstView);
                return;
            }
            string NewWoType = "";
            DataRow row = dtWo.Rows[0];
            this.txtWo.Text = Convert.ToString(row["TWO_NO"]);
            this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
            this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
            this.txtlocation.Text = Convert.ToString(row["RESV04"]);
            this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
            this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
            this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
            NewWoType = Convert.ToString(row["RESV05"]).Trim();
            if (Convert.ToString(row["RESV05"]).Trim().Equals("ZP13"))
                ELPS_WOTYE = "ZP13";

            #region
            //电池片
            foreach (DataRow rows in wo.Tables[1].Rows)
            {
                if (!this.ddlCellBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlCellBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //玻璃
            foreach (DataRow rows in wo.Tables[2].Rows)
            {
                if (!this.ddlGlassBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlGlassBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //EVN
            foreach (DataRow rows in wo.Tables[3].Rows)
            {
                if (!this.ddlEVABatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlEVABatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //TPT
            foreach (DataRow rows in wo.Tables[4].Rows)
            {
                if (!this.ddlTPTBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlTPTBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //CONBOX
            foreach (DataRow rows in wo.Tables[5].Rows)
            {
                if (!this.ddlConBoxBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlConBoxBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //AIFRAME 1 长边框
            foreach (DataRow rows in wo.Tables[6].Rows)
            {
                if (!this.ddlAIFrameBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlAIFrameBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            //AIFRAME 2 短边框
           // foreach (DataRow rows in wo.Tables[1].Rows)
            foreach (DataRow rows in wo.Tables[7].Rows)
            {
                if (!this.ddlShortAIFrameBatch.Items.Contains(Convert.ToString(rows["Batch"]).Trim()))
                    this.ddlShortAIFrameBatch.Items.Add(Convert.ToString(rows["Batch"]).Trim());
            }
            #endregion
            #region 小组件工单如果没有发料，给一个默认值
            if (NewWoType.Equals("ZP09"))
            {
                //电池片
                if (this.ddlCellBatch.Items.Count < 1)
                {
                    DataTable dtCell = ProductStorageDAL.GetStorageMitemCodeInfo("CELL");
                    if (dtCell != null)
                    {
                        DataRow rowCell = dtCell.Rows[0];
                        this.ddlCellBatch.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_02"]));
                        this.ddlCellBatch.SelectedIndex = 0;
                        this.ddlCellBatch.Enabled = false;

                        this.ddltxtCell.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_03"]));
                        this.ddltxtCell.SelectedIndex = 0;
                        this.ddltxtCell.Enabled = false;
                    }
                }
                //玻璃
                if (this.ddlGlassBatch.Items.Count < 1)
                {
                    DataTable dtGLASS = ProductStorageDAL.GetStorageMitemCodeInfo("GLASS");
                    if (dtGLASS != null)
                    {
                        DataRow rowGLASS = dtGLASS.Rows[0];
                        this.ddlGlassBatch.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_02"]));
                        this.ddlGlassBatch.SelectedIndex = 0;
                        this.ddlGlassBatch.Enabled = false;

                        this.ddltxtGlassCode.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_03"]));
                        this.ddltxtGlassCode.SelectedIndex = 0;
                        this.ddltxtGlassCode.Enabled = false;
                    }
                }
                // EVA
                if (this.ddlEVABatch.Items.Count < 1)
                {
                    DataTable dtEVA = ProductStorageDAL.GetStorageMitemCodeInfo("EVA");
                    if (dtEVA != null)
                    {
                        DataRow rowEVA = dtEVA.Rows[0];
                        this.ddlEVABatch.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_02"]));
                        this.ddlEVABatch.SelectedIndex = 0;
                        this.ddlEVABatch.Enabled = false;

                        this.ddltxtEVACode.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_03"]));
                        this.ddltxtEVACode.SelectedIndex = 0;
                        this.ddltxtEVACode.Enabled = false;
                    }
                }
                // TPT
                if (this.ddlTPTBatch.Items.Count < 1)
                {
                    DataTable dtTPT = ProductStorageDAL.GetStorageMitemCodeInfo("TPT");
                    if (dtTPT != null)
                    {
                        DataRow rowTPT = dtTPT.Rows[0];
                        this.ddlTPTBatch.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_02"]));
                        this.ddlTPTBatch.SelectedIndex = 0;
                        this.ddlTPTBatch.Enabled = false;

                        this.ddltxtTPTCode.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_03"]));
                        this.ddltxtTPTCode.SelectedIndex = 0;
                        this.ddltxtTPTCode.Enabled = false;
                    }
                }
                //长边框
                if (this.ddlAIFrameBatch.Items.Count < 1)
                {
                    DataTable dtAIFRAMELONG = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-LONG");
                    if (dtAIFRAMELONG != null)
                    {
                        DataRow rowAIFRAMELONG = dtAIFRAMELONG.Rows[0];
                        this.ddlAIFrameBatch.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_02"]));
                        this.ddlAIFrameBatch.SelectedIndex = 0;
                        this.ddlAIFrameBatch.Enabled = false;

                        this.ddltxtAIFrameCode.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_03"]));
                        this.ddltxtAIFrameCode.SelectedIndex = 0;
                        this.ddltxtAIFrameCode.Enabled = false;
                    }
                }
                //短边框
                if (this.ddlShortAIFrameBatch.Items.Count < 1)
                {
                    DataTable dtAIFRAMESHORT = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-SHORT");
                    if (dtAIFRAMESHORT != null)
                    {
                        DataRow rowAIFRAMESHORT = dtAIFRAMESHORT.Rows[0];
                        this.ddlShortAIFrameBatch.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_02"]));
                        this.ddlShortAIFrameBatch.SelectedIndex = 0;
                        this.ddlShortAIFrameBatch.Enabled = false;

                        this.ddltxtShortAIFrameCode.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_03"]));
                        this.ddltxtShortAIFrameCode.SelectedIndex = 0;
                        this.ddltxtShortAIFrameCode.Enabled = false;
                    }
                }
                //接线盒
                if (this.ddlConBoxBatch.Items.Count < 1)
                {
                    DataTable dtCONBOX = ProductStorageDAL.GetStorageMitemCodeInfo("CONBOX");
                    if (dtCONBOX != null)
                    {
                        DataRow rowCONBOX = dtCONBOX.Rows[0];
                        this.ddlConBoxBatch.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_02"]));
                        this.ddlConBoxBatch.SelectedIndex = 0;
                        this.ddlConBoxBatch.Enabled = false;

                        this.ddltxtConBoxCod.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_03"]));
                        this.ddltxtConBoxCod.SelectedIndex = 0;
                        this.ddltxtConBoxCod.Enabled = false;
                    }
                }
            }
            #endregion

            #region 双玻组件的虚拟物料指定批次
            var bom = wo.Tables[8];
            if (bom != null && bom.Rows != null && bom.Rows.Count > 0)
            {
                //电池片
                if (this.ddlCellBatch.Items.Count < 1)
                {
                    var cellRows = bom.Select("MaterialCategory='CELL' AND Resv04='1'");
                    if (cellRows != null && cellRows.Length > 0)
                    {
                        var cellRow = cellRows[0];
                        this.ddlCellBatch.Items.Add(Convert.ToString(cellRow["Resv05"]));
                        this.ddlCellBatch.SelectedIndex = 0;
                        this.ddlCellBatch.Enabled = false;

                        this.ddltxtCell.Items.Add(Convert.ToString(cellRow["MaterialCode"]));
                        this.ddltxtCell.SelectedIndex = 0;
                        this.ddltxtCell.Enabled = false;
                    }
                }
                //玻璃
                if (this.ddlGlassBatch.Items.Count < 1)
                {
                    var glassRows = bom.Select("MaterialCategory='GLASS' AND Resv04='1'");
                    if (glassRows != null && glassRows.Length > 0)
                    {
                        var glassRow = glassRows[0];
                        this.ddlGlassBatch.Items.Add(Convert.ToString(glassRow["Resv05"]));
                        this.ddlGlassBatch.SelectedIndex = 0;
                        this.ddlGlassBatch.Enabled = false;

                        this.ddltxtGlassCode.Items.Add(Convert.ToString(glassRow["MaterialCode"]));
                        this.ddltxtGlassCode.SelectedIndex = 0;
                        this.ddltxtGlassCode.Enabled = false;
                    }
                }
                // EVA
                if (this.ddlEVABatch.Items.Count < 1)
                {
                    var evaRows = bom.Select("MaterialCategory='EVA' AND Resv04='1'");
                    if (evaRows != null && evaRows.Length > 0)
                    {
                        var evaRow = evaRows[0];
                        this.ddlEVABatch.Items.Add(Convert.ToString(evaRow["Resv05"]));
                        this.ddlEVABatch.SelectedIndex = 0;
                        this.ddlEVABatch.Enabled = false;

                        this.ddltxtEVACode.Items.Add(Convert.ToString(evaRow["MaterialCode"]));
                        this.ddltxtEVACode.SelectedIndex = 0;
                        this.ddltxtEVACode.Enabled = false;
                    }
                }
                // TPT
                if (this.ddlTPTBatch.Items.Count < 1)
                {
                    var tptRows = bom.Select("MaterialCategory='TPT' AND Resv04='1'");
                    if (tptRows != null && tptRows.Length > 0)
                    {
                        var tptRow = tptRows[0];
                        this.ddlTPTBatch.Items.Add(Convert.ToString(tptRow["Resv05"]));
                        this.ddlTPTBatch.SelectedIndex = 0;
                        this.ddlTPTBatch.Enabled = false;

                        this.ddltxtTPTCode.Items.Add(Convert.ToString(tptRow["MaterialCode"]));
                        this.ddltxtTPTCode.SelectedIndex = 0;
                        this.ddltxtTPTCode.Enabled = false;
                    }
                }
            }
            #endregion
        }
        #endregion

        #region 托号查询
        private void txtCartonQuery_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    ToolsClass.Log("托号不能为空!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
                DataTable dt = ProductStorageDAL.GetCartonStorageInfoByCarton(Convert.ToString(this.txtCarton.Text.Trim()));
                if (dt != null && dt.Rows.Count > 0)
                    SetDataGridView(dt);
                else
                {
                    ToolsClass.Log("没有查询到数据!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
            }
        }
        private void txtCartonQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                PicBoxCarton_Click(null, null);
            }
        }
        private void PicBoxCarton_Click(object sender, EventArgs e)
        {
            var frm = new FormCartonInfoQuery(this.txtCarton.Text.Trim().ToString());
            frm.ShowDialog();
            DataTable dt = frm.CartonList;
            if (frm != null)
                frm.Dispose();
            SetDataGridView(dt);
        }

        private bool checkrepeat(string carton)
        {
            foreach (DataGridViewRow datarow in dataGridView1.Rows)
            {
                if (datarow.Cells[2].Value != null && datarow.Cells[2].Value.Equals(carton))
                {
                    ToolsClass.Log("托号：" + carton + " 已在列表第 " + (datarow.Index + 1) + " 行", "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }
        private void SetDataGridView(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string cartonStatus = "";
                    if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        cartonStatus = "已测试";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        cartonStatus = "已包装";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        cartonStatus = "已入库";
                    else
                        cartonStatus = Convert.ToString(row["CartonStatus"]);

                    if (checkrepeat(Convert.ToString(row["CartonID"])))
                        dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { false, this.dataGridView1.Rows.Count + 1, Convert.ToString(row["CartonID"]), null, cartonStatus, null, Convert.ToString(row["Cust_BoxID"]), Convert.ToString(row["SNQTY"]) });

                }
                this.txtCarton.Clear();
                this.txtCarton.Focus();
            }
            else
            {
                ToolsClass.Log("没有选数据!", "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        #region 物料查询

        #region 电池片
        private void ddlCellBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlCellBatch.Text.Trim().ToString(), "Cell", this.ddlCellBatch, this.ddltxtCell);
            }

        }
        private void ddlCellBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlCellBatch.Text.Trim(), this.ddltxtCell, true, "Cell");
                if (this.ddltxtCell.Text.Equals(""))
                    this.ddlCellBatch.Text = "";
            }
        }
        private void PicBoxCellBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlCellBatch.Text.Trim().ToString(), "Cell", this.ddlCellBatch, this.ddltxtCell);
        }

        private void ddlCellBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlCellBatch.Text.Trim(), this.ddltxtCell, true, "Cell");
        }

        private void ddlCellBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlCellBatch.Text.Trim(), this.ddltxtCell, false, "Cell");
            if (this.ddltxtCell.Text.Equals(""))
                this.ddlCellBatch.Text = "";
        }

        #endregion

        #region GLASS
        private void ddlGlassBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlGlassBatch.Text.Trim(), this.ddltxtGlassCode, true, "GLASS");
                if (this.ddltxtGlassCode.Text.Equals(""))
                    this.ddlGlassBatch.Text = "";
            }
        }
        private void ddlGlassBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlGlassBatch.Text.Trim().ToString(), "GLASS", this.ddlGlassBatch, this.ddltxtGlassCode);
            }
        }

        private void PicBoxGlassBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlGlassBatch.Text.Trim().ToString(), "GLASS", this.ddlGlassBatch, this.ddltxtGlassCode);
        }

        private void ddlGlassBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlGlassBatch.Text.Trim(), this.ddltxtGlassCode, true, "GLASS");
        }
        private void ddlGlassBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlGlassBatch.Text.Trim(), this.ddltxtGlassCode, false, "GLASS");
            if (this.ddltxtGlassCode.Text.Equals(""))
                this.ddlGlassBatch.Text = "";
        }
        #endregion

        #region EVA
        private void ddlEVABatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlEVABatch.Text.Trim(), this.ddltxtEVACode, true, "EVA");
                if (this.ddltxtEVACode.Text.Equals(""))
                    this.ddlEVABatch.Text = "";
            }
        }

        private void ddlEVABatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlEVABatch.Text.Trim().ToString(), "EVA", this.ddlEVABatch, this.ddltxtEVACode);
            }
        }

        private void PicBoxEVABatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlEVABatch.Text.Trim().ToString(), "EVA", this.ddlEVABatch, this.ddltxtEVACode);
        }

        private void ddlEVABatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlEVABatch.Text.Trim(), this.ddltxtEVACode, true, "EVA");
        }

        private void ddlEVABatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlEVABatch.Text.Trim(), this.ddltxtEVACode, false, "EVA");
            if (this.ddltxtEVACode.Text.Equals(""))
                this.ddlEVABatch.Text = "";
        }
        #endregion

        #region TPT
        private void ddlTPTBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlTPTBatch.Text.Trim(), this.ddltxtTPTCode, true, "TPT");
                if (this.ddltxtTPTCode.Text.Equals(""))
                    this.ddlTPTBatch.Text = "";
            }
        }
        private void ddlTPTBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlTPTBatch.Text.Trim().ToString(), "TPT", this.ddlTPTBatch, this.ddltxtTPTCode);
            }
        }
        private void PicBoxTPTBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlTPTBatch.Text.Trim().ToString(), "TPT", this.ddlTPTBatch, this.ddltxtTPTCode);
        }
        private void ddlTPTBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlTPTBatch.Text.Trim(), this.ddltxtTPTCode, true, "TPT");
        }
        private void ddlTPTBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlTPTBatch.Text.Trim(), this.ddltxtTPTCode, false, "TPT");
            if (this.ddltxtTPTCode.Text.Equals(""))
                this.ddlTPTBatch.Text = "";
        }
        #endregion

        #region 接线盒
        private void ddlConBoxBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlConBoxBatch.Text.Trim().ToString(), "CONBOX", this.ddlConBoxBatch, this.ddltxtConBoxCod);
            }
        }

        private void ddlConBoxBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlConBoxBatch.Text.Trim(), this.ddltxtConBoxCod, true, "CONBOX");
                if (this.ddltxtConBoxCod.Text.Equals(""))
                    this.ddlConBoxBatch.Text = "";
            }
        }

        private void PicBoxConBoxBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlConBoxBatch.Text.Trim().ToString(), "CONBOX", this.ddlConBoxBatch, this.ddltxtConBoxCod);
        }

        private void ddlConBoxBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlConBoxBatch.Text.Trim(), this.ddltxtConBoxCod, true, "CONBOX");
        }
        private void ddlConBoxBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlConBoxBatch.Text.Trim(), this.ddltxtConBoxCod, false, "CONBOX");
            if (this.ddltxtConBoxCod.Text.Equals(""))
                this.ddlConBoxBatch.Text = "";
        }
        #endregion

        #region 长边框
        private void ddlAIFrameBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlAIFrameBatch.Text.Trim().ToString(), "AIFRAME-LONG", this.ddlAIFrameBatch, this.ddltxtAIFrameCode);
            }
        }

        private void ddlAIFrameBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlAIFrameBatch.Text.Trim(), this.ddltxtAIFrameCode, true, "AIFRAME-LONG");
                if (this.ddltxtAIFrameCode.Text.Equals(""))
                    this.ddlAIFrameBatch.Text = "";
            }
        }
        private void PicBoxAIFrameBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlAIFrameBatch.Text.Trim().ToString(), "AIFRAME-LONG", this.ddlAIFrameBatch, this.ddltxtAIFrameCode);
        }

        private void ddlAIFrameBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlAIFrameBatch.Text.Trim(), this.ddltxtAIFrameCode, true, "AIFRAME-LONG");
        }
        private void ddlAIFrameBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlAIFrameBatch.Text.Trim(), this.ddltxtAIFrameCode, false, "AIFRAME-LONG");
            if (this.ddltxtAIFrameCode.Text.Equals(""))
                this.ddlAIFrameBatch.Text = "";
        }
        #endregion

        #region 短边框
        private void ddlShortAIFrameBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                SetMitemBatch(this.ddlShortAIFrameBatch.Text.Trim().ToString(), "AIFRAME-SHORT", this.ddlShortAIFrameBatch, this.ddltxtShortAIFrameCode);
            }
        }

        private void ddlShortAIFrameBatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                setMitemCode(this.ddlShortAIFrameBatch.Text.Trim(), this.ddltxtShortAIFrameCode, true, "AIFRAME-SHORT");
                if (this.ddltxtShortAIFrameCode.Text.Equals(""))
                    this.ddlShortAIFrameBatch.Text = "";
            }
        }

        private void ddlShortAIFrameBatch_Leave(object sender, EventArgs e)
        {
            setMitemCode(this.ddlShortAIFrameBatch.Text.Trim(), this.ddltxtShortAIFrameCode, false, "AIFRAME-SHORT");
            if (this.ddltxtShortAIFrameCode.Text.Equals(""))
                this.ddlShortAIFrameBatch.Text = "";
        }

        private void ddlShortAIFrameBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMitemCode(this.ddlShortAIFrameBatch.Text.Trim(), this.ddltxtShortAIFrameCode, true, "AIFRAME-SHORT");
        }

        private void PicBoxShortAIFrameBatch_Click(object sender, EventArgs e)
        {
            SetMitemBatch(this.ddlShortAIFrameBatch.Text.Trim().ToString(), "AIFRAME-SHORT", this.ddlShortAIFrameBatch, this.ddltxtShortAIFrameCode);
        }
        #endregion

        private void ShowBatchInfo(string _BatchNo, string MitemType, object ddl, object txt, string _wo)
        {
            Control ddl_text = ddl as Control;
            Control text = txt as Control;
            var frm = new FormMitemBatch(_BatchNo, MitemType, _wo, WoType);
            frm.ShowDialog();
            ddl_text.Text = frm.Batch;
            text.Text = frm.MitemCode;
            if (frm != null)
                frm.Dispose();
        }

        private void setMitemCode(string batchno, object label, bool flag, string MitemType)
        {
            if (this.txtWo.Text.Trim().Equals(""))
            {
                ToolsClass.Log("请先选择工单!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }

            ComboBox c = label as ComboBox;
            c.Items.Clear();

            if (!batchno.Equals(""))
            {
                DataTable MitemCode = null;
                if (!string.IsNullOrEmpty(ELPS_WOTYE))
                    WoType = ELPS_WOTYE;

                if (ProductStorageDAL.GetMulMitemCode(batchno, MitemType, this.txtWo.Text.Trim(), WoType, out MitemCode) != null)
                {
                    foreach (DataRow row in MitemCode.Rows)
                    {
                        if (!c.Items.Contains(Convert.ToString(row["MaterialCode"]).Trim()))
                            c.Items.Add(Convert.ToString(row["MaterialCode"]).Trim());
                    }
                }
                else
                {
                    //MessageBox.Show("此批次:" + batchno + " 没找到所对应的物料，请确认！", MitemType);
                    //c.Text = "";
                    return;
                }
            }
            else
            {
                if (flag)
                {
                    MessageBox.Show("输入的批次号不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }
        private void SetMitemBatch(string batch, string type, object ddl, object txt)
        {
            if (!this.txtWo.Text.Equals(""))
                ShowBatchInfo(batch, type, ddl, txt, this.txtWo.Text.Trim());
            else
            {
                ToolsClass.Log("请先选择工单!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }
        }
        #endregion

        #region 窗体事件
        /// <summary>
        /// 包装方式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlPackingPattern_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlPackingPattern.SelectedIndex == 0)
                PackingPatternValue = "";
            else if (this.ddlPackingPattern.SelectedIndex == 1)
                PackingPatternValue = "A";
            else if (this.ddlPackingPattern.SelectedIndex == 2)
                PackingPatternValue = "B";
            else if (this.ddlPackingPattern.SelectedIndex == 3)
                PackingPatternValue = "C";
            else if (this.ddlPackingPattern.SelectedIndex == 4)
                PackingPatternValue = "D";
            else if (this.ddlPackingPattern.SelectedIndex == 5)
                PackingPatternValue = "E";
        }
        /// <summary>
        /// 工单选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlWoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearData(false);

            if (this.ddlWoType.SelectedIndex == 0)
            {
                WoType = "";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
            }
            else if (this.ddlWoType.SelectedIndex == 1)
            {
                if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M01"))
                {
                    WoType = "ZP01";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                 
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M07"))
                {
                    WoType = "ZP07";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN01"))
                {
                    WoType = "ZPA1";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("YN02"))
                {
                    WoType = "ZPA2";
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
                else
                {
                    WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                    WoType = WoType.Replace("M", "");
                    this.txtWoOrder.Clear();
                    this.txtWoOrder.Focus();
                }
            }
            else if (this.ddlWoType.SelectedIndex == 2)
            {
                
               // WoType = "ZP11";
                switch (FormCover.PlanCode)
                {
                    case "CS":
                        WoType = "ZP11";
                        break;
                    case "VNSM":
                        WoType = "ZPA9";
                        break;
                    case "CAMO":
                        WoType = "ZCRO";
                        break;
                    default:
                        WoType = "";
                        break;
                }
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
            }
        }
        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }

        private void Set_Click(object sender, EventArgs e)
        {
            try
            {
                #region
                if (this.dataGridView1.Rows.Count > 0)
                {
                    if (dgvIndex.Count > 0)
                    {
                        dgvIndex.Clear();
                    }
                    if (CartonArrayS.Count > 0)
                    {
                        CartonArrayS.Clear();
                    }
                    for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                    {
                        if (this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString().ToUpper() == "TRUE")
                        {
                            dgvIndex.Add(i, this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString());
                            CartonArrayS.Add(this.dataGridView1.Rows[i].Cells[2].EditedFormattedValue.ToString());
                        }
                    }
                    if (dgvIndex.Count < 1)
                    {
                        ToolsClass.Log("没有选中要设置的数据,请确认!", "ABNORMAL", lstView);
                        return;
                    }
                }
                else
                {
                    ToolsClass.Log("没有要设置的数据,请确认!", "ABNORMAL", lstView);
                    return;
                }

                if (!CheckInput())
                    return;

                foreach (int RowIndex in dgvIndex.Keys)
                {
                    this.dataGridView1.Rows[RowIndex].Cells["CartonStatus"].Value = "待入库";
                    this.dataGridView1.Rows[RowIndex].Cells["OrderNo"].Value = this.txtWo.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["ProductCode"].Value = this.txtMitemCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["Factory"].Value = this.txtPlanCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["Workshop"].Value = this.txtFactory.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["PackingLocation"].Value = this.txtlocation.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["CellEff"].Value = this.ddlCellTransfer.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["OrderStatus"].Value = "1";
                    this.dataGridView1.Rows[RowIndex].Cells["ByIm"].Value = ByIm_flag;
                    this.dataGridView1.Rows[RowIndex].Cells["CellCode"].Value = this.ddltxtCell.Text.Trim();
                    this.dataGridView1.Rows[RowIndex].Cells["CellBatch"].Value = this.ddlCellBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["CellPrintMode"].Value = "";// this.ddlCellNetBoard.SelectedItem; 网版
                    this.dataGridView1.Rows[RowIndex].Cells["GlassCode"].Value = this.ddltxtGlassCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["GlassBatch"].Value = this.ddlGlassBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["EvaCode"].Value = this.ddltxtEVACode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["EvaBatch"].Value = this.ddlEVABatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["TptCode"].Value = this.ddltxtTPTCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["TptBatch"].Value = this.ddlTPTBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["ConBoxCode"].Value = this.ddltxtConBoxCod.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["ConBoxBatch"].Value = this.ddlConBoxBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["LongFrameCode"].Value = this.ddltxtAIFrameCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["LongFrameBatch"].Value = this.ddlAIFrameBatch.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["GlassThickness"].Value = "";// Convert.ToString(this.ddlGlassLength.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["PackingMode"].Value = PackingPatternValue;
                    this.dataGridView1.Rows[RowIndex].Cells["ShortFrameBatch"].Value = Convert.ToString(this.ddlShortAIFrameBatch.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["ShortFrameCode"].Value = Convert.ToString(this.ddltxtShortAIFrameCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["IsCancelPacking"].Value = "1";
                    this.dataGridView1.Rows[RowIndex].Cells["SalesOrderNo"].Value = Convert.ToString(this.txtSalesOrderNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["SalesItemNo"].Value = Convert.ToString(this.txtSalesItemNo.Text.Trim());
                    //默认值为1
                    this.dataGridView1.Rows[RowIndex].Cells["IsOnlyPacking"].Value = "1";
                    //外协后工单
                    this.dataGridView1.Rows[RowIndex].Cells["OrderNo1"].Value = this.txtWO1.Text.Trim();
                }
                ToolsClass.Log("设置成功!", "NORMAL", lstView);
                Reset_Click(null, null);
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("设置数据时发生异常：" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            ClearData(true);
            ResetFormat(false);
            this.txtWoOrder.SelectAll();
            this.txtWoOrder.Focus();
            return;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Left = chbSelected.Left - i + j;
            }

            else if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {

                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Top = chbSelected.Top - i + j;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 2))
            {
                string CartonNumber = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CartonID"].Value);
                string WoNo = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["OrderNo"].Value);
                string CellCode = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CellCode"].Value);
                string CellBatch = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CellBatch"].Value);
                if (!string.IsNullOrEmpty(CartonNumber) && (!string.IsNullOrEmpty(WoNo)))
                {
                    var frm = new FormModifySNCELL(CartonNumber, CellCode, CellBatch, WoNo, _ListPackingSNCellInfo);
                    if (frm.ShowDialog() != DialogResult.No)
                        frm.Show();
                    _ListPackingSNCellInfo = frm._list;
                    if (frm != null)
                    {
                        frm.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("选中的托号还没有设置工单信息", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridViewCheckBoxCell dgcb = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells["Selected"];
                if ((bool)dgcb.FormattedValue)
                {
                    dgcb.Value = false;
                }
                else
                {
                    dgcb.Value = true;
                }
            }
        }

        private void SetIsEnable(bool flag)
        {
            if (flag)
            {
                CartonQueryList.Enabled = true;
                this.button1.Enabled = true;
                this.Save.Enabled = true;
                this.Set.Enabled = true;
                this.Reset.Enabled = true;
                this.splitContainer3.Panel1.Enabled = true;
                this.dataGridView1.Enabled = true;
                this.chbSelected.Enabled = true;

            }
            else
            {
                CartonQueryList.Enabled = false;
                this.button1.Enabled = false;
                this.Save.Enabled = false;
                this.Set.Enabled = false;
                this.Reset.Enabled = false;
                this.splitContainer3.Panel1.Enabled = false;
                this.dataGridView1.Enabled = false;
                this.chbSelected.Enabled = false;
            }
        }

        /// <summary>
        /// 存储要入库的箱号
        /// </summary>
        List<string> cartonList = new List<string>();
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                SetIsEnable(false);

                #region
                if (this.dataGridView1.Rows.Count < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有要入库的数据!", "ABNORMAL", lstView);
                    return;
                }

                if (cartonList.Count > 0)
                    cartonList.Clear();

                int StorageCount = 0;
                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if (Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE" && dgvRow.Cells[3].Value != null)
                    {
                        StorageCount = StorageCount + 1;
                        if (!cartonList.Contains(Convert.ToString(dgvRow.Cells[2].Value)))
                            cartonList.Add(Convert.ToString(dgvRow.Cells[2].Value));
                    }
                }

                if (StorageCount == 0)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有选中要入库的数据!", "ABNORMAL", lstView);
                    return;
                }


                if (_ListSapWo.Count > 0)
                    _ListSapWo.Clear();

                DateTime CurrentTime = DateTime.Now;
                string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期               
                string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码

                var tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
                var createdOn = DT.DateTime().LongDateTime;
                var createdBy = FormCover.CurrUserName;
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
               // DataTable cartonTable = null;
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    DataTable cartonTable = null;
                    if (!(bool)this.dataGridView1.Rows[i].Cells[0].Value)
                        continue;

                    //获取打包的数据
                    DataTable newdt = ProductStorageDAL.GetCartonInfo(Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonID"].Value));
                    if (newdt == null || newdt.Rows.Count == 0)
                    {
                        SetIsEnable(true);
                        ToolsClass.Log("托号：" + Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonID"].Value) + "获取组件信息失败", "ABNORMAL", lstView);
                        continue;
                    }

                    #region
                    foreach (DataRow newdtrow in newdt.Rows)
                    {
                        SapWoModule SapWo = new SapWoModule();

                        //电池片处理
                        for (int m = 0; m < _ListPackingSNCellInfo.Count; m++)
                        {
                            if (_ListPackingSNCellInfo[m].SN.Equals(Convert.ToString(newdtrow["SN"]).Trim()))
                            {
                                SapWo.CellCode = _ListPackingSNCellInfo[m].CellCode;
                                SapWo.CellBatch = _ListPackingSNCellInfo[m].CellBatch;
                                break;
                            }
                        }

                        if (SapWo.CellCode == null || SapWo.CellBatch == null)
                        {
                            SapWo.CellCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellCode"].Value);
                            SapWo.CellBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellBatch"].Value);
                        }
                        SapWo.ActionCode = "I";
                        SapWo.SysId = Convert.ToString(newdtrow["SN"]);

                        if (Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo1"].Value).Trim().Equals(""))
                            SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value);//外协后工单
                        else
                        {
                            SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo1"].Value);//外协后工单
                            SapWo.RESV01 = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value);//外协前工单
                        }

                        SapWo.ProductCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ProductCode"].Value);
                        SapWo.PostedOn = PostedOn;
                        SapWo.FinishedOn = Convert.ToDateTime(newdtrow["ProcessDateTime"]).ToString("yyyy-MM-dd HH:mm:ss.fff");
                        SapWo.Unit = "PC";
                        SapWo.Factory = Convert.ToString(this.dataGridView1.Rows[i].Cells["Factory"].Value);
                        SapWo.Workshop = Convert.ToString(this.dataGridView1.Rows[i].Cells["Workshop"].Value);
                        SapWo.PackingLocation = Convert.ToString(this.dataGridView1.Rows[i].Cells["PackingLocation"].Value);
                        SapWo.CellEff = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellEff"].Value);
                        SapWo.ModuleSN = Convert.ToString(newdtrow["SN"]);
                        SapWo.CartonNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonID"].Value);
                        SapWo.TestPower = Convert.ToString(newdtrow["Pmax"]);
                        SapWo.StdPower = Convert.ToString(newdtrow["stdPower"]);
                        SapWo.OrderStatus = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderStatus"].Value);
                        SapWo.PostKey = PostKey;
                        SapWo.ModuleGrade = Convert.ToString(newdtrow["ModuleClass"]);
                        SapWo.ByIm = Convert.ToString(this.dataGridView1.Rows[i].Cells["ByIm"].Value);
                        SapWo.CellPrintMode = Convert.ToString(this.dataGridView1.Rows[i].Cells["CellPrintMode"].Value);
                        SapWo.GlassCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassCode"].Value);
                        SapWo.GlassBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassBatch"].Value);
                        SapWo.EvaCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["EvaCode"].Value);
                        SapWo.EvaBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["EvaBatch"].Value);
                        SapWo.TptCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["TptCode"].Value);
                        SapWo.TptBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["TptBatch"].Value);
                        SapWo.ConBoxCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ConBoxCode"].Value);
                        SapWo.ConBoxBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["ConBoxBatch"].Value);
                        SapWo.LongFrameCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["LongFrameCode"].Value);
                        SapWo.LongFrameBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["LongFrameBatch"].Value);
                        SapWo.SalesItemNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesItemNo"].Value);
                        SapWo.SalesOrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesOrderNo"].Value);
                        SapWo.ShortFrameBatch = Convert.ToString(this.dataGridView1.Rows[i].Cells["ShortFrameBatch"].Value);
                        SapWo.ShortFrameCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ShortFrameCode"].Value);
                        SapWo.PackingMode = Convert.ToString(this.dataGridView1.Rows[i].Cells["PackingMode"].Value);
                        SapWo.GlassThickness = Convert.ToString(this.dataGridView1.Rows[i].Cells["GlassThickness"].Value);
                        SapWo.IsCancelPacking = "1";
                        SapWo.IsOnlyPacking = Convert.ToString(this.dataGridView1.Rows[i].Cells["IsOnlyPacking"].Value);
                        SapWo.CustomerCartonNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["CustomerCartonNo"].Value);
                        SapWo.InnerJobNo = "";

                        _ListSapWo.Add(SapWo);

                        var moduleColor = string.Empty;
                        var tolerance = string.Empty;
                        var isRework = "N";
                        var stdPowerLevel = string.Empty;
                        if (cartonTable == null)
                        {
                            cartonTable = ProductStorageDAL.GetSAPCartonStorageInfo(SapWo.CartonNo);
                        }

                        if (cartonTable != null && cartonTable.Rows != null && cartonTable.Rows.Count > 0)
                        {
                            var rows = cartonTable.Select(string.Format("[组件序列号]='{0}'", SapWo.ModuleSN));
                            if (rows.Length > 0)
                            {
                                moduleColor = Convert.ToString(rows[0]["组件颜色"]);
                                tolerance = Convert.ToString(rows[0]["公差"]);
                                isRework = Convert.ToString(rows[0]["是否重工"]);
                                stdPowerLevel = Convert.ToString(rows[0]["StdPowerLevel"]);
                            }
                        }
                        var tsapReceiptUploadModule = new TsapReceiptUploadModule
                        {
                            Sysid = Guid.NewGuid().ToString(""),
                            CreatedOn = createdOn,
                            CreatedBy = createdBy,
                            GroupHistKey = groupHistKey,
                            CartonNo = SapWo.CartonNo,
                            CustomerCartonNo = SapWo.CustomerCartonNo,
                            FinishedOn = SapWo.FinishedOn,
                            ModuleColor = moduleColor,
                            ModuleSn = SapWo.ModuleSN,
                            OrderNo = SapWo.OrderNo,
                            SalesOrderNo = SapWo.SalesOrderNo,
                            SalesItemNo = SapWo.SalesItemNo,
                            OrderStatus = SapWo.OrderStatus,
                            ProductCode = SapWo.ProductCode,
                            Unit = SapWo.Unit,
                            Factory = SapWo.Factory,
                            Workshop = SapWo.Workshop,
                            PackingLocation = SapWo.PackingLocation,
                            PackingMode = SapWo.PackingMode,
                            CellEff = SapWo.CellEff,
                            TestPower = SapWo.TestPower,
                            StdPower = SapWo.StdPower,
                            ModuleGrade = SapWo.ModuleGrade,
                            ByIm = SapWo.ByIm,
                            Tolerance = tolerance,
                            CellCode = SapWo.CellCode,
                            CellBatch = SapWo.CellBatch,
                            CellPrintMode = SapWo.CellPrintMode,
                            GlassCode = SapWo.GlassCode,
                            GlassBatch = SapWo.GlassBatch,
                            EvaCode = SapWo.EvaCode,
                            EvaBatch = SapWo.EvaBatch,
                            TptCode = SapWo.TptCode,
                            TptBatch = SapWo.TptBatch,
                            ConboxCode = SapWo.ConBoxCode,
                            ConboxBatch = SapWo.ConBoxBatch,
                            ConboxType = string.Empty,
                            LongFrameCode = SapWo.LongFrameCode,
                            LongFrameBatch = SapWo.LongFrameBatch,
                            ShortFrameCode = SapWo.ShortFrameCode,
                            ShortFrameBatch = SapWo.ShortFrameBatch,
                            GlassThickness = SapWo.GlassThickness,
                            IsRework = isRework,
                            IsByProduction = "false",
                            Resv01 = stdPowerLevel
                        };

                        //GET WORKTYPE


                        if (WoType.ToUpper().Equals("ZCRO"))
                        {
                            // if (tsapReceiptUploadModule.OrderNo.StartsWith("111"))
                            tsapReceiptUploadModule.IsRework = "Y";
                        }
                        tsapReceiptUploadModules.Add(tsapReceiptUploadModule);
                    }
                    #endregion
                }

                if (_ListSapWo.Count > 0)
                    
                {
                    string msg = "";
                    if (!ProductStorageDAL.CheckWoStorage(_ListSapWo, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    if (!DataAccess.QueryReworkOrgBatch(tsapReceiptUploadModules, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    //var findAll =
                    //    tsapReceiptUploadModules.FindAll(
                    //        p =>
                    //        string.IsNullOrEmpty(p.ProductCode) || string.IsNullOrEmpty(p.OrderNo) ||
                    //        string.IsNullOrEmpty(p.CellCode) || string.IsNullOrEmpty(p.CellBatch) ||
                    //        string.IsNullOrEmpty(p.GlassCode) || string.IsNullOrEmpty(p.GlassBatch) ||
                    //        string.IsNullOrEmpty(p.EvaCode) || string.IsNullOrEmpty(p.EvaBatch) ||
                    //        string.IsNullOrEmpty(p.TptCode) || string.IsNullOrEmpty(p.TptBatch) ||
                    //        string.IsNullOrEmpty(p.ConboxCode) || string.IsNullOrEmpty(p.ConboxBatch) ||
                    //        string.IsNullOrEmpty(p.LongFrameCode) || string.IsNullOrEmpty(p.LongFrameBatch) ||
                    //        string.IsNullOrEmpty(p.ShortFrameCode) || string.IsNullOrEmpty(p.ShortFrameBatch) ||
                    //        string.IsNullOrEmpty(p.PackingMode) || string.IsNullOrEmpty(p.ModuleGrade) ||
                    //        string.IsNullOrEmpty(p.ByIm) || string.IsNullOrEmpty(p.StdPower) ||
                    //        string.IsNullOrEmpty(p.ModuleColor) || string.IsNullOrEmpty(p.Tolerance));
                    //if (findAll.Count > 0)
                    //{
                    //    const string errorMsg =
                    //        "SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、电流分档、标称功率、组件颜色、公差";
                    //    SetIsEnable(true);
                    //    ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                    //    return;
                    //}

                    List<string> listMessages = SapMessages(tsapReceiptUploadModules);
                    if (listMessages.Count > 0)
                    {
                        SetIsEnable(true);
                        foreach (var errorMsg in listMessages)
                        {
                            ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        }
                        return;
                    }

                    if (!DataAccess.IsSapInventoryUseManulUpload(FormCover.CurrentFactory.Trim(), FormCover.InterfaceConnString))
                    {
                        //入库
                        SaveXml(_ListSapWo);

                        //对上传入库成功的做log记录
                        ProductStorageDAL.SaveStorageInfoLog(_ListSapWoLog);
                    }
                    else
                    {
                        //保存数据供手动上传使用
                        SavePreUploadData(_ListSapWo, tsapReceiptUploadModules);
                    }
                    if (_ListSapWoLog.Count > 0)
                        _ListSapWoLog.Clear();
                }

                SetIsEnable(true);
                this.chbSelected.Checked = false;
                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常(处理包装数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        private List<string> SapMessages(List<TsapReceiptUploadModule> sapReceipt)
        {
            List<string> messages = new List<string>();
            try
            {

                //"SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、
                //玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、
                //接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、
                //电流分档、标称功率、组件颜色、公差";

                if (sapReceipt != null && sapReceipt.Count > 0)
                {
                    StringBuilder strtmp;
                    int i = 1;
                    foreach (TsapReceiptUploadModule sapModule in sapReceipt)
                    {
                        strtmp = new StringBuilder();
                        bool blntmp = true;

                        if (!string.IsNullOrEmpty(sapModule.OrderNo))
                        {
                            strtmp.Append("工单号:" + sapModule.OrderNo + "--");
                        }
                        else
                        {
                            strtmp.Append("【第 " + i.ToString() + "行工单号为空】");
                            blntmp = false;
                        }
                        if (!string.IsNullOrEmpty(sapModule.ModuleSn))
                        {
                            strtmp.Append("序列号:" + sapModule.ModuleSn + "   缺失的值【");

                        }
                        if (string.IsNullOrEmpty(sapModule.ProductCode))
                        {
                            strtmp.Append("产品物料代码、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellCode))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellBatch))
                        {
                            strtmp.Append("电池片批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassCode))
                        {
                            strtmp.Append("玻璃批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassBatch))
                        {
                            strtmp.Append("EVA物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaCode))
                        {
                            strtmp.Append("EVA批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaBatch))
                        {
                            strtmp.Append("背板物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptCode))
                        {
                            strtmp.Append("背板批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptBatch))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxCode))
                        {
                            strtmp.Append("接线盒物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxBatch))
                        {
                            strtmp.Append("接线盒批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameCode))
                        {
                            strtmp.Append("长边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameBatch))
                        {
                            strtmp.Append("长边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameCode))
                        {
                            strtmp.Append("短边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameBatch))
                        {
                            strtmp.Append("短边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.PackingMode))
                        {
                            strtmp.Append("包装方式、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleGrade))
                        {
                            strtmp.Append("组件等级、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ByIm))
                        {
                            strtmp.Append("电流分档、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.StdPower))
                        {
                            strtmp.Append("标称功率、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleColor))
                        {
                            strtmp.Append("组件颜色、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.Tolerance))
                        {
                            strtmp.Append("公差、");
                            blntmp = false;
                        }
                        if (strtmp != null && !string.IsNullOrEmpty(strtmp.ToString()) && blntmp == false)
                        {
                            messages.Add(strtmp + "】 【第 " + i.ToString() + " 行】");
                        }
                        i = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add("SAP 数据检查异常:" + ex.Message.ToString());
            }
            return messages;
        }
        private bool SaveXml(List<SapWoModule> listWo)
        {
            try
            {
                #region
                DataTable dt = new DataTable();
                string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(listWo, Encoding.UTF8);
                SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");
                string cars = "";
                if (cartonList.Count > 0)
                {
                    foreach (string carton in cartonList)
                        cars += "," + carton;
                }
                ToolsClass.Log("内部托号：" + cars + " 入库数据保存");
                ToolsClass.Log(SapStorageString);
                try
                {
                    SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                    service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                    string aa = service.packingDataMesToSap(SapStorageString);
                    string bb;
                    dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
                }
                catch (Exception ex)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("连接SAP时,发生异常." + ex.Message + "", "ABNORMAL", lstView);
                    return false;
                }

                if (!listWo.Count.Equals(dt.Rows.Count))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("上传SAP和SAP返回的总数据不一致", "ABNORMAL", lstView);
                    return false;
                }

                try
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        //if (_ListCartonSucess.Count > 0)                        
                        //   _ListCartonSucess.Clear();

                        #region
                        string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                        string posttime = DT.DateTime().LongDateTime;
                        foreach (string carton in cartonList)
                        {
                            string flag = "Y";
                            string wo = "";
                            for (int mmm = 0; mmm < listWo.Count; mmm++)
                            {
                                if (carton == listWo[mmm].CartonNo)
                                {
                                    if (wo.Equals(""))
                                        wo = listWo[mmm].OrderNo;
                                    DataRow[] rows = dt.Select("key = '" + listWo[mmm].ModuleSN + "'");
                                    if (rows.Length > 0)
                                    {
                                        //上传失败
                                        if (Convert.ToString(rows[0]["Result"]).Equals("0"))
                                        {
                                            flag = "N";
                                            SetIsEnable(true);
                                            ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:" + Convert.ToString(rows[0]["Message"]) + "", "ABNORMAL", lstView);
                                            break;
                                        }
                                    }
                                    else //上传SAP失败
                                    {
                                        flag = "N";
                                        SetIsEnable(true);
                                        ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:SAP返回数据丢失", "ABNORMAL", lstView);
                                        break;
                                    }

                                }
                            }
                            //上传成功
                            if (flag.Equals("Y"))
                            {
                                if (ProductStorageDAL.updatepackingdata(carton, wo, joinid, posttime))
                                {
                                    //if(!_ListCartonSucess.Contains(carton))
                                    //_ListCartonSucess.Add(carton);

                                    SetIsEnable(true);
                                    ToolsClass.Log("托号：" + carton + " 上传SAP保存成功", "NORMAL", lstView);

                                    try
                                    {
                                        #region 移除上传成功的托号
                                        foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                                        {
                                            if (dgr.Cells[2].Value != null && dgr.Cells[2].Value.Equals(carton))
                                            {
                                                this.dataGridView1.Rows.RemoveAt(dgr.Index);
                                                break;
                                            }
                                        }

                                        DataTable SN = ProductStorageDAL.GetSNInfoByCarton(carton);
                                        if (SN != null && SN.Rows.Count > 0)
                                        {
                                            foreach (DataRow newsn in SN.Rows)
                                            {
                                                //成功的托号赋值给一个新的List,做Log记录
                                                for (int p = 0; p < _ListSapWo.Count; p++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListSapWo[p].ModuleSN)
                                                    {
                                                        _ListSapWoLog.Add(_ListSapWo[p]);
                                                    }
                                                }

                                                for (int q = 0; q < _ListPackingSNCellInfo.Count; q++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListPackingSNCellInfo[q].SN)
                                                    {
                                                        _ListPackingSNCellInfo.RemoveAt(q);
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                else
                                {
                                    SetIsEnable(true);
                                    ToolsClass.Log("托号：" + carton + "上传SAP保存失败(更新包装数据库错误)", "NORMAL", lstView);
                                }
                            }
                        }
                        #endregion

                        //#region 写入库交易记录

                        //if (_ListCartonSucess.Count > 0)
                        //{
                        //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "Invertory");                       
                        //}
                        //#endregion
                    }
                    else
                    {
                        SetIsEnable(true);
                        ToolsClass.Log("上传SAP保存,返回数据错误", "ABNORMAL", lstView);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("处理SAP返回数据时发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                    return false;
                }
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("上传SAP时候发生异常(处理SAP返回数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        private bool SavePreUploadData(List<SapWoModule> listWo, List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            try
            {
                #region

                var cars = string.Join(",", cartonList.ToArray());
                ToolsClass.Log("内部托号：" + cars + "入库数据保存");

                try
                {
                    #region
                    string joinid = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                    string posttime = DT.DateTime().LongDateTime;
                    foreach (string carton in cartonList)
                    {
                        var listCartonSapWo = listWo.FindAll(p => p.CartonNo == carton);
                        if (listCartonSapWo.Count <= 0)
                        {
                            Save.Enabled = true;
                            button1.Enabled = true;
                            ToolsClass.Log("数据出错!", "ABNORMAL", lstView);
                            return false;
                        }
                        var listTsapReceiptUploadModules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton);
                        if (listTsapReceiptUploadModules.Count <= 0)
                        {
                            Save.Enabled = true;
                            button1.Enabled = true;
                            ToolsClass.Log("数据出错!", "ABNORMAL", lstView);
                            return false;
                        }
                        if (!ProductStorageDAL.updatepackingdatabySn(listTsapReceiptUploadModules, listCartonSapWo, joinid, posttime))
                        {
                            Save.Enabled = true;
                            button1.Enabled = true;
                            ToolsClass.Log("托号：" + carton + "待上传SAP数据保存失败(更新包装数据库错误)", "NORMAL", lstView);
                            return false;
                        }
                        this.Save.Enabled = true;
                        this.button1.Enabled = true;
                        ToolsClass.Log("托号：" + carton + "待上传SAP数据保存成功", "NORMAL", lstView);

                        #region 移除上传成功的托号

                        foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                        {
                            if (dgr.Cells[2].Value == null || !dgr.Cells[2].Value.Equals(carton))
                                continue;
                            dataGridView1.Rows.RemoveAt(dgr.Index);
                            break;
                        }

                        #endregion
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("保存待上传SAP数据时发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                    return false;
                }
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("上传SAP时候发生异常(处理SAP返回数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        private void ddlWostatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlWostatus.SelectedIndex == 0)
                WoStatus = "";
            else if (this.ddlWostatus.SelectedIndex == 1)
                WoStatus = "1";
            else if (this.ddlWostatus.SelectedIndex == 1)
                WoStatus = "2";
        }

        private void ddlByIm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlByIm.SelectedIndex == 0)
                ByIm_flag = "";
            else if (this.ddlByIm.SelectedIndex == 1)
                ByIm_flag = "Y";
            else if (this.ddlByIm.SelectedIndex == 2)
                ByIm_flag = "N";
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            if (_ListSapWo.Count > 0)
                _ListSapWo.Clear();

            if (_ListPackingSNCellInfo.Count > 0)
                _ListPackingSNCellInfo.Clear();

            if (cartonList.Count > 0)
                cartonList.Clear();

            this.txtCarton.Clear();
            this.txtCarton.Focus();
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

        }
        private void txtGlassCode_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }
        private void txtEVACode_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }
        private void txtTPTCode_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }
        private void txtConBoxCod_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }
        private void txtAIFrameCode_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }
        private void txtCell_KeyDown(object sender, KeyEventArgs e)
        {
            return;
        }

        private void ddlCancelStorageFlag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlCancelStorageFlag.SelectedIndex == 0)
                _CancelStorageFlag = "";
            else if (this.ddlCancelStorageFlag.SelectedIndex == 1)
                _CancelStorageFlag = "1";
            else if (this.ddlCancelStorageFlag.SelectedIndex == 2)
                _CancelStorageFlag = "2";
        }

        private void ddlIsOnlyPacking_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlIsOnlyPacking.SelectedIndex == 0)
                _IsOnlyPacking = "";
            else if (this.ddlIsOnlyPacking.SelectedIndex == 1)
                _IsOnlyPacking = "2";
            else if (this.ddlIsOnlyPacking.SelectedIndex == 2)
                _IsOnlyPacking = "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        #region
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.txtWO1.Enabled = this.checkBox2.Checked;
            this.pictureBox1.Enabled = this.checkBox2.Checked;
            this.txtWO1.Clear();
            this.txtWO1.Focus();
        }

        private void txtWO1_Leave(object sender, EventArgs e)
        {
            //判断是否有外协工单
            if (!this.txtWO1.Text.Trim().ToString().Equals(""))
            {
                DataTable dt = ProductStorageDAL.GetWoMasterIno(this.txtWO1.Text.Trim());
                if (dt == null || dt.Rows.Count == 0)
                {
                    if (this.checkBox2.Checked)
                    {
                        ToolsClass.Log("输入的外协工单：" + this.txtWO1.Text.Trim() + " 没有从SAP下载", "ABNORMAL", lstView);
                        this.txtWO1.Clear();
                        this.txtWO1.Focus();
                        return;
                    }
                }
                else
                {
                    DataRow row = dt.Rows[0];
                    this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
                    this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.txtlocation.Text = Convert.ToString(row["RESV04"]);
                    this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
                    this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
                    this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
                }
            }
            else
            {
                if (this.checkBox2.Checked)
                {
                    ToolsClass.Log("请输入外协工单号码", "ABNORMAL", lstView);
                    this.txtWO1.Focus();
                }
            }
        }

        private void txtWO1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                txtWO1_Leave(null, null);
        }

        private void txtWO1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                pictureBox1_Click(null, null);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), "");
            frm.ShowDialog();
            this.txtWO1.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
        }
        #endregion



    }
}
