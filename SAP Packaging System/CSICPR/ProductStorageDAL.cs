﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;


namespace CSICPR
{
    /// <summary>
    /// SAP入库业务逻辑层
    /// </summary>

    public class ProductStorageDAL
    {


        #region 私有方法
        /// <summary>
        /// 从SAP读取数据方法
        /// </summary>
        /// <returns></returns>
        /// 
//        public static SqlDataReader GetMySqlDataTable(string mysqlstring, string connectstring = "")
//        {
//        	
//			this.Initialise();
//	        string connection = "Persist Security Info=False;server= 10.127.33.38;database=cssi;uid=jli;password=2ed@MyQ2";
//	        	
//	        if (this.OpenConnection() == true)
//	        {
//	            MySqlCommand cmd = new MySqlCommand(mysql, connection);
//	            MySqlDataAdapter returnVal = new MySqlDataAdapter(mysql,connection);
//	            DataTable dt = new DataTable();
//	            returnVal.Fill(dt);
//	            this.CloseConnection();
//	            return dt;
//	        }
//	        else
//	        {
//	            this.CloseConnection();
//	            DataTable dt = new DataTable();
//	            return dt;            
//	        }
//			
//        }
        private static DataTable GetDataTableData(string SqlConnectString, string sql)
        {
            try
            {
                DataTable dt = null;
                DataSet ds = SqlHelper.ExecuteDataset(SqlConnectString, CommandType.Text, sql);
          
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
                else
                    return dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        #endregion
		  
        /// <summary>
        /// 格式化转换，小数点后保留两位，多余两位四舍五入，少于两位补0
        /// </summary>
        /// <param name="oriValue"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        public static string GetDecimalString(string oriValue, int decimals)
        {
            decimal dec = Math.Round(decimal.Parse(oriValue), decimals, MidpointRounding.AwayFromZero);
            if (decimals > 1)
                return String.Format("{0:F}", dec);
            return String.Format("{0:0.0}", dec);
        }
        /// <summary>
        /// 查看组件是否是待入库的状态,返回1表示待入库，返回0表示已入库或有异常
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetSNStatus(string sn)
        {
            try
            {
                string sql = @"SELECT COUNT(distinct SN) AS SNQTY
                               FROM  Product prd WITH(nolock),
                                     Box bx with(nolock)
                               WHERE prd.SN = '{0}'
                                     AND prd.BoxID = bx.BoxID
                                     AND prd.Flag='1' AND bx.Flag='1'";
                sql = string.Format(sql, sn.Trim());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null)
                {
                    DataRow row = dt.Rows[0];
                    return (int)row["SNQTY"];
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }
        /// <summary>
        /// 包装系统入库上传每托组件的数量
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetSNStroageQty(string CartonNo)
        {
            try
            {
                string sql = @"SELECT COUNT(distinct SN) AS SNQTY
                               FROM  Product prd WITH(nolock),
                                     Box bx with(nolock)
                               WHERE prd.BoxID = '{0}' AND bx.BoxID ='{0}'
                                     AND prd.BoxID = bx.BoxID
                                     AND prd.Flag='1' AND bx.Flag='1'";
                sql = string.Format(sql, CartonNo.Trim());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null)
                {
                    DataRow row = dt.Rows[0];
                    return (int)row["SNQTY"];
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }
        /// <summary>
        /// 以组件查询此组件是否维护过物料信息,返回1表示维护过，0表示没有维护过
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetSNStatusFromConfig(string SN)
        {
            try
            {
                string sql = @"SELECT COUNT(distinct SN) AS SNQTY
                               FROM {0}.[dbo].[Product] prd WITH(nolock) inner join {0}.[dbo].Box bx with(nolock) 
                                     on prd.BoxID = bx.BoxID  inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) 
                                     on  prd.SN = SN.MODULE_SN inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) 
                                     on SN.PACK_MA_SYSID = template.SYSID
                                WHERE prd.sn = '{2}' AND prd.Flag='1' AND bx.Flag='1'";
                sql = string.Format(sql, FormCover.PackingDBName, FormCover.CenterDBName, SN.Trim());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null)
                {
                    DataRow row = dt.Rows[0];
                    return (int)row["SNQTY"];
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }
        /// <summary>
        /// 检查组件是否有维护物料信息
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetSNStroageInfoFromConfig(string SerialNumber)
        {
            try
            {
                string sql = @"SELECT COUNT(distinct SN) AS SNQTY
                                FROM {0}.[dbo].[Product] prd WITH(nolock)  inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) on  prd.SN = SN.MODULE_SN inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) on SN.PACK_MA_SYSID = template.SYSID inner join
                                     {2}.[dbo].[T_WORK_ORDERS] wo with(nolock) on template.OrderNo = wo.TWO_NO
                                WHERE prd.SN = '{3}'  AND prd.Flag='1'";
                sql = string.Format(sql, FormCover.PackingDBName, FormCover.CenterDBName, FormCover.SAPDBName, SerialNumber.Trim());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null)
                {
                    DataRow row = dt.Rows[0];
                    return (int)row["SNQTY"];
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }
        /// <summary>
        /// 每个托里多少个组件已经维护信息了
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetSNStroageQtyFromConfig(string CartonNo)
        {
            try
            {
                string sql = @"SELECT COUNT(distinct SN) AS SNQTY
                               FROM {0}.[dbo].[Product] prd WITH(nolock) inner join {0}.[dbo].Box bx with(nolock) 
                                     on prd.BoxID = bx.BoxID  inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) 
                                     on  prd.SN = SN.MODULE_SN inner join
                                     {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) 
                                     on SN.PACK_MA_SYSID = template.SYSID inner join
                                     {2}.[dbo].[T_WORK_ORDERS] wo with(nolock) on template.OrderNo = wo.TWO_NO
                                WHERE prd.BoxID = '{3}' AND bx.BoxID ='{3}' AND prd.Flag='1' AND bx.Flag='1'";
                sql = string.Format(sql, FormCover.PackingDBName, FormCover.CenterDBName, FormCover.SAPDBName, CartonNo.Trim());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null)
                {
                    DataRow row = dt.Rows[0];
                    return (int)row["SNQTY"];
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }
        /// <summary>
        /// 入库上传托的数量
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static int GetUploadStorageQty(string ControlName, string Workshop)
        {
            try
            {
                string sql = @"SELECT * FROM T_SYS_MAPPING WITH(NOLOCK) Where  FUNCTION_CODE='{0}' and MAPPING_KEY_01 = '{1}'";
                sql = string.Format(sql, ControlName.Trim(), Workshop.Trim().ToUpper());
                DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    DataRow row = dt.Rows[0];
                    if (!Convert.ToString(row["MAPPING_KEY_02"]).Trim().Equals(""))
                        return Convert.ToInt32((row["MAPPING_KEY_02"]));
                    else
                        return 0;
                }
                else
                {
                    MessageBox.Show("没有获取入库上传的最大托数:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("从接口数据库获取入库上传的最大托数发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return 0;
            }
        }

        /// <summary>
        /// 通过托号获取入库信息
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static DataTable GetSAPCartonStorageInfo(string CartonNo)
        {
//            string sql = @"SELECT distinct prd.SN AS '组件序列号',prd.BoxID AS '内部托号',prd.Cust_BoxID AS '客户托号',bx.PackDate AS '包装日期',prd.Pmax AS '实测功率',prd.StdPower AS '标称功率',prd.ModuleClass AS '组件等级',
//                           orders.TWO_NO AS '生产订单号',orders.TWO_WORKSHOP AS '车间',orders.TWO_PRODUCT_NAME AS '产品物料代码',orders.RESV04 AS '入库地点',orders.RESV06 AS '工厂',orders.TWO_ORDER_NO AS '销售订单',orders.TWO_ORDER_ITEM AS '销售订单项目',
//                           template.ByIm AS '电流分档',template.CellBatch AS '电池片批次',template.CellCode AS '电池片物料代码',template.CellEff AS '电池片转换效率',template.CellPrintMode AS '电池片网版',
//                           template.ConBoxBatch AS '接线盒批次',template.ConBoxCode AS '接线盒物料代码',template.EvaBatch AS 'EVA 批次号',template.EvaCode AS 'EVA 物料代码',template.GlassBatch AS '玻璃批次',
//                           template.GlassCode AS '玻璃物料代码',template.GlassThickness AS '玻璃厚度',template.LongFrameBatch AS '长边框批次',template.LongFrameCode AS '长边框物料代码',
//                           template.PackingMode AS '包装方式',template.ShortFrameBatch AS '短边框批次',template.ShortFrameCode AS '短边框物料代码',
//                           template.TptBatch AS 'TPT 批次',template.TptCode AS 'TPT物料代码',ISNULL(c.ItemValue,'') as '组件颜色',ISNULL(template.Tolerance,'') as '公差',CASE ISNULL(orders.RESV05,'') WHEN 'ZCRO' THEN 'Y' ELSE 'N' END as '是否重工'
//                                ,bx.BoxType StdPowerLevel, template.Resv01 AS 'Market', template.Resv02 AS 'LID'
//                           FROM {0}.[dbo].[Box] bx with(nolock) inner join 
//								{0}.[dbo].[Product] prd with(nolock) on bx.BoxID = prd.BoxID left join
//	                            {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) on prd.SN = sn.MODULE_SN left join
//                                {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) on SN.PACK_MA_SYSID = template.SYSID left join
//                                {2}.[dbo].[T_WORK_ORDERS] orders with(nolock) on orders.TWO_NO = template.OrderNo
//                                
//                                --组件颜色
//                                left join {0}.[dbo].[ElecParaTest] a with(nolock) on prd.SN = a.FNumber 
//                                left join {0}.[dbo].[Scheme] b with(nolock) on a.SchemeInterID = b.InterID 
//                                left join {0}.[dbo].[Item] c with(nolock) on b.ItemColorID = c.InterID 
//                                
//                          WHERE bx.BoxID='{3}' 
//                          ORDER BY orders.TWO_NO desc,prd.SN desc ";

//Jacky20160217
string sql = @"SELECT distinct prd.SN AS '组件序列号',prd.BoxID AS '内部托号',prd.Cust_BoxID AS '客户托号',bx.PackDate AS '包装日期',prd.Pmax AS '实测功率',prd.StdPower AS '标称功率',prd.ModuleClass AS '组件等级',
                           orders.TWO_NO AS '生产订单号',orders.TWO_WORKSHOP AS '车间',orders.TWO_PRODUCT_NAME AS '产品物料代码',orders.RESV04 AS '入库地点',orders.RESV06 AS '工厂',orders.TWO_ORDER_NO AS '销售订单',orders.TWO_ORDER_ITEM AS '销售订单项目',
                           template.ByIm AS '电流分档',template.CellBatch AS '电池片批次',template.CellCode AS '电池片物料代码',template.CellEff AS '电池片转换效率',template.CellPrintMode AS '电池片网版',
                           template.ConBoxBatch AS '接线盒批次',template.ConBoxCode AS '接线盒物料代码',template.EvaBatch AS 'EVA 批次号',template.EvaCode AS 'EVA 物料代码',template.GlassBatch AS '玻璃批次',
                           template.GlassCode AS '玻璃物料代码',template.GlassThickness AS '玻璃厚度',template.LongFrameBatch AS '长边框批次',template.LongFrameCode AS '长边框物料代码',
                           template.PackingMode AS '包装方式',template.ShortFrameBatch AS '短边框批次',template.ShortFrameCode AS '短边框物料代码',
                           template.TptBatch AS 'TPT 批次',template.TptCode AS 'TPT物料代码',ISNULL(c.ItemValue,'') as '组件颜色',ISNULL(template.Tolerance,'') as '公差',CASE ISNULL(orders.RESV05,'') WHEN 'ZCRO' THEN 'Y' ELSE 'N' END as '是否重工'
                                ,bx.BoxType StdPowerLevel, template.Resv01 AS 'Market', template.Resv02 AS 'LID', template.Resv03 AS 'CabelLength', template.Resv04 AS 'JBoxType'
                           FROM {0}.[dbo].[Box] bx with(nolock) inner join 
								{0}.[dbo].[Product] prd with(nolock) on bx.BoxID = prd.BoxID left join
	                            {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) on prd.SN = sn.MODULE_SN left join
                                {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) on SN.PACK_MA_SYSID = template.SYSID left join
                                {2}.[dbo].[T_WORK_ORDERS] orders with(nolock) on orders.TWO_NO = template.OrderNo
                                
                                --组件颜色
                                left join {0}.[dbo].[ElecParaTest] a with(nolock) on prd.SN = a.FNumber 
                                left join {0}.[dbo].[Scheme] b with(nolock) on a.SchemeInterID = b.InterID 
                                left join {0}.[dbo].[Item] c with(nolock) on b.ItemColorID = c.InterID 
                                
                          WHERE bx.BoxID='{3}' 
                          ORDER BY orders.TWO_NO desc,prd.SN desc ";
            sql = string.Format(sql, FormCover.PackingDBName, FormCover.CenterDBName, FormCover.SAPDBName, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }
        /// <summary>
        /// 从集中数据库获取托号的车间
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static string GetCartonFromCenterDB(string CartonNo)
        {
            try
            {
                string sql = @"Select * FROM [T_MODULE_CARTON] with(nolock) where RESV03 = '{0}'";
                sql = string.Format(sql, CartonNo);
                DataTable dt = GetDataTableData(FormCover.CenterDBConnString, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    DataRow row = dt.Rows[0];
                    return Convert.ToString(row["RESV04"]).Trim().ToUpper();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        /// <summary>
        /// 从包装数据库获取组件信息
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static DataTable GetSNInfoFromPacking(string CartonNo)
        {
            string sql = @"SELECT * FROM [Product] with(nolock) where BoxID='{0}' ";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }
        /// <summary>
        /// 从集中数据库获取入库信息
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static DataTable GetSNInfoFromInterface(string serialnumber)
        {
            string sql = @"SELECT * FROM [T_MODULE_REWORK] with(nolock) where MODULE_SN='{0}' ";
            sql = string.Format(sql, serialnumber);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }

        /// <summary>
        /// 通过组件号获取入库信息
        /// </summary>
        /// <param name="SN"></param>
        /// <returns></returns>
        public static DataTable GetSAPSNStorageInfo(string SN)
        {
            string sql = @"SELECT prd.SN AS '组件序列号',prd.BoxID AS '内部托号',prd.Cust_BoxID AS '客户托号',bx.PackDate AS '包装日期',prd.Pmax AS '实测功率',prd.StdPower AS '标称功率',prd.ModuleClass AS '组件等级',
                           orders.TWO_NO AS '生产订单号',orders.TWO_WORKSHOP AS '车间',orders.TWO_PRODUCT_NAME AS '产品物料代码',orders.RESV04 AS '入库地点',orders.RESV06 AS '工厂',orders.TWO_ORDER_NO AS '销售订单',orders.TWO_ORDER_ITEM AS '销售订单项目',
                           template.ByIm AS '电流分档',template.CellBatch AS '电池片批次',template.CellCode AS '电池片物料代码',template.CellEff AS '电池片转换效率',template.CellPrintMode AS '电池片网版',
                           template.ConBoxBatch AS '接线盒批次',template.ConBoxCode AS '接线盒物料代码',template.EvaBatch AS 'EVA 批次号',template.EvaCode AS 'EVA 物料代码',template.GlassBatch AS '玻璃批次',
                           template.GlassCode AS '玻璃物料代码',template.GlassThickness AS '玻璃厚度',template.LongFrameBatch AS '长边框批次',template.LongFrameCode AS '长边框物料代码',
                           template.PackingMode AS '包装方式',template.ShortFrameBatch AS '短边框批次',template.ShortFrameCode AS '短边框物料代码',
                           template.TptBatch AS 'TPT 批次',template.TptCode AS 'TPT物料代码',ISNULL(c.ItemValue,'') as '组件颜色',ISNULL(d.VALUE18,'') as '公差',CASE ISNULL(orders.RESV05,'') WHEN 'ZP11' THEN 'Y' ELSE 'N' END as '是否重工'
                                ,bx.BoxType StdPowerLevel
                           FROM {0}.[dbo].[Box] bx with(nolock) inner join 
                                {0}.[dbo].[Product] prd with(nolock) on bx.BoxID = prd.BoxID left join
                                {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SN WITH(NOLOCK) on prd.SN = sn.MODULE_SN left join
                                {1}.[dbo].[T_PACK_MATERIAL_TEMPLATE] template with(nolock) on SN.PACK_MA_SYSID = template.SYSID left join
                                {2}.[dbo].[T_WORK_ORDERS] orders with(nolock) on orders.TWO_NO = template.OrderNo  
                                
                                --组件颜色
                                left join {0}.[dbo].[ElecParaTest] a with(nolock) on prd.SN = a.FNumber 
                                left join {0}.[dbo].[Scheme] b with(nolock) on a.SchemeInterID = b.InterID 
                                left join {0}.[dbo].[Item] c with(nolock) on b.ItemColorID = c.InterID 
                                
                                --公差
                                left join {2}.[dbo].[T_WORKORDER_ATTRIBUTE] d with(nolock) on orders.TWO_NO = d.ORDER_NO  
                          WHERE prd.SN='{3}' ORDER BY orders.TWO_NO desc,prd.SN desc ";
            sql = string.Format(sql, FormCover.PackingDBName, FormCover.CenterDBName, FormCover.SAPDBName, SN);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 获取打包的托号信息(入库)
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static DataTable GetCartonInfo(string CartonNo)
        {
            string sql = @"SELECT prd.SN,prd.flag,prd.BoxID,prd.Pmax,prd.ModuleClass,prd.ProcessDateTime,prd.stdPower,box.Cust_BoxID,box.BoxType StdPowerLevel
                           FROM [Box] box with(nolock) inner join  product prd with(nolock)
                               on  box.BoxID = prd.BoxID and box.BoxID ='{0}' and box.Flag in( '1','5') order by SN desc ";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 查看此组件是否在集中数据库T_MODEL表存在
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static DataTable GetSnInfoFromCenterDB(string sn)
        {
            string sql = @"SELECT *
                           FROM [T_MODULE] with(nolock) where MODULE_SN = '{0}' ";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }
        /// <summary>
        /// 查看此组件是否在集中数据库T_MODEL_test表存在
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public static DataTable GetSnInfoFromCenterDBTest(string sn)
        {
            string sql = @"SELECT *
                           FROM [T_MODULE_TEST] with(nolock) where MODULE_SN = '{0}' ";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }
        /// <summary>
        /// 获取打包的托号信息(撤销入库)
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DataTable GetCartonInfo(string CartonNo, string type)
        {
            string sql = @"SELECT prd.SN,prd.flag,prd.BoxID,prd.Pmax,prd.ModuleClass,prd.ProcessDateTime,prd.stdPower,box.Cust_BoxID,prd.WorkOrder,prd.ReWorkOrder
                           FROM [Box] box with(nolock) inner join  product prd with(nolock)
                               on  box.BoxID = prd.BoxID and box.BoxID ='{0}' and box.Flag = '2'";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static DataTable GetCartonStatus(string CartonNo)
        {
            string sql = @"SELECT [IsUsed],[BoxID]
                           FROM [Box] box with(nolock) where BoxID ='{0}'";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 本车间的组件返回数据，不是本车间的组件返回
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public static DataTable GetSNReworkInfoFromPacking(string sn)
        {
            string sql = @"SELECT *
                           FROM [Product] with(nolock) where SN ='{0}'";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 获取是否本车间的拆托
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public static DataTable GetLocalFlag(string carton)
        {
            string sql = @"SELECT distinct LocalFlag
                           FROM [Product] with(nolock) where boxid ='{0}'";
            sql = string.Format(sql, carton);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 查找组件信息
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public static DataTable GetSNInfoBySn(string sn)
        {
            string sql = ToolsClass.getSQL("tbHandPackingSQL");
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        /// <summary>
        /// 查找组件信息,检查是否为重工
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public static DataTable GetSNInfoByReworkFromCenterDB(string sn)
        {
            string sql = @"SELECT *
                       FROM [T_MODULE_REWORK] with(nolock) where MODULE_SN='{0}' and FLAG='0' and RESV1='" + FormCover.CurrentFactory.Trim().ToUpper() + "'";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }


        public static DataTable SNByReworkFromCenterDB(string sn)
        {
            string sql = @"SELECT *
                       FROM [T_MODULE_REWORK] with(nolock) where MODULE_SN='{0}' and WORKSHOP='" + FormCover.CurrentFactory.Trim().ToUpper() + "'";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }


        public static DataTable GetSNInfoByFromPackingDB(string sn)
        {
            string sql = @" SELECT *
                       FROM [Product] with(nolock) where SN='{0}' and FLAG = '6' and LocalFlag='1'";
            sql = string.Format(sql, sn);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static string GetSNImByFromPackingDB(string sn)
        {
            string sql = @"SELECT TOP 1 [Im]
  FROM [ElecParaTest] WITH(NOLOCK)
  WHERE FNumber='{0}' 
  ORDER BY InterID DESC ";
            sql = string.Format(sql, sn);
            var dt = GetDataTableData(FormCover.connectionBase, sql);
            if (dt == null || dt.Rows == null || dt.Rows.Count <= 0)
                return string.Empty;

            return dt.Rows[0][0].ToString();
        }

        public static DataTable GetCancelCartonInfo(string CartonNo)
        {
            string sql = @"SELECT prd.SN,prd.flag,prd.BoxID,prd.Pmax,prd.ModuleClass,prd.ProcessDateTime,prd.stdPower
                           FROM [Box] box with(nolock) inner join  product prd with(nolock)
                               on  box.BoxID = prd.BoxID and box.BoxID ='{0}' and box.Flag  in ('0','4','6','2','3')";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static DataTable GetDissemblyCartonInfo(string CartonNo)
        {
            string sql = @"SELECT distinct prd.LocalFlag
                           FROM [Box] box with(nolock) inner join  product prd with(nolock) on  box.BoxID = prd.BoxID and box.BoxID ='{0}'";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static DataTable GetSNInfoByCarton(string CartonNo)
        {
            string sql = @"SELECT prd.SN
                           FROM [Box] box with(nolock) inner join  product prd with(nolock)
                               on  box.BoxID = prd.BoxID and box.BoxID ='{0}'";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        #region 工单
        /// <summary>
        /// 从SAP获取工单单头信息
        /// </summary>
        /// <param name="Wo"></param>
        /// <returns></returns>
        /// 

        public static DataTable GetWoMasterIno(string QueryType, string WoMaster, string Wotype, DateTime woplanstartfrom, DateTime woplanstartto)
        {
            if (!WoMaster.Equals(""))
                WoMaster = WoMaster + "%";
            if (!Wotype.Equals(""))
            {
                if (Wotype.Trim().Equals("ZP07"))
                {
                    Wotype = "'ZP07','ZP13'";
                }
                else if (Wotype.Trim().Equals("ZP11"))
                { }
                else if (Wotype.Trim().Equals("ZP02"))
                {
                    Wotype = "'ZP02','ZP13'";
                }
                else
                {
                    Wotype = "'" + Wotype + "'";
                }
            }
            woplanstartto = woplanstartto.AddDays(1);
            string sql = @"SELECT [TWO_NO]  
                                      ,[TWO_WORKSHOP]
                                      ,[TWO_PRODUCT_NAME]
                                      ,[RESV04]
                                      ,[RESV06]
                                      ,[TWO_ORDER_NO]
                                      ,[RESV05]  
                              FROM [T_WORK_ORDERS] with(nolock)";
            if (QueryType.Equals("Wo"))
            {
                if (!Wotype.Equals(""))
                {
                    if (!Wotype.Trim().Equals("ZP11"))
                        sql += "where TWO_NO like '{0}' and RESV05 in (" + Wotype + ")";

                    else
                    {
                        if (FormCover.CurrentFactory.Trim().Equals("M01") || FormCover.CurrentFactory.Trim().Equals("M09"))
                            sql += "where TWO_NO like '{0}' and RESV05 = '" + Wotype + "' AND TWO_WORKSHOP in('M01','M09')";
                        else if (FormCover.CurrentFactory.Trim().Equals("M07") || FormCover.CurrentFactory.Trim().Equals("M13"))
                            sql += "where TWO_NO like '{0}' and RESV05 = '" + Wotype + "' AND TWO_WORKSHOP in('M07','M13')";
                        else
                            sql += "where TWO_NO like '{0}' and RESV05 = '" + Wotype + "' AND TWO_WORKSHOP='" + FormCover.CurrentFactory.Trim() + "'";
                    }
                }
                else
                {
                    sql += "where TWO_NO like '{0}'";
                }
            }
            else
                if(!Wotype.Equals(""))
                sql += "where RESV05 in (" + Wotype + ") AND [TWO_PLANSTART_DATETIME] >= '" + woplanstartfrom.ToString("yyyy-MM-dd") + "' and TWO_PLANSTART_DATETIME <'" + woplanstartto.ToString("yyyy-MM-dd") + "'";
                else
                sql += " where TWO_PLANSTART_DATETIME >= '" + woplanstartfrom.ToString("yyyy-MM-dd") + "' and TWO_PLANSTART_DATETIME <'" + woplanstartto.ToString("yyyy-MM-dd") + "'";
            sql = string.Format(sql, WoMaster);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }





        public static DataTable GetReworkWoCartonIno(string wo)
        {
            //处理工单时使用
            //string workshop = "";
            //if (FormCover.CurrentFactory.Trim().Equals("M09"))
            //    workshop = "M01";
            //else if (FormCover.CurrentFactory.Trim().Equals("M13"))
            //    workshop = "M07";
            //else
            //    workshop = FormCover.CurrentFactory.Trim();

//            string sql = @"SELECT distinct packing.PostCode,packing.OrderNo,packing.CartonNo,packing.PickingFrom,packing.PickingTo,packing.PickingType,packing.ReworkFlag,carton.Resv04
//                            FROM {0}.[dbo].[T_PICKING]  packing with(nolock),
//                                 {0}.[dbo].[T_WORK_ORDERS] orderno with(nolock),
//                                 {1}.[dbo].[T_MODULE_CARTON] carton with(nolock)
//                            WHERE packing.PickingType in('261')
//                                  and packing.OrderNo = orderno.TWO_NO
//                                  and orderno.TWO_NO = '{2}'
//                                  and packing.ReworkFlag ='0'
//                                  and orderno.TWO_WORKSHOP = '{3}'
//                                  and packing.CartonNo= carton.Resv03 COLLATE database_default
//                                  order by packing.OrderNo,packing.PostCode,packing.PickingFrom";

            string sql = @"SELECT distinct packing.PostCode,packing.OrderNo,packing.CartonNo,packing.PickingFrom,packing.PickingTo,packing.PickingType,packing.ReworkFlag,orderno.TWO_WORKSHOP as workshop
                            FROM {0}.[dbo].[T_PICKING]  packing with(nolock),
                                 {0}.[dbo].[T_WORK_ORDERS] orderno with(nolock),
                                 {1}.[dbo].[T_MODULE_CARTON] carton with(nolock)
                            WHERE packing.PickingType in('261')
                                  and packing.OrderNo = orderno.TWO_NO
                                  and orderno.TWO_NO = '{2}'
                                  and packing.ReworkFlag ='0'
                                  and packing.CartonNo= carton.Resv03 COLLATE database_default
                                  order by packing.OrderNo,packing.PostCode,packing.PickingFrom";

            sql = string.Format(sql, FormCover.SAPDBName, FormCover.CenterDBName, wo);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        /// <summary>
        /// 查询重工工单信息
        /// </summary>
        /// <param name="wo"></param>
        /// <returns></returns>
        public static DataTable GetReworkWoIno(string wo)
        {
            if (wo.Equals(""))
                return null;
            else
                wo = wo + "%";

//            string sql = @"SELECT DISTINCT packing.OrderNo,orderno.TWO_NO,orderno.TWO_ORDER_NO,orderno.TWO_PRODUCT_NAME
//                                FROM [T_PICKING]  packing with(nolock),
//                                     [T_SYS_MAPPING] mapping with(nolock),
//                                     [T_WORK_ORDERS] orderno with(nolock)
//                                WHERE mapping.FUNCTION_CODE = 'SAP-MES-PICKING-TO' 
//                                      and mapping.MAPPING_KEY_02='{0}' 
//                                      and packing.PickingTo = mapping.MAPPING_KEY_01
//                                      and packing.PickingType in('261','262')
//                                      and packing.ReworkFlag='0'
//                                      and packing.OrderNo = orderno.TWO_NO
//                                      and orderno.TWO_NO like '{1}'
//                                order by packing.OrderNo";
            string sql = @"SELECT DISTINCT packing.OrderNo,orderno.TWO_NO,orderno.TWO_ORDER_NO,orderno.TWO_PRODUCT_NAME
                                FROM [T_PICKING]  packing with(nolock),
                                     [T_WORK_ORDERS] orderno with(nolock)
                                WHERE  packing.OrderNo = orderno.TWO_NO
                                       and packing.PickingType in('261','262')
                                      and packing.ReworkFlag='0'
                                      and orderno.TWO_NO like '{0}'
                                order by packing.OrderNo";

            sql = string.Format(sql, wo);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        /// <summary>
        /// 查询重工工单信息
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public static DataTable GetReworkWorkOrder(string orderNo,string fac)
        {
            if (orderNo.Equals(""))
                return null;
            orderNo = orderNo + "%";

            # region not use 
//            string sql = @"SELECT DISTINCT orderno.TWO_NO,orderno.TWO_ORDER_NO,orderno.TWO_PRODUCT_NAME
//                           FROM [T_WORK_ORDERS] orderno with(nolock)
//                           WHERE orderno.TWO_WORKSHOP = '{0}' 
//                                and orderno.TWO_NO like '{1}'
//                                AND orderno.RESV05 = 'ZP11'
//                           order by orderno.TWO_NO ";
//            sql = string.Format(sql, FormCover.CurrentFactory, orderNo);
            # endregion

//            string sql = @"SELECT DISTINCT orderno.TWO_NO,orderno.TWO_ORDER_NO,orderno.TWO_PRODUCT_NAME
//                           FROM [T_WORK_ORDERS] orderno with(nolock)
//                           WHERE orderno.TWO_WORKSHOP = '{0}' 
//                                and orderno.TWO_NO like '{1}'
//                                AND orderno.RESV05 = '{2}'
//                           order by orderno.TWO_NO ";

            string sql = @"SELECT DISTINCT orderno.TWO_NO,orderno.TWO_ORDER_NO,orderno.TWO_PRODUCT_NAME
                           FROM [T_WORK_ORDERS] orderno with(nolock)
                           WHERE  orderno.TWO_NO like '{0}'
                           AND orderno.RESV05 = '{1}'
                           order by orderno.TWO_NO ";

            //工单类型
            string ZPtype = "";
            switch (fac)
            {
                case "CS":
                    ZPtype = "ZP11";
                    break;
                case "VNSM":
                    ZPtype = "ZPA9";
                    break;
                case "CAMO":
                    ZPtype = "ZCRO";
                    break;
                default:
                    ZPtype = "ZP11";
                    break;
            }

            sql = string.Format(sql, orderNo, ZPtype);

            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }
		//Jacky
        public static int GetQtyLeftInTemplate(string templatename)
        {
        	string PO = "";
        	string getPO = @"select orderno from T_PACK_MATERIAL_TEMPLATE where MaterialTemplateName = '{0}'";
        	getPO = string.Format(getPO,templatename);
        	SqlDataReader rdpo = ToolsClass.GetDataReader(getPO,FormCover.CenterDBConnString);
        	if (rdpo!=null && rdpo.Read())
        		PO = rdpo.GetString(0);

        	 #region not use
//        	 if(templatename.Length < 10)
//        		PO = templatename.Substring(0,9);
//        	 else
//        		PO = templatename.Substring(0,10);
//        	 
//        	string sql = @"Select [TWO_WO_QTY] from [csimes_SapInterface].[dbo].[T_WORK_ORDERS] where TWO_NO = '"+ PO +"'";
//        	SqlDataReader rd = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);
//        	int WoQTY = 0;
//        	if(rd!= null&&rd.Read())
//        	{
//        		WoQTY = Convert.ToInt32(rd.GetValue(0));
//        		rd.Close();
//        	}
//        	sql = @"select count(distinct Module_SN) from T_PACK_MATERIAL_TEMPLATE_MODULE_LK as A inner join T_PACK_MATERIAL_TEMPLATE as B on A.PACK_MA_SYSID = B.SYSID
//  					where B.MaterialTemplateName like '{0}%'";
//        	sql = string.Format(sql,PO);
//        	
//        	rd = ToolsClass.GetDataReader(sql,FormCover.CenterDBConnString);
//        	int UsedQTY = 0;
//        	if(rd!= null&&rd.Read())
//        	{
//        		UsedQTY = Convert.ToInt32(rd.GetValue(0));
//        		
//        	}
//        	if(UsedQTY > WoQTY)
//        		return 0;
//        	return WoQTY-UsedQTY;
			#endregion
			string sql = @"Select [RESV07] as Stock,[RESV08] as Pend from [csimes_SapInterface].[dbo].[T_WORK_ORDERS] where TWO_NO = '"+ PO +"'";
        	SqlDataReader rd = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);
			int Stock = 0, Pend = 0;
        	if(rd!= null&&rd.Read())
        	{
      			Stock = Convert.ToInt32(rd.GetValue(0));
      			Pend  = Convert.ToInt32(rd.GetValue(1));
       			rd.Close();
        	}
        	if (Stock > Pend) return 0;       		
        	
        	return Pend - Stock;        	
        }
        public static DataTable GetWoDataIno(string WoMaster)
        {
            string sql = @"SELECT *
                              FROM [T_WORK_ORDERS] with(nolock)";

            sql += "where TWO_NO = '{0}'";

            sql = string.Format(sql, WoMaster);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }



        public static DataTable GetWoMasterIno(string WoMaster)
        {
            string sql = @"SELECT [TWO_NO]  
                                      ,[TWO_WORKSHOP]
                                      ,[TWO_PRODUCT_NAME]
                                      ,[RESV04]
                                      ,[RESV06]
                                      ,[TWO_ORDER_NO]
                                      ,[RESV05]  
                                      ,[TWO_ORDER_ITEM]
                              FROM [T_WORK_ORDERS] with(nolock)";

            sql += "where TWO_NO = '{0}' and TWO_WORKSHOP='" + FormCover.CurrentFactory.Trim() + "' ";

            sql = string.Format(sql, WoMaster);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetWoMasterIno(string WoMaster, string WoType)
        {
            if (!WoMaster.Equals(""))
                WoMaster = WoMaster + "%";
            string sql = @"SELECT [TWO_SYSID]
                          ,[TWO_NO_ID]
                          ,[TWO_NO]
                          ,[TWO_ORDER_NO]
                          ,[TWO_ORDER_ITEM]
                          ,[TWO_PRODUCT_NAME]
                          ,[TWO_PRODUCT_REVERSION]
                          ,[TWO_PROCESS_NAME]
                          ,[TWO_PROCESS_REVERSION]
                          ,[TWO_BOM_SYSID]
                          ,[TWO_WO_BOM_GUID]
                          ,[TWO_WO_QTY]
                          ,[TWO_WO_ORIGINAL_QTY]
                          ,[TWO_CURRENT_STATUS]
                          ,[TWO_PLANSTART_DATETIME]
                          ,[TWO_PLANCOMPLETE_DATETIME]
                          ,[TWO_ACTCOMPLETE_DATETIME]
                          ,[TWO_LABELTEMPLATE_ID]
                          ,[TWO_WO_LABELTEMPLATE_GUID]
                          ,[TWO_PACKING_STANDARD_ID]
                          ,[TWO_WO_PACKING_GUID]
                          ,[TWO_STDPOWER_CATEGORY_ID]
                          ,[TWO_LASTUPDATE_TIMESTAMP]
                          ,[TWO_K3_SN_GUID]
                          ,[TWO_REMARK]
                          ,[TWO_CTM_MAX]
                          ,[TWO_CTM_MIN]
                          ,[TWO_CTM_PROOFING_ALERT]
                          ,[TWO_CTM_PROOFING_LIMITED]
                          ,[TWO_CTM_PROOFING_STD_CNT]
                          ,[TWO_SORTING_PLANES]
                          ,[TWO_SORTING_MAXPRINTNUM]
                          ,[TWO_WORKSHOP]
                          ,[TWO_CREATE_BY]
                          ,[TWO_CREATE_ON]
                          ,[TWO_MODIFIED_BY]
                          ,[TWO_MODIFIED_ON]
                          ,[RESV01]
                          ,[RESV02]
                          ,[RESV03]
                          ,[RESV04]
                          ,[RESV05]
                          ,[RESV06]
                          ,[RESV07]
                          ,[RESV08]
                          ,[RESV09]
                          ,[RESV10] 
                          FROM [T_WORK_ORDERS] with(nolock)";

            sql += "where TWO_WORKSHOP='" + FormCover.CurrentFactory.Trim() + "' and TWO_NO like '{0}' and RESV05 ='" + WoType + "' ";

            sql = string.Format(sql, WoMaster);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataSet GetWoInfo(string Wo, string WoType)
        {
            if (WoType.Equals("ZP07"))
                WoType = "ZP07','ZP13";
            else if (WoType.Equals("ZP02"))
                WoType = "ZP02','ZP13";
            string sql = @"SELECT *
                            FROM [csimes_SapInterface].[dbo].[T_WORK_ORDERS] WITH(NOLOCK)
                            WHERE TWO_NO='{0}' and RESV05 in('{1}')
  
                            SELECT DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM csimes_SapInterface.dbo.T_WORKORDER_BOM bom WITH(NOLOCK) INNER JOIN
                                 csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK) 
                                 ON bom.OrderNo = packing.OrderNo and bom.MaterialCode = packing.MaterialCode
                            WHERE packing.OrderNo='{0}' AND bom.MaterialCategory='Cell' 
                                  and packing.PickingType='261'
      
                            SELECT DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM csimes_SapInterface.dbo.T_WORKORDER_BOM bom WITH(NOLOCK) INNER JOIN
                                 csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK) 
                                 ON bom.OrderNo = packing.OrderNo and bom.MaterialCode = packing.MaterialCode
                            WHERE packing.OrderNo='{0}' AND bom.MaterialCategory='Glass' 
                                  and packing.PickingType='261'
         
                            SELECT DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM csimes_SapInterface.dbo.T_WORKORDER_BOM bom WITH(NOLOCK) INNER JOIN
                                 csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK) 
                                 ON bom.OrderNo = packing.OrderNo and bom.MaterialCode = packing.MaterialCode
                            WHERE packing.OrderNo='{0}' AND bom.MaterialCategory='EVA' 
                                  and packing.PickingType='261'
      
                            SELECT DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM csimes_SapInterface.dbo.T_WORKORDER_BOM bom WITH(NOLOCK) INNER JOIN
                                 csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK) 
                                 ON bom.OrderNo = packing.OrderNo and bom.MaterialCode = packing.MaterialCode
                            WHERE packing.OrderNo='{0}' AND bom.MaterialCategory='TPT' 
                                  and packing.PickingType='261'
      
                            SELECT DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM csimes_SapInterface.dbo.T_WORKORDER_BOM bom WITH(NOLOCK) INNER JOIN
                                 csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK) 
                                 ON bom.OrderNo = packing.OrderNo and bom.MaterialCode = packing.MaterialCode
                            WHERE packing.OrderNo='{0}' AND bom.MaterialCategory='CONBOX' 
                                  and packing.PickingType='261'

                            SELECT  DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] bom with (nolock),
                                 [csimes_SapInterface].[dbo].[TSAP_MATERIAL] material with(nolock),
                                  csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK)
                            WHERE bom.MaterialCategory='AIFRAME' and packing.OrderNo ='{0}'
                                  and bom.MaterialCode = material.MaterialCode
                                  and material.FrameType='1'
                                  and bom.MaterialCode = packing.MaterialCode
                                  and bom.OrderNo = packing.OrderNo
                                  and packing.PickingType='261'
      
                            SELECT  DISTINCT packing.Batch as Batch,packing.MaterialCode 
                            FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] bom with (nolock),
                                 [csimes_SapInterface].[dbo].[TSAP_MATERIAL] material with(nolock),
                                  csimes_SapInterface.dbo.T_PICKING packing WITH(NOLOCK)
                            WHERE bom.MaterialCategory='AIFRAME' and packing.OrderNo ='{0}'
                                  and bom.MaterialCode = material.MaterialCode
                                  and material.FrameType='2'
                                  and bom.MaterialCode = packing.MaterialCode
                                  and bom.OrderNo = packing.OrderNo
                                  and packing.PickingType='261'

SELECT *
FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] WITH(NOLOCK) 
WHERE OrderNo = '{0}' 
                            ";
            sql = string.Format(sql, Wo, WoType);
            return GetDataSetData(FormCover.InterfaceConnString, sql);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchno">批次号</param>
        /// <param name="mitemType">物料类型</param>
        /// <param name="_wo">工单</param>
        /// <param name="MitemCode">物料代码</param>
        /// <returns></returns>
        public static string GetMitemCode(string batchno, string mitemType, string _wo, string WOTYPE, out string MitemCode)
        {
            MitemCode = "";
            string sql = "";
            if (!WOTYPE.Trim().Equals("ZP11"))
            {
                if (!WOTYPE.Trim().Equals("ZP13"))//elps工单
                {
                    sql = @"SELECT distinct packing.MaterialCode,packing.MaterialDescription,packing.Batch,mapping.MAPPING_KEY_02 AS MaterailType
                          FROM [T_PICKING]  packing with(nolock),
                               [TSAP_MATERIAL] material with(nolock),
                               [T_SYS_MAPPING] mapping with(nolock),
                               [T_WORKORDER_BOM] wo with(nolock)
                          where packing.MaterialCode = material.MaterialCode 
                                and material.MaterialGroup = mapping.MAPPING_KEY_01
                                and packing.Batch like '{0}' and mapping.FUNCTION_CODE='SAP-MES-MATERIAL-CATEGORY'
                                and mapping.MAPPING_KEY_02='{1}'    
                                and packing.PickingTo in (SELECT MAPPING_KEY_01 FROM [T_SYS_MAPPING] with(nolock)
		                                                 where  FUNCTION_CODE='SAP-MES-PICKING-TO' and MAPPING_KEY_02 ='{2}')
                                and wo.OrderNo = '{3}'
                                and wo.MaterialCode = packing.MaterialCode ";

                    if (mitemType.Split('-').Length > 1)
                    {
                        string[] MaterialTypes = mitemType.Split('-');
                        if (MaterialTypes[1].Trim().ToUpper().Equals("SHORT"))
                            sql = sql + " and material.[FrameType] ='2'";
                        else if (MaterialTypes[1].Trim().ToUpper().Equals("LONG"))
                            sql = sql + " and material.[FrameType] ='1'";
                        mitemType = MaterialTypes[0].Trim().ToString();
                    }
                    sql = string.Format(sql, batchno, mitemType, FormCover.CurrentFactory, _wo);
                }
                else
                {
                    sql = @"SELECT distinct packing.MaterialCode,packing.MaterialDescription,packing.Batch,mapping.MAPPING_KEY_02 AS MaterailType
                          FROM [T_PICKING]  packing with(nolock),
                               [TSAP_MATERIAL] material with(nolock),
                               [T_SYS_MAPPING] mapping with(nolock),
                               [T_WORKORDER_BOM] wo with(nolock)
                          where packing.MaterialCode = material.MaterialCode 
                                and material.MaterialGroup = mapping.MAPPING_KEY_01
                                and packing.Batch like '{0}' and mapping.FUNCTION_CODE='SAP-MES-MATERIAL-CATEGORY'
                                and mapping.MAPPING_KEY_02='{1}'    
                                and packing.PickingTo in (SELECT MAPPING_KEY_01 FROM [T_SYS_MAPPING] with(nolock)
		                                                 where  FUNCTION_CODE='ELPS_WO_TYPE')
                                and wo.OrderNo = '{2}'
                                and wo.MaterialCode = packing.MaterialCode ";

                    if (mitemType.Split('-').Length > 1)
                    {
                        string[] MaterialTypes = mitemType.Split('-');
                        if (MaterialTypes[1].Trim().ToUpper().Equals("SHORT"))
                            sql = sql + " and material.[FrameType] ='2'";
                        else if (MaterialTypes[1].Trim().ToUpper().Equals("LONG"))
                            sql = sql + " and material.[FrameType] ='1'";
                        mitemType = MaterialTypes[0].Trim().ToString();
                    }
                    sql = string.Format(sql, batchno, mitemType, _wo);
                }
            }
            else
            {
                sql = @"SELECT distinct packing.MaterialCode,packing.MaterialDescription,packing.Batch,mapping.MAPPING_KEY_02 AS MaterailType
                          FROM [T_PICKING]  packing with(nolock),
                               [TSAP_MATERIAL] material with(nolock),
                               [T_SYS_MAPPING] mapping with(nolock)
                          where packing.MaterialCode = material.MaterialCode 
                                and material.MaterialGroup = mapping.MAPPING_KEY_01
                                and packing.Batch like '{0}' and mapping.FUNCTION_CODE='SAP-MES-MATERIAL-CATEGORY'
                                and mapping.MAPPING_KEY_02='{1}'    
                                and packing.PickingTo in (SELECT MAPPING_KEY_01 FROM [T_SYS_MAPPING] with(nolock)
		                                                 where  FUNCTION_CODE='SAP-MES-PICKING-TO' and MAPPING_KEY_02 ='{2}')";
                if (mitemType.Split('-').Length > 1)
                {
                    string[] MaterialTypes = mitemType.Split('-');
                    if (MaterialTypes[1].Trim().ToUpper().Equals("SHORT"))
                        sql = sql + " and material.[FrameType] ='2'";
                    else if (MaterialTypes[1].Trim().ToUpper().Equals("LONG"))
                        sql = sql + " and material.[FrameType] ='1'";
                    mitemType = MaterialTypes[0].Trim().ToString();
                }
                sql = string.Format(sql, batchno, mitemType, FormCover.CurrentFactory);
            }

            DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                MitemCode = Convert.ToString(dt.Rows[0]["MaterialCode"]);
                return Convert.ToString(dt.Rows[0]["MaterialCode"]);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 返回批次对应的物料代码明细
        /// </summary>
        /// <param name="batchno"></param>
        /// <param name="mitemType"></param>
        /// <param name="_wo"></param>
        /// <param name="WOTYPE"></param>
        /// <param name="MitemCode"></param>
        /// <returns></returns>
        public static DataTable GetMulMitemCode(string batchno, string mitemType, string _wo, string WOTYPE, out DataTable MitemCodes)
        {
            MitemCodes = null;
            string sql = @"SELECT distinct packing.MaterialCode
                            FROM [T_PICKING]  packing with(nolock),
                            [T_WORKORDER_BOM] wo with(nolock)
                            where  wo.OrderNo = '{0}'
                                   and wo.MaterialCode = packing.MaterialCode
                                   and wo.OrderNo = packing.OrderNo 
                                   and packing.Batch='{1}'";
            sql = string.Format(sql, _wo, batchno);

            DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                MitemCodes = dt;
                return MitemCodes;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获取工单单身信息
        /// </summary>
        /// <param name="WoDetail"></param>
        /// <returns></returns>
        public static DataTable GetWoDetailInfo(string WoDetail)
        {
            string sql = @"SELECT [OrderNo],[MaterialCategory],[MaterialCode],[Batch]    
                           FROM [dbo].[T_WORKORDER_BOM] WITH(NOLOCK) where OrderNo='{0}' and WorkShop='" + FormCover.CurrentFactory.Trim() + "' ";
            sql = string.Format(sql, WoDetail);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }
        public static DataTable GetWoDetailInfo(string Wo, string WoType)
        {
            string sql = @"SELECT wo.[TWO_NO],wo.[TWO_WORKSHOP],wo.[TWO_PRODUCT_NAME],wo.[RESV04],wo.[RESV06],wo.[TWO_ORDER_NO],wo.[RESV05],
                                   detail.[OrderNo],detail.[MaterialCategory],detail.[MaterialCode],detail.[Batch]   
                           FROM [T_WORK_ORDERS] wo with(nolock)  inner join  [T_WORKORDER_BOM]  detail with(nolock) on wo.TWO_NO = detail.OrderNo 
                           WHERE wo.TWO_WORKSHOP = '" + FormCover.CurrentFactory.Trim() + "'  and wo.TWO_NO = detail.OrderNo and wo.TWO_NO='" + Wo + "' and wo.RESV05='" + WoType + "'";
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetStorageMitemCodeInfo(string MitemCode)
        {
            string sql = @"SELECT * FROM  T_SYS_MAPPING WITH(NOLOCK) 
                           WHERE  [FUNCTION_CODE] ='StorageMitemCode'  AND MAPPING_KEY_01='{0}'";
            sql = string.Format(sql, MitemCode);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static string GetWOType(string wo)
        {
            string wotype = "";
            string sql = @"SELECT * FROM [T_WORK_ORDERS] with(nolock) where  TWO_NO='{0}'";
            sql = string.Format(sql, wo);

            DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);

            if (dt != null && dt.Rows.Count > 0)
                wotype = Convert.ToString(dt.Rows[0]["RESV05"]);

            return wotype;
        }

        public static DataTable GetPackingSystemCartonNoRuleFlag()
        {
            string sql = @"SELECT * FROM  T_SYS_MAPPING WITH(NOLOCK) 
                            WHERE FUNCTION_CODE='PackingCartonNoRule' AND MAPPING_KEY_01='Y'";
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DateTime GetServiceDate()
        {
            DateTime dttime = new DateTime();
            string sql = @"SELECT GETDATE() AS SYSDATE";
            DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                dttime = Convert.ToDateTime(dt.Rows[0]["SYSDATE"]);
            }
            return dttime;
        }

        public static int GetCartonNoLenth()
        {
            try
            {
                int length = 0;
                string sql = @"SELECT * FROM  T_SYS_MAPPING WITH(NOLOCK) 
                            WHERE FUNCTION_CODE='CartonNoLenth' ";

                DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);

                if (dt != null && dt.Rows.Count > 0)
                    length = Convert.ToInt32(dt.Rows[0]["MAPPING_KEY_01"]);

                return length;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        #region 取消入库
        /// <summary>
        /// 入库信息查询
        /// </summary>
        /// <param name="QueryType">查询类型</param>
        /// <param name="carton">托号</param>
        /// <param name="StorageDateFrom">入库开始日期</param>
        /// <param name="StorageDateTo">入库结束日期</param>
        /// <returns></returns>
        public static DataTable GetCancelStorageInfo(string QueryType, string carton, DateTime StorageDateFrom, DateTime StorageDateTo)
        {
            string sql = @"SELECT '0' AS Selected, prd.BoxID AS CartonID,'已入库' AS CartonStatus,COUNT(*) AS SNQTY,box.StorageDate,box.[Cust_BoxID] FROM [Box] box with(nolock)  inner join [Product] prd  with(nolock) on box.BoxID = prd.BoxID ";
            if (QueryType.Equals("StorageDate"))
                sql += @"where box.StorageDate >='" + StorageDateFrom.ToShortDateString() + "' and box.StorageDate<'" + StorageDateTo.ToShortDateString() + "' and box.Flag='2' group by prd.BoxID,box.StorageDate,box.[Cust_BoxID]";
            else
                sql += @"where box.boxid = '" + carton + "' and box.Flag='2' group by prd.BoxID,box.StorageDate,box.[Cust_BoxID]";
            return GetDataTableData(FormCover.connectionBase, sql);
        }
        /// <summary>
        /// 获取每托的信息
        /// </summary>
        /// <param name="Carton">托号</param>
        /// <returns></returns>
        public static DataTable GetCartonWoQty(string Carton)
        {
            string sql = @"SELECT [BoxID],[WorkOrder],SN
                            FROM   [Product] with(nolock) where BoxID='{0}' ";
            sql = string.Format(sql, Carton);
            return GetDataTableData(FormCover.connectionBase, sql);
        }
        /// <summary>
        /// 扣除撤销入库工单的数量的SQL
        /// </summary>
        /// <param name="carton">托号</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string GetCancelStorageWoQty(string carton, out string msg)
        {
            msg = "";
            try
            {
                #region 获取托中的工单数量
                string sqlInterfaceWo = "";
                DataTable dt = ProductStorageDAL.GetCartonWoQty(carton);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var WoQtyList = ProductStorageDAL.GetEveryWOStorageTotalQty(dt);

                    #region
                    if (WoQtyList.Count > 0)
                    {
                        for (int i = 0; i < WoQtyList.Count; i++)
                        {
                            int WaintStorageQty = 0;
                            DataTable dtQty = ProductStorageDAL.GetWOStorageQty(WoQtyList[i][0]);
                            if ((dtQty != null) && dtQty.Rows.Count > 0)
                            {
                                DataRow row = dtQty.Rows[0];
                                int WOStorageQty = Convert.ToInt32(row["RESV07"]);//工单已入库数量
                                if (WOStorageQty > 0)
                                    WaintStorageQty = WOStorageQty - Convert.ToInt16(WoQtyList[i][1]);
                                else
                                    WaintStorageQty = 0;
                            }
                            else
                            {
                                msg = "工单 " + WoQtyList[i][0] + " 没有从SAP下载，请确认！";
                                return "";
                            }
                            string stringInterfaceWo = @"UPDATE {0}.[dbo].[T_WORK_ORDERS] SET RESV07='{1}' WHERE TWO_NO = '{2}';";
                            stringInterfaceWo = string.Format(stringInterfaceWo, FormCover.SAPDBName, WaintStorageQty, WoQtyList[i][0]);
                            sqlInterfaceWo += stringInterfaceWo;
                        }
                    }
                    return sqlInterfaceWo;
                    #endregion
                }
                else
                {
                    msg = "没有获取到托(" + carton + ")所对应的信息，请确认";
                    return "";
                }
                #endregion

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return "";
            }
        }
        public static bool GetWoMaxCount(string WoMaster)
        {
        	try{
        		string sql = @"select two_wo_qty from T_WORK_ORDERS Where TWO_NO = '{0}'";
        		sql = string.Format(sql,WoMaster);
        		DataTable dt = GetDataTableData(FormCover.InterfaceConnString,sql);
        		
        		
        		sql =  @"UPDATE [T_WORK_ORDERS] SET RESV08 = '{0}',RESV09 = '0'  WHERE TWO_NO ='{1}' ";
        		sql = string.Format(sql, dt.Rows[0][0].ToString(), WoMaster);
        		 
        		using (SqlConnection SqlConnection = new SqlConnection(FormCover.InterfaceConnString))
                {
                    SqlConnection.Open();
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        SqlCommand.Connection = SqlConnection;
                        if (SqlHelper.ExecuteNonQuery(SqlConnection, CommandType.Text, sql) < 1)
                        {
                            return false;
                        }
                    }
                }
                return true;     		
        	
        	}
        	catch (Exception ex)
            {
              
                return false;
            }
        	
        }
        public static bool UpdateWoDataIno(string WoMaster, int inputqty, int scrapqty, out string msg)
        {
            msg = "";
            try
            {
                string sql = @"UPDATE [T_WORK_ORDERS] SET RESV08 = '{0}',RESV09 = '{1}'  WHERE TWO_NO ='{2}' ";
                sql = string.Format(sql, inputqty, scrapqty, WoMaster);
                using (SqlConnection SqlConnection = new SqlConnection(FormCover.InterfaceConnString))
                {
                    SqlConnection.Open();
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        SqlCommand.Connection = SqlConnection;
                        if (SqlHelper.ExecuteNonQuery(SqlConnection, CommandType.Text, sql) < 1)
                        {
                            msg = "更新数据库失败";
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 更新取消入库的数据
        /// </summary>
        /// <param name="carton">托号</param>
        /// <returns></returns>
        public static bool UpdateCancelStorageInfo(string carton, out string msg, string joinid, string posttime)
        {
            msg = "";

            #region
            string sql = @"update {0}.dbo.Box set flag='1',CancelStorageDate = getdate(),CancelStorageOperator='" + FormCover.currUserName + "'  where BoxID='" + carton + "'";
            string sql2 = @"update {0}.dbo.Product set flag='1',WorkOrder='',CancelStorageDate = getdate(),CancelStorageOperator='" + FormCover.currUserName + "'  where BoxID='" + carton + "'";
            sql = string.Format(sql, FormCover.PackingDBName);
            sql2 = string.Format(sql2, FormCover.PackingDBName);
            string sql3 = @"UPDATE {0}.dbo.T_MODULE SET WORK_ORDER=''
                                              WHERE MODULE_SN IN(
                                              SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                                              {0}.dbo.T_MODULE modules with(nolock)
                                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                                              and list.MODULE_SYSID = modules.SYSID
                                              and cartons.RESV03 = '{1}');
                                          UPDATE {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON=''
                                          WHERE MODULE_SN in ( SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                                              {0}.dbo.T_MODULE modules with(nolock)
                                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                                              and list.MODULE_SYSID = modules.SYSID
                                              and cartons.RESV03 = '{1}');
                                          UPDATE {0}.dbo.[T_MODULE_CARTON]  SET RESV04='' WHERE RESV03 = '{1}' ";
            sql3 = string.Format(sql3, FormCover.CenterDBName, carton);

            string interfaceSQL = GetCancelStorageWoQty(carton, out msg);
            #endregion

            List<MODULEPACKINGTRANSACTION> _ListModulePacking = new List<MODULEPACKINGTRANSACTION>();
            List<CARTONPACKINGTRANSACTION> _ListCartonPacking = new List<CARTONPACKINGTRANSACTION>();
            SaveStorageDataForReport(carton, "UnInvertory", _ListModulePacking, _ListCartonPacking, joinid, posttime);

            using (SqlConnection SqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                SqlConnection.Open();
                using (SqlTransaction SqlTransaction = SqlConnection.BeginTransaction())
                {
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        SqlCommand.Connection = SqlConnection;
                        SqlCommand.Transaction = SqlTransaction;
                        try
                        {
                            if (!interfaceSQL.Equals(""))
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, interfaceSQL, null) < 1)
                                {
                                    msg = "";
                                    SqlTransaction.Rollback();
                                    return false;
                                }
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql2, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql3, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }
                            if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTransaction, _ListCartonPacking, _ListModulePacking, out msg))
                            {
                                // ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                SqlTransaction.Rollback();
                                return false;
                            }
                            SqlTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            msg = ex.Message;
                            SqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// 更新取消入库的数据
        /// </summary>
        /// <param name="carton">托号</param>
        /// <returns></returns>
        public static bool UpdateCancelStorageInfo(string carton, out string msg, string joinid, string posttime,List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            msg = "";

            #region
            string sql = @"update {0}.dbo.Box set flag='1',CancelStorageDate = getdate(),CancelStorageOperator='" + FormCover.currUserName + "'  where BoxID='" + carton + "'";
            string sql2 = @"update {0}.dbo.Product set flag='1',WorkOrder='',CancelStorageDate = getdate(),CancelStorageOperator='" + FormCover.currUserName + "'  where BoxID='" + carton + "'";
            sql = string.Format(sql, FormCover.PackingDBName);
            sql2 = string.Format(sql2, FormCover.PackingDBName);
            string sql3 = @"UPDATE {0}.dbo.T_MODULE SET WORK_ORDER=''
                                              WHERE MODULE_SN IN(
                                              SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                                              {0}.dbo.T_MODULE modules with(nolock)
                                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                                              and list.MODULE_SYSID = modules.SYSID
                                              and cartons.RESV03 = '{1}');
                                          UPDATE {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON=''
                                          WHERE MODULE_SN in ( SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                                              {0}.dbo.T_MODULE modules with(nolock)
                                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                                              and list.MODULE_SYSID = modules.SYSID
                                              and cartons.RESV03 = '{1}');
                                          UPDATE {0}.dbo.[T_MODULE_CARTON]  SET RESV04='' WHERE RESV03 = '{1}' ";
            sql3 = string.Format(sql3, FormCover.CenterDBName, carton);

            string interfaceSQL = GetCancelStorageWoQty(carton, out msg);
            #endregion

            List<MODULEPACKINGTRANSACTION> _ListModulePacking = new List<MODULEPACKINGTRANSACTION>();
            List<CARTONPACKINGTRANSACTION> _ListCartonPacking = new List<CARTONPACKINGTRANSACTION>();
            SaveStorageDataForReport(carton, "UnInvertory", _ListModulePacking, _ListCartonPacking, joinid, posttime);

            using (SqlConnection SqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                SqlConnection.Open();
                using (SqlTransaction SqlTransaction = SqlConnection.BeginTransaction())
                {
                    foreach (var tsapReceiptUploadModule in tsapReceiptUploadModules)
                    {
                        if (!DataAccess.UpdateTsapReceiptUploadModule(tsapReceiptUploadModule, SqlConnection, SqlTransaction))
                        {
                            SqlTransaction.Rollback();
                            return false;
                        }
                    }
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        SqlCommand.Connection = SqlConnection;
                        SqlCommand.Transaction = SqlTransaction;
                        try
                        {
                            if (!interfaceSQL.Equals(""))
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, interfaceSQL, null) < 1)
                                {
                                    msg = "";
                                    SqlTransaction.Rollback();
                                    return false;
                                }
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql2, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql3, null) < 1)
                            {
                                msg = "";
                                SqlTransaction.Rollback();
                                return false;
                            }
                            if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTransaction, _ListCartonPacking, _ListModulePacking, out msg))
                            {
                                // ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                SqlTransaction.Rollback();
                                return false;
                            }
                            SqlTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            msg = ex.Message;
                            SqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        #endregion

        /// <summary>
        ///  包装信息查询
        /// </summary>
        /// <param name="QueryType">查询类型</param>
        /// <param name="carton">托号</param>
        /// <param name="StorageDateFrom">包装开始日期</param>
        /// <param name="StorageDateTo">包装结束日期</param>
        /// <param name="Counter">货柜号</param>
        /// <returns></returns>
         public static DataTable GetCartonStorageInfo(string QueryType, string carton, DateTime PackingDateFrom, DateTime PackingDateTo, string counter)
        {
            carton = carton + "%";
            counter = counter + "%";
            PackingDateTo = PackingDateTo.AddDays(1);
            string sql = @"SELECT distinct '0' AS Selected, prd.BoxID AS CartonID,box.flag as CartonStatus,COUNT(*) AS SNQTY,box.[Cust_BoxID] FROM [Box] box with(nolock)  inner join [Product] prd  with(nolock) on box.BoxID = prd.BoxID ";
            if (QueryType.Equals("PackingDate"))
                sql += @"where box.PackDate >'" + PackingDateFrom.ToShortDateString() + "' and box.PackDate <'" + PackingDateTo.ToShortDateString() + "' and box.Flag='1' group by prd.BoxID,box.flag,box.Cust_BoxID";
            else if (QueryType.Equals("Carton"))
                sql += @"where box.boxid like '" + carton + "' and box.Flag='1' group by prd.BoxID,box.flag,box.Cust_BoxID";
            else
                sql += @"where box.JobNo like '" + counter + "' and box.Flag='1' group by prd.BoxID,box.flag,box.Cust_BoxID";
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static DataTable GetCartonStorageInfoByCarton(string carton)
        {
            string sql = @"SELECT distinct '0' AS Selected, prd.BoxID AS CartonID,box.flag as CartonStatus,COUNT(*) AS SNQTY ,Box.Cust_BoxID FROM [Box] box with(nolock)  inner join [Product] prd  with(nolock) on box.BoxID = prd.BoxID 
                          where box.boxid = '" + carton + "' and box.Flag in ('1','5') group by prd.BoxID,box.flag,Box.Cust_BoxID";

            return GetDataTableData(FormCover.connectionBase, sql);
        }
    
     	 public static DataTable GetSNStroageBC(string CartonNo)//jacky
        {
            try
            {
                string sql = @"SELECT SN FROM  Product prd WITH(nolock),
                                     Box bx with(nolock)
                               WHERE prd.BoxID = '{0}' AND bx.BoxID ='{0}'
                                     AND prd.BoxID = bx.BoxID";               
                sql = string.Format(sql, CartonNo.Trim());
                return GetDataTableData(FormCover.connectionBase, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取组件数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }
     	 
     	 public static DataTable GetSnInfoUsingSetofSn (List<string> Temp)//jacky
        {
            string sql = string.Empty;
            DataTable dt = null;        
           string sqlprefix = string.Empty;
          
      
           
           	sql = @"SELECT a.[MODULE_SN],a.[PACK_MA_SYSID], a.[POST_ON],a.[LASTUPDATEUSER],a.[LASTUPDATETIME],b.[MaterialTemplateName]
                    FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] a
                    LEFT JOIN [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] b
                    ON a.[PACK_MA_SYSID] = b.[SYSID]
                    WHERE a.[MODULE_SN] in ('{0}'";
          
            for(int i = 0; i < Temp.Count; i++)
            {
            	
            
            	sql = string.Format(sql, Temp[i]);
            	if(i+1 < Temp.Count)
            		sql= sql+ @",'{0}'";          	
            	
            }
            sql= sql+")";
           dt = GetDataTableData(FormCover.InterfaceConnString, sql);
            return dt;
           
        }

        /// <summary>
        /// 获取批次料号
        /// </summary>
        /// <param name="batch">批次号</param>
        /// <parma name="MaterialType"></parma>物料类型</parm>
        /// <parma name="wo"></parma>工单</parm>
        /// <parma name="wotype"></parma>工单类型</parm>
        /// <returns></returns>
        public static DataTable GetBatchInfo(string batch, string MaterialType, string wo, string wotype)
        {
            string sql = "";
            if (!batch.Equals(""))
                batch = batch + "%";
            if (!wotype.Trim().Equals("ZP11"))
            {
                sql = @"SELECT distinct packing.MaterialCode,packing.MaterialDescription,packing.Batch,mapping.MAPPING_KEY_02 AS MaterailType
                          FROM [T_PICKING]  packing with(nolock),
                               [TSAP_MATERIAL] material with(nolock),
                               [T_SYS_MAPPING] mapping with(nolock),
                               [T_WORKORDER_BOM] wo with(nolock)
                          where packing.MaterialCode = material.MaterialCode 
                                and material.MaterialGroup = mapping.MAPPING_KEY_01
                                and packing.Batch like '{0}' and mapping.FUNCTION_CODE='SAP-MES-MATERIAL-CATEGORY'
                                and mapping.MAPPING_KEY_02='{1}'    
                                and packing.PickingTo in (SELECT MAPPING_KEY_01 FROM [T_SYS_MAPPING] with(nolock)
		                                                 where  FUNCTION_CODE='SAP-MES-PICKING-TO' and MAPPING_KEY_02 ='{2}')
                                and wo.OrderNo = {3}
                                and wo.MaterialCode = packing.MaterialCode";

                if (MaterialType.Split('-').Length > 1)
                {
                    string[] MaterialTypes = MaterialType.Split('-');
                    if (MaterialTypes[1].Trim().ToUpper().Equals("SHORT"))
                        sql = sql + " and material.[FrameType] ='2'"; //短边框
                    else if (MaterialTypes[1].Trim().ToUpper().Equals("LONG"))
                        sql = sql + " and material.[FrameType] ='1'";//长边框
                    MaterialType = MaterialTypes[0].Trim().ToString();
                }
                sql = string.Format(sql, batch, MaterialType, FormCover.CurrentFactory, wo);
            }
            else
            {
                sql = @"SELECT distinct packing.MaterialCode,packing.MaterialDescription,packing.Batch,mapping.MAPPING_KEY_02 AS MaterailType
                          FROM [T_PICKING]  packing with(nolock),
                               [TSAP_MATERIAL] material with(nolock),
                               [T_SYS_MAPPING] mapping with(nolock)
                          where packing.MaterialCode = material.MaterialCode 
                                and material.MaterialGroup = mapping.MAPPING_KEY_01
                                and packing.Batch like '{0}' and mapping.FUNCTION_CODE='SAP-MES-MATERIAL-CATEGORY'
                                and mapping.MAPPING_KEY_02='{1}'    
                                and packing.PickingTo in (SELECT MAPPING_KEY_01 FROM [T_SYS_MAPPING] with(nolock)
		                                                 where  FUNCTION_CODE='SAP-MES-PICKING-TO' and MAPPING_KEY_02 ='{2}')";

                if (MaterialType.Split('-').Length > 1)
                {
                    string[] MaterialTypes = MaterialType.Split('-');
                    if (MaterialTypes[1].Trim().ToUpper().Equals("SHORT"))
                        sql = sql + " and material.[FrameType] ='2'"; //短边框
                    else if (MaterialTypes[1].Trim().ToUpper().Equals("LONG"))
                        sql = sql + " and material.[FrameType] ='1'";//长边框
                    MaterialType = MaterialTypes[0].Trim().ToString();
                }
                sql = string.Format(sql, batch, MaterialType, FormCover.CurrentFactory);

            }
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        //读取xml字符串到DataSet中
        /// <summary>
        /// 读取xml字符串到DataSet中
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static DataSet ReadXmlToDataSet(string xmlString)
        {
            
        	StringReader sr = null;
            XmlTextReader xtr = null;
            DataSet ds = new DataSet();
            try
            {
                sr = new StringReader(xmlString);
                xtr = new XmlTextReader(sr);
                ds.ReadXml(xtr);
            }
            catch (Exception ex)
            {
                //Log
                throw ex;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (xtr != null)
                    xtr.Close();
            }

            return ds;
        }

        public static DataTable ReadXmlToDataTable(string xmlString, string nodeName, out string message)
        {
            message = "";

            try
            {
                DataSet ds = ReadXmlToDataSet(xmlString);
                if (ds == null || ds.Tables.Count <= 0)
                {
                    message = "Read xml string to dataset fail";
                    return null;
                }

                DataTable table = ds.Tables[nodeName];
                if (table == null || table.Rows.Count <= 0)
                {
                    message = "Cann't find the data of node: " + nodeName;
                    return null;
                }

                return table;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }

        }
        public static string GetCartonBySN(string SN)
        {
            string sql = @"SELECT BoxID FROM [Product] WHERE SN='{0}'";
            sql = string.Format(sql, SN);
            DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
            if (dt != null && dt.Rows.Count > 0)
                return Convert.ToString(dt.Rows[0]["BoxID"]);
            else
                return "";
        }

        /// <summary>
        /// 更新包装和集中数据库
        /// </summary>
        /// <param name="SNLocalDBExist">SN在本机数据库存在</param>
        /// <param name="SNNotLocalDBExist">SN不在本机数据库存在</param>
        /// <param name="boxid">内部箱号</param>
        /// <param name="custboxid">客户箱号</param>
        /// <param name="number">箱的数量</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool UpdateData(List<string> SNLocalDBExist, List<string> SNNotLocalDBExist,
            string boxid, string custboxid, int number, string StdPower,
            List<List<string>> moduleClassList, out string msg)
        {
            try
            {
                msg = "";

                #region 本车间存在的组件
                string SNExists = "";
                if (SNLocalDBExist.Count > 1)
                {
                    foreach (string sn in SNLocalDBExist)
                    {
                        SNExists += ",'" + sn + "'";
                    }
                    SNExists = SNExists.Substring(1, SNExists.Length - 1);
                    SNExists = "(" + SNExists + ")";
                }
                else if (SNLocalDBExist.Count == 1)
                {
                    foreach (string sn in SNLocalDBExist)
                    {
                        SNExists = "('" + sn + "')";
                    }
                }
                #endregion

                #region 本车间不存在的组件
                string SNNOTExists = "";
                if (SNNotLocalDBExist.Count > 1)
                {
                    foreach (string sn in SNNotLocalDBExist)
                    {
                        SNNOTExists += ",'" + sn + "'";
                    }
                    SNNOTExists = SNNOTExists.Substring(1, SNNOTExists.Length - 1);
                    SNNOTExists = "(" + SNNOTExists + ")";
                }
                else if (SNNotLocalDBExist.Count == 1)
                {
                    foreach (string sn in SNNotLocalDBExist)
                    {
                        SNNOTExists = "('" + sn + "')";
                    }
                }
                #endregion

                //所有的组件号
                string snTotals = "";
                if ((SNLocalDBExist.Count > 0) && SNNotLocalDBExist.Count > 0)
                    snTotals = SNExists.Substring(0, SNExists.Trim().Length - 1) + "," + SNNOTExists.Substring(1, SNNOTExists.Trim().Length - 1);
                else if ((SNLocalDBExist.Count > 0) && (SNNotLocalDBExist.Count < 1))
                    snTotals = SNExists;
                else if ((SNNotLocalDBExist.Count > 0) && (SNLocalDBExist.Count < 1))
                    snTotals = SNNOTExists;

                int SnCount = 0;
                SnCount = SNNotLocalDBExist.Count + SNLocalDBExist.Count;

                string insterSql = "";
                if (!SNNOTExists.Equals(""))
                {
                    //写入本车间不存在的组件信息
                    insterSql = @"INSERT INTO {0}.[dbo].[Product]
                                           ([SN],[ModelType],[Flag],[StdPower],[Pmax],[OperatorID],[localflag],Process)     
                                    SELECT [MODULE_SN],[MODULE_TYPE],'5',[STD_POWER_LEVEL],[PMAX],'Auto','1','T5'
                                    FROM {1}.[dbo].[T_MODULE_REWORK] with(nolock) 
                                    WHERE module_sn in {2} and flag='0';
                                    INSERT INTO {0}.[dbo].[ElecParaTest]
                                           ([FNumber],[Temp],[VOC],[ISC],[Pmax],[Vm],[Im]
                                           ,[FF],[Eff],[Operator])
                                    SELECT [MODULE_SN],[TEMP],[VOC],[ISC],[PMAX],[VMP],[IMP],[FF],[EFF],'Auto' 
                                    FROM {1}.[dbo].[T_MODULE_REWORK] with(nolock) 
                                    WHERE  MODULE_SN in {2} AND FLAG='0';
                                     UPDATE {0}.dbo.[Product] SET  [Product].InterNewTempTableID = test.InterID
                                     FROM {0}.dbo.Product prd with(nolock),{0}.dbo.ElecParaTest test with(nolock)
                                     WHERE prd.SN =  test.FNumber AND prd.SN in {2} ";
                    insterSql = string.Format(insterSql, FormCover.PackingDBName, FormCover.CenterDBName, SNNOTExists);
                }

                //更新包装系统数据库数据
                string updateSql = @" UPDATE {0}.[dbo].[Product] 
                                   SET [Cust_BoxID]='{1}',BoxID='{2}',Flag='5',Reworker='{3}',ReworkDate= getdate()
                                   WHERE SN in {4};
                                   UPDATE {0}.[dbo].Box SET Number='{5}',IsUsed='Y',PackDate= getdate(),
                                        PackOperator='{3}',Flag='5',Cust_BoxID='{1}'
                                   WHERE BoxID ='{2}';
                                   if not exists (select BoxID from {0}.dbo.Box where boxid='{2}') insert into {0}.dbo.Box(BoxID,Number,IsUsed,PackOperator,Cust_BoxID,Flag,PackDate,BoxType,ModelType) values('{2}','" + SnCount + "','Y','" + FormCover.CurrUserWorkID + "','{1}','5',getdate(),'','')";

                updateSql = string.Format(updateSql, FormCover.PackingDBName, custboxid, boxid,
                            FormCover.currUserName, snTotals, number);

                //更新集中数据库数据
                string CenterSql = @"INSERT INTO {0}.[dbo].[T_MODULE_CARTON]
                                    ([SYSID],[CARTON_NO] ,[CREATED_NO],[CREATED_BY],[RESV03],[STD_POWER_LEVEL],[RESV04])
                                    VALUES
                                    ('{1}','{2}','{3}','{4}','{5}','{6}','{8}');
                                    INSERT INTO {0}.[dbo].[T_MODULE_PACKING_LIST](MODULE_SYSID ,MODULE_CARTON_SYSID)
                                    SELECT  [SYSID],'{1}'
                                    FROM {0}.[dbo].[T_MODULE] with(nolock) 
                                    WHERE MODULE_SN in {7};
                                    UPDATE {0}.[dbo].[T_MODULE_REWORK] SET FLAG='1'
                                    WHERE MODULE_SN in {7};
                                    UPDATE {0}.[dbo].[T_MODULE] SET WORKSHOP='{8}'
                                    WHERE MODULE_SN in {7};";
                CenterSql = string.Format(CenterSql, FormCover.CenterDBName, Guid.NewGuid().ToString() + "|" + DateTime.Now.ToString("HH:mm:ss.ff"), custboxid,
                                 DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), FormCover.CurrUserName,
                                 boxid, StdPower, snTotals, FormCover.CurrentFactory.Trim());

                #region 对于本车存在的组件，在集中数据存不存在的处理
                #region
                string CenterDBModelsql = "";
                List<string> SnModelList = new List<string>();
                foreach (string sn in SNLocalDBExist)
                {
                    DataTable dt = GetSnInfoFromCenterDB(sn);
                    if ((dt != null) && (dt.Rows.Count > 0))
                    { }
                    else
                        SnModelList.Add(sn);
                }
                if (SnModelList.Count > 0)
                {
                    foreach (string sn in SnModelList)
                    {
                        string Modelsql = @"INSERT INTO {0}.[dbo].[T_MODULE]([SYSID]
                                           ,[MODULE_SN],[BARCODE],[WORKSHOP] ,[MODULE_TYPE],[CREATED_ON]
                                           ,[CREATED_BY])
                                           SELECT '{1}',SN,SN,'{2}',ModelType,'{3}','{4}' FROM {5}.[dbo].[Product] with(nolock) where SN = '{6}';";
                        Modelsql = string.Format(Modelsql, FormCover.CenterDBName.Trim(),
                            Guid.NewGuid().ToString() + "|" + DateTime.Now.ToString("HH:mm:ss.ff"),
                            FormCover.CurrentFactory.Trim(), FormCover.currUserName.Trim(), DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss.ff"), FormCover.PackingDBName, sn);
                        CenterDBModelsql += Modelsql;
                    }
                }
                #endregion

                #region
                string CenterDBTestsql = "";
                List<string> SnTestList = new List<string>();
                foreach (string sn in SNLocalDBExist)
                {
                    DataTable dt = GetSnInfoFromCenterDBTest(sn);
                    if ((dt != null) && (dt.Rows.Count > 0))
                    { }
                    else
                        SnTestList.Add(sn);
                }
                if (SnTestList.Count > 0)
                {
                    foreach (string sn in SnTestList)
                    {
                        string Testsql = @"INSERT INTO {0}.[dbo].[T_MODULE_TEST]
                                           ([SYSID] ,[MODULE_SN],[TEMP],[ISC] ,[VOC],[IMP],[VMP],[PMAX],[FF] ,[EFF],[TEST_DATE] ,[CREATED_NO] ,[CREATED_BY])
                                           SELECT TOP 1 '{1}', FNumber, [Temp],[ISC],[VOC],[Im],[Vm],[Pmax],[FF],[Eff],[TestDateTime],'{2}','{3}'
                                           FROM {4}.[dbo].[ElecParaTest] WITH(NOLOCK) WHERE FNumber='{5}' ORDER BY TestDateTime DESC ;";
                        Testsql = string.Format(Testsql, FormCover.CenterDBName.Trim(),
                            Guid.NewGuid().ToString() + "|" + DateTime.Now.ToString("HH:mm:ss.ff"),
                            FormCover.currUserName.Trim(), DateTime.Now.ToString("yyyy-mm-dd HH:mm:ss.ff"), FormCover.PackingDBName, sn);
                        CenterDBTestsql += Testsql;
                    }
                }
                #endregion
                #endregion

                //更新包装系统的组件等级
                string UpdatePackingModuleClass = "";
                foreach (List<string> vlist in moduleClassList)
                {
                    string sn = vlist[0];
                    string moduleclass = vlist[1];
                    string sql = @"UPDATE {0}.[dbo].[Product] SET ModuleClass='{1}',Process='T5' WHERE SN='{2}' ";
                    sql = string.Format(sql, FormCover.PackingDBName, moduleclass.Trim(), sn.Trim());
                    UpdatePackingModuleClass += sql;
                }

                using (SqlConnection SqlConnection = new SqlConnection(FormCover.CenterDBConnString))
                {
                    SqlConnection.Open();
                    using (SqlTransaction SqlTran = SqlConnection.BeginTransaction())
                    {
                        try
                        {
                            if (SnModelList.Count > 0)
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, CenterDBModelsql, null) < 0)
                                {
                                    msg = "更新集中数据库t_model失败";
                                    SqlTran.Rollback();
                                    return false;
                                }
                            }

                            if (SnTestList.Count > 0)
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, CenterDBTestsql, null) < 0)
                                {
                                    msg = "更新集中数据库t_test失败";
                                    SqlTran.Rollback();
                                    return false;
                                }
                            }

                            if (!SNNOTExists.Equals(""))
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, insterSql, null) < 0)
                                {
                                    msg = "写入本车间不存在的组件信息失败";
                                    SqlTran.Rollback();
                                    return false;
                                }
                            }
                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, updateSql, null) < 0)
                            {
                                msg = "更新包装系统数据库数据失败";
                                SqlTran.Rollback();
                                return false;
                            }
                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, CenterSql, null) < 0)
                            {
                                msg = "更新集中数据库数据失败";
                                SqlTran.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, UpdatePackingModuleClass, null) < 0)
                            {
                                msg = "更新包装系统组件等级失败";
                                SqlTran.Rollback();
                                return false;
                            }

                            SqlTran.Commit();
                        }
                        catch (Exception ex)
                        {
                            msg = "打包发生异常 " + ex.Message + "";
                            SqlTran.Rollback();
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return false;
            }
        }
        /// <summary>
        /// 通过托号获取组件数量
        /// </summary>
        /// <param name="carton"></param>
        /// <returns></returns>
        public static int GetBoxIDSNQty(string carton)
        {
            try
            {
                int snqty = 0;
                string sql = @"SELECT COUNT(*) AS SNQTY FROM [Product] where BoxID='{0}'";
                sql = string.Format(sql, carton);
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    snqty = Convert.ToInt16(row["SNQTY"]);

                }
                return snqty;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// 更新数据库SQL
        /// </summary>
        /// <param name="carton"></param>
        /// <param name="wo"></param>
        /// <returns></returns>
        public static bool updatepackingdata(string carton, string wo, string joind, string posttime)
        {
            #region 更新每个托中工单的数量
            int StorageQty = 0;
            string stringInterfaceWo = "";
            try
            {
                int snqty = ProductStorageDAL.GetBoxIDSNQty(carton);
                if (snqty > 0)
                {
                    DataTable dt = ProductStorageDAL.GetWOStorageQty(wo);
                    int WOStorageQty = 0;//已入库数量
                    if ((dt != null) && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        WOStorageQty = Convert.ToInt32(row["RESV07"]);//工单已入库数量
                    }
                    int qty = snqty + WOStorageQty;
                    stringInterfaceWo = @"UPDATE {0}.[dbo].[T_WORK_ORDERS] SET RESV07='{1}' WHERE TWO_NO = '{2}';";
                    stringInterfaceWo = string.Format(stringInterfaceWo, FormCover.SAPDBName, qty, wo);
                }
                StorageQty = snqty;
            }
            catch
            {
                return false;
            }

            #endregion

            #region
            string sql = @"update {0}.dbo.Product set [StorageDate] = getdate(),[StorageOperator] ='{1}',
                                 [Flag]='2',[WorkOrder]='{2}' where boxid='{3}' and flag in ('1','5') ";
            string sql1 = @"update {0}.dbo.box set [StorageDate] = getdate(),[StorageOperator] ='{1}',flag='2' where boxid='{2}' and flag in ('1','5') ";

            string sql3 = @"UPDATE {0}.dbo.T_MODULE SET WORK_ORDER='{1}'
                              WHERE MODULE_SN IN(
                              SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                              {0}.dbo.T_MODULE modules with(nolock)
                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                              and list.MODULE_SYSID = modules.SYSID
                              and cartons.RESV03 = '{2}');
                              UPDATE {0}.[dbo].[T_MODULE_CARTON] SET RESV04 = '{3}' Where RESV03 = '{2}'";

            sql = string.Format(sql, FormCover.PackingDBName, FormCover.currUserName, wo, carton);

            sql1 = string.Format(sql1, FormCover.PackingDBName, FormCover.currUserName, carton);

            sql3 = string.Format(sql3, FormCover.CenterDBName, wo, carton, FormCover.CurrentFactory.Trim().ToUpper());
            #endregion


            List<MODULEPACKINGTRANSACTION> _ListModulePacking = new List<MODULEPACKINGTRANSACTION>();
            List<CARTONPACKINGTRANSACTION> _ListCartonPacking = new List<CARTONPACKINGTRANSACTION>();
            //SaveStorageDataForReport(carton,"Invertory",_ListModulePacking,_ListCartonPacking,joind,posttime);

            using (SqlConnection SqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                SqlConnection.Open();
                using (SqlTransaction SqlTransaction = SqlConnection.BeginTransaction())
                {
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        try
                        {
                            SqlCommand.Connection = SqlConnection;
                            SqlCommand.Transaction = SqlTransaction;

                            if (StorageQty > 0 && (!stringInterfaceWo.Equals("")))
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, stringInterfaceWo) < 1)
                                {
                                    SqlTransaction.Rollback();
                                    return false;
                                }
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql1) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql3) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }
                            SaveStorageDataForReport(carton, "Invertory", _ListModulePacking, _ListCartonPacking, joind, posttime);
                            string msg = "";
                            if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTransaction, _ListCartonPacking, _ListModulePacking, out msg))
                            {
                                // ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                SqlTransaction.Rollback();
                                return false;
                            }

                            SqlTransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            SqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 检查工单入库数量是否大于工单实际投产量-报废量-已入库数量
        /// </summary>
        /// <param name="_ListSapWo">传工单入库参数</param>
        /// <param name="msg">返回参数</param>
        public static bool CheckWoStorage(List<SapWoModule> _ListSapWo, out string msg)
        {
            msg = "";
            var list = ProductStorageDAL.GetEveryWOStorageTotalQty(_ListSapWo);
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    DataTable dt = ProductStorageDAL.GetWOStorageQty(list[i][0]);
                    if ((dt != null) && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        int WOTotalQty = Convert.ToInt32(row["TWO_WO_QTY"]);//工单总数量
                        int WOInputQty = Convert.ToInt32(row["RESV08"]);//工单投入数量
                        int WOStorageQty = Convert.ToInt32(row["RESV07"]);//工单已入库数量
                        int ScrapQty = Convert.ToInt32(row["RESV09"]);//工单报废数量
                        int WOWaitQty = Convert.ToInt32(list[i][1]);//工单待入库数量
                        if ((WOStorageQty + WOWaitQty + ScrapQty) > WOInputQty)
                        {
                            msg = "Production order " + list[i][0] + "with stocked in Qty(" + WOStorageQty + "), pending stocking Qty (" + WOWaitQty + ") and scrap Qty" + ScrapQty + " is more than total production order yield(" + WOInputQty + ")，please confirm！";
                            return false;
                        }
                    }
                    else
                    {
                        msg = "production order " + list[i][0] + " wasn't download from SAP！";
                        return false;
                    }
                }
            }
            else
            {
                msg = "No data found!";
                return false;
            }
            return true;
        }
        /// <summary>
        /// 返回工单已入库数量
        /// </summary>
        /// <param name="wo"></param>
        /// <returns></returns>
        public static DataTable GetWOStorageQty(string wo)
        {
            string sql = @"SELECT * FROM [T_WORK_ORDERS] with(nolock) 
                           WHERE TWO_NO='{0}'";
            sql = string.Format(sql, wo);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }
        /// <summary>
        /// 统计工单待入库的数量
        /// </summary>
        /// <param name="_ListSapWo"></param>
        /// <returns></returns>
        public static List<List<string>> GetEveryWOStorageTotalQty(List<SapWoModule> _ListSapWo)
        {
            try
            {
                string OrderNo = "";
                int OrderQty = 0;
                //统计工单和工单数量
                List<List<string>> OrderNoList = new List<List<string>>();//统计工单和工单数量
                //统计工单的种类
                List<string> OrderNoS = new List<string>();
                foreach (SapWoModule wo in _ListSapWo)
                {
                    if (!OrderNoS.Contains(wo.OrderNo))
                    {
                        if (OrderNo.Equals(""))
                            OrderNo = wo.OrderNo;

                        List<string> OrderNoQty = new List<string>();//计算工单中组件数量
                        foreach (SapWoModule woChild in _ListSapWo)
                        {
                            if (OrderNo == woChild.OrderNo)
                                OrderQty = OrderQty + 1;
                        }

                        if (!OrderNoQty.Contains(OrderNo))
                        {
                            OrderNoQty.Add(OrderNo);
                            OrderNoQty.Add(Convert.ToString(OrderQty));
                            OrderNoList.Add(OrderNoQty);
                        }

                        OrderNoS.Add(OrderNo);

                        OrderNo = "";
                        OrderQty = 0;
                    }
                }
                return OrderNoList;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 统计工单待入库的数量
        /// </summary>
        /// <param name="_ListSapWo"></param>
        /// <returns></returns>
        public static List<List<string>> GetEveryWOStorageTotalQty(DataTable _ListSapWo)
        {
            try
            {
                string OrderNo = "";
                int OrderQty = 0;
                //统计工单和工单数量
                List<List<string>> OrderNoList = new List<List<string>>();//统计工单和工单数量
                //统计工单的种类
                List<string> OrderNoS = new List<string>();
                foreach (DataRow wo in _ListSapWo.Rows)
                {
                    if (!OrderNoS.Contains(wo["WorkOrder"]))
                    {
                        if (OrderNo.Equals(""))
                            OrderNo = Convert.ToString(wo["WorkOrder"]);

                        List<string> OrderNoQty = new List<string>();//计算工单中组件数量
                        foreach (DataRow woChild in _ListSapWo.Rows)
                        {
                            if (OrderNo == Convert.ToString(woChild["WorkOrder"]).Trim())
                                OrderQty = OrderQty + 1;
                        }

                        if (!OrderNoQty.Contains(OrderNo))
                        {
                            OrderNoQty.Add(OrderNo);
                            OrderNoQty.Add(Convert.ToString(OrderQty));
                            OrderNoList.Add(OrderNoQty);
                        }

                        OrderNoS.Add(OrderNo);

                        OrderNo = "";
                        OrderQty = 0;
                    }
                }
                return OrderNoList;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 获取更新工单入库数量的SQL
        /// </summary>
        /// <param name="_ListSapWo"></param>
        /// <returns></returns>
        public static string GetUpdateWoStorageQtySql(List<SapWoModule> _ListSapWo)
        {
            try
            {
                string sqlInterfaceWo = "";
                var OrderNoList = ProductStorageDAL.GetEveryWOStorageTotalQty(_ListSapWo);
                if (OrderNoList.Count > 0)
                {
                    for (int i = 0; i < OrderNoList.Count; i++)
                    {
                        DataTable dt = ProductStorageDAL.GetWOStorageQty(OrderNoList[i][0]);
                        int WOStorageQty = 0;
                        if ((dt != null) && dt.Rows.Count > 0)
                        {
                            DataRow row = dt.Rows[0];
                            WOStorageQty = Convert.ToInt32(row["RESV07"]) + Convert.ToInt32(OrderNoList[i][1]);//工单已入库数量
                        }
                        string stringInterfaceWo = @"UPDATE {0}.[dbo].[T_WORK_ORDERS] SET RESV07='{1}' WHERE TWO_NO = '{2}';";
                        stringInterfaceWo = string.Format(stringInterfaceWo, FormCover.SAPDBName, WOStorageQty, OrderNoList[i][0]);
                        sqlInterfaceWo += stringInterfaceWo;
                    }
                }
                return sqlInterfaceWo;
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// 更新数据库，传入的LIST必须是一个托的
        /// </summary>
        /// <param name="_ListSapWo"></param>
        /// <returns></returns>
        public static bool updatepackingdatabySn(List<SapWoModule> _ListSapWo, string joinid, string posttime)
        {
            //更新每个托中工单的数量
            string sqlInterfaceWo = "";
            if (_ListSapWo.Count > 0)
                sqlInterfaceWo = ProductStorageDAL.GetUpdateWoStorageQtySql(_ListSapWo);

            #region 拼更新数据库的SQL
            string sql = "";
            string sql3 = "";
            string carton = "";
            foreach (SapWoModule wo in _ListSapWo)
            {
                if (carton.Equals(""))
                    carton = wo.CartonNo;

                string stringsql = @"update {0}.dbo.Product set [StorageDate] = getdate(),[StorageOperator] ='{1}',
                                 [Flag]='2',[WorkOrder]='{2}' where SN='{3}' and flag in ('1','5');";
                stringsql = string.Format(stringsql, FormCover.PackingDBName, FormCover.currUserName, wo.OrderNo, wo.ModuleSN);
                sql += stringsql;

                string stringsql3 = @"UPDATE {0}.dbo.T_MODULE SET WORK_ORDER='{1}'
                              WHERE MODULE_SN IN(
                              SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                              {0}.dbo.T_MODULE modules with(nolock)
                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                              and list.MODULE_SYSID = modules.SYSID
                              and cartons.RESV03 = '{2}'
                              and modules.MODULE_SN ='{3}');
                              UPDATE {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON='{4}'
                              WHERE MODULE_SN='{3}';
                              UPDATE {0}.[dbo].[T_MODULE_CARTON] SET RESV04 = '{5}' Where RESV03 = '{2}'";
                stringsql3 = string.Format(stringsql3, FormCover.CenterDBName, wo.OrderNo, carton, wo.ModuleSN, wo.PostedOn, FormCover.CurrentFactory.Trim().ToUpper());
                sql3 += stringsql3;

            }
            string sql1 = @"update {0}.dbo.box set [StorageDate] = getdate(),[StorageOperator] ='{1}',flag='2' where boxid='{2}' and flag in ('1','5') ";
            sql1 = string.Format(sql1, FormCover.PackingDBName, FormCover.currUserName, carton);
            #endregion

            List<MODULEPACKINGTRANSACTION> _ListModulePacking = new List<MODULEPACKINGTRANSACTION>();
            List<CARTONPACKINGTRANSACTION> _ListCartonPacking = new List<CARTONPACKINGTRANSACTION>();
            //SaveStorageDataForReport(carton, "Invertory", _ListModulePacking, _ListCartonPacking,joinid,posttime);

            #region 更新数据库
            using (SqlConnection SqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                SqlConnection.Open();
                using (SqlTransaction SqlTransaction = SqlConnection.BeginTransaction())
                {
                    using (SqlCommand SqlCommand = new SqlCommand())
                    {
                        try
                        {
                            SqlCommand.Connection = SqlConnection;
                            SqlCommand.Transaction = SqlTransaction;
                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql1) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sql3) < 1)
                            {
                                SqlTransaction.Rollback();
                                return false;
                            }

                            if (!sqlInterfaceWo.Equals(""))
                            {
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTransaction, CommandType.Text, sqlInterfaceWo) < 1)
                                {
                                    SqlTransaction.Rollback();
                                    return false;
                                }
                            }
                            SaveStorageDataForReport(carton, "Invertory", _ListModulePacking, _ListCartonPacking, joinid, posttime);
                            string msg = "";
                            if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTransaction, _ListCartonPacking, _ListModulePacking, out msg))
                            {
                                // ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                SqlTransaction.Rollback();
                                return false;
                            }

                            SqlTransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            SqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            #endregion
        }

        public static bool updatepackingdatabySn(List<TsapReceiptUploadModule> tsapReceiptUploadModules, List<SapWoModule> listSapWo, string joinid, string posttime)
        {
            //更新每个托中工单的数量
            var sqlInterfaceWo = string.Empty;
            if (listSapWo.Count > 0)
                sqlInterfaceWo = ProductStorageDAL.GetUpdateWoStorageQtySql(listSapWo);

            #region 拼更新数据库的SQL
            string sql = "";
            string sql3 = "";
            string carton = "";
            foreach (SapWoModule wo in listSapWo)
            {
                if (carton.Equals(""))
                    carton = wo.CartonNo;

                string stringsql = @"update {0}.dbo.Product set [StorageDate] = getdate(),[StorageOperator] ='{1}',
                                 [Flag]='2',[WorkOrder]='{2}' where SN='{3}' and flag in ('1','5');";
                stringsql = string.Format(stringsql, FormCover.PackingDBName, FormCover.currUserName, wo.OrderNo, wo.ModuleSN);
                sql += stringsql;

                string stringsql3 = @"UPDATE {0}.dbo.T_MODULE SET WORK_ORDER='{1}'
                              WHERE MODULE_SN IN(
                              SELECT DISTINCT modules.MODULE_SN FROM {0}.dbo.T_MODULE_CARTON cartons with(nolock),
                              {0}.dbo.T_MODULE_PACKING_LIST list with(nolock),
                              {0}.dbo.T_MODULE modules with(nolock)
                              where cartons.SYSID = list.MODULE_CARTON_SYSID
                              and list.MODULE_SYSID = modules.SYSID
                              and cartons.RESV03 = '{2}'
                              and modules.MODULE_SN ='{3}');
                              UPDATE {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON='{4}'
                              WHERE MODULE_SN='{3}';
                              UPDATE {0}.[dbo].[T_MODULE_CARTON] SET RESV04 = '{5}' Where RESV03 = '{2}'";
                stringsql3 = string.Format(stringsql3, FormCover.CenterDBName, wo.OrderNo, carton, wo.ModuleSN, wo.PostedOn, FormCover.CurrentFactory.Trim().ToUpper());
                sql3 += stringsql3;

            }
            string sql1 = @"update {0}.dbo.box set [StorageDate] = getdate(),[StorageOperator] ='{1}',flag='2' where boxid='{2}' and flag in ('1','5') ";
            sql1 = string.Format(sql1, FormCover.PackingDBName, FormCover.currUserName, carton);
            #endregion

            var listModulePacking = new List<MODULEPACKINGTRANSACTION>();
            var listCartonPacking = new List<CARTONPACKINGTRANSACTION>();

            #region 更新数据库
            using (var sqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                sqlConnection.Open();
                using (var sqlTransaction = sqlConnection.BeginTransaction())
                {
                    foreach (var tsapReceiptUploadModule in tsapReceiptUploadModules)
                    {
                        if (!DataAccess.InsertTsapReceiptUploadModule(tsapReceiptUploadModule, sqlConnection, sqlTransaction))
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                        if (!DataAccess.InsertTsapReceiptUploadModule(FormCover.SAPDBName, tsapReceiptUploadModule, sqlConnection, sqlTransaction))
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                    }
                    using (var sqlCommand = new SqlCommand())
                    {
                        try
                        {
                            sqlCommand.Connection = sqlConnection;
                            sqlCommand.Transaction = sqlTransaction;
                            if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTransaction, CommandType.Text, sql) < 1)
                            {
                                sqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTransaction, CommandType.Text, sql1) < 1)
                            {
                                sqlTransaction.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTransaction, CommandType.Text, sql3) < 1)
                            {
                                sqlTransaction.Rollback();
                                return false;
                            }

                            if (!sqlInterfaceWo.Equals(""))
                            {
                                if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTransaction, CommandType.Text, sqlInterfaceWo) < 1)
                                {
                                    sqlTransaction.Rollback();
                                    return false;
                                }
                            }
                            /*
                            //保存保险数据放到入库上传的时候做
                            SaveStorageDataForReport(carton, "Invertory", listModulePacking, listCartonPacking, joinid, posttime);
                            string msg;
                            if (!new CartonPackingTransactionDal().SaveStorage(sqlConnection, sqlTransaction, listCartonPacking, listModulePacking, out msg))
                            {
                                // ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                sqlTransaction.Rollback();
                                return false;
                            }
                            */

                            sqlTransaction.Commit();
                            return true;
                        }
                        catch
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            #endregion
        }

        public static bool SaveUploadEsbResult(TsapReceiptUploadJobNo tsapReceiptUploadJobNo, List<TsapReceiptUploadModule> tsapReceiptUploadModules, string joinid, string posttime)
        {
            var listModulePacking = new List<MODULEPACKINGTRANSACTION>();
            var listCartonPacking = new List<CARTONPACKINGTRANSACTION>();

            #region 更新数据库
            using (var sqlConnection = new SqlConnection(FormCover.connectionBase))
            {
                sqlConnection.Open();
                using (var sqlTransaction = sqlConnection.BeginTransaction())
                {
                    if (!DataAccess.InsertTsapReceiptUploadJobNo(tsapReceiptUploadJobNo, sqlConnection, sqlTransaction))
                    {
                        sqlTransaction.Rollback();
                        return false;
                    }
                    foreach (var tsapReceiptUploadModule in tsapReceiptUploadModules)
                    {
                        if (!DataAccess.UpdateTsapReceiptUploadModule(tsapReceiptUploadModule, sqlConnection, sqlTransaction))
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                    }
                    try
                    {
                        var cartonNos = tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
                        foreach (var cartonNo in cartonNos)
                        {
                            SaveStorageDataForReport(cartonNo, "Invertory", listModulePacking, listCartonPacking, joinid, posttime);
                            string msg;
                            if (!new CartonPackingTransactionDal().SaveStorage(sqlConnection, sqlTransaction, listCartonPacking, listModulePacking, out msg))
                            {
                                ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                                sqlTransaction.Rollback();
                                return false;
                            }
                        }

                        sqlTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        sqlTransaction.Rollback();
                        return false;
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// 保存入库数据到接口数据库
        /// </summary>
        /// <param name="_ListSapWo"></param>
        public static void SaveStorageInfoLog(List<SapWoModule> _ListSapWo)
        {
            try
            {
                string NowDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                if (_ListSapWo.Count > 0)
                {
                    using (SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString))
                    {
                        sqlConne.Open();
                        using (SqlTransaction sqlTrasn = sqlConne.BeginTransaction())
                        {
                            try
                            {
                                foreach (SapWoModule test in _ListSapWo)
                                {
                                    AddOneStorageInfo(sqlConne, sqlTrasn, test, NowDateTime);
                                }
                                sqlTrasn.Commit();
                            }
                            catch (Exception ex)
                            {
                                sqlTrasn.Rollback();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// 保存入库数据
        public static int AddOneStorageInfo(SqlConnection SqlConn, SqlTransaction SqlTrans, SapWoModule model, string NowDateTime)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into T_STORAGE_LOG(");
                strSql.Append("[ActionCode],[SysId],[OrderNo],[ProductCode],[PostedOn],[FinishedOn],[Unit],[Factory],[Workshop],");
                strSql.Append("[PackingLocation],[CellEff],[CellCode],[CellBatch],[ModuleSN],[CartonNo],[TestPower],[StdPower],[OrderStatus],[PostKey],");
                strSql.Append("[ModuleGrade],[ByIm],[CellPrintMode],[GlassCode],");
                strSql.Append("[GlassBatch],[EvaCode],[EvaBatch],[TptCode],[TptBatch],");
                strSql.Append("[ConBoxCode],[ConBoxBatch],[LongFrameCode],[LongFrameBatch],[SalesItemNo],");
                strSql.Append("[SalesOrderNo],[ShortFrameBatch],[ShortFrameCode],[PackingMode],[GlassThickness],");
                strSql.Append("[IsCancelPacking],[IsOnlyPacking],[CustomerCartonNo],[InnerJobNo],");
                strSql.Append("[Operator],[OperatorDate],[RESV01])");
                strSql.Append("values (");
                strSql.Append("@ActionCode,@SysId,@OrderNo,@ProductCode,@PostedOn,@FinishedOn,@Unit,@Factory,@Workshop,");
                strSql.Append("@PackingLocation,@CellEff,@CellCode,@CellBatch,@ModuleSN,@CartonNo,@TestPower,@StdPower,@OrderStatus,@PostKey,");
                strSql.Append("@ModuleGrade,@ByIm,@CellPrintMode,@GlassCode,");
                strSql.Append("@GlassBatch,@EvaCode,@EvaBatch,@TptCode,@TptBatch,");
                strSql.Append("@ConBoxCode,@ConBoxBatch,@LongFrameCode,@LongFrameBatch,@SalesItemNo,");
                strSql.Append("@SalesOrderNo,@ShortFrameBatch,@ShortFrameCode,@PackingMode,@GlassThickness,");
                strSql.Append("@IsCancelPacking,@IsOnlyPacking,@CustomerCartonNo,@InnerJobNo,");
                strSql.Append("@Operator,@OperatorDate,@RESV01)");
                SqlParameter[] parameters = {
                    new SqlParameter("@ActionCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@SysId", SqlDbType.NVarChar,100),
                    new SqlParameter("@OrderNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@ProductCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@PostedOn", SqlDbType.NVarChar,100),
                    new SqlParameter("@FinishedOn", SqlDbType.NVarChar,100),
                    new SqlParameter("@Unit", SqlDbType.NVarChar,100),
                    new SqlParameter("@Factory", SqlDbType.NVarChar,100),
                    new SqlParameter("@Workshop", SqlDbType.NVarChar,100),
                    new SqlParameter("@PackingLocation", SqlDbType.NVarChar,100),
                    new SqlParameter("@CellEff", SqlDbType.NVarChar,100),
                    new SqlParameter("@CellCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@CellBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@ModuleSN", SqlDbType.NVarChar,100),
                    new SqlParameter("@CartonNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@TestPower", SqlDbType.NVarChar,100),
                    new SqlParameter("@StdPower", SqlDbType.NVarChar,100),
                    new SqlParameter("@OrderStatus", SqlDbType.NVarChar,100),
                    new SqlParameter("@PostKey", SqlDbType.NVarChar,100),
                    new SqlParameter("@ModuleGrade", SqlDbType.NVarChar,100),
                    new SqlParameter("@ByIm", SqlDbType.NVarChar,100),
                    new SqlParameter("@CellPrintMode", SqlDbType.NVarChar,100),
                    new SqlParameter("@GlassCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@GlassBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@EvaCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@EvaBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@TptCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@TptBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@ConBoxCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@ConBoxBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@LongFrameCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@LongFrameBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@SalesItemNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@SalesOrderNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@ShortFrameBatch", SqlDbType.NVarChar,100),
                    new SqlParameter("@ShortFrameCode", SqlDbType.NVarChar,100),
                    new SqlParameter("@PackingMode", SqlDbType.NVarChar,100),
                    new SqlParameter("@GlassThickness", SqlDbType.NVarChar,100),
                    new SqlParameter("@IsCancelPacking", SqlDbType.NVarChar,100),
                    new SqlParameter("@IsOnlyPacking", SqlDbType.NVarChar,100),
                    new SqlParameter("@CustomerCartonNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@InnerJobNo", SqlDbType.NVarChar,100),
                    new SqlParameter("@Operator", SqlDbType.NVarChar,100),
                    new SqlParameter("@OperatorDate",SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV01", SqlDbType.NVarChar,100)};
                parameters[0].Value = model.ActionCode;
                parameters[1].Value = model.SysId;
                parameters[2].Value = model.OrderNo;
                parameters[3].Value = model.ProductCode;
                parameters[4].Value = model.PostedOn;
                parameters[5].Value = model.FinishedOn;
                parameters[6].Value = model.Unit;
                parameters[7].Value = model.Factory;
                parameters[8].Value = model.Workshop;
                parameters[9].Value = model.PackingLocation;
                parameters[10].Value = model.CellEff;
                parameters[11].Value = model.CellCode;
                parameters[12].Value = model.CellBatch;
                parameters[13].Value = model.ModuleSN;
                parameters[14].Value = model.CartonNo;
                parameters[15].Value = model.TestPower;
                parameters[16].Value = model.StdPower;
                parameters[17].Value = model.OrderStatus;
                parameters[18].Value = model.PostKey;
                parameters[19].Value = model.ModuleGrade;
                parameters[20].Value = model.ByIm;
                parameters[21].Value = model.CellPrintMode;

                parameters[22].Value = model.GlassCode;
                parameters[23].Value = model.GlassBatch;
                parameters[24].Value = model.EvaCode;
                parameters[25].Value = model.EvaBatch;
                parameters[26].Value = model.TptCode;
                parameters[27].Value = model.TptBatch;
                parameters[28].Value = model.ConBoxCode;
                parameters[29].Value = model.ConBoxBatch;
                parameters[30].Value = model.LongFrameCode;
                parameters[31].Value = model.LongFrameBatch;
                parameters[32].Value = model.SalesItemNo;
                parameters[33].Value = model.SalesOrderNo;
                parameters[34].Value = model.ShortFrameBatch;
                parameters[35].Value = model.ShortFrameCode;
                parameters[36].Value = model.PackingMode;
                parameters[37].Value = model.GlassThickness;
                parameters[38].Value = model.IsCancelPacking;
                parameters[39].Value = model.IsOnlyPacking;
                parameters[40].Value = model.CustomerCartonNo;
                parameters[41].Value = model.InnerJobNo;
                parameters[42].Value = FormCover.currUserName;
                parameters[43].Value = NowDateTime;
                parameters[44].Value = model.RESV01;
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, strSql.ToString(), parameters);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// 入库数据log记录
        /// </summary>
        /// <param name="QueryType">查询类型</param>
        /// <param name="carton">内部托号</param>
        /// <param name="StorageDateFrom">入库数据上传开始日期</param>
        /// <param name="StorageDateTo">入库数据上传结束日期</param>
        /// <param name="wo">工单号</param>
        /// <returns></returns>
        public static DataTable GetStorageInfo(string QueryType, string carton, DateTime StorageDateFrom, DateTime StorageDateTo, string wo)
        {
            carton = carton + "%";
            wo = wo + "%";
            StorageDateTo = StorageDateTo.AddDays(1);
            string sql = @"SELECT DISTINCT 
                                   [OrderNo]
                                  ,[PostedOn]
                                  ,[Workshop]
                                  ,[CartonNo]
                                  ,[CustomerCartonNo]
                                  ,[IsCancelPacking]
                                  ,[Operator]
                                  ,[OperatorDate] FROM [T_STORAGE_LOG] with(nolock) ";
            if (QueryType.Equals("Wo"))
                sql += @"where OrderNo like '" + wo + "' and workshop='" + FormCover.CurrentFactory + "' order by PostedOn desc ";
            else if (QueryType.Equals("Carton"))
                sql += @"where CartonNo like '" + carton + "'  and workshop='" + FormCover.CurrentFactory + "' order by PostedOn desc";
            else
                sql += @"where workshop='" + FormCover.CurrentFactory + "' and PostedOn >'" + StorageDateFrom.ToString("yyyy-MM-dd 00:00:00") + "' and PostedOn <'" + StorageDateTo.ToString("yyyy-MM-dd 23:59:59") + "'  order by PostedOn desc";
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }


        public static DataTable GetStorageDetailInfo(string OrderNo, string CartonNo, string IsCancelPacking,
                   string CustomerCartonNo, string Workshop, string PostedOn, string Operator, string OperatorDate)
        {
            string sql = @"SELECT [OrderNo] AS '工单'
                                  ,[CartonNo] AS '内部托号'
                                  ,[ProductCode] AS '产品物料代码'
                                  ,[ModuleSN] AS '组件序列号'
                                  ,[CellEff] AS '电池片转换效率'
                                  ,[CellCode] AS '电池片物料号'
                                  ,[CellBatch] AS '电池片批次'
                                  ,[CellPrintMode] AS '电池片网版'
                                  ,[GlassCode] AS '玻璃物料号'
                                  ,[GlassBatch] AS '玻璃批次'
                                  ,[EvaCode] AS 'EVA物料号'
                                  ,[EvaBatch] AS 'EVA批次'
                                  ,[TptCode] AS '背板物料号'
                                  ,[TptBatch] AS '背板批次'
                                  ,[ConBoxCode] AS '接线盒物料号'
                                  ,[ConBoxBatch] AS '接线盒批次'
                                  ,[LongFrameCode] AS '长边框物料号'
                                  ,[LongFrameBatch] AS '长边框批次'
                                  ,[ShortFrameCode] AS '短边框物料号'
                                  ,[ShortFrameBatch] AS '短边框批次号'
                                  ,[PackingMode] AS '包装方式'
                                  ,[GlassThickness] AS '玻璃厚度'
                     FROM [T_STORAGE_LOG] with(nolock) WHERE OrderNo ='{0}' 
                               AND CartonNo ='{1}' 
                               AND IsCancelPacking ='{2}'
                               AND  CustomerCartonNo ='{3}' 
                               AND Workshop ='{4}' 
                               AND PostedOn ='{5}'
                               AND Operator = '{6}'
                               AND OperatorDate ='{7}' ";
            sql = string.Format(sql, OrderNo, CartonNo, IsCancelPacking, CustomerCartonNo, Workshop, PostedOn, Operator, OperatorDate);
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetModuleClassInfo(string FunctionCode)
        {
            string sql = @"SELECT [SYSID]
                          ,[FUNCTION_CODE]
                          ,[MAPPING_KEY_01]
                          ,[MAPPING_KEY_02]
                          ,[MAPPING_KEY_03]
                          ,[MAPPING_KEY_04]
                          ,[MAPPING_KEY_05]
                          ,[MAPPING_KEY_06]
                          ,[MAPPING_KEY_07]
                          ,[MAPPING_KEY_08]
                          ,[MAPPING_KEY_09]
                          ,[CREATED_BY]
                          ,[CREATED_ON]
                          ,[MODIFIED_BY]
                          ,[MODIFIED_ON]
                          ,[STATUS]
                          ,[RESV01]
                          ,[RESV02]
                          ,[RESV03]
                          ,[RESV04]
                          ,[RESV05]
                          ,[RESV06]
                          ,[RESV07]
                          ,[RESV08]
                          ,[RESV09]
                          ,[RESV10]
                      FROM [T_SYS_MAPPING] with(nolock)
                              WHERE FUNCTION_CODE='ModuleClass' 
                          ";
            sql = string.Format(sql, FunctionCode.Trim());
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetStorageDetailLogInfo(string QueryType, string carton, DateTime StorageDateFrom, DateTime StorageDateTo, string wo)
        {
            carton = carton + "%";
            wo = wo + "%";
            StorageDateTo = StorageDateTo.AddDays(1);
            string sql = @"SELECT 
                          [OrderNo] AS '生产订单号'
                          ,[SalesOrderNo] AS '销售订单'
                          ,[SalesItemNo] AS '销售订单项目'
                          ,[ProductCode] AS '产品物料代码'
                          ,[FinishedOn] AS '组件包装完工日期'
                          ,[PostedOn] AS '上传日期'
                          ,[Unit] AS '单位'
                          ,[Factory] AS '工厂'
                          ,[Workshop] AS '车间'
                          ,[PackingLocation] AS '入库地点'
                          ,[CellEff] AS '电池片转换效率'
                          ,[ModuleSN] AS '组件序列号'
                          ,[CartonNo] AS '内部托号'
                          ,[CustomerCartonNo] AS '客户托号'
                          ,[TestPower] AS '实测功率'
                          ,[StdPower] AS '标称功率'
                          ,[OrderStatus] AS '生产订单状态'
                          ,[PostKey] AS '上传条目号码'
                          ,[ModuleGrade] AS '组件等级'
                          ,[ByIm] AS '电池是否分档'
                          ,[CellCode] AS '电池片物料号'
                          ,[CellBatch] AS '电池片批次'
                          ,[CellPrintMode] AS '电池片网版'
                          ,[GlassCode] AS '玻璃物料号'
                          ,[GlassBatch] AS '玻璃批次'
                          ,[EvaCode] AS 'EVA物料号'
                          ,[EvaBatch] AS 'EVA批次'
                          ,[TptCode] AS '背板物料号'
                          ,[TptBatch] AS '背板批次'
                          ,[ConBoxCode] AS '接线盒物料号'
                          ,[ConBoxBatch] AS '接线盒批次'
                          ,[LongFrameCode] AS '长铝边框物料号'
                          ,[LongFrameBatch] AS '长铝边框批次'
                          ,[ShortFrameCode] AS '短边框物料号'
                          ,[ShortFrameBatch] AS '短边框批次号'
                          ,[PackingMode] AS '包装方式'
                          ,[GlassThickness] AS '玻璃厚度'
                          ,[IsCancelPacking] AS '是否撤销入库'
                          ,[IsOnlyPacking] AS '是否是拼托'
                          ,[Operator] AS '操作者'
                          ,[OperatorDate] AS '操作时间' FROM [T_STORAGE_LOG] with(nolock) ";
            if (QueryType.Equals("Wo"))
                sql += @"where OrderNo like '" + wo + "' and workshop='" + FormCover.CurrentFactory + "' order by PostedOn desc ";
            else if (QueryType.Equals("Carton"))
                sql += @"where CartonNo like '" + carton + "'  and workshop='" + FormCover.CurrentFactory + "' order by PostedOn desc";
            else
                sql += @"where workshop='" + FormCover.CurrentFactory + "' and PostedOn >'" + StorageDateFrom.ToString("yyyy-MM-dd 00:00:00") + "' and PostedOn <'" + StorageDateTo.ToString("yyyy-MM-dd 23:59:59") + "'  order by PostedOn desc";
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetSapStorageDealResultInfo(string Carton, string QueryType, DateTime DateFrom, DateTime DateTo)
        {
            Carton = Carton + "%";
            DateTo = DateTo.AddDays(1);
            string FactoryCode = "";
            if ((FormCover.CurrentFactory.Trim().ToUpper().Equals("M01")) || (FormCover.CurrentFactory.Trim().ToUpper().Equals("M09")))
                FactoryCode = "('1101','1106')";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M02"))
                FactoryCode = "('1102')";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M03"))
                FactoryCode = "('1103')";
            else if (FormCover.CurrentFactory.Trim().ToUpper().Equals("M07"))
                FactoryCode = "('1107','1105')";

            string sql = @"SELECT 
                               [CartonNo]
                               ,[ProcessedResult]
                               ,[Remark]
                               ,[CreatedOn]
                           FROM [TSAP_PACKING_CARTON] with(nolock)";

            if (QueryType.Trim().ToUpper().Equals("CARTON"))
            {
                sql += @"WHERE  Resv02 in {0}  and CartonNo like '{1}'  order by CreatedOn desc";
                sql = string.Format(sql, FactoryCode, Carton);
            }
            else if (QueryType.Trim().ToUpper().Equals("DATE"))
            {
                sql += @"WHERE  Resv02 in {0}  and CreatedOn >'" + DateFrom.ToString("yyyy-MM-dd 00:00:00") + "' and CreatedOn <'" + DateTo.ToString("yyyy-MM-dd 23:59:59") + "'  ORDER BY CreatedOn DESC";
                sql = string.Format(sql, FactoryCode);
            }

            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable GetCartonInfoFromCenterDB(string CartonNo)
        {
            string sql = @"SELECT *
                            FROM [T_MODULE_CARTON] carton with(nolock),
                                 [T_MODULE_PACKING_LIST] list with(nolock),
                                 [T_MODULE] module with(nolock),
                                 [T_MODULE_TEST] test with(nolock)
                            WHERE carton.RESV03='{0}' 
                                  and carton.SYSID = list.MODULE_CARTON_SYSID
                                  and module.SYSID = list.MODULE_SYSID
                                  and test.MODULE_SN= module.MODULE_SN";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }

        public static DataTable GetWoInfoFromSAPInFo(string QueryType, string wo, DateTime WoDateFrom, DateTime WoDateTo)
        {
            wo = wo + "%";
            WoDateTo = WoDateTo.AddDays(1);
            string sql = @"SELECT DISTINCT 
                                   [TWO_NO]
                                  ,[TWO_ORDER_NO]
                                  ,[TWO_ORDER_ITEM]
                                  ,[TWO_PRODUCT_NAME]
                                  ,[TWO_WO_QTY]
                                  ,[TWO_WO_ORIGINAL_QTY]     
                                  ,[TWO_PLANSTART_DATETIME]
                                  ,[TWO_PLANCOMPLETE_DATETIME]
                                  ,[TWO_WORKSHOP] 
                                  ,TWO_CREATE_ON
                                  FROM [T_WORK_ORDERS] with(nolock) ";
            if (QueryType.Trim().ToUpper().Equals("WO"))
                //sql += @"where TWO_NO like '" + wo + "' and TWO_WORKSHOP='" + FormCover.CurrentFactory.Trim().ToUpper() + "' ORDER BY TWO_CREATE_ON DESC ";
           sql += @"where TWO_NO like '" + wo + "'  ORDER BY TWO_CREATE_ON DESC ";
            else if (QueryType.Trim().ToUpper().Equals("WOCREATEDATE"))
                //sql += @"where TWO_WORKSHOP='" + FormCover.CurrentFactory.Trim().ToUpper() + "' and TWO_CREATE_ON >'" + WoDateFrom.ToString("yyyy-MM-dd 00:00:00") + "' and TWO_CREATE_ON <'" + WoDateTo.ToString("yyyy-MM-dd 23:59:59") + "'  ORDER BY TWO_CREATE_ON DESC";
            sql += @"where  TWO_CREATE_ON >'" + WoDateFrom.ToString("yyyy-MM-dd 00:00:00") + "' and TWO_CREATE_ON <'" + WoDateTo.ToString("yyyy-MM-dd 23:59:59") + "'  ORDER BY TWO_CREATE_ON DESC";
           
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        /// <summary>
        /// 读取包装数据库连接字符串
        /// </summary>
        /// <param name="workshop">车间代码</param>
        /// <returns></returns>
        public static ConfigString GetPackingSystemConfigInfo(string PlanCode, string Workshop)
        {
            try
            {
                string sql = @"SELECT * FROM T_CONNECTION_STRING WITH(NOLOCK) Where Factory='{0}' and Workshop='{1}' and ProgramName='PackingSystem' ";
                sql = string.Format(sql, PlanCode.Trim().ToUpper(), Workshop.Trim().ToUpper());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    ConfigString cfg = new ConfigString();
                    DataRow row = dt.Rows[0];
                    cfg.Workshop = Convert.ToString(row["Workshop"]);
                    cfg.DataSource = Convert.ToString(row["DataSource"]);
                    cfg.DatabaseName = Convert.ToString(row["DatabaseName"]);
                    cfg.LoginUserName = Convert.ToString(row["LoginUserName"]);
                    cfg.LoginPassword = Convert.ToString(row["LoginPassword"]);
                    cfg.DbLinkName = Convert.ToString(row["DbLinkName"]);
                    return cfg;
                }
                else
                {
                    MessageBox.Show("获取包装数据库连接字符串失败:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取包装数据库连接字符串时发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 读取接口数据库连接字符串
        /// </summary>
        /// <param name="workshop">车间代码</param>
        /// <returns></returns>
        public static ConfigString GetInterfaceConfigInfo(string PlanCode)
        {
            try
            {
                string sql = @"SELECT * FROM T_CONNECTION_STRING WITH(NOLOCK) Where  Factory='{0}' and  Workshop='Public' and ProgramName='SapInterface'  ";
                sql = string.Format(sql, PlanCode.Trim().ToUpper());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    ConfigString cfg = new ConfigString();
                    DataRow row = dt.Rows[0];
                    cfg.Workshop = Convert.ToString(row["Workshop"]);
                    cfg.DataSource = Convert.ToString(row["DataSource"]);
                    cfg.DatabaseName = Convert.ToString(row["DatabaseName"]);
                    cfg.LoginUserName = Convert.ToString(row["LoginUserName"]);
                    cfg.LoginPassword = Convert.ToString(row["LoginPassword"]);
                    cfg.DbLinkName = Convert.ToString(row["DbLinkName"]);
                    return cfg;
                }
                else
                {
                    MessageBox.Show("获取接口数据库连接字符串失败:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取接口数据库连接字符串时发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 读取集中数据库连接字符串
        /// </summary>
        /// <param name="workshop">车间代码</param>
        /// <returns></returns>
        public static ConfigString GetCenterDBConfigInfo(string PlanCode)
        {
            try
            {
                string sql = @"SELECT * FROM T_CONNECTION_STRING WITH(NOLOCK) Where  Factory='{0}' and  Workshop='Public' and ProgramName='CenterDb' ";
                sql = string.Format(sql, PlanCode.Trim().ToUpper());
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    ConfigString cfg = new ConfigString();
                    DataRow row = dt.Rows[0];
                    cfg.Workshop = Convert.ToString(row["Workshop"]);
                    cfg.DataSource = Convert.ToString(row["DataSource"]);
                    cfg.DatabaseName = Convert.ToString(row["DatabaseName"]);
                    cfg.LoginUserName = Convert.ToString(row["LoginUserName"]);
                    cfg.LoginPassword = Convert.ToString(row["LoginPassword"]);
                    cfg.DbLinkName = Convert.ToString(row["DbLinkName"]);
                    return cfg;
                }
                else
                {
                    MessageBox.Show("获取集中数据库连接字符串失败:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取集中数据库连接字符串时发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 读取ESB数据库连接字符串
        /// </summary>
        /// <param name="planCode"></param>
        /// <returns></returns>
        public static ConfigString GetEsbConfigInfo(string planCode)
        {
            try
            {
                var sql = @"SELECT * FROM T_CONNECTION_STRING WITH(NOLOCK) Where  Factory='{0}' and  Workshop='Public' and ProgramName='EsbDatabase' ";
                sql = string.Format(sql, planCode.Trim().ToUpper());
                var dt = GetDataTableData(FormCover.connectionBase, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    var cfg = new ConfigString();
                    var row = dt.Rows[0];
                    cfg.Workshop = Convert.ToString(row["Workshop"]);
                    cfg.DataSource = Convert.ToString(row["DataSource"]);
                    cfg.DatabaseName = Convert.ToString(row["DatabaseName"]);
                    cfg.LoginUserName = Convert.ToString(row["LoginUserName"]);
                    cfg.LoginPassword = Convert.ToString(row["LoginPassword"]);
                    cfg.DbLinkName = Convert.ToString(row["DbLinkName"]);
                    return cfg;
                }
                MessageBox.Show("获取ESB数据库连接字符串失败:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("获取ESB数据库连接字符串时发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 从配置表读取数据
        /// </summary>
        /// <param name="ControlName">控制名</param>
        /// <param name="Workshop">车间</param>
        /// <returns></returns>zz
        public static string GetSysMapping(string ControlName, string Workshop)
        {
            try
            {
                string sql = @"SELECT * FROM T_SYS_MAPPING WITH(NOLOCK) Where  FUNCTION_CODE='{0}' and MAPPING_KEY_02 = '{1}'";
                sql = string.Format(sql, ControlName.Trim(), Workshop.Trim().ToUpper());
                DataTable dt = GetDataTableData(FormCover.InterfaceConnString, sql);
                if ((dt != null) && (dt.Rows.Count == 1))
                {
                    DataRow row = dt.Rows[0];
                    if (!Convert.ToString(row["MAPPING_KEY_01"]).Trim().Equals(""))
                        return Convert.ToString(row["MAPPING_KEY_01"]).Trim();
                    else
                        return "";
                }
                else
                {
                    MessageBox.Show("从接口数据库获取配置表(T_SYS_MAPPING)数据失败:" + sql + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("从接口数据库获取配置表(T_SYS_MAPPING)数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return "";
            }
        }


        /// <summary>
        /// 从配置表读取数据(多车间)
        /// </summary>
        /// <param name="ControlName">控制名</param>
        /// <param name="Workshop">车间</param>
        /// <returns></returns>
        public static DataTable GetMultiWorkShopMapping(string ControlName, string Workshop)
        {
            try
            {
                string sql = @"SELECT * FROM T_SYS_MAPPING WITH(NOLOCK) Where  FUNCTION_CODE='{0}' and MAPPING_KEY_01 = '{1}'";
                sql = string.Format(sql, ControlName.Trim(), Workshop.Trim().ToUpper());
                return GetDataTableData(FormCover.connectionBase, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show("从接口数据库获取配置表(T_SYS_MAPPING)数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 从配置表读取数据(TOLERANCE)
        /// </summary>
        /// <param name="ControlName">控制名</param>
        /// <returns></returns>
        /// 

        public static DataTable GetValueFromInterface(string ControlName,string Factory)
        {
            try
            {
                string sql = @"SELECT MAPPING_KEY_01 as value FROM T_SYS_MAPPING WITH(NOLOCK) Where  FUNCTION_CODE='{0}' and  MAPPING_KEY_02 ='{1}'  order by MAPPING_KEY_01";
                sql = string.Format(sql, ControlName.Trim(),Factory);
                return GetDataTableData(FormCover.InterfaceConnString, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show("从接口数据库获取配置表(T_SYS_MAPPING)数据发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }

        /// <summary>
        /// 客户托号在集中数据库是否已经存在
        /// </summary>
        /// <param name="CartonNo"></param>
        /// <returns></returns>
        public static bool GetCustCartonInfoFromCenterDB(string CartonNo)
        {
            try
            {
                string sql = @"SELECT *
                           FROM [T_MODULE_CARTON] carton with(nolock)
                           WHERE carton.CARTON_NO='{0}'";
                sql = string.Format(sql, CartonNo);
                DataTable dt = GetDataTableData(FormCover.CenterDBConnString, sql);
                if (dt != null && dt.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
        /// <summary>
        /// 获取组件的测试数据
        /// </summary>
        /// <param name="SerialNumber">组件序列号</param>
        /// <returns></returns>
        public static DataTable GetFTData(string SerialNumber)
        {
            try
            {
                string sql = @"SELECT TOP 1 prd.SN,PRD.StdPower,prd.Pmax,test.* 
                               FROM [Product] prd with(nolock) ,
                                    [ElecParaTest] test with(nolock)
                               WHERE prd.InterID = (SELECT MAX(InterID) FROM [Product] WITH(NOLOCK) where SN='{0}')
                                     and prd.InterNewTempTableID = test.InterID
                                     and test.FNumber ='{0}'";
                sql = string.Format(sql, SerialNumber.Trim());
                return GetDataTableData(FormCover.connectionBase, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show("读取组件的测试信息发生异常:" + ex.Message + "", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }
        }




        #region
        /// <summary>
        /// 获取物料模板信息
        /// </summary>
        /// <param name="templateName">物料模板名称</param>
        /// <returns></returns>
        public static DataTable GetMaterialTemplateInfo(string templateName)
        {
            string sql = string.Empty;
            string currentWorkshop = string.Empty;

            switch (FormCover.CurrentFactory)
            {
                case "M09":
                    currentWorkshop = "M01";
                    break;
                case "M13":
                    currentWorkshop = "M07";
                    break;
                default:
                    currentWorkshop = FormCover.CurrentFactory;
                    break;
            }
            string line = ToolsClass.getConfig("Line");
            if(FormCover.HasPowerControl("Rework"))
            {
	            	
	            if (!templateName.Equals(""))
	            {
	                templateName = "%" + templateName + "%";
	                sql = @"SELECT top 10 [MaterialTemplateName],[Description],[SYSID]
	                        FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] with (nolock)
	                        WHERE Workshop = '{0}' and [MaterialTemplateName] LIKE '{1}' and orderno {2} like 'CC%' and OperatorDate between getdate()-7 and getdate() order by OperatorDate desc ";
	                if(line.ToUpper() == "C")
	               	 	sql = string.Format(sql, currentWorkshop, templateName,"");
	                else
	                	sql = string.Format(sql, currentWorkshop, templateName,"not");	
	            }
	            else
	            {
	                sql = @"SELECT [MaterialTemplateName],[Description],[SYSID]
	                        FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] with (nolock) 
	                        WHERE Workshop = '{0}' and orderno {1} like 'CC%'  and OperatorDate between getdate()-4 and getdate() order by OperatorDate desc ";
	                if(line.ToUpper() == "C")
	                	sql = string.Format(sql, currentWorkshop,"");
	                else
	                	sql = string.Format(sql, currentWorkshop,"not");
	            }
            }
            else
            {
	            if (!templateName.Equals(""))
	            {
	                templateName = "%" + templateName + "%";
	                sql = @"SELECT top 7 [MaterialTemplateName],[Description],[SYSID]
	                        FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] with (nolock)
	                        WHERE Workshop = '{0}' and resv05 is null and [MaterialTemplateName] LIKE '{1}' and orderno {2} like 'CC%' and OperatorDate between getdate()-7 and getdate() order by OperatorDate desc ";
	                if(line.ToUpper() == "C")
	               	 	sql = string.Format(sql, currentWorkshop, templateName,"");// London Jacky
	                else
	                	sql = string.Format(sql, currentWorkshop, templateName,"not");// Guelph	Jacky
	            }
	            else
	            {
	                sql = @"SELECT [MaterialTemplateName],[Description],[SYSID]
	                        FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] with (nolock) 
	                        WHERE Workshop = '{0}' and resv05 is null and orderno {1} like 'CC%'  and OperatorDate between getdate()-4 and getdate() order by OperatorDate desc ";
	                if(line.ToUpper() == "C")
	                	sql = string.Format(sql, currentWorkshop,"");
	                else
	                	sql = string.Format(sql, currentWorkshop,"not");
	            }
            }

            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }


        /// <summary>
        /// 根据工单获取Tolerance
        /// </summary>
        /// <param name="sysID"></param>
        /// <returns></returns>
        public static DataSet GetTolerancebyWorkOrder(string wrkorder)
        {
            string sqlStr = "";
  
                sqlStr = @"
            SELECT isnull(value18,'') as Tolerance
            FROM [csimes_SapInterface].[dbo].[T_WORKORDER_ATTRIBUTE]
             where order_no='{0}'";

            sqlStr = string.Format(sqlStr, wrkorder);
            return GetDataSetData(FormCover.InterfaceConnString, sqlStr);
        }

       
        /// <summary>
        /// 根据工单获取物料编码
        /// </summary>
        /// <param name="sysID"></param>
        /// <returns></returns>
        public static DataSet GetMaterialCodebyWorkOrder(string wrkorder)
        {
            string sqlStr = "";
            sqlStr = @"
                SELECT  distinct MaterialCode , MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] with (nolock)
                where (MaterialCategory='Cell' or MaterialGroup = '2000') and OrderNo ='{0}'
  
				SELECT  distinct MaterialCode , MaterialDescription
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] with (nolock)
                where( MaterialCategory='Glass' or MaterialGroup ='1001')  and OrderNo ='{0}'

                SELECT  distinct MaterialCode , MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] with (nolock)
                where (MaterialCategory='EVA' or  MaterialGroup ='1002') and OrderNo ='{0}'

                SELECT  distinct MaterialCode , MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] with (nolock)
                where (MaterialCategory='TPT' or MaterialGroup ='1003') and OrderNo ='{0}'

                SELECT  distinct MaterialCode , MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] with (nolock)
                where (MaterialCategory='CONBOX' or MaterialGroup ='1004')  and OrderNo ='{0}'

                SELECT  distinct bom.MaterialCode , bom.MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] bom with (nolock),
                     [csimes_SapInterface].[dbo].[TSAP_MATERIAL] material with(nolock)
                where (bom.MaterialCategory='AIFRAME' or bom.MaterialGroup ='1009')and bom.OrderNo ='{0}'
                      and bom.MaterialCode = material.MaterialCode
                      and material.FrameType='1'

                SELECT  distinct bom.MaterialCode , bom.MaterialDescription 
                FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] bom with (nolock),
                     [csimes_SapInterface].[dbo].[TSAP_MATERIAL] material with(nolock)
                where (bom.MaterialCategory='AIFRAME' or bom.MaterialGroup ='1009' )and bom.OrderNo ='{0}'
                      and bom.MaterialCode = material.MaterialCode
                      and material.FrameType='2'

	            SELECT isnull(value18,'') as Tolerance
	            FROM [csimes_SapInterface].[dbo].[T_WORKORDER_ATTRIBUTE]
	             where order_no='{0}'

				Select Top 1 [RESV01] as Market,[RESV02] as LID,
				 [RESV03] as CabelLength,[RESV04] as JboxType 
				from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] 
				 where OrderNo = '{0}' order by CreatedOn Desc "; //jacky20160203
            	
            	
            sqlStr = string.Format(sqlStr, wrkorder);
            return GetDataSetData(FormCover.InterfaceConnString, sqlStr);
        }
        
        
        
          

        /// <summary>
        /// 根据条件获取dataset
        /// </summary>
        /// <returns></returns>
        private static DataSet GetDataSetData(string SqlConnectString, string sql)
        {
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(SqlConnectString, CommandType.Text, sql);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 获取物料模板信息
        /// </summary>
        /// <param name="sysID">数据库sysID</param>
        /// <returns></returns>
        public static DataTable GetMaterialTemplateInfoBySySID(string sysID)
        {
            string sqlStr = "";
            sqlStr = @"SELECT  [SYSID]
                            ,[MaterialTemplateName]
                            ,isnull([Description],'') AS [Description]
                            ,isnull([OrderNo],'') AS [OrderNo]
                            ,isnull([Workshop],'') 
                            ,[ByIm] =case when [ByIm] is null then '' when [ByIm] = 'Y' then N'是' when [ByIm] ='N' then N'否'else [ByIm] end
                            ,isnull([CellEff],'')
                            ,[PackingMode] = case when [PackingMode] is null then '' when [PackingMode] = 'A' then N'横包装' when [PackingMode] ='B' then N'竖包装'	
	                            when [PackingMode] = 'C' then N'双件装' when [PackingMode] ='D' then N'单件装' when [PackingMode] ='E' then N'其它' else [PackingMode] end
                            ,isnull([GlassThickness],'')
                            ,isnull([CellCode],'')
                            ,isnull([CellBatch],'')
                            ,isnull([CellPrintMode],'')
                            ,isnull([GlassCode],'')
                            ,isnull([GlassBatch],'')
                            ,isnull([EvaCode],'')
                            ,isnull([EvaBatch],'')
                            ,isnull([TptCode],'')
                            ,isnull([TptBatch],'')
                            ,isnull([ConBoxCode],'')
                            ,isnull([ConBoxBatch],'')
                            ,isnull([LongFrameCode],'')
                            ,isnull([LongFrameBatch],'')
                            ,isnull([ShortFrameCode],'')
                            ,isnull([ShortFrameBatch],'')
                            ,[ModuleGrade] = case when [ModuleGrade] is null then '' when [ModuleGrade] = 'A' then N'A 等级' when [ModuleGrade] ='B' then N'B 等级'	
	                            when [ModuleGrade] = 'F' then N'废品' else [ModuleGrade] end
                            ,isnull([Operator],'')
                            ,isnull([OperatorDate],''),isnull([Tolerance],'')
                        FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] with (nolock)
                WHERE sysid = '" + sysID + "'";

            return GetDataTableData(FormCover.InterfaceConnString, sqlStr);
        }

        public static DataTable getSAPBatchbyMaterialCode(string WorkShop, string OrderNo, string MaterialCategory, string MaterialCode)
        {
            #region
            //SqlCommand sqlComm = null;
            //SqlConnection sqlCon = null;
            //SqlDataAdapter sda = null;
            //DataTable dt = null;

            ////if (WorkShop.ToUpper().Equals("M01"))
            ////    WorkShop = "('1101')";
            ////else if (WorkShop.ToUpper().Equals("M09"))
            ////    WorkShop = "('1106')";
            ////else if (WorkShop.ToUpper().Equals("M03"))
            ////    WorkShop = "('1103')";
            ////else if (WorkShop.ToUpper().Equals("M07"))
            ////    WorkShop = "('1105','1107')";
            ////else if (WorkShop.ToUpper().Equals("M02"))
            ////    WorkShop = "('1102','1107')";
            //try
            //{
            //    using (sqlCon = new SqlConnection(FormCover.InterfaceConnString))
            //    {
            //        sqlCon.Open();
            //        sqlComm = new SqlCommand("csimes_SapInterface.dbo.Proc_GetList_Pack_SAPBatch_TEST", sqlCon);
            //        //设置命令的类型为存储过程
            //        sqlComm.CommandType = CommandType.StoredProcedure;
            //        //设置参数
            //        sqlComm.Parameters.Add("@WorkShop", SqlDbType.VarChar);
            //        sqlComm.Parameters.Add("@OrderNo", SqlDbType.VarChar);
            //        sqlComm.Parameters.Add("@MaterialCategory", SqlDbType.VarChar);
            //        sqlComm.Parameters.Add("@MaterialCode", SqlDbType.VarChar);

            //        //为参数赋值
            //        sqlComm.Parameters["@WorkShop"].Value = WorkShop;
            //        sqlComm.Parameters["@OrderNo"].Value = OrderNo;
            //        sqlComm.Parameters["@MaterialCategory"].Value = MaterialCategory;
            //        sqlComm.Parameters["@MaterialCode"].Value = MaterialCode;

            //        sqlComm.ExecuteNonQuery();

            //        sda = new SqlDataAdapter(sqlComm);
            //        dt = new DataTable();
            //        sda.Fill(dt);
            //        return dt;
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}

            ////string sqlStr = "";
            ////sqlStr = @"exec csimes_SapInterface.dbo.Proc_GetList_Pack_SAPBatch '{0}','{1}','{2}','{3}'";
            ////sqlStr = string.Format(sqlStr, WorkShop, OrderNo, MaterialCategory, MaterialCode);
            ////return GetDataTableDatawithProcedure(FormCover.InterfaceConnString, sqlStr);
            #endregion

//            string sql = @"SELECT ' ' AS BATCH 
//	                       UNION	
//	                       SELECT DISTINCT RTRIM(LTRIM(BATCH ))
//	                       FROM csimes_SapInterface.dbo.T_PICKING WITH(NOLOCK)
//                           WHERE MaterialCode = '{0}'
//                                  AND OrderNo  = '{1}'
//                                  AND PickingType = '261'
//                                  AND (Batch IS NOT NULL OR Batch<>'')";  // jacky 20160120
//            
            string sql = @" SELECT DISTINCT RTRIM(LTRIM(BATCH ))  AS BATCH
	                       FROM csimes_SapInterface.dbo.T_PICKING WITH(NOLOCK)
                           WHERE MaterialCode = '{0}'
                                  AND OrderNo  = '{1}'
                                  AND PickingType = '261'
                                  AND (Batch IS NOT NULL OR Batch<>'')";
            sql = string.Format(sql, MaterialCode.Trim(), OrderNo.Trim());
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        public static DataTable getSAPBatchbyMaterialCodeInBom(string WorkShop, string OrderNo, string MaterialCategory, string MaterialCode)
        {
            string sql = @"SELECT LTRIM(RTRIM(ISNULL([Resv05],''))) BATCH
                              FROM [csimes_SapInterface].[dbo].[T_WORKORDER_BOM] NOLOCK 
                              WHERE WorkShop='{0}'
                              AND OrderNo='{1}'
                              AND MaterialCategory='{2}'
                              AND MaterialCode='{3}'
                            UNION SELECT ' ' AS BATCH ";
            sql = string.Format(sql, WorkShop.Trim(), OrderNo.Trim(), MaterialCategory.Trim(), MaterialCode.Trim());
            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }

        /// <summary>
        /// 更新物料模板
        /// <returns></returns>
        public static int UpdateMaterialTemplate(
            string TemplateName,
            string TemplateDesc,
            string WorkOrder,
            string WorkShop,
            string ByIm,
            string CellTransfer,
            string PackingPattern,
            string GlassLength,
            string CellCode,
            string CellBatch,
            string GlassCode,
            string GlassBatch,
            string EvaCode,
            string EvaBatch,
            string TptCode,
            string TptBatch,
            string ConBoxCode,
            string ConBoxBatch,
            string LongFrameCode,
            string LongFrameBatch,
            string ShortFrameCode,
            string ShortFrameBatch,
            string ModuleGrade,
            string Tolerance,
            string Market,
            string LID,
            string CabelLength,
            string JBoxType            
            )
        {

            string strSql = @"UPDATE [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE]
                    SET [Description]= @Description,
                    [OrderNo]= @OrderNo,[Workshop]= @Workshop,[CellEff]= @CellEff,[ByIm]= @ByIm,
                    [CellCode]= @CellCode,[CellBatch]= @CellBatch,[GlassCode]= @GlassCode,[GlassBatch]= @GlassBatch,
                    [EvaCode]= @EvaCode,[EvaBatch]= @EvaBatch,[TptCode]= @TptCode,[TptBatch]= @TptBatch,
                    [ConBoxCode]= @ConBoxCode,[ConBoxBatch]= @ConBoxBatch,[LongFrameCode]= @LongFrameCode,[LongFrameBatch]= @LongFrameBatch,
                    [ShortFrameCode]= @ShortFrameCode,[ShortFrameBatch]= @ShortFrameBatch,
                    [PackingMode]= @PackingMode,[GlassThickness]= @GlassThickness,[Operator]= @Operator,[OperatorDate]=@OperatorDate,[ModuleGrade]=@ModuleGrade,[Tolerance]=@Tolerance,
                    [Resv01] = @Market, [Resv02] = @LID, [Resv03] = @CabelLength, [Resv04] = @JBoxType
                 	WHERE [MaterialTemplateName]=@MaterialTemplateName";



            SqlParameter[] parameters = {
                new SqlParameter("@MaterialTemplateName", SqlDbType.NVarChar,100),
                new SqlParameter("@Description", SqlDbType.NVarChar,255),
                new SqlParameter("@OrderNo", SqlDbType.NVarChar,100),
                new SqlParameter("@Workshop", SqlDbType.NVarChar,100),
                new SqlParameter("@CellEff", SqlDbType.NVarChar,100),
                new SqlParameter("@ByIm", SqlDbType.NVarChar,100),
                new SqlParameter("@CellCode", SqlDbType.NVarChar,100),
                new SqlParameter("@CellBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassCode", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@EvaCode", SqlDbType.NVarChar,100),
                new SqlParameter("@EvaBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@TptCode", SqlDbType.NVarChar,100),
                new SqlParameter("@TptBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@ConBoxCode", SqlDbType.NVarChar,100),
                new SqlParameter("@ConBoxBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@LongFrameCode", SqlDbType.NVarChar,100),
                new SqlParameter("@LongFrameBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@ShortFrameCode", SqlDbType.NVarChar,100),
                new SqlParameter("@ShortFrameBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@PackingMode", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassThickness", SqlDbType.NVarChar,100),
                new SqlParameter("@Operator", SqlDbType.NVarChar,100),
                new SqlParameter("@OperatorDate", SqlDbType.NVarChar,100),
                new SqlParameter("@ModuleGrade", SqlDbType.NVarChar,50),
                new SqlParameter("@Tolerance", SqlDbType.NVarChar,100),
				new SqlParameter("@Market", SqlDbType.NVarChar,100), 
 				new SqlParameter("@LID", SqlDbType.NVarChar,100),   
				new SqlParameter("@CabelLength", SqlDbType.NVarChar,100), 
 				new SqlParameter("@JBoxType", SqlDbType.NVarChar,100)            };

            parameters[0].Value = TemplateName;
            parameters[1].Value = TemplateDesc;
            parameters[2].Value = WorkOrder;
            parameters[3].Value = WorkShop;
            parameters[4].Value = CellTransfer;
            parameters[5].Value = ByIm;
            parameters[6].Value = CellCode;
            parameters[7].Value = CellBatch;
            parameters[8].Value = GlassCode;
            parameters[9].Value = GlassBatch;
            parameters[10].Value = EvaCode;
            parameters[11].Value = EvaBatch;
            parameters[12].Value = TptCode;
            parameters[13].Value = TptBatch;
            parameters[14].Value = ConBoxCode;
            parameters[15].Value = ConBoxBatch;
            parameters[16].Value = LongFrameCode;
            parameters[17].Value = LongFrameBatch;
            parameters[18].Value = ShortFrameCode;
            parameters[19].Value = ShortFrameBatch;
            parameters[20].Value = PackingPattern;
            parameters[21].Value = GlassLength;
            parameters[22].Value = FormCover.currUserName;
            parameters[23].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            parameters[24].Value = ModuleGrade;
            parameters[25].Value = Tolerance;
            parameters[26].Value = Market;
            parameters[27].Value = LID;
           	parameters[28].Value = CabelLength;
			parameters[29].Value = JBoxType;

            try
            {
                using (SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString))
                {
                    sqlConne.Open();
                    return SqlHelper.ExecuteNonQuery(sqlConne, CommandType.Text, strSql, parameters);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        /// <summary>
        /// 新增物料模板
        /// <returns></returns>
        public static int AddMaterialTemplate(
            string TemplateName,
            string TemplateDesc,
            string WorkOrder,
            string WorkShop,
            string ByIm,
            string CellTransfer,
            string PackingPattern,
            string GlassLength,
            string CellCode,
            string CellBatch,
            string GlassCode,
            string GlassBatch,
            string EvaCode,
            string EvaBatch,
            string TptCode,
            string TptBatch,
            string ConBoxCode,
            string ConBoxBatch,
            string LongFrameCode,
            string LongFrameBatch,
            string ShortFrameCode,
            string ShortFrameBatch,
            string ModuleGrade,
            string Tolerance,
           	string Market,
           	string LID,
           	string CabelLength,
           	string JBoxType,
            bool isRework)
        {

//            string strSql = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE]
//                        ([SYSID],[MaterialTemplateName],[Description],[OrderNo],[Workshop],[CellEff],[ByIm],
//                        [CellCode],[CellBatch],[GlassCode],[GlassBatch],[EvaCode],[EvaBatch],[TptCode],[TptBatch],
//                        [ConBoxCode],[ConBoxBatch],[LongFrameCode],[LongFrameBatch],[ShortFrameCode],[ShortFrameBatch],
//                        [PackingMode],[GlassThickness],[Operator],[OperatorDate],[ModuleGrade],[Tolerance],[Resv01],[Resv02])
//                        VALUES
//                        (@SYSID,@MaterialTemplateName,@Description,@OrderNo,@Workshop,@CellEff,@ByIm,
//                        @CellCode,@CellBatch,@GlassCode,@GlassBatch,@EvaCode,@EvaBatch,@TptCode,@TptBatch,
//                        @ConBoxCode,@ConBoxBatch,@LongFrameCode,@LongFrameBatch,@ShortFrameCode,@ShortFrameBatch,
//                        @PackingMode,@GlassThickness,@Operator,@OperatorDate,@ModuleGrade,@Tolerance,@Market,@LID)";
			string strSql = "";
			if(isRework)
			{
				 strSql = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE]
                        ([SYSID],[MaterialTemplateName],[Description],[OrderNo],[Workshop],[CellEff],[ByIm],
                        [CellCode],[CellBatch],[GlassCode],[GlassBatch],[EvaCode],[EvaBatch],[TptCode],[TptBatch],
                        [ConBoxCode],[ConBoxBatch],[LongFrameCode],[LongFrameBatch],[ShortFrameCode],[ShortFrameBatch],
                        [PackingMode],[GlassThickness],[Operator],[OperatorDate],[ModuleGrade],[Tolerance],[Resv01],[Resv02],[Resv03],[Resv04],[Resv05])
                        VALUES
                        (@SYSID,@MaterialTemplateName,@Description,@OrderNo,@Workshop,@CellEff,@ByIm,
                        @CellCode,@CellBatch,@GlassCode,@GlassBatch,@EvaCode,@EvaBatch,@TptCode,@TptBatch,
                        @ConBoxCode,@ConBoxBatch,@LongFrameCode,@LongFrameBatch,@ShortFrameCode,@ShortFrameBatch,
                        @PackingMode,@GlassThickness,@Operator,@OperatorDate,@ModuleGrade,@Tolerance,@Market,@LID,@CabelLength,@JBoxType,'True')";
				 
			}
			else
			{
            	strSql = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE]
                        ([SYSID],[MaterialTemplateName],[Description],[OrderNo],[Workshop],[CellEff],[ByIm],
                        [CellCode],[CellBatch],[GlassCode],[GlassBatch],[EvaCode],[EvaBatch],[TptCode],[TptBatch],
                        [ConBoxCode],[ConBoxBatch],[LongFrameCode],[LongFrameBatch],[ShortFrameCode],[ShortFrameBatch],
                        [PackingMode],[GlassThickness],[Operator],[OperatorDate],[ModuleGrade],[Tolerance],[Resv01],[Resv02],[Resv03],[Resv04])
                        VALUES
                        (@SYSID,@MaterialTemplateName,@Description,@OrderNo,@Workshop,@CellEff,@ByIm,
                        @CellCode,@CellBatch,@GlassCode,@GlassBatch,@EvaCode,@EvaBatch,@TptCode,@TptBatch,
                        @ConBoxCode,@ConBoxBatch,@LongFrameCode,@LongFrameBatch,@ShortFrameCode,@ShortFrameBatch,
                        @PackingMode,@GlassThickness,@Operator,@OperatorDate,@ModuleGrade,@Tolerance,@Market,@LID,@CabelLength,@JBoxType)";
			}


            SqlParameter[] parameters = {
                new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
                new SqlParameter("@MaterialTemplateName", SqlDbType.NVarChar,100),
                new SqlParameter("@Description", SqlDbType.NVarChar,255),
                new SqlParameter("@OrderNo", SqlDbType.NVarChar,100),
                new SqlParameter("@Workshop", SqlDbType.NVarChar,100),
                new SqlParameter("@CellEff", SqlDbType.NVarChar,100),
                new SqlParameter("@ByIm", SqlDbType.NVarChar,100),
                new SqlParameter("@CellCode", SqlDbType.NVarChar,100),
                new SqlParameter("@CellBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassCode", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@EvaCode", SqlDbType.NVarChar,100),
                new SqlParameter("@EvaBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@TptCode", SqlDbType.NVarChar,100),
                new SqlParameter("@TptBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@ConBoxCode", SqlDbType.NVarChar,100),
                new SqlParameter("@ConBoxBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@LongFrameCode", SqlDbType.NVarChar,100),
                new SqlParameter("@LongFrameBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@ShortFrameCode", SqlDbType.NVarChar,100),
                new SqlParameter("@ShortFrameBatch", SqlDbType.NVarChar,100),
                new SqlParameter("@PackingMode", SqlDbType.NVarChar,100),
                new SqlParameter("@GlassThickness", SqlDbType.NVarChar,100),
                new SqlParameter("@Operator", SqlDbType.NVarChar,100),
                new SqlParameter("@OperatorDate", SqlDbType.NVarChar,100),
                new SqlParameter("@ModuleGrade", SqlDbType.NVarChar,100),
                new SqlParameter("@Tolerance", SqlDbType.NVarChar,50),
				new SqlParameter("@Market", SqlDbType.NVarChar,100), 
 				new SqlParameter("@LID", SqlDbType.NVarChar,100),
				new SqlParameter("@CabelLength", SqlDbType.NVarChar,100), 
 				new SqlParameter("@JBoxType", SqlDbType.NVarChar,100)             };

            parameters[0].Value = Guid.NewGuid().ToString();
            parameters[1].Value = TemplateName;
            parameters[2].Value = TemplateDesc;
            parameters[3].Value = WorkOrder;
            parameters[4].Value = WorkShop;
            parameters[5].Value = CellTransfer;
            parameters[6].Value = ByIm;
            parameters[7].Value = CellCode;
            parameters[8].Value = CellBatch;
            parameters[9].Value = GlassCode;
            parameters[10].Value = GlassBatch;
            parameters[11].Value = EvaCode;
            parameters[12].Value = EvaBatch;
            parameters[13].Value = TptCode;
            parameters[14].Value = TptBatch;
            parameters[15].Value = ConBoxCode;
            parameters[16].Value = ConBoxBatch;
            parameters[17].Value = LongFrameCode;
            parameters[18].Value = LongFrameBatch;
            parameters[19].Value = ShortFrameCode;
            parameters[20].Value = ShortFrameBatch;
            parameters[21].Value = PackingPattern;
            parameters[22].Value = GlassLength;
            parameters[23].Value = FormCover.currUserName;
            parameters[24].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            parameters[25].Value = ModuleGrade;
            parameters[26].Value = Tolerance;
			parameters[27].Value = Market;
			parameters[28].Value = LID;
			parameters[29].Value = CabelLength;
			parameters[30].Value = JBoxType;
            try
            {
                using (SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString))
                {
                    sqlConne.Open();
                    return SqlHelper.ExecuteNonQuery(sqlConne, CommandType.Text, strSql, parameters);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 增加关联组件号与物料模板
        /// <returns></returns>
        public static int AddSNandMaterialTemplateLink(string TemplateSysID, List<string> SNList)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString);
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

            try
            {
           
                foreach (string s in SNList)
                {
                	
                    string strSqlInsert = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                        ([MODULE_SN],[PACK_MA_SYSID],[LASTUPDATETIME],[LASTUPDATEUSER])
                        VALUES('{0}','{1}','{2}','{3}')";
                    strSqlInsert = string.Format(strSqlInsert, s, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), FormCover.currUserName);

                    cmd.CommandText = strSqlInsert;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
                }
                sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }


        }

        /// <summary>
        /// 更新关联组件号与物料模板
        ///  <returns></returns>
        public static int UpdateSNandMaterialTemplateLink(string TemplateSysID, List<string> SNList)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString);
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

  
            try
            {
            	
            	
                foreach (string s in SNList)
                {

                    string strSqlUpdate = @"UPDATE [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                                        SET [PACK_MA_SYSID] = '{0}',[LASTUPDATETIME]='{1}',[LASTUPDATEUSER]='{2}'
                                        WHERE [MODULE_SN] = '{3}'";
                    strSqlUpdate = string.Format(strSqlUpdate, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), FormCover.currUserName, s);

                    cmd.CommandText = strSqlUpdate;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }

                }
                sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }


        }

        /// <summary>
        /// 取消组件号与物料模板的关联
        public static int DeleteSNandMaterialTemplateLink(List<string> SNList)
        {
            int retval = 0;
            SqlConnection sqlConne = new SqlConnection(FormCover.InterfaceConnString);
            sqlConne.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            string strSNList = string.Empty;

            foreach (string s in SNList)
            {
                strSNList = strSNList + ",'" + s + "'";
            }

            strSNList = strSNList.Substring(1);

            try
            {
                string strSql = @"DELETE FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                                WHERE MODULE_SN IN ({0})";

                strSql = string.Format(strSql, strSNList);
                cmd.CommandText = strSql;
                retval = cmd.ExecuteNonQuery();

                return retval;
            }
            catch
            {
                return 0;
            }
            finally
            {
                sqlConne.Close();
                sqlConne.Dispose();
            }

        }
        /// <summary>
        /// 获取组件与物料模板的对应关系
        /// </summary>
        /// <param name="SNStart"></param>
        /// <param name="SNEnd"></param>
        /// <returns></returns>
        public static DataTable GetSNandMaterialTemplateLink(string SNStart, string SNEnd)
        {
            string sql = string.Empty;
            sql = @"SELECT a.[MODULE_SN],a.[PACK_MA_SYSID], a.[POST_ON],a.[LASTUPDATEUSER],a.[LASTUPDATETIME],b.[MaterialTemplateName]
                    FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] a
                    LEFT JOIN [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] b
                    ON a.[PACK_MA_SYSID] = b.[SYSID]
                    WHERE a.[MODULE_SN] BETWEEN '{0}' AND '{1}'";
            sql = string.Format(sql, SNStart, SNEnd);

            return GetDataTableData(FormCover.InterfaceConnString, sql);
        }
        public static bool HasLinkedWithoutStoraged(string templateName)
        {
            DataTable dt = new DataTable();

            string sql = string.Empty;
            sql = @"SELECT top 1 * FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] lk 
                    WHERE exists (
	                    select * from [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] tml 
		                    where lk.PACK_MA_SYSID = tml.SYSID and tml.MaterialTemplateName = '{0}')  ";
            sql = string.Format(sql, templateName);
            dt = GetDataTableData(FormCover.InterfaceConnString, sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        public static bool HasLinkedWithStoraged(string templateName)
        {
            DataTable dt = new DataTable();

            string sql = string.Empty;
            sql = @"SELECT top 1 * FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] lk 
                    WHERE exists (
	                    select * from [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] tml 
		                    where lk.PACK_MA_SYSID = tml.SYSID and tml.MaterialTemplateName = '{0}') 
		                    And Rtrim(Ltrim(ISNULL(lk.[POST_ON],''))) <> ''";
            sql = string.Format(sql, templateName);
            dt = GetDataTableData(FormCover.InterfaceConnString, sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion



        /// <summary>
        /// 保存入库交易
        /// </summary>
        /// <param name="cartonlist"></param>
        /*
         public static void SaveStorageDataForReport(List<string> cartonlist, string Type)
         {
             try
             {
                 #region
                 #region 拼SQL字符串
                 string cartons = "";
                 foreach (var list in cartonlist)
                 {
                     string vCarton = list.Trim();
                     cartons += "'" + vCarton + "',";
                 }
                 cartons = cartons.Substring(0, cartons.Length - 1);
                 #endregion
                 //string sql = "SELECT SN,ModelType,Pmax,StdPower,BoxID  FROM Product NOLOCK  WHERE BoxID IN ({0})";
                 string sql = @"SELECT 
                              B.SN SN,
                              B.ModelType ModelType,
                              B.BOXID BOXID,
                              B.STDPOWER StdPower,
                              B.PMAX PMAX,
                              B.WorkOrder WORKORDER,
                              A.Temp TEMP,
                              A.VOC VOC,
                              A.ISC ISC,
                              A.Pmax PMAX,
                              A.Vm VM,
                              A.Im IM,
                              A.FF FF,
                              A.Eff EFF,
                              A.TestDateTime TESTTIME
                              FROM ElecParaTest A WITH(NOLOCK) INNER JOIN Product B WITH(NOLOCK) ON
                              A.FNumber =B.SN 
                              AND A.InterID=B.InterID
                               WHERE  B.BoxID  IN ({0})
                               ORDER BY B.BoxID,B.SN";
                 sql = string.Format(sql, cartons);
                 DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                 if (dt != null && dt.Rows.Count > 0)
                 {
                     if (_ListCartonPacking.Count > 0)
                         _ListCartonPacking.Clear();

                     if (_ListModulePacking.Count > 0)
                         _ListModulePacking.Clear();
                     string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                     string posttime = DT.DateTime().LongDateTime;
                     foreach (var list in cartonlist)
                     {
                         DataRow[] rows = dt.Select("BOXID = '" + list + "'");
                         string cartonqty = "";
                         cartonqty = Convert.ToString(rows.Length);

                         #region
                         CARTONPACKINGTRANSACTION cartonpacking = new CARTONPACKINGTRANSACTION();
                         cartonpacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                         cartonpacking.Orgnization = "CS";
                         cartonpacking.Carton = list;
                         cartonpacking.JobNo = "";
                         if (Type.Equals("Invertory"))
                         {
                             cartonpacking.Action = "Invertory";
                         }
                         else if (Type.Equals("UnInvertory"))
                         {
                             cartonpacking.Action = "UnInvertory";
                         }
                         cartonpacking.ActionTxnID = joinid;
                         cartonpacking.ActionDate = posttime;
                         cartonpacking.ActionBy = FormCover.CurrUserName;
                         cartonpacking.ModuleQty = cartonqty;
                         cartonpacking.StdPowerLevel = Convert.ToString(rows[0]["StdPower"]);
                         cartonpacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                         _ListCartonPacking.Add(cartonpacking);
                         #endregion

                         if (rows.Length > 0)
                         {
                             for (int i = 0; i < rows.Length; i++)
                             {
                                 #region
                                 String modulesn = Convert.ToString(rows[i]["SN"]);
                                 MODULEPACKINGTRANSACTION modulepacking = new MODULEPACKINGTRANSACTION();
                                 modulepacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                                 modulepacking.ModuleSn = modulesn;
                                 modulepacking.CartonNoSysID = cartonpacking.SysID;
                                 modulepacking.ProductNo = "";
                                 modulepacking.ModuleType = Convert.ToString(rows[i]["ModelType"]);
                                 modulepacking.ModuleStdPower = Convert.ToString(rows[i]["StdPower"]);
                                 modulepacking.Pmax = Convert.ToString(rows[i]["Pmax"]);
                                 modulepacking.ModuleStdPower = Convert.ToString(rows[i]["StdPower"]);
                                 modulepacking.Workshop = FormCover.CurrentFactory.ToUpper().Trim();
                                 modulepacking.Temp = Convert.ToString(rows[i]["TEMP"]);
                                 modulepacking.VOC = Convert.ToString(rows[i]["VOC"]);
                                 modulepacking.ISC = Convert.ToString(rows[i]["ISC"]);
                                 modulepacking.IMP = Convert.ToString(rows[i]["IM"]);
                                 modulepacking.VMP = Convert.ToString(rows[i]["VM"]);
                                 modulepacking.FF = Convert.ToString(rows[i]["FF"]);
                                 modulepacking.EFF = Convert.ToString(rows[i]["EFF"]);
                                 modulepacking.TestDate = Convert.ToString(rows[i]["TESTTIME"]);
                                 modulepacking.WorkOrder = Convert.ToString(rows[i]["WORKORDER"]);
                                 modulepacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                                 _ListModulePacking.Add(modulepacking);
                                 #endregion

                             }
                         }
                     }
                     #region  写入库交易
                     string msg = "";
                     if (!new CartonPackingTransactionDal().SaveStorage(_ListCartonPacking, _ListModulePacking, out msg))
                     {
                         ToolsClass.Log("入库记录数据保存失败:" + msg + "", "ABNORMAL");
                         return;
                     }
                     #endregion

                 }
                 #endregion
             }
             catch (Exception ex)
             {
             }
         }
         * */


        public static DataTable CancelStorageSucess(string CartonNo)
        {
            string sql = @"SELECT 
                             B.SN SN,
                             B.ModelType ModelType,
                             B.BOXID BOXID,
                             B.STDPOWER StdPower,
                             B.PMAX PMAX,
                             B.WorkOrder WORKORDER,
                             A.Temp TEMP,
                             A.VOC VOC,
                             A.ISC ISC,
                             A.Pmax PMAX,
                             A.Vm VM,
                             A.Im IM,
                             A.FF FF,
                             A.Eff EFF,
                             A.TestDateTime TESTTIME
                             FROM ElecParaTest A WITH(NOLOCK) INNER JOIN Product B WITH(NOLOCK) ON
                             A.FNumber =B.SN 
                             AND A.InterID=B.InterID
                              WHERE  B.BoxID  IN ('{0}')
                              ORDER BY B.BoxID,B.SN";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.connectionBase, sql);
        }

        public static DataTable ReworkStorageSucess(string CartonNo)
        {
            string sql = @"SELECT                            
                           B.MODULE_SN SN,
                           B.MODULE_TYPE ModelType,
                           C.RESV03 BOXID,
                           C.CARTON_NO CUSTBOXID,
                           C.STD_POWER_LEVEL StdPower,
                           D.PMAX PMAX,
                           B.WORK_ORDER WORKORDER,
                           D.TEMP TEMP,
                           D.VOC VOC,
                           D.VOC ISC,                           
                           D.VMP VM,
                           D.IMP IM,
                           D.FF FF,
                           D.EFF EFF,
                           D.TEST_DATE TESTTIME,
                           C.SHIP_JOB_NO JOBNO
                           FROM [CentralizedDatabase].[dbo].[T_MODULE_PACKING_LIST]  a WITH(NOLOCK) , 
                           [CentralizedDatabase].[dbo].[T_MODULE] b WITH(NOLOCK), 
                           [CentralizedDatabase].[dbo].[T_MODULE_CARTON] c with(nolock),
                           [CentralizedDatabase].[dbo].[T_MODULE_TEST] d WITH(NOLOCK) 
                           WHERE C.RESV03  in {0}
                               and a.MODULE_SYSID = b.SYSID 
                               and a.MODULE_CARTON_SYSID = c.SYSID
                               and b.MODULE_SN=d.MODULE_SN
                               ORDER BY C.RESV03";
            sql = string.Format(sql, CartonNo);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }

        public static DataTable ReworkStorageSucessByModuleSns(string moduleSnArray)
        {
            string sql = @"SELECT                            
                           B.MODULE_SN SN,
                           B.MODULE_TYPE ModelType,
                           C.RESV03 BOXID,
                           C.CARTON_NO CUSTBOXID,
                           C.STD_POWER_LEVEL StdPower,
                           D.PMAX PMAX,
                           B.WORK_ORDER WORKORDER,
                           D.TEMP TEMP,
                           D.VOC VOC,
                           D.VOC ISC,                           
                           D.VMP VM,
                           D.IMP IM,
                           D.FF FF,
                           D.EFF EFF,
                           D.TEST_DATE TESTTIME,
                           C.SHIP_JOB_NO JOBNO
                           FROM 
                           [CentralizedDatabase].[dbo].[T_MODULE_PACKING_LIST]  a WITH(NOLOCK) , 
                           [CentralizedDatabase].[dbo].[T_MODULE] b WITH(NOLOCK), 
                           [CentralizedDatabase].[dbo].[T_MODULE_CARTON] c with(nolock),
                           [CentralizedDatabase].[dbo].[T_MODULE_TEST] d WITH(NOLOCK) 
                           WHERE b.MODULE_SN  in {0}
                               and a.MODULE_SYSID = b.SYSID 
                               and a.MODULE_CARTON_SYSID = c.SYSID
                               and b.MODULE_SN=d.MODULE_SN
                               ORDER BY b.MODULE_SN ";
            sql = string.Format(sql, moduleSnArray);
            return GetDataTableData(FormCover.CenterDBConnString, sql);
        }

        public static void SaveStorageDataForReport(string carton, string Type, List<MODULEPACKINGTRANSACTION> _ListModulePacking,
          List<CARTONPACKINGTRANSACTION> _ListCartonPacking, string joinid, string posttime)
        {
            try
            {
                string sql = @"SELECT 
                             B.SN SN,
                             B.ModelType ModelType,
                             B.BOXID BOXID,
                             B.Cust_BoxID CUSTBOXID,
                             B.STDPOWER StdPower,
                             B.PMAX PMAX,
                             B.WorkOrder WORKORDER,
                             A.Temp TEMP,
                             A.VOC VOC,
                             A.ISC ISC,
                             A.Pmax PMAX,
                             A.Vm VM,
                             A.Im IM,
                             A.FF FF,
                             A.Eff EFF,
                             A.TestDateTime TESTTIME
                             FROM ElecParaTest A WITH(NOLOCK) INNER JOIN Product B WITH(NOLOCK) ON
                             A.FNumber =B.SN 
                             AND A.InterID=B.InterNewTempTableID
                              WHERE  B.BoxID  IN ('{0}')
                              ORDER BY B.BoxID,B.SN";
                sql = string.Format(sql, carton);
                DataTable dt = GetDataTableData(FormCover.connectionBase, sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (_ListCartonPacking.Count > 0)
                        _ListCartonPacking.Clear();

                    if (_ListModulePacking.Count > 0)
                        _ListModulePacking.Clear();
                    //string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                    //string posttime = DT.DateTime().LongDateTime;

                    string cartonqty = "";
                    cartonqty = Convert.ToString(dt.Rows.Count.ToString());
                    #region
                    CARTONPACKINGTRANSACTION cartonpacking = new CARTONPACKINGTRANSACTION();
                    cartonpacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                    cartonpacking.Orgnization = "CS";
                    cartonpacking.Carton = carton;
                    cartonpacking.JobNo = "";
                    if (Type.Equals("Invertory"))
                    {
                        cartonpacking.Action = "Invertory";
                    }
                    else if (Type.Equals("UnInvertory"))
                    {
                        cartonpacking.Action = "UnInvertory";
                    }

                    cartonpacking.Resv01 = "CSAS";//2014-03-24
                    cartonpacking.Resv02 = Convert.ToString(dt.Rows[0]["CUSTBOXID"]);//2014-03-24
                    cartonpacking.ActionTxnID = joinid;
                    cartonpacking.ActionDate = posttime;
                    cartonpacking.ActionBy = FormCover.CurrUserName;
                    cartonpacking.ModuleQty = cartonqty;
                    cartonpacking.StdPowerLevel = Convert.ToString(dt.Rows[0]["StdPower"]);
                    cartonpacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                    _ListCartonPacking.Add(cartonpacking);
                    #endregion
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        #region
                        String modulesn = Convert.ToString(dt.Rows[i]["SN"]);
                        MODULEPACKINGTRANSACTION modulepacking = new MODULEPACKINGTRANSACTION();
                        modulepacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                        modulepacking.ModuleSn = modulesn;
                        modulepacking.CartonNoSysID = cartonpacking.SysID;
                        modulepacking.ProductNo = "";
                        modulepacking.ModuleType = Convert.ToString(dt.Rows[i]["ModelType"]);
                        modulepacking.ModuleStdPower = Convert.ToString(dt.Rows[i]["StdPower"]);
                        modulepacking.Pmax = Convert.ToString(dt.Rows[i]["Pmax"]);
                        modulepacking.ModuleStdPower = Convert.ToString(dt.Rows[i]["StdPower"]);
                        modulepacking.Workshop = FormCover.CurrentFactory.ToUpper().Trim();
                        modulepacking.Temp = Convert.ToString(dt.Rows[i]["TEMP"]);
                        modulepacking.VOC = Convert.ToString(dt.Rows[i]["VOC"]);
                        modulepacking.ISC = Convert.ToString(dt.Rows[i]["ISC"]);
                        modulepacking.IMP = Convert.ToString(dt.Rows[i]["IM"]);
                        modulepacking.VMP = Convert.ToString(dt.Rows[i]["VM"]);
                        modulepacking.FF = Convert.ToString(dt.Rows[i]["FF"]);
                        modulepacking.EFF = Convert.ToString(dt.Rows[i]["EFF"]);
                        modulepacking.TestDate = Convert.ToString(dt.Rows[i]["TESTTIME"]);
                        modulepacking.WorkOrder = Convert.ToString(dt.Rows[i]["WORKORDER"]);
                        modulepacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                        _ListModulePacking.Add(modulepacking);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}
