﻿namespace CSICPR
{
    partial class FormStorageReport    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CartonNo = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.IsCancelPacking = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Workshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.PostedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Operator = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.OperatorDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.ddlQueryType = new System.Windows.Forms.ComboBox();
        	this.lblCartonQuery = new System.Windows.Forms.Label();
        	this.PalStorageDate = new System.Windows.Forms.Panel();
        	this.lblPackingFrom = new System.Windows.Forms.Label();
        	this.dtpStorageDateFrom = new System.Windows.Forms.DateTimePicker();
        	this.lblPackingTo = new System.Windows.Forms.Label();
        	this.dtpStorageDateTo = new System.Windows.Forms.DateTimePicker();
        	this.PalCarton = new System.Windows.Forms.Panel();
        	this.txtCarton = new System.Windows.Forms.TextBox();
        	this.label2 = new System.Windows.Forms.Label();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.dataGridView2 = new System.Windows.Forms.DataGridView();
        	this.button2 = new System.Windows.Forms.Button();
        	this.PalWo = new System.Windows.Forms.Panel();
        	this.txtWo = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.label4 = new System.Windows.Forms.Label();
        	this.button1 = new System.Windows.Forms.Button();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.PalStorageDate.SuspendLayout();
        	this.PalCarton.SuspendLayout();
        	this.panel1.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
        	this.PalWo.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowDrop = true;
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.AllowUserToResizeColumns = false;
        	this.dataGridView1.AllowUserToResizeRows = false;
        	dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
        	this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
        	this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        	dataGridViewCellStyle2.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        	dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        	dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        	dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        	this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        	this.dataGridView1.ColumnHeadersHeight = 30;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.RowIndex,
        	        	        	this.OrderNo,
        	        	        	this.CartonNo,
        	        	        	this.IsCancelPacking,
        	        	        	this.CustomerCartonNo,
        	        	        	this.Workshop,
        	        	        	this.PostedOn,
        	        	        	this.Operator,
        	        	        	this.OperatorDate});
        	dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
        	dataGridViewCellStyle3.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.PeachPuff;
        	dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        	this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
        	this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridView1.Location = new System.Drawing.Point(0, 95);
        	this.dataGridView1.Name = "dataGridView1";
        	dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
        	dataGridViewCellStyle4.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
        	dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        	dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        	dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        	this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
        	this.dataGridView1.RowHeadersVisible = false;
        	this.dataGridView1.RowHeadersWidth = 16;
        	this.dataGridView1.RowTemplate.Height = 23;
        	this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.dataGridView1.Size = new System.Drawing.Size(1004, 525);
        	this.dataGridView1.TabIndex = 16;
        	this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
        	// 
        	// RowIndex
        	// 
        	this.RowIndex.HeaderText = "序号";
        	this.RowIndex.Name = "RowIndex";
        	this.RowIndex.ReadOnly = true;
        	this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	this.RowIndex.Width = 50;
        	// 
        	// OrderNo
        	// 
        	this.OrderNo.HeaderText = "工单号";
        	this.OrderNo.Name = "OrderNo";
        	this.OrderNo.ReadOnly = true;
        	// 
        	// CartonNo
        	// 
        	this.CartonNo.HeaderText = "内部托号";
        	this.CartonNo.Name = "CartonNo";
        	this.CartonNo.ReadOnly = true;
        	this.CartonNo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        	this.CartonNo.Width = 120;
        	// 
        	// IsCancelPacking
        	// 
        	this.IsCancelPacking.HeaderText = "入库状态";
        	this.IsCancelPacking.Name = "IsCancelPacking";
        	this.IsCancelPacking.ReadOnly = true;
        	// 
        	// CustomerCartonNo
        	// 
        	this.CustomerCartonNo.HeaderText = "客户托号";
        	this.CustomerCartonNo.Name = "CustomerCartonNo";
        	this.CustomerCartonNo.ReadOnly = true;
        	// 
        	// Workshop
        	// 
        	this.Workshop.HeaderText = "车间";
        	this.Workshop.Name = "Workshop";
        	this.Workshop.ReadOnly = true;
        	// 
        	// PostedOn
        	// 
        	this.PostedOn.HeaderText = "上传日期";
        	this.PostedOn.Name = "PostedOn";
        	this.PostedOn.ReadOnly = true;
        	this.PostedOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	this.PostedOn.Width = 160;
        	// 
        	// Operator
        	// 
        	this.Operator.HeaderText = "操作人";
        	this.Operator.Name = "Operator";
        	this.Operator.ReadOnly = true;
        	// 
        	// OperatorDate
        	// 
        	this.OperatorDate.HeaderText = "操作时间";
        	this.OperatorDate.Name = "OperatorDate";
        	this.OperatorDate.ReadOnly = true;
        	this.OperatorDate.Width = 160;
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(416, 54);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(65, 24);
        	this.btnQuery.TabIndex = 168;
        	this.btnQuery.Tag = "button1";
        	this.btnQuery.Text = "查询";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// ddlQueryType
        	// 
        	this.ddlQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlQueryType.FormattingEnabled = true;
        	this.ddlQueryType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.ddlQueryType.Location = new System.Drawing.Point(138, 18);
        	this.ddlQueryType.Name = "ddlQueryType";
        	this.ddlQueryType.Size = new System.Drawing.Size(148, 21);
        	this.ddlQueryType.TabIndex = 169;
        	this.ddlQueryType.SelectedIndexChanged += new System.EventHandler(this.ddlQueryType_SelectedIndexChanged);
        	// 
        	// lblCartonQuery
        	// 
        	this.lblCartonQuery.AutoSize = true;
        	this.lblCartonQuery.Location = new System.Drawing.Point(16, 22);
        	this.lblCartonQuery.Name = "lblCartonQuery";
        	this.lblCartonQuery.Size = new System.Drawing.Size(64, 13);
        	this.lblCartonQuery.TabIndex = 170;
        	this.lblCartonQuery.Text = "查 询 类 型";
        	// 
        	// PalStorageDate
        	// 
        	this.PalStorageDate.Controls.Add(this.lblPackingFrom);
        	this.PalStorageDate.Controls.Add(this.dtpStorageDateFrom);
        	this.PalStorageDate.Controls.Add(this.lblPackingTo);
        	this.PalStorageDate.Controls.Add(this.dtpStorageDateTo);
        	this.PalStorageDate.Location = new System.Drawing.Point(16, 53);
        	this.PalStorageDate.Name = "PalStorageDate";
        	this.PalStorageDate.Size = new System.Drawing.Size(381, 39);
        	this.PalStorageDate.TabIndex = 171;
        	// 
        	// lblPackingFrom
        	// 
        	this.lblPackingFrom.AutoSize = true;
        	this.lblPackingFrom.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.lblPackingFrom.Location = new System.Drawing.Point(3, 15);
        	this.lblPackingFrom.Name = "lblPackingFrom";
        	this.lblPackingFrom.Size = new System.Drawing.Size(21, 14);
        	this.lblPackingFrom.TabIndex = 0;
        	this.lblPackingFrom.Text = "从";
        	// 
        	// dtpStorageDateFrom
        	// 
        	this.dtpStorageDateFrom.CustomFormat = "yyyy-MM-dd";
        	this.dtpStorageDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpStorageDateFrom.Location = new System.Drawing.Point(106, 11);
        	this.dtpStorageDateFrom.Name = "dtpStorageDateFrom";
        	this.dtpStorageDateFrom.Size = new System.Drawing.Size(100, 20);
        	this.dtpStorageDateFrom.TabIndex = 5;
        	// 
        	// lblPackingTo
        	// 
        	this.lblPackingTo.AutoSize = true;
        	this.lblPackingTo.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.lblPackingTo.Location = new System.Drawing.Point(241, 14);
        	this.lblPackingTo.Name = "lblPackingTo";
        	this.lblPackingTo.Size = new System.Drawing.Size(21, 14);
        	this.lblPackingTo.TabIndex = 6;
        	this.lblPackingTo.Text = "到";
        	// 
        	// dtpStorageDateTo
        	// 
        	this.dtpStorageDateTo.CustomFormat = "yyyy-MM-dd";
        	this.dtpStorageDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpStorageDateTo.Location = new System.Drawing.Point(272, 11);
        	this.dtpStorageDateTo.Name = "dtpStorageDateTo";
        	this.dtpStorageDateTo.Size = new System.Drawing.Size(104, 20);
        	this.dtpStorageDateTo.TabIndex = 7;
        	// 
        	// PalCarton
        	// 
        	this.PalCarton.Controls.Add(this.txtCarton);
        	this.PalCarton.Controls.Add(this.label2);
        	this.PalCarton.Location = new System.Drawing.Point(16, 57);
        	this.PalCarton.Name = "PalCarton";
        	this.PalCarton.Size = new System.Drawing.Size(287, 38);
        	this.PalCarton.TabIndex = 172;
        	// 
        	// txtCarton
        	// 
        	this.txtCarton.Location = new System.Drawing.Point(126, 3);
        	this.txtCarton.Name = "txtCarton";
        	this.txtCarton.Size = new System.Drawing.Size(148, 20);
        	this.txtCarton.TabIndex = 94;
        	this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(1, 7);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(64, 13);
        	this.label2.TabIndex = 94;
        	this.label2.Text = "内 部 托 号";
        	// 
        	// panel1
        	// 
        	this.panel1.Controls.Add(this.PalStorageDate);
        	this.panel1.Controls.Add(this.dataGridView2);
        	this.panel1.Controls.Add(this.button2);
        	this.panel1.Controls.Add(this.PalWo);
        	this.panel1.Controls.Add(this.PalCarton);
        	this.panel1.Controls.Add(this.label4);
        	this.panel1.Controls.Add(this.button1);
        	this.panel1.Controls.Add(this.btnQuery);
        	this.panel1.Controls.Add(this.ddlQueryType);
        	this.panel1.Controls.Add(this.lblCartonQuery);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(1004, 95);
        	this.panel1.TabIndex = 174;
        	// 
        	// dataGridView2
        	// 
        	this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView2.Location = new System.Drawing.Point(829, 53);
        	this.dataGridView2.Name = "dataGridView2";
        	this.dataGridView2.RowTemplate.Height = 23;
        	this.dataGridView2.Size = new System.Drawing.Size(89, 25);
        	this.dataGridView2.TabIndex = 175;
        	this.dataGridView2.Visible = false;
        	// 
        	// button2
        	// 
        	this.button2.Location = new System.Drawing.Point(487, 53);
        	this.button2.Name = "button2";
        	this.button2.Size = new System.Drawing.Size(65, 24);
        	this.button2.TabIndex = 178;
        	this.button2.Tag = "button1";
        	this.button2.Text = "清空";
        	this.button2.UseVisualStyleBackColor = true;
        	this.button2.Click += new System.EventHandler(this.button2_Click);
        	// 
        	// PalWo
        	// 
        	this.PalWo.Controls.Add(this.txtWo);
        	this.PalWo.Controls.Add(this.label1);
        	this.PalWo.Location = new System.Drawing.Point(16, 57);
        	this.PalWo.Name = "PalWo";
        	this.PalWo.Size = new System.Drawing.Size(290, 35);
        	this.PalWo.TabIndex = 173;
        	// 
        	// txtWo
        	// 
        	this.txtWo.Location = new System.Drawing.Point(122, 3);
        	this.txtWo.Name = "txtWo";
        	this.txtWo.Size = new System.Drawing.Size(148, 20);
        	this.txtWo.TabIndex = 94;
        	this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(0, 7);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(58, 13);
        	this.label1.TabIndex = 94;
        	this.label1.Text = "工   单  号";
        	// 
        	// label4
        	// 
        	this.label4.Location = new System.Drawing.Point(636, 64);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(39, 13);
        	this.label4.TabIndex = 177;
        	// 
        	// button1
        	// 
        	this.button1.Location = new System.Drawing.Point(732, 53);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(79, 24);
        	this.button1.TabIndex = 175;
        	this.button1.Tag = "button1";
        	this.button1.Text = "导出Excel";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.button1_Click);
        	// 
        	// FormStorageReport
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1004, 620);
        	this.Controls.Add(this.dataGridView1);
        	this.Controls.Add(this.panel1);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        	this.Name = "FormStorageReport";
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Tag = "FRM0002";
        	this.Text = "入库数据上传查询";
        	this.Load += new System.EventHandler(this.FormCartonInfoQuery_Load);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.PalStorageDate.ResumeLayout(false);
        	this.PalStorageDate.PerformLayout();
        	this.PalCarton.ResumeLayout(false);
        	this.PalCarton.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
        	this.PalWo.ResumeLayout(false);
        	this.PalWo.PerformLayout();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox ddlQueryType;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalStorageDate;
        private System.Windows.Forms.Label lblPackingFrom;
        private System.Windows.Forms.DateTimePicker dtpStorageDateFrom;
        private System.Windows.Forms.Label lblPackingTo;
        private System.Windows.Forms.DateTimePicker dtpStorageDateTo;
        private System.Windows.Forms.Panel PalCarton;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PalWo;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private System.Windows.Forms.DataGridViewLinkColumn CartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsCancelPacking;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Workshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operator;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperatorDate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView2;
    }
}