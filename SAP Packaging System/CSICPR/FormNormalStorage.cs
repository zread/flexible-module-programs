﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;
using System.Xml;
using System.IO;


using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;
using CSICPR.Properties;

namespace CSICPR
{
    public partial class FormNormalStorage : Form
    {
        //private List<string> _ListCartonSucess = new List<string>();
        private static List<LanguageItemModel> LanMessList;//定义语言集
        public FormNormalStorage()
        {
            InitializeComponent();
            CbUpload.SelectedIndex = 0;  
			CbUpload.Enabled = false;            
        }

        #region 公有变量
        private static FormNormalStorage theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormNormalStorage();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        /// <summary>
        /// 设置：记录选中的记录
        /// </summary>
        private Dictionary<int, string> dgvIndex = new Dictionary<int, string>();
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        /// <summary>
        /// 暂存修改过的组件电池片信息
        /// </summary>
        private List<CellSerialNumberTemp> _ListPackingSNCellInfo = new List<CellSerialNumberTemp>();

        /// <summary>
        ///记录上传入库成功的做记录
        /// </summary>
        private List<SapWoModule> _ListSapWoLog = new List<SapWoModule>();

        /// <summary>
        /// 存储要入库的箱号
        /// </summary>
        List<string> cartonList = new List<string>();

        /// <summary>
        /// 存储数据维护OK待入库的箱号
        /// </summary>
        List<string> cartonStroageList = new List<string>();
        #endregion

        #region 托号查询
        private void txtCartonQuery_KeyPress(object sender, KeyPressEventArgs e)
        {
        	return;
    		//Nothing
        }
        private void txtCartonQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                PicBoxCarton_Click(null, null);
            }
        }
        private void PicBoxCarton_Click(object sender, EventArgs e)
        {
            var frm = new FormCartonInfoQuery(this.txtCarton.Text.Trim().ToString());
            frm.ShowDialog();
            DataTable dt = frm.CartonList;
            if (frm != null)
                frm.Dispose();
            SetDataGridView(dt);
        }

        private bool checkrepeat(string carton)
        {
            foreach (DataGridViewRow datarow in dataGridView1.Rows)
            {
                if (datarow.Cells[2].Value != null && datarow.Cells[2].Value.Equals(carton))
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message58", "托号{0}已在列表第{1}行"), carton, (datarow.Index + 1)), "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }
        private void SetDataGridView(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string cartonStatus = "";
//                    if (Convert.ToString(row["CartonStatus"]).Equals("0")) 
//                    	cartonStatus = "Tested";
//                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
//                        cartonStatus = "Packed";
//                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
//                        cartonStatus = "Stocked in";
//                    else
//                        cartonStatus = Convert.ToString(row["CartonStatus"]);

					if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        cartonStatus = LanguageHelper.GetMessage(LanMessList, "Message3", "已测试");
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        cartonStatus = LanguageHelper.GetMessage(LanMessList, "Message4", "已包装");
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        cartonStatus = LanguageHelper.GetMessage(LanMessList, "Message5", "已入库");
                    else
                        cartonStatus = Convert.ToString(row["CartonStatus"]);

                    if (checkrepeat(Convert.ToString(row["CartonID"])))
                        dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { false, this.dataGridView1.Rows.Count + 1, Convert.ToString(row["CartonID"]), cartonStatus, Convert.ToString(row["Cust_BoxID"]), Convert.ToString(row["SNQTY"]) });

                }
                this.txtCarton.Clear();
                this.txtCarton.Focus();
            }
            else
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message6", "没有选数据!"), "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        #region 窗体事件

        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Left = chbSelected.Left - i + j;
            }

            else if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {

                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Top = chbSelected.Top - i + j;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 2))
            {
                string CartonNumber = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CartonID"].Value);
                if (!string.IsNullOrEmpty(CartonNumber))
                {
                    DataTable dt = ProductStorageDAL.GetSAPCartonStorageInfo(CartonNumber);
                    if (dt != null && dt.Rows.Count > 0)
                    { }
                    else
                    {
                        setIsEnable(true);
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message7", "此托没有维护要入库的物料批次信息:") + CartonNumber, "ABNORMAL", lstView);
                        return;
                    }

                    var frm = new FormSNDetail(CartonNumber);
                    if (frm.ShowDialog() != DialogResult.No)
                        frm.Show();
                    if (frm != null)
                    {
                        frm.Dispose();
                    }
                }
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridViewCheckBoxCell dgcb = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells["Selected"];
                if ((bool)dgcb.FormattedValue)
                {
                    dgcb.Value = false;
                }
                else
                {
                    dgcb.Value = true;
                }
            }
        }

        private bool SaveXml(List<SapWoModule> listWo)
        {
            try
            {
                #region
                DataTable dt = new DataTable();
                string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(listWo, Encoding.UTF8);
                SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");
                string cars = "";
                if (cartonList.Count > 0)
                {
                    foreach (string carton in cartonList)
                        cars += "," + carton;
                }
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message8", "内部托号入库数据保存：") + cars);
                ToolsClass.Log(SapStorageString);
                try
                {
                    SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                    service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                    string aa = service.packingDataMesToSap(SapStorageString);
                    string bb;
                    dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
                }
                catch (Exception ex)
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message9", "连接SAP时,发生异常.") + ex.Message + "", "ABNORMAL", lstView);
                    return false;
                }

                if (!listWo.Count.Equals(dt.Rows.Count))
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "上传SAP和SAP返回的总数据不一致"), "ABNORMAL", lstView);
                    return false;
                }

                try
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        #region
                        List<SapWoModule> _ListCartonSapWo = new List<SapWoModule>();
                        //if (_ListCartonSucess.Count > 0)
                        //   _ListCartonSucess.Clear();
                        string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                        string posttime = DT.DateTime().LongDateTime;
                        foreach (string carton in cartonList)
                        {
                            string flag = "Y";
                            if (_ListCartonSapWo.Count > 0)
                                _ListCartonSapWo.Clear();
                            for (int mmm = 0; mmm < listWo.Count; mmm++)
                            {
                                if (carton == listWo[mmm].CartonNo)
                                {
                                    DataRow[] rows = dt.Select("key = '" + listWo[mmm].ModuleSN + "'");
                                    if (rows.Length > 0)
                                    {
                                        //上传失败
                                        if (Convert.ToString(rows[0]["Result"]).Equals("0"))
                                        {
                                            flag = "N";
                                            this.Save.Enabled = true;
                                            this.button1.Enabled = true;
                                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11", "托号上传SAP保存失败.原因:") + carton + Convert.ToString(rows[0]["Message"]) + "", "ABNORMAL", lstView);
                                            break;
                                        }
                                        else
                                            _ListCartonSapWo.Add(listWo[mmm]);
                                    }
                                    else //上传SAP失败
                                    {
                                        flag = "N";
                                        this.Save.Enabled = true;
                                        this.button1.Enabled = true;
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message12", "托号上传SAP保存失败.原因:SAP返回数据丢失:") + carton, "ABNORMAL", lstView);
                                        break;
                                    }
                                }
                            }
                            //上传成功
                            if (flag.Equals("Y"))
                            {
                                if (ProductStorageDAL.updatepackingdatabySn(_ListCartonSapWo, joinid, posttime))
                                {

                                    //if (!_ListCartonSucess.Contains(carton))
                                    //    _ListCartonSucess.Add(carton);
                                    this.Save.Enabled = true;
                                    this.button1.Enabled = true;
                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message13", "托号 上传SAP保存成功：") + carton, "NORMAL", lstView);

                                    try
                                    {
                                        #region 移除上传成功的托号
                                        foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                                        {
                                            if (dgr.Cells[2].Value != null && dgr.Cells[2].Value.Equals(carton))
                                            {
                                                this.dataGridView1.Rows.RemoveAt(dgr.Index);
                                                break;
                                            }
                                        }

                                        DataTable SN = ProductStorageDAL.GetSNInfoByCarton(carton);
                                        if (SN != null && SN.Rows.Count > 0)
                                        {
                                            foreach (DataRow newsn in SN.Rows)
                                            {
                                                //成功的托号赋值给一个新的List,做Log记录
                                                for (int p = 0; p < _ListSapWo.Count; p++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListSapWo[p].ModuleSN)
                                                    {
                                                        _ListSapWoLog.Add(_ListSapWo[p]);
                                                    }
                                                }

                                                for (int q = 0; q < _ListPackingSNCellInfo.Count; q++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListPackingSNCellInfo[q].SN)
                                                    {
                                                        _ListPackingSNCellInfo.RemoveAt(q);
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                else
                                {
                                    this.Save.Enabled = true;
                                    this.button1.Enabled = true;
                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message14", "托号上传SAP保存失败更新包装数据库错误：") + carton, "NORMAL", lstView);
                                }
                            }
                        }
                        //#region 写入库交易记录
                        //if (_ListCartonSucess.Count > 0)
                        //{
                        //    string posttime = DT.DateTime().LongDateTime;
                        //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "Invertory");
                        //}
                        //#endregion

                        #endregion
                    }
                    else
                    {
                        this.Save.Enabled = true;
                        this.button1.Enabled = true;
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message15", "上传SAP保存,返回数据错误"), "ABNORMAL", lstView);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message16", "处理SAP返回数据时发生异常:") + ex.Message + "!", "ABNORMAL", lstView);
                    return false;
                }
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                this.Save.Enabled = true;
                this.button1.Enabled = true;
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message17", "上传SAP时候发生异常(处理SAP返回数据):") + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        private bool SavePreUploadData(List<SapWoModule> listWo, List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            try
            {
                #region

                var cars = string.Join(",", cartonList.ToArray());
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message18", "内部托号入库数据保存：") + cars);

                var joinId = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                var postTime = DT.DateTime().LongDateTime;
                foreach (var carton in cartonList)
                {
                    var listCartonSapWo = listWo.FindAll(p => p.CartonNo == carton);
                    if (listCartonSapWo.Count <= 0)
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message19", "数据出错!"), "ABNORMAL", lstView);
                        return false;
                    }
                    var listTsapReceiptUploadModules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton);
                    if (listTsapReceiptUploadModules.Count <= 0)
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message19","数据出错!" ), "ABNORMAL", lstView);
                        return false;
                    }
                    if (!ProductStorageDAL.updatepackingdatabySn(listTsapReceiptUploadModules, listCartonSapWo, joinId, postTime))
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message20","托号待上传SAP数据保存失败(更新包装数据库错误)："  )+ carton , "NORMAL", lstView);
                        return false;
                    }
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message21", "托号待上传SAP数据保存成功：") + carton, "NORMAL", lstView);
    
                    #region 移除上传成功的托号

                    foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                    {
                        if (dgr.Cells[2].Value == null || !dgr.Cells[2].Value.Equals(carton))
                            continue;
                        dataGridView1.Rows.RemoveAt(dgr.Index);
                        break;
                    }

                    #endregion
                }

                return true;
                #endregion
            }
            catch (Exception ex)
            {
                Save.Enabled = true;
                button1.Enabled = true;
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message22", "待上传SAP数据保存发生异常:") + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        //清空数据
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            if (_ListSapWo.Count > 0)
                _ListSapWo.Clear();

            if (_ListPackingSNCellInfo.Count > 0)
                _ListPackingSNCellInfo.Clear();

            if (cartonList.Count > 0)
                cartonList.Clear();

            this.txtCarton.Clear();
            this.txtCarton.Focus();
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }
        #endregion

        private void setIsEnable(bool flag)
        {
            if (flag)
            {
                this.Save.Enabled = true;
                this.button1.Enabled = true;
                this.chbSelected.Enabled = true;
                this.dataGridView1.Enabled = true;
                this.txtCarton.Enabled = true;
                this.PicBoxCarton.Enabled = true;
            }
            else
            {
                this.Save.Enabled = false;
                this.button1.Enabled = false;
                this.chbSelected.Enabled = false;
                this.dataGridView1.Enabled = false;
                this.txtCarton.Enabled = false;
                this.PicBoxCarton.Enabled = false;
            }
        }
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                setIsEnable(false);

                #region
                if (this.dataGridView1.Rows.Count < 1)
                {
                    setIsEnable(true);
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message23", "没有要入库的数据!"), "ABNORMAL", lstView);
                    return;
                }

                if (cartonList.Count > 0)
                    cartonList.Clear();

                if (cartonStroageList.Count > 0)
                    cartonStroageList.Clear();

                int StorageCount = 0;
                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if (Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE")
                    {
                        StorageCount = StorageCount + 1;
                        if (!cartonStroageList.Contains(Convert.ToString(dgvRow.Cells[2].Value)))
                            cartonStroageList.Add(Convert.ToString(dgvRow.Cells[2].Value));
                    }
                }

                if (StorageCount == 0)
                {
                    setIsEnable(true);
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message24", "没有选中要入库的数据!"), "ABNORMAL", lstView);
                    return;
                }

                //对上传数据完整性检查
                int maxQty = ProductStorageDAL.GetUploadStorageQty("MAXSTORAGEQTY", FormCover.CurrentFactory);
                if (maxQty <= 0)
                {
                    setIsEnable(true);
                    return;
                }
                else
                {
                    if (StorageCount > maxQty)
                    {
                        setIsEnable(true);
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message25", "每次最多只能上传笔数：") + maxQty, "ABNORMAL", lstView);
                        return;
                    }
                }

                //检查数据是否维护齐全
                
                foreach (string carton in cartonStroageList)
                {
                     	int stroageQty = ProductStorageDAL.GetSNStroageQty(carton);
              
                    if (stroageQty == 0)
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message26", "托号已经入库： ") + carton, "ABNORMAL", lstView);
                        continue;
                    }
                    else
                    {
                        int configQty = ProductStorageDAL.GetSNStroageQtyFromConfig(carton);
                        if (configQty == 0)
                        {
                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message27", "托号所对应的组件没有维护物料信息或信息没有维护全，请查询： ") + carton, "ABNORMAL", lstView);
                             continue;
                        }
                        else if (configQty != stroageQty)
                        {
                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message28", "托号所对应的组件中,有部分组件没有维护物料信息,请单击内部托号的连接查询：") + carton, "ABNORMAL", lstView);
                            continue;
                        }
                        else if (configQty == stroageQty)
                        {
                            if (!cartonList.Contains(Convert.ToString(carton)))
                                cartonList.Add(Convert.ToString(carton));
                        }
                    }
                }

                //检查待入库的托号是否已经超过了工单总数量

                //拼xml文件
                if (_ListSapWo.Count > 0)
                    _ListSapWo.Clear();

                if (cartonList.Count < 1)
                {
                    setIsEnable(true);
                    return;
                }

                DateTime CurrentTime = DateTime.Now;
                string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
                	//CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期
                string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码

                var tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
                var createdOn = DT.DateTime().LongDateTime;
                //var createdOn = dt_pick.Value.ToString("yyyy-MM-dd HH:mm:ss.fff");
                //MessageBox.Show(createdOn);
                var createdBy = FormCover.CurrUserName;
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
                foreach (string cartons in cartonList)
                {
                    //获取打包的数据
                    DataTable newdt = ProductStorageDAL.GetSAPCartonStorageInfo(cartons);
                    if (newdt == null || newdt.Rows.Count == 0)
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message29", "托号获取组件入库信息失败：") + cartons, "ABNORMAL", lstView);
                        continue;
                    }

                    #region 并xml
				
                    foreach (DataRow newdtrow in newdt.Rows)
                    {
                    	string MC = newdtrow["组件颜色"].ToString();
                    	if(string.Equals("Dark",MC.ToString().Trim()))
                    		MC = "D";
                    	else if(string.Equals("Blue",MC.ToString().Trim()))
                    		MC = "B";
                    	else if(string.Equals("Lite",MC.ToString().Trim()))
                    		MC = "BB";
                    	else 
                    		MC = "NA";
                    	
                    	SapWoModule SapWo = new SapWoModule();
//                        SapWo.CellCode = Convert.ToString(newdtrow["电池片物料代码"]);
                        SapWo.CellCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["电池片物料代码"])))?"NA" : Convert.ToString(newdtrow["电池片物料代码"]);
//                        SapWo.CellBatch = Convert.ToString(newdtrow["电池片批次"]);
                        SapWo.CellBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["电池片批次"]))) ? "NA" : Convert.ToString(newdtrow["电池片批次"]);

                        SapWo.ActionCode = "I";
                        SapWo.SysId = Convert.ToString(newdtrow["组件序列号"]);
                        SapWo.OrderNo = Convert.ToString(newdtrow["生产订单号"]);
                        SapWo.ProductCode = Convert.ToString(newdtrow["产品物料代码"]);
                        SapWo.PostedOn = PostedOn;
                        SapWo.FinishedOn = Convert.ToDateTime(newdtrow["包装日期"]).ToString("yyyy-MM-dd HH:mm:ss.fff");
                        SapWo.Unit = "PC";
                        SapWo.Factory = Convert.ToString(newdtrow["工厂"]);
                        SapWo.Workshop = Convert.ToString(newdtrow["车间"]);
                        SapWo.PackingLocation = Convert.ToString(newdtrow["入库地点"]);
                        SapWo.CellEff = Convert.ToString(newdtrow["电池片转换效率"]);
                        SapWo.ModuleSN = Convert.ToString(newdtrow["组件序列号"]);
                        SapWo.CartonNo = Convert.ToString(newdtrow["内部托号"]);
                        SapWo.TestPower = Convert.ToString(newdtrow["实测功率"]);
                        SapWo.StdPower = Convert.ToString(newdtrow["标称功率"]);
                        SapWo.OrderStatus = "1";
                        SapWo.PostKey = PostKey;
                        SapWo.ModuleGrade = Convert.ToString(newdtrow["组件等级"]);
                        SapWo.ByIm = Convert.ToString(newdtrow["电流分档"]);
                        SapWo.CellPrintMode = Convert.ToString(newdtrow["电池片网版"]);

//                        SapWo.GlassCode = Convert.ToString(newdtrow["玻璃物料代码"]);
                        SapWo.GlassCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["玻璃物料代码"]))) ? "NA" : Convert.ToString(newdtrow["玻璃物料代码"]);

//                        SapWo.GlassBatch = Convert.ToString(newdtrow["玻璃批次"]);
                        SapWo.GlassBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["玻璃批次"]))) ? "NA" : Convert.ToString(newdtrow["玻璃批次"]);

//                        SapWo.EvaCode = Convert.ToString(newdtrow["EVA 物料代码"]);
                        SapWo.EvaCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["EVA 物料代码"]))) ? "NA" : Convert.ToString(newdtrow["EVA 物料代码"]);

//                        SapWo.EvaBatch = Convert.ToString(newdtrow["EVA 批次号"]);
                        SapWo.EvaBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["EVA 批次号"]))) ? "NA" : Convert.ToString(newdtrow["EVA 批次号"]);

//                        SapWo.TptCode = Convert.ToString(newdtrow["TPT物料代码"]);
                        SapWo.TptCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["TPT物料代码"]))) ? "NA" : Convert.ToString(newdtrow["TPT物料代码"]);

//                        SapWo.TptBatch = Convert.ToString(newdtrow["TPT 批次"]);
                        SapWo.TptBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["TPT 批次"]))) ? "NA" : Convert.ToString(newdtrow["TPT 批次"]);

//                        SapWo.ConBoxCode = Convert.ToString(newdtrow["接线盒物料代码"]);
                        SapWo.ConBoxCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["接线盒物料代码"]))) ? "NA" : Convert.ToString(newdtrow["接线盒物料代码"]);

//                        SapWo.ConBoxBatch = Convert.ToString(newdtrow["接线盒批次"]);
                        SapWo.ConBoxBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["接线盒批次"]))) ? "NA" : Convert.ToString(newdtrow["接线盒批次"]);

//                        SapWo.LongFrameCode = Convert.ToString(newdtrow["长边框物料代码"]);
                        SapWo.LongFrameCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["长边框物料代码"]))) ? "NA" : Convert.ToString(newdtrow["长边框物料代码"]);

//                        SapWo.LongFrameBatch = Convert.ToString(newdtrow["长边框批次"]);
                        SapWo.LongFrameBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["长边框批次"]))) ? "NA" : Convert.ToString(newdtrow["长边框批次"]);

//                        SapWo.SalesItemNo = Convert.ToString(newdtrow["销售订单项目"]);
                        SapWo.SalesItemNo = (string.IsNullOrEmpty(Convert.ToString(newdtrow["销售订单项目"]))) ? "NA" : Convert.ToString(newdtrow["销售订单项目"]);

//                        SapWo.SalesOrderNo = Convert.ToString(newdtrow["销售订单"]);
                        SapWo.SalesOrderNo = (string.IsNullOrEmpty(Convert.ToString(newdtrow["销售订单"]))) ? "NA" : Convert.ToString(newdtrow["销售订单"]);

//                        SapWo.ShortFrameBatch = Convert.ToString(newdtrow["短边框批次"]);
                        SapWo.ShortFrameBatch = (string.IsNullOrEmpty(Convert.ToString(newdtrow["短边框批次"]))) ? "NA" : Convert.ToString(newdtrow["短边框批次"]);

//                        SapWo.ShortFrameCode = Convert.ToString(newdtrow["短边框物料代码"]);
                        SapWo.ShortFrameCode = (string.IsNullOrEmpty(Convert.ToString(newdtrow["短边框物料代码"]))) ? "NA" : Convert.ToString(newdtrow["短边框物料代码"]);

                        SapWo.PackingMode = Convert.ToString(newdtrow["包装方式"]); 
                        SapWo.PackingMode = Convert.ToString(newdtrow["玻璃厚度"]);
                

                        SapWo.IsCancelPacking = "1";
                        SapWo.IsOnlyPacking = "1";
                        SapWo.CustomerCartonNo = Convert.ToString(newdtrow["客户托号"]);
                        SapWo.InnerJobNo = "";
                        SapWo.RESV01 = Convert.ToString(newdtrow["Market"]);
                        SapWo.RESV02 = Convert.ToString(newdtrow["LID"]);
                        SapWo.RESV03 = Convert.ToString(newdtrow["CabelLength"]);
                        SapWo.RESV04 = Convert.ToString(newdtrow["JBoxType"]);
                        //modify on rework.
                        string sqlrework = "select reworkorder from product where SN = '{0}' and reworkorder != ''";
                        sqlrework = string.Format(sqlrework,SapWo.ModuleSN);
                        SqlDataReader rd = ToolsClass.GetDataReader(sqlrework);
                        if(rd!=null && rd.Read())
                        {
                        	SapWo.OrderNo = rd.GetString(0);
                        	
                        }
                        
                        

                        var tsapReceiptUploadModule = new TsapReceiptUploadModule
                        {
                            Sysid = Guid.NewGuid().ToString(""),
                            CreatedOn = createdOn,
                            CreatedBy = createdBy,
                            GroupHistKey = groupHistKey,
                            CartonNo = SapWo.CartonNo,
                            CustomerCartonNo = SapWo.CustomerCartonNo,
                            FinishedOn = SapWo.FinishedOn,
                            //ModuleColor jacky                          	
                            ModuleColor = MC,
                            ModuleSn = SapWo.ModuleSN,
                            OrderNo = SapWo.OrderNo,
                            SalesOrderNo = SapWo.SalesOrderNo,
                            SalesItemNo = SapWo.SalesItemNo,
                            OrderStatus = SapWo.OrderStatus,
                            ProductCode = SapWo.ProductCode,
                            Unit = SapWo.Unit,
                            Factory = SapWo.Factory,
                            Workshop = SapWo.Workshop,
                            PackingLocation = SapWo.PackingLocation,
                            PackingMode = SapWo.PackingMode,
                            CellEff = SapWo.CellEff,
                            TestPower = SapWo.TestPower,
                            StdPower = SapWo.StdPower,
                            ModuleGrade = SapWo.ModuleGrade,
                            ByIm = SapWo.ByIm,
                            Tolerance = Convert.ToString(newdtrow["公差"]),
                            CellCode = SapWo.CellCode,
                            CellBatch = SapWo.CellBatch,
                            CellPrintMode = SapWo.CellPrintMode,
                            GlassCode = SapWo.GlassCode,
                            GlassBatch = SapWo.GlassBatch,
                            EvaCode = SapWo.EvaCode,
                            EvaBatch = SapWo.EvaBatch,
                            TptCode = SapWo.TptCode,
                            TptBatch = SapWo.TptBatch,
                            ConboxCode = SapWo.ConBoxCode,
                            ConboxBatch = SapWo.ConBoxBatch,
                            ConboxType = string.Empty,
                            LongFrameCode = SapWo.LongFrameCode,
                            LongFrameBatch = SapWo.LongFrameBatch,
                            ShortFrameCode = SapWo.ShortFrameCode,
                            ShortFrameBatch = SapWo.ShortFrameBatch,
                            GlassThickness = SapWo.GlassThickness,
                            IsRework = Convert.ToString(newdtrow["是否重工"]),
                            IsByProduction = "false",
                            Resv01 = Convert.ToString(newdtrow["StdPowerLevel"]),
                            Resv02 = SapWo.RESV01,
                            Resv03 = SapWo.RESV02,
                            //Resv02 = "Market",
                            //Resv03 = "LID",
                            Resv04 = SapWo.RESV03,
                            Resv05 = SapWo.RESV04,// jacky 20160203
                        };
                        tsapReceiptUploadModules.Add(tsapReceiptUploadModule);

                        _ListSapWo.Add(SapWo);
                    }
                    #endregion
                }

                if (_ListSapWo.Count > 0)
                {
                    string msg = "";
                    if (!ProductStorageDAL.CheckWoStorage(_ListSapWo, out msg))
                    {
                        setIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    if (!DataAccess.QueryReworkOrgBatch(tsapReceiptUploadModules, out msg))
                    {
                        setIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }

                    //var findAll =
                    //    tsapReceiptUploadModules.FindAll(
                    //        p =>
                    //        string.IsNullOrEmpty(p.ProductCode) || string.IsNullOrEmpty(p.OrderNo) ||
                    //        string.IsNullOrEmpty(p.CellCode) || string.IsNullOrEmpty(p.CellBatch) ||
                    //        string.IsNullOrEmpty(p.GlassCode) || string.IsNullOrEmpty(p.GlassBatch) ||
                    //        string.IsNullOrEmpty(p.EvaCode) || string.IsNullOrEmpty(p.EvaBatch) ||
                    //        string.IsNullOrEmpty(p.TptCode) || string.IsNullOrEmpty(p.TptBatch) ||
                    //        string.IsNullOrEmpty(p.ConboxCode) || string.IsNullOrEmpty(p.ConboxBatch) ||
                    //        string.IsNullOrEmpty(p.LongFrameCode) || string.IsNullOrEmpty(p.LongFrameBatch) ||
                    //        string.IsNullOrEmpty(p.ShortFrameCode) || string.IsNullOrEmpty(p.ShortFrameBatch) ||
                    //        string.IsNullOrEmpty(p.PackingMode) || string.IsNullOrEmpty(p.ModuleGrade) ||
                    //        string.IsNullOrEmpty(p.ByIm) || string.IsNullOrEmpty(p.StdPower) ||
                    //        string.IsNullOrEmpty(p.ModuleColor) || string.IsNullOrEmpty(p.Tolerance));
                    //if (findAll.Count > 0)
                    //{
                    //    const string errorMsg =
                    //        "SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、电流分档、标称功率、组件颜色、公差";
                    //    setIsEnable(true);
                    //    ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                    //    return;
                    //}

                    List<string> listMessages = SapMessages(tsapReceiptUploadModules);
                    if (listMessages.Count > 0)
                    {
                        setIsEnable(true);
                        foreach (var errorMsg in listMessages)
                        {
                            ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        }
                        return;
                    }
                    if (
                        !DataAccess.IsSapInventoryUseManulUpload(FormCover.CurrentFactory.Trim(),
                            FormCover.InterfaceConnString))
                    {
                        //入库
                        SaveXml(_ListSapWo);

                        //对上传入库成功的做log记录
                        ProductStorageDAL.SaveStorageInfoLog(_ListSapWoLog);
                    }
                    else
                    {
                        //保存数据供手动上传使用
                        SavePreUploadData(_ListSapWo, tsapReceiptUploadModules);
                    }
                    if (_ListSapWoLog.Count > 0)
                        _ListSapWoLog.Clear();
                }
                if(CbUpload.SelectedIndex == 1)
                {
#region test auto upload                	
                	try
			            {
			                if (tsapReceiptUploadModules == null || tsapReceiptUploadModules.Count <= 0)
			                {
			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message59","未找到待上传数据"), "ABNORMAL", lstView);
			                    return;
			                }
			                var cartonNos = tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
			                foreach (var cartonNo in cartonNos)
			                {
			                    string msg;
			                    if (!DataAccess.CanUploadToSap(cartonNo, FormCover.connectionBase, out msg))
			                    {
			                        ToolsClass.Log(msg, "ABNORMAL", lstView);
			                        return;
			                    }
			                }
			               
			                
			                   var jobNo = "NULL";
			     
			                //var dateTime = DT.DateTime().LongDateTime2;//Jacky
			                var dateTime = DT.DateTime().ShortDate;                
			                var groupHistKey1 = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
			                tsapReceiptUploadModules.ForEach(p =>
			                {
			                    p.CreatedOn = dateTime;
			                    p.CreatedBy = FormCover.CurrUserName;
			                    p.JobNo = jobNo;
			                    p.GroupHistKey = groupHistKey;
			                    p.UploadStatus = "Finished";
			                });
			                var tsapReceiptUploadJobNo = new TsapReceiptUploadJobNo
			                {
			                    Sysid = Guid.NewGuid().ToString("N"),
			                    CreatedOn = dateTime,
			                    CreatedBy = FormCover.CurrUserName,
			                    JobNo = jobNo,
			                    InnerJobNo = string.Empty,
			                    GroupHistKey = groupHistKey,
			                    Resv01 = FormCover.CurrentFactory
			                };
			                if (!UploadToEsb(tsapReceiptUploadModules))
			                    return;
			                var actionTxnId = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");
			                //var actionDate = DT.DateTime().LongDateTime; //jacky
			              	var actionDate = DT.DateTime().ShortDate;
			               
			                if (!ProductStorageDAL.SaveUploadEsbResult(tsapReceiptUploadJobNo, tsapReceiptUploadModules, actionTxnId, actionDate))
			                {
			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message60","保存上传结果失败"), "ABNORMAL", lstView);
			                    return;
			                }
			                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message61","上传成功"), "NORMAL", lstView);
			               
			            }
			            catch (Exception ex)
			            {
			                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message62", "上传出错：") + ex.Message, "ABNORMAL", lstView);
			            }
                }
#endregion
                setIsEnable(true);
                this.chbSelected.Checked = false;

                #endregion
            }
            catch (Exception ex)
            {
                setIsEnable(true);
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message30", "上传SAP时候发生异常(处理包装数据):") + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }
        private List<string> SapMessages(List<TsapReceiptUploadModule> sapReceipt)
        {
            List<string> messages = new List<string>();
            try
            {

                //"SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、
                //玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、
                //接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、
                //电流分档、标称功率、组件颜色、公差";

                if (sapReceipt != null && sapReceipt.Count > 0)
                {
                    StringBuilder strtmp;
                    int i = 1;
                    foreach (TsapReceiptUploadModule sapModule in sapReceipt)
                    {
                        strtmp = new StringBuilder();
                        bool blntmp = true;

                        if (!string.IsNullOrEmpty(sapModule.OrderNo))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message31", "工单号:" )+ sapModule.OrderNo + "--");
                        }
                        else
                        {
                            strtmp.Append(string.Format(LanguageHelper.GetMessage(LanMessList, "Message32", "【第{0}行工单号为空】"), i.ToString()));
                           // blntmp = false;
                        }
                        if (!string.IsNullOrEmpty(sapModule.ModuleSn))
                        {
                            strtmp.Append(string.Format(LanguageHelper.GetMessage(LanMessList, "Message33", "序列号{0}缺失的值【:"), sapModule.ModuleSn)); ;

                        }
                        if (string.IsNullOrEmpty(sapModule.ProductCode))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message34", "产品物料代码、"));
							//blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellCode))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message35", "电池片物料号、"));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellBatch))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message36","电池片批次、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassCode))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message37","玻璃批次、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassBatch))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message38","EVA物料号、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaCode))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message39","EVA批次、" ));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaBatch))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message40","背板物料号、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptCode))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message41", "背板批次、"));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptBatch))
                        {
                           	strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message42", "电池片物料号、"));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxCode))
                        {
                             strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message43","接线盒物料号、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxBatch))
                        {
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message44", "接线盒批次、"));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameCode))
                        {
                           // strtmp.Append("长边框物料号、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message45","长边框物料号、" ));
                           // blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameBatch))
                        {
                           // strtmp.Append("长边框批次、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message46","长边框批次、" ));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameCode))
                        {
                            //strtmp.Append("短边框物料号、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message47", "短边框物料号、"));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameBatch))
                        {
                            //strtmp.Append("短边框批次、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message48", "短边框批次、"));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.PackingMode))
                        {
                            //strtmp.Append("包装方式、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message49","包装方式、" ));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleGrade))
                        {
                            //strtmp.Append("组件等级、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message50","组件等级、" ));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ByIm))
                        {
                            //strtmp.Append("电流分档、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message51","电流分档、" ));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.StdPower))
                        {
                           // strtmp.Append("标称功率、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message52", "标称功率、"));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleColor))
                        {
                            //strtmp.Append("组件颜色、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message53", "组件颜色、"));
                            //blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.Tolerance))
                        {
                            //strtmp.Append("公差、");
                            strtmp.Append(LanguageHelper.GetMessage(LanMessList, "Message54","公差、" ));
                           // blntmp = false;
                        }
                        if (strtmp != null && !string.IsNullOrEmpty(strtmp.ToString()) && blntmp == false)
                        {
                           // messages.Add(strtmp + "】 【第 " + i.ToString() + " 行】");
                            messages.Add(strtmp + string.Format(LanguageHelper.GetMessage(LanMessList, "Message55", "】 【第 {0}行】"), i.ToString()));
                        }
                        i = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add(LanguageHelper.GetMessage(LanMessList, "Message56", "SAP 数据检查异常:") + ex.Message.ToString());
            }
            return messages;
        }
        private void FormNormalStorage_Load(object sender, EventArgs e)
        {
    
         
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion   
            
            
            string temp = LanguageHelper.GetMessage(LanMessList, "Message57", "提示信息");                
			this.lstView.Columns.Add(temp, 630, HorizontalAlignment.Left);
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
        
        void BtnQueryClick(object sender, EventArgs e)
        {
        		string strTemp = txtCarton.Text;
                string[] sDataSet = strTemp.Split('\n');
                 for (int i = 0; i < sDataSet.Length; i++)
                {
	                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
	                    if (sDataSet[i] != null && sDataSet[i] != "")
	                    {
//			                    if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
//			                {
//			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "托号不能为空!"), "ABNORMAL", lstView);
//			                    this.txtCarton.SelectAll();
//			                    this.txtCarton.Focus();
//			                    return;
//			                }
//			                DataTable dt = ProductStorageDAL.GetCartonStorageInfoByCarton(Convert.ToString(this.txtCarton.Text.Trim()));
//			                if (dt != null && dt.Rows.Count > 0)
//			                    SetDataGridView(dt);
//			                else
//			                {
//			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2","没有查询到数据!" ), "ABNORMAL", lstView);
//			                    this.txtCarton.SelectAll();
//			                    this.txtCarton.Focus();
//			                    return;
//			                }
	                    	
	                    	if (sDataSet[i].Equals(""))
			                {
			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "托号不能为空!"), "ABNORMAL", lstView);
			                    this.txtCarton.SelectAll();
			                    this.txtCarton.Focus();
			                    return;
			                }
	                    	DataTable dt = ProductStorageDAL.GetCartonStorageInfoByCarton(sDataSet[i]);
			                if (dt != null && dt.Rows.Count > 0)
			                    SetDataGridView(dt);
			                else
			                {
			                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2","没有查询到数据!" ), "ABNORMAL", lstView);
			                    this.txtCarton.SelectAll();
			                    this.txtCarton.Focus();
			                    return;
			                }
	                    }
                }
        }
          private bool IsUploadedToSap(string jobNo)
        {
            //http://xxx:xx/QuerySAPInventoryStatus?workshop=?&jobno=?&innerjobno=?&datefrom=?&dateto=?
            var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +
                      "QuerySAPInventoryStatus?workshop=" + FormCover.CurrentFactory +
                      "&jobno=" + jobNo;
            var req = WebRequest.Create(url);
            ToolsClass.Log("IsUploadedToSap::" + url);
            req.Method = "GET";
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message63", "服务器反馈结果为空"), "ABNORMAL", lstView);
                    return false;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = LanguageHelper.GetMessage(LanMessList, "Message64", "连接错误");
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("IsUploadedToSap::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<SOAP:BODY>", @"<SOAPBODY>");
                result = result.Replace(@"</SOAP:BODY>", @"</SOAPBODY>");
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message65","服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                }
                var responseStatusTable = ds.Tables["RESPONSESTATUS"];
                if (responseStatusTable == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message65","服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                }
                var statusCode = responseStatusTable.Rows[0][0].ToString();
                var statusMessage = responseStatusTable.Rows[0][1].ToString();
                if (statusCode == "000")
                {
                    return ds.Tables["ITEM"] != null;
                }
                else
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message66", "查询柜号") + jobNo + LanguageHelper.GetMessage(LanMessList, "Message15", "历史上传信息出错::") + statusMessage, "ABNORMAL", lstView);
                    return false;
                }
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return false;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return false;
            }
        }

        private bool UploadToEsb(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            //var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +"SAPInventory"; 
           	var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +"CanadianSolar/QuerySAPInventoryStatus"; 
			
           	
            //var url = "http://10.127.33.33:8188/CanadianSolar/QuerySAPInventoryStatus";    
            //MessageBox.Show("url="+url);
            //var req = WebRequest.Create(url);
            //HttpWebRequest req=(HttpWebRequest)WebRequest.Create(url);
            var req =WebRequest.Create(url);
            req.Timeout = 15000;
            //MessageBox.Show(req.ToString());
            
            ToolsClass.Log("UploadToEsb::" + url);
            req.Method = "POST";
            var soap = BuildSoapXml(tsapReceiptUploadModules);
           //req.ContentType = "text/plain";
            req.ContentType = "text/xml;charset=UTF-8";
            
           // req.SendChunked=true;
           // req.TransferEncoding="utf-8";
          
           
            ToolsClass.Log("UploadToEsb::" + soap);
            //字符转字节
            var bytes = Encoding.UTF8.GetBytes(soap); 

//            using (System.IO.StreamWriter file =
//		            new System.IO.StreamWriter(@"C:\Users\Jacky.li\Documents\Jacky\WriteLineSBYTES.txt"))
//		        {		           
//                	file.WriteLine(bytes.ToString());	             
//		           
//		        }            

            var writer = req.GetRequestStream();
            writer.Write(bytes, 0, bytes.Length);
            writer.Flush();
            writer.Close();
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message63", "服务器反馈结果为空"), "ABNORMAL", lstView);
                    return false;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = LanguageHelper.GetMessage(LanMessList, "Message64", "连接错误");
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("UploadToEsb::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<ENTERWAREHOUSERESPONSE XMLNS=""HTTP://ESB.CANADIANSOLAR.COM"">",@"<ENTERWAREHOUSERESPONSE>");
                result = result.Replace(@"<SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                //yuhua
                result = result.Replace(@"<RETURN>", string.Empty);
				result = result.Replace(@"</RETURN>", string.Empty);

                #region testing
//		                using (System.IO.StreamWriter file = 
//		            new System.IO.StreamWriter(@"C:\Users\Jacky.li\Documents\Jacky\WriteLineschina.txt"))
//		        {
//		           
//                	file.WriteLine(result.ToString());
//		                
//		            
//		        }
                #endregion
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message65", "服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                    
                }
                var row = ds.Tables[0].Rows[0];
                var isSuccess = row[0].ToString().ToUpper();
                var message = row[1].ToString();
                if (isSuccess != "true".ToUpper())
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message62", "上传失败::") + message, "ABNORMAL", lstView);
                    return false;
                }
                return true;
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return false;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return false;
            }
        }

        private string BuildSoapXml(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            var cartons =
                tsapReceiptUploadModules.Select(
                    p => new { p.CartonNo, p.CustomerCartonNo, p.InnerJobNo, p.JobNo, p.ModuleColor }).
                    Distinct().ToList();

            
           // Encoding Utf8 = new UTF8Encoding(false);     //Initializes a new instance of the UTF8Encoding class. A parameter specifies whether to provide a Unicode byte order mark.  
		         
            
            var st = new MemoryStream();
            //初始化一个XmlTextWriter,编码方式为Encoding.UTF8
            
            var tr = new XmlTextWriter(st, Encoding.UTF8);
            tr.WriteStartDocument();
			
            tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Envelope
           //tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/wsdl");//soap:Envelope
            tr.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");

            tr.WriteStartElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Header
            tr.WriteStartElement(null, "RequestHeader", "http://esb.canadiansolar.com");//RequestHeader ***changed this from NULL
            tr.WriteElementString("AppCode", "PackingSystem_" + FormCover.CurrentFactory);
            tr.WriteElementString("Version", "1");
            tr.WriteElementString("ClientTimestamp", tsapReceiptUploadModules[0].CreatedOn);
            tr.WriteElementString("GUID", tsapReceiptUploadModules[0].GroupHistKey);
            tr.WriteEndElement();//RequestHeader
            tr.WriteEndElement();//soap:Header

            tr.WriteStartElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Body
            tr.WriteStartElement(null, "EnterWarehouse", "http://esb.canadiansolar.com");//EnterWarehouse ***changed this from NULL
            tr.WriteStartElement(null, "PackingData", null);//PackingData
            //循环Carton
            for (var i = 0; i < cartons.Count; i++)
            {
                var carton = cartons[i];
                var findByCartonNo =
                    tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo)
                        .OrderBy(p => p.FinishedOn)
                        .Take(1).FirstOrDefault();
                
                tr.WriteStartElement(null, "Carton", null); //Carton
                tr.WriteElementString("CartonNo", carton.CartonNo);
                tr.WriteElementString("CustomerCartonNo", carton.CustomerCartonNo);
                tr.WriteElementString("InnerJobNo", carton.InnerJobNo);
                tr.WriteElementString("JobNo", carton.JobNo);
                tr.WriteElementString("FinishedOn", findByCartonNo.FinishedOn);
                //tr.WriteElementString("FinishedOn", DTpick.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                tr.WriteElementString("ModuleColor", carton.ModuleColor);
                tr.WriteStartElement(null, "Modules", null); //Modules
                //循环Module
                var modules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo);
                foreach (var module in modules)
                {
                    tr.WriteStartElement(null, "Module", null); //Module
                    tr.WriteElementString("ModuleSN", module.ModuleSn);
                    tr.WriteElementString("OrderNo", module.OrderNo);
                    tr.WriteElementString("SalesOrderNo", module.SalesOrderNo);
                    tr.WriteElementString("SalesItemNo", module.SalesItemNo);
                    tr.WriteElementString("OrderStatus", module.OrderStatus);
                    tr.WriteElementString("ProductCode", module.ProductCode);
                    tr.WriteElementString("Unit", module.Unit);
                    tr.WriteElementString("Factory", module.Factory);
                    tr.WriteElementString("Workshop", module.Workshop);
                    tr.WriteElementString("PackingLocation", module.PackingLocation);
                    tr.WriteElementString("PackingMode", module.PackingMode);
                    tr.WriteElementString("CellEff", module.CellEff);
                    tr.WriteElementString("TestPower", module.TestPower);
                    tr.WriteElementString("StdPower", module.StdPower);
                    tr.WriteElementString("ModuleGrade", module.ModuleGrade);
                    tr.WriteElementString("ByIm", module.ByIm);
                    tr.WriteElementString("Tolerance", module.Tolerance);
                    tr.WriteElementString("CellCode", module.CellCode);
                    tr.WriteElementString("CellBatch", module.CellBatch);
                    tr.WriteElementString("CellPrintMode", module.CellPrintMode);
                    tr.WriteElementString("GlassCode", module.GlassCode);
                    tr.WriteElementString("GlassBatch", module.GlassBatch);
                    tr.WriteElementString("EvaCode", module.EvaCode);
                    tr.WriteElementString("EvaBatch", module.EvaBatch);
                    tr.WriteElementString("TptCode", module.TptCode);
                    tr.WriteElementString("TptBatch", module.TptBatch);
                    tr.WriteElementString("ConBoxCode", module.ConboxCode);
                    tr.WriteElementString("ConBoxBatch", module.ConboxBatch);
                    tr.WriteElementString("ConBoxType", module.ConboxType);
                    tr.WriteElementString("LongFrameCode", module.LongFrameCode);
                    tr.WriteElementString("LongFrameBatch", module.LongFrameBatch);
                    tr.WriteElementString("ShortFrameCode", module.ShortFrameCode);
                    tr.WriteElementString("ShortFrameBatch", module.ShortFrameBatch);
                    tr.WriteElementString("GlassThickness", module.GlassThickness);
                    tr.WriteElementString("IsRework", module.IsRework);
                    tr.WriteElementString("IsByProduction", module.IsByProduction);
                    tr.WriteElementString("StdPowerLevel", module.Resv01);
                    tr.WriteElementString("Market", module.Resv02);
                    tr.WriteElementString("LID", module.Resv03); //jakcy20160203

                    tr.WriteEndElement(); //Module
                }
                tr.WriteEndElement(); //Modules
                tr.WriteEndElement(); //Carton 
            }

            tr.WriteEndElement();//PackingData
            tr.WriteEndElement();//EnterWarehouse
            tr.WriteEndElement();//soap:Body

            tr.WriteEndElement();//soap:Envelope

            tr.WriteEndDocument();

            tr.Flush();

            var bytes = st.ToArray();
            return Encoding.UTF8.GetString(bytes);
        }
    }
}
