﻿namespace CSICPR
{
    partial class FormBusyMSG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLoading = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbLoading
            // 
            this.lbLoading.BackColor = System.Drawing.SystemColors.Control;
            this.lbLoading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbLoading.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbLoading.Image = global::CSICPR.Properties.Resources.LoadSoftList;
            this.lbLoading.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbLoading.Location = new System.Drawing.Point(0, 0);
            this.lbLoading.Name = "lbLoading";
            this.lbLoading.Size = new System.Drawing.Size(200, 38);
            this.lbLoading.TabIndex = 13;
            this.lbLoading.Text = "正在处理，请稍候…";
            this.lbLoading.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FormBusyMSG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(200, 38);
            this.Controls.Add(this.lbLoading);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBusyMSG";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormBusyMSG";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbLoading;
    }
}