﻿namespace CSICPR
{
    partial class FormNormalStorage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNormalStorage));
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.panel3 = new System.Windows.Forms.Panel();
        	this.GpbCartonQuery = new System.Windows.Forms.GroupBox();
        	this.chbSelected = new System.Windows.Forms.CheckBox();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        	this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CartonID = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.CartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.SNQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.panel4 = new System.Windows.Forms.Panel();
        	this.label1 = new System.Windows.Forms.Label();
        	this.CbUpload = new System.Windows.Forms.ComboBox();
        	this.PicBoxCarton = new System.Windows.Forms.PictureBox();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.button1 = new System.Windows.Forms.Button();
        	this.Save = new System.Windows.Forms.Button();
        	this.CartonQueryList = new System.Windows.Forms.Panel();
        	this.txtCarton = new System.Windows.Forms.TextBox();
        	this.label2 = new System.Windows.Forms.Label();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.panel3.SuspendLayout();
        	this.GpbCartonQuery.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.panel4.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxCarton)).BeginInit();
        	this.CartonQueryList.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// panel1
        	// 
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(895, 17);
        	this.panel1.TabIndex = 3;
        	// 
        	// panel3
        	// 
        	this.panel3.Controls.Add(this.GpbCartonQuery);
        	this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.panel3.Location = new System.Drawing.Point(0, 17);
        	this.panel3.Name = "panel3";
        	this.panel3.Size = new System.Drawing.Size(895, 572);
        	this.panel3.TabIndex = 4;
        	// 
        	// GpbCartonQuery
        	// 
        	this.GpbCartonQuery.Controls.Add(this.chbSelected);
        	this.GpbCartonQuery.Controls.Add(this.dataGridView1);
        	this.GpbCartonQuery.Controls.Add(this.panel4);
        	this.GpbCartonQuery.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.GpbCartonQuery.Location = new System.Drawing.Point(0, 0);
        	this.GpbCartonQuery.Name = "GpbCartonQuery";
        	this.GpbCartonQuery.Size = new System.Drawing.Size(895, 572);
        	this.GpbCartonQuery.TabIndex = 3;
        	this.GpbCartonQuery.TabStop = false;
        	this.GpbCartonQuery.Text = "托号查询";
        	// 
        	// chbSelected
        	// 
        	this.chbSelected.AutoSize = true;
        	this.chbSelected.BackColor = System.Drawing.SystemColors.Control;
        	this.chbSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
        	this.chbSelected.Location = new System.Drawing.Point(19, 185);
        	this.chbSelected.Name = "chbSelected";
        	this.chbSelected.Size = new System.Drawing.Size(15, 14);
        	this.chbSelected.TabIndex = 49;
        	this.chbSelected.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
        	this.chbSelected.UseVisualStyleBackColor = false;
        	this.chbSelected.CheckedChanged += new System.EventHandler(this.chbSelected_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToResizeRows = false;
        	dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
        	this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
        	this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.Selected,
        	        	        	this.RowIndex,
        	        	        	this.CartonID,
        	        	        	this.CartonStatus,
        	        	        	this.CustomerCartonNo,
        	        	        	this.SNQTY,
        	        	        	this.Column1});
        	dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
        	dataGridViewCellStyle3.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
        	dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        	this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
        	this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridView1.Location = new System.Drawing.Point(3, 180);
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.RowHeadersVisible = false;
        	this.dataGridView1.RowTemplate.Height = 23;
        	this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.dataGridView1.Size = new System.Drawing.Size(889, 389);
        	this.dataGridView1.TabIndex = 47;
        	this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
        	// 
        	// Selected
        	// 
        	this.Selected.HeaderText = "";
        	this.Selected.Name = "Selected";
        	this.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        	this.Selected.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        	this.Selected.Width = 40;
        	// 
        	// RowIndex
        	// 
        	dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        	this.RowIndex.DefaultCellStyle = dataGridViewCellStyle2;
        	this.RowIndex.HeaderText = "序号";
        	this.RowIndex.Name = "RowIndex";
        	this.RowIndex.ReadOnly = true;
        	this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	this.RowIndex.Width = 35;
        	// 
        	// CartonID
        	// 
        	this.CartonID.HeaderText = "内部托号";
        	this.CartonID.Name = "CartonID";
        	this.CartonID.ReadOnly = true;
        	this.CartonID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        	this.CartonID.Width = 120;
        	// 
        	// CartonStatus
        	// 
        	this.CartonStatus.HeaderText = "托号状态";
        	this.CartonStatus.Name = "CartonStatus";
        	this.CartonStatus.ReadOnly = true;
        	this.CartonStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// CustomerCartonNo
        	// 
        	this.CustomerCartonNo.HeaderText = "客户托号";
        	this.CustomerCartonNo.Name = "CustomerCartonNo";
        	this.CustomerCartonNo.Width = 120;
        	// 
        	// SNQTY
        	// 
        	this.SNQTY.HeaderText = "组件数量";
        	this.SNQTY.Name = "SNQTY";
        	this.SNQTY.ReadOnly = true;
        	this.SNQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// Column1
        	// 
        	this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
        	this.Column1.HeaderText = "";
        	this.Column1.Name = "Column1";
        	// 
        	// panel4
        	// 
        	this.panel4.Controls.Add(this.label1);
        	this.panel4.Controls.Add(this.CbUpload);
        	this.panel4.Controls.Add(this.PicBoxCarton);
        	this.panel4.Controls.Add(this.btnQuery);
        	this.panel4.Controls.Add(this.button1);
        	this.panel4.Controls.Add(this.Save);
        	this.panel4.Controls.Add(this.CartonQueryList);
        	this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel4.Location = new System.Drawing.Point(3, 16);
        	this.panel4.Name = "panel4";
        	this.panel4.Size = new System.Drawing.Size(889, 164);
        	this.panel4.TabIndex = 48;
        	this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(453, 18);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(71, 13);
        	this.label1.TabIndex = 95;
        	this.label1.Text = "UploadtoSAP";
        	// 
        	// CbUpload
        	// 
        	this.CbUpload.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.CbUpload.FormattingEnabled = true;
        	this.CbUpload.Items.AddRange(new object[] {
        	        	        	"Manual",
        	        	        	"Auto"});
        	this.CbUpload.Location = new System.Drawing.Point(530, 12);
        	this.CbUpload.Name = "CbUpload";
        	this.CbUpload.Size = new System.Drawing.Size(131, 21);
        	this.CbUpload.TabIndex = 168;
        	// 
        	// PicBoxCarton
        	// 
        	this.PicBoxCarton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCarton.BackgroundImage")));
        	this.PicBoxCarton.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCarton.InitialImage")));
        	this.PicBoxCarton.Location = new System.Drawing.Point(400, 14);
        	this.PicBoxCarton.Name = "PicBoxCarton";
        	this.PicBoxCarton.Size = new System.Drawing.Size(27, 20);
        	this.PicBoxCarton.TabIndex = 167;
        	this.PicBoxCarton.TabStop = false;
        	this.PicBoxCarton.Visible = false;
        	this.PicBoxCarton.Click += new System.EventHandler(this.PicBoxCarton_Click);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(311, 10);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(83, 24);
        	this.btnQuery.TabIndex = 165;
        	this.btnQuery.Text = "Add To List";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.BtnQueryClick);
        	// 
        	// button1
        	// 
        	this.button1.Location = new System.Drawing.Point(322, 122);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(72, 26);
        	this.button1.TabIndex = 164;
        	this.button1.Tag = "button1";
        	this.button1.Text = "重置";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.button1_Click);
        	// 
        	// Save
        	// 
        	this.Save.Location = new System.Drawing.Point(416, 122);
        	this.Save.Name = "Save";
        	this.Save.Size = new System.Drawing.Size(72, 28);
        	this.Save.TabIndex = 50;
        	this.Save.Tag = "button1";
        	this.Save.Text = "入库";
        	this.Save.UseVisualStyleBackColor = true;
        	this.Save.Click += new System.EventHandler(this.Save_Click);
        	// 
        	// CartonQueryList
        	// 
        	this.CartonQueryList.Controls.Add(this.txtCarton);
        	this.CartonQueryList.Controls.Add(this.label2);
        	this.CartonQueryList.Location = new System.Drawing.Point(3, 8);
        	this.CartonQueryList.Name = "CartonQueryList";
        	this.CartonQueryList.Size = new System.Drawing.Size(302, 150);
        	this.CartonQueryList.TabIndex = 93;
        	// 
        	// txtCarton
        	// 
        	this.txtCarton.Location = new System.Drawing.Point(147, 4);
        	this.txtCarton.Multiline = true;
        	this.txtCarton.Name = "txtCarton";
        	this.txtCarton.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        	this.txtCarton.Size = new System.Drawing.Size(148, 143);
        	this.txtCarton.TabIndex = 94;
        	this.txtCarton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCartonQuery_KeyDown);
        	this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCartonQuery_KeyPress);
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(1, 8);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(64, 13);
        	this.label2.TabIndex = 94;
        	this.label2.Text = "内 部 托 号";
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.lstView);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.panel2.Location = new System.Drawing.Point(0, 589);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(895, 100);
        	this.panel2.TabIndex = 5;
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(0, 0);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(895, 100);
        	this.lstView.TabIndex = 1;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// FormNormalStorage
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(895, 689);
        	this.Controls.Add(this.panel3);
        	this.Controls.Add(this.panel1);
        	this.Controls.Add(this.panel2);
        	this.Name = "FormNormalStorage";
        	this.Text = "批量入库";
        	this.Load += new System.EventHandler(this.FormNormalStorage_Load);
        	this.panel3.ResumeLayout(false);
        	this.GpbCartonQuery.ResumeLayout(false);
        	this.GpbCartonQuery.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.panel4.ResumeLayout(false);
        	this.panel4.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxCarton)).EndInit();
        	this.CartonQueryList.ResumeLayout(false);
        	this.CartonQueryList.PerformLayout();
        	this.panel2.ResumeLayout(false);
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.ComboBox CbUpload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQuery;

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.GroupBox GpbCartonQuery;
        private System.Windows.Forms.CheckBox chbSelected;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewLinkColumn CartonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Panel CartonQueryList;
        private System.Windows.Forms.PictureBox PicBoxCarton;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
    }
}