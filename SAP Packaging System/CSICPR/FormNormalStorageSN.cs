﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormNormalStorageSN : Form
    {
        // private List<string> _ListCartonSucess = new List<string>();
        private static List<LanguageItemModel> LanMessList;//定义语言集

        public FormNormalStorageSN()
        {
            InitializeComponent();
        }

        private void SNStorage_Load(object sender, EventArgs e)
        {
            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, "转常规工单");
            this.ddlWoType.Items.Insert(2, "外协后工单");
            this.ddlWoType.Items.Insert(3, "返工工单");
            this.ddlWoType.SelectedIndex = 0;

            //提示信息
            this.lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            //界面Grid列头禁止排序
            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            //界面Gird列头添加checkbox按钮
            HeadCheckBox.datagridviewCheckboxHeaderCell ch = new HeadCheckBox.datagridviewCheckboxHeaderCell();
            ch.OnCheckBoxClicked += new HeadCheckBox.datagridviewCheckboxHeaderCell.HeaderEventHander(ch_OnCheckBoxClicked);
            DataGridViewCheckBoxColumn checkboxCol = this.dataGridView1.Columns[0] as DataGridViewCheckBoxColumn;
            checkboxCol.HeaderCell = ch;
            checkboxCol.HeaderCell.Value = string.Empty;

            this.txtCarton.Focus();

            this.checkBox2.Checked = false;
            this.txtWO1.Enabled = false;
            this.pictureBox2.Enabled = false;

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlWoType);
            # endregion
        }

        #region 公有变量
        private static FormNormalStorageSN theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormNormalStorageSN();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        /// <summary>
        /// 工单类型
        /// </summary>
        private string WoType = "";
        /// <summary>
        /// 工单状态
        /// </summary>
        private string WoStatus = "";
        /// <summary>
        /// 记录选中的记录
        /// </summary>
        private Dictionary<int, string> dgvIndex = new Dictionary<int, string>();
        /// <summary>
        /// 设置：记录选中的组件号
        /// </summary>
        private List<string> SNArrayS = new List<string>();
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        /// <summary>
        /// 电流分档
        /// </summary>
        private string ByIm_flag = "";
        /// <summary>
        /// 撤销入库标识
        /// </summary>
        private string _CancelStorageFlag = "";
        /// <summary>
        /// 是否拼托
        /// </summary>
        private string _IsOnlyPacking = "";
        /// <summary>
        /// 包装方式
        /// </summary>
        private string PackingPatternValue = "";
        #endregion

        #region 私有方法

        /// <summary>
        /// 工单：初始化界面
        /// </summary>
        private void ClearData(bool flag)
        {
            this.txtWo.Clear();
            this.txtFactory.Clear();
            this.txtMitemCode.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();
            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();

            if (flag)
                this.ddlWoType.SelectedIndex = -1;

            this.checkBox2.Checked = false;
            this.txtWO1.Clear();
            this.txtWO1.Enabled = false;
            this.pictureBox2.Enabled = false;
        }

        #endregion

        #region 托号查询
        /// <summary>
        /// 托号查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CartonQuery()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            string CartonNo = Convert.ToString(this.txtCarton.Text.Trim());
            if (CartonNo.Equals(""))
            {
                ToolsClass.Log("托号不能为空!", "ABNORMAL", lstView);
                this.txtCarton.SelectAll();
                this.txtCarton.Focus();
                return;
            }
            else
            {
                DataTable dt = ProductStorageDAL.GetSAPCartonStorageInfo(CartonNo);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (this.dataGridView1.Rows.Count > 0)
                        this.dataGridView1.Rows.Clear();
                    int RowNo = 0;
                    int RowIndex = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[RowNo].Cells["ModuleSN"].Value = Convert.ToString(row["组件序列号"]);
                        this.dataGridView1.Rows[RowNo].Cells["OrderNo"].Value = Convert.ToString(row["生产订单号"]);
                        this.dataGridView1.Rows[RowNo].Cells["CartonNo"].Value = Convert.ToString(row["内部托号"]);
                        this.dataGridView1.Rows[RowNo].Cells["CustomerCartonNo"].Value = Convert.ToString(row["客户托号"]);
                        this.dataGridView1.Rows[RowNo].Cells["TestPower"].Value = Convert.ToString(row["实测功率"]);
                        this.dataGridView1.Rows[RowNo].Cells["StdPower"].Value = Convert.ToString(row["标称功率"]);
                        this.dataGridView1.Rows[RowNo].Cells["ModuleGrade"].Value = Convert.ToString(row["组件等级"]);
                        this.dataGridView1.Rows[RowNo].Cells["ProductCode"].Value = Convert.ToString(row["产品物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["FinishedOn"].Value = Convert.ToString(row["包装日期"]);
                        this.dataGridView1.Rows[RowNo].Cells["Factory"].Value = Convert.ToString(row["工厂"]);
                        this.dataGridView1.Rows[RowNo].Cells["Workshop"].Value = Convert.ToString(row["车间"]);
                        this.dataGridView1.Rows[RowNo].Cells["PackingLocation"].Value = Convert.ToString(row["入库地点"]);
                        this.dataGridView1.Rows[RowNo].Cells["CellEff"].Value = Convert.ToString(row["电池片转换效率"]);
                        this.dataGridView1.Rows[RowNo].Cells["ByIm"].Value = Convert.ToString(row["电流分档"]);
                        this.dataGridView1.Rows[RowNo].Cells["OrderStatus"].Value = "1";
                        this.dataGridView1.Rows[RowNo].Cells["CellCode"].Value = Convert.ToString(row["电池片物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["CellBatch"].Value = Convert.ToString(row["电池片批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["CellPrintMode"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["GlassCode"].Value = Convert.ToString(row["玻璃物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["GlassBatch"].Value = Convert.ToString(row["玻璃批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["EvaCode"].Value = Convert.ToString(row["EVA 物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["EvaBatch"].Value = Convert.ToString(row["EVA 批次号"]);
                        this.dataGridView1.Rows[RowNo].Cells["TptCode"].Value = Convert.ToString(row["TPT物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["TptBatch"].Value = Convert.ToString(row["TPT 批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["ConBoxCode"].Value = Convert.ToString(row["接线盒物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["ConBoxBatch"].Value = Convert.ToString(row["接线盒批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["LongFrameCode"].Value = Convert.ToString(row["长边框物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["LongFrameBatch"].Value = Convert.ToString(row["长边框批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["GlassThickness"].Value = "";
                        this.dataGridView1.Rows[RowNo].Cells["PackingMode"].Value = Convert.ToString(row["包装方式"]);
                        this.dataGridView1.Rows[RowNo].Cells["ShortFrameCode"].Value = Convert.ToString(row["短边框物料代码"]);
                        this.dataGridView1.Rows[RowNo].Cells["shortFrameBatch"].Value = Convert.ToString(row["短边框批次"]);
                        this.dataGridView1.Rows[RowNo].Cells["IsCancelPacking"].Value = "1";
                        this.dataGridView1.Rows[RowNo].Cells["SalesOrderNo"].Value = Convert.ToString(row["销售订单"]);
                        this.dataGridView1.Rows[RowNo].Cells["SalesItemNo"].Value = Convert.ToString(row["销售订单项目"]);
                        this.dataGridView1.Rows[RowNo].Cells["IsOnlyPacking"].Value = "1";
                        this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                        RowNo++;
                        RowIndex++;
                    }
                    this.dataGridView1.CurrentCell = this.dataGridView1.Rows[0].Cells[3];
                    this.txtCarton.Clear();
                }
                else
                {
                    ToolsClass.Log("托号不存在或者已经入过库!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
            }
        }
        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                CartonQuery();
            }
        }

        private void txtCarton_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114)
            {
                pictureBox1_Click(sender, e);
            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var frm = new FormCartonInfo(this.txtCarton.Text.Trim());
            frm.ShowDialog();
            this.txtCarton.Text = frm.Carton;
            if (frm != null)
                frm.Dispose();
            CartonQuery();
        }
        #endregion

        #region 页面事件

        private void CartonReset_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            this.txtCarton.Text = "";
            this.txtCarton.Focus();

        }

        private void SetIsEnable(bool flag)
        {
            if (flag)
            {
                this.SapSave.Enabled = true;
                this.Set.Enabled = true;
                this.Reset.Enabled = true;

                this.ddlWoType.Enabled = true;
                this.txtWoOrder.Enabled = true;
                this.PicBoxWO.Enabled = true;

                this.txtCarton.Enabled = true;
                this.pictureBox1.Enabled = true;
                this.CartonReset.Enabled = true;
            }
            else
            {
                this.SapSave.Enabled = false;
                this.Set.Enabled = false;
                this.Reset.Enabled = false;

                this.ddlWoType.Enabled = false;
                this.txtWoOrder.Enabled = false;
                this.PicBoxWO.Enabled = false;

                this.txtCarton.Enabled = false;
                this.pictureBox1.Enabled = false;
                this.CartonReset.Enabled = false;
            }
        }
        /// <summary>
        /// sap入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SapSave_Click(object sender, EventArgs e)
        {
            try
            {
                SetIsEnable(false);

                #region
                if (this.dataGridView1.Rows.Count < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有要入库的数据!", "ABNORMAL", lstView);
                    return;
                }

                if (_ListSapWo.Count > 0)
                    _ListSapWo.Clear();

                string carton = "";
                DateTime CurrentTime = DateTime.Now;
                string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期
                string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码

                var tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
                var createdOn = DT.DateTime().LongDateTime;
                var createdBy = FormCover.CurrUserName;
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
                for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                {
                    if (carton.Equals(""))
                        carton = Convert.ToString(this.dataGridView1.Rows[i].Cells["CartonNo"].Value).Trim();

                    string SN = Convert.ToString(this.dataGridView1.Rows[i].Cells["ModuleSN"].Value).Trim();

                    //判断此组件是否是为返工工单
                    string ReworkWo = Convert.ToString(this.dataGridView1.Rows[i].Cells["ReworkWo"].Value).Trim();
                    if (ReworkWo.Equals("")) //空表示非返工工单
                    {
                        int stroageStatus = ProductStorageDAL.GetSNStatus(SN);
                        if (stroageStatus == 0)
                        {
                            SetIsEnable(true);
                            ToolsClass.Log("组件号 " + SN + " 已经入库或者没打包!", "ABNORMAL", lstView);
                            return;
                        }
                        else
                        {
                            int configStatus = ProductStorageDAL.GetSNStatusFromConfig(SN);
                            if (configStatus == 0)
                            {
                                SetIsEnable(true);
                                ToolsClass.Log("此组件 " + SN + " 没有维护物料信息!", "ABNORMAL", lstView);
                                return;
                            }
                        }
                    }
                    else //如果返工工单，检查组件之前是否做过入库，入库没有入库，必须要输入物料信息
                    {
                    }

                    #region
                    //获取组件打包的数据
                    DataTable newdt = ProductStorageDAL.GetSAPSNStorageInfo(SN);
                    if (newdt == null || newdt.Rows.Count == 0)
                    {
                        SetIsEnable(true);
                        ToolsClass.Log("组件号：" + SN + "获取入库信息失败", "ABNORMAL", lstView);
                        return;
                    }

                    SapWoModule SapWo = new SapWoModule();
                    DataRow newdtrow = newdt.Rows[0];
                    SapWo.CellCode = Convert.ToString(newdtrow["电池片物料代码"]);
                    SapWo.CellBatch = Convert.ToString(newdtrow["电池片批次"]);
                    SapWo.ActionCode = "I";
                    SapWo.SysId = Convert.ToString(newdtrow["组件序列号"]);
                    SapWo.PostedOn = PostedOn;
                    SapWo.FinishedOn = Convert.ToDateTime(newdtrow["包装日期"]).ToString("yyyy-MM-dd HH:mm:ss.fff");
                    SapWo.Unit = "PC";

                    //外协工单
                    if (!Convert.ToString(this.dataGridView1.Rows[i].Cells["WaiXieWo"].Value).Trim().Equals(""))
                    {
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["WaiXieWo"].Value).Trim();
                    }//转常规工单
                    else if (!Convert.ToString(this.dataGridView1.Rows[i].Cells["NormalWo"].Value).Trim().Equals(""))
                    {
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["NormalWo"].Value).Trim();
                    }//返工工单
                    else if (!Convert.ToString(this.dataGridView1.Rows[i].Cells["ReworkWo"].Value).Trim().Equals(""))
                    {
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["ReworkWo"].Value).Trim();
                    }
                    else //正常工单
                    {
                        SapWo.OrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value).Trim();
                    }
                    //储存原始工单
                    SapWo.RESV01 = Convert.ToString(this.dataGridView1.Rows[i].Cells["OrderNo"].Value).Trim();

                    SapWo.ProductCode = Convert.ToString(this.dataGridView1.Rows[i].Cells["ProductCode"].Value).Trim(); //产品物料编码
                    SapWo.Factory = Convert.ToString(this.dataGridView1.Rows[i].Cells["Factory"].Value).Trim(); //工厂
                    SapWo.Workshop = Convert.ToString(this.dataGridView1.Rows[i].Cells["Workshop"].Value).Trim(); //车间
                    SapWo.PackingLocation = Convert.ToString(this.dataGridView1.Rows[i].Cells["PackingLocation"].Value).Trim(); //入库地点
                    SapWo.SalesItemNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesItemNo"].Value).Trim(); //销售订单项目
                    SapWo.SalesOrderNo = Convert.ToString(this.dataGridView1.Rows[i].Cells["SalesOrderNo"].Value).Trim(); //销售订单

                    SapWo.CellEff = Convert.ToString(newdtrow["电池片转换效率"]);
                    SapWo.ModuleSN = Convert.ToString(newdtrow["组件序列号"]);
                    SapWo.CartonNo = Convert.ToString(newdtrow["内部托号"]);
                    SapWo.TestPower = Convert.ToString(newdtrow["实测功率"]);
                    SapWo.StdPower = Convert.ToString(newdtrow["标称功率"]);
                    SapWo.OrderStatus = "1";
                    SapWo.PostKey = PostKey;
                    SapWo.ModuleGrade = Convert.ToString(newdtrow["组件等级"]);
                    SapWo.ByIm = Convert.ToString(newdtrow["电流分档"]);
                    SapWo.CellPrintMode = Convert.ToString(newdtrow["电池片网版"]);
                    SapWo.GlassCode = Convert.ToString(newdtrow["玻璃物料代码"]);
                    SapWo.GlassBatch = Convert.ToString(newdtrow["玻璃批次"]);
                    SapWo.EvaCode = Convert.ToString(newdtrow["EVA 物料代码"]);
                    SapWo.EvaBatch = Convert.ToString(newdtrow["EVA 批次号"]);
                    SapWo.TptCode = Convert.ToString(newdtrow["TPT物料代码"]);
                    SapWo.TptBatch = Convert.ToString(newdtrow["TPT 批次"]);
                    SapWo.ConBoxCode = Convert.ToString(newdtrow["接线盒物料代码"]);
                    SapWo.ConBoxBatch = Convert.ToString(newdtrow["接线盒批次"]);
                    SapWo.LongFrameCode = Convert.ToString(newdtrow["长边框物料代码"]);
                    SapWo.LongFrameBatch = Convert.ToString(newdtrow["长边框批次"]);
                    SapWo.ShortFrameBatch = Convert.ToString(newdtrow["短边框批次"]);
                    SapWo.ShortFrameCode = Convert.ToString(newdtrow["短边框物料代码"]);
                    SapWo.PackingMode = Convert.ToString(newdtrow["包装方式"]);
                    SapWo.GlassThickness = Convert.ToString(newdtrow["玻璃厚度"]);
                    SapWo.IsCancelPacking = "1";
                    SapWo.IsOnlyPacking = "1";
                    SapWo.CustomerCartonNo = Convert.ToString(newdtrow["客户托号"]);
                    SapWo.InnerJobNo = "";
                    _ListSapWo.Add(SapWo);

                    var tsapReceiptUploadModule = new TsapReceiptUploadModule
                    {
                        Sysid = Guid.NewGuid().ToString(""),
                        CreatedOn = createdOn,
                        CreatedBy = createdBy,
                        GroupHistKey = groupHistKey,
                        CartonNo = SapWo.CartonNo,
                        CustomerCartonNo = SapWo.CustomerCartonNo,
                        FinishedOn = SapWo.FinishedOn,
                        ModuleColor = Convert.ToString(newdtrow["组件颜色"]),
                        ModuleSn = SapWo.ModuleSN,
                        OrderNo = SapWo.OrderNo,
                        SalesOrderNo = SapWo.SalesOrderNo,
                        SalesItemNo = SapWo.SalesItemNo,
                        OrderStatus = SapWo.OrderStatus,
                        ProductCode = SapWo.ProductCode,
                        Unit = SapWo.Unit,
                        Factory = SapWo.Factory,
                        Workshop = SapWo.Workshop,
                        PackingLocation = SapWo.PackingLocation,
                        PackingMode = SapWo.PackingMode,
                        CellEff = SapWo.CellEff,
                        TestPower = SapWo.TestPower,
                        StdPower = SapWo.StdPower,
                        ModuleGrade = SapWo.ModuleGrade,
                        ByIm = SapWo.ByIm,
                        Tolerance = Convert.ToString(newdtrow["公差"]),
                        CellCode = SapWo.CellCode,
                        CellBatch = SapWo.CellBatch,
                        CellPrintMode = SapWo.CellPrintMode,
                        GlassCode = SapWo.GlassCode,
                        GlassBatch = SapWo.GlassBatch,
                        EvaCode = SapWo.EvaCode,
                        EvaBatch = SapWo.EvaBatch,
                        TptCode = SapWo.TptCode,
                        TptBatch = SapWo.TptBatch,
                        ConboxCode = SapWo.ConBoxCode,
                        ConboxBatch = SapWo.ConBoxBatch,
                        ConboxType = string.Empty,
                        LongFrameCode = SapWo.LongFrameCode,
                        LongFrameBatch = SapWo.LongFrameBatch,
                        ShortFrameCode = SapWo.ShortFrameCode,
                        ShortFrameBatch = SapWo.ShortFrameBatch,
                        GlassThickness = SapWo.GlassThickness,
                        IsRework = Convert.ToString(newdtrow["是否重工"]),
                        IsByProduction = "false",
                        Resv01 = Convert.ToString(newdtrow["StdPowerLevel"])
                    };
                    if (!ReworkWo.Equals(""))
                        tsapReceiptUploadModule.IsRework = "Y";
                    tsapReceiptUploadModules.Add(tsapReceiptUploadModule);
                    #endregion
                }
                if (_ListSapWo.Count > 0)
                {
                    string msg = "";
                    if (!ProductStorageDAL.CheckWoStorage(_ListSapWo, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    if (!DataAccess.QueryReworkOrgBatch(tsapReceiptUploadModules, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    /*
                    var findAll =
                        tsapReceiptUploadModules.FindAll(
                            p =>
                            string.IsNullOrEmpty(p.ProductCode) || string.IsNullOrEmpty(p.OrderNo) ||
                            string.IsNullOrEmpty(p.CellCode) || string.IsNullOrEmpty(p.CellBatch) ||
                            string.IsNullOrEmpty(p.GlassCode) || string.IsNullOrEmpty(p.GlassBatch) ||
                            string.IsNullOrEmpty(p.EvaCode) || string.IsNullOrEmpty(p.EvaBatch) ||
                            string.IsNullOrEmpty(p.TptCode) || string.IsNullOrEmpty(p.TptBatch) ||
                            string.IsNullOrEmpty(p.ConboxCode) || string.IsNullOrEmpty(p.ConboxBatch) ||
                            string.IsNullOrEmpty(p.LongFrameCode) || string.IsNullOrEmpty(p.LongFrameBatch) ||
                            string.IsNullOrEmpty(p.ShortFrameCode) || string.IsNullOrEmpty(p.ShortFrameBatch) ||
                            string.IsNullOrEmpty(p.PackingMode) || string.IsNullOrEmpty(p.ModuleGrade) ||
                            string.IsNullOrEmpty(p.ByIm) || string.IsNullOrEmpty(p.StdPower) ||
                            string.IsNullOrEmpty(p.ModuleColor) || string.IsNullOrEmpty(p.Tolerance));
                    if (findAll.Count > 0)
                    {
                        const string errorMsg =
                            "SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、电流分档、标称功率、组件颜色、公差";
                        SetIsEnable(true);
                        ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        return;
                    }
                     */

                    List<string> listMessages = SapMessages(tsapReceiptUploadModules);
                    if (listMessages.Count > 0)
                    {
                        SetIsEnable(true);
                        foreach (var errorMsg in listMessages)
                        {
                            ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        }
                        return;
                    }

                    if (
                        !DataAccess.IsSapInventoryUseManulUpload(FormCover.CurrentFactory.Trim(),
                            FormCover.InterfaceConnString))
                    {
                        SaveXmlFormat(_ListSapWo, carton);
                    }
                    else
                    {
                        //保存数据供手动上传使用
                        SavePreUploadData(_ListSapWo, carton, tsapReceiptUploadModules);
                    }
                }
                SetIsEnable(true);
                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常(处理包装数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }


        private List<string> SapMessages(List<TsapReceiptUploadModule> sapReceipt)
        {
            List<string> messages = new List<string>();
            try
            {
               
                //"SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、
                //玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、
                //接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、
                //电流分档、标称功率、组件颜色、公差";
                
                if (sapReceipt != null && sapReceipt.Count > 0)
                {
                    StringBuilder strtmp;
                    int i=1;
                    foreach (TsapReceiptUploadModule sapModule in sapReceipt)
                    {
                        strtmp = new StringBuilder();
                        bool blntmp = true;

                        if(!string.IsNullOrEmpty(sapModule.OrderNo))
                        {
                            strtmp.Append("工单号:"+sapModule.OrderNo+"--");
                        }
                        else
                        {
                            strtmp.Append("【第 "+i.ToString()+"行工单号为空】");
                            blntmp = false;
                        }
                        if (!string.IsNullOrEmpty(sapModule.ModuleSn))
                        {
                            strtmp.Append("序列号:" + sapModule.ModuleSn+"   缺失的值【");

                        }
                        if(string.IsNullOrEmpty(sapModule.ProductCode))
                        {
                            strtmp.Append("产品物料代码、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.CellCode))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.CellBatch))
                        {
                            strtmp.Append("电池片批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.GlassCode))
                        {
                            strtmp.Append("玻璃批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.GlassBatch))
                        {
                            strtmp.Append("EVA物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.EvaCode))
                        {
                            strtmp.Append("EVA批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.EvaBatch))
                        {
                            strtmp.Append("背板物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.TptCode))
                        {
                            strtmp.Append("背板批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.TptBatch))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ConboxCode))
                        {
                            strtmp.Append("接线盒物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ConboxBatch))
                        {
                            strtmp.Append("接线盒批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.LongFrameCode))
                        {
                            strtmp.Append("长边框物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.LongFrameBatch))
                        {
                            strtmp.Append("长边框批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ShortFrameCode))
                        {
                            strtmp.Append("短边框物料号、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ShortFrameBatch))
                        {
                            strtmp.Append("短边框批次、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.PackingMode))
                        {
                            strtmp.Append("包装方式、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ModuleGrade))
                        {
                            strtmp.Append("组件等级、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ByIm))
                        {
                            strtmp.Append("电流分档、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.StdPower))
                        {
                            strtmp.Append("标称功率、");
                            blntmp = false;
                        }
                        if(string.IsNullOrEmpty(sapModule.ModuleColor))
                        {
                            strtmp.Append("组件颜色、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.Tolerance))
                        {
                            strtmp.Append("公差、");
                            blntmp = false;
                        }
                        if (strtmp !=null &&!string.IsNullOrEmpty(strtmp.ToString()) && blntmp==false)
                        {
                            messages.Add(strtmp+"】 【第 "+i.ToString()+" 行】");
                        }
                        i=i+1;
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add("SAP 数据检查异常:" + ex.Message.ToString());
            }
            return messages;
        }
        /// <summary>
        /// 上传SAP
        /// </summary>
        /// <returns></returns>
        private void SaveXmlFormat(List<SapWoModule> listWo, string carton)
        {
            try
            {
                #region
                string flag = "Y";
                string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(listWo, Encoding.UTF8);
                SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");

                ToolsClass.Log("内部托号：" + Convert.ToString(this.dataGridView1.Rows[0].Cells["CartonNo"].Value).Trim() + "入库数据保存");
                ToolsClass.Log(SapStorageString);
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                string aa = service.packingDataMesToSap(SapStorageString);
                string bb;
                DataTable dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
                if (!listWo.Count.Equals(dt.Rows.Count))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("上传SAP和SAP返回的总数据不一致", "ABNORMAL", lstView);
                    return;
                }
                string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                if (dt != null && dt.Rows.Count > 0)
                {
                    //if (_ListCartonSucess.Count > 0)
                    //    _ListCartonSucess.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["Result"]).Equals("0"))
                        {
                            SetIsEnable(true);
                            ToolsClass.Log("上传SAP保存失败,原因:" + row["Message"] + "", "ABNORMAL", lstView);
                            flag = "N";
                            break;
                        }
                    }
                    //更新数据库
                    if (flag.Equals("Y"))
                    {
                        if (ProductStorageDAL.updatepackingdatabySn(_ListSapWo, joinid, posttime))
                        {
                            //if (!_ListCartonSucess.Contains(carton))
                            ////    _ListCartonSucess.Add(carton);                           
                            //#region 写入库交易记录
                            //if (_ListCartonSucess.Count > 0)
                            //{
                            //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "Invertory");
                            //}
                            //#endregion

                            //上传成功后，保存数据
                            ProductStorageDAL.SaveStorageInfoLog(_ListSapWo);

                            SetIsEnable(true);
                            ToolsClass.Log("托号：" + carton + " 上传SAP保存成功", "NORMAL", lstView);

                            if (this.dataGridView1.Rows.Count > 0)
                                this.dataGridView1.Rows.Clear();
                        }
                        else
                        {
                            SetIsEnable(true);
                            ToolsClass.Log("托号：" + carton + " 上传SAP保存失败(更新包装数据库失败)", "ABNORMAL", lstView);
                        }
                    }
                }
                else
                {
                    SetIsEnable(true);
                    ToolsClass.Log("上传SA保存返回数据错误", "ABNORMAL", lstView);
                    return;
                }

                #region
                //StringBuilder code = new StringBuilder();
                //code.Append("<?xml version='1.0' encoding='utf-8' ?>");
                //code.Append("<MesPackingData>");
                //foreach (SapWoModule wo in listWo)
                //{
                //    code.Append("<PackingData>");
                //    code.Append(string.Format("<ActionCode>{0}</ActionCode>", wo.OrderNo));
                //    code.Append(string.Format("<SysId>{0}</SysId>", wo.SysId));
                //    code.Append(string.Format("<OrderNo>{0}</OrderNo>", wo.OrderNo));
                //    code.Append(string.Format("<ProductCode>{0}</ProductCode>", wo.ProductCode));
                //    code.Append(string.Format("<FinishedOn>{0}</FinishedOn>", wo.FinishedOn));
                //    code.Append(string.Format("<PostedOn>{0}</PostedOn>", wo.PostedOn));
                //    code.Append(string.Format("<Unit>{0}</Unit>", wo.Unit));
                //    code.Append(string.Format("<Factory>{0}</Factory>", wo.Factory));
                //    code.Append(string.Format("<Workshop>{0}</Workshop>", wo.Workshop));
                //    code.Append(string.Format("<PackingLocation>{0}</PackingLocation>", wo.PackingLocation));
                //    code.Append(string.Format("<CellEff>{0}</CellEff>", wo.CellEff));
                //    code.Append(string.Format("<ModuleSN>{0}</ModuleSN>", wo.ModuleSN));
                //    code.Append(string.Format("<CartonNo>{0}</CartonNo>", wo.CartonNo));
                //    code.Append(string.Format("<TestPower>{0}</TestPower>", wo.TestPower));
                //    code.Append(string.Format("<StdPower>{0}</StdPower>", wo.StdPower));
                //    code.Append(string.Format("<OrderStatus>{0}</OrderStatus>", wo.OrderStatus));
                //    code.Append(string.Format("<PostKey>{0}</PostKey>", wo.PostKey));
                //    code.Append(string.Format("<ModuleGrade>{0}</ModuleGrade>", wo.ModuleGrade));
                //    code.Append(string.Format("<ByIm>{0}</ByIm>", wo.ByIm));
                //    code.Append(string.Format("<CellCode>{0}</CellCode>", wo.CellCode));
                //    code.Append(string.Format("<CellBatch>{0}</CellBatch>", wo.CellBatch));
                //    code.Append(string.Format("<CellPrintMode>{0}</CellPrintMode>", wo.CellPrintMode));
                //    code.Append(string.Format("<GlassCode>{0}</GlassCode>", wo.GlassCode));
                //    code.Append(string.Format("<GlassBatch>{0}</GlassBatch>", wo.GlassBatch));
                //    code.Append(string.Format("<EvaCode>{0}</EvaCode>", wo.EvaCode));
                //    code.Append(string.Format("<EvaBatch>{0}</EvaBatch>", wo.EvaBatch));
                //    code.Append(string.Format("<TptCode>{0}</TptCode>", wo.TptCode));
                //    code.Append(string.Format("<TptBatch>{0}</TptBatch>", wo.TptBatch));
                //    code.Append(string.Format("<ConBoxCode>{0}</ConBoxCode>", wo.ConBoxCode));
                //    code.Append(string.Format("<ConBoxBatch>{0}</ConBoxBatch>", wo.ConBoxBatch));
                //    code.Append(string.Format("<FrameCode>{0}</FrameCode>", wo.FrameCode));
                //    code.Append(string.Format("<FrameBatch>{0}</FrameBatch>", wo.FrameBatch));
                //    code.Append("</PackingData>");
                //}
                //code.ToString();
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("上传SAP保存时发生异常(处理SAP返回数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        private void SavePreUploadData(List<SapWoModule> listWo, string carton, List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            try
            {
                #region

                ToolsClass.Log("内部托号：" + Convert.ToString(this.dataGridView1.Rows[0].Cells["CartonNo"].Value).Trim() + "入库数据保存");

                string joinid = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                if (ProductStorageDAL.updatepackingdatabySn(tsapReceiptUploadModules, listWo, joinid, posttime))
                {
                    SetIsEnable(true);
                    ToolsClass.Log("托号：" + carton + "保存待上传SAP数据成功", "NORMAL", lstView);

                    if (this.dataGridView1.Rows.Count > 0)
                        this.dataGridView1.Rows.Clear();
                }
                else
                {
                    SetIsEnable(true);
                    ToolsClass.Log("托号：" + carton + "保存待上传SAP数据失败(更新包装数据库失败)", "ABNORMAL", lstView);
                }

                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        /// <summary>
        /// 工单重置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, EventArgs e)
        {
            if (dgvIndex.Count > 0)
                dgvIndex.Clear();
            ClearData(true);
            this.txtWoOrder.SelectAll();
            this.txtWoOrder.Focus();
            return;
        }

        /// <summary>
        /// 工单类型选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlWoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                WoType = "";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                this.LblWo.Text = "工       单";
                this.label3.Text = "工       单";
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 1) //转常规工单
            {
                WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                WoType = WoType.Replace("M", "");
                this.LblWo.Text = "转常规 工单";
                this.label3.Text = "转常规 工单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 2)//外协工单
            {
                WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                WoType = WoType.Replace("M", "");
                this.LblWo.Text = "外协后 工单";
                this.label3.Text = "外协后 工单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 3)//返工工单
            {
                WoType = "ZP11";
                this.LblWo.Text = "返 工 工 单";
                this.label3.Text = "返 工 工 单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
        }

        private void Reset1()
        {
            this.txtWo.Clear();
            this.txtMitemCode.Clear();
            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();
            this.txtFactory.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();
        }
        /// <summary>
        /// 给拖号设置工单信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Set_Click(object sender, EventArgs e)
        {
            try
            {
                #region

                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("工单类型不能为空,请选择", "ABNORMAL", lstView);
                    return;
                }
                else
                {
                    if (this.txtWo.Text.Trim().Equals(""))
                    {
                        if (this.ddlWoType.SelectedIndex == 1)
                        {
                            ToolsClass.Log("转常规工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                        else if (this.ddlWoType.SelectedIndex == 2)
                        {
                            ToolsClass.Log("外协后工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                        else if (this.ddlWoType.SelectedIndex == 3)
                        {
                            ToolsClass.Log("返工工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                    }
                }

                if (this.dataGridView1.Rows.Count > 0)
                {
                    if (dgvIndex.Count > 0)
                    {
                        dgvIndex.Clear();
                    }
                    if (SNArrayS.Count > 0)
                    {
                        SNArrayS.Clear();
                    }
                    for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                    {
                        if (this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString().ToUpper() == "TRUE")
                        {
                            dgvIndex.Add(i, this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString());
                            SNArrayS.Add(this.dataGridView1.Rows[i].Cells[2].EditedFormattedValue.ToString());
                        }
                    }
                    if (dgvIndex.Count < 1)
                    {
                        ToolsClass.Log("没有选中要设置的数据,请确认!", "ABNORMAL", lstView);
                        return;
                    }
                }
                else
                {
                    ToolsClass.Log("没有要设置的数据,请确认!", "ABNORMAL", lstView);
                    return;
                }

                //判断重工工单的是否有新增组件
                if (this.ddlWoType.SelectedIndex == 3)
                {
                    foreach (string serialnumber in SNArrayS)
                    {
                        DataTable dtnew = ProductStorageDAL.GetSNInfoFromInterface(serialnumber);
                        //SAP已经入库过
                        if ((dtnew != null) && (dtnew.Rows.Count > 0))
                            continue;
                        else
                        {
                            int configQty = ProductStorageDAL.GetSNStroageInfoFromConfig(serialnumber);
                            if (configQty == 0)
                            {
                                ToolsClass.Log("组件 " + serialnumber + " 为重工时新加组件，所对应的物料信息必须维护", "ABNORMAL", lstView);
                                return;
                            }
                        }
                    }

                }

                foreach (int RowIndex in dgvIndex.Keys)
                {
                    this.dataGridView1.Rows[RowIndex].Cells["ProductCode"].Value = Convert.ToString(this.txtMitemCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["Factory"].Value = Convert.ToString(this.txtPlanCode.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["Workshop"].Value = Convert.ToString(this.txtFactory.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["PackingLocation"].Value = Convert.ToString(this.txtlocation.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["SalesOrderNo"].Value = Convert.ToString(this.txtSalesOrderNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["SalesItemNo"].Value = Convert.ToString(this.txtSalesItemNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["NormalWo"].Value = "";
                    this.dataGridView1.Rows[RowIndex].Cells["WaiXieWo"].Value = "";
                    this.dataGridView1.Rows[RowIndex].Cells["ReworkWo"].Value = "";
                    if (this.ddlWoType.SelectedIndex == 1) //转常规工单
                        this.dataGridView1.Rows[RowIndex].Cells["NormalWo"].Value = this.txtWo.Text.Trim();
                    else if (this.ddlWoType.SelectedIndex == 2) //外协工单
                        this.dataGridView1.Rows[RowIndex].Cells["WaiXieWo"].Value = this.txtWo.Text.Trim().ToString();
                    else if (this.ddlWoType.SelectedIndex == 3) //返工工单
                        this.dataGridView1.Rows[RowIndex].Cells["ReworkWo"].Value = this.txtWo.Text.Trim().ToString();
                }
                ToolsClass.Log("设置成功!", "NORMAL", lstView);
                Reset_Click(null, null);
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("设置数据时发生异常：" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }
        /// <summary>
        /// 单击Grid列标题中的 CheckBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ch_OnCheckBoxClicked(object sender, HeadCheckBox.datagridviewCheckboxHeaderEventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = e.CheckedState;
            }
        }
        #endregion


        #region 工单查询
        private void txtWoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请先选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    this.ddlWoType.SelectAll();
                    this.txtWoOrder.Clear();
                    return;
                }

                if (string.IsNullOrEmpty(Convert.ToString(this.txtWoOrder.Text.Trim())))
                {
                    ToolsClass.Log("输入的工单不能为空,请重新输入!", "ABNORMAL", lstView);
                    this.txtWoOrder.Focus();
                    return;
                }

                if (dgvIndex.Count > 0)
                    dgvIndex.Clear();

                ClearData(false);


                SetWOData(true);
            }
        }
        private void txtWoOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    return;
                }
                PicBoxWO_Click(null, null);
            }

        }
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                ToolsClass.Log("请选择工单类型!", "ABNORMAL", lstView);
                this.ddlWoType.Focus();
                return;
            }

            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), WoType);
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
        }
        private void SetWOData(bool flag)
        {
            ClearData(false);
            if (Convert.ToString(this.txtWoOrder.Text.Trim()).Equals(""))
            {

                ToolsClass.Log("输入的工单为空，请确认！", "ABNORMAL", lstView);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }
            DataTable wo = ProductStorageDAL.GetWoMasterIno(Convert.ToString(this.txtWoOrder.Text.Trim()), WoType);
            if (wo != null && wo.Rows.Count > 0)
            {
                SetWoInfo(wo, WoType);
            }
            else
            {
                if (flag)
                {
                    ToolsClass.Log("工单：" + Convert.ToString(this.txtWoOrder.Text.Trim()) + " 没有从SAP下载或工单类型不一致,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
                else
                {
                    ToolsClass.Log("工单:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "在SAP没有做物料转储,请确认!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
            }
            this.txtWoOrder.Clear();
        }
        private void SetWoInfo(DataTable wo, string _Wotype)
        {
            DataRow row = wo.Rows[0];
            this.txtWo.Text = Convert.ToString(row["TWO_NO"]);
            this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
            this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
            this.txtlocation.Text = Convert.ToString(row["RESV04"]);
            this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
            this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
            this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
        }
        #endregion

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        #region
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.txtWO1.Enabled = this.checkBox2.Checked;
            this.pictureBox2.Enabled = this.checkBox2.Checked;
            this.txtWO1.Clear();
            this.txtWO1.Focus();
        }

        private void txtWO1_Leave(object sender, EventArgs e)
        {
            //判断是否有外协工单
            if (!this.txtWO1.Text.Trim().ToString().Equals(""))
            {
                DataTable dt = ProductStorageDAL.GetWoMasterIno(this.txtWO1.Text.Trim());
                if (dt == null || dt.Rows.Count == 0)
                {
                    if (this.checkBox2.Checked)
                    {
                        ToolsClass.Log("输入的外协工单：" + this.txtWO1.Text.Trim() + " 没有从SAP下载", "ABNORMAL", lstView);
                        this.txtWO1.Clear();
                        this.txtWO1.Focus();
                        return;
                    }
                }
                else
                {
                    DataRow row = dt.Rows[0];
                    this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
                    this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.txtlocation.Text = Convert.ToString(row["RESV04"]);
                    this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
                    this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
                    this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
                }
            }
            else
            {
                if (this.checkBox2.Checked)
                {
                    ToolsClass.Log("请输入外协工单号码", "ABNORMAL", lstView);
                    this.txtWO1.Focus();
                }
            }
        }

        private void txtWO1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                pictureBox2_Click(null, null);
            }
        }

        private void txtWO1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                txtWO1_Leave(null, null);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), "");
            frm.ShowDialog();
            this.txtWO1.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
        }
        #endregion

    }
}
