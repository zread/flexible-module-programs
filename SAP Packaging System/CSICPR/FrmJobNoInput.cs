﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FrmJobNoInput : Form
    {
        public FrmJobNoInput()
        {
            InitializeComponent();
        }

        public FrmJobNoInput(string fromDate,string toDate,string jobNo,string boxID,string fromCarton,string toCarton,QueryType queryType)
        {
            InitializeComponent();

            _FromDate = fromDate;
            _ToDate = toDate;
            _JobNo = jobNo;
            _BoxID = boxID;

            _FromCarton = fromCarton;
            _ToCarton = toCarton;
            _QueryType = queryType;
        }

        string _FromDate = "";
        string _ToDate = "";
        string _JobNo = "";
        string _BoxID = "";
        string _FromCarton = "";
        string _ToCarton = "";
        QueryType _QueryType = QueryType.ByDate;

        DataTable _Table = null;


        private void FrmJobNoInput_Load(object sender, EventArgs e)
        {
            BindingGrid();
            this.label3.Text = ToolsClass.message;
            this.chkSelectAll.Checked = true;
            SelectedAll(this.chkSelectAll.Checked);
        }

        private void BindingGrid()
        {
            if (!string.IsNullOrEmpty(_BoxID))
            {
                BindingGridBoxID();
            }
            else
            {
                BindingGridDate();
            }

            if (_QueryType == QueryType.ByCarton)
                BindingGridBoxID();
            else if (_QueryType == QueryType.ByCartonSpan)
                BindingGridCartonSpan();
            else if (_QueryType == QueryType.ByDate)
                BindingGridDate();
            else if (_QueryType == QueryType.ByJobNo)
                BindingGridJobNo();
        }

        private void BindingGridDate()
        {
            string sql = @"
                SELECT 
	                0 'Selected',
	                [Product].BoxID ,
	                COUNT(SN) BoxCnt,
	                ISNULL(JobNo,'') JobNo
                FROM [Product]
					INNER JOIN (
SELECT BoxID,JobNo,MAX(CreateDate) CreateDate
FROM Box
GROUP BY BoxID,JobNo
	) BOX ON
						Box.BoxID=[Product].BoxID
                WHERE  LEN([Product].[BoxID]) > 0 
			                AND [Product].[ProcessDateTime]>'{0}' 
			                AND [Product].[ProcessDateTime]<'{1}'
            ";

            sql = string.Format(sql, _FromDate, _ToDate);

            if (!string.IsNullOrEmpty(_JobNo))
                sql += @" AND [Box].JobNo LIKE '%" + _JobNo + "%'";

            sql += @" GROUP BY [Product].BoxID,JobNo ORDER BY [Product].BoxID,JobNo";

            _Table = ToolsClass.GetDataTable(sql);

            this.grdJobNo.DataSource = _Table.DefaultView;
        }

        private void BindingGridBoxID()
        {
            string sql = @"
                SELECT 
	                0 'Selected',
	                [Product].BoxID ,
	                COUNT(SN) BoxCnt,
	                ISNULL(JobNo,'') JobNo
                FROM [Product]
					INNER JOIN (
SELECT BoxID,JobNo,MAX(CreateDate) CreateDate
FROM Box
GROUP BY BoxID,JobNo
	) BOX ON
						Box.BoxID=[Product].BoxID
                WHERE  LEN([Product].[BoxID]) > 0 
			                AND [Product].BoxID LIKE '%{0}%'
                
            ";

            sql = string.Format(sql, _BoxID);
             
            sql += @" GROUP BY [Product].BoxID,JobNo";

            _Table = ToolsClass.GetDataTable(sql);

            this.grdJobNo.DataSource = _Table.DefaultView;
        }

        private void BindingGridJobNo()
        {
            string sql = @"
                SELECT 
	                0 'Selected',
	                [Product].BoxID ,
	                COUNT(SN) BoxCnt,
	                ISNULL(JobNo,'') JobNo
                FROM [Product]
					INNER JOIN (
SELECT BoxID,JobNo,MAX(CreateDate) CreateDate
FROM Box
GROUP BY BoxID,JobNo
	) BOX ON
						Box.BoxID=[Product].BoxID
                WHERE  LEN([Product].[BoxID]) > 0 
            ";
             
            if (!string.IsNullOrEmpty(_JobNo))
                sql += @" AND [Box].JobNo LIKE '%" + _JobNo + "%'";

            sql += @" GROUP BY [Product].BoxID,JobNo ORDER BY [Product].BoxID,JobNo";

            _Table = ToolsClass.GetDataTable(sql);

            this.grdJobNo.DataSource = _Table.DefaultView;
        }

        private void BindingGridCartonSpan()
        {
            string sql = @"
                SELECT 
	                0 'Selected',
	                [Product].BoxID ,
	                COUNT(SN) BoxCnt,
	                ISNULL(JobNo,'') JobNo
                FROM [Product]
					INNER JOIN (
SELECT BoxID,JobNo,MAX(CreateDate) CreateDate
FROM Box
GROUP BY BoxID,JobNo
	) BOX ON
						Box.BoxID=[Product].BoxID
                WHERE  LEN([Product].[BoxID]) > 0 
            ";
             

            if (!string.IsNullOrEmpty(_FromCarton))
                sql += @" AND [Box].BoxID >= '" + _FromCarton + "'";

            if (!string.IsNullOrEmpty(_ToCarton))
                sql += @" AND [Box].BoxID <= '" + _ToCarton + "'";

            sql += @" GROUP BY [Product].BoxID,JobNo ORDER BY [Product].BoxID,JobNo";

            _Table = ToolsClass.GetDataTable(sql);

            this.grdJobNo.DataSource = _Table.DefaultView;
        }

        private void grdJobNo_DoubleClick(object sender, EventArgs e)
        { 
        }

        private void grdJobNo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <= 0)
                return;

            string boxID = this.grdJobNo.Rows[e.RowIndex].Cells[2].Value.ToString();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //非空验证
            string jobNo = this.txtJobNo.Text.Trim();
            if (string.IsNullOrEmpty(jobNo))
            {
                if (MessageBox.Show("Job No. 为空时，将对选中的箱号进行拆柜操作！", "提示", 
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Cancel)
                {
                    return;
                }
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (DataRow row in _Table.Rows)
            {
                if (row[0].ToString() == "1")
                {
                    dict[row[1].ToString()] = jobNo;
                }
            }

            string const_M03_Sql = @"
                UPDATE Box
                SET JobNo='{0}'
                WHERE BoxID='{1}'
            ";

            string sql = "";
            int iRst = -1;

            //保存到M03数据库
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                sql = string.Format(const_M03_Sql, kvp.Value, kvp.Key);
                iRst = ToolsClass.ExecuteNonQuery(sql);
                //修改： Transaction
            }

            string const_Center_Sql = @"
                UPDATE T_MODULE_CARTON
                SET SHIP_JOB_NO='{0}'
                WHERE CARTON_NO='{1}'
            ";
            //保存到集中数据库
            foreach (KeyValuePair<string, string> kvp in dict)
            {
                sql = string.Format(const_Center_Sql, kvp.Value, kvp.Key);
                iRst = DbHelperSQL.ExecuteNonQuery(sql);
                //修改： Transaction
            }
            
            MessageBox.Show("保存成功");
            BindingGrid();
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            bool chk = this.chkSelectAll.Checked;
            SelectedAll(chk);
        }

        private void SelectedAll(bool chk)
        {
            for (int i = 0; i < this.grdJobNo.Rows.Count; i++)
            {
                if (chk && this.grdJobNo.Rows[i].Cells[3].Value != null &&
                    !string.IsNullOrEmpty(this.grdJobNo.Rows[i].Cells[3].Value.ToString().Trim()))
                {
                    continue;
                }
                this.grdJobNo.Rows[i].Cells[0].Value = chk ? 1 : 0;
            }
        }

        private bool UpdateAll()
        {
            for (int i = 0; i < this.grdJobNo.Rows.Count; i++)
            {
                if (this.grdJobNo.Rows[i].Cells[3].Value != null &&
                    !string.IsNullOrEmpty(this.grdJobNo.Rows[i].Cells[3].Value.ToString().Trim()))
                    continue;
                else
                    return false;
            }
            return true;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FormJobNo frm = new FormJobNo();
            frm.Show();
        }
    }
}
