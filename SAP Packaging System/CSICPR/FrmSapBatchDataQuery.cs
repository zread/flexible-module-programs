﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;
using CSICPR.Properties;

namespace CSICPR
{
    public partial class FrmSapBatchDataQuery : Form
    {
        public FrmSapBatchDataQuery()
        {
            InitializeComponent();
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FrmSapBatchDataQuery _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmSapBatchDataQuery
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        private void RebindGrid(DataTable table)
        {
            dgvData.DataSource = null;
            dgvData.DataSource = table;
        }

        private void FrmSapInventoryUploadResult_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.cmbQueryType);
            # endregion

            cmbQueryType.SelectedIndex = 0;

            RebindGrid(null);

        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            RebindGrid(null);
            var selectedIndex = cmbQueryType.SelectedIndex;
            DataTable dt = null;
            switch (selectedIndex)
            {
                case 0:
                    var jobNo = txtJobNo.Text.Trim();
                    if (string.IsNullOrEmpty(jobNo))
                    {
                        ToolsClass.Log("btnQuery_Click::出货柜号不能为空");
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message1","出货柜号不能为空"));
                        return;
                    }
                    dt = DataAccess.QuerySapBatchDataByJobNo(jobNo, FormCover.EsbConnString);
                    break;
                case 1:
                    var cartonNos = txtCartonNos.Lines;
                    if (cartonNos.Length <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::托号不能为空");
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message2","托号不能为空"));
                       return;
                    }
                    var listCartonNos = new List<string>();
                    foreach (var cartonNo in cartonNos)
                    {
                        if (string.IsNullOrEmpty(cartonNo) || cartonNo.Trim().Length <= 0)
                            continue;
                        listCartonNos.Add(cartonNo);
                    }
                    if (listCartonNos.Count <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::托号不能为空");
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message2","托号不能为空"));
                        return;
                    }
                    var cartonNoArray = string.Join("','", listCartonNos.ToArray());
                    dt = DataAccess.QuerySapBatchDataByCartonNos(cartonNoArray, FormCover.EsbConnString);
                    break;
                case 2:
                    var moduleSns = txtModuleSns.Lines;
                    if (moduleSns.Length <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::组件序列号不能为空");
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3","组件序列号不能为空"));
                        return;
                    }
                    var listModuleSns = new List<string>();
                    foreach (var moduleSn in moduleSns)
                    {
                        if (string.IsNullOrEmpty(moduleSn) || moduleSn.Trim().Length <= 0)
                            continue;
                        listModuleSns.Add(moduleSn);
                    }
                    if (listModuleSns.Count <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::组件序列号不能为空");
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "组件序列号不能为空"));
                        return;
                    }
                    var moduleSnArray = string.Join("','", listModuleSns.ToArray());
                    dt = DataAccess.QuerySapBatchDataByModuleSns(moduleSnArray, FormCover.EsbConnString);
                    break;
            }
            RebindGrid(dt);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            cmbQueryType.SelectedIndex = 0;
            RebindGrid(null);
        }

        private void cmbQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtJobNo.Visible = false;
            txtCartonNos.Visible = false;
            txtModuleSns.Visible = false;
            pnlTop.Height = 70;
            btnQuery.Location = new Point(466, 38);
            btnReset.Location = new Point(550, 38);
            btnExport.Location = new Point(635, 38);
            var selectedIndex = cmbQueryType.SelectedIndex;
            switch (selectedIndex)
            {
                case 0:
                    txtJobNo.Clear();
                    txtJobNo.Visible = true;
                    txtJobNo.Focus();
                    break;
                case 1:
                    txtCartonNos.Clear();
                    txtCartonNos.Visible = true;
                    txtCartonNos.Focus();
                    pnlTop.Height = 126;
                    btnQuery.Location = new Point(466, 92);
                    btnReset.Location = new Point(550, 92);
                    btnExport.Location = new Point(635, 92);
                    break;
                case 2:
                    txtModuleSns.Clear();
                    txtModuleSns.Visible = true;
                    txtModuleSns.Focus();
                    pnlTop.Height = 126;
                    btnQuery.Location = new Point(466, 92);
                    btnReset.Location = new Point(550, 92);
                    btnExport.Location = new Point(635, 92);
                    break;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataToExcel(dgvData);
        }

        public void DataToExcel(DataGridView m_DataView)
        {
            SaveFileDialog kk = new SaveFileDialog();
            kk.Title = "保存EXECL文件";
            kk.Filter = "EXECL文件(*.xls)|*.xls|所有文件(*.*)|*.*";
            kk.FilterIndex = 1;
            if (kk.ShowDialog() == DialogResult.OK)
            {
                string FileName = kk.FileName + ".xls";
                if (File.Exists(FileName))
                    File.Delete(FileName);
                FileStream objFileStream;
                StreamWriter objStreamWriter;
                string strLine = "";
                objFileStream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write);
                objStreamWriter = new StreamWriter(objFileStream, System.Text.Encoding.Unicode);
                for (int i = 0; i < m_DataView.Columns.Count; i++)
                {
                    if (m_DataView.Columns[i].Visible == true)
                    {
                        strLine = strLine + m_DataView.Columns[i].HeaderText.ToString() + Convert.ToChar(9);
                    }
                }
                objStreamWriter.WriteLine(strLine);
                strLine = "";

                for (int i = 0; i < m_DataView.Rows.Count; i++)
                {
                    if (m_DataView.Columns[0].Visible == true)
                    {
                        if (m_DataView.Rows[i].Cells[0].Value == null)
                            strLine = strLine + " " + Convert.ToChar(9);
                        else
                            strLine = strLine + m_DataView.Rows[i].Cells[0].Value.ToString() + Convert.ToChar(9);
                    }
                    for (int j = 1; j < m_DataView.Columns.Count; j++)
                    {
                        if (m_DataView.Columns[j].Visible == true)
                        {
                            if (m_DataView.Rows[i].Cells[j].Value == null)
                                strLine = strLine + " " + Convert.ToChar(9);
                            else
                            {
                                string rowstr = "";
                                rowstr = m_DataView.Rows[i].Cells[j].Value.ToString();
                                if (rowstr.IndexOf("\r\n") > 0)
                                    rowstr = rowstr.Replace("\r\n", " ");
                                if (rowstr.IndexOf("\t") > 0)
                                    rowstr = rowstr.Replace("\t", " ");
                                strLine = strLine + rowstr + Convert.ToChar(9);
                            }
                        }
                    }
                    objStreamWriter.WriteLine(strLine);
                    strLine = "";
                }
                objStreamWriter.Close();
                objFileStream.Close();
                MessageBox.Show(this, LanguageHelper.GetMessage(LanMessList, "Message4", "保存EXCEL成功"), " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}