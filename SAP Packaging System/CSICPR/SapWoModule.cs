﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Data;

namespace CSICPR
{
    /// <summary>
    /// MesSap:Mes与Sap对接入库实体类
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "PackingData")]
    [XmlType(TypeName = "PackingData")]
    public class SapWoModule
    {
        /// <summary>
        /// SAP所对应的类
        /// </summary>
        private string _ActionCode = "";
        /// <summary>
        /// 操作代码,规则update为U;insert为I;delete为D;
        /// </summary>
        public string ActionCode
        {
            get { return _ActionCode; }
            set { _ActionCode = value; }
        }

        private string _SysId = "";
        /// <summary>
        /// 自定义序号
        /// </summary>
        public string SysId
        {
            get { return _SysId; }
            set { _SysId = value; }
        }

        private string _OrderNo = "";
        /// <summary>
        /// 生产订单号，MES为工单
        /// </summary>
        public string OrderNo
        {
            get { return _OrderNo; }
            set { _OrderNo = value; }
        }

        private string _SalesOrderNo = "";
        /// <summary>
        /// 销售订单
        /// </summary>
        public string SalesOrderNo
        {
            get { return _SalesOrderNo; }
            set { _SalesOrderNo = value; }
        }

        private string _SalesItemNo = "";
        /// <summary>
        /// 销售订单项目
        /// </summary>
        public string SalesItemNo
        {
            get { return _SalesItemNo; }
            set { _SalesItemNo = value; }
        }

        private string _ProductCode = "";
        /// <summary>
        /// 产品物料代码
        /// </summary>
        public string ProductCode
        {
            get { return _ProductCode; }
            set { _ProductCode = value; }
        }
        private string _FinishedOn = "";
        /// <summary>
        /// 组件包装完工日期
        /// </summary>
        public string FinishedOn
        {
            get { return _FinishedOn; }
            set { _FinishedOn = value; }
        }
        private string _PostedOn = "";
        /// <summary>
        /// 上传日期
        /// </summary>
        public string PostedOn
        {
            get { return _PostedOn; }
            set { _PostedOn = value; }
        }
        private string _Unit = "";
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit
        {
            get { return _Unit; }
            set { _Unit = value; }
        }
        private string _Factory = "";
        /// <summary>
        /// 工厂
        /// </summary>
        public string Factory
        {
            get { return _Factory; }
            set { _Factory = value; }
        }
        private string _Workshop = "";
        /// <summary>
        /// 车间
        /// </summary>
        public string Workshop
        {
            get { return _Workshop; }
            set { _Workshop = value; }
        }
        private string _PackingLocation = "";
        /// <summary>
        /// 入库地点
        /// </summary>
        public string PackingLocation
        {
            get { return _PackingLocation; }
            set { _PackingLocation = value; }
        }
        private string _CellEff = "";
        /// <summary>
        /// 电池片转换效率
        /// </summary>
        public string CellEff
        {
            get { return _CellEff; }
            set { _CellEff = value; }
        }
        private string _ModuleSN = "";
        /// <summary>
        /// 组件序列号
        /// </summary>
        public string ModuleSN
        {
            get { return _ModuleSN; }
            set { _ModuleSN = value; }
        }
        private string _CartonNo = "";
        /// <summary>
        /// 包装箱号
        /// </summary>
        public string CartonNo
        {
            get { return _CartonNo; }
            set { _CartonNo = value; }
        }

        private string _CustomerCartonNo = "";
        /// <summary>
        /// 客户托号
        /// </summary>
        public string CustomerCartonNo
        {
            get { return _CustomerCartonNo; }
            set { _CustomerCartonNo = value; }
        }
        private string _InnerJobNo = "";
        /// <summary>
        /// 内部柜号
        /// </summary>
        public string InnerJobNo
        {
            get { return _InnerJobNo; }
            set { _InnerJobNo = value; }
        }
        private string _TestPower = "";
        /// <summary>
        /// 实测功率
        /// </summary>
        public string TestPower
        {
            get { return _TestPower; }
            set { _TestPower = value; }
        }
        private string _StdPower = "";
        /// <summary>
        /// 标称功率
        /// </summary>
        public string StdPower
        {
            get { return _StdPower; }
            set { _StdPower = value; }
        }
        private string _OrderStatus = "";
        /// <summary>
        /// 生产订单状态
        /// </summary>
        public string OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        private string _PostKey = "";
        /// <summary>
        /// 上传条目号码 接口编号 + 年月日时分秒毫秒
        /// </summary>
        public string PostKey
        {
            get { return _PostKey; }
            set { _PostKey = value; }
        }
        private string _ModuleGrade = "";
        /// <summary>
        /// 组件等级
        /// </summary>
        public string ModuleGrade
        {
            get { return _ModuleGrade; }
            set { _ModuleGrade = value; }
        }
        private string _ByIm = "";
        /// <summary>
        /// 电池分档 传入Y/N，表示是否做了电池分档
        /// </summary>
        public string ByIm
        {
            get { return _ByIm; }
            set { _ByIm = value; }
        }

        private string _CellCode;
        /// <summary>
        /// 电池片物料号
        /// </summary>
        public string CellCode
        {
            get { return _CellCode; }
            set { _CellCode = value; }
        }
        private string _CellBatch;
        /// <summary>
        /// 电池片批次
        /// </summary>
        public string CellBatch
        {
            get { return _CellBatch; }
            set { _CellBatch = value; }
        }
        private string _CellPrintMode;
        /// <summary>
        /// 电池片网版本
        /// </summary>
        public string CellPrintMode
        {
            get { return _CellPrintMode; }
            set { _CellPrintMode = value; }
        }
        private string _GlassCode;
        /// <summary>
        /// 玻璃物料号
        /// </summary>
        public string GlassCode
        {
            get { return _GlassCode; }
            set { _GlassCode = value; }
        }
        private string _GlassBatch;
        /// <summary>
        /// 玻璃批次
        /// </summary>
        public string GlassBatch
        {
            get { return _GlassBatch; }
            set { _GlassBatch = value; }
        }
        private string _EvaCode;
        /// <summary>
        /// EVA物料号
        /// </summary>
        public string EvaCode
        {
            get { return _EvaCode; }
            set { _EvaCode = value; }
        }
        private string _EvaBatch;
        /// <summary>
        /// EVA批次
        /// </summary>
        public string EvaBatch
        {
            get { return _EvaBatch; }
            set { _EvaBatch = value; }
        }
        private string _TptCode;
        /// <summary>
        /// 背板物料号
        /// </summary>
        public string TptCode
        {
            get { return _TptCode; }
            set { _TptCode = value; }
        }
        private string _TptBatch;
        /// <summary>
        /// 背板批次
        /// </summary>
        public string TptBatch
        {
            get { return _TptBatch; }
            set { _TptBatch = value; }
        }
        private string _ConBoxCode;
        /// <summary>
        /// 接线盒物料号
        /// </summary>
        public string ConBoxCode
        {
            get { return _ConBoxCode; }
            set { _ConBoxCode = value; }
        }
        private string _ConBoxBatch;
        /// <summary>
        /// 接线盒批次
        /// </summary>
        public string ConBoxBatch
        {
            get { return _ConBoxBatch; }
            set { _ConBoxBatch = value; }
        }
        private string _LongFrameCode;
        /// <summary>
        /// 长铝边框物料号
        /// </summary>
        public string LongFrameCode
        {
            get { return _LongFrameCode; }
            set { _LongFrameCode = value; }
        }
        private string _LongFrameBatch;
        /// <summary>
        /// 长铝边框批次
        /// </summary>
        public string LongFrameBatch
        {
            get { return _LongFrameBatch; }
            set { _LongFrameBatch = value; }
        }

        private string _ShortFrameCode = "";
        /// <summary>
        /// 短边框物料号
        /// </summary>
        public string ShortFrameCode
        {
            get { return _ShortFrameCode; }
            set { _ShortFrameCode = value; }
        }

        private string _ShortFrameBatch = "";
        /// <summary>
        /// 短边框批次号
        /// </summary>
        public string ShortFrameBatch
        {
            get { return _ShortFrameBatch; }
            set { _ShortFrameBatch = value; }
        }

        private string _PackingMode = "";
        /// <summary>
        /// 包装方式
        /// </summary>
        public string PackingMode
        {
            get { return _PackingMode; }
            set { _PackingMode = value; }
        }

        private string _GlassThickness = "";
        /// <summary>
        /// 玻璃厚度
        /// </summary>
        public string GlassThickness
        {
            get { return _GlassThickness; }
            set { _GlassThickness = value; }
        }

        private string _IsCancelPacking = "";
        /// <summary>
        /// 是否撤销入库
        /// </summary>
        public string IsCancelPacking
        {
            get { return _IsCancelPacking; }
            set { _IsCancelPacking = value; }
        }

        private string _IsOnlyPacking = "";
        /// <summary>
        /// 是否是拼托
        /// </summary>
        public string IsOnlyPacking
        {
            get { return _IsOnlyPacking; }
            set { _IsOnlyPacking = value; }
        }

        private string _RESV01;
        /// <summary>
        /// 预留栏位01
        /// </summary>
        public string RESV01
        {
            get { return _RESV01; }
            set { _RESV01 = value; }
        }
        private string _RESV02;
        /// <summary>
        /// 预留栏位02
        /// </summary>
        public string RESV02
        {
            get { return _RESV02; }
            set { _RESV02 = value; }
        }
        private string _RESV03;
        /// <summary>
        /// 预留栏位03
        /// </summary>
        public string RESV03
        {
            get { return _RESV03; }
            set { _RESV03 = value; }
        }
        private string _RESV04;
        /// <summary>
        /// 入库地点
        /// </summary>
        public string RESV04
        {
            get { return _RESV04; }
            set { _RESV04 = value; }
        }
        private string _RESV05;
        /// <summary>
        /// 工单类型
        /// </summary>
        public string RESV05
        {
            get { return _RESV05; }
            set { _RESV05 = value; }
        }
        private string _RESV06;
        /// <summary>
        /// 工厂
        /// </summary>
        public string RESV06
        {
            get { return _RESV06; }
            set { _RESV06 = value; }
        }
        private string _RESV07;
        /// <summary>
        /// 预留栏位07
        /// </summary>
        public string RESV07
        {
            get { return _RESV07; }
            set { _RESV07 = value; }
        }
        private string _RESV08;
        /// <summary>
        /// 预留栏位08
        /// </summary>
        public string RESV08
        {
            get { return _RESV08; }
            set { _RESV08 = value; }
        }
        private string _RESV09;
        /// <summary>
        /// 预留栏位09
        /// </summary>
        public string RESV09
        {
            get { return _RESV09; }
            set { _RESV09 = value; }
        }
        private string _RESV10;
        /// <summary>
        /// 预留栏位10
        /// </summary>
        public string RESV10
        {
            get { return _RESV10; }
            set { _RESV10 = value; }
        }
//        private string _MarketInfo;
//        public string MarketInfo
//        {
//            get { return _MarketInfo; }
//            set { _MarketInfo = value; }
//        }
//        
//         private string _LID = "";
//        /// <summary>
//        /// LID
//        /// </summary>
//        public string LIDWO
//        {
//            get { return _LID; }
//            set { _LID = value; }
//        }
//        private string _CabelLength = "";
//        public string CabelLength
//        {
//        	get{ return _CabelLength; }
//        	set{ _CabelLength = Value; }
//        
//        }
//		private string _JBoxType = "";
//		public string JBoxType
//		{
//			get { return _JBoxType; }
//			set { _JBoxType = value; }				
//		}
    }

    [Serializable]
    public class CellSerialNumberTemp
    {
        public CellSerialNumberTemp()
        { }
        private string _CartonNo = "";
        /// <summary>
        /// 托号
        /// </summary>
        public string CartonNo
        {
            get { return _CartonNo; }
            set { _CartonNo = value; }
        }
        private string _SN = "";
        /// <summary>
        /// 组件号
        /// </summary>
        public string SN
        {
            get { return _SN; }
            set { _SN = value; }
        }
        private string _CellCode = "";
        /// <summary>
        /// 电池片物料号
        /// </summary>
        public string CellCode
        {
            get { return _CellCode; }
            set { _CellCode = value; }
        }
        private string _CellBatch = "";
        /// <summary>
        /// 电池片物料批次
        /// </summary>
        public string CellBatch
        {
            get { return _CellBatch; }
            set { _CellBatch = value; }
        }
    }

    /// <summary>
    /// 数据库连接字符串配置数据信息
    /// </summary>
    [Serializable]
    public class ConfigString
    {
        private string _Sysid = "";
        /// <summary>
        /// 系统ID
        /// </summary>
        public string Sysid
        {
            get { return _Sysid; }
            set { _Sysid = value; }
        }
        private string _Factory = "";
        /// <summary>
        /// 厂区,CS-常熟
        /// </summary>
        public string Factory
        {
            get { return _Factory; }
            set { _Factory = value; }
        }
        private string _Workshop = "";
        /// <summary>
        /// 车间
        /// </summary>
        public string Workshop
        {
            get { return _Workshop; }
            set { _Workshop = value; }
        }
        private string _ProgramName = "";
        /// <summary>
        /// 配置名
        /// </summary>
        public string ProgramName
        {
            get { return _ProgramName; }
            set { _ProgramName = value; }
        }
        private string _DataSource = "";
        /// <summary>
        /// 数据库连接字符串中的DataSource
        /// </summary>
        public string DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }
        private string _DatabaseName = "";
        /// <summary>
        /// 数据库名字
        /// </summary>
        public string DatabaseName
        {
            get { return _DatabaseName; }
            set { _DatabaseName = value; }
        }
        private string _LoginUserName = "";
        /// <summary>
        /// 数据库登陆用户名
        /// </summary>
        public string LoginUserName
        {
            get { return _LoginUserName; }
            set { _LoginUserName = value; }
        }
        private string _LoginPassword = "";
        /// <summary>
        /// 数据库登陆密码
        /// </summary>
        public string LoginPassword
        {
            get { return _LoginPassword; }
            set { _LoginPassword = value; }
        }
        private string _DbLinkName = "";
        /// <summary>
        /// 数据库所在服务器名字
        /// </summary>
        public string DbLinkName
        {
            get { return _DbLinkName; }
            set { _DbLinkName = value; }
        }
        private string _Resv01 = "";
        /// <summary>
        /// 预留字段1
        /// </summary>
        public string Resv01
        {
            get { return _Resv01; }
            set { _Resv01 = value; }
        }
        private string _Resv02 = "";
        /// <summary>
        /// 预留字段2
        /// </summary>
        public string Resv02
        {
            get { return _Resv02; }
            set { _Resv02 = value; }
        }
        private string _Resv03 = "";
        /// <summary>
        /// 预留字段3
        /// </summary>
        public string Resv03
        {
            get { return _Resv03; }
            set { _Resv03 = value; }
        }
        private string _Resv04 = "";
        /// <summary>
        /// 预留字段4
        /// </summary>
        public string Resv04
        {
            get { return _Resv04; }
            set { _Resv04 = value; }
        }
        private string _Resv05 = "";
        /// <summary>
        /// 预留字段5
        /// </summary>
        public string Resv05
        {
            get { return _Resv05; }
            set { _Resv05 = value; }
        }
        private string _Resv06 = "";
        /// <summary>
        /// 预留字段6
        /// </summary>
        public string Resv06
        {
            get { return _Resv06; }
            set { _Resv06 = value; }
        }
        private string _Resv07 = "";
        /// <summary>
        /// 预留字段7
        /// </summary>
        public string Resv07
        {
            get { return _Resv07; }
            set { _Resv07 = value; }
        }
        private string _Resv08 = "";
        /// <summary>
        /// 预留字段8
        /// </summary>
        public string Resv08
        {
            get { return _Resv08; }
            set { _Resv08 = value; }
        }
        private string _Resv09 = "";
        /// <summary>
        /// 预留字段9
        /// </summary>
        public string Resv09
        {
            get { return _Resv09; }
            set { _Resv09 = value; }
        }
        private string _Resv10 = "";
        /// <summary>
        /// 预留字段10
        /// </summary>
        public string Resv10
        {
            get { return _Resv10; }
            set { _Resv10 = value; }
        }
        private string _CreatedOn = "";
        /// <summary>
        /// 创建日期
        /// </summary>
        public string CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        private string _CreatedBy = "";
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        private string _ModifiedOn = "";
        /// <summary>
        ///更新时间
        /// </summary>
        public string ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        private string _ModifedBy = "";
        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifedBy
        {
            get { return _ModifedBy; }
            set { _ModifedBy = value; }
        }
    }

    [Serializable]
    public class SysConfig
    {
        private string _SYSID = "";
        /// <summary>
        /// 系统ID
        /// </summary>
        public string SYSID
        {
            get { return _SYSID; }
            set { _SYSID = value; }
        }
        private string _FUNCTION_CODE = "";
        /// <summary>
        /// 功能名字
        /// </summary>
        public string FUNCTION_CODE
        {
            get { return _FUNCTION_CODE; }
            set { _FUNCTION_CODE = value; }
        }
        private string _MAPPING_KEY_01 = "";
        /// <summary>
        /// 配置数值1
        /// </summary>
        public string MAPPING_KEY_011
        {
            get { return _MAPPING_KEY_01; }
            set { _MAPPING_KEY_01 = value; }
        }

        private string _MAPPING_KEY_02 = "";
        /// <summary>
        /// 配置数值2
        /// </summary>
        public string MAPPING_KEY_012
        {
            get { return _MAPPING_KEY_02; }
            set { _MAPPING_KEY_02 = value; }
        }

        private string _MAPPING_KEY_03 = "";
        /// <summary>
        /// 配置数值3
        /// </summary>
        public string MAPPING_KEY_013
        {
            get { return _MAPPING_KEY_03; }
            set { _MAPPING_KEY_03 = value; }
        }

        private string _MAPPING_KEY_04 = "";
        /// <summary>
        /// 配置数值4
        /// </summary>
        public string MAPPING_KEY_014
        {
            get { return _MAPPING_KEY_04; }
            set { _MAPPING_KEY_04 = value; }
        }
        private string _MAPPING_KEY_05 = "";
        /// <summary>
        /// 配置数值5
        /// </summary>
        public string MAPPING_KEY_015
        {
            get { return _MAPPING_KEY_05; }
            set { _MAPPING_KEY_05 = value; }
        }
        private string _MAPPING_KEY_06 = "";
        /// <summary>
        /// 配置数值6
        /// </summary>
        public string MAPPING_KEY_016
        {
            get { return _MAPPING_KEY_06; }
            set { _MAPPING_KEY_06 = value; }
        }
        private string _MAPPING_KEY_07 = "";
        /// <summary>
        /// 配置数值7
        /// </summary>
        public string MAPPING_KEY_017
        {
            get { return _MAPPING_KEY_07; }
            set { _MAPPING_KEY_07 = value; }
        }

        private string _MAPPING_KEY_08 = "";
        /// <summary>
        /// 配置数值8
        /// </summary>
        public string MAPPING_KEY_08
        {
            get { return _MAPPING_KEY_08; }
            set { _MAPPING_KEY_08 = value; }
        }

        private string _MAPPING_KEY_09 = "";
        /// <summary>
        /// 配置数值9
        /// </summary>
        public string MAPPING_KEY_09
        {
            get { return MAPPING_KEY_09; }
            set { MAPPING_KEY_09 = value; }
        }
        private string _Resv01 = "";
        /// <summary>
        /// 预留字段1
        /// </summary>
        public string Resv01
        {
            get { return _Resv01; }
            set { _Resv01 = value; }
        }
        private string _Resv02 = "";
        /// <summary>
        /// 预留字段2
        /// </summary>
        public string Resv02
        {
            get { return _Resv02; }
            set { _Resv02 = value; }
        }
        private string _Resv03 = "";
        /// <summary>
        /// 预留字段3
        /// </summary>
        public string Resv03
        {
            get { return _Resv03; }
            set { _Resv03 = value; }
        }
        private string _Resv04 = "";
        /// <summary>
        /// 预留字段4
        /// </summary>
        public string Resv04
        {
            get { return _Resv04; }
            set { _Resv04 = value; }
        }
        private string _Resv05 = "";
        /// <summary>
        /// 预留字段5
        /// </summary>
        public string Resv05
        {
            get { return _Resv05; }
            set { _Resv05 = value; }
        }
        private string _Resv06 = "";
        /// <summary>
        /// 预留字段6
        /// </summary>
        public string Resv06
        {
            get { return _Resv06; }
            set { _Resv06 = value; }
        }
        private string _Resv07 = "";
        /// <summary>
        /// 预留字段7
        /// </summary>
        public string Resv07
        {
            get { return _Resv07; }
            set { _Resv07 = value; }
        }
        private string _Resv08 = "";
        /// <summary>
        /// 预留字段8
        /// </summary>
        public string Resv08
        {
            get { return _Resv08; }
            set { _Resv08 = value; }
        }
        private string _Resv09 = "";
        /// <summary>
        /// 预留字段9
        /// </summary>
        public string Resv09
        {
            get { return _Resv09; }
            set { _Resv09 = value; }
        }
        private string _Resv10 = "";
        /// <summary>
        /// 预留字段10
        /// </summary>
        public string Resv10
        {
            get { return _Resv10; }
            set { _Resv10 = value; }
        }
        private string _CreatedOn = "";
        /// <summary>
        /// 创建日期
        /// </summary>
        public string CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        private string _CreatedBy = "";
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        private string _ModifiedOn = "";
        /// <summary>
        ///更新时间
        /// </summary>
        public string ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        private string _ModifedBy = "";
        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifedBy
        {
            get { return _ModifedBy; }
            set { _ModifedBy = value; }
        }
         
        
        private string _MarketInfo = "";
        /// <summary>
        /// market
        /// </summary>
 
    }
}
