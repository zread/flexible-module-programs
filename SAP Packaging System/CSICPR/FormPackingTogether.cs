﻿using System;
//using System.IO;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using LablePLibrary;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;


namespace CSICPR
{
    public partial class FormPackingTogether : Form
    {
        #region"private parameters"
        private DataTable prodDateSource;
        private string lastBarCode = "";
        private Dictionary<string, string> nominalPower = new Dictionary<string, string>();//存储标称功率 modify by Gengao 2012-09-05
        private string mixStr = "";
        private ArrayList lst;
        private string sqlIsSch, sqlGetArtNo, sqlGetPoint;
        private string sSN, sModel;
        private int nudCPTNum, nudMix, printNum;
        private string nudBoxPower = "";
        private string nudBoxPowerGrade = "";
        private bool cbCheckPower, cbCheckSN, rbAllPrint, rbOnePrint, rbNoPrint, rbLargeLabelPrint, rbTrueP, rbNomalP, rbModel, rbAutoPacking, rbHandPacking; //modified by shen yu on July 15, 2011, added rbLargeLabelPrint variable
        private string firstcode, secondcode, thirdcode;//自动生产箱号：firstcode-车间，secondcode-序列号，thirdcode-颜色
        private int labelCnt;
        private string tbCheckCode, sModulLabePath, sCartonLabelPath, sCartonPara;
        private string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";//标签模板参数配置文件
        private ArrayList arrayCellColor;
        private string sPattern = "";
        private string sCellColor = "", sCellColorCode = "", sModuleSize = "", sCellType = "";
        private string celltype = ""; //added by Shen Yu on July 15, 2011 for checking module type
        private string sModuleClass = "";//add by alex.dong 2011-07-22 for camo module grade
        private bool CheckModuleClass;//检查是否混组件包装.
        private DataTable DtModuleClass;//组件等级
        //private clsCSGenerateCarton GenerateCarton;
        private CSLargeLabel csLabel;
        private int iLabelH, iLabelV;
        private bool bIsConfig = false;
        //-------------end add
        private Dictionary<string, string> labelParameters = new Dictionary<string, string>();//存储标签参数名称
        private Dictionary<string, string> labelValue = new Dictionary<string, string>();//存储标签参数值
        private LargeLabel largeLabel;		//added by Shen Yu on July 18, 2011 
        /// <summary>
        /// 内部托号
        /// </summary>
        private string sTempBoxID = "";
        /// <summary>
        /// 客户托号
        /// </summary>
        private string custBoxID = "";
        private string Work_Shop = "";//add by alex.dong|2012-06-07|v2.0.6
        private string CurrentWorkShopDBconn = "";//add by alex.dong|2012-06-07|v2.0.6
        private bool IsCheckPattern = false;//True:检查Pattern,false:不检查patten

        private List<MODULE> _ListModule = new List<MODULE>();//CenterDB
        private List<MODULE_TEST> _ListModuleTest = new List<MODULE_TEST>();//CenterDB
        private string _CartonPower = "";//CenterDB 标称功率
        /// <summary>
        /// 存放本车间的重工组件
        /// </summary>
        private List<string> SNLocalDBExist = new List<string>();
        /// <summary>
        /// 不是在本车间的重工组件
        /// </summary>
        private List<string> SNNotLocalDBExist = new List<string>();
        #endregion
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private FormPackingTogether()
        {
            InitializeComponent();

            prodDateSource = new DataTable();

            if (tbHandPackingSQL.Text.Trim().Equals(""))
            {
                tbHandPackingSQL.Text = ToolsClass.getSQL(tbHandPackingSQL.Name);
            }

            loadParameters();
            if (this.printNum <= 0)
                this.printNum = 1;
            string[] nudBowPowerArrays = this.nudBoxPower.ToString().Split('_');
            if (nudBowPowerArrays.Length > 1)
            {
                this.tbNominalPowerGrade.Text = nudBowPowerArrays[1];
                this.tbNominalPower.Text = nudBowPowerArrays[0];
            }
            else
            {
                this.tbNominalPowerGrade.Text = "";
                this.tbNominalPower.Text = this.nudBoxPower.ToString();
            }
            //end modify by Gengao 2012-09-05

            arrayCellColor = new ArrayList();

            //v2.2|2011-08-05|add by alex.dong|for ChangShu Carton No
            if (ToolsClass.sSite != ToolsClass.CAMO)//2011-08-10|add by alex.dong|For distinguish Workshop
            {
                sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                {
                    this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                    this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                    this.secondcode = ToolsClass.getConfig("SchSerialCode");
                    this.labelCnt = 99999;
                    custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                }
                else
                {
                    custBoxID = sTempBoxID;
                    //custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                }
                this.tbBoxCode.MaxLength = sTempBoxID.Length;
                this.tbCustBoxCode.MaxLength = custBoxID.Length;
                if (sTempBoxID.Equals(custBoxID))
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = sTempBoxID;
                }
                else
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = custBoxID;
                }
                if (this.tbBoxCode.Text.Trim().Equals(""))
                    ToolsClass.Log("维护的箱号范围已经使用，请重新维护", "ABNORMAL", lstView);
            }
            //----------------------------end add v2.2
            Work_Shop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");//add by alex.dong|2012-06-07|v2.0.6
            if (Work_Shop.ToUpper().Equals("M08"))
                btMixPackage.Visible = false;
            CurrentWorkShopDBconn = FormCover.connectionBase;
            labelParameters = ToolsClass.getLabelConfig();//Initial, Get Label Parameters
        }

        private void InitFormPacking()
        {
            prodDateSource = new DataTable();

            if (tbHandPackingSQL.Text.Trim().Equals(""))
            {
                tbHandPackingSQL.Text = ToolsClass.getSQL(tbHandPackingSQL.Name);
            }

            loadParameters();
            if (this.printNum <= 0)
                this.printNum = 1;
            string[] nudBowPowerArrays = this.nudBoxPower.ToString().Split('_');
            if (nudBowPowerArrays.Length > 1)
            {
                this.tbNominalPowerGrade.Text = nudBowPowerArrays[1];
                this.tbNominalPower.Text = nudBowPowerArrays[0];
            }
            else
            {
                this.tbNominalPowerGrade.Text = "";
                this.tbNominalPower.Text = this.nudBoxPower.ToString();
            }
            //end modify by Gengao 2012-09-05

            arrayCellColor = new ArrayList();

            //v2.2|2011-08-05|add by alex.dong|for ChangShu Carton No
            if (ToolsClass.sSite != ToolsClass.CAMO)//2011-08-10|add by alex.dong|For distinguish Workshop
            {
                sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                {
                    this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                    this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                    this.secondcode = ToolsClass.getConfig("SchSerialCode");
                    this.labelCnt = 99999;
                    custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                }
                else
                {
                    custBoxID = sTempBoxID;
                    //custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                }
                this.tbBoxCode.MaxLength = sTempBoxID.Length;
                this.tbCustBoxCode.MaxLength = custBoxID.Length;
                if (sTempBoxID.Equals(custBoxID))
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = sTempBoxID;
                }
                else
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = custBoxID;
                }
                if (this.tbBoxCode.Text.Trim().Equals(""))
                    ToolsClass.Log("维护的箱号范围已经使用，请重新维护", "ABNORMAL", lstView);
            }
            //----------------------------end add v2.2
            Work_Shop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");//add by alex.dong|2012-06-07|v2.0.6
            if (Work_Shop.ToUpper().Equals("M08"))
                btMixPackage.Visible = false;
            CurrentWorkShopDBconn = FormCover.connectionBase;
            labelParameters = ToolsClass.getLabelConfig();//Initial, Get Label Parameters
        }

        #region"Event"
        private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
        private static FormPackingTogether theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormPackingTogether();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        private void btColose_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            if (FormCover.HasPower("btColose", btColose.Text))
                new FormBoxHandling("Y","", this, true, this.rbModel, this.rbTrueP, false, btColose.Text).ShowDialog();
        }
        #endregion

        private void btPacking_Click(object sender, EventArgs e)
        {
            try
            {
                
                #region
                if (dataGridView1.RowCount != this.nudCPTNum)
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log("包装数量和要打包数量不一致，请确认！", "ABNORMAL", lstView);
                    return;
                }

                this.btPacking.Enabled = false;

                if (tbBoxCode.Text.Trim().Length != ProductStorageDAL.GetCartonNoLenth())
                {
                    this.btPacking.Enabled = true;
                    ToolsClass.Log("内部箱号(" + this.tbBoxCode.Text.Trim() + ")长度不对,必须为" + ProductStorageDAL.GetCartonNoLenth() + "!", "ABNORMAL", lstView);
                    return;
                }
                else
                {
                    #region 箱号必须是数字,用过后不能再被使用
                    string boxid = Convert.ToString(this.tbBoxCode.Text.Trim());
                    string flag = "Y";
                    foreach (char c in boxid)
                    {
                        if ((!char.IsNumber(c)))
                        {
                            flag = "N";
                            break;
                        }
                    }
                    if (flag.Equals("N"))
                    {
                        this.btPacking.Enabled = true;
                        ToolsClass.Log("内部箱号(" + boxid + ")必须为数字!", "ABNORMAL", lstView);
                        this.tbBarCode.SelectAll();
                        this.tbBoxCode.Focus();
                        return;
                    }
                    //箱号用过不允许再被使用
                    DataTable dt = ProductStorageDAL.GetCartonStatus(boxid);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        if (Convert.ToString(row["IsUsed"]).Equals("L"))
                        {
                            this.btPacking.Enabled = true;
                            ToolsClass.Log("内部箱号(" + boxid + ")为重工工单对应箱号,不能再被使用", "ABNORMAL", lstView);
                            this.tbBarCode.SelectAll();
                            this.tbBoxCode.Focus();
                            return;
                        }
                    }
                    #endregion

                    if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(2, 2)).Equals(Convert.ToString(ToolsClass.sSite.Replace("M", ""))))
                    {
                        this.btPacking.Enabled = true;
                        ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 的规则与本车间不符合!", "ABNORMAL", lstView);
                        return;
                    }

                    if (!isPackage(tbBoxCode.Text.Trim(), true))
                    {
                        this.btPacking.Enabled = true;
                        ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 已经被使用!", "ABNORMAL", lstView);
                        return;
                    }

                    if (ProductStorageDAL.GetCustCartonInfoFromCenterDB(this.tbCustBoxCode.Text.Trim()))
                    {
                        btPacking.Enabled = true;
                        ToolsClass.Log("客户托号：" + this.tbCustBoxCode.Text.Trim() + " 已经被使用!", "ABNORMAL", lstView);
                        return;
                    }

                    if (!isPackage(tbCustBoxCode.Text.Trim(), true))
                    {
                        btPacking.Enabled = true;
                        ToolsClass.Log("客户托号：" + tbCustBoxCode.Text.Trim() + " 已经被内部托号使用!", "ABNORMAL", lstView);
                        return;
                    }

                    #region 箱号添加车间后规则
                    DataTable CartonRules = ProductStorageDAL.GetPackingSystemCartonNoRuleFlag();
                    if (CartonRules != null && CartonRules.Rows.Count > 0)
                    {
                        if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(5, 2)).Equals(Convert.ToString(System.DateTime.Now.ToString("MM"))))
                        {
                            ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 中的月份(" + this.tbBoxCode.Text.Trim().Substring(5, 2) + ")与本月份（" + System.DateTime.Now.ToString("MM") + "）不符!", "ABNORMAL", lstView);
                            return;
                        }
                    }
                    #endregion
                }           
                #endregion

                if (SNLocalDBExist.Count > 0)
                    SNLocalDBExist.Clear();

                if (SNNotLocalDBExist.Count > 0)
                    SNNotLocalDBExist.Clear();

                List<List<string>> moduleClassList = new List<List<string>>();
                #region 保存数据库
                string StdPower = "";
                string sSerialNumber = "";
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (string.IsNullOrEmpty(StdPower))
                        StdPower = Convert.ToString(row.Cells[4].Value);

                    if (row.Cells[0].Value != null)
                    {
                        //此组件是否为本车间组件
                        sSerialNumber = row.Cells[0].Value.ToString().Trim();
                        DataTable sn = ProductStorageDAL.GetSNReworkInfoFromPacking(sSerialNumber);
                        if (sn != null && sn.Rows.Count > 0)
                            SNLocalDBExist.Add(sSerialNumber);
                        else
                            SNNotLocalDBExist.Add(sSerialNumber);

                        //添加组件等级
                        List<string> moduleClass = new List<string>();
                        moduleClass.Add(sSerialNumber);
                        moduleClass.Add(row.Cells["Column12"].Value.ToString().Trim());
                        moduleClassList.Add(moduleClass);
                    }
                }
                string msg="";
                if (!ProductStorageDAL.UpdateData(SNLocalDBExist, SNNotLocalDBExist, this.tbBoxCode.Text.Trim(), 
                    this.tbCustBoxCode.Text.Trim(),this.dataGridView1.Rows.Count,StdPower,moduleClassList, out msg))
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log("内部箱号: " + tbBoxCode.Text + "  并托打包失败,原因为:" + msg + "", "ABNORMAL", lstView);
                    return;
                }
                #endregion

                #region 暂时不用
                //#region
                //// 打印标签
                //if (this.sModulLabePath.Length == 0)
                //{
                //    btPacking.Enabled = true;
                //    return;
                //}

                //if (!sModuleClass.Equals("A") && (ToolsClass.sSite == ToolsClass.CAMO))
                //{
                //    if (!(MessageBox.Show("Are you sure you want to package this carton as Class " + sModuleClass + "?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                //        return;
                //}

                ////按车间排序
                //dataGridView1.Sort(dataGridView1.Columns[2], ListSortDirection.Ascending);
                ////构造SQL语句
                //string sql = "", sql2 = "", model = "", conStr = "", sMuduleNo = "";
                //model = dataGridView1[1, 0].Value.ToString();
                //int location = model.IndexOf('-');
                //if (location == -1)
                //{
                //    if (this.cbCheckPower)
                //        model = model + "-" + this.nudMix + "MP";
                //    else
                //        model = model + "-" + dataGridView1[4, 0].Value.ToString() + "P";
                //}
                //else
                //{
                //    if (this.cbCheckPower)
                //        model = model.Substring(0, location + 1) + this.nudMix + "MP";
                //    else
                //        model = model.Substring(0, location + 1) + dataGridView1[4, 0].Value.ToString() + "P";
                //}
                //#endregion

                //#region
                ////updatePackageData(sMuduleNo, model);//更新包装数据库

                ////#region//CenterDB

                ////if (dataGridView1.Rows.Count > 0 && _ListModule != null && _ListModule.Count > 0
                ////    && _ListModuleTest != null && _ListModuleTest.Count > 0)
                ////{
                ////    #region
                ////    try
                ////    {
                ////        foreach (MODULE_TEST test in _ListModuleTest)
                ////        {
                ////            new MODULE_TESTDAL().Add(test);
                ////        }

                ////        MODULE_CARTON moCarton = new MODULE_CARTON();
                ////        moCarton.SYSID = Guid.NewGuid().ToString();
                ////        if (Work_Shop.ToUpper().Equals("M08"))
                ////            moCarton.SYSID = moCarton.SYSID + "-08";
                ////        moCarton.STD_POWER_LEVEL = _CartonPower;
                ////        moCarton.CARTON_NO = tbBoxCode.Text.Trim();
                ////        moCarton.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                ////        moCarton.CREATED_BY = FormCover.CurrUserName;
                ////        moCarton.MODIFIED_BY = FormCover.CurrUserName;
                ////        moCarton.MODIFIED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                ////        new MODULE_CARTONDAL().Add(moCarton);

                ////        StringBuilder log = new StringBuilder();
                ////        foreach (MODULE mo in _ListModule)
                ////        {
                ////            if (mo.REMARK != "UnPacking")
                ////            {
                ////                new MODULEDAL().Add(mo);
                ////            }

                ////            MODULE_PACKING_LIST molk = new MODULE_PACKING_LIST();
                ////            molk.MODULE_CARTON_SYSID = moCarton.SYSID;
                ////            molk.MODULE_SYSID = mo.SYSID;
                ////            new MODULE_PACKING_LISTDAL().Add(molk);

                ////            log.AppendLine(string.Format("PackingList.CartonId:{0} ModuleSysId:{1}", moCarton.SYSID, mo.SYSID));
                ////        }

                ////        _ListModule = new List<MODULE>();
                ////        _ListModuleTest = new List<MODULE_TEST>();

                ////        ToolsClass.Log("PackingList\n" + log.ToString(), "PackingList", lstView);

                ////    }
                ////    catch (Exception exc)
                ////    {
                ////        ToolsClass.Log(exc.Message, "ABNORMAL", lstView);
                ////    }
                ////    #endregion
                ////}
                ////#endregion
                //#endregion

                //#region 保存,更新数据库
                //if (dataGridView1.Rows.Count > 0 && _ListModule != null && _ListModule.Count > 0
                //    && _ListModuleTest != null && _ListModuleTest.Count > 0)
                //{
                //    if (!_ListModule.Count.Equals(_ListModuleTest.Count))
                //    {
                //        MessageBox.Show("测试数据和包装数据的数量不一致");
                //        return;
                //    }
                //    #region
                //    try
                //    {
                //        MODULE_CARTON moCarton = new MODULE_CARTON();
                //        moCarton.SYSID = Guid.NewGuid().ToString();
                //        if (Work_Shop.ToUpper().Equals("M08"))
                //            moCarton.SYSID = moCarton.SYSID + "-08";

                //        string[] NewCartonPower = _CartonPower.Split('_');
                //        if (NewCartonPower.Length > 1)
                //            moCarton.STD_POWER_LEVEL = NewCartonPower[0];
                //        else
                //            moCarton.STD_POWER_LEVEL = _CartonPower;
                //        moCarton.CARTON_NO = tbCustBoxCode.Text.Trim();  
                //        moCarton.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                //        moCarton.CREATED_BY = FormCover.CurrUserName;
                //        moCarton.MODIFIED_BY = FormCover.CurrUserName;
                //        moCarton.MODIFIED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                //        moCarton.RESV03 = this.tbBoxCode.Text.Trim();

                //        if (!new Module_CartonDal().Packing(_ListModuleTest, _ListModule, moCarton, model, this.tbBoxCode.Text.Trim(), sModuleClass, this.dataGridView1.RowCount,this.tbCustBoxCode.Text.Trim()))
                //        {
                //            ToolsClass.Log("打包失败", "请查找原因", lstView);
                //            return;
                //            //updatePackageData(sMuduleNo, model);
                //        }

                //        _ListModule = new List<MODULE>();
                //        _ListModuleTest = new List<MODULE_TEST>();

                //        ToolsClass.Log("打包完成,开始打印标签", "PackingList", lstView);

                //    }
                //    catch (Exception exc)
                //    {
                //        ToolsClass.Log("打包时发生异常:" + exc.Message, "ABNORMAL", lstView);
                //        return;
                //    }
                //    #endregion
                //}
                //#endregion

                //#region"批量打印标签"
                //if (this.rbAllPrint && this.sModulLabePath.Length > 0)
                //{
                //    lst = new ArrayList();
                //    for (int i = 0; i < dataGridView1.RowCount; i++)
                //    {
                //        printLabel(i);
                //    }
                //}
                //#endregion

                //#region"注释CAMO列印大标签"
                ////	added by Shen Yu on July 18, 2011 for Large Label Printing
                ////////if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true && (ToolsClass.sSite == ToolsClass.CAMO))//2011-08-10|add by alex.dong|For distinguish Workshop
                ////////{
                ////////    largeLabel = new LargeLabel();

                ////////    String tempsql = "select CONVERT(varchar(10), [ProcessDate], 101) from Process where SN='" + dataGridView1[0,0].Value.ToString() + "'";
                ////////    SqlDataReader read = ToolsClass.GetDataReader(tempsql);
                ////////    if(read.Read())
                ////////    {
                ////////        largeLabel.packingDate = read.GetString(0);
                ////////    }
                ////////    read.Close();
                ////////    read = null;

                ////////    largeLabel.cartonNumber = tbBoxCode.Text;
                ////////    largeLabel.colour = sCellColor;
                ////////    largeLabel.model = dataGridView1[1,0].Value.ToString();
                ////////    largeLabel.power = tbNominalPower.Text.Trim() + "W";//dataGridView1[4,0].Value.ToString()--modify by alex.dong
                ////////    largeLabel.cartonclass = sModuleClass;

                ////////    if(largeLabel.model[largeLabel.model.Length-1] == 'M')
                ////////    {
                ////////        largeLabel.cellType = "Mono";
                ////////    }
                ////////    if(largeLabel.model[largeLabel.model.Length-1] == 'P')
                ////////    {
                ////////        largeLabel.cellType = "Poly";
                ////////    }
                ////////    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                ////////    {
                ////////        largeLabel.arraylist.Add(dataGridView1[0,i].Value.ToString());
                ////////    }
                ////////    sql = LablePrint.LargeLabelPrint(this.sCartonLabelPath, largeLabel, 1);
                ////////    largeLabel = null;
                ////////    if (sql.Length > 0)
                ////////    {
                ////////        //MessageBox.Show("Packaging data is saved, Label Print Fail:" + sql, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ////////        ToolsClass.Log("Packaging data is saved, Carton label print Failed:" + sql, "ABNORMAL", lstView);
                ////////        return;
                ////////    }
                ////////}
                //#endregion

                //#region"常熟列印大标签"
                //if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
                //{
                //    if (this.sCartonLabelPath.IndexOf(".xls") > 0)
                //    {
                //        largeLabel = new LargeLabel();
                //        largeLabel.cartonNumber = tbBoxCode.Text;
                //        largeLabel.colour = sCellColor;
                //        largeLabel.model = dataGridView1[1, 0].Value.ToString();

                //        string[] powerArray = tbNominalPower.Text.Trim().Split('_');
                //        if (powerArray.Length > 1)
                //        {
                //            largeLabel.power = powerArray[0].ToString();
                //            largeLabel.powerGrade = powerArray[1];
                //        }
                //        else
                //        {
                //            largeLabel.power = tbNominalPower.Text.Trim();
                //            largeLabel.powerGrade = tbNominalPowerGrade.Text.ToString().TrimEnd();
                //        }

                //        largeLabel.size = sModuleSize;
                //        largeLabel.pattern = sPattern;

                //        if (largeLabel.model[largeLabel.model.Length - 1] == 'M')
                //        {
                //            largeLabel.cellType = "Mono";
                //            largeLabel.IsHavePattern = false;
                //        }
                //        if (largeLabel.model[largeLabel.model.Length - 1] == 'P')
                //        {
                //            largeLabel.cellType = "Poly";
                //            if (!string.IsNullOrEmpty(sPattern) && IsCheckPattern == true)
                //                largeLabel.IsHavePattern = true;
                //        }
                //        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                //        {
                //            largeLabel.arraylist.Add(dataGridView1[0, i].Value.ToString());
                //        }
                //        csLabel = new CSLargeLabel("", this.sCartonLabelPath);
                //        csLabel.Write2Excel(this.iLabelH, iLabelV, largeLabel);
                //    }
                //    else
                //    {
                //        ToolsClass.Log("竖包装标签选择不正确，请选择【Excel】格式的标签文件！", "ABNORMAL", lstView);
                //        return;
                //    }
                //}
                //#endregion

                //#region"打印包标签"
                //if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == false)
                //{
                //    sql = LablePrint.printingLable(this.sCartonLabelPath, this.sCartonPara, this.tbCustBoxCode.Text, 2);
                //    if (sql.Length > 0)
                //    {
                //        ToolsClass.Log("Packaging data is saved, Carton label print Failed:" + sql, "ABNORMAL", lstView);
                //    }
                //}
                //#endregion
                #endregion

                #region

                ToolsClass.Log("内部箱号: " + tbBoxCode.Text + "  打包完成", "NORMAL", lstView);

                if (!sModuleClass.Equals("A") && (ToolsClass.sSite == ToolsClass.CAMO))
                {
                    sModuleClass = "A";
                    ToolsClass.saveXMLNode("ModuleClass", sModuleClass);
                }
                loadParameters();
                if (ToolsClass.sSite != ToolsClass.CAMO)
                {
                    sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                    if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                    {
                        this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                        this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                        this.secondcode = ToolsClass.getConfig("SchSerialCode");
                        this.labelCnt = 99999;
                        custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                    }
                    else
                    {
                        custBoxID = sTempBoxID;
                    }

                    if (sTempBoxID.Equals(custBoxID))
                    {
                        tbBoxCode.Text = sTempBoxID;
                        tbCustBoxCode.Text = sTempBoxID;
                    }
                    else
                    {
                        tbBoxCode.Text = sTempBoxID;
                        tbCustBoxCode.Text = custBoxID;
                    }
                }
                else
                {
                    tbBoxCode.Text = "";
                    tbCustBoxCode.Text = "";
                }

                arrayCellColor.Clear();
                sPattern = "";
                nominalPower.Clear();
                dataGridView1.Rows.Clear();
                #endregion

                this.btPacking.Enabled = false;
                this.chbCarton.Checked = false;
                this.chbCustCarton.Checked = false;
                this.tbBoxCode.Enabled = false;
                this.tbCustBoxCode.Enabled = false;
            }
            catch (Exception ex)
            {
                this.btPacking.Enabled = true;
                ToolsClass.Log("打包时发生异常:"+ex.Message+"!", "ABNORMAL", lstView);
                return;
            }
        }

        private void FormPacking_Activated(object sender, EventArgs e)
        {
            if (tbBarCode.Enabled) tbBarCode.Focus();
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbHandPackingSQL.Undo();
        }

        #region 包装验证
        private void tbBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                #region
                if (bIsConfig)
                {
                    loadParameters();
                    bIsConfig = false;
                }

                if (dataGridView1.Rows.Count == 0)
                {
                    arrayCellColor.Clear();
                    sPattern = "";
                    nominalPower.Clear();
                }

                if (e.KeyChar == '\r')
                {
                    Thread.Sleep(50);

                    string strTemp = tbBarCode.Text;
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                        if (sDataSet[i] != null && sDataSet[i] != "")
                            package(sDataSet[i], sender, e);
                    }
                    tbBarCode.Clear();
                    if (dataGridView1.Rows.Count > 0)
                        dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.Rows.Count - 1;
                }
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("打包时发生异常:"+ex.Message+"!", "ABNORMAL", lstView);
                return;
            }
        }

        /// <summary>
        /// 组件打包验证
        /// </summary>
        /// <param name="sSerialNumber">组件序列号</param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="isBatch"></param>
        private void package(string sSerialNumber, object sender, KeyPressEventArgs e, bool isBatch = true)
        {
            try
            {
                #region
                //是否已经打包完成
                if (dataGridView1.RowCount == this.nudCPTNum)
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log("已经达到箱包装数量，请包装！", "ABNORMAL", lstView);
                    return;
                }

                //检查是否已经打包过
                if (!isPackage(sSerialNumber, false))
                {
                    //ToolsClass.Log("组件：" + sSerialNumber + " 已经打包过!", "ABNORMAL", lstView);
                    return;
                }

                #region 检查箱号是否符合规则
                if (this.dataGridView1.Rows.Count < 1)
                {
                    if (tbBoxCode.Text.Trim().Length != ProductStorageDAL.GetCartonNoLenth())
                    {
                        ToolsClass.Log("内部箱号(" + this.tbBoxCode.Text.Trim() + ")长度不对,必须为" + ProductStorageDAL.GetCartonNoLenth() + "!", "ABNORMAL", lstView);
                        return;
                    }
                    else
                    {
                        #region 内部箱号必须是数字,用过后不能再被使用
                        string boxid = Convert.ToString(this.tbBoxCode.Text.Trim());
                        string flag = "Y";
                        foreach (char c in boxid)
                        {
                            if ((!char.IsNumber(c)))
                            {
                                flag = "N";
                                break;
                            }
                        }
                        if (flag.Equals("N"))
                        {
                            ToolsClass.Log("内部箱号(" + boxid + ")必须为数字!", "ABNORMAL", lstView);
                            this.tbBarCode.SelectAll();
                            this.tbBoxCode.Focus();
                            return;
                        }
                        //箱号用过不允许再被使用
                        DataTable dt1 = ProductStorageDAL.GetCartonStatus(boxid);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            DataRow row = dt1.Rows[0];
                            if (Convert.ToString(row["IsUsed"]).Equals("L"))
                            {
                                ToolsClass.Log("内部箱号(" + boxid + ")为重工工单对应箱号,不能再被使用", "ABNORMAL", lstView);
                                this.tbBarCode.SelectAll();
                                this.tbBoxCode.Focus();
                                return;
                            }
                        }
                        #endregion

                        if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(2, 2)).Equals(Convert.ToString(ToolsClass.sSite.Replace("M", ""))))
                        {
                            ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 的规则与本车间不符合!", "ABNORMAL", lstView);
                            return;
                        }

                        if (!isPackage(tbBoxCode.Text.Trim(), true))
                        {
                            ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 已经被使用!", "ABNORMAL", lstView);
                            return;
                        }

                        #region 箱号添加车间后规则
                        DataTable CartonRules = ProductStorageDAL.GetPackingSystemCartonNoRuleFlag();
                        if (CartonRules != null && CartonRules.Rows.Count > 0)
                        {
                            if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(5, 2)).Equals(Convert.ToString(System.DateTime.Now.ToString("MM"))))
                            {
                                ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 中的月份(" + this.tbBoxCode.Text.Trim().Substring(5, 2) + ")与本月份（" + System.DateTime.Now.ToString("MM") + "）不符!", "ABNORMAL", lstView);
                                return;
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                //检查组件是否已经扫描在列表里
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[0].Value != null && row.Cells[0].Value.Equals(sSerialNumber))
                    {
                        ToolsClass.Log("这个组件：" + sSerialNumber + " 在打包列表第 " + (row.Index + 1) + " 行", "ABNORMAL", lstView);
                        tbBarCode.Clear();
                        return;
                    }
                }

                //组件等级需要提前维护
                if (this.sModuleClass.Trim().Equals(""))
                {
                    MessageBox.Show("组件等级不能为空，请在包装参数配置里维护");
                    return;
                }

                #region 检查组件是否为重工工单所属组件
                DataTable dt = ProductStorageDAL.GetSNInfoByReworkFromCenterDB(sSerialNumber);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows.Count > 1)
                    {
                        ToolsClass.Log("此组件(" + sSerialNumber + ")属于多个重工工单,请确认!", "ABNORMAL", lstView);
                        return;
                    }

                    DataRow row = dt.Rows[0];

                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column1"].Value = row["MODULE_SN"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column2"].Value = row["MODULE_TYPE"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column3"].Value = row["WORKSHOP"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column4"].Value = row["PMAX"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column5"].Value = row["STD_POWER_LEVEL"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column8"].Value = null;
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column6"].Value = null;
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column7"].Value = row["WORK_ORDER"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column10"].Value = row["WORK_ORDER"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column11"].Value = row["REWORK"];
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column13"].Value = row["IMP"];
                    DataGridViewComboBoxColumn cb;
                    cb = (DataGridViewComboBoxColumn)dataGridView1.Columns["Column12"];
                    cb.DataSource = DtModuleClass;
                    cb.DisplayMember = "MAPPING_KEY_01";
                    cb.ValueMember = "MAPPING_KEY_01";
                    this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column12"].Value = sModuleClass;
                    this.dataGridView1.Columns["Column12"].ReadOnly = !this.CheckModuleClass;

                    //this.dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { row["MODULE_SN"], row["MODULE_TYPE"],
                    //    row["WORKSHOP"], row["PMAX"], row["STD_POWER_LEVEL"], null, null, row["WORK_ORDER"], row["WORK_ORDER"], row["REWORK"] });
                    ToolsClass.Log("添加组件:" + sSerialNumber + " 到打包列表", "NORMAL", lstView);
                 }
                 else
                 {
                     //多次重工
                     DataTable dt1 = ProductStorageDAL.GetSNInfoByFromPackingDB(sSerialNumber);
                     if (dt1 != null && dt1.Rows.Count > 0)
                     {
                         DataRow rows = dt1.Rows[0];

                         this.dataGridView1.Rows.Add();
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column1"].Value = rows["SN"];
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column2"].Value = rows["ModelType"];
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column3"].Value = FormCover.CurrentFactory;
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column4"].Value = rows["Pmax"];
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column5"].Value = rows["StdPower"];
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column8"].Value = null;
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column6"].Value = null;
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column7"].Value = null;
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column10"].Value = null;
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column11"].Value = rows["ReWorkOrder"];
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column13"].Value =
                             ProductStorageDAL.GetSNImByFromPackingDB(sSerialNumber);
                         DataGridViewComboBoxColumn cb;
                         cb = (DataGridViewComboBoxColumn)dataGridView1.Columns["Column12"];
                         cb.DataSource = DtModuleClass;
                         cb.DisplayMember = "MAPPING_KEY_01";
                         cb.ValueMember = "MAPPING_KEY_01";
                         this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column12"].Value = sModuleClass;
                         this.dataGridView1.Columns["Column12"].ReadOnly = !this.CheckModuleClass;

                         //this.dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { rows["SN"], rows["ModelType"],
                         //    FormCover.CurrentFactory, rows["Pmax"], rows["StdPower"], 
                         //    null, null, null, null, rows["ReWorkOrder"] });
                         ToolsClass.Log("添加组件:" + sSerialNumber + " 到打包列表", "NORMAL", lstView);
                     }
                     else
                     {
                         ToolsClass.Log("此组件：" + sSerialNumber + " 不是本车间重工工单所属组件,请确认!", "ABNORMAL", lstView);
                         return;
                     }
                 }
                #endregion

                if (dataGridView1.RowCount == this.nudCPTNum)
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log("已经达到箱包装数量，请包装！", "ABNORMAL", lstView);
                    return;
                }

                #region 暂时不用
                //遏制检查
                //if (checkBox1.Checked)
                //{
                //    if (FormCurb.isIn(sSerialNumber))
                //    {
                //        ToolsClass.Log(sSerialNumber + " was prohibited,can not be packaged！", "ABNORMAL", lstView);
                //        tbBarCode.Clear();
                //        return;
                //    }
                //}
                #endregion
         
                #region
                ////检查是否打印组件标签
                //if (this.sModulLabePath.Length == 0 && (this.rbAllPrint || this.rbOnePrint))
                //{
                //    tbBarCode.Text = "";
                //    tbBarCode.Focus();
                //    ToolsClass.Log("组件(" + sSerialNumber + ")没有维护标签模板", "ABNORMAL", lstView);
                //    return;
                //}

                //if (!verifyCTM(sSerialNumber))
                //    return;

                //if (!verifyModuleCanBePackage(sSerialNumber))
                //    return;

                ////打印SN组件标签
                //if (this.rbOnePrint && this.sModulLabePath.Length > 0)
                //{
                //    printLabel(dataGridView1.Rows.Count - 1);
                //}

                //if (dataGridView1.RowCount == this.nudCPTNum)
                //{
                //    if (this.rbAutoPacking)
                //    {
                //        btPacking.Enabled = true;
                //        btPacking_Click(sender, e);
                //    }
                //    else
                //    {
                //        btPacking.Enabled = true;
                //        ToolsClass.Log("已经达到箱包装数量，请包装！", "ABNORMAL", lstView);
                //        return;
                //    }
                //}
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("打包时发生异常："+ex.Message+"!", "ABNORMAL", lstView);
                return;
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Index < 0)
                return;

            string sn = this.dataGridView1[0, e.Row.Index].Value.ToString();
            for (int i = 0; i < _ListModule.Count; i++)
            {
                if (sn == _ListModule[i].MODULE_SN)
                {
                    _ListModule.RemoveAt(i);
                    break;
                }
            }

            for (int i = 0; i < _ListModuleTest.Count; i++)
            {
                if (sn == _ListModuleTest[i].MODULE_SN)
                {
                    _ListModuleTest.RemoveAt(i);
                    break;
                }
            }
        }
        #endregion

        #region"Control Event"
        private void btRePrint_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            //FormRePrint frp = new FormRePrint(this, true, this.rbModel, this.rbTrueP);
            //frp.ShowDialog();
            //frp.Dispose();
        }

        private void btTodayPackged_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            //if (FormCover.HasPower("btTodayPackged", btTodayPackged.Text))
            //    new FormBoxHandling("", this, true, this.rbModel, this.rbTrueP, true, btTodayPackged.Text).ShowDialog();
        }

        private void FormPacking_FormClosing(object sender, FormClosingEventArgs e)
        {
            nominalPower.Clear();
            arrayCellColor.Clear();
            sPattern = "";

            if (_ListModule.Count > 0)
                _ListModule.Clear();
            if (_ListModuleTest.Count > 0)
                _ListModuleTest.Clear();

            int j = 0;
            //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //else
            //    j = int.Parse(ToolsClass.getConfig("SerialCode"));

            if (!tbBoxCode.Text.Trim().Equals(""))
            {
                j = int.Parse(ToolsClass.getConfig("SerialCode"));

                string strsql = "update Box set IsUsed='N' where BoxID='" + tbBoxCode.Text.Trim() + "'";
                int iSql = ToolsClass.ExecuteNonQuery(strsql);
                j--;
                //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                //    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
                //else
                if (iSql > 0)
                    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            }
            if (!this.tbCustBoxCode.Text.Trim().Equals(""))
            {
                j = int.Parse(ToolsClass.getConfig("SchSerialCode"));

                string strsql = "update Box set IsUsed='N' where BoxID='" + tbCustBoxCode.Text.Trim() + "'";
                int iSql = ToolsClass.ExecuteNonQuery(strsql);
                j--;
                //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                if (iSql > 0)
                    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
                //else
                //    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            }
            ToolsClass.Log("包装画面已经关闭,  当前箱号: " + tbBoxCode.Text, "NORMAL", lstView);
        }

        private void btTodayNoPackge_Click(object sender, EventArgs e)
        {
            new FormNoPackge().ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                if (new FormCurb().ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    checkBox1.Checked = false;
            }
            tbBarCode.Focus();
        }

        private void btnBatchMode_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                var frm = new FormConfig("Y");
                frm.ShowDialog();
                if (frm != null)
                {
                    frm.Dispose();
                }
                bIsConfig = true;
            }
            else
            {
                MessageBox.Show("当前正在打包，请清除打包资料！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ToolsClass.Log("当前正在打包，请清除打包资料！", "ABNORMAL", lstView);
            }
            //InitFormPacking();
            #region Marked by alex.dong
            //////ArrayList Serialist = new ArrayList();
            //////KeyPressEventArgs ep = new KeyPressEventArgs('\r');
            //////OpenFileDialog open = new OpenFileDialog();
            //////open.Title = "Import Module Serial Number";
            //////open.Filter = "Notepad File(*.txt)|*.txt";
            //////if (open.ShowDialog() == DialogResult.OK)
            //////{
            //////    StreamReader sReader = new StreamReader(open.FileName);
            //////    string sLine = "";
            //////    while (sLine != null)
            //////    {
            //////        sLine = sReader.ReadLine();
            //////        if (sLine != null && sLine.Trim() !="")
            //////            Serialist.Add(sLine);
            //////    }
            //////    sReader.Close();
            //////}
            //////int i = Serialist.Count;
            //////foreach (string sSN in Serialist)
            //////{
            //////    tbBarCode.Text = sSN;
            //////    tbBarCode_KeyPress(sender, ep);
            //////}
            #endregion
        }

        private void nudIdealPower_ValueChanged(object sender, EventArgs e)
        {
        }

        private void tbNominalPower_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.tbNominalPower.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(this.tbNominalPowerGrade.Text.TrimEnd()))
                {
                    ToolsClass.saveXMLNode("IdealPower", this.tbNominalPower.Text.Trim() + "_" + this.tbNominalPowerGrade.Text.TrimEnd());//Modify by Gengao 2012-09-05
                    this.nudBoxPower = this.tbNominalPower.Text.Trim() + "_" + this.tbNominalPowerGrade.Text.TrimEnd(); //Modify by Gengao 2012-09-05
                    ToolsClass.Log("上次包装的标称功率是:" + this.tbNominalPower.Text + " 标称功率档次是：" + this.tbNominalPowerGrade.Text.Trim(), "NORMAL", lstView);//Save Nominal Power
                }
                else
                {
                    ToolsClass.saveXMLNode("IdealPower", this.tbNominalPower.Text.Trim());
                    this.nudBoxPower = this.tbNominalPower.Text.Trim();
                    ToolsClass.Log("上次包装的标称功率是:" + this.tbNominalPower.Text, "NORMAL", lstView);//Save Nominal Power
                }
            }
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(dataGridView1.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(System.Globalization.CultureInfo.CurrentCulture),
                        dataGridView1.DefaultCellStyle.Font, b, e.RowBounds.Location.X + 20, e.RowBounds.Location.Y + 4);
            }
        }

        private void FormPacking_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion


            lstView.Columns.Add("Package Log", this.Width, HorizontalAlignment.Left);
            this.chbCarton.Checked = false;
            this.chbCustCarton.Checked = false;
            this.tbBoxCode.Enabled = false;
            this.tbCustBoxCode.Enabled = false;

            DtModuleClass = ProductStorageDAL.GetModuleClassInfo("ModuleClass");
            if (DtModuleClass != null && DtModuleClass.Rows.Count > 0)
            { }
            else
            {
                MessageBox.Show("组件等级没有维护，请联系系统管理员");
                this.Close();
            }
        }

        private void FormPacking_Resize(object sender, EventArgs e)
        {
            if (lstView.Columns.Count > 0)
                lstView.Columns[0].Width = this.Width;
        }

        private void btMixPackage_Click(object sender, EventArgs e)
        {
            new FormNoPackge().ShowDialog();
        }
        #endregion

        #region"Method"
        /// <summary>
        /// 检查混装功率种类
        /// </summary>
        /// <param name="p">标称功率</param>
        /// <param name="sSerialNumber">组件序列号</param>
        /// <returns>true:不能混装，false:可以混装</returns>
        private bool checkPower(string p, string sSerialNumber = "") //Modify by Gengao 2012-09-05
        {
            try
            {
                if (nominalPower.ContainsKey(p))
                {
                    nominalPower[p] = nominalPower[p] + 1;
                }
                else
                {
                    if (this.cbCheckPower)
                    {
                        if (nominalPower.Count < this.nudMix)
                        {
                            nominalPower.Add(p, "1"); //Modify by Gengao 2012-09-05
                        }
                        else
                        {
                            ToolsClass.Log(sSerialNumber + " 组件标称功率是:" + p + ",超过最大混装数量!", "ABNORMAL", lstView);//Module Nominal Power is,exceeds mix limit
                            return false;
                        }
                    }
                    else
                    {
                        if (nominalPower.Count == 0 && this.nudBoxPower.ToString() == p)
                        {
                            nominalPower.Add(p, "1");
                        }
                        else
                        {
                            string[] arrays = p.Split('_');
                            if (arrays.Length > 1)
                                ToolsClass.Log(sSerialNumber + " 组件标称功率是:" + arrays[0] + " 标称功率档次为：" + arrays[1] + " , 不允许混装", "ABNORMAL", lstView);
                            else
                                ToolsClass.Log(sSerialNumber + " 组件标称功率是:" + arrays[0] + " 标称功率档次为： 不允许混装", "ABNORMAL", lstView);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(sSerialNumber + " 组件在检查是否可以混装时发生异常:"+ex.Message+"", "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 检查工作号(工单)是否可以混装
        /// </summary>
        /// <param name="sn"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        private bool checkWorkCode(string sn, out string reason)
        {
            string tigWorkCode = "";
            reason = "";
            int location = this.tbCheckCode.LastIndexOf('-') + 1;
            reason = sn.Substring(0, location);
            if (this.tbCheckCode.EndsWith("*"))
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    tigWorkCode = dataGridView1[0, 0].Value.ToString().Substring(0, location);
                }
                else
                {
                    tigWorkCode = reason;
                }
            }
            else
            {
                tigWorkCode = this.tbCheckCode.Substring(location);
            }
            if (tigWorkCode == reason)
            {
                //reason = "";
            }
            else
            {
                if (MessageBox.Show(sn + " 组件工作号为：" + reason + (tbCheckCode.EndsWith("*") ? "，与其它组件不同" : "，与指定工作号不符") + "，要继续吗？", "提示信息", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 将组件号码和箱号在数据库中做Mapping关系
        /// </summary>
        /// <param name="sMuduleNo">组件序列号</param>
        /// <param name="model">版型</param>
        private void updatePackageData(string sMuduleNo, string model)
        {
            string conStr = "";
            object obj = dataGridView1[2, 0].Value;
            int start = 0;
            for (int f = 1; f < dataGridView1.RowCount; f++)
            {
                if (dataGridView1[2, f].Value == obj && f != dataGridView1.RowCount - 1)
                {
                    continue;
                }
                else
                {
                    if (obj.ToString() == "")
                        conStr = FormCover.connectionBase;
                    else
                        conStr = ToolsClass.getSQL(obj.ToString());
                    if ((f == dataGridView1.RowCount - 1) && obj == dataGridView1[2, f].Value)
                        f = dataGridView1.RowCount;
                    else
                        obj = dataGridView1[2, f].Value;
                }
                string sql = "", sql2 = "";
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(conStr))
                using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    SqlTransaction transaction = null;
                    try
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        transaction = conn.BeginTransaction();
                        cmd.Transaction = transaction;
                        sql = "Update Box set IsUsed='Y',BoxType='" + model + "',ModelType='',Number=" + dataGridView1.RowCount + ",Mix='" + mixStr + "',PackOperator='" + FormCover.CurrUserWorkID + "' where BoxID='" + tbBoxCode.Text.Trim() + "';";
                        sql = sql + "if not exists (select BoxID from Box where boxid='" + tbBoxCode.Text.Trim() + "') insert into Box(BoxID,BoxType,ModelType,Number,IsUsed,PackOperator) values('" + tbBoxCode.Text.Trim() + "','" + model + "',''," + dataGridView1.RowCount + ",'Y','" + FormCover.CurrUserWorkID + "')";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                        for (int t = start; t < f; t++)
                        {
                            sMuduleNo = dataGridView1[0, t].Value.ToString();
                            sql2 = "UPDATE [Product] SET [Process]='T5',[BoxID]='" + tbBoxCode.Text.Trim() + "',[Mix]='" + mixStr + "',[ModuleClass]='" + sModuleClass + "'" + ",[ProcessDateTime]=getdate()" + " WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from Product where SN='" + sMuduleNo + "')";
                            cmd.CommandText = sql2;
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (transaction != null)
                            transaction.Rollback();
                        ToolsClass.Log("包装失败:" + ex.Message, "ABNORMAL", lstView);
                        return;
                    }
                }
                start = f;
                if (f == dataGridView1.RowCount - 1)
                    f--;
            }

            btPacking.Enabled = false;
            mixStr = "";
        }

        /// <summary>
        /// 加载包装参数
        /// </summary>
        private void loadParameters()
        {
            try
            {
                this.sModulLabePath = ToolsClass.getConfig("ModuleLabelPath");
                this.sCartonLabelPath = ToolsClass.getConfig("CartonLabelPath");
                this.sCartonPara = ToolsClass.getConfig("CartonLabelParameter");

                this.nudCPTNum = int.Parse(ToolsClass.getConfig("CartonQty"));
                this.nudBoxPower = ToolsClass.getConfig("IdealPower");
                this.nudMix = int.Parse(ToolsClass.getConfig("MixCount"));

                this.cbCheckPower = bool.Parse(ToolsClass.getConfig("MixCount", true, "Checked"));
                this.cbCheckSN = bool.Parse(ToolsClass.getConfig("CheckRule", true, "Checked"));
                this.tbCheckCode = ToolsClass.getConfig("CheckRule");

                this.rbOnePrint = bool.Parse(ToolsClass.getConfig("OnePrint"));
                this.rbAllPrint = bool.Parse(ToolsClass.getConfig("BatchPrint"));
                this.rbNoPrint = bool.Parse(ToolsClass.getConfig("NoPrint"));
                this.rbLargeLabelPrint = bool.Parse(ToolsClass.getConfig("LargeLabelPrint"));

                this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
                this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));
                this.rbNomalP = bool.Parse(ToolsClass.getConfig("NominalPower"));
                if (ToolsClass.sSite == ToolsClass.CAMO)
                {
                    this.firstcode = ToolsClass.getConfig("StationNo").Replace('\n', ' ').Trim();
                    this.sModuleClass = ToolsClass.getConfig("ModuleClass");
                }
                else
                {
                    this.firstcode = ToolsClass.getConfig("FirstCode").Replace('\n', ' ').Trim();
                    this.labelCnt = int.Parse(ToolsClass.getConfig("LabelCount").Replace('\n', ' ').Trim());
                    this.thirdcode = ToolsClass.getConfig("ColorCode").Replace('\n', ' ').Trim();
                    this.iLabelH = int.Parse(ToolsClass.getConfig("ExcelH"));
                    this.iLabelV = int.Parse(ToolsClass.getConfig("ExcelV"));
                    this.sModuleClass = ToolsClass.getConfig("ModuleClass");
                }

                try
                {
                    this.CheckModuleClass = bool.Parse(ToolsClass.getConfig("CheckModuleClass"));
                }
                catch (Exception ex)
                {
                    this.CheckModuleClass = false;
                }

                this.secondcode = ToolsClass.getConfig("SerialCode");

                this.rbAutoPacking = bool.Parse(ToolsClass.getConfig("AutoMode"));
                this.rbHandPacking = bool.Parse(ToolsClass.getConfig("ManualMode"));

                this.printNum = int.Parse(ToolsClass.getConfig("PrintCount"));

                ToolsClass.Log("Parameter loading successful!", "NORMAL", lstView);
            }
            catch
            {
                ToolsClass.Log("Parameter loading failed!", "ABNORMAL", lstView);
            }
        }

        /// <summary>
        /// 收集Label标签value，记录在ArrayList
        /// </summary>
        private void arrayLabelInfo(string sModelType, string sSerialNo = "")
        {
            if (sSerialNo != "")
                this.sSN = sSerialNo;
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            {
                sqlGetPoint = "select Pmpp,Vmpp,Impp,VOC,ISC from point with(nolock) where InterID =(select ItemPointID from Scheme with(nolock) where InterID=(Select distinct SchemeInterID From ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')))";
                //(select ItemPointID from Scheme where ItemMPID=(select InterID from Item where ItemValue='"+temp[1]+"') and ItemModelID=(select InterID from Item where ItemValue='" + temp[0] + "'))";
                sqlGetArtNo = "select NOValue as ART_No from ModelType with(nolock) where ModelTypeID=";
                sqlIsSch = "select Pmax as Pmpp,VOC as Voc,ISC as Isc,Vm as Vmpp,Im as Impp from ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')";

                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                string sArtNo = "";
                int iPmpp = 0, iImpp = 0, iVmpp = 0, iVOC = 0, iISC = 0;
                //sqlGetPoint = sqlGetPoint + "'" + sSN + "'))";
                SqlDataReader read1 = ToolsClass.GetDataReader(sqlGetPoint);
                if (read1.Read())
                {
                    iPmpp = read1.GetInt32(0);
                    iVmpp = read1.GetInt32(1);
                    iImpp = read1.GetInt32(2);
                    iVOC = read1.GetInt32(3);
                    iISC = read1.GetInt32(4);
                }

                sqlGetArtNo = sqlGetArtNo + "'" + sModelType + "'";
                SqlDataReader read2 = ToolsClass.GetDataReader(sqlGetArtNo);
                if (read2.Read())
                    sArtNo = read2.GetString(0);

                if (read.Read())
                {
                    this.sSN = sArtNo + this.sSN + ":" + ToolsClass.Round(read.GetDouble(0), iPmpp).ToString() + ":" + ToolsClass.Round(read.GetDouble(1), iVOC).ToString() + ":" + ToolsClass.Round(read.GetDouble(2), iISC).ToString() + ":" + ToolsClass.Round(read.GetDouble(3), iVmpp).ToString() + ":" + ToolsClass.Round(read.GetDouble(4), iImpp).ToString();
                }

                read.Close(); read = null;
                read1.Close(); read1 = null;
                read2.Close(); read2 = null;
            }
            else
            {
                sqlIsSch = "select CellSpec as Size,CellType as type,CellColor as Color from Product with(nolock) where SN='" + this.sSN + "' and InterID =(select max(InterID) from Product with(nolock) where SN='" + this.sSN + "')";
                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                if (read.Read())
                {
                    this.sSN = this.sSN + ":" + this.sModel + ":" + read.GetString(0) + ":" + read.GetString(1) + ":" + read.GetString(2);
                }

                read.Close(); read = null;
            }
            lst.Add(this.sSN);
        }

        /// <summary>
        /// 收集Label标签value，记录在数据字典
        /// </summary>
        /// <param name="sModelType">组件版型</param>
        /// <param name="sSerialNo">组件序列号</param>
        private void dictLabelInfo(string sModelType, string sSerialNo = "")
        {
            if (sSerialNo != "")
                this.sSN = sSerialNo;
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            {
                sqlGetPoint = "select Pmpp,Vmpp,Impp,VOC,ISC from point with(nolock) where InterID =(select ItemPointID from Scheme with(nolock) where InterID=(Select distinct SchemeInterID From ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')))";
                sqlGetArtNo = "select NOValue as ART_No from ModelType where ModelTypeID=";
                sqlIsSch = "select Pmax as Pmpp,VOC as Voc,ISC as Isc,Vm as Vmpp,Im as Impp from ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')";

                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                string sArtNo = "";
                int iPmpp = 0, iImpp = 0, iVmpp = 0, iVOC = 0, iISC = 0;
                SqlDataReader read1 = ToolsClass.GetDataReader(sqlGetPoint);
                if (read1.Read())
                {
                    iPmpp = read1.GetInt32(0);
                    iVmpp = read1.GetInt32(1);
                    iImpp = read1.GetInt32(2);
                    iVOC = read1.GetInt32(3);
                    iISC = read1.GetInt32(4);
                }

                sqlGetArtNo = sqlGetArtNo + "'" + sModelType + "'";
                SqlDataReader read2 = ToolsClass.GetDataReader(sqlGetArtNo);
                if (read2.Read())
                    sArtNo = read2.GetString(0);

                if (read.Read())
                {
                    this.labelValue = new Dictionary<string, string>();
                    this.labelValue.Add(this.labelParameters["ModuleNo"], sArtNo + this.sSN);
                    this.labelValue.Add(this.labelParameters["Pmpp"], ToolsClass.Round(read.GetDouble(0), iPmpp).ToString());
                    this.labelValue.Add(this.labelParameters["Voc"], ToolsClass.Round(read.GetDouble(1), iVOC).ToString());
                    this.labelValue.Add(this.labelParameters["Isc"], ToolsClass.Round(read.GetDouble(2), iISC).ToString());
                    this.labelValue.Add(this.labelParameters["Vmpp"], ToolsClass.Round(read.GetDouble(3), iVmpp).ToString());
                    this.labelValue.Add(this.labelParameters["Impp"], ToolsClass.Round(read.GetDouble(4), iImpp).ToString());
                }

                read.Close(); read = null;
                read1.Close(); read1 = null;
                read2.Close(); read2 = null;
            }
            else
            {
                sqlIsSch = "select Size,type,Color,ElecParaTest.Pmax from(select CellSpec as Size,CellType as type,CellColor as Color,InterNewTempTableID from Product with(nolock) where SN='" + this.sSN + "' and InterID =(select max(InterID) from Product with(nolock) where SN='" + this.sSN + "'))as Prod inner join ElecParaTest on ElecParaTest.InterID=Prod.InterNewTempTableID";
                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                if (read.Read())
                {
                    this.labelValue = new Dictionary<string, string>();
                    this.labelValue.Add(this.labelParameters["ModuleNo"], this.sSN);
                    this.labelValue.Add(this.labelParameters["Model"], this.sModel);
                    this.labelValue.Add(this.labelParameters["Size"], read.GetString(0));
                    this.labelValue.Add(this.labelParameters["ChipType"], read.GetString(1));
                    this.labelValue.Add(this.labelParameters["Color"], read.GetString(2));
                    this.labelValue.Add(this.labelParameters["Pmpp"], ToolsClass.Round(read.GetDouble(3), 1).ToString());
                }

                read.Close(); read = null;
            }
        }

        /// <summary>
        /// 验证CTM是否判等
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyCTM(string sSerialNumber)
        {
            try
            {
                #region
                bool verify = true;
                string sqlCTM = "select CTMGrade from Product with(nolock) where SN='{0}' and InterID =(select max(InterID) from product with(nolock) where SN='{0}')";
                sqlCTM = string.Format(sqlCTM, sSerialNumber);
                SqlDataReader sdr = ToolsClass.GetDataReader(sqlCTM);
                if (sdr.Read())
                {
                    string sCTMGrade = "";
                    try
                    {
                        sCTMGrade = sdr.GetString(0);
                    }
                    catch
                    {
                        sCTMGrade = "";
                    }
                    if (sCTMGrade.ToUpper().Equals("NG"))
                    {
                        ToolsClass.Log("Module：" + sSerialNumber + " CTM assessment NG，Cannot Pack!!!", "ABNORMAL", lstView);
                        verify = false;
                    }
                    if (sCTMGrade.ToUpper().Equals("W") || sCTMGrade.Equals(""))
                    {
                        ToolsClass.Log("Module：" + sSerialNumber + " CTM assessment not done，Cannot pack!!!", "ABNORMAL", lstView);
                        verify = false;
                    }
                }
                sdr.Close();
                sdr = null;
                return verify;
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("Module(" + sSerialNumber + ")Error during CTM", "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 重打印标签
        /// </summary>
        /// <param name="codeArr"></param>
        /// <param name="BoxCode"></param>
        /// <param name="sModelPowerSet"></param>
        /// <param name="sModelTypeSet"></param>
        /// <returns></returns>
        public string RePrint(string[,] codeArr = null, string BoxCode = "", string[] sModelPowerSet = null, string[] sModelTypeSet = null)
        {
            string rt = "";
            if (this.sModulLabePath.Length == 0)
            {
                return "Setting Module Label File Path Before Print Label";
            }
            if (codeArr != null && this.sModulLabePath.Length > 0 && sModelPowerSet != null && this.rbLargeLabelPrint == false)
            {
                //2011-05-24|add by alex.dong|for schuco----------------------------------------------
                lst = new ArrayList();
                for (int i = 0; i < codeArr.GetLength(0); i++)
                {
                    this.sSN = codeArr[i, 0];
                    string sModelType = sModelPowerSet[i];
                    dictLabelInfo(sModelType);

                    rt = LablePrint.printingLable(this.sModulLabePath, labelValue, this.printNum);
                    if (rt.Length > 0)
                    {
                        return rt;
                    }
                }
            }

            if (BoxCode.Length > 8 && this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == false)
            {
                rt = LablePrint.printingLable(this.sCartonLabelPath, this.sCartonPara, BoxCode, this.printNum);
            }
            return rt;
        }

        /// <summary>
        /// 验证Cell Color是否一致.不一致，不能装箱
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyCellColorandGetBoxID(string sSerialNumber)
        {
            try
            {
                #region
                bool verifyFlag = true;
                sCellColor = "";
                sModuleSize = "";
                string sqlCellColor = "select CellColor,CellSpec,Pattern from Product with(nolock) where SN='{0}' and InterID = (select max(InterID) FROM Product with(nolock) Where SN ='{0}')";
                SqlDataReader rdColor = ToolsClass.GetDataReader(string.Format(sqlCellColor, sSerialNumber));
                if (rdColor.Read())
                {
                    sCellColor = rdColor.GetString(0);
                    sModuleSize = rdColor.GetString(1);
                    if (IsCheckPattern)
                    {
                        if (!rdColor.IsDBNull(2))
                        {
                            if (sPattern == "")
                            {
                                sPattern = rdColor.GetString(2);
                            }
                            else
                            {
                                if (!sPattern.Equals(rdColor.GetString(2).Trim()))
                                {
                                    ToolsClass.Log("Module" + sSerialNumber + "Pattern:" + rdColor.GetString(2) + " is diferent with Pattern:" + sPattern , "ABNORMAL", lstView);
                                    return false;
                                }
                            }
                        }
                    }
                    if (arrayCellColor.Count == 0)
                    {
                        sCellColorCode = "";
                        if (sCellColor.ToUpper().Equals("BLUE"))		
                            sCellColorCode = "B";
                        if (sCellColor.ToUpper().Equals("DARK"))		
                            sCellColorCode = "D";
                        if (sCellColor.ToUpper().Equals("LITE"))
                            sCellColorCode = "L";
                        arrayCellColor.Add(sCellColor.ToUpper());
                        if (ToolsClass.sSite == ToolsClass.CAMO)
                        {
                            tbBoxCode.Text = ToolsClass.getBoxID(this.firstcode, sCellColorCode);
                            ToolsClass.Log("Generate New Carton Number: " + tbBoxCode.Text, "NORMAL", lstView);
                        }
                    }
                }
                rdColor.Close(); rdColor = null;
                if (arrayCellColor.Count > 0)
                {
                    if (!arrayCellColor.Contains(sCellColor.ToUpper()))
                    {
                        ToolsClass.Log("Module:" + sSerialNumber + " Cell Color:" + sCellColor + " is different with Cell Color:" + arrayCellColor[0].ToString() + "Cannot pack different color", "ABNORMAL", lstView);
                        return false;
                    }
                }
                if (tbBoxCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log("Pallet number is used up，set up new naming rule for pallet number", "ABNORMAL", lstView);
                    return false;
                }
                return verifyFlag;
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("Module" + sSerialNumber + "error during color, type checking"+ex.Message+"","ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 验证组件是否符合打包规则
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyModuleCanBePackage(string sSerialNumber)
        {
            try
            {
                if (!isPackage(sSerialNumber, false))
                    return false;

                using (SqlDataReader reader = ToolsClass.GetDataReader(string.Format(tbHandPackingSQL.Text, sSerialNumber)))
                {
                    if (reader != null && reader.Read())
                    {
                        #region 组件处理状态
                        string boxCode = "";
                        if (reader.IsDBNull(4))
                            boxCode = "";
                        else
                            boxCode = reader.GetString(4).Trim();
                        if (boxCode != "T4")
                        {
                            ToolsClass.Log(sSerialNumber + " Process status（" + boxCode + "），Cannot pack", "ABNORMAL", lstView);
                            tbBarCode.Clear();
                            return false;
                        }
                        #endregion

                        #region 检查混装功率种数
                        string nPower = "0"; //标称功率
                        object pw = reader.GetDouble(3); //实测功率
                        nPower = ToolsClass.getIdealPower(reader.GetDouble(3), sSerialNumber.Trim());//标称功率（含档次）
                        _CartonPower = nPower.ToString();//标称功率                                           
                        if (nPower == "-1")
                        {
                            ToolsClass.Log("Module " + sSerialNumber + ": Power " + reader.GetDouble(3).ToString() + " not in the range of setted power range, cannot pack.", "ABNORMAL", lstView);
                            tbBarCode.Clear();
                            return false;
                        }
                        else
                        {
                            if (this.nominalPower.Count == 0)
                            {
                                string[] array = nPower.ToString().Split('_');
                                if (array.Length > 1)
                                {
                                    this.tbNominalPowerGrade.Text = array[1].TrimEnd();
                                    this.tbNominalPower.Text = "";
                                    this.tbNominalPower.Text = array[0].ToString();
                                    if (!reader.IsDBNull(7))   
                                        this.txtRework.Text = Convert.ToString(reader.GetString(7).Trim());
                                    else
                                        this.txtRework.Text = "";
                                }
                                else
                                {
                                    this.tbNominalPowerGrade.Text = "";
                                    this.tbNominalPower.Text = "";
                                    this.tbNominalPower.Text = nPower.ToString();
                                    if (!reader.IsDBNull(7))
                                        this.txtRework.Text = Convert.ToString(reader.GetString(7).Trim());
                                    else
                                        this.txtRework.Text = "";
                                }
                                ToolsClass.Log("Nominal power from the first panel packed in this box is:" + this.tbNominalPower.Text.ToString() + " Nominal power grade is：" + this.tbNominalPowerGrade.Text, "NORMAL", lstView);
                            }
                        }
                        if (!checkPower(nPower, sSerialNumber))
                            return false;

                        #endregion

                        #region 检查组件型号，颜色
                        if (dataGridView1.DisplayedRowCount(true) == 0)
                        {
                            celltype = reader.GetString(1);
                        }
                        else
                        {
                            if (reader.GetString(1) != celltype)
                            {
                                ToolsClass.Log("Cannot Pack different type modules!", "ABNORMAL", lstView);
                                tbBarCode.Clear();
                                return false;
                            }
                        }

                        if (!verifyCellColorandGetBoxID(sSerialNumber))
                            return false;
                        #endregion

                        #region 检查工作号
                        string workCode = "";
                        if (this.cbCheckSN)
                        {
                            if (!checkWorkCode(sSerialNumber, out workCode))
                            {
                                return false;
                            }
                        }
                        #endregion

                        #region CenterDB
                        try
                        {
                            #region
                            string sqlQuery = @"
                                        WITH PRD AS
                                        (		
	                                        SELECT [SN],[ModelType],[Process],[BoxID],InterNewTempTableID,[Art_No] 
	                                        FROM [Product]  with(nolock)
	                                        Where [SN] ='{0}' 
		                                        AND [InterID] = (SELECT MAX(InterID) FROM [Product]  with(nolock) WHERE [SN] ='{0}')
                                        )
                                        SELECT *
                                        FROM PRD 
	                                        INNER JOIN [ElecParaTest] EPT ON
		                                        PRD.[InterNewTempTableID]=EPT.[InterID]
                        ";
                            sqlQuery = string.Format(sqlQuery, sSerialNumber);
                            DataSet ds = DbHelperSQL.Query(FormCover.connectionBase, sqlQuery, null);
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow row = ds.Tables[0].Rows[0];
                                string module = row["SN"].ToString();
                                string workShop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");
                                string isExist = new MODULEDAL().CheckRepeat(module, workShop);
                                if (isExist == "1")
                                {
                                    DataSet set = new MODULEDAL().GetInfoRepSN(module);
                                    if (set != null && set.Tables[0].Rows.Count > 0)
                                    {
                                        MessageBox.Show("serial number" + module + " is already existing" + set.Tables[0].Rows[0][0].ToString() + " workshop, in pallet number" + set.Tables[0].Rows[0][1].ToString());
                                        ToolsClass.Log("serial number" + module + "is already existing" + set.Tables[0].Rows[0][0].ToString() + "workshop in pallet number" + set.Tables[0].Rows[0][1].ToString());
                                    }
                                    else
                                    {
                                        MessageBox.Show("serial number:" + module + " is already exist or duplicated with other workshop");
                                        ToolsClass.Log("serial number:" + module + " is already exist or duplicated with other workshop", "ABNORMAL", lstView);
                                    }

                                    return false;
                                }
                                if (isExist == "3")
                                {
                                    MessageBox.Show("serial number:" + module + " is already exist or duplicated with other workshop");
                                    ToolsClass.Log("serial number:" + module + " is already exist or duplicated with other workshop", "ABNORMAL", lstView);
                                    return false;
                                }
                                if (isExist == "0")
                                {
                                    string currenttime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"); ;
                                    MODULE mo = new MODULE();
                                    mo.SYSID = Guid.NewGuid().ToString();
                                    if (Work_Shop.ToUpper().Equals("M08"))
                                        mo.SYSID = mo.SYSID + "-08";
                                    mo.MODULE_SN = module;
                                    mo.BARCODE = module;
                                    mo.WORKSHOP = workShop;
                                    mo.WORK_ORDER = module.Substring(0, 9);
                                    mo.MODULE_TYPE = row["ModelType"].ToString();
                                    mo.CREATED_ON = currenttime; //可以理解为包装时间
                                    mo.CREATED_BY = FormCover.currUserName;
                                    mo.MODIFIED_BY = FormCover.currUserName;
                                    mo.MODIFIED_ON = currenttime;
                                    mo.RESV01 = row["Art_No"].ToString();

                                    string[] NewCartonPower = _CartonPower.Split('_');
                                    if (NewCartonPower.Length > 1)
                                    {
                                        mo.RESV02 = row["ModelType"].ToString().Insert(mo.MODULE_TYPE.IndexOf("-") + 1, NewCartonPower[0]);
                                        mo.RESV05 = NewCartonPower[0]; //标称功率
                                        mo.RESV04 = row["PMAX"].ToString();//实测功率
                                    }
                                    else
                                    {
                                        mo.RESV02 = row["ModelType"].ToString().Insert(mo.MODULE_TYPE.IndexOf("-") + 1, _CartonPower);
                                        mo.RESV05 = _CartonPower;//标称功率
                                        mo.RESV04 = row["PMAX"].ToString();//实测功率
                                    }

                                    mo.CustBoxID = this.tbCustBoxCode.Text.Trim();

                                    _ListModule.Add(mo);

                                    MODULE_TEST moTest = new MODULE_TEST();
                                    moTest.SYSID = Guid.NewGuid().ToString();
                                    if (Work_Shop.ToUpper().Equals("M08"))
                                        moTest.SYSID = moTest.SYSID + "-08";
                                    moTest.MODULE_SN = module;
                                    moTest.TEMP = row["Temp"].ToString();
                                    moTest.ISC = row["ISC"].ToString();
                                    moTest.VOC = row["VOC"].ToString();
                                    moTest.IMP = row["Im"].ToString();
                                    moTest.VMP = row["Vm"].ToString();
                                    moTest.FF = row["FF"].ToString();
                                    moTest.EFF = row["Eff"].ToString();
                                    moTest.PMAX = row["PMAX"].ToString();
                                    moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.CREATED_NO = currenttime;
                                    moTest.CREATED_BY = FormCover.currUserName;
                                    moTest.MODIFIED_ON = DateTime.Parse(row["CheckDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.MODIFIED_BY = FormCover.currUserName;
                                    _ListModuleTest.Add(moTest);
                                }
                                if (isExist == "2")
                                {
                                    MODULE_TEST moTest = new MODULE_TEST();
                                    moTest.SYSID = Guid.NewGuid().ToString();
                                    if (Work_Shop.ToUpper().Equals("M08"))
                                        moTest.SYSID = moTest.SYSID + "-08";
                                    moTest.MODULE_SN = module;
                                    moTest.TEMP = row["Temp"].ToString();
                                    moTest.ISC = row["ISC"].ToString();
                                    moTest.VOC = row["VOC"].ToString();
                                    moTest.IMP = row["Im"].ToString();
                                    moTest.VMP = row["Vm"].ToString();
                                    moTest.FF = row["FF"].ToString();
                                    moTest.EFF = row["Eff"].ToString();
                                    moTest.PMAX = row["PMAX"].ToString();
                                    moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.CREATED_BY = FormCover.currUserName;
                                    moTest.MODIFIED_ON = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.MODIFIED_BY = FormCover.currUserName;
                                    _ListModuleTest.Add(moTest);

                                    string sqlQuerySNId = string.Format("SELECT SYSID FROM T_MODULE where MODULE_SN='{0}'", module);
                                    object obj = DbHelperSQL.ExecuteScalar(sqlQuerySNId, null);
                                    MODULE mo = new MODULE();
                                    if (obj != null && obj != DBNull.Value)
                                    {
                                        mo.SYSID = obj.ToString();
                                        mo.REMARK = "UnPacking";
                                        _ListModule.Add(mo);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Cannot find module：" + sSerialNumber);
                                    }
                                }
                            }
                            #endregion
                        }
                        catch (Exception exc)
                        {
                            ToolsClass.Log(exc.Message, "ABNORMAL", lstView);
                        }
                        #endregion

                        #region 添加组件到list列表
                        string nGrade = "";
                        string[] nPowerArrays = nPower.Split('_');
                        if (nPowerArrays.Length > 1)
                        {
                            nPower = nPowerArrays[0];
                            nGrade = nPowerArrays[1].ToString().Trim();
                        }

                        dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { reader.GetValue(0), reader.GetValue(1), reader.GetValue(2), reader.GetValue(3), nPower, nGrade, reader.GetValue(4), workCode, reader.GetValue(6), reader.GetValue(7)});
                        ToolsClass.Log("Adding module:" + reader.GetValue(0).ToString() + " to packing list", "NORMAL", lstView);
                        #endregion
                    }
                    else
                    {
                        ToolsClass.Log("serial number:" + sSerialNumber + " not exist", "ABNORMAL", lstView);
                        tbBarCode.Clear();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ToolsClass.Log("Module:" + sSerialNumber + "error during packing rule checking"+ex.Message+"", "ABNORMAL", lstView);
                return false; 
            }
        }

        /// <summary>
        /// 打印单个组件的标签
        /// </summary>
        /// <param name="i"></param>
        private void printLabel(int i = 0)
        {
            string model = "";
            if (this.rbModel)
            {
                model = dataGridView1[1, i].Value.ToString();
                int location = model.IndexOf('-');
                if (location == -1)
                {
                    model = model + "-" + dataGridView1[4, i].Value.ToString();
                }
                else
                {
                    model = model.Insert(location + 1, dataGridView1[4, i].Value.ToString());
                }
            }
            else if (this.rbTrueP)
            {
                model = dataGridView1[3, i].Value.ToString();
            }
            else
            {
                model = dataGridView1[4, i].Value.ToString();
            }
            if (!ToolsClass.bChipType)
                model = model.Substring(0, model.Length - 1);
            this.sModel = model;

            this.sSN = dataGridView1[0, i].Value.ToString();

            string sModelType = "";
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                sModelType = dataGridView1[1, i].Value.ToString().Replace(" ", "-").Insert(dataGridView1[1, i].Value.ToString().Replace(" ", "-").IndexOf('-') + 1, dataGridView1[4, i].Value.ToString());
            else
                sModelType = dataGridView1[1, i].Value.ToString().Insert(dataGridView1[1, i].Value.ToString().IndexOf('-') + 1, dataGridView1[4, i].Value.ToString());//dataGridView1[1, i].Value.ToString().Substring(0, dataGridView1[1, i].Value.ToString().IndexOf('-') + 1) + dataGridView1[4, i].Value.ToString();

            dictLabelInfo(sModelType);
            string returnMsg = LablePrint.printingLable(this.sModulLabePath, labelValue, this.printNum);
            if (returnMsg.Length > 0)
            {
                ToolsClass.Log("Packaging data is saved, Module label print Failed:" + returnMsg, "ABNORMAL", lstView);
                return;
            }
        }

        /// <summary>
        /// 验证组件是否已经打包或箱号是否被使用
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <param name="IsBoxOrModule">true：Box，False：Module</param>
        /// <returns></returns>
        private bool isPackage(string sParameter, bool IsBoxOrModule = true)
        {
            try
            {
                #region
                string sql = "";
                if (IsBoxOrModule)
                    sql = "select BoxID from Product with(nolock) where BoxID='{0}'";
                else
                    sql = "select BoxID from Product with(nolock) where SN='{0}' and InterID = (select max(InterID) from Product with(nolock) where SN ='{0}')";

                string sBoxID = "";
                sql = string.Format(sql, sParameter);
                SqlDataReader sdr = ToolsClass.GetDataReader(sql);
                while (sdr != null && sdr.Read())
                {
                    if (IsBoxOrModule)
                    {
                        return false;
                    }
                    else
                    {
                        sBoxID = sdr.GetString(0);
                        if (!sBoxID.Trim().Equals(""))
                        {
                            ToolsClass.Log("Module：" + sParameter + " is already packed in:" + sBoxID, "ABNORMAL", lstView);
                            return false;
                        }
                    }
                }
                sdr.Close(); sdr = null;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        private void tbBoxCode_TextChanged(object sender, EventArgs e)
        {
            //if (tbBoxCode.Text.Trim().Length >= 10 && !sTempBoxID.Equals(tbBoxCode.Text.Trim()))
            //{
            //    int j = 0;
            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    //    j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //    //else
            //    j = int.Parse(ToolsClass.getConfig("SerialCode"));

            //    string strsql = "update Box set IsUsed='N' where BoxID='" + sTempBoxID + "'";
            //    int iSql = ToolsClass.ExecuteNonQuery(strsql);
            //    j--;

            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    //    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
            //    //else
            //    if (iSql > 0)
            //        ToolsClass.saveXMLNode("SerialCode", j.ToString());
            //}
        }

        #region 不在使用的事件
        private void cbMergeBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (cbMergeBox.Checked)
            //{
            //    lblWS.Enabled = true;
            //    lblWSFrom.Enabled = true;
            //    cmbWorkShop.Enabled = true;

            //    if(cmbWorkShop.Text.Length>0)
            //        FormCover.connectionBase = ToolsClass.getConfig(cmbWorkShop.Text, false, "", "config.xml");
            //}
            //else
            //{
            //    lblWS.Enabled = false;
            //    lblWSFrom.Enabled = false;
            //    cmbWorkShop.Text = "";
            //    cmbWorkShop.Enabled = false;
            //    FormCover.connectionBase = CurrentWorkShopDBconn;
            //}
        }

        private void cmbWorkShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbWorkShop.Text.Length > 0)
            //    FormCover.connectionBase = ToolsClass.getConfig(cmbWorkShop.Text, false, "", "config.xml");
        }
        #endregion
        private void cbPatternFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPatternFlag.Checked)
                IsCheckPattern = true;
            else
                IsCheckPattern = false;
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void chbCarton_CheckedChanged(object sender, EventArgs e)
        {
            this.tbBoxCode.Enabled = this.chbCarton.Checked;
        }

        private void chbCustCarton_CheckedChanged(object sender, EventArgs e)
        {
            this.tbCustBoxCode.Enabled = this.chbCustCarton.Checked;
        }

        private void tbCustBoxCode_TextChanged(object sender, EventArgs e)
        {
            //if (tbCustBoxCode.Text.Trim().Length >= 10 && !custBoxID.Equals(tbCustBoxCode.Text.Trim()))
            //{
            //    int j = 0;
            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //    //else
            //    //    j = int.Parse(ToolsClass.getConfig("SerialCode"));

            //    string strsql = "update Box set IsUsed='N' where BoxID='" + custBoxID + "'";
            //    int iSql = ToolsClass.ExecuteNonQuery(strsql);
            //    j--;

            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    if (iSql > 0)
            //        ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
            //    //else
            //    //    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            //}
        }
    }
}
