﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FormWOInfoQuery : Form
    {     
        #region 私有变量

        private static List<LanguageItemModel> LanMessList;//定义语言集
        private string WONumber = "";
        private string WoType = "";
        public string  Wo="";
        private string WOQueryType = "";
        private DateTime WoPlanMfgDateFrom;
        private DateTime WoPlanMfgDateTo;
        int StorageCount = 0;
        #endregion
         
        #region 私有方法
        #region
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }
        private void SetDataGridViewData(string WOQuerytype)
        {
            if (!WOQuerytype.Equals("Wo"))
            {
                WoPlanMfgDateFrom = Convert.ToDateTime(this.dtpWOPlanDateFrom.Value);
                WoPlanMfgDateTo = Convert.ToDateTime(this.dtpWOPlanDateTo.Value);
            }
            if (FormCover.CurrentFactory.ToString() == "CAMO")
                WoType = "";

            DataTable dt = ProductStorageDAL.GetWoMasterIno(WOQuerytype, this.txtWo.Text.Trim().ToString(), WoType, WoPlanMfgDateFrom, WoPlanMfgDateTo);

            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["WONO"].Value = Convert.ToString(row["TWO_NO"]);
                    this.dataGridView1.Rows[RowNo].Cells["Mitem"].Value = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.dataGridView1.Rows[RowNo].Cells["Order"].Value = Convert.ToString(row["TWO_ORDER_NO"]);
                    RowNo++;
                    RowIndex++;
                }
            }

            if (WOQuerytype.Equals("Wo"))
            {
                this.txtWo.SelectAll();
                this.txtWo.Focus();
            }
            else
            {
                this.dtpWOPlanDateFrom.Focus();
            }
        }
        #endregion
        #endregion

        #region 窗体事件
        public FormWOInfoQuery()
        {
            InitializeComponent();
        }
        public FormWOInfoQuery(string WO,string WOTYPE)
        {
            InitializeComponent();
            WONumber = WO;
            WoType = WOTYPE;
        }
        private void FormCartonInfoQuery_Load(object sender, EventArgs e)
        {
           
            this.Text = "orderno";

            //查询类型
            this.ddlQueryType.Items.Insert(0, "工单");
            this.ddlQueryType.Items.Insert(1, "计划生产日期");
            
            this.ddlQueryType.SelectedIndex = 0;
            WOQueryType = "Wo";
            this.PalWOPlanDate.Visible = false;
            this.PalWO.Visible = true;
            this.txtWo.Text = WONumber;
            this.txtWo.SelectAll();
            this.txtWo.Focus();

            if (!WONumber.Equals(""))
                SetDataGridViewData(WOQueryType);

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);
            # endregion

            
        }
        #endregion

        #region 窗体页面事件
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData(WOQueryType);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                Wo = Convert.ToString(this.dataGridView1.CurrentRow.Cells["WONO"].Value);
            else
            {
                MessageBox.Show("没有选中工单","信息提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }
            this.Close();
           
        }
        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalWOPlanDate.Visible = true;
                this.PalWO.Visible = false;
                this.dtpWOPlanDateFrom.Focus();
                WOQueryType = "WoPlanMfgDate";
            }
            else if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalWOPlanDate.Visible = false;
                this.PalWO.Visible = true;
                this.txtWo.Clear();
                this.txtWo.Focus();
                WOQueryType = "Wo";  
            }
            else if (this.ddlQueryType.SelectedIndex == 2) //返工工单
            {
                this.PalWOPlanDate.Visible = false;
                this.PalWO.Visible = true;
                this.txtWo.Clear();
                this.txtWo.Focus();
                WOQueryType = "Wo";
            }
            ClearDataGridViewData();
        }
        #endregion

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Wo = this.dataGridView1.CurrentRow.Cells["WONO"].Value.ToString();
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Wo = this.dataGridView1.Rows[e.RowIndex].Cells["WONO"].Value.ToString();
            this.Close();
        }
    }
}
