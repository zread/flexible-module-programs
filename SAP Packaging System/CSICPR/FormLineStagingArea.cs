﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FormLineStagingArea : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集

        System.Drawing.Graphics gph1;
        System.Drawing.Graphics gph2;


        SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB; User ID = reader; Password=12345");
        SqlConnection con = new SqlConnection("Data Source = ca01s0015; Initial Catalog = csimes_SapInterface; User ID = reader; Password=12345");
        public FormLineStagingArea(string pa)
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 4;
            label2.Text = pa;
            gph1 = pictureBox1.CreateGraphics();
            gph2 = pictureBox2.CreateGraphics();
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB;User ID = reader; Password=12345");
                string sql = "select two_no from (  SELECT two_no, ROW_NUMBER() over(order by TWO_CREATE_ON desc) rn FROM[csimes_SapInterface].[dbo].[T_WORK_ORDERS] where two_no like  'C" + label2.Text + "%' and LEN(TWO_NO) = 9) tb1 where rn = 1";

                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    textBox2.Text = sdr["two_no"].ToString();

                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB; User ID = reader; Password=12345");

                string sql1 = "select two_no from (  SELECT two_no, ROW_NUMBER() over(order by TWO_CREATE_ON desc) rn FROM[csimes_SapInterface].[dbo].[T_WORK_ORDERS] where two_no like  'C" + label2.Text + "%' and LEN(TWO_NO) = 9) tb1 where rn = 2";
                conn.Open();
                SqlCommand sc1 = new SqlCommand(sql1, conn);
                SqlDataReader sdr1 = sc1.ExecuteReader();

                while (sdr1.Read())
                {
                    textBox3.Text = sdr1["two_no"].ToString();

                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }


        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }


        private void FormLineStagingArea_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        CheckBox lastChecked;
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox activeCheckBox = sender as CheckBox;
            if (activeCheckBox != lastChecked && lastChecked != null) lastChecked.Checked = false;
            lastChecked = activeCheckBox.Checked ? activeCheckBox : null;


            if (checkBox2.Checked == true)
            {


                Pen blackpen = new Pen(Color.Yellow, 3);
                gph2.DrawLine(blackpen, 0, 60, 49, 0);
                textBox4.Visible = true;
                label6.Visible = true;
                button1.Visible = true;
                button2.Visible = true;
                Skid.Visible = true;
                skidnum.Visible = true;
                Mosize.Visible = true;
                Recieved.Visible = true;
                textBox4.Focus();



                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = csimes_SapInterface; User ID = reader; Password=12345");
                    string Mo = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where MaterialGroup = 2000 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + textBox3.Text + "') and OrderNo != '" + textBox3.Text + "'  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    while (sdr.Read())
                    {
                        Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " pcs";
                    }
                    conn.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }


                try
                {
                    SqlConnection conn2 = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo2 = "SELECT isnull(Sum(Qty),0) + isnull(sum(StrinQty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox3.Text + "' group by ProductionOrder";

                    conn2.Open();
                    SqlCommand sc2 = new SqlCommand(Mo2, conn2);
                    SqlDataReader sdr2 = sc2.ExecuteReader();

                    if(sdr2.Read())
                    {
                        Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        Recieved.Text = "Received From Warehouse: 0 pcs ";
                    }
                    conn2.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }

             



            }
            if (checkBox2.Checked == false)
            {

                pictureBox2.Image = null;
                if (checkBox1.Checked == false)
                {
                    textBox4.Visible = false;
                    label5.Visible = false;
                    button1.Visible = false;
                    button2.Visible = false;
                    label6.Visible = false;
                    textBox1.Visible = false;
                    button3.Visible = false;
                    button4.Visible = false;
                    textBox4.Clear();
                    textBox4.ReadOnly = false;
                    dataGridView1.Visible = false;
                    button5.Visible = false;
                    CellQty.Visible = false;
                    label7.Visible = false;
                    comboBox1.Visible = false;
                    label8.Visible = false;
                    Skid.Visible = false;
                    skidnum.Visible = false;
                    skidnum.ReadOnly = false;
                    Mosize.Visible = false;
                    Recieved.Visible = false;
                    ProdWarehouse.Visible = false;
                    skidnum.Clear();
                    S_U.Visible = false;
                }

            }



        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox activeCheckBox = sender as CheckBox;
            if (activeCheckBox != lastChecked && lastChecked != null) lastChecked.Checked = false;
            lastChecked = activeCheckBox.Checked ? activeCheckBox : null;

            if (checkBox1.Checked == true)
            {

                Pen blackpen = new Pen(Color.Red, 3);
                gph1.DrawLine(blackpen, 0, 9, 49, 60);
                textBox4.Visible = true;
                label6.Visible = true;
                button1.Visible = true;
                button2.Visible = true;
                Skid.Visible = true;
                skidnum.Visible = true;
                Mosize.Visible = true;
                Recieved.Visible = true;
                textBox4.Focus();
                
                
               
                
                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = csimes_SapInterface; User ID = reader; Password=12345");
                    string Mo = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where MaterialGroup = 2000 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '"+textBox2.Text+ "') and OrderNo != '" + textBox2.Text + "'  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    while (sdr.Read())
                    {
                        Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " pcs";
                    }
                    conn.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }

                try
                {
                    SqlConnection conn2 = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo2 = "SELECT isnull(Sum(Qty),0) +isnull(sum(StrinQty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox2.Text + "' group by ProductionOrder";

                    conn2.Open();
                    SqlCommand sc2 = new SqlCommand(Mo2, conn2);
                    SqlDataReader sdr2 = sc2.ExecuteReader();

                   if (sdr2.Read())
                    {
                        Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        Recieved.Text = "Received From Warehouse: 0 pcs ";
                    }
                    conn2.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }

             


            }
            if (checkBox1.Checked == false)
            {
                pictureBox1.Image = null;
                if (checkBox2.Checked == false)
                {
                    textBox4.Visible = false;
                    label5.Visible = false;
                    button1.Visible = false;
                    button2.Visible = false;
                    label6.Visible = false;
                    textBox1.Visible = false;
                    button3.Visible = false;
                    button4.Visible = false;
                    textBox4.Clear();
                    textBox4.ReadOnly = false;
                    dataGridView1.Visible = false;
                    button5.Visible = false;
                    CellQty.Visible = false;
                    label7.Visible = false;
                    comboBox1.Visible = false;
                    label8.Visible = false;
                    Skid.Visible = false;
                    skidnum.Visible = false;
                    skidnum.ReadOnly = false;
                    Mosize.Visible = false;
                    Recieved.Visible = false;
                    ProdWarehouse.Visible = false;
                    skidnum.Clear();
                    S_U.Visible = false;
                }
            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            textBox1.Clear();
            skidnum.Clear();
            if (string.IsNullOrEmpty(textBox4.Text.ToString()))
            {
                MessageBox.Show("Please Input the Batch NO.");

                return;
            }
            if (checkBox1.Checked)
            {
                


                string sql = "select * from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where Batch='" + textBox4.Text + "' and MaterialGroup=2000 and OrderNo in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05='" + textBox2.Text + "')";
//
//                string sql = @"select batch from csimes_SapInterface.dbo.T_PICKING where MaterialCode in ( select MaterialCode from [csimes_SapInterface].[dbo].TSAP_WORKORDER_BOM where  MaterialGroup=2000 and Orderno in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05= '" + textBox2.Text + "'))"
//						+"AND OrderNo  in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05= '" + textBox2.Text + "') and batch = '" + textBox4.Text + "'";
//                
                DataTable dt = RetrieveMO(sql);
               
                if (dt!= null && dt.Rows.Count >= 1)
                {
                    textBox1.Visible = true;
                    label5.Visible = true;
                    button3.Visible = true;
                    textBox1.Multiline = true;
                    dataGridView1.Visible = false;
                    button4.Visible = false;
                    button5.Visible = false;
                    textBox4.ReadOnly = true;
                    textBox1.ReadOnly = false;
                    comboBox1.Visible = true;
                    label8.Visible = true;
                    Skid.Visible = true;
                    skidnum.Visible = true;
                    skidnum.Focus();
                    skidnum.ReadOnly = false;
                    
                }
                else
                {
                    MessageBox.Show("This Batch Information Does Not Match with MO Requirement");
                    textBox4.Clear();
                    return;
                }
            }
            if (checkBox2.Checked)
            {
//
//                string sql = @"select batch from csimes_SapInterface.dbo.T_PICKING where MaterialCode in ( select MaterialCode from [csimes_SapInterface].[dbo].TSAP_WORKORDER_BOM where  MaterialGroup=2000 and Orderno in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05= '" + textBox3.Text + "'))"
//						+"AND OrderNo  in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05= '" + textBox3.Text + "') and batch = '" + textBox4.Text + "'";
//                
				 string sql = "select * from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where Batch='" + textBox4.Text + "' and MaterialGroup=2000 and OrderNo in (select orderno from [csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05='" + textBox3.Text + "')";
				DataTable dt = RetrieveMO(sql);
            
                if (dt.Rows.Count >= 1)
                {
                    textBox1.Visible = true;
                    label5.Visible = true;
                    button3.Visible = true;
                    textBox1.Multiline = true;
                    dataGridView1.Visible = false;
                    button4.Visible = false;
                    button5.Visible = false;
                    textBox4.ReadOnly = true;
                    textBox1.ReadOnly = false;
                    comboBox1.Visible = true;
                    label8.Visible = true;
                    Skid.Visible = true;
                    skidnum.Visible = true;
                    skidnum.Focus();
                    skidnum.ReadOnly = false;
                }

                else
                {
                    MessageBox.Show("This Batch Information Does Not Match with MO Requirement");
                    textBox4.Clear();
                    return;
                }
                
            }
            button1.Enabled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void skidnum_Keydown(object sender, KeyEventArgs e)
        {
             if (e.KeyCode == Keys.Enter)
                 textBox1.Focus(); 
        }

        private void textBox4_Keydown(object sender, KeyEventArgs e)
        {
            /** if (e.KeyCode == Keys.Enter)
                 button1.PerformClick(); **/
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            string text = textBox1.Text.ToString();
            string[] T = text.Split('\n');
            for (int i = 0; i < T.Length; i++)
            {
                T[i] = T[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                string sql = "insert into [CAPA01DB01].[dbo].[CellCarton] values('{0}','{1}','{2}','{3}',getdate(),null,null,null,null,null,'{4}')";
                if (checkBox1.Checked && T[i] != "")
                {

                    sql = string.Format(sql, textBox2.Text.ToString(), textBox4.Text.ToString(), T[i], Convert.ToInt32(comboBox1.Text),skidnum.Text.ToString());
                    Executenonequery(sql);
                }
                if (checkBox2.Checked && T[i] != "")
                {
                    sql = string.Format(sql, textBox3.Text.ToString(), textBox4.Text.ToString(), T[i], Convert.ToInt32(comboBox1.Text), skidnum.Text.ToString());
                    Executenonequery(sql);
                }
            }
            if (checkBox1.Checked)
            {
                string sqlselect = "select * from (select ROW_NUMBER()over(partition by CartonNo order by ID) rn from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "'and ProductionOrder = '" + textBox2.Text + "' and SkidNo = '" + skidnum.Text + "') tb1 where rn=2";
                DataTable dt = RetrieveData(sqlselect);
                if (dt.Rows.Count >= 1)
                {
                    MessageBox.Show("Some Carton No. is Duplicate, Please Let Your Team leader Modify it.");
                    textBox1.Clear();
                    skidnum.ReadOnly = true;
                }
                else
                {
                    MessageBox.Show("New Carton No. are Stocked In.");
                    textBox1.Clear();
                    skidnum.ReadOnly = true;
                }
            }
            if (checkBox2.Checked)
            {
                string sqlselect = "select * from (select ROW_NUMBER()over(partition by CartonNo order by ID) rn from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "'and ProductionOrder = '" + textBox3.Text + "' and SkidNo = '" + skidnum.Text + "') tb1 where rn=2";
                DataTable dt = RetrieveData(sqlselect);
                if (dt.Rows.Count >= 1)
                {
                    MessageBox.Show("Some Carton No. is Duplicate, Please Let Your Team leader Modify it.");
                    textBox1.Clear();
                    skidnum.ReadOnly = true;
                }
                else
                {
                    MessageBox.Show("New Carton No. are Stocked In.");
                    textBox1.Clear();
                    skidnum.ReadOnly = true;
                }
            }
            button3.Enabled = true;

        }
        private void Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                Mycommand.ExecuteNonQuery();
                conn.Close();



            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        private DataTable RetrieveMO (string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                
                SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = csimes_SapInterface; User ID = reader; Password=12345");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            
            textBox1.Clear();
            CellQty.Clear();
       
            if (checkBox1.Checked)
            {
                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo = "SELECT isnull(Sum(Qty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox2.Text + "' group by ProductionOrder";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    if (sdr.Read())
                    {
                        ProdWarehouse.Text = "Production Warehouse Stock: " + sdr["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        ProdWarehouse.Text = "Production Warehouse Stock: 0 pcs ";
                    }
                    conn.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }
                string sql = "select ID, SkidNo,CartonNo,Qty,CreatedDate,ProductionOrder from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "' and ProductionOrder = '" + textBox2.Text + "' and SkidNo = '" + skidnum.Text + "'";

                DataTable dt = RetrieveData(sql);
                dataGridView1.DataSource = dt;


                if (dt.Rows.Count > 0)
                {
                    textBox1.Visible = true;
                    label5.Visible = true;
                    button3.Visible = false;
                    textBox4.ReadOnly = true;
                    skidnum.ReadOnly = true;
                    ProdWarehouse.Visible = true;
                    S_U.Visible = true;
                    dataGridView1.Visible = true;

                    if (FormCover.HasPowerControl("FormLineModify"))
                    {
                        button4.Visible = true;
                        button5.Visible = true;
                        label7.Visible = true;
                        CellQty.Visible = true;

                    }
                    //Define permission here

                    /* {
                         button4.Visible = true;
                         button5.Visible = true;

                     }*/
                    HighlightDuplicate(dataGridView1);
                }
                if (string.IsNullOrEmpty(skidnum.Text.ToString()))
                {
                    MessageBox.Show("Please Input Storage Unit.");
                }

                if (dt.Rows.Count == 0 && !string.IsNullOrEmpty(skidnum.Text.ToString()))
                {
                    MessageBox.Show("No Carton was Stocked In.");

                    textBox4.ReadOnly = false;
                    S_U.Visible = false;
                    textBox4.Clear();
                    skidnum.ReadOnly = false;
                    skidnum.Clear();
                    ProdWarehouse.Visible = false;
                }
                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo = "SELECT isnull(Sum(Qty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox2.Text + "' and SkidNo = '" + skidnum.Text + "' group by SkidNo";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    if (sdr.Read())
                    {
                        S_U.Text = "Current Skid Total Quantity: " + sdr["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        S_U.Text = "Current Skid Total Quantity: 0 pcs ";
                    }
                    conn.Close();
                }

                catch (Exception e3)
                {
                    throw e3;
                }
            }
            if (checkBox2.Checked)
            {
                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo = "SELECT isnull(Sum(Qty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox3.Text + "' group by ProductionOrder";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    if (sdr.Read())
                    {
                        ProdWarehouse.Text = "Production Warehouse Stock: " + sdr["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        ProdWarehouse.Text = "Production Warehouse Stock: 0 pcs ";
                    }
                    conn.Close();
                }

                catch (Exception e2)
                {
                    throw e2;
                }
                string sql = "select ID, SkidNo,CartonNo,Qty,CreatedDate,ProductionOrder from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "' and ProductionOrder = '" + textBox3.Text + "' and SkidNo = '" + skidnum.Text + "'";

                DataTable dt = RetrieveData(sql);
                dataGridView1.DataSource = dt;


                if (dt.Rows.Count > 0)
                {
                    textBox1.Visible = true;
                    label5.Visible = true;
                    button3.Visible = false;
                    textBox4.ReadOnly = true;
                    skidnum.ReadOnly = true;
                    ProdWarehouse.Visible = true;
                    S_U.Visible = true;
                    dataGridView1.Visible = true;

                    if (FormCover.HasPowerControl("FormLineModify"))
                    {
                        button4.Visible = true;
                        button5.Visible = true;
                        label7.Visible = true;
                        CellQty.Visible = true;

                    }
                    //Define permission here

                    /* {
                         button4.Visible = true;
                         button5.Visible = true;

                     }*/
                    HighlightDuplicate(dataGridView1);
                }
                if (string.IsNullOrEmpty(skidnum.Text.ToString()))
                {
                    MessageBox.Show("Please Input Storage Unit.");
                }

                if (dt.Rows.Count == 0 && !string.IsNullOrEmpty(skidnum.Text.ToString()))
                {
                    MessageBox.Show("No Carton was Stocked In.");

                    textBox4.ReadOnly = false;
                    S_U.Visible = false;
                    textBox4.Clear();
                    skidnum.ReadOnly = false;
                    skidnum.Clear();
                    ProdWarehouse.Visible = false;
                }
                try
                {
                    SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                    string Mo = "SELECT isnull(Sum(Qty), 0) as Qty FROM[CAPA01DB01].[dbo].[CellCarton] where ProductionOrder = '" + textBox3.Text + "' and SkidNo = '" + skidnum.Text + "' group by SkidNo";

                    conn.Open();
                    SqlCommand sc = new SqlCommand(Mo, conn);
                    SqlDataReader sdr = sc.ExecuteReader();

                    if (sdr.Read())
                    {
                        S_U.Text = "Current Skid Total Quantity: " + sdr["Qty"].ToString() + " pcs";
                    }
                    else
                    {
                        S_U.Text = "Current Skid Total Quantity: 0 pcs ";
                    }
                    conn.Close();
                }

                catch (Exception e3)
                {
                    throw e3;
                }
            }






            /***** DataTable dt = new DataTable();
             try
             {
                 SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                 string sql = "select CartonNo from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "'";
                 conn.Open();
                 SqlCommand sc = new SqlCommand(sql, conn);
                 SqlDataAdapter sda = new SqlDataAdapter(sc);
                 sda.Fill(dt);
                 SqlDataReader sdr = sc.ExecuteReader();

                 int linenum = dt.Rows.Count;
                 for(int i=0; i<linenum; i++)
                 {

                     textBox1.Text += dt.Rows[i]["CartonNo"].ToString()+"\r\n";
                 }

                 conn.Close();
             }

             catch (Exception e1)
             {
                 throw e1;
             }
             if (dt.Rows.Count > 0)
             {
                 textBox1.Visible = true;
                 label5.Visible = true;
                 button3.Visible = false;
                 textBox4.ReadOnly = true;
                 textBox1.ReadOnly = true;




             }


             else
             {
                 MessageBox.Show("This Batch Information Does Not Match with MO Requirement");
                 textBox4.Clear();
                 return;
             }   ******/

            button2.Enabled = true;

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0 && FormCover.HasPowerControl("FormLineModify"))
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                textBox1.Multiline = false;
                textBox1.ReadOnly = false;
                textBox1.Text = Convert.ToString(selectedRow.Cells["CartonNo"].Value);
                CellQty.Text = Convert.ToString(selectedRow.Cells["Qty"].Value)+"";
                comboBox1.Visible = false;
                label8.Visible = false;
                Skid.Visible = false;
                skidnum.Visible = false;

            }
        }
        private void HighlightDuplicate(DataGridView grv)
        {
            
            //use the currentRow to compare against
            for (int currentRow = 0; currentRow < grv.Rows.Count - 1; currentRow++)
            {
                DataGridViewRow rowToCompare = grv.Rows[currentRow];
                //specify otherRow as currentRow + 1
                for (int otherRow = currentRow + 1; otherRow < grv.Rows.Count; otherRow++)
                {
                    DataGridViewRow row = grv.Rows[otherRow];

                    bool duplicateRow = true;
                    //compare cell ENVA_APP_ID between the two rows
                    if (!rowToCompare.Cells["CartonNo"].Value.Equals(row.Cells["CartonNo"].Value))
                    {
                        duplicateRow = false;                    
                    }
                    if (rowToCompare.Cells["CartonNo"].Value.Equals(row.Cells["CartonNo"].Value)&& !rowToCompare.Cells["ProductionOrder"].Value.Equals(row.Cells["ProductionOrder"].Value))
                    {
                        duplicateRow = false;
                    }

                    //highlight both the currentRow and otherRow if ENVA_APP_ID matches 
                    if (duplicateRow)
                    {
                        rowToCompare.DefaultCellStyle.BackColor = Color.Red;
                        rowToCompare.DefaultCellStyle.ForeColor = Color.Black;
                        row.DefaultCellStyle.BackColor = Color.Red;
                        row.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }
            }
        }


        private void FormLineStagingArea_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Enabled = false;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
            string sql = "Update [CAPA01DB01].[dbo].[CellCarton] set CartonNo = '" + textBox1.Text + "',Qty = " +Convert.ToInt32(CellQty.Text)+ ",ModifyDate = GETDATE() ,ModifyBy = '"+FormCover.currUserName.ToString()+"' where CartonNo = '" + selectedRow.Cells["CartonNo"].Value + "' and ID = '" + selectedRow.Cells["ID"].Value + "' and BatchNo = '" + textBox4.Text + "'";
            Executenonequery(sql);
            MessageBox.Show(selectedRow.Cells["CartonNo"].Value.ToString() +"("+selectedRow.Cells["Qty"].Value.ToString()
                +" pcs)"+
                "Carton is Modified to " + textBox1.Text+"("+CellQty.Text+" pcs)");
            button4.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.Enabled = false;
            String Message = "Do you want to delete this information?";
            String Caption = "Delete";
            DialogResult result = MessageBox.Show(Message, Caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];

                string sql = "Delete from  [CAPA01DB01].[dbo].[CellCarton]  where CartonNo = '" + selectedRow.Cells["CartonNo"].Value + "' and ID = '" + selectedRow.Cells["ID"].Value + "' and BatchNo = '" + textBox4.Text + "'";
                Executenonequery(sql);
                string sql1 = "select ID,SkidNo, CartonNo,Qty,CreatedDate,ProductionOrder from [CAPA01DB01].[dbo].[CellCarton] where BatchNo = '" + textBox4.Text + "'";

                DataTable dt = RetrieveData(sql1);
                dataGridView1.DataSource = dt;
                textBox1.Clear();
            }
            button5.Enabled = true;
        }

        private void CellQty_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
       
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void Mosize_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
