﻿namespace CSICPR
{
    partial class FormCTM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBarCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCTMList = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.lblNo = new System.Windows.Forms.Label();
            this.lblQueryCnt = new System.Windows.Forms.Label();
            this.btOutpu = new System.Windows.Forms.Button();
            this.lbltmp = new System.Windows.Forms.Label();
            this.txtLine = new System.Windows.Forms.TextBox();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbQueryTerm = new System.Windows.Forms.ComboBox();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lstViewCTM = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbBarCode
            // 
            this.tbBarCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbBarCode.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbBarCode.Location = new System.Drawing.Point(161, 3);
            this.tbBarCode.MaxLength = 5000;
            this.tbBarCode.Multiline = true;
            this.tbBarCode.Name = "tbBarCode";
            this.tbBarCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbBarCode.Size = new System.Drawing.Size(173, 147);
            this.tbBarCode.TabIndex = 4;
            this.tbBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBarCode_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(50, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 5;
            this.label6.Text = "组件号码录入：";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(432, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 27;
            this.label1.Text = "CTM判等：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(159, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 12);
            this.label2.TabIndex = 28;
            this.label2.Text = "组件号码录入框，可以批次复制录入";
            // 
            // cbCTMList
            // 
            this.cbCTMList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCTMList.FormattingEnabled = true;
            this.cbCTMList.Location = new System.Drawing.Point(497, 60);
            this.cbCTMList.Name = "cbCTMList";
            this.cbCTMList.Size = new System.Drawing.Size(80, 20);
            this.cbCTMList.TabIndex = 31;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cbCTMList);
            this.panel1.Controls.Add(this.tbBarCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(797, 172);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.lstViewCTM);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 172);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(797, 270);
            this.panel2.TabIndex = 33;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtNo);
            this.panel3.Controls.Add(this.lblNo);
            this.panel3.Controls.Add(this.lblQueryCnt);
            this.panel3.Controls.Add(this.btOutpu);
            this.panel3.Controls.Add(this.lbltmp);
            this.panel3.Controls.Add(this.txtLine);
            this.panel3.Controls.Add(this.dateTo);
            this.panel3.Controls.Add(this.dateFrom);
            this.panel3.Controls.Add(this.lblTo);
            this.panel3.Controls.Add(this.lblFrom);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.cbQueryTerm);
            this.panel3.Controls.Add(this.btnQuery);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(797, 46);
            this.panel3.TabIndex = 1;
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(246, 16);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(100, 21);
            this.txtNo.TabIndex = 41;
            // 
            // lblNo
            // 
            this.lblNo.AutoSize = true;
            this.lblNo.Location = new System.Drawing.Point(181, 21);
            this.lblNo.Name = "lblNo";
            this.lblNo.Size = new System.Drawing.Size(65, 12);
            this.lblNo.TabIndex = 40;
            this.lblNo.Text = "组件号码：";
            // 
            // lblQueryCnt
            // 
            this.lblQueryCnt.AutoSize = true;
            this.lblQueryCnt.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblQueryCnt.Location = new System.Drawing.Point(558, 21);
            this.lblQueryCnt.Name = "lblQueryCnt";
            this.lblQueryCnt.Size = new System.Drawing.Size(41, 12);
            this.lblQueryCnt.TabIndex = 39;
            this.lblQueryCnt.Text = "label5";
            // 
            // btOutpu
            // 
            this.btOutpu.Location = new System.Drawing.Point(723, 16);
            this.btOutpu.Name = "btOutpu";
            this.btOutpu.Size = new System.Drawing.Size(69, 23);
            this.btOutpu.TabIndex = 38;
            this.btOutpu.Text = "导出Excel";
            this.btOutpu.UseVisualStyleBackColor = true;
            this.btOutpu.Click += new System.EventHandler(this.btOutpu_Click);
            // 
            // lbltmp
            // 
            this.lbltmp.AutoSize = true;
            this.lbltmp.Location = new System.Drawing.Point(193, 21);
            this.lbltmp.Name = "lbltmp";
            this.lbltmp.Size = new System.Drawing.Size(53, 12);
            this.lbltmp.TabIndex = 32;
            this.lbltmp.Text = "测试员：";
            // 
            // txtLine
            // 
            this.txtLine.Location = new System.Drawing.Point(246, 16);
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(100, 21);
            this.txtLine.TabIndex = 33;
            // 
            // dateTo
            // 
            this.dateTo.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTo.Location = new System.Drawing.Point(420, 16);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(136, 21);
            this.dateTo.TabIndex = 35;
            this.dateTo.Value = new System.DateTime(2011, 8, 7, 0, 0, 0, 0);
            // 
            // dateFrom
            // 
            this.dateFrom.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateFrom.Location = new System.Drawing.Point(246, 16);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(136, 21);
            this.dateFrom.TabIndex = 34;
            this.dateFrom.Value = new System.DateTime(2011, 8, 7, 0, 0, 0, 0);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(387, 21);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(41, 12);
            this.lblTo.TabIndex = 37;
            this.lblTo.Text = "截止：";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(180, 21);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(65, 12);
            this.lblFrom.TabIndex = 36;
            this.lblFrom.Text = "开始时间：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(10, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 33;
            this.label4.Text = "查询条件：";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbQueryTerm
            // 
            this.cbQueryTerm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbQueryTerm.FormattingEnabled = true;
            this.cbQueryTerm.Items.AddRange(new object[] {
            "测试日期",
            "测试人员",
            "组件号码"});
            this.cbQueryTerm.Location = new System.Drawing.Point(87, 16);
            this.cbQueryTerm.Name = "cbQueryTerm";
            this.cbQueryTerm.Size = new System.Drawing.Size(80, 20);
            this.cbQueryTerm.TabIndex = 32;
            this.cbQueryTerm.SelectedIndexChanged += new System.EventHandler(this.cbQueryTerm_SelectedIndexChanged);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(672, 16);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(45, 23);
            this.btnQuery.TabIndex = 1;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(779, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "---------------------------------------------------------------------------------" +
                "------------------------------------------------";
            // 
            // lstViewCTM
            // 
            this.lstViewCTM.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lstViewCTM.Location = new System.Drawing.Point(0, 45);
            this.lstViewCTM.Name = "lstViewCTM";
            this.lstViewCTM.Size = new System.Drawing.Size(797, 225);
            this.lstViewCTM.TabIndex = 0;
            this.lstViewCTM.UseCompatibleStateImageBehavior = false;
            // 
            // FormCTM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 442);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "FormCTM";
            this.Text = "CTM判等";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCTM_FormClosing);
            this.Load += new System.EventHandler(this.FormCTM_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbBarCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCTMList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lstViewCTM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbQueryTerm;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.TextBox txtLine;
        private System.Windows.Forms.Label lbltmp;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Button btOutpu;
        private System.Windows.Forms.Label lblQueryCnt;
        private System.Windows.Forms.Label lblNo;
        private System.Windows.Forms.TextBox txtNo;

    }
}