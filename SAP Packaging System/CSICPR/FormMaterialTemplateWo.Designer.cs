﻿namespace CSICPR
{
    partial class FormMaterialTemplateWo    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.ddlQueryType = new System.Windows.Forms.ComboBox();
        	this.lblCartonQuery = new System.Windows.Forms.Label();
        	this.PalWOPlanDate = new System.Windows.Forms.Panel();
        	this.lblPackingFrom = new System.Windows.Forms.Label();
        	this.dtpWOPlanDateFrom = new System.Windows.Forms.DateTimePicker();
        	this.lblPackingTo = new System.Windows.Forms.Label();
        	this.dtpWOPlanDateTo = new System.Windows.Forms.DateTimePicker();
        	this.PalWO = new System.Windows.Forms.Panel();
        	this.txtWo = new System.Windows.Forms.TextBox();
        	this.label2 = new System.Windows.Forms.Label();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.btnSave = new System.Windows.Forms.Button();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.WONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Mitem = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.PalWOPlanDate.SuspendLayout();
        	this.PalWO.SuspendLayout();
        	this.panel1.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(285, 43);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(65, 24);
        	this.btnQuery.TabIndex = 168;
        	this.btnQuery.Tag = "button1";
        	this.btnQuery.Text = "查询";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// ddlQueryType
        	// 
        	this.ddlQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlQueryType.FormattingEnabled = true;
        	this.ddlQueryType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.ddlQueryType.Location = new System.Drawing.Point(93, 8);
        	this.ddlQueryType.Name = "ddlQueryType";
        	this.ddlQueryType.Size = new System.Drawing.Size(148, 21);
        	this.ddlQueryType.TabIndex = 169;
        	this.ddlQueryType.SelectedIndexChanged += new System.EventHandler(this.ddlQueryType_SelectedIndexChanged);
        	// 
        	// lblCartonQuery
        	// 
        	this.lblCartonQuery.AutoSize = true;
        	this.lblCartonQuery.Location = new System.Drawing.Point(16, 11);
        	this.lblCartonQuery.Name = "lblCartonQuery";
        	this.lblCartonQuery.Size = new System.Drawing.Size(64, 13);
        	this.lblCartonQuery.TabIndex = 170;
        	this.lblCartonQuery.Text = "查 询 类 型";
        	// 
        	// PalWOPlanDate
        	// 
        	this.PalWOPlanDate.Controls.Add(this.lblPackingFrom);
        	this.PalWOPlanDate.Controls.Add(this.dtpWOPlanDateFrom);
        	this.PalWOPlanDate.Controls.Add(this.lblPackingTo);
        	this.PalWOPlanDate.Controls.Add(this.dtpWOPlanDateTo);
        	this.PalWOPlanDate.Location = new System.Drawing.Point(11, 39);
        	this.PalWOPlanDate.Name = "PalWOPlanDate";
        	this.PalWOPlanDate.Size = new System.Drawing.Size(261, 39);
        	this.PalWOPlanDate.TabIndex = 171;
        	// 
        	// lblPackingFrom
        	// 
        	this.lblPackingFrom.AutoSize = true;
        	this.lblPackingFrom.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.lblPackingFrom.Location = new System.Drawing.Point(2, 7);
        	this.lblPackingFrom.Name = "lblPackingFrom";
        	this.lblPackingFrom.Size = new System.Drawing.Size(21, 14);
        	this.lblPackingFrom.TabIndex = 0;
        	this.lblPackingFrom.Text = "从";
        	// 
        	// dtpWOPlanDateFrom
        	// 
        	this.dtpWOPlanDateFrom.CustomFormat = "yyyy-MM-dd";
        	this.dtpWOPlanDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpWOPlanDateFrom.Location = new System.Drawing.Point(24, 3);
        	this.dtpWOPlanDateFrom.Name = "dtpWOPlanDateFrom";
        	this.dtpWOPlanDateFrom.Size = new System.Drawing.Size(100, 20);
        	this.dtpWOPlanDateFrom.TabIndex = 5;
        	// 
        	// lblPackingTo
        	// 
        	this.lblPackingTo.AutoSize = true;
        	this.lblPackingTo.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.lblPackingTo.Location = new System.Drawing.Point(133, 8);
        	this.lblPackingTo.Name = "lblPackingTo";
        	this.lblPackingTo.Size = new System.Drawing.Size(21, 14);
        	this.lblPackingTo.TabIndex = 6;
        	this.lblPackingTo.Text = "到";
        	// 
        	// dtpWOPlanDateTo
        	// 
        	this.dtpWOPlanDateTo.CustomFormat = "yyyy-MM-dd";
        	this.dtpWOPlanDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpWOPlanDateTo.Location = new System.Drawing.Point(156, 4);
        	this.dtpWOPlanDateTo.Name = "dtpWOPlanDateTo";
        	this.dtpWOPlanDateTo.Size = new System.Drawing.Size(104, 20);
        	this.dtpWOPlanDateTo.TabIndex = 7;
        	// 
        	// PalWO
        	// 
        	this.PalWO.Controls.Add(this.txtWo);
        	this.PalWO.Controls.Add(this.label2);
        	this.PalWO.Location = new System.Drawing.Point(13, 41);
        	this.PalWO.Name = "PalWO";
        	this.PalWO.Size = new System.Drawing.Size(259, 35);
        	this.PalWO.TabIndex = 172;
        	// 
        	// txtWo
        	// 
        	this.txtWo.Location = new System.Drawing.Point(79, 3);
        	this.txtWo.Name = "txtWo";
        	this.txtWo.Size = new System.Drawing.Size(148, 20);
        	this.txtWo.TabIndex = 94;
        	this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(2, 7);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(52, 13);
        	this.label2.TabIndex = 94;
        	this.label2.Text = "工       单";
        	// 
        	// panel1
        	// 
        	this.panel1.Controls.Add(this.btnSave);
        	this.panel1.Controls.Add(this.btnQuery);
        	this.panel1.Controls.Add(this.ddlQueryType);
        	this.panel1.Controls.Add(this.lblCartonQuery);
        	this.panel1.Controls.Add(this.PalWO);
        	this.panel1.Controls.Add(this.PalWOPlanDate);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(465, 81);
        	this.panel1.TabIndex = 174;
        	// 
        	// btnSave
        	// 
        	this.btnSave.Location = new System.Drawing.Point(362, 43);
        	this.btnSave.Name = "btnSave";
        	this.btnSave.Size = new System.Drawing.Size(65, 24);
        	this.btnSave.TabIndex = 174;
        	this.btnSave.Tag = "button1";
        	this.btnSave.Text = "确定";
        	this.btnSave.UseVisualStyleBackColor = true;
        	this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.AllowUserToResizeRows = false;
        	dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
        	this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
        	this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.RowIndex,
        	        	        	this.WONO,
        	        	        	this.Mitem,
        	        	        	this.Order});
        	dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
        	dataGridViewCellStyle2.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
        	dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        	this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
        	this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridView1.Location = new System.Drawing.Point(0, 81);
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.RowHeadersVisible = false;
        	this.dataGridView1.RowTemplate.Height = 23;
        	this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.dataGridView1.Size = new System.Drawing.Size(465, 345);
        	this.dataGridView1.TabIndex = 175;
        	this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
        	this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
        	// 
        	// RowIndex
        	// 
        	this.RowIndex.FillWeight = 50F;
        	this.RowIndex.HeaderText = "序号";
        	this.RowIndex.Name = "RowIndex";
        	this.RowIndex.ReadOnly = true;
        	// 
        	// WONO
        	// 
        	this.WONO.FillWeight = 120F;
        	this.WONO.HeaderText = "工单";
        	this.WONO.Name = "WONO";
        	this.WONO.ReadOnly = true;
        	// 
        	// Mitem
        	// 
        	this.Mitem.FillWeight = 150F;
        	this.Mitem.HeaderText = "产品物料代码";
        	this.Mitem.Name = "Mitem";
        	this.Mitem.ReadOnly = true;
        	this.Mitem.Width = 150;
        	// 
        	// Order
        	// 
        	this.Order.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
        	this.Order.HeaderText = "订单";
        	this.Order.Name = "Order";
        	this.Order.ReadOnly = true;
        	// 
        	// FormMaterialTemplateWo
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(465, 426);
        	this.Controls.Add(this.dataGridView1);
        	this.Controls.Add(this.panel1);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
        	this.MaximizeBox = false;
        	this.MinimizeBox = false;
        	this.Name = "FormMaterialTemplateWo";
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Tag = "FRM0002";
        	this.Load += new System.EventHandler(this.FormMaterialTemplateWo_Load);
        	this.PalWOPlanDate.ResumeLayout(false);
        	this.PalWOPlanDate.PerformLayout();
        	this.PalWO.ResumeLayout(false);
        	this.PalWO.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ComboBox ddlQueryType;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalWOPlanDate;
        private System.Windows.Forms.Label lblPackingFrom;
        private System.Windows.Forms.DateTimePicker dtpWOPlanDateFrom;
        private System.Windows.Forms.Label lblPackingTo;
        private System.Windows.Forms.DateTimePicker dtpWOPlanDateTo;
        private System.Windows.Forms.Panel PalWO;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn WONO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mitem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order;
    }
}