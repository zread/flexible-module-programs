﻿namespace CSICPR
{
    partial class FrmSapInventoryDataQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblTestPowerSumValue = new System.Windows.Forms.Label();
            this.lblModuleCntValue = new System.Windows.Forms.Label();
            this.lblTestPowerSum = new System.Windows.Forms.Label();
            this.lblModuleCnt = new System.Windows.Forms.Label();
            this.dptTo = new System.Windows.Forms.DateTimePicker();
            this.dptFrom = new System.Windows.Forms.DateTimePicker();
            this.txtModuleSns = new System.Windows.Forms.TextBox();
            this.txtCustomerCartonNos = new System.Windows.Forms.TextBox();
            this.txtCartonNos = new System.Windows.Forms.TextBox();
            this.txtJobNo = new System.Windows.Forms.TextBox();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.cmbQueryType = new System.Windows.Forms.ComboBox();
            this.lblQueryType = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.colGUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCARTON_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCUSTOMER_CARTON_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colJOB_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colINNER_JOB_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFINISHED_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMODULE_COLOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMODULE_SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSALES_ORDER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSALES_ITEM_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colORDER_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPRODUCT_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFACTORY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colWORKSHOP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPACKING_LOCATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPACKING_MODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCELL_EFF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTEST_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSTD_POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMODULE_GRADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBYIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTOLERANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCELL_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCELL_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCELL_PRINT_MODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGLASS_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGLASS_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEVA_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEVA_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTPT_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTPT_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCONBOX_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCONBOX_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCONBOX_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLONG_FRAME_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLONG_FRAME_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSHORT_FRAME_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSHORT_FRAME_BATCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colGLASS_THICKNESS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIS_REWORK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIS_BY_PRODUCTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBATCH_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVOUCHER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.lblTestPowerSumValue);
            this.pnlTop.Controls.Add(this.lblModuleCntValue);
            this.pnlTop.Controls.Add(this.lblTestPowerSum);
            this.pnlTop.Controls.Add(this.lblModuleCnt);
            this.pnlTop.Controls.Add(this.dptTo);
            this.pnlTop.Controls.Add(this.dptFrom);
            this.pnlTop.Controls.Add(this.txtModuleSns);
            this.pnlTop.Controls.Add(this.txtCustomerCartonNos);
            this.pnlTop.Controls.Add(this.txtCartonNos);
            this.pnlTop.Controls.Add(this.txtJobNo);
            this.pnlTop.Controls.Add(this.txtOrderNo);
            this.pnlTop.Controls.Add(this.cmbQueryType);
            this.pnlTop.Controls.Add(this.lblQueryType);
            this.pnlTop.Controls.Add(this.btnExport);
            this.pnlTop.Controls.Add(this.btnReset);
            this.pnlTop.Controls.Add(this.btnQuery);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(3, 3);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(942, 70);
            this.pnlTop.TabIndex = 0;
            // 
            // lblTestPowerSumValue
            // 
            this.lblTestPowerSumValue.AutoSize = true;
            this.lblTestPowerSumValue.Location = new System.Drawing.Point(834, 46);
            this.lblTestPowerSumValue.Name = "lblTestPowerSumValue";
            this.lblTestPowerSumValue.Size = new System.Drawing.Size(11, 12);
            this.lblTestPowerSumValue.TabIndex = 65;
            this.lblTestPowerSumValue.Text = "0";
            // 
            // lblModuleCntValue
            // 
            this.lblModuleCntValue.AutoSize = true;
            this.lblModuleCntValue.ForeColor = System.Drawing.Color.Black;
            this.lblModuleCntValue.Location = new System.Drawing.Point(834, 27);
            this.lblModuleCntValue.Name = "lblModuleCntValue";
            this.lblModuleCntValue.Size = new System.Drawing.Size(11, 12);
            this.lblModuleCntValue.TabIndex = 66;
            this.lblModuleCntValue.Text = "0";
            // 
            // lblTestPowerSum
            // 
            this.lblTestPowerSum.AutoSize = true;
            this.lblTestPowerSum.Location = new System.Drawing.Point(738, 46);
            this.lblTestPowerSum.Name = "lblTestPowerSum";
            this.lblTestPowerSum.Size = new System.Drawing.Size(89, 12);
            this.lblTestPowerSum.TabIndex = 63;
            this.lblTestPowerSum.Text = "实测功率汇总：";
            // 
            // lblModuleCnt
            // 
            this.lblModuleCnt.AutoSize = true;
            this.lblModuleCnt.Location = new System.Drawing.Point(738, 27);
            this.lblModuleCnt.Name = "lblModuleCnt";
            this.lblModuleCnt.Size = new System.Drawing.Size(89, 12);
            this.lblModuleCnt.TabIndex = 64;
            this.lblModuleCnt.Text = "组件数量汇总：";
            // 
            // dptTo
            // 
            this.dptTo.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dptTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dptTo.Location = new System.Drawing.Point(308, 39);
            this.dptTo.Name = "dptTo";
            this.dptTo.Size = new System.Drawing.Size(152, 21);
            this.dptTo.TabIndex = 62;
            this.dptTo.Visible = false;
            // 
            // dptFrom
            // 
            this.dptFrom.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dptFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dptFrom.Location = new System.Drawing.Point(150, 39);
            this.dptFrom.Name = "dptFrom";
            this.dptFrom.Size = new System.Drawing.Size(152, 21);
            this.dptFrom.TabIndex = 61;
            this.dptFrom.Visible = false;
            // 
            // txtModuleSns
            // 
            this.txtModuleSns.AcceptsReturn = true;
            this.txtModuleSns.Location = new System.Drawing.Point(150, 39);
            this.txtModuleSns.Multiline = true;
            this.txtModuleSns.Name = "txtModuleSns";
            this.txtModuleSns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtModuleSns.Size = new System.Drawing.Size(180, 75);
            this.txtModuleSns.TabIndex = 60;
            this.txtModuleSns.Visible = false;
            // 
            // txtCustomerCartonNos
            // 
            this.txtCustomerCartonNos.AcceptsReturn = true;
            this.txtCustomerCartonNos.Location = new System.Drawing.Point(150, 39);
            this.txtCustomerCartonNos.Multiline = true;
            this.txtCustomerCartonNos.Name = "txtCustomerCartonNos";
            this.txtCustomerCartonNos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCustomerCartonNos.Size = new System.Drawing.Size(180, 75);
            this.txtCustomerCartonNos.TabIndex = 59;
            this.txtCustomerCartonNos.Visible = false;
            // 
            // txtCartonNos
            // 
            this.txtCartonNos.AcceptsReturn = true;
            this.txtCartonNos.Location = new System.Drawing.Point(150, 39);
            this.txtCartonNos.Multiline = true;
            this.txtCartonNos.Name = "txtCartonNos";
            this.txtCartonNos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCartonNos.Size = new System.Drawing.Size(180, 75);
            this.txtCartonNos.TabIndex = 58;
            this.txtCartonNos.Visible = false;
            // 
            // txtJobNo
            // 
            this.txtJobNo.Location = new System.Drawing.Point(150, 39);
            this.txtJobNo.Name = "txtJobNo";
            this.txtJobNo.Size = new System.Drawing.Size(180, 21);
            this.txtJobNo.TabIndex = 57;
            this.txtJobNo.Visible = false;
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(150, 39);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(180, 21);
            this.txtOrderNo.TabIndex = 56;
            this.txtOrderNo.Visible = false;
            // 
            // cmbQueryType
            // 
            this.cmbQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueryType.FormattingEnabled = true;
            this.cmbQueryType.Items.AddRange(new object[] {
            "工单",
            "出货柜号",
            "托号",
            "客户托号",
            "组件序列号",
            "入库上传时间范围"});
            this.cmbQueryType.Location = new System.Drawing.Point(15, 39);
            this.cmbQueryType.Name = "cmbQueryType";
            this.cmbQueryType.Size = new System.Drawing.Size(121, 20);
            this.cmbQueryType.TabIndex = 55;
            this.cmbQueryType.SelectedIndexChanged += new System.EventHandler(this.cmbQueryType_SelectedIndexChanged);
            // 
            // lblQueryType
            // 
            this.lblQueryType.AutoSize = true;
            this.lblQueryType.Location = new System.Drawing.Point(15, 13);
            this.lblQueryType.Name = "lblQueryType";
            this.lblQueryType.Size = new System.Drawing.Size(53, 12);
            this.lblQueryType.TabIndex = 54;
            this.lblQueryType.Text = "查询条件";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(627, 37);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(90, 22);
            this.btnExport.TabIndex = 53;
            this.btnExport.Text = "导出到Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(553, 38);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(68, 22);
            this.btnReset.TabIndex = 53;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(479, 38);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(68, 22);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colGUID,
            this.colCARTON_NO,
            this.colCUSTOMER_CARTON_NO,
            this.colJOB_NO,
            this.colINNER_JOB_NO,
            this.colFINISHED_ON,
            this.colMODULE_COLOR,
            this.colMODULE_SN,
            this.colORDER_NO,
            this.colSALES_ORDER_NO,
            this.colSALES_ITEM_NO,
            this.colORDER_STATUS,
            this.colPRODUCT_CODE,
            this.colUNIT,
            this.colFACTORY,
            this.colWORKSHOP,
            this.colPACKING_LOCATION,
            this.colPACKING_MODE,
            this.colCELL_EFF,
            this.colTEST_POWER,
            this.colSTD_POWER,
            this.colMODULE_GRADE,
            this.colBYIM,
            this.colTOLERANCE,
            this.colCELL_CODE,
            this.colCELL_BATCH,
            this.colCELL_PRINT_MODE,
            this.colGLASS_CODE,
            this.colGLASS_BATCH,
            this.colEVA_CODE,
            this.colEVA_BATCH,
            this.colTPT_CODE,
            this.colTPT_BATCH,
            this.colCONBOX_CODE,
            this.colCONBOX_BATCH,
            this.colCONBOX_TYPE,
            this.colLONG_FRAME_CODE,
            this.colLONG_FRAME_BATCH,
            this.colSHORT_FRAME_CODE,
            this.colSHORT_FRAME_BATCH,
            this.colGLASS_THICKNESS,
            this.colIS_REWORK,
            this.colIS_BY_PRODUCTION,
            this.colBATCH_NO,
            this.colVOUCHER,
            this.colCREATE_ON});
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(3, 73);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.RowTemplate.Height = 23;
            this.dgvData.Size = new System.Drawing.Size(942, 280);
            this.dgvData.TabIndex = 1;
            // 
            // colGUID
            // 
            this.colGUID.DataPropertyName = "GUID";
            this.colGUID.HeaderText = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.ReadOnly = true;
            this.colGUID.Width = 150;
            // 
            // colCARTON_NO
            // 
            this.colCARTON_NO.DataPropertyName = "CARTON_NO";
            this.colCARTON_NO.HeaderText = "托号";
            this.colCARTON_NO.Name = "colCARTON_NO";
            this.colCARTON_NO.ReadOnly = true;
            this.colCARTON_NO.Width = 150;
            // 
            // colCUSTOMER_CARTON_NO
            // 
            this.colCUSTOMER_CARTON_NO.DataPropertyName = "CUSTOMER_CARTON_NO";
            this.colCUSTOMER_CARTON_NO.HeaderText = "客户托号";
            this.colCUSTOMER_CARTON_NO.Name = "colCUSTOMER_CARTON_NO";
            this.colCUSTOMER_CARTON_NO.ReadOnly = true;
            this.colCUSTOMER_CARTON_NO.Width = 150;
            // 
            // colJOB_NO
            // 
            this.colJOB_NO.DataPropertyName = "JOB_NO";
            this.colJOB_NO.HeaderText = "出货柜号";
            this.colJOB_NO.Name = "colJOB_NO";
            this.colJOB_NO.ReadOnly = true;
            this.colJOB_NO.Width = 150;
            // 
            // colINNER_JOB_NO
            // 
            this.colINNER_JOB_NO.DataPropertyName = "INNER_JOB_NO";
            this.colINNER_JOB_NO.HeaderText = "车间内部柜号";
            this.colINNER_JOB_NO.Name = "colINNER_JOB_NO";
            this.colINNER_JOB_NO.ReadOnly = true;
            this.colINNER_JOB_NO.Width = 150;
            // 
            // colFINISHED_ON
            // 
            this.colFINISHED_ON.DataPropertyName = "FINISHED_ON";
            this.colFINISHED_ON.HeaderText = "打托时间";
            this.colFINISHED_ON.Name = "colFINISHED_ON";
            this.colFINISHED_ON.ReadOnly = true;
            this.colFINISHED_ON.Width = 150;
            // 
            // colMODULE_COLOR
            // 
            this.colMODULE_COLOR.DataPropertyName = "MODULE_COLOR";
            this.colMODULE_COLOR.HeaderText = "组件颜色";
            this.colMODULE_COLOR.Name = "colMODULE_COLOR";
            this.colMODULE_COLOR.ReadOnly = true;
            this.colMODULE_COLOR.Width = 150;
            // 
            // colMODULE_SN
            // 
            this.colMODULE_SN.DataPropertyName = "MODULE_SN";
            this.colMODULE_SN.HeaderText = "组件序列号";
            this.colMODULE_SN.Name = "colMODULE_SN";
            this.colMODULE_SN.ReadOnly = true;
            this.colMODULE_SN.Width = 150;
            // 
            // colORDER_NO
            // 
            this.colORDER_NO.DataPropertyName = "ORDER_NO";
            this.colORDER_NO.HeaderText = "生产订单号";
            this.colORDER_NO.Name = "colORDER_NO";
            this.colORDER_NO.ReadOnly = true;
            this.colORDER_NO.Width = 150;
            // 
            // colSALES_ORDER_NO
            // 
            this.colSALES_ORDER_NO.DataPropertyName = "SALES_ORDER_NO";
            this.colSALES_ORDER_NO.HeaderText = "销售订单";
            this.colSALES_ORDER_NO.Name = "colSALES_ORDER_NO";
            this.colSALES_ORDER_NO.ReadOnly = true;
            this.colSALES_ORDER_NO.Width = 150;
            // 
            // colSALES_ITEM_NO
            // 
            this.colSALES_ITEM_NO.DataPropertyName = "SALES_ITEM_NO";
            this.colSALES_ITEM_NO.HeaderText = "销售订单行项目";
            this.colSALES_ITEM_NO.Name = "colSALES_ITEM_NO";
            this.colSALES_ITEM_NO.ReadOnly = true;
            this.colSALES_ITEM_NO.Width = 150;
            // 
            // colORDER_STATUS
            // 
            this.colORDER_STATUS.DataPropertyName = "ORDER_STATUS";
            this.colORDER_STATUS.HeaderText = "生产订单状态";
            this.colORDER_STATUS.Name = "colORDER_STATUS";
            this.colORDER_STATUS.ReadOnly = true;
            this.colORDER_STATUS.Width = 150;
            // 
            // colPRODUCT_CODE
            // 
            this.colPRODUCT_CODE.DataPropertyName = "PRODUCT_CODE";
            this.colPRODUCT_CODE.HeaderText = "产品物料代码";
            this.colPRODUCT_CODE.Name = "colPRODUCT_CODE";
            this.colPRODUCT_CODE.ReadOnly = true;
            this.colPRODUCT_CODE.Width = 150;
            // 
            // colUNIT
            // 
            this.colUNIT.DataPropertyName = "UNIT";
            this.colUNIT.HeaderText = "单位";
            this.colUNIT.Name = "colUNIT";
            this.colUNIT.ReadOnly = true;
            this.colUNIT.Width = 150;
            // 
            // colFACTORY
            // 
            this.colFACTORY.DataPropertyName = "FACTORY";
            this.colFACTORY.HeaderText = "工厂";
            this.colFACTORY.Name = "colFACTORY";
            this.colFACTORY.ReadOnly = true;
            this.colFACTORY.Width = 150;
            // 
            // colWORKSHOP
            // 
            this.colWORKSHOP.DataPropertyName = "WORKSHOP";
            this.colWORKSHOP.HeaderText = "车间";
            this.colWORKSHOP.Name = "colWORKSHOP";
            this.colWORKSHOP.ReadOnly = true;
            this.colWORKSHOP.Width = 150;
            // 
            // colPACKING_LOCATION
            // 
            this.colPACKING_LOCATION.DataPropertyName = "PACKING_LOCATION";
            this.colPACKING_LOCATION.HeaderText = "入库地点";
            this.colPACKING_LOCATION.Name = "colPACKING_LOCATION";
            this.colPACKING_LOCATION.ReadOnly = true;
            this.colPACKING_LOCATION.Width = 150;
            // 
            // colPACKING_MODE
            // 
            this.colPACKING_MODE.DataPropertyName = "PACKING_MODE";
            this.colPACKING_MODE.HeaderText = "包装方式";
            this.colPACKING_MODE.Name = "colPACKING_MODE";
            this.colPACKING_MODE.ReadOnly = true;
            this.colPACKING_MODE.Width = 150;
            // 
            // colCELL_EFF
            // 
            this.colCELL_EFF.DataPropertyName = "CELL_EFF";
            this.colCELL_EFF.HeaderText = "电池片转换效率";
            this.colCELL_EFF.Name = "colCELL_EFF";
            this.colCELL_EFF.ReadOnly = true;
            this.colCELL_EFF.Width = 150;
            // 
            // colTEST_POWER
            // 
            this.colTEST_POWER.DataPropertyName = "TEST_POWER";
            this.colTEST_POWER.HeaderText = "实测功率";
            this.colTEST_POWER.Name = "colTEST_POWER";
            this.colTEST_POWER.ReadOnly = true;
            this.colTEST_POWER.Width = 150;
            // 
            // colSTD_POWER
            // 
            this.colSTD_POWER.DataPropertyName = "STD_POWER";
            this.colSTD_POWER.HeaderText = "标称功率";
            this.colSTD_POWER.Name = "colSTD_POWER";
            this.colSTD_POWER.ReadOnly = true;
            this.colSTD_POWER.Width = 150;
            // 
            // colMODULE_GRADE
            // 
            this.colMODULE_GRADE.DataPropertyName = "MODULE_GRADE";
            this.colMODULE_GRADE.HeaderText = "组件等级";
            this.colMODULE_GRADE.Name = "colMODULE_GRADE";
            this.colMODULE_GRADE.ReadOnly = true;
            this.colMODULE_GRADE.Width = 150;
            // 
            // colBYIM
            // 
            this.colBYIM.DataPropertyName = "BYIM";
            this.colBYIM.HeaderText = "电流分档";
            this.colBYIM.Name = "colBYIM";
            this.colBYIM.ReadOnly = true;
            this.colBYIM.Width = 150;
            // 
            // colTOLERANCE
            // 
            this.colTOLERANCE.DataPropertyName = "TOLERANCE";
            this.colTOLERANCE.HeaderText = "公差";
            this.colTOLERANCE.Name = "colTOLERANCE";
            this.colTOLERANCE.ReadOnly = true;
            this.colTOLERANCE.Width = 150;
            // 
            // colCELL_CODE
            // 
            this.colCELL_CODE.DataPropertyName = "CELL_CODE";
            this.colCELL_CODE.HeaderText = "电池片物料号";
            this.colCELL_CODE.Name = "colCELL_CODE";
            this.colCELL_CODE.ReadOnly = true;
            this.colCELL_CODE.Width = 150;
            // 
            // colCELL_BATCH
            // 
            this.colCELL_BATCH.DataPropertyName = "CELL_BATCH";
            this.colCELL_BATCH.HeaderText = "电池片批次";
            this.colCELL_BATCH.Name = "colCELL_BATCH";
            this.colCELL_BATCH.ReadOnly = true;
            this.colCELL_BATCH.Width = 150;
            // 
            // colCELL_PRINT_MODE
            // 
            this.colCELL_PRINT_MODE.DataPropertyName = "CELL_PRINT_MODE";
            this.colCELL_PRINT_MODE.HeaderText = "电池片网版本";
            this.colCELL_PRINT_MODE.Name = "colCELL_PRINT_MODE";
            this.colCELL_PRINT_MODE.ReadOnly = true;
            this.colCELL_PRINT_MODE.Width = 150;
            // 
            // colGLASS_CODE
            // 
            this.colGLASS_CODE.DataPropertyName = "GLASS_CODE";
            this.colGLASS_CODE.HeaderText = "玻璃物料号";
            this.colGLASS_CODE.Name = "colGLASS_CODE";
            this.colGLASS_CODE.ReadOnly = true;
            this.colGLASS_CODE.Width = 150;
            // 
            // colGLASS_BATCH
            // 
            this.colGLASS_BATCH.DataPropertyName = "GLASS_BATCH";
            this.colGLASS_BATCH.HeaderText = "玻璃批次";
            this.colGLASS_BATCH.Name = "colGLASS_BATCH";
            this.colGLASS_BATCH.ReadOnly = true;
            this.colGLASS_BATCH.Width = 150;
            // 
            // colEVA_CODE
            // 
            this.colEVA_CODE.DataPropertyName = "EVA_CODE";
            this.colEVA_CODE.HeaderText = "EVA物料号";
            this.colEVA_CODE.Name = "colEVA_CODE";
            this.colEVA_CODE.ReadOnly = true;
            this.colEVA_CODE.Width = 150;
            // 
            // colEVA_BATCH
            // 
            this.colEVA_BATCH.DataPropertyName = "EVA_BATCH";
            this.colEVA_BATCH.HeaderText = "EVA批次";
            this.colEVA_BATCH.Name = "colEVA_BATCH";
            this.colEVA_BATCH.ReadOnly = true;
            this.colEVA_BATCH.Width = 150;
            // 
            // colTPT_CODE
            // 
            this.colTPT_CODE.DataPropertyName = "TPT_CODE";
            this.colTPT_CODE.HeaderText = "背板物料号";
            this.colTPT_CODE.Name = "colTPT_CODE";
            this.colTPT_CODE.ReadOnly = true;
            this.colTPT_CODE.Width = 150;
            // 
            // colTPT_BATCH
            // 
            this.colTPT_BATCH.DataPropertyName = "TPT_BATCH";
            this.colTPT_BATCH.HeaderText = "背板批次";
            this.colTPT_BATCH.Name = "colTPT_BATCH";
            this.colTPT_BATCH.ReadOnly = true;
            this.colTPT_BATCH.Width = 150;
            // 
            // colCONBOX_CODE
            // 
            this.colCONBOX_CODE.DataPropertyName = "CONBOX_CODE";
            this.colCONBOX_CODE.HeaderText = "接线盒物料号";
            this.colCONBOX_CODE.Name = "colCONBOX_CODE";
            this.colCONBOX_CODE.ReadOnly = true;
            this.colCONBOX_CODE.Width = 150;
            // 
            // colCONBOX_BATCH
            // 
            this.colCONBOX_BATCH.DataPropertyName = "CONBOX_BATCH";
            this.colCONBOX_BATCH.HeaderText = "接线盒批次";
            this.colCONBOX_BATCH.Name = "colCONBOX_BATCH";
            this.colCONBOX_BATCH.ReadOnly = true;
            this.colCONBOX_BATCH.Width = 150;
            // 
            // colCONBOX_TYPE
            // 
            this.colCONBOX_TYPE.DataPropertyName = "CONBOX_TYPE";
            this.colCONBOX_TYPE.HeaderText = "接线盒类型";
            this.colCONBOX_TYPE.Name = "colCONBOX_TYPE";
            this.colCONBOX_TYPE.ReadOnly = true;
            this.colCONBOX_TYPE.Width = 150;
            // 
            // colLONG_FRAME_CODE
            // 
            this.colLONG_FRAME_CODE.DataPropertyName = "LONG_FRAME_CODE";
            this.colLONG_FRAME_CODE.HeaderText = "长边框物料号";
            this.colLONG_FRAME_CODE.Name = "colLONG_FRAME_CODE";
            this.colLONG_FRAME_CODE.ReadOnly = true;
            this.colLONG_FRAME_CODE.Width = 150;
            // 
            // colLONG_FRAME_BATCH
            // 
            this.colLONG_FRAME_BATCH.DataPropertyName = "LONG_FRAME_BATCH";
            this.colLONG_FRAME_BATCH.HeaderText = "长边框批次";
            this.colLONG_FRAME_BATCH.Name = "colLONG_FRAME_BATCH";
            this.colLONG_FRAME_BATCH.ReadOnly = true;
            this.colLONG_FRAME_BATCH.Width = 150;
            // 
            // colSHORT_FRAME_CODE
            // 
            this.colSHORT_FRAME_CODE.DataPropertyName = "SHORT_FRAME_CODE";
            this.colSHORT_FRAME_CODE.HeaderText = "短边框物料号";
            this.colSHORT_FRAME_CODE.Name = "colSHORT_FRAME_CODE";
            this.colSHORT_FRAME_CODE.ReadOnly = true;
            this.colSHORT_FRAME_CODE.Width = 150;
            // 
            // colSHORT_FRAME_BATCH
            // 
            this.colSHORT_FRAME_BATCH.DataPropertyName = "SHORT_FRAME_BATCH";
            this.colSHORT_FRAME_BATCH.HeaderText = "短边框批次";
            this.colSHORT_FRAME_BATCH.Name = "colSHORT_FRAME_BATCH";
            this.colSHORT_FRAME_BATCH.ReadOnly = true;
            this.colSHORT_FRAME_BATCH.Width = 150;
            // 
            // colGLASS_THICKNESS
            // 
            this.colGLASS_THICKNESS.DataPropertyName = "GLASS_THICKNESS";
            this.colGLASS_THICKNESS.HeaderText = "玻璃厚度";
            this.colGLASS_THICKNESS.Name = "colGLASS_THICKNESS";
            this.colGLASS_THICKNESS.ReadOnly = true;
            this.colGLASS_THICKNESS.Width = 150;
            // 
            // colIS_REWORK
            // 
            this.colIS_REWORK.DataPropertyName = "IS_REWORK";
            this.colIS_REWORK.HeaderText = "是否重工";
            this.colIS_REWORK.Name = "colIS_REWORK";
            this.colIS_REWORK.ReadOnly = true;
            this.colIS_REWORK.Width = 150;
            // 
            // colIS_BY_PRODUCTION
            // 
            this.colIS_BY_PRODUCTION.DataPropertyName = "IS_BY_PRODUCTION";
            this.colIS_BY_PRODUCTION.HeaderText = "是否副产品";
            this.colIS_BY_PRODUCTION.Name = "colIS_BY_PRODUCTION";
            this.colIS_BY_PRODUCTION.ReadOnly = true;
            this.colIS_BY_PRODUCTION.Width = 150;
            // 
            // colBATCH_NO
            // 
            this.colBATCH_NO.DataPropertyName = "BATCH_NO";
            this.colBATCH_NO.HeaderText = "批次号";
            this.colBATCH_NO.Name = "colBATCH_NO";
            this.colBATCH_NO.ReadOnly = true;
            this.colBATCH_NO.Width = 150;
            // 
            // colVOUCHER
            // 
            this.colVOUCHER.DataPropertyName = "VOUCHER";
            this.colVOUCHER.HeaderText = "凭证号";
            this.colVOUCHER.Name = "colVOUCHER";
            this.colVOUCHER.ReadOnly = true;
            this.colVOUCHER.Width = 150;
            // 
            // colCREATE_ON
            // 
            this.colCREATE_ON.DataPropertyName = "CREATE_ON";
            this.colCREATE_ON.HeaderText = "记录时间";
            this.colCREATE_ON.Name = "colCREATE_ON";
            this.colCREATE_ON.ReadOnly = true;
            this.colCREATE_ON.Width = 150;
            // 
            // FrmSapInventoryDataQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 356);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.pnlTop);
            this.Name = "FrmSapInventoryDataQuery";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "入库数据查询";
            this.Load += new System.EventHandler(this.FrmSapInventoryUploadResult_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ComboBox cmbQueryType;
        private System.Windows.Forms.Label lblQueryType;
        private System.Windows.Forms.TextBox txtOrderNo;
        private System.Windows.Forms.TextBox txtJobNo;
        private System.Windows.Forms.TextBox txtModuleSns;
        private System.Windows.Forms.TextBox txtCustomerCartonNos;
        private System.Windows.Forms.TextBox txtCartonNos;
        private System.Windows.Forms.DateTimePicker dptTo;
        private System.Windows.Forms.DateTimePicker dptFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCARTON_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCUSTOMER_CARTON_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colJOB_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colINNER_JOB_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFINISHED_ON;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMODULE_COLOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMODULE_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn colORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSALES_ORDER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSALES_ITEM_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colORDER_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPRODUCT_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUNIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFACTORY;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWORKSHOP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPACKING_LOCATION;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPACKING_MODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCELL_EFF;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTEST_POWER;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSTD_POWER;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMODULE_GRADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBYIM;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTOLERANCE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCELL_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCELL_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCELL_PRINT_MODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGLASS_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGLASS_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEVA_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEVA_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTPT_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTPT_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCONBOX_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCONBOX_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCONBOX_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLONG_FRAME_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLONG_FRAME_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSHORT_FRAME_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSHORT_FRAME_BATCH;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGLASS_THICKNESS;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIS_REWORK;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIS_BY_PRODUCTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBATCH_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVOUCHER;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCREATE_ON;
        private System.Windows.Forms.Label lblTestPowerSumValue;
        private System.Windows.Forms.Label lblModuleCntValue;
        private System.Windows.Forms.Label lblTestPowerSum;
        private System.Windows.Forms.Label lblModuleCnt;
        private System.Windows.Forms.Button btnExport;
    }
}