﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormSNDetail : Form
    {
        public FormSNDetail()
        {
            InitializeComponent();
        }
        private string CartonNo = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CartonNo">托号</param>
        /// <param name="OldCellCode">电池片物料号</param>
        /// <param name="OldCellBatch">电池片批次号</param>
        /// <param name="_ListPackingSN">暂存组件电池片信息</param>
        public FormSNDetail(string _CartonNo)
        {
            InitializeComponent();
            CartonNo = _CartonNo;
            
        }

        private void FormSNDetail_Load(object sender, EventArgs e)
        {
            DataTable dt = ProductStorageDAL.GetSAPCartonStorageInfo(CartonNo);
            if (dt != null && dt.Rows.Count > 0)
            {
                this.dataGridView1.DataSource = dt;
                this.Text = "托号: " + CartonNo +";共 "+dt.Rows.Count+" 笔数据";  
                #region
                //foreach (DataRow row in dt.Rows)
                //{
                //    dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { this.dataGridView1.Rows.Count + 1,
                //        Convert.ToString(row["SN"]), 
                //        Convert.ToString(row["TWO_NO"]), 
                //        Convert.ToString(row["BoxID"]),
                //        Convert.ToString(row["Cust_BoxID"]),
                //        Convert.ToString(row["TWO_PRODUCT_NAME"]),
                //        Convert.ToString(row["RESV04"]),
                //        Convert.ToString(row["RESV06"]),
                //        Convert.ToString(row["TWO_ORDER_NO"]),
                //        Convert.ToString(row["TWO_ORDER_ITEM"]),
                //        Convert.ToString(row["ByIm"]),
                //        Convert.ToString(row["CellBatch"]),
                //        Convert.ToString(row["CellCode"]),
                //        Convert.ToString(row["CellEff"]),
                //        Convert.ToString(row["CellPrintMode"]),
                //        Convert.ToString(row["ConBoxBatch"]),
                //        Convert.ToString(row["ConBoxCode"]),
                //        Convert.ToString(row["EvaBatch"]),
                //        Convert.ToString(row["EvaCode"]),
                //        Convert.ToString(row["GlassBatch"]),
                //        Convert.ToString(row["GlassCode"]),
                //        Convert.ToString(row["GlassThickness"]),
                //        Convert.ToString(row["LongFrameBatch"]),
                //        Convert.ToString(row["LongFrameCode"]),
                //        Convert.ToString(row["ModuleGrade"]),
                //        Convert.ToString(row["PackingMode"]),
                //        Convert.ToString(row["ShortFrameBatch"]),
                //        Convert.ToString(row["ShortFrameCode"]),
                //        Convert.ToString(row["TptBatch"]),
                //        Convert.ToString(row["TptCode"])
                //    });
                //}
                #endregion
            }
            else
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show("获取组件信息失败");
                return;
            }
        }
    }
}
