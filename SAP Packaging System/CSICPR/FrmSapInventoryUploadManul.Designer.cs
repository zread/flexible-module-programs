﻿namespace CSICPR
{
    partial class FrmSapInventoryUploadManul
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.pnlTop = new System.Windows.Forms.Panel();
        	this.BtRetry = new System.Windows.Forms.Button();
        	this.CbTECO = new System.Windows.Forms.CheckBox();
        	this.DTpick = new System.Windows.Forms.DateTimePicker();
        	this.lblTestPowerSumValue = new System.Windows.Forms.Label();
        	this.lblModuleCntValue = new System.Windows.Forms.Label();
        	this.lblTestPowerSum = new System.Windows.Forms.Label();
        	this.lblModuleCnt = new System.Windows.Forms.Label();
        	this.lblInfo = new System.Windows.Forms.Label();
        	this.btnReset = new System.Windows.Forms.Button();
        	this.btnUpload = new System.Windows.Forms.Button();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.pnlJobNo = new System.Windows.Forms.Panel();
        	this.txtJobNo = new System.Windows.Forms.TextBox();
        	this.lblJobNo = new System.Windows.Forms.Label();
        	this.PlCarton = new System.Windows.Forms.Panel();
        	this.txtCartons = new System.Windows.Forms.TextBox();
        	this.lblCartonNo = new System.Windows.Forms.Label();
        	this.dgvData = new System.Windows.Forms.DataGridView();
        	this.colCommandName = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.colCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colFinishedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colModuleColor = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colModuleSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colSalesOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colSalesItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colOrderStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colFactory = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colWorkshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colPackingLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colPackingMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCellEff = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTestPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStdPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colModuleGrade = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colByIm = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTolerance = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCellCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCellBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCellPrintMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colGlassCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colGlassBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colEvaCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colEvaBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTptCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTptBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colConboxCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colConboxBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colConboxType = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colLongFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colLongFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colShortFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colShortFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colGlassThickness = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colIsRework = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colIsByProduction = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.CbAuto = new System.Windows.Forms.CheckBox();
        	this.pnlTop.SuspendLayout();
        	this.pnlJobNo.SuspendLayout();
        	this.PlCarton.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// pnlTop
        	// 
        	this.pnlTop.Controls.Add(this.CbAuto);
        	this.pnlTop.Controls.Add(this.BtRetry);
        	this.pnlTop.Controls.Add(this.CbTECO);
        	this.pnlTop.Controls.Add(this.DTpick);
        	this.pnlTop.Controls.Add(this.lblTestPowerSumValue);
        	this.pnlTop.Controls.Add(this.lblModuleCntValue);
        	this.pnlTop.Controls.Add(this.lblTestPowerSum);
        	this.pnlTop.Controls.Add(this.lblModuleCnt);
        	this.pnlTop.Controls.Add(this.lblInfo);
        	this.pnlTop.Controls.Add(this.btnReset);
        	this.pnlTop.Controls.Add(this.btnUpload);
        	this.pnlTop.Controls.Add(this.btnQuery);
        	this.pnlTop.Controls.Add(this.pnlJobNo);
        	this.pnlTop.Controls.Add(this.PlCarton);
        	this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pnlTop.Location = new System.Drawing.Point(3, 3);
        	this.pnlTop.Name = "pnlTop";
        	this.pnlTop.Size = new System.Drawing.Size(1046, 140);
        	this.pnlTop.TabIndex = 0;
        	// 
        	// BtRetry
        	// 
        	this.BtRetry.Location = new System.Drawing.Point(822, 97);
        	this.BtRetry.Name = "BtRetry";
        	this.BtRetry.Size = new System.Drawing.Size(73, 24);
        	this.BtRetry.TabIndex = 59;
        	this.BtRetry.Text = "Retry";
        	this.BtRetry.UseVisualStyleBackColor = true;
        	this.BtRetry.Click += new System.EventHandler(this.BtRetryClick);
        	// 
        	// CbTECO
        	// 
        	this.CbTECO.Location = new System.Drawing.Point(684, 97);
        	this.CbTECO.Name = "CbTECO";
        	this.CbTECO.Size = new System.Drawing.Size(77, 26);
        	this.CbTECO.TabIndex = 58;
        	this.CbTECO.Text = "TECO";
        	this.CbTECO.UseVisualStyleBackColor = true;
        	this.CbTECO.CheckedChanged += new System.EventHandler(this.CbTECOCheckedChanged);
        	// 
        	// DTpick
        	// 
        	this.DTpick.Enabled = false;
        	this.DTpick.Location = new System.Drawing.Point(453, 102);
        	this.DTpick.Name = "DTpick";
        	this.DTpick.Size = new System.Drawing.Size(200, 20);
        	this.DTpick.TabIndex = 57;
        	// 
        	// lblTestPowerSumValue
        	// 
        	this.lblTestPowerSumValue.AutoSize = true;
        	this.lblTestPowerSumValue.Location = new System.Drawing.Point(759, 34);
        	this.lblTestPowerSumValue.Name = "lblTestPowerSumValue";
        	this.lblTestPowerSumValue.Size = new System.Drawing.Size(13, 13);
        	this.lblTestPowerSumValue.TabIndex = 56;
        	this.lblTestPowerSumValue.Text = "0";
        	// 
        	// lblModuleCntValue
        	// 
        	this.lblModuleCntValue.AutoSize = true;
        	this.lblModuleCntValue.ForeColor = System.Drawing.Color.Black;
        	this.lblModuleCntValue.Location = new System.Drawing.Point(759, 13);
        	this.lblModuleCntValue.Name = "lblModuleCntValue";
        	this.lblModuleCntValue.Size = new System.Drawing.Size(13, 13);
        	this.lblModuleCntValue.TabIndex = 56;
        	this.lblModuleCntValue.Text = "0";
        	// 
        	// lblTestPowerSum
        	// 
        	this.lblTestPowerSum.AutoSize = true;
        	this.lblTestPowerSum.Location = new System.Drawing.Point(663, 34);
        	this.lblTestPowerSum.Name = "lblTestPowerSum";
        	this.lblTestPowerSum.Size = new System.Drawing.Size(85, 13);
        	this.lblTestPowerSum.TabIndex = 55;
        	this.lblTestPowerSum.Text = "实测功率汇总：";
        	// 
        	// lblModuleCnt
        	// 
        	this.lblModuleCnt.AutoSize = true;
        	this.lblModuleCnt.Location = new System.Drawing.Point(663, 13);
        	this.lblModuleCnt.Name = "lblModuleCnt";
        	this.lblModuleCnt.Size = new System.Drawing.Size(85, 13);
        	this.lblModuleCnt.TabIndex = 55;
        	this.lblModuleCnt.Text = "组件数量汇总：";
        	// 
        	// lblInfo
        	// 
        	this.lblInfo.AutoSize = true;
        	this.lblInfo.ForeColor = System.Drawing.Color.Red;
        	this.lblInfo.Location = new System.Drawing.Point(622, 63);
        	this.lblInfo.Name = "lblInfo";
        	this.lblInfo.Size = new System.Drawing.Size(183, 13);
        	this.lblInfo.TabIndex = 54;
        	this.lblInfo.Text = "出货柜号为空，表示不做二级包装";
        	this.lblInfo.Visible = false;
        	// 
        	// btnReset
        	// 
        	this.btnReset.Location = new System.Drawing.Point(453, 18);
        	this.btnReset.Name = "btnReset";
        	this.btnReset.Size = new System.Drawing.Size(68, 24);
        	this.btnReset.TabIndex = 53;
        	this.btnReset.Text = "重置";
        	this.btnReset.UseVisualStyleBackColor = true;
        	this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
        	// 
        	// btnUpload
        	// 
        	this.btnUpload.Location = new System.Drawing.Point(360, 99);
        	this.btnUpload.Name = "btnUpload";
        	this.btnUpload.Size = new System.Drawing.Size(73, 24);
        	this.btnUpload.TabIndex = 52;
        	this.btnUpload.Text = "上传";
        	this.btnUpload.UseVisualStyleBackColor = true;
        	this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(359, 18);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(83, 24);
        	this.btnQuery.TabIndex = 2;
        	this.btnQuery.Text = "添加到列表";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// pnlJobNo
        	// 
        	this.pnlJobNo.Controls.Add(this.txtJobNo);
        	this.pnlJobNo.Controls.Add(this.lblJobNo);
        	this.pnlJobNo.Location = new System.Drawing.Point(359, 53);
        	this.pnlJobNo.Name = "pnlJobNo";
        	this.pnlJobNo.Size = new System.Drawing.Size(249, 34);
        	this.pnlJobNo.TabIndex = 49;
        	// 
        	// txtJobNo
        	// 
        	this.txtJobNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtJobNo.Location = new System.Drawing.Point(78, 3);
        	this.txtJobNo.MaxLength = 24;
        	this.txtJobNo.Name = "txtJobNo";
        	this.txtJobNo.Size = new System.Drawing.Size(168, 20);
        	this.txtJobNo.TabIndex = 52;
        	this.txtJobNo.Visible = false;
        	// 
        	// lblJobNo
        	// 
        	this.lblJobNo.AutoSize = true;
        	this.lblJobNo.Location = new System.Drawing.Point(3, 8);
        	this.lblJobNo.Name = "lblJobNo";
        	this.lblJobNo.Size = new System.Drawing.Size(64, 13);
        	this.lblJobNo.TabIndex = 6;
        	this.lblJobNo.Text = "出 货 柜 号";
        	this.lblJobNo.Visible = false;
        	// 
        	// PlCarton
        	// 
        	this.PlCarton.Controls.Add(this.txtCartons);
        	this.PlCarton.Controls.Add(this.lblCartonNo);
        	this.PlCarton.Location = new System.Drawing.Point(24, 15);
        	this.PlCarton.Name = "PlCarton";
        	this.PlCarton.Size = new System.Drawing.Size(332, 118);
        	this.PlCarton.TabIndex = 49;
        	// 
        	// txtCartons
        	// 
        	this.txtCartons.AcceptsReturn = true;
        	this.txtCartons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtCartons.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.txtCartons.Location = new System.Drawing.Point(150, 5);
        	this.txtCartons.MaxLength = 0;
        	this.txtCartons.Multiline = true;
        	this.txtCartons.Name = "txtCartons";
        	this.txtCartons.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        	this.txtCartons.Size = new System.Drawing.Size(168, 106);
        	this.txtCartons.TabIndex = 52;
        	// 
        	// lblCartonNo
        	// 
        	this.lblCartonNo.AutoSize = true;
        	this.lblCartonNo.Location = new System.Drawing.Point(3, 8);
        	this.lblCartonNo.Name = "lblCartonNo";
        	this.lblCartonNo.Size = new System.Drawing.Size(64, 13);
        	this.lblCartonNo.TabIndex = 6;
        	this.lblCartonNo.Text = "内 部 托 号";
        	// 
        	// dgvData
        	// 
        	this.dgvData.AllowUserToAddRows = false;
        	this.dgvData.AllowUserToDeleteRows = false;
        	this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.colCommandName,
			this.colCartonNo,
			this.colCustomerCartonNo,
			this.colFinishedOn,
			this.colModuleColor,
			this.colModuleSn,
			this.colOrderNo,
			this.colSalesOrderNo,
			this.colSalesItemNo,
			this.colOrderStatus,
			this.colProductCode,
			this.colUnit,
			this.colFactory,
			this.colWorkshop,
			this.colPackingLocation,
			this.colPackingMode,
			this.colCellEff,
			this.colTestPower,
			this.colStdPower,
			this.colModuleGrade,
			this.colByIm,
			this.colTolerance,
			this.colCellCode,
			this.colCellBatch,
			this.colCellPrintMode,
			this.colGlassCode,
			this.colGlassBatch,
			this.colEvaCode,
			this.colEvaBatch,
			this.colTptCode,
			this.colTptBatch,
			this.colConboxCode,
			this.colConboxBatch,
			this.colConboxType,
			this.colLongFrameCode,
			this.colLongFrameBatch,
			this.colShortFrameCode,
			this.colShortFrameBatch,
			this.colGlassThickness,
			this.colIsRework,
			this.colIsByProduction});
        	this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dgvData.Location = new System.Drawing.Point(3, 143);
        	this.dgvData.Name = "dgvData";
        	this.dgvData.ReadOnly = true;
        	this.dgvData.RowTemplate.Height = 23;
        	this.dgvData.Size = new System.Drawing.Size(1046, 347);
        	this.dgvData.TabIndex = 1;
        	this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
        	// 
        	// colCommandName
        	// 
        	this.colCommandName.DataPropertyName = "CommandName";
        	this.colCommandName.HeaderText = "操作";
        	this.colCommandName.Name = "colCommandName";
        	this.colCommandName.ReadOnly = true;
        	this.colCommandName.Text = "Remove";
        	this.colCommandName.UseColumnTextForLinkValue = true;
        	this.colCommandName.Width = 60;
        	// 
        	// colCartonNo
        	// 
        	this.colCartonNo.DataPropertyName = "CartonNo";
        	this.colCartonNo.HeaderText = "内部托号";
        	this.colCartonNo.Name = "colCartonNo";
        	this.colCartonNo.ReadOnly = true;
        	this.colCartonNo.Width = 150;
        	// 
        	// colCustomerCartonNo
        	// 
        	this.colCustomerCartonNo.DataPropertyName = "CustomerCartonNo";
        	this.colCustomerCartonNo.HeaderText = "客户自定义托号";
        	this.colCustomerCartonNo.Name = "colCustomerCartonNo";
        	this.colCustomerCartonNo.ReadOnly = true;
        	this.colCustomerCartonNo.Width = 150;
        	// 
        	// colFinishedOn
        	// 
        	this.colFinishedOn.DataPropertyName = "FinishedOn";
        	this.colFinishedOn.HeaderText = "打托时间";
        	this.colFinishedOn.Name = "colFinishedOn";
        	this.colFinishedOn.ReadOnly = true;
        	this.colFinishedOn.Width = 150;
        	// 
        	// colModuleColor
        	// 
        	this.colModuleColor.DataPropertyName = "ModuleColor";
        	this.colModuleColor.HeaderText = "组件颜色";
        	this.colModuleColor.Name = "colModuleColor";
        	this.colModuleColor.ReadOnly = true;
        	this.colModuleColor.Width = 150;
        	// 
        	// colModuleSn
        	// 
        	this.colModuleSn.DataPropertyName = "ModuleSn";
        	this.colModuleSn.HeaderText = "组件序列号";
        	this.colModuleSn.Name = "colModuleSn";
        	this.colModuleSn.ReadOnly = true;
        	this.colModuleSn.Width = 150;
        	// 
        	// colOrderNo
        	// 
        	this.colOrderNo.DataPropertyName = "OrderNo";
        	this.colOrderNo.HeaderText = "生产订单号";
        	this.colOrderNo.Name = "colOrderNo";
        	this.colOrderNo.ReadOnly = true;
        	this.colOrderNo.Width = 150;
        	// 
        	// colSalesOrderNo
        	// 
        	this.colSalesOrderNo.DataPropertyName = "SalesOrderNo";
        	this.colSalesOrderNo.HeaderText = "销售订单";
        	this.colSalesOrderNo.Name = "colSalesOrderNo";
        	this.colSalesOrderNo.ReadOnly = true;
        	this.colSalesOrderNo.Width = 150;
        	// 
        	// colSalesItemNo
        	// 
        	this.colSalesItemNo.DataPropertyName = "SalesItemNo";
        	this.colSalesItemNo.HeaderText = "销售订单行项目";
        	this.colSalesItemNo.Name = "colSalesItemNo";
        	this.colSalesItemNo.ReadOnly = true;
        	this.colSalesItemNo.Width = 150;
        	// 
        	// colOrderStatus
        	// 
        	this.colOrderStatus.DataPropertyName = "OrderStatus";
        	this.colOrderStatus.HeaderText = "生产订单状态";
        	this.colOrderStatus.Name = "colOrderStatus";
        	this.colOrderStatus.ReadOnly = true;
        	this.colOrderStatus.Width = 150;
        	// 
        	// colProductCode
        	// 
        	this.colProductCode.DataPropertyName = "ProductCode";
        	this.colProductCode.HeaderText = "产品物料代码";
        	this.colProductCode.Name = "colProductCode";
        	this.colProductCode.ReadOnly = true;
        	this.colProductCode.Width = 150;
        	// 
        	// colUnit
        	// 
        	this.colUnit.DataPropertyName = "Unit";
        	this.colUnit.HeaderText = "单位";
        	this.colUnit.Name = "colUnit";
        	this.colUnit.ReadOnly = true;
        	this.colUnit.Width = 150;
        	// 
        	// colFactory
        	// 
        	this.colFactory.DataPropertyName = "Factory";
        	this.colFactory.HeaderText = "工厂";
        	this.colFactory.Name = "colFactory";
        	this.colFactory.ReadOnly = true;
        	this.colFactory.Width = 150;
        	// 
        	// colWorkshop
        	// 
        	this.colWorkshop.DataPropertyName = "Workshop";
        	this.colWorkshop.HeaderText = "车间";
        	this.colWorkshop.Name = "colWorkshop";
        	this.colWorkshop.ReadOnly = true;
        	this.colWorkshop.Width = 150;
        	// 
        	// colPackingLocation
        	// 
        	this.colPackingLocation.DataPropertyName = "PackingLocation";
        	this.colPackingLocation.HeaderText = "入库地点";
        	this.colPackingLocation.Name = "colPackingLocation";
        	this.colPackingLocation.ReadOnly = true;
        	this.colPackingLocation.Width = 150;
        	// 
        	// colPackingMode
        	// 
        	this.colPackingMode.DataPropertyName = "PackingMode";
        	this.colPackingMode.HeaderText = "包装方式";
        	this.colPackingMode.Name = "colPackingMode";
        	this.colPackingMode.ReadOnly = true;
        	this.colPackingMode.Width = 150;
        	// 
        	// colCellEff
        	// 
        	this.colCellEff.DataPropertyName = "CellEff";
        	this.colCellEff.HeaderText = "电池片转换效率";
        	this.colCellEff.Name = "colCellEff";
        	this.colCellEff.ReadOnly = true;
        	this.colCellEff.Width = 150;
        	// 
        	// colTestPower
        	// 
        	this.colTestPower.DataPropertyName = "TestPower";
        	this.colTestPower.HeaderText = "实测功率";
        	this.colTestPower.Name = "colTestPower";
        	this.colTestPower.ReadOnly = true;
        	this.colTestPower.Width = 150;
        	// 
        	// colStdPower
        	// 
        	this.colStdPower.DataPropertyName = "StdPower";
        	this.colStdPower.HeaderText = "标称功率";
        	this.colStdPower.Name = "colStdPower";
        	this.colStdPower.ReadOnly = true;
        	this.colStdPower.Width = 150;
        	// 
        	// colModuleGrade
        	// 
        	this.colModuleGrade.DataPropertyName = "ModuleGrade";
        	this.colModuleGrade.HeaderText = "组件等级";
        	this.colModuleGrade.Name = "colModuleGrade";
        	this.colModuleGrade.ReadOnly = true;
        	this.colModuleGrade.Width = 150;
        	// 
        	// colByIm
        	// 
        	this.colByIm.DataPropertyName = "ByIm";
        	this.colByIm.HeaderText = "电流分档";
        	this.colByIm.Name = "colByIm";
        	this.colByIm.ReadOnly = true;
        	this.colByIm.Width = 150;
        	// 
        	// colTolerance
        	// 
        	this.colTolerance.DataPropertyName = "Tolerance";
        	this.colTolerance.HeaderText = "公差";
        	this.colTolerance.Name = "colTolerance";
        	this.colTolerance.ReadOnly = true;
        	this.colTolerance.Width = 150;
        	// 
        	// colCellCode
        	// 
        	this.colCellCode.DataPropertyName = "CellCode";
        	this.colCellCode.HeaderText = "电池片物料号";
        	this.colCellCode.Name = "colCellCode";
        	this.colCellCode.ReadOnly = true;
        	this.colCellCode.Width = 150;
        	// 
        	// colCellBatch
        	// 
        	this.colCellBatch.DataPropertyName = "CellBatch";
        	this.colCellBatch.HeaderText = "电池片批次";
        	this.colCellBatch.Name = "colCellBatch";
        	this.colCellBatch.ReadOnly = true;
        	this.colCellBatch.Width = 150;
        	// 
        	// colCellPrintMode
        	// 
        	this.colCellPrintMode.DataPropertyName = "CellPrintMode";
        	this.colCellPrintMode.HeaderText = "电池片网版本";
        	this.colCellPrintMode.Name = "colCellPrintMode";
        	this.colCellPrintMode.ReadOnly = true;
        	this.colCellPrintMode.Width = 150;
        	// 
        	// colGlassCode
        	// 
        	this.colGlassCode.DataPropertyName = "GlassCode";
        	this.colGlassCode.HeaderText = "玻璃物料号";
        	this.colGlassCode.Name = "colGlassCode";
        	this.colGlassCode.ReadOnly = true;
        	this.colGlassCode.Width = 150;
        	// 
        	// colGlassBatch
        	// 
        	this.colGlassBatch.DataPropertyName = "GlassBatch";
        	this.colGlassBatch.HeaderText = "玻璃批次";
        	this.colGlassBatch.Name = "colGlassBatch";
        	this.colGlassBatch.ReadOnly = true;
        	this.colGlassBatch.Width = 150;
        	// 
        	// colEvaCode
        	// 
        	this.colEvaCode.DataPropertyName = "EvaCode";
        	this.colEvaCode.HeaderText = "EVA物料号";
        	this.colEvaCode.Name = "colEvaCode";
        	this.colEvaCode.ReadOnly = true;
        	this.colEvaCode.Width = 150;
        	// 
        	// colEvaBatch
        	// 
        	this.colEvaBatch.DataPropertyName = "EvaBatch";
        	this.colEvaBatch.HeaderText = "EVA批次";
        	this.colEvaBatch.Name = "colEvaBatch";
        	this.colEvaBatch.ReadOnly = true;
        	this.colEvaBatch.Width = 150;
        	// 
        	// colTptCode
        	// 
        	this.colTptCode.DataPropertyName = "TptCode";
        	this.colTptCode.HeaderText = "背板物料号";
        	this.colTptCode.Name = "colTptCode";
        	this.colTptCode.ReadOnly = true;
        	this.colTptCode.Width = 150;
        	// 
        	// colTptBatch
        	// 
        	this.colTptBatch.DataPropertyName = "TptBatch";
        	this.colTptBatch.HeaderText = "背板批次";
        	this.colTptBatch.Name = "colTptBatch";
        	this.colTptBatch.ReadOnly = true;
        	this.colTptBatch.Width = 150;
        	// 
        	// colConboxCode
        	// 
        	this.colConboxCode.DataPropertyName = "ConboxCode";
        	this.colConboxCode.HeaderText = "接线盒物料号";
        	this.colConboxCode.Name = "colConboxCode";
        	this.colConboxCode.ReadOnly = true;
        	this.colConboxCode.Width = 150;
        	// 
        	// colConboxBatch
        	// 
        	this.colConboxBatch.DataPropertyName = "ConboxBatch";
        	this.colConboxBatch.HeaderText = "接线盒批次";
        	this.colConboxBatch.Name = "colConboxBatch";
        	this.colConboxBatch.ReadOnly = true;
        	this.colConboxBatch.Width = 150;
        	// 
        	// colConboxType
        	// 
        	this.colConboxType.DataPropertyName = "ConboxType";
        	this.colConboxType.HeaderText = "接线盒类型";
        	this.colConboxType.Name = "colConboxType";
        	this.colConboxType.ReadOnly = true;
        	this.colConboxType.Width = 150;
        	// 
        	// colLongFrameCode
        	// 
        	this.colLongFrameCode.DataPropertyName = "LongFrameCode";
        	this.colLongFrameCode.HeaderText = "长边框物料号";
        	this.colLongFrameCode.Name = "colLongFrameCode";
        	this.colLongFrameCode.ReadOnly = true;
        	this.colLongFrameCode.Width = 150;
        	// 
        	// colLongFrameBatch
        	// 
        	this.colLongFrameBatch.DataPropertyName = "LongFrameBatch";
        	this.colLongFrameBatch.HeaderText = "长边框批次";
        	this.colLongFrameBatch.Name = "colLongFrameBatch";
        	this.colLongFrameBatch.ReadOnly = true;
        	this.colLongFrameBatch.Width = 150;
        	// 
        	// colShortFrameCode
        	// 
        	this.colShortFrameCode.DataPropertyName = "ShortFrameCode";
        	this.colShortFrameCode.HeaderText = "短边框物料号";
        	this.colShortFrameCode.Name = "colShortFrameCode";
        	this.colShortFrameCode.ReadOnly = true;
        	this.colShortFrameCode.Width = 150;
        	// 
        	// colShortFrameBatch
        	// 
        	this.colShortFrameBatch.DataPropertyName = "ShortFrameBatch";
        	this.colShortFrameBatch.HeaderText = "短边框批次";
        	this.colShortFrameBatch.Name = "colShortFrameBatch";
        	this.colShortFrameBatch.ReadOnly = true;
        	this.colShortFrameBatch.Width = 150;
        	// 
        	// colGlassThickness
        	// 
        	this.colGlassThickness.DataPropertyName = "GlassThickness";
        	this.colGlassThickness.HeaderText = "玻璃厚度";
        	this.colGlassThickness.Name = "colGlassThickness";
        	this.colGlassThickness.ReadOnly = true;
        	this.colGlassThickness.Width = 150;
        	// 
        	// colIsRework
        	// 
        	this.colIsRework.DataPropertyName = "IsRework";
        	this.colIsRework.HeaderText = "是否重工";
        	this.colIsRework.Name = "colIsRework";
        	this.colIsRework.ReadOnly = true;
        	this.colIsRework.Width = 150;
        	// 
        	// colIsByProduction
        	// 
        	this.colIsByProduction.DataPropertyName = "IsByProduction";
        	this.colIsByProduction.HeaderText = "是否副产品";
        	this.colIsByProduction.Name = "colIsByProduction";
        	this.colIsByProduction.ReadOnly = true;
        	this.colIsByProduction.Width = 150;
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lstView.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(3, 490);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(1046, 107);
        	this.lstView.TabIndex = 3;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// CbAuto
        	// 
        	this.CbAuto.Location = new System.Drawing.Point(739, 97);
        	this.CbAuto.Name = "CbAuto";
        	this.CbAuto.Size = new System.Drawing.Size(77, 26);
        	this.CbAuto.TabIndex = 60;
        	this.CbAuto.Text = "Auto";
        	this.CbAuto.UseVisualStyleBackColor = true;
        	this.CbAuto.CheckedChanged += new System.EventHandler(this.CbAutoCheckedChanged);
        	// 
        	// FrmSapInventoryUploadManul
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1052, 600);
        	this.Controls.Add(this.dgvData);
        	this.Controls.Add(this.pnlTop);
        	this.Controls.Add(this.lstView);
        	this.Name = "FrmSapInventoryUploadManul";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "入库上传SAP";
        	this.Load += new System.EventHandler(this.FrmSapInventoryUploadManul_Load);
        	this.pnlTop.ResumeLayout(false);
        	this.pnlTop.PerformLayout();
        	this.pnlJobNo.ResumeLayout(false);
        	this.pnlJobNo.PerformLayout();
        	this.PlCarton.ResumeLayout(false);
        	this.PlCarton.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
        	this.ResumeLayout(false);

        }
        private System.Windows.Forms.DateTimePicker DTpick;

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label lblCartonNo;
        private System.Windows.Forms.Panel PlCarton;
        private System.Windows.Forms.TextBox txtCartons;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Panel pnlJobNo;
        private System.Windows.Forms.TextBox txtJobNo;
        private System.Windows.Forms.Label lblJobNo;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.DataGridViewLinkColumn colCommandName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFinishedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModuleColor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModuleSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSalesOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSalesItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrderStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFactory;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWorkshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackingLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPackingMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCellEff;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTestPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStdPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModuleGrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn colByIm;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTolerance;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCellCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCellBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCellPrintMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGlassCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGlassBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEvaCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEvaBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTptCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTptBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConboxCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConboxBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colConboxType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLongFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLongFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShortFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colShortFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn colGlassThickness;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsRework;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIsByProduction;
        private System.Windows.Forms.Label lblTestPowerSumValue;
        private System.Windows.Forms.Label lblModuleCntValue;
        private System.Windows.Forms.Label lblTestPowerSum;
        private System.Windows.Forms.Label lblModuleCnt;
        private System.Windows.Forms.CheckBox CbTECO;
        private System.Windows.Forms.Button BtRetry;
        private System.Windows.Forms.CheckBox CbAuto;
    }
}