﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace CSICPR
{
    public class CenterDBCommond
    {

    }

    /// <summary>
    /// MODULE:实体类
    /// </summary>
    [Serializable]
    public class MODULE
    {
        public MODULE()
        { }
        #region Model
        private string _sysid = "";
        private string _module_sn = "";
        private string _barcode = "";
        private string _workshop = "";
        private string _work_order = "";
        private string _module_type = "";
        private string _product_no = "";
        private string _remark = "";
        private string _created_on = "";
        private string _created_by = "";
        private string _modified_on = "";
        private string _modified_by = "";
        private string _resv01 = "";
        private string _resv02 = "";
        private string _resv03 = "";
        private string _resv04 = "";
        private string _resv05 = "";

        private string _resv06 = "";
        /// <summary>
        /// 组件等级
        /// </summary>
        public string Resv06
        {
            get { return _resv06; }
            set { _resv06 = value; }
        }
        private string _custBoxID = "";

        /// <summary>
        /// 客户托号
        /// </summary>
        public string CustBoxID
        {
            get { return _custBoxID; }
            set { _custBoxID = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SYSID
        {
            set { _sysid = value; }
            get { return _sysid; }
        }
        /// <summary>
        /// 组件号码
        /// </summary>
        public string MODULE_SN
        {
            set { _module_sn = value; }
            get { return _module_sn; }
        }
        /// <summary>
        /// 组件号码
        /// </summary>
        public string BARCODE
        {
            set { _barcode = value; }
            get { return _barcode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WORKSHOP
        {
            set { _workshop = value; }
            get { return _workshop; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WORK_ORDER
        {
            set { _work_order = value; }
            get { return _work_order; }
        }
        /// <summary>
        /// 板型
        /// </summary>
        public string MODULE_TYPE
        {
            set { _module_type = value; }
            get { return _module_type; }
        }
        /// <summary>
        /// 产品编号
        /// </summary>
        public string PRODUCT_NO
        {
            set { _product_no = value; }
            get { return _product_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string REMARK
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_ON
        {
            set { _created_on = value; }
            get { return _created_on; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_ON
        {
            set { _modified_on = value; }
            get { return _modified_on; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_BY
        {
            set { _modified_by = value; }
            get { return _modified_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV01
        {
            set { _resv01 = value; }
            get { return _resv01; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV02
        {
            set { _resv02 = value; }
            get { return _resv02; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV03
        {
            set { _resv03 = value; }
            get { return _resv03; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV04
        {
            set { _resv04 = value; }
            get { return _resv04; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV05
        {
            set { _resv05 = value; }
            get { return _resv05; }
        }
        #endregion Model
    }

    /// <summary>
    /// 数据访问类:MODULE
    /// </summary>
    public class MODULEDAL
    {
        public MODULEDAL()
        { }
        #region  Method
        /// <summary>
        /// 根据组件号查询重号信息
        /// </summary>
        /// <param name="sn"></param>
        /// <returns></returns>
        public DataSet GetInfoRepSN(string sn)
        {

            string sql = @"
SELECT M.WORKSHOP,MC.CARTON_NO
FROM T_MODULE M
	INNER JOIN T_MODULE_PACKING_LIST MPL ON
		M.SYSID=MPL.MODULE_SYSID
	INNER JOIN T_MODULE_CARTON MC ON
		MC.SYSID=MPL.MODULE_CARTON_SYSID
WHERE M.MODULE_SN='{0}'";
            return DbHelperSQL.Query(string.Format(sql, sn));


        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string MODULE_SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE");
            strSql.Append(" where MODULE_SN=@MODULE_SN ");
            SqlParameter[] parameters = {
                    new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50)};
            parameters[0].Value = MODULE_SN;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 集中数据库检查重复， 0 没有做过包装 。 1 做过包装。  2 表示本车间的重工
        /// </summary>
        /// <param name="MODULE_SN"></param>
        /// <returns></returns>
        public string CheckRepeat(string MODULE_SN, string Work_Shop = "")
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE");
            strSql.Append(" where MODULE_SN=@MODULE_SN ");
            SqlParameter[] parameters = {
                    new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50)};
            parameters[0].Value = MODULE_SN;

            bool snIsHave = DbHelperSQL.Exists(strSql.ToString(), parameters);
            if (snIsHave)
            {
                if (Work_Shop.ToUpper().Equals("M08"))
                {
                    string checkSql = @"SELECT * FROM T_MODULE where MODULE_SN='{0}'";
                    using (SqlDataReader reader = ToolsClass.GetDataReader(string.Format(checkSql, MODULE_SN), DbHelperSQL.sqlConn))
                    {
                        if (reader != null && reader.Read())
                        {
                            if (reader.IsDBNull(3))
                                return "3";
                        }
                        return "2";
                    }
                }
                else
                {
                    string checkSql = @" SELECT * FROM T_MODULE SN
                                INNER JOIN T_MODULE_PACKING_LIST LK
                                ON LK.MODULE_SYSID=SN.SYSID
                                INNER JOIN T_MODULE_CARTON CARTON
                                ON LK.MODULE_CARTON_SYSID=CARTON.SYSID  
                                WHERE SN.MODULE_SN='{0}' ";
                    checkSql = string.Format(checkSql, MODULE_SN);
                    object obj = DbHelperSQL.ExecuteScalar(checkSql, null);
                    if (obj == null || obj == DBNull.Value)
                    {
                        return "2";
                    }
                    else
                    {
                        return "1";
                    }
                }
            }
            else
            {
                return "0";
            }

        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(MODULE model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into T_MODULE(");
            strSql.Append("SYSID,MODULE_SN,BARCODE,WORKSHOP,WORK_ORDER,MODULE_TYPE,PRODUCT_NO,REMARK,CREATED_ON,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05)");
            strSql.Append(" values (");
            strSql.Append("@SYSID,@MODULE_SN,@BARCODE,@WORKSHOP,@WORK_ORDER,@MODULE_TYPE,@PRODUCT_NO,@REMARK,@CREATED_ON,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05)");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@BARCODE", SqlDbType.NVarChar,50),
					new SqlParameter("@WORKSHOP", SqlDbType.NVarChar,50),
					new SqlParameter("@WORK_ORDER", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_TYPE", SqlDbType.NVarChar,50),
					new SqlParameter("@PRODUCT_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100)};
            parameters[0].Value = model.SYSID;
            parameters[1].Value = model.MODULE_SN;
            parameters[2].Value = model.BARCODE;
            parameters[3].Value = model.WORKSHOP;
            parameters[4].Value = model.WORK_ORDER;
            parameters[5].Value = model.MODULE_TYPE;
            parameters[6].Value = model.PRODUCT_NO;
            parameters[7].Value = model.REMARK;
            parameters[8].Value = model.CREATED_ON;
            parameters[9].Value = model.CREATED_BY;
            parameters[10].Value = model.MODIFIED_ON;
            parameters[11].Value = model.MODIFIED_BY;
            parameters[12].Value = model.RESV01;
            parameters[13].Value = model.RESV02;
            parameters[14].Value = model.RESV03;
            parameters[15].Value = model.RESV04;
            parameters[16].Value = model.RESV05;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULE model)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into {0}.dbo.T_MODULE(");
                strSql.Append("SYSID,MODULE_SN,BARCODE,WORKSHOP,WORK_ORDER,MODULE_TYPE,PRODUCT_NO,REMARK,CREATED_ON,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05)");
                strSql.Append(" values (");
                strSql.Append("@SYSID,@MODULE_SN,@BARCODE,@WORKSHOP,@WORK_ORDER,@MODULE_TYPE,@PRODUCT_NO,@REMARK,@CREATED_ON,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05)");
                SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@BARCODE", SqlDbType.NVarChar,50),
					new SqlParameter("@WORKSHOP", SqlDbType.NVarChar,50),
					new SqlParameter("@WORK_ORDER", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_TYPE", SqlDbType.NVarChar,50),
					new SqlParameter("@PRODUCT_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100)};
                parameters[0].Value = model.SYSID;
                parameters[1].Value = model.MODULE_SN;
                parameters[2].Value = model.BARCODE;
                parameters[3].Value = model.WORKSHOP;
                parameters[4].Value = model.WORK_ORDER;
                parameters[5].Value = model.MODULE_TYPE;
                parameters[6].Value = model.PRODUCT_NO;
                parameters[7].Value = model.REMARK;
                parameters[8].Value = model.CREATED_ON;
                parameters[9].Value = model.CREATED_BY;
                parameters[10].Value = model.MODIFIED_ON;
                parameters[11].Value = model.MODIFIED_BY;
                parameters[12].Value = model.RESV01;
                parameters[13].Value = model.RESV02;
                parameters[14].Value = model.RESV03;
                parameters[15].Value = model.RESV04;
                parameters[16].Value = model.RESV05;

                string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                return 0;
            }
            //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(MODULE model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update T_MODULE set ");
            strSql.Append("MODULE_SN=@MODULE_SN,");
            strSql.Append("BARCODE=@BARCODE,");
            strSql.Append("WORKSHOP=@WORKSHOP,");
            strSql.Append("WORK_ORDER=@WORK_ORDER,");
            strSql.Append("MODULE_TYPE=@MODULE_TYPE,");
            strSql.Append("PRODUCT_NO=@PRODUCT_NO,");
            strSql.Append("REMARK=@REMARK,");
            strSql.Append("CREATED_ON=@CREATED_ON,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("MODIFIED_ON=@MODIFIED_ON,");
            strSql.Append("MODIFIED_BY=@MODIFIED_BY,");
            strSql.Append("RESV01=@RESV01,");
            strSql.Append("RESV02=@RESV02,");
            strSql.Append("RESV03=@RESV03,");
            strSql.Append("RESV04=@RESV04,");
            strSql.Append("RESV05=@RESV05");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@BARCODE", SqlDbType.NVarChar,50),
					new SqlParameter("@WORKSHOP", SqlDbType.NVarChar,50),
					new SqlParameter("@WORK_ORDER", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_TYPE", SqlDbType.NVarChar,50),
					new SqlParameter("@PRODUCT_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.MODULE_SN;
            parameters[1].Value = model.BARCODE;
            parameters[2].Value = model.WORKSHOP;
            parameters[3].Value = model.WORK_ORDER;
            parameters[4].Value = model.MODULE_TYPE;
            parameters[5].Value = model.PRODUCT_NO;
            parameters[6].Value = model.REMARK;
            parameters[7].Value = model.CREATED_ON;
            parameters[8].Value = model.CREATED_BY;
            parameters[9].Value = model.MODIFIED_ON;
            parameters[10].Value = model.MODIFIED_BY;
            parameters[11].Value = model.RESV01;
            parameters[12].Value = model.RESV02;
            parameters[13].Value = model.RESV03;
            parameters[14].Value = model.RESV04;
            parameters[15].Value = model.RESV05;
            parameters[16].Value = model.SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string SYSID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE ");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SYSIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE ");
            strSql.Append(" where SYSID in (" + SYSIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion  Method
    }

    /// <summary>
    /// MODULE_CARTON:实体类
    /// </summary>
    [Serializable]
    public class MODULE_CARTON
    {
        public MODULE_CARTON()
        { }
        #region Model
        private string _sysid;
        private string _carton_no = "";
        private string _std_power_level = "";
        private string _internal_job_no = "";
        private string _ship_job_no = "";
        private string _created_no = "";
        private string _created_by = "";
        private string _modified_no = "";
        private string _modified_by = "";
        private string _remark = "";
        private string _resv01 = "";
        private string _resv02 = "";
        private string _resv03 = "";
        private string _resv04 = "";
        private string _resv05 = "";
        private string _resv06 = "";
        private string _resv07 = "";
        private string _resv08 = "";
        private string _resv09 = "";
        private string _resv010 = "";
        /// <summary>
        /// 
        /// </summary>
        public string SYSID
        {
            set { _sysid = value; }
            get { return _sysid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CARTON_NO
        {
            set { _carton_no = value; }
            get { return _carton_no; }
        }
        /// <summary>
        /// 标称功率
        /// </summary>
        public string STD_POWER_LEVEL
        {
            set { _std_power_level = value; }
            get { return _std_power_level; }
        }
        /// <summary>
        /// 内部柜号

        /// </summary>
        public string INTERNAL_JOB_NO
        {
            set { _internal_job_no = value; }
            get { return _internal_job_no; }
        }
        /// <summary>
        /// 出货编号
        /// </summary>
        public string SHIP_JOB_NO
        {
            set { _ship_job_no = value; }
            get { return _ship_job_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_NO
        {
            set { _created_no = value; }
            get { return _created_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_NO
        {
            set { _modified_no = value; }
            get { return _modified_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_BY
        {
            set { _modified_by = value; }
            get { return _modified_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string REMARK
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV01
        {
            set { _resv01 = value; }
            get { return _resv01; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV02
        {
            set { _resv02 = value; }
            get { return _resv02; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV03
        {
            set { _resv03 = value; }
            get { return _resv03; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV04
        {
            set { _resv04 = value; }
            get { return _resv04; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV05
        {
            set { _resv05 = value; }
            get { return _resv05; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV06
        {
            set { _resv06 = value; }
            get { return _resv06; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV07
        {
            set { _resv07 = value; }
            get { return _resv07; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV08
        {
            set { _resv08 = value; }
            get { return _resv08; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV09
        {
            set { _resv09 = value; }
            get { return _resv09; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV010
        {
            set { _resv010 = value; }
            get { return _resv010; }
        }
        #endregion Model

    }

    /// <summary>
    /// 数据访问类:MODULE_CARTON
    /// </summary>
    public class MODULE_CARTONDAL
    {
        public MODULE_CARTONDAL()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CARTON_NO)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE_CARTON");
            strSql.Append(" where CARTON_NO=@CARTON_NO ");
            SqlParameter[] parameters = {
					new SqlParameter("@CARTON_NO", SqlDbType.NVarChar,50)};
            parameters[0].Value = CARTON_NO;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(MODULE_CARTON model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into T_MODULE_CARTON(");
            strSql.Append("SYSID,CARTON_NO,STD_POWER_LEVEL,INTERNAL_JOB_NO,SHIP_JOB_NO,CREATED_NO,CREATED_BY,MODIFIED_NO,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV010)");
            strSql.Append(" values (");
            strSql.Append("@SYSID,@CARTON_NO,@STD_POWER_LEVEL,@INTERNAL_JOB_NO,@SHIP_JOB_NO,@CREATED_NO,@CREATED_BY,@MODIFIED_NO,@MODIFIED_BY,@REMARK,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV010)");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@CARTON_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@STD_POWER_LEVEL", SqlDbType.NVarChar,50),
					new SqlParameter("@INTERNAL_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@SHIP_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV07", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV08", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV09", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV010", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.SYSID;
            parameters[1].Value = model.CARTON_NO;
            parameters[2].Value = model.STD_POWER_LEVEL;
            parameters[3].Value = model.INTERNAL_JOB_NO;
            parameters[4].Value = model.SHIP_JOB_NO;
            parameters[5].Value = model.CREATED_NO;
            parameters[6].Value = model.CREATED_BY;
            parameters[7].Value = model.MODIFIED_NO;
            parameters[8].Value = model.MODIFIED_BY;
            parameters[9].Value = model.REMARK;
            parameters[10].Value = model.RESV01;
            parameters[11].Value = model.RESV02;
            parameters[12].Value = model.RESV03;
            parameters[13].Value = model.RESV04;
            parameters[14].Value = model.RESV05;
            parameters[15].Value = model.RESV06;
            parameters[16].Value = model.RESV07;
            parameters[17].Value = model.RESV08;
            parameters[18].Value = model.RESV09;
            parameters[19].Value = model.RESV010;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULE_CARTON model)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into {0}.dbo.T_MODULE_CARTON(");
                strSql.Append("SYSID,CARTON_NO,STD_POWER_LEVEL,INTERNAL_JOB_NO,SHIP_JOB_NO,CREATED_NO,CREATED_BY,MODIFIED_NO,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV010)");
                strSql.Append(" values (");
                strSql.Append("@SYSID,@CARTON_NO,@STD_POWER_LEVEL,@INTERNAL_JOB_NO,@SHIP_JOB_NO,@CREATED_NO,@CREATED_BY,@MODIFIED_NO,@MODIFIED_BY,@REMARK,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV010)");
                SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@CARTON_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@STD_POWER_LEVEL", SqlDbType.NVarChar,50),
					new SqlParameter("@INTERNAL_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@SHIP_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV07", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV08", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV09", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV010", SqlDbType.NVarChar,50)};
                parameters[0].Value = model.SYSID;
                parameters[1].Value = model.CARTON_NO;
                parameters[2].Value = model.STD_POWER_LEVEL;
                parameters[3].Value = model.INTERNAL_JOB_NO;
                parameters[4].Value = model.SHIP_JOB_NO;
                parameters[5].Value = model.CREATED_NO;
                parameters[6].Value = model.CREATED_BY;
                parameters[7].Value = model.MODIFIED_NO;
                parameters[8].Value = model.MODIFIED_BY;
                parameters[9].Value = model.REMARK;
                parameters[10].Value = model.RESV01;
                parameters[11].Value = model.RESV02;
                parameters[12].Value = model.RESV03;
                parameters[13].Value = model.RESV04;
                parameters[14].Value = model.RESV05;
                parameters[15].Value = model.RESV06;
                parameters[16].Value = model.RESV07;
                parameters[17].Value = model.RESV08;
                parameters[18].Value = model.RESV09;
                parameters[19].Value = model.RESV010;

                string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                return 0;
            }
            //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(MODULE_CARTON model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update T_MODULE_CARTON set ");
            strSql.Append("CARTON_NO=@CARTON_NO,");
            strSql.Append("STD_POWER_LEVEL=@STD_POWER_LEVEL,");
            strSql.Append("INTERNAL_JOB_NO=@INTERNAL_JOB_NO,");
            strSql.Append("SHIP_JOB_NO=@SHIP_JOB_NO,");
            strSql.Append("CREATED_NO=@CREATED_NO,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("MODIFIED_NO=@MODIFIED_NO,");
            strSql.Append("MODIFIED_BY=@MODIFIED_BY,");
            strSql.Append("REMARK=@REMARK,");
            strSql.Append("RESV01=@RESV01,");
            strSql.Append("RESV02=@RESV02,");
            strSql.Append("RESV03=@RESV03,");
            strSql.Append("RESV04=@RESV04,");
            strSql.Append("RESV05=@RESV05,");
            strSql.Append("RESV06=@RESV06,");
            strSql.Append("RESV07=@RESV07,");
            strSql.Append("RESV08=@RESV08,");
            strSql.Append("RESV09=@RESV09,");
            strSql.Append("RESV010=@RESV010");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CARTON_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@STD_POWER_LEVEL", SqlDbType.NVarChar,50),
					new SqlParameter("@INTERNAL_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@SHIP_JOB_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV07", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV08", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV09", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV010", SqlDbType.NVarChar,50),
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.CARTON_NO;
            parameters[1].Value = model.STD_POWER_LEVEL;
            parameters[2].Value = model.INTERNAL_JOB_NO;
            parameters[3].Value = model.SHIP_JOB_NO;
            parameters[4].Value = model.CREATED_NO;
            parameters[5].Value = model.CREATED_BY;
            parameters[6].Value = model.MODIFIED_NO;
            parameters[7].Value = model.MODIFIED_BY;
            parameters[8].Value = model.REMARK;
            parameters[9].Value = model.RESV01;
            parameters[10].Value = model.RESV02;
            parameters[11].Value = model.RESV03;
            parameters[12].Value = model.RESV04;
            parameters[13].Value = model.RESV05;
            parameters[14].Value = model.RESV06;
            parameters[15].Value = model.RESV07;
            parameters[16].Value = model.RESV08;
            parameters[17].Value = model.RESV09;
            parameters[18].Value = model.RESV010;
            parameters[19].Value = model.SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string SYSID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_CARTON ");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SYSIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_CARTON ");
            strSql.Append(" where SYSID in (" + SYSIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public MODULE_CARTON GetModel(string SYSID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SYSID,CARTON_NO,STD_POWER_LEVEL,INTERNAL_JOB_NO,SHIP_JOB_NO,CREATED_NO,CREATED_BY,MODIFIED_NO,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV010 from T_MODULE_CARTON ");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            MODULE_CARTON model = new MODULE_CARTON();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SYSID"] != null && ds.Tables[0].Rows[0]["SYSID"].ToString() != "")
                {
                    model.SYSID = ds.Tables[0].Rows[0]["SYSID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CARTON_NO"] != null && ds.Tables[0].Rows[0]["CARTON_NO"].ToString() != "")
                {
                    model.CARTON_NO = ds.Tables[0].Rows[0]["CARTON_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["STD_POWER_LEVEL"] != null && ds.Tables[0].Rows[0]["STD_POWER_LEVEL"].ToString() != "")
                {
                    model.STD_POWER_LEVEL = ds.Tables[0].Rows[0]["STD_POWER_LEVEL"].ToString();
                }
                if (ds.Tables[0].Rows[0]["INTERNAL_JOB_NO"] != null && ds.Tables[0].Rows[0]["INTERNAL_JOB_NO"].ToString() != "")
                {
                    model.INTERNAL_JOB_NO = ds.Tables[0].Rows[0]["INTERNAL_JOB_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SHIP_JOB_NO"] != null && ds.Tables[0].Rows[0]["SHIP_JOB_NO"].ToString() != "")
                {
                    model.SHIP_JOB_NO = ds.Tables[0].Rows[0]["SHIP_JOB_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_NO"] != null && ds.Tables[0].Rows[0]["CREATED_NO"].ToString() != "")
                {
                    model.CREATED_NO = ds.Tables[0].Rows[0]["CREATED_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MODIFIED_NO"] != null && ds.Tables[0].Rows[0]["MODIFIED_NO"].ToString() != "")
                {
                    model.MODIFIED_NO = ds.Tables[0].Rows[0]["MODIFIED_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MODIFIED_BY"] != null && ds.Tables[0].Rows[0]["MODIFIED_BY"].ToString() != "")
                {
                    model.MODIFIED_BY = ds.Tables[0].Rows[0]["MODIFIED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["REMARK"] != null && ds.Tables[0].Rows[0]["REMARK"].ToString() != "")
                {
                    model.REMARK = ds.Tables[0].Rows[0]["REMARK"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV01"] != null && ds.Tables[0].Rows[0]["RESV01"].ToString() != "")
                {
                    model.RESV01 = ds.Tables[0].Rows[0]["RESV01"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV02"] != null && ds.Tables[0].Rows[0]["RESV02"].ToString() != "")
                {
                    model.RESV02 = ds.Tables[0].Rows[0]["RESV02"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV03"] != null && ds.Tables[0].Rows[0]["RESV03"].ToString() != "")
                {
                    model.RESV03 = ds.Tables[0].Rows[0]["RESV03"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV04"] != null && ds.Tables[0].Rows[0]["RESV04"].ToString() != "")
                {
                    model.RESV04 = ds.Tables[0].Rows[0]["RESV04"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV05"] != null && ds.Tables[0].Rows[0]["RESV05"].ToString() != "")
                {
                    model.RESV05 = ds.Tables[0].Rows[0]["RESV05"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV06"] != null && ds.Tables[0].Rows[0]["RESV06"].ToString() != "")
                {
                    model.RESV06 = ds.Tables[0].Rows[0]["RESV06"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV07"] != null && ds.Tables[0].Rows[0]["RESV07"].ToString() != "")
                {
                    model.RESV07 = ds.Tables[0].Rows[0]["RESV07"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV08"] != null && ds.Tables[0].Rows[0]["RESV08"].ToString() != "")
                {
                    model.RESV08 = ds.Tables[0].Rows[0]["RESV08"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV09"] != null && ds.Tables[0].Rows[0]["RESV09"].ToString() != "")
                {
                    model.RESV09 = ds.Tables[0].Rows[0]["RESV09"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV010"] != null && ds.Tables[0].Rows[0]["RESV010"].ToString() != "")
                {
                    model.RESV010 = ds.Tables[0].Rows[0]["RESV010"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SYSID,CARTON_NO,STD_POWER_LEVEL,INTERNAL_JOB_NO,SHIP_JOB_NO,CREATED_NO,CREATED_BY,MODIFIED_NO,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV010 ");
            strSql.Append(" FROM T_MODULE_CARTON ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  Method
    }

    /// <summary>
    /// MODULE_PACKING_LIST:实体类
    /// </summary>
    [Serializable]
    public class MODULE_PACKING_LIST
    {
        public MODULE_PACKING_LIST()
        { }
        #region Model
        private string _module_sysid;
        private string _module_carton_sysid;
        /// <summary>
        /// 
        /// </summary>
        public string MODULE_SYSID
        {
            set { _module_sysid = value; }
            get { return _module_sysid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODULE_CARTON_SYSID
        {
            set { _module_carton_sysid = value; }
            get { return _module_carton_sysid; }
        }
        #endregion Model
    }

    /// <summary>
    /// 数据访问类:MODULE_PACKING_LIST
    /// </summary>
    public partial class MODULE_PACKING_LISTDAL
    {
        public MODULE_PACKING_LISTDAL()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(MODULE_PACKING_LIST model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into T_MODULE_PACKING_LIST(");
            strSql.Append("MODULE_SYSID,MODULE_CARTON_SYSID)");
            strSql.Append(" values (");
            strSql.Append("@MODULE_SYSID,@MODULE_CARTON_SYSID)");
            SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_CARTON_SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.MODULE_SYSID;
            parameters[1].Value = model.MODULE_CARTON_SYSID;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULE_PACKING_LIST model)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into {0}.dbo.T_MODULE_PACKING_LIST(");
                strSql.Append("MODULE_SYSID,MODULE_CARTON_SYSID)");
                strSql.Append(" values (");
                strSql.Append("@MODULE_SYSID,@MODULE_CARTON_SYSID)");
                SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_CARTON_SYSID", SqlDbType.NVarChar,50)};
                parameters[0].Value = model.MODULE_SYSID;
                parameters[1].Value = model.MODULE_CARTON_SYSID;

                string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                return 0;
            }
            //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(MODULE_PACKING_LIST model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update T_MODULE_PACKING_LIST set ");
            strSql.Append("MODULE_SYSID=@MODULE_SYSID,");
            strSql.Append("MODULE_CARTON_SYSID=@MODULE_CARTON_SYSID");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_CARTON_SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.MODULE_SYSID;
            parameters[1].Value = model.MODULE_CARTON_SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {
            //该表无主键信息，请自定义主键/条件字段
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_PACKING_LIST ");
            strSql.Append(" where ");
            SqlParameter[] parameters = {
};

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select MODULE_SYSID,MODULE_CARTON_SYSID ");
            strSql.Append(" FROM T_MODULE_PACKING_LIST ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" MODULE_SYSID,MODULE_CARTON_SYSID ");
            strSql.Append(" FROM T_MODULE_PACKING_LIST ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }

    /// <summary>
    /// MODULE_TEST:实体类
    /// </summary>
    [Serializable]
    public partial class MODULE_TEST
    {
        public MODULE_TEST()
        { }
        #region Model
        private string _sysid;
        private string _module_sn = "";
        private string _temp = "";
        private string _isc = "";
        private string _voc = "";
        private string _imp = "";
        private string _vmp = "";
        private string _pmax = "";
        private string _ff = "";
        private string _eff = "";
        private string _test_date = "";
        private string _created_no = "";
        private string _created_by = "";
        private string _modified_on = "";
        private string _modified_by = "";
        private string _remark = "";
        private string _resv01 = "";
        private string _resv02 = "";
        private string _resv03 = "";
        private string _resv04 = "";
        private string _resv05 = "";
        /// <summary>
        /// 
        /// </summary>
        public string SYSID
        {
            set { _sysid = value; }
            get { return _sysid; }
        }
        /// <summary>
        /// 组件序列号
        /// </summary>
        public string MODULE_SN
        {
            set { _module_sn = value; }
            get { return _module_sn; }
        }
        /// <summary>
        /// 温度
        /// </summary>
        public string TEMP
        {
            set { _temp = value; }
            get { return _temp; }
        }
        /// <summary>
        /// 开路电流
        /// </summary>
        public string ISC
        {
            set { _isc = value; }
            get { return _isc; }
        }
        /// <summary>
        /// 开路电压
        /// </summary>
        public string VOC
        {
            set { _voc = value; }
            get { return _voc; }
        }
        /// <summary>
        /// 短路电流
        /// </summary>
        public string IMP
        {
            set { _imp = value; }
            get { return _imp; }
        }
        /// <summary>
        /// 短路电压
        /// </summary>
        public string VMP
        {
            set { _vmp = value; }
            get { return _vmp; }
        }
        /// <summary>
        /// 实测功率
        /// </summary>
        public string PMAX
        {
            set { _pmax = value; }
            get { return _pmax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FF
        {
            set { _ff = value; }
            get { return _ff; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EFF
        {
            set { _eff = value; }
            get { return _eff; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TEST_DATE
        {
            set { _test_date = value; }
            get { return _test_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_NO
        {
            set { _created_no = value; }
            get { return _created_no; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_ON
        {
            set { _modified_on = value; }
            get { return _modified_on; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MODIFIED_BY
        {
            set { _modified_by = value; }
            get { return _modified_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string REMARK
        {
            set { _remark = value; }
            get { return _remark; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV01
        {
            set { _resv01 = value; }
            get { return _resv01; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV02
        {
            set { _resv02 = value; }
            get { return _resv02; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV03
        {
            set { _resv03 = value; }
            get { return _resv03; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV04
        {
            set { _resv04 = value; }
            get { return _resv04; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RESV05
        {
            set { _resv05 = value; }
            get { return _resv05; }
        }
        #endregion Model

    }
    /// <summary>
    /// 数据访问类:MODULE_TEST
    /// </summary>
    public partial class MODULE_TESTDAL
    {
        public MODULE_TESTDAL()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string SYSID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE_TEST");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public void Add(MODULE_TEST model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into T_MODULE_TEST(");
            strSql.Append("SYSID,MODULE_SN,TEMP,ISC,VOC,IMP,VMP,PMAX,FF,EFF,TEST_DATE,CREATED_NO,CREATED_BY,MODIFIED_ON,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05)");
            strSql.Append(" values (");
            strSql.Append("@SYSID,@MODULE_SN,@TEMP,@ISC,@VOC,@IMP,@VMP,@PMAX,@FF,@EFF,@TEST_DATE,@CREATED_NO,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@REMARK,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05)");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@TEMP", SqlDbType.NVarChar,50),
					new SqlParameter("@ISC", SqlDbType.NVarChar,50),
					new SqlParameter("@VOC", SqlDbType.NVarChar,50),
					new SqlParameter("@IMP", SqlDbType.NVarChar,50),
					new SqlParameter("@VMP", SqlDbType.NVarChar,50),
					new SqlParameter("@PMAX", SqlDbType.NVarChar,50),
					new SqlParameter("@FF", SqlDbType.NVarChar,50),
					new SqlParameter("@EFF", SqlDbType.NVarChar,50),
					new SqlParameter("@TEST_DATE", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100)};
            parameters[0].Value = model.SYSID;
            parameters[1].Value = model.MODULE_SN;
            parameters[2].Value = model.TEMP;
            parameters[3].Value = model.ISC;
            parameters[4].Value = model.VOC;
            parameters[5].Value = model.IMP;
            parameters[6].Value = model.VMP;
            parameters[7].Value = model.PMAX;
            parameters[8].Value = model.FF;
            parameters[9].Value = model.EFF;
            parameters[10].Value = model.TEST_DATE;
            parameters[11].Value = model.CREATED_NO;
            parameters[12].Value = model.CREATED_BY;
            parameters[13].Value = model.MODIFIED_ON;
            parameters[14].Value = model.MODIFIED_BY;
            parameters[15].Value = model.REMARK;
            parameters[16].Value = model.RESV01;
            parameters[17].Value = model.RESV02;
            parameters[18].Value = model.RESV03;
            parameters[19].Value = model.RESV04;
            parameters[20].Value = model.RESV05;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULE_TEST model)
        {
            try
            {
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into {0}.dbo.T_MODULE_TEST(");
                strSql.Append("SYSID,MODULE_SN,TEMP,ISC,VOC,IMP,VMP,PMAX,FF,EFF,TEST_DATE,CREATED_NO,CREATED_BY,MODIFIED_ON,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05)");
                strSql.Append(" values (");
                strSql.Append("@SYSID,@MODULE_SN,@TEMP,@ISC,@VOC,@IMP,@VMP,@PMAX,@FF,@EFF,@TEST_DATE,@CREATED_NO,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@REMARK,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05)");
                SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@TEMP", SqlDbType.NVarChar,50),
					new SqlParameter("@ISC", SqlDbType.NVarChar,50),
					new SqlParameter("@VOC", SqlDbType.NVarChar,50),
					new SqlParameter("@IMP", SqlDbType.NVarChar,50),
					new SqlParameter("@VMP", SqlDbType.NVarChar,50),
					new SqlParameter("@PMAX", SqlDbType.NVarChar,50),
					new SqlParameter("@FF", SqlDbType.NVarChar,50),
					new SqlParameter("@EFF", SqlDbType.NVarChar,50),
					new SqlParameter("@TEST_DATE", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100)};
                parameters[0].Value = model.SYSID;
                parameters[1].Value = model.MODULE_SN;
                parameters[2].Value = model.TEMP;
                parameters[3].Value = model.ISC;
                parameters[4].Value = model.VOC;
                parameters[5].Value = model.IMP;
                parameters[6].Value = model.VMP;
                parameters[7].Value = model.PMAX;
                parameters[8].Value = model.FF;
                parameters[9].Value = model.EFF;
                parameters[10].Value = model.TEST_DATE;
                parameters[11].Value = model.CREATED_NO;
                parameters[12].Value = model.CREATED_BY;
                parameters[13].Value = model.MODIFIED_ON;
                parameters[14].Value = model.MODIFIED_BY;
                parameters[15].Value = model.REMARK;
                parameters[16].Value = model.RESV01;
                parameters[17].Value = model.RESV02;
                parameters[18].Value = model.RESV03;
                parameters[19].Value = model.RESV04;
                parameters[20].Value = model.RESV05;

                string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                return 0;
            }
            //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(MODULE_TEST model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update T_MODULE_TEST set ");
            strSql.Append("MODULE_SN=@MODULE_SN,");
            strSql.Append("TEMP=@TEMP,");
            strSql.Append("ISC=@ISC,");
            strSql.Append("VOC=@VOC,");
            strSql.Append("IMP=@IMP,");
            strSql.Append("VMP=@VMP,");
            strSql.Append("PMAX=@PMAX,");
            strSql.Append("FF=@FF,");
            strSql.Append("EFF=@EFF,");
            strSql.Append("TEST_DATE=@TEST_DATE,");
            strSql.Append("CREATED_NO=@CREATED_NO,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("MODIFIED_ON=@MODIFIED_ON,");
            strSql.Append("MODIFIED_BY=@MODIFIED_BY,");
            strSql.Append("REMARK=@REMARK,");
            strSql.Append("RESV01=@RESV01,");
            strSql.Append("RESV02=@RESV02,");
            strSql.Append("RESV03=@RESV03,");
            strSql.Append("RESV04=@RESV04,");
            strSql.Append("RESV05=@RESV05");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
					new SqlParameter("@TEMP", SqlDbType.NVarChar,50),
					new SqlParameter("@ISC", SqlDbType.NVarChar,50),
					new SqlParameter("@VOC", SqlDbType.NVarChar,50),
					new SqlParameter("@IMP", SqlDbType.NVarChar,50),
					new SqlParameter("@VMP", SqlDbType.NVarChar,50),
					new SqlParameter("@PMAX", SqlDbType.NVarChar,50),
					new SqlParameter("@FF", SqlDbType.NVarChar,50),
					new SqlParameter("@EFF", SqlDbType.NVarChar,50),
					new SqlParameter("@TEST_DATE", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_NO", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,50),
					new SqlParameter("@REMARK", SqlDbType.NVarChar,50),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.MODULE_SN;
            parameters[1].Value = model.TEMP;
            parameters[2].Value = model.ISC;
            parameters[3].Value = model.VOC;
            parameters[4].Value = model.IMP;
            parameters[5].Value = model.VMP;
            parameters[6].Value = model.PMAX;
            parameters[7].Value = model.FF;
            parameters[8].Value = model.EFF;
            parameters[9].Value = model.TEST_DATE;
            parameters[10].Value = model.CREATED_NO;
            parameters[11].Value = model.CREATED_BY;
            parameters[12].Value = model.MODIFIED_ON;
            parameters[13].Value = model.MODIFIED_BY;
            parameters[14].Value = model.REMARK;
            parameters[15].Value = model.RESV01;
            parameters[16].Value = model.RESV02;
            parameters[17].Value = model.RESV03;
            parameters[18].Value = model.RESV04;
            parameters[19].Value = model.RESV05;
            parameters[20].Value = model.SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string SYSID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_TEST ");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBySN(string sn)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_TEST ");
            strSql.Append(" where MODULE_SN=@MODULE_SN ");
            SqlParameter[] parameters = {
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50)};
            parameters[0].Value = sn;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SYSIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from T_MODULE_TEST ");
            strSql.Append(" where SYSID in (" + SYSIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public MODULE_TEST GetModel(string SYSID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SYSID,MODULE_SN,TEMP,ISC,VOC,IMP,VMP,PMAX,FF,EFF,TEST_DATE,CREATED_NO,CREATED_BY,MODIFIED_ON,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05 from T_MODULE_TEST ");
            strSql.Append(" where SYSID=@SYSID ");
            SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50)};
            parameters[0].Value = SYSID;

            MODULE_TEST model = new MODULE_TEST();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SYSID"] != null && ds.Tables[0].Rows[0]["SYSID"].ToString() != "")
                {
                    model.SYSID = ds.Tables[0].Rows[0]["SYSID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MODULE_SN"] != null && ds.Tables[0].Rows[0]["MODULE_SN"].ToString() != "")
                {
                    model.MODULE_SN = ds.Tables[0].Rows[0]["MODULE_SN"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TEMP"] != null && ds.Tables[0].Rows[0]["TEMP"].ToString() != "")
                {
                    model.TEMP = ds.Tables[0].Rows[0]["TEMP"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ISC"] != null && ds.Tables[0].Rows[0]["ISC"].ToString() != "")
                {
                    model.ISC = ds.Tables[0].Rows[0]["ISC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["VOC"] != null && ds.Tables[0].Rows[0]["VOC"].ToString() != "")
                {
                    model.VOC = ds.Tables[0].Rows[0]["VOC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IMP"] != null && ds.Tables[0].Rows[0]["IMP"].ToString() != "")
                {
                    model.IMP = ds.Tables[0].Rows[0]["IMP"].ToString();
                }
                if (ds.Tables[0].Rows[0]["VMP"] != null && ds.Tables[0].Rows[0]["VMP"].ToString() != "")
                {
                    model.VMP = ds.Tables[0].Rows[0]["VMP"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PMAX"] != null && ds.Tables[0].Rows[0]["PMAX"].ToString() != "")
                {
                    model.PMAX = ds.Tables[0].Rows[0]["PMAX"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FF"] != null && ds.Tables[0].Rows[0]["FF"].ToString() != "")
                {
                    model.FF = ds.Tables[0].Rows[0]["FF"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EFF"] != null && ds.Tables[0].Rows[0]["EFF"].ToString() != "")
                {
                    model.EFF = ds.Tables[0].Rows[0]["EFF"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TEST_DATE"] != null && ds.Tables[0].Rows[0]["TEST_DATE"].ToString() != "")
                {
                    model.TEST_DATE = ds.Tables[0].Rows[0]["TEST_DATE"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_NO"] != null && ds.Tables[0].Rows[0]["CREATED_NO"].ToString() != "")
                {
                    model.CREATED_NO = ds.Tables[0].Rows[0]["CREATED_NO"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MODIFIED_ON"] != null && ds.Tables[0].Rows[0]["MODIFIED_ON"].ToString() != "")
                {
                    model.MODIFIED_ON = ds.Tables[0].Rows[0]["MODIFIED_ON"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MODIFIED_BY"] != null && ds.Tables[0].Rows[0]["MODIFIED_BY"].ToString() != "")
                {
                    model.MODIFIED_BY = ds.Tables[0].Rows[0]["MODIFIED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["REMARK"] != null && ds.Tables[0].Rows[0]["REMARK"].ToString() != "")
                {
                    model.REMARK = ds.Tables[0].Rows[0]["REMARK"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV01"] != null && ds.Tables[0].Rows[0]["RESV01"].ToString() != "")
                {
                    model.RESV01 = ds.Tables[0].Rows[0]["RESV01"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV02"] != null && ds.Tables[0].Rows[0]["RESV02"].ToString() != "")
                {
                    model.RESV02 = ds.Tables[0].Rows[0]["RESV02"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV03"] != null && ds.Tables[0].Rows[0]["RESV03"].ToString() != "")
                {
                    model.RESV03 = ds.Tables[0].Rows[0]["RESV03"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV04"] != null && ds.Tables[0].Rows[0]["RESV04"].ToString() != "")
                {
                    model.RESV04 = ds.Tables[0].Rows[0]["RESV04"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RESV05"] != null && ds.Tables[0].Rows[0]["RESV05"].ToString() != "")
                {
                    model.RESV05 = ds.Tables[0].Rows[0]["RESV05"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SYSID,MODULE_SN,TEMP,ISC,VOC,IMP,VMP,PMAX,FF,EFF,TEST_DATE,CREATED_NO,CREATED_BY,MODIFIED_ON,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05 ");
            strSql.Append(" FROM T_MODULE_TEST ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SYSID,MODULE_SN,TEMP,ISC,VOC,IMP,VMP,PMAX,FF,EFF,TEST_DATE,CREATED_NO,CREATED_BY,MODIFIED_ON,MODIFIED_BY,REMARK,RESV01,RESV02,RESV03,RESV04,RESV05 ");
            strSql.Append(" FROM T_MODULE_TEST ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }

    /// <summary>
    /// 数据库访问类：Module_Carton
    /// </summary>
    public partial class Module_CartonDal
    {
        public Module_CartonDal()
        { }
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="_ListModuleTest">组件清单</param>
        /// <param name="_ListModule"></param>
        /// <param name="moduleCarton"></param>
        /// <param name="model"></param>
        /// <param name="tbBarCode">内部托号</param>
        /// <param name="sModuleClass"></param>
        /// <param name="RowCount"></param>
        /// <param name="CustBoxID">客户托号</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool Packing(List<MODULE_TEST> _ListModuleTest, List<MODULE> _ListModule,
            MODULE_CARTON moduleCarton, string model, string tbBarCode, string sModuleClass,
            int RowCount, string CustBoxID, out string msg)
        {
            msg = "";
            string sql = "";
            string sql2 = "";
            string sMuduleNo = "";
            using (SqlConnection CenterSqlConnection = new SqlConnection(FormCover.CenterDBConnString))
            {
                CenterSqlConnection.Open();
                using (SqlTransaction CenterSqlTransaction = CenterSqlConnection.BeginTransaction())
                {
                    using (SqlCommand CenterSqlCommand = new SqlCommand())
                    {
                        CenterSqlCommand.Connection = CenterSqlConnection;
                        CenterSqlCommand.Transaction = CenterSqlTransaction;
                        try
                        {
                            #region 集中数据库
                            foreach (MODULE_TEST test in _ListModuleTest)
                            {
                                //如果此组件之前测试过，删除测试数据
                                sql = @"DELETE FROM {0}.[dbo].[T_MODULE_TEST] WHERE MODULE_SN ='{1}';
                                        UPDATE {0}.[dbo].[T_MODULE_REWORK] SET FLAG='1'
                                        WHERE MODULE_SN = '{1}';
                                        DELETE FROM {0}.[dbo].[T_MODULE] WHERE MODULE_SN ='{1}'";
                                sql = string.Format(sql, FormCover.CenterDBName, test.MODULE_SN.Trim());
                                SqlHelper.ExecuteNonQuery(CenterSqlConnection, CenterSqlTransaction, CommandType.Text, sql, null);

                                if (new MODULE_TESTDAL().Add(CenterSqlConnection, CenterSqlTransaction, test) < 1)
                                {
                                    msg = "组件 " + test.MODULE_SN + " 在更新集中数据库失败(T_MODULE_TEST)";
                                    CenterSqlTransaction.Rollback();
                                    break;
                                }
                            }

                            if (!msg.Equals(""))
                                return false;

                            StringBuilder log = new StringBuilder();
                            foreach (MODULE mo in _ListModule)
                            {
                                if (mo.REMARK != "UnPacking")
                                {
                                    if (new MODULEDAL().Add(CenterSqlConnection, CenterSqlTransaction, mo) < 1)
                                    {
                                        msg = "组件 " + mo.MODULE_SN + " 在更新集中数据库失败(T_MODULE)";
                                        CenterSqlTransaction.Rollback();
                                        break;
                                    }
                                }

                                MODULE_PACKING_LIST molk = new MODULE_PACKING_LIST();
                                molk.MODULE_CARTON_SYSID = moduleCarton.SYSID;
                                molk.MODULE_SYSID = mo.SYSID;

                                if (new MODULE_PACKING_LISTDAL().Add(CenterSqlConnection, CenterSqlTransaction, molk) < 1)
                                {
                                    msg = "组件 " + mo.MODULE_SN + " 在更新集中数据库失败(T_MODULE_PACKING_LIST)";
                                    CenterSqlTransaction.Rollback();
                                    break;
                                }
                                log.AppendLine(string.Format("PackingList.CartonId:{0} ModuleSysId:{1}", moduleCarton.SYSID, mo.SYSID));
                            }

                            if (!msg.Equals(""))
                                return false;

                            if (new MODULE_CARTONDAL().Add(CenterSqlConnection, CenterSqlTransaction, moduleCarton) < 1)
                            {
                                msg = "更新集中数据库失败(T_MODULE_CARTON)";
                                CenterSqlTransaction.Rollback();
                                return false;
                            }
                            #endregion

                            //如果包装和集中数据库做DB-LINK或者在一台服务器上
                            if (ProductStorageDAL.GetSysMapping("PackingSystemSameFlag", FormCover.CurrentFactory).Trim().ToUpper().Equals("Y"))
                            {
                                #region 包装数据库
                                try
                                {
                                    sql = @"Update {0}.dbo.Box set IsUsed='Y',BoxType='" + model + "',ModelType='',PackDate = getdate() ,flag='1', Number=" + RowCount + ",PackOperator='" + FormCover.CurrUserWorkID +
                                                          "',[Cust_BoxID]='" + CustBoxID + "' where BoxID='" + tbBarCode + "';";
                                    sql = sql + "if not exists (select BoxID from {0}.dbo.Box where boxid='" + tbBarCode + "') insert into {0}.dbo.Box(BoxID,BoxType,ModelType,Number,IsUsed,PackOperator,Cust_BoxID,Flag) values('" + tbBarCode + "','" + model + "',''," + RowCount + ",'Y','" + FormCover.CurrUserWorkID + "','" + CustBoxID + "','1')";

                                    sql = string.Format(sql, FormCover.PackingDBName.Trim());
                                    if (SqlHelper.ExecuteNonQuery(CenterSqlConnection, CenterSqlTransaction, CommandType.Text, sql, null) < 1)
                                    {
                                        msg = "箱号" + tbBarCode + " 更新包装数据库失败(Box)";
                                        CenterSqlTransaction.Rollback();
                                        return false;
                                    }

                                    foreach (MODULE mo in _ListModule)
                                    {
                                        sMuduleNo = mo.MODULE_SN.ToString();
                                        string Std = mo.RESV05;//标准功率
                                        string Pmx = mo.RESV04;//实测功率
                                        string vModuleClass = mo.Resv06;//组件等级
                                        if ((!Std.Equals("")) && (!Pmx.Equals("")))
                                        {
                                            //if (!sModuleClass.Equals(""))
                                            //sql2 = @"UPDATE {0}.dbo.[Product] SET [StdPower] ='" + Std + "',[Pmax]='" + Pmx + "',[Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "',[ModuleClass]='" + sModuleClass + "',[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            //else
                                            //    sql2 = @"UPDATE {0}.dbo.[Product] SET [StdPower] ='" + Std + "',[Pmax]='" + Pmx + "',[Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "' ,[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            sql2 = @"UPDATE {0}.dbo.[Product] SET [StdPower] ='" + Std + "',[Pmax]='" + Pmx + "',[Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "',[ModuleClass]='" + vModuleClass + "',[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            sql2 = string.Format(sql2, FormCover.PackingDBName.Trim());
                                        }
                                        else
                                        {
                                            //if (!sModuleClass.Equals(""))
                                            //sql2 = @"UPDATE {0}.dbo.[Product] SET [Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "',[ModuleClass]='" + sModuleClass + "',[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            //else
                                            //    sql2 = @"UPDATE {0}.dbo.[Product] SET [Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "' ,[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            sql2 = @"UPDATE {0}.dbo.[Product] SET [Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "',[ModuleClass]='" + vModuleClass + "',[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                            sql2 = string.Format(sql2, FormCover.PackingDBName.Trim());
                                        }
                                        if (SqlHelper.ExecuteNonQuery(CenterSqlConnection, CenterSqlTransaction, CommandType.Text, sql2, null) < 1)
                                        {
                                            msg = "组件 " + mo.MODULE_SN + " 更新包装数据库(Product)失败";
                                            CenterSqlTransaction.Rollback();
                                            return false;
                                        }
                                    }
                                    CenterSqlTransaction.Commit();
                                }
                                catch (Exception ex)
                                {
                                    msg = ex.Message;
                                    CenterSqlTransaction.Rollback();
                                    return false;
                                }
                                #endregion
                            }
                            else
                            {
                                #region 包装数据库
                                using (SqlConnection PackingSqlConn = new SqlConnection(FormCover.connectionBase))
                                {
                                    PackingSqlConn.Open();
                                    using (SqlTransaction PackingSqlTransaction = PackingSqlConn.BeginTransaction())
                                    {
                                        using (SqlCommand PackingSqlCommand = new SqlCommand())
                                        {
                                            try
                                            {
                                                PackingSqlCommand.Connection = PackingSqlConn;
                                                PackingSqlCommand.Transaction = PackingSqlTransaction;

                                                sql = @"Update {0}.dbo.Box set IsUsed='Y',BoxType='" + model + "',ModelType='',PackDate = getdate() ,flag='1', Number=" + RowCount + ",PackOperator='" + FormCover.CurrUserWorkID +
                                                      "',[Cust_BoxID]='" + CustBoxID + "' where BoxID='" + tbBarCode + "';";
                                                sql = sql + "if not exists (select BoxID from {0}.dbo.Box where boxid='" + tbBarCode + "') insert into {0}.dbo.Box(BoxID,BoxType,ModelType,Number,IsUsed,PackOperator,Cust_BoxID,Flag) values('" + tbBarCode + "','" + model + "',''," + RowCount + ",'Y','" + FormCover.CurrUserWorkID + "','" + CustBoxID + "','1')";

                                                sql = string.Format(sql, FormCover.PackingDBName.Trim());
                                                if (SqlHelper.ExecuteNonQuery(PackingSqlConn, PackingSqlTransaction, CommandType.Text, sql, null) < 1)
                                                {
                                                    msg = "箱号" + tbBarCode + " 更新包装数据库失败";
                                                    CenterSqlTransaction.Rollback();
                                                    PackingSqlTransaction.Rollback();
                                                    return false;
                                                }

                                                foreach (MODULE mo in _ListModule)
                                                {
                                                    sMuduleNo = mo.MODULE_SN.ToString();
                                                    string Std = mo.RESV05;//标准功率
                                                    string Pmx = mo.RESV04;//实测功率
                                                    if (!sModuleClass.Equals(""))
                                                        sql2 = @"UPDATE {0}.dbo.[Product] SET [StdPower] ='" + Std + "',[Pmax]='" + Pmx + "',[Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "',[ModuleClass]='" + sModuleClass + "',[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                                    else
                                                        sql2 = @"UPDATE {0}.dbo.[Product] SET [StdPower] ='" + Std + "',[Pmax]='" + Pmx + "',[Process]='T5',[flag]='1',[PackDate]= getdate(),[PackOperator] = '" + FormCover.CurrUserWorkID + "',[BoxID]='" + tbBarCode + "' ,[Cust_BoxID]='" + CustBoxID + "' WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from {0}.dbo.Product where SN='" + sMuduleNo + "')";
                                                    sql2 = string.Format(sql2, FormCover.PackingDBName.Trim());
                                                    if (SqlHelper.ExecuteNonQuery(PackingSqlConn, PackingSqlTransaction, CommandType.Text, sql2, null) < 1)
                                                    {
                                                        msg = "组件 " + mo.MODULE_SN + " 更新包装数据库(Product)失败";
                                                        CenterSqlTransaction.Rollback();
                                                        PackingSqlTransaction.Rollback();
                                                        break;
                                                    }
                                                }
                                                if (!msg.Equals(""))
                                                    return false;

                                                CenterSqlTransaction.Commit();
                                                PackingSqlTransaction.Commit();
                                            }
                                            catch (Exception ex)
                                            {
                                                msg = ex.Message;
                                                CenterSqlTransaction.Rollback();
                                                PackingSqlTransaction.Rollback();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            msg = ex.Message;
                            CenterSqlTransaction.Rollback();
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }

    //add by hexing 2013.12.18
    /// <summary>
    ///  ModulePackingTransaction:实体类
    /// </summary>
    [Serializable]
    public class MODULEPACKINGTRANSACTION
    {
        /// <summary>
        /// 主键
        /// </summary>
        public String SysID { get; set; }
        /// <summary>
        /// 组件号码
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Barcode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String WorkOrder { get; set; }
        /// <summary>
        /// 版型/产品型号
        /// </summary>
        public String ModuleType { get; set; }
        /// <summary>
        /// 产品编号（MES）
        /// </summary>
        public String ProductNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Workshop { get; set; }
        /// <summary>
        /// 交易ID，用以关联T_Carton_Packing_Transaction
        /// </summary>
        public String CartonNoSysID { get; set; }
        /// <summary>
        /// 实测功率
        /// </summary>
        public String Pmax { get; set; }
        /// <summary>
        /// 标称功率
        /// </summary>
        public String ModuleStdPower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Temp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ISC { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String VOC { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String IMP { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String VMP { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String FF { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String EFF { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String TestDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public String ModifiedOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public String ModifiedBy { get; set; }
        /// <summary>
        /// 预留字段1
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留字段2
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留字段3
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留字段4
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留字段5
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留字段6
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留字段7
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留字段8
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留字段9
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留字段10
        /// </summary>
        public String Resv10 { get; set; }

    }
    /// <summary>
    /// 数据访问类:ModulePackingTransaction
    /// </summary>
    public class MODULEPACKINGTRANSACTIONDAL
    {
        public MODULEPACKINGTRANSACTIONDAL()
        { }
        #region  Method
        /// <summary>
        /// 增加一条数据
        /// </summary>
       
        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULEPACKINGTRANSACTION model,out string msg)
        {
            try
            {
                msg = "";
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into [CentralizedDatabase].[dbo].[T_Module_Packing_Transaction](");
                strSql.Append("SysID,MODULE_SN,BARCODE,WORK_ORDER,MODULE_TYPE,PRODUCT_NO,WORKSHOP,CartonNoSysID,PMAX,ModuleStdPower,TEMP,ISC,VOC,IMP,VMP,FF,EFF,TEST_DATE,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)");
                strSql.Append(" values (");
                strSql.Append("@SysID,@MODULE_SN,@BARCODE,@WORK_ORDER,@MODULE_TYPE,@PRODUCT_NO,@WORKSHOP,@CartonNoSysID,@PMAX,@ModuleStdPower,@TEMP,@ISC,@VOC,@IMP,@VMP,@FF,@EFF,@TEST_DATE,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV10)");
                SqlParameter[] parameters = {
					new SqlParameter("@SysID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
                    new SqlParameter("@BARCODE", SqlDbType.NVarChar,50),
                    new SqlParameter("@WORK_ORDER", SqlDbType.NVarChar,50),
                    new SqlParameter("@MODULE_TYPE", SqlDbType.NVarChar,50),
                    new SqlParameter("@PRODUCT_NO", SqlDbType.NVarChar,50),
                    new SqlParameter("@WORKSHOP", SqlDbType.NVarChar,50),
					new SqlParameter("@CartonNoSysID", SqlDbType.NVarChar,50),
                    new SqlParameter("@PMAX", SqlDbType.NVarChar,50),
					new SqlParameter("@ModuleStdPower", SqlDbType.NVarChar,50),
					new SqlParameter("@TEMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@ISC", SqlDbType.NVarChar,50),
                    new SqlParameter("@VOC", SqlDbType.NVarChar,50),
                    new SqlParameter("@IMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@VMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@FF", SqlDbType.NVarChar,50),
                    new SqlParameter("@EFF", SqlDbType.NVarChar,50),
                    new SqlParameter("@TEST_DATE", SqlDbType.NVarChar,50),                    
					//new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV07", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV08", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV09", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV10", SqlDbType.NVarChar,100)
                                        };
                parameters[0].Value = model.SysID;
                parameters[1].Value = model.ModuleSn;
                parameters[2].Value = model.Barcode;
                parameters[3].Value = model.WorkOrder;
                parameters[4].Value = model.ModuleType;
                parameters[5].Value = model.ProductNo;
                parameters[6].Value = model.Workshop;
                parameters[7].Value = model.CartonNoSysID;
                parameters[8].Value = model.Pmax;
                parameters[9].Value = model.ModuleStdPower;
                parameters[10].Value = model.Temp;
                parameters[11].Value = model.ISC;
                parameters[12].Value = model.VOC;
                parameters[13].Value = model.IMP;
                parameters[14].Value = model.VMP;
                parameters[15].Value = model.FF;
                parameters[16].Value = model.EFF;
                parameters[17].Value = model.TestDate;
               // parameters[18].Value = model.CreatedOn;
                parameters[18].Value = model.CreatedBy;
                parameters[19].Value = model.ModifiedOn;
                parameters[20].Value = model.ModifiedBy;
                parameters[21].Value = model.Resv01;
                parameters[22].Value = model.Resv02;
                parameters[23].Value = model.Resv03;
                parameters[24].Value = model.Resv04;
                parameters[25].Value = model.Resv05;
                parameters[26].Value = model.Resv06;
                parameters[27].Value = model.Resv07;
                parameters[28].Value = model.Resv08;
                parameters[29].Value = model.Resv09;
                parameters[30].Value = model.Resv10;
                string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return 0;
            }

        #endregion
        }
    }
     /// <summary>
     /// CartonPackingTransaction：实体类
     /// </summary>
     [Serializable]
     public class CARTONPACKINGTRANSACTION
        {
            /// <summary>
            /// 主键
            /// </summary>
            public String SysID { get; set; }
            /// <summary>
            /// 阿特斯组织机构（包括‘CS’，‘LY’，‘OutSea’,'OutSourcing'）
            /// </summary>
            public String Orgnization { get; set; }
            /// <summary>
            /// 箱号
            /// </summary>
            public String Carton { get; set; }
            /// <summary>
            /// 柜号
            /// </summary>
            public String JobNo { get; set; }
            /// <summary>
            /// 操作组件相关的动作（包括：Inventory,UnInventory,LCL,unLCL,OutSeaLCL,OutSeaUnLCL,OutSourcingLCL,OutSourcingUnLCL）
            /// </summary>
            public String Action { get; set; }
            /// <summary>
            /// 交易ID，用以关联T_Module_Packing_Transaction
            /// </summary>
            public String ActionTxnID { get; set; }
            /// <summary>
            /// 操作组件相关的动作的时间
            /// </summary>
            public String ActionDate { get; set; }
            /// <summary>
            /// 操作人
            /// </summary>
            public String ActionBy { get; set; }
            /// <summary>
            /// 原始托号
            /// </summary>
            public String OriginalCarton { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public String OriginalJobNo { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public String StdPowerLevel { get; set; }
            /// <summary>
            /// 组件数量
            /// </summary>
            public String ModuleQty { get; set; }
            /// <summary>
            /// 创建时间
            /// </summary>
            public String CreatedOn { get; set; }
            /// <summary>
            /// 创建人
            /// </summary>
            public String CreatedBy { get; set; }
            /// <summary>
            /// 修改日期
            /// </summary>
            public String ModifiedOn { get; set; }
            /// <summary>
            /// 修改人
            /// </summary>
            public String ModifiedBy { get; set; }
            /// <summary>
            /// 预留字段1
            /// </summary>
            public String Resv01 { get; set; }
            /// <summary>
            /// 预留字段2
            /// </summary>
            public String Resv02 { get; set; }
            /// <summary>
            /// 预留字段3
            /// </summary>
            public String Resv03 { get; set; }
            /// <summary>
            /// 预留字段4
            /// </summary>
            public String Resv04 { get; set; }
            /// <summary>
            /// 预留字段5
            /// </summary>
            public String Resv05 { get; set; }
            /// <summary>
            /// 预留字段6
            /// </summary>
            public String Resv06 { get; set; }
            /// <summary>
            /// 预留字段7
            /// </summary>
            public String Resv07 { get; set; }
            /// <summary>
            /// 预留字段8
            /// </summary>
            public String Resv08 { get; set; }
            /// <summary>
            /// 预留字段9
            /// </summary>
            public String Resv09 { get; set; }
            /// <summary>
            /// 预留字段10
            /// </summary>
            public String Resv10 { get; set; }

        }
     /// <summary>
      ///  数据访问类:ModulePackingTransaction
      /// </summary>
     public class CARTONPACKINGTRANSACTIONDAL
        {
         public CARTONPACKINGTRANSACTIONDAL()
            { }
            #region  Method
            /// <summary>
            /// 增加一条数据
            /// </summary>
            public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, CARTONPACKINGTRANSACTION model,out string msg)
            {
                try
                {
                    msg = "";
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("insert into [CentralizedDatabase].[dbo].[T_Carton_Packing_Transaction](");
                    strSql.Append("SysID,Orgnization,Carton,JobNo,Action,ActionTxnID,ActionDate,ActionBy,OriginalCarton,OriginalJobNo,StdPowerLevel,ModuleQty,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)");
                    strSql.Append(" values (");
                    strSql.Append("@SYSID,@Orgnization,@Carton,@JobNo,@Action,@ActionTxnID,@ActionDate,@ActionBy,@OriginalCarton,@OriginalJobNo,@StdPowerLevel,@ModuleQty,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV10)");
                    SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@Orgnization", SqlDbType.NVarChar,50),
					new SqlParameter("@Carton", SqlDbType.NVarChar,50),
					new SqlParameter("@JobNo", SqlDbType.NVarChar,50),
					new SqlParameter("@Action", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionTxnID", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionDate", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionBy", SqlDbType.NVarChar,50),
	                new SqlParameter("@OriginalCarton", SqlDbType.NVarChar,50),	
                    new SqlParameter("@OriginalJobNo", SqlDbType.NVarChar,50),	
                    new SqlParameter("@StdPowerLevel", SqlDbType.NVarChar,50),	
                    new SqlParameter("@ModuleQty", SqlDbType.NVarChar,50),	
                   // new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,100),			
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV07", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV08", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV09", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV10", SqlDbType.NVarChar,100)};
                    parameters[0].Value = model.SysID;
                    parameters[1].Value = model.Orgnization;
                    parameters[2].Value = model.Carton;
                    parameters[3].Value = model.JobNo;
                    parameters[4].Value = model.Action;
                    parameters[5].Value = model.ActionTxnID;
                    parameters[6].Value = model.ActionDate;
                    parameters[7].Value = model.ActionBy;
                    parameters[8].Value = model.OriginalCarton;
                    parameters[9].Value = model.OriginalJobNo;
                    parameters[10].Value = model.StdPowerLevel;
                    parameters[11].Value = model.ModuleQty;
                   // parameters[12].Value = model.CreatedOn;
                    parameters[12].Value = model.CreatedBy;
                    parameters[13].Value = model.ModifiedOn;
                    parameters[14].Value = model.ModifiedBy;
                    parameters[15].Value = model.Resv01;
                    parameters[16].Value = model.Resv02;
                    parameters[17].Value = model.Resv03;
                    parameters[18].Value = model.Resv04;
                    parameters[19].Value = model.Resv05;
                    parameters[20].Value = model.Resv06;
                    parameters[21].Value = model.Resv07;
                    parameters[22].Value = model.Resv08;
                    parameters[23].Value = model.Resv09;
                    parameters[24].Value = model.Resv10;
                    //return DbHelperSQL.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, strSql, parameters);
                    // return  DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
                    string _strSql = string.Format(strSql.ToString(), FormCover.CenterDBName.Trim());
                    return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    return 0;
                }
                //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            }
            #endregion

        }

     /// <summary>
     /// 数据库访问类：Module_Carton
     /// </summary>
     public partial class CartonPackingTransactionDal
     {
         public CartonPackingTransactionDal()
         { }
         /// <summary>
         /// 保存数据
         /// </summary>

         public bool SaveStorage(SqlConnection sql1, SqlTransaction trans,List<CARTONPACKINGTRANSACTION> _ListCartonpPacking, List<MODULEPACKINGTRANSACTION> _ListModulePacking, out string msg)
         {
             //try
             //{
                 msg = "";
                 //using (SqlConnection CenterSqlConnection = new SqlConnection(FormCover.CenterDBConnString))
                 //{
                     //CenterSqlConnection.Open();
                     //using (SqlTransaction CenterSqlTransaction = CenterSqlConnection.BeginTransaction())
                     //{
                         try
                         {
                             #region 集中数据库入库记录
                             foreach (MODULEPACKINGTRANSACTION MODULESN in _ListModulePacking)
                             {
                                 if (new MODULEPACKINGTRANSACTIONDAL().Add(sql1, trans, MODULESN, out msg) < 1)
                                 {
                                     msg = "组件 " + MODULESN.ModuleSn + " 在更新集中数据库失败(T_Module_Packing_Transaction)";
                                     trans.Rollback();
                                     break;
                                 }
                             }
                             if (!msg.Equals(""))
                             {
                                 trans.Rollback();
                                 return false;
                             }
                             foreach (CARTONPACKINGTRANSACTION CartonPacking in _ListCartonpPacking)
                             {
                                 if (new CARTONPACKINGTRANSACTIONDAL().Add(sql1, trans, CartonPacking, out msg) < 1)
                                 {
                                     msg = "更新集中数据库失败(T_MODULE_PACKING_TRANSACTION)";
                                     trans.Rollback();
                                     return false;
                                 }
                             }

                             #endregion

                             //CenterSqlTransaction.Commit();
                         }
                         catch (Exception ex)
                         {
                             msg = ex.Message;
                             trans.Rollback();
                             return false;
                         }
                     //}
                 //}
                 return true;
             //}
             //catch (Exception ex)
             //{
             //    msg = ex.Message;
             //    return false;
             //}
         }
     }


    }

