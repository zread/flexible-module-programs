﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FormReWorkOrder : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private static FormReWorkOrder theSingleton = null;
        private List<CARTONPACKINGTRANSACTION> _ListCartonSucess = new List<CARTONPACKINGTRANSACTION>();
        private List<MODULEPACKINGTRANSACTION> _ListModuleSucess = new List<MODULEPACKINGTRANSACTION>();
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormReWorkOrder();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        public FormReWorkOrder()
        {
            InitializeComponent();
        }
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }
        private void SetDataGridViewData()
        {
            if (this.txtWo.Text.Trim().Equals(""))
                return;

            DataTable dt = ProductStorageDAL.GetReworkWoCartonIno(this.txtWo.Text.Trim());
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = false;
                    this.dataGridView1.Rows[RowNo].Cells["ReworkOrder"].Value = Convert.ToString(row["OrderNo"]);
                    this.dataGridView1.Rows[RowNo].Cells["PostCode"].Value = Convert.ToString(row["PostCode"]);
                    this.dataGridView1.Rows[RowNo].Cells["CartonNo"].Value = Convert.ToString(row["CartonNo"]);
                    if (Convert.ToString(row["ReworkFlag"]).Trim().Equals("0"))
                       this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value ="待确认";
                    else if (Convert.ToString(row["ReworkFlag"]).Trim().Equals("1"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "已确认";
                    this.dataGridView1.Rows[RowNo].Cells["Factory"].Value = Convert.ToString(row["WorkShop"]); ;
                    this.dataGridView1.Rows[RowNo].Cells["FactoryCode"].Value = Convert.ToString(row["PickingFrom"]);
                    this.dataGridView1.Rows[RowNo].Cells["CartonStatusCode"].Value = Convert.ToString(row["ReworkFlag"]);
                    this.dataGridView1.Rows[RowNo].Cells["SAPCartonStatusCode"].Value = Convert.ToString(row["PickingType"]);
                    if (Convert.ToString(row["PickingType"]).Equals("261"))
                        this.dataGridView1.Rows[RowNo].Cells["SAPCartonStatus"].Value = "待重工";
                    else if (Convert.ToString(row["PickingType"]).Equals("262"))
                        this.dataGridView1.Rows[RowNo].Cells["SAPCartonStatus"].Value = "已撤销重工";

                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                this.txtWo.SelectAll();
                MessageBox.Show("重工工单:"+this.txtWo.Text.Trim()+"还没有从SAP下载，或者已经重工确认完成");
                return;
            }
            this.txtWo.Clear();
            this.txtWo.Focus();
        }

        /// <summary>
        /// 快捷键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtWo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                PicBoxWo_Click(null, null);
            }
        }

        /// <summary>
        /// 回车键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null, null);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData();
        }

        private void PicBoxWo_Click(object sender, EventArgs e)
        {
            var frm = new FormReWorkOrderInfo(this.txtWo.Text.Trim());    
            frm.ShowDialog();
            this.txtWo.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            btnQuery_Click(null,null);
        }

        List<string> FactoryCodeList = new List<string>();
        List<string> CartonNoList = new List<string>();
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
            {
                string wo = "";
                string msg = "";
                if (FactoryCodeList.Count > 0)
                    FactoryCodeList.Clear();
                if (CartonNoList.Count > 0)
                    CartonNoList.Clear();

                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if (wo.Equals(""))
                        wo = Convert.ToString(dgvRow.Cells["ReworkOrder"].Value);

                    if ((Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE") && (!Convert.ToString(dgvRow.Cells["CartonNo"].Value).Equals("")))
                    {
                        if (!CartonNoList.Contains(Convert.ToString(dgvRow.Cells["CartonNo"].Value)))
                        {
                            FactoryCodeList.Add(Convert.ToString(dgvRow.Cells["FactoryCode"].Value).Trim());
                            CartonNoList.Add(Convert.ToString(dgvRow.Cells["CartonNo"].Value).Trim());
                        }
                    }
                }

               

                if ((FactoryCodeList.Count > 0) && (CartonNoList.Count > 0) && !wo.Equals(""))
                    {

                        if (!PackinSystemg.PackinSystemgRework(wo, FactoryCodeList, CartonNoList, out msg))
                        {
                            MessageBox.Show(msg);
                        }
                        else
                        {
                            MessageBox.Show("确认成功");

                            if (this.dataGridView1.Rows.Count > 0)
                                this.dataGridView1.Rows.Clear();
                            //#region  写重工入库交易
                            //string msg1 = "";
                            //if (!new CartonPackingTransactionDal().SaveStorage(_ListCartonSucess, _ListModuleSucess, out msg1))
                            //{
                            //    ToolsClass.Log("重工入库记录数据保存失败:" + msg1 + "", "ABNORMAL");
                            //    return;
                            //}
                            //#endregion
                        }
                    }
                    else
                    {
                        MessageBox.Show("查询到的数据不符合重工确认条件");
                    }
            }
            else
            {
                MessageBox.Show("没有要确认的数据");
            }
            this.chbSelected.Checked = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.txtWo.Clear();
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
            this.txtWo.Focus();
        }

        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridViewCheckBoxCell dgcb = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells["RowIndex"];
                if ((bool)dgcb.FormattedValue)
                {
                    dgcb.Value = false;
                    string poston = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["PostCode"].Value);
                    foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                    {
                        if (poston.Equals(Convert.ToString(dgvRow.Cells[2].Value)))
                           dgvRow.Cells[0].Value = false;                       
                    }
                }
                else
                {
                    dgcb.Value = true;
                    string poston = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["PostCode"].Value);
                    foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                    {
                        if (poston.Equals(Convert.ToString(dgvRow.Cells[2].Value)))
                            dgvRow.Cells[0].Value = true;
                    }
                }
            }
        }

        private void FormReWorkOrder_Load(object sender, EventArgs e)
        {
        
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
        }
    }
}
