﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public class LanguageHelper
    {

        /// <summary>
        /// 从config.xml 获取默认语言
        /// </summary>
        private static string _Language;

        /// <summary>
        /// 从config.xml 获取集中数据库连接字符串
        /// </summary>
        private static string Language
        {
            get
            {
                //if (string.IsNullOrEmpty(_Language))
                    //_Language = ToolsClass.getConfig("Language", false, "", "config.xml").ToUpper();
                    _Language = FormCover.Language;
                return _Language;
            }
        }

        /// <summary>
        /// 获取form所有信息
        /// </summary>
        /// <param name="formName"></param>
        /// <returns></returns>
        public static List<LanguageItemModel> getLanguageItem(string formName, string typename)
        {
            string strSQL;
            if (string.IsNullOrEmpty(typename))
                strSQL = string.Format("SELECT FORM_NAME,CONTROLS_NAME,TYPE,ITEM_FOR,LANGUAGE_CH,LANGUAGE_EN,LANGUAGE_OTHER FROM Language_Items NOLOCK WHERE FORM_NAME='{0}'", formName);
            else
                strSQL = string.Format("SELECT FORM_NAME,CONTROLS_NAME,TYPE,ITEM_FOR,LANGUAGE_CH,LANGUAGE_EN,LANGUAGE_OTHER FROM Language_Items NOLOCK WHERE FORM_NAME='{0}' AND TYPE='{1}'", formName, typename);


            var reader = SqlHelper.ExecuteReader(FormCover.connectionBase, CommandType.Text, strSQL);

            return DataUtils.ReaderToList<LanguageItemModel>(reader);
        }

        /// <summary>
        /// 获取控件的名称
        /// </summary>
        /// <param name="form"></param>
        public static void getNames(Form form)
        {
            LanguageItemModel lm = new LanguageItemModel();
            List<LanguageItemModel> lmList = getLanguageItem(form.Name.ToString(), null);


            Control.ControlCollection controlNames = form.Controls;

            try
            {
                foreach (Control control in controlNames)
                {

                    if (control.GetType() == typeof(System.Windows.Forms.Panel))
                        GetSubControls(control.Controls, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.GroupBox))
                    {
                        if (!control.Text.Trim().Equals(""))
                        {
                            lm = lmList.Where(p => p.CONTROLS_NAME == control.Name).FirstOrDefault();
                            if (lm != null)
                                control.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                                    //"EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;

                        }
                        GetSubControls(control.Controls, lmList);
                    }

                    if (control.GetType() == typeof(System.Windows.Forms.SplitContainer))
                        GetSubControls(control.Controls, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.SplitterPanel))
                        GetSubControls(control.Controls, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.DataGridView))
                        GetDataGridView(control, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.ToolStrip))
                        GetToolStrip(control, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.ListView))
                        GetListView(control, lmList);

                    if (control.GetType() == typeof(System.Windows.Forms.TabControl))
                        GetTabControl(control, lmList);



                    lm = lmList.Where(p => p.CONTROLS_NAME == control.Name).FirstOrDefault();
                    if (lm != null)
                    {
                        //language == "CN" ? "中文" : (language == "CN" ? "英文" : (language == "YN" ? "越南文" : "else"));
                        //control.Text = Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                        control.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI :""));
                    }


                }

                //窗体的名称
                lm = lmList.Where(p => p.CONTROLS_NAME == form.Name.ToString()).FirstOrDefault();
                if (lm != null)
                {
                    form.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// 获得子控件的显示名
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="table"></param>
        private static void GetSubControls(Control.ControlCollection controls, List<LanguageItemModel> lmList)
        {
            LanguageItemModel lm = new LanguageItemModel();
            foreach (Control control in controls)
            {

                if (control.GetType() == typeof(System.Windows.Forms.Panel))
                    GetSubControls(control.Controls, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.GroupBox))
                {
                    if (!control.Text.Trim().Equals(""))
                    {
                        lm = lmList.Where(p => p.CONTROLS_NAME == control.Name).FirstOrDefault();
                        if (lm != null)
                            control.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                                //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;

                    }
                    GetSubControls(control.Controls, lmList);
                }

                if (control.GetType() == typeof(System.Windows.Forms.SplitContainer))
                    GetSubControls(control.Controls, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.SplitterPanel))
                    GetSubControls(control.Controls, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.DataGridView))
                    GetDataGridView(control, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.ToolStrip))
                    GetToolStrip(control, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.ListView))
                    GetListView(control, lmList);

                if (control.GetType() == typeof(System.Windows.Forms.TabControl))
                    GetTabControl(control, lmList);

                //if (control.GetType() == typeof(System.Windows.Forms.ContextMenuStrip))
                //    GetContextMenuStrip(control, lmList);


                lm = lmList.Where(p => p.CONTROLS_NAME == control.Name).FirstOrDefault();
                if (lm != null)
                {
                    control.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }
            }
        }

        private static void GetDataGridView(Control control, List<LanguageItemModel> lmList)
        {
            LanguageItemModel lm = new LanguageItemModel();
            DataGridView dgv = (DataGridView)control;
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == dgv.Columns[i].Name).FirstOrDefault();
                if (lm != null)
                {
                    dgv.Columns[i].HeaderText = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }
            }
        }

        private static void GetToolStrip(Control control, List<LanguageItemModel> lmList)
        {
            LanguageItemModel lm = new LanguageItemModel();
            ToolStrip toolstrip = (ToolStrip)control;
            for (int i = 0; i < toolstrip.Items.Count; i++)
            {
                lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == toolstrip.Items[i].Name).FirstOrDefault();
                if (lm != null)
                {
                    toolstrip.Items[i].Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }
                if (toolstrip.Items[i].GetType() == typeof(System.Windows.Forms.ToolStripSplitButton))
                {
                    ToolStripSplitButton toolstripChild = (ToolStripSplitButton)toolstrip.Items[i];
                    for (int j = 0; j < toolstripChild.DropDownItems.Count; j++)
                    {
                        lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == toolstripChild.DropDownItems[j].Name).FirstOrDefault();
                        if (lm != null)
                        {
                            toolstripChild.DropDownItems[j].Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                                //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                        }
                    }
                }

            }

        }

        private static void GetListView(Control control, List<LanguageItemModel> lmList)
        {

            LanguageItemModel lm = new LanguageItemModel();
            ListView dgv = (ListView)control;
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == dgv.Columns[i].Name).FirstOrDefault();
                if (lm != null)
                {
                    dgv.Columns[i].Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }
            }

            //ContextMenuStrip
            ContextMenuStrip contextms = dgv.ContextMenuStrip;
            if (contextms != null)
            {
                for (int i = 0; i < contextms.Items.Count; i++)
                {
                    lm = lmList.Where(p => p.ITEM_FOR == contextms.Name && p.CONTROLS_NAME == contextms.Items[i].Name).FirstOrDefault();
                    if (lm != null)
                    {
                        contextms.Items[i].Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                            //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                    }
                }

            }

        }

        private static void GetTabControl(Control control, List<LanguageItemModel> lmList)
        {
            LanguageItemModel lm = new LanguageItemModel();

            TabControl dgv = (TabControl)control;
            Control.ControlCollection controllist = dgv.Controls;
            foreach (Control con1 in controllist)
            {
                lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == con1.Name).FirstOrDefault();
                if (lm != null)
                {
                    con1.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }


                TabPage page = (TabPage)con1;
                Control.ControlCollection controlist2 = page.Controls;
                foreach (Control con2 in controlist2)
                {

                    if (con2.GetType() == typeof(System.Windows.Forms.Label) || con2.GetType() == typeof(System.Windows.Forms.CheckBox))
                    {
                        lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == con2.Name).FirstOrDefault();
                        if (lm != null)
                        {
                            con2.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                                //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                        }
                    }
                    //如果是GridView
                    if (con2.GetType() == typeof(System.Windows.Forms.DataGrid))
                        GetListView(control, lmList);
                    if (con2.GetType() == typeof(System.Windows.Forms.GroupBox))
                    {
                        if (!con2.Text.Trim().Equals(""))
                        {
                            lm = lmList.Where(p => p.CONTROLS_NAME == con2.Name).FirstOrDefault();
                            if (lm != null)
                                con2.Text = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                                    //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;

                        }
                    }
                }
            }


        }

        public  static void GetCombomBox(Form fm, Control control)
        {
            LanguageItemModel lm = new LanguageItemModel();
            List<LanguageItemModel> lmList = getLanguageItem(fm.Name.ToString(), null);
            ComboBox combox = (ComboBox)control;
            for (int i = 0; i < combox.Items.Count; i++)
            {
                lm = lmList.Where(p => p.ITEM_FOR == control.Name &&  p.CONTROLS_NAME ==  Convert.ToString(combox.Items[i])).FirstOrDefault();
                if (lm != null)
                {
                    combox.Items[i] = Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                        //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
                }
            }


        }

        # region
        //private static void GetContextMenuStrip(Control control, List<LanguageItemModel> lmList)
        //{
        //    LanguageItemModel lm = new LanguageItemModel();
        //    ContextMenuStrip dgv = (ContextMenuStrip)control;
        //    for (int i = 0; i < dgv.Items.Count; i++)
        //    {
        //        lm = lmList.Where(p => p.ITEM_FOR == control.Name && p.CONTROLS_NAME == dgv.Items[i].Name).FirstOrDefault();
        //        if (lm != null)
        //        {
        //            dgv.Items[i].Text = Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
        //        }
        //    }

        //}
        # endregion

        public static string GetMessage(List<LanguageItemModel> messagelist, String name, String originalTest)
        {
            LanguageItemModel lm = new LanguageItemModel();
            lm = messagelist.Where(p => p.CONTROLS_NAME == name).FirstOrDefault();
            if (lm != null)
                return Language == "CH" ? lm.LANGUAGE_CH : (Language == "EN" ? lm.LANGUAGE_EN : (Language == "AI" ? lm.LANGUAGE_VI : ""));
                    //Language == "EN" ? lm.LANGUAGE_EN : lm.LANGUAGE_CH;
            return originalTest;

        }

        private static String SetText(String val1)
        {
            StringBuilder builder = new StringBuilder();
            string value = val1;
            string[] str1 = value.Split(' ');
            for (int i = 0; i < str1.Length; i++)
            {
                builder.Append(str1[i]);
                if ((i + 1) % 2 == 0)
                    builder.AppendLine();

            }
            return builder.ToString();
        }
    }
}
