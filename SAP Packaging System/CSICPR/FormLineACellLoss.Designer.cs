﻿namespace CSICPR
{
    partial class FormLineACellLoss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LineA = new System.Windows.Forms.Label();
            this.MO = new System.Windows.Forms.ComboBox();
            this.MOLabel = new System.Windows.Forms.Label();
            this.Clock = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Seperate = new System.Windows.Forms.Panel();
            this.FromStringer = new System.Windows.Forms.Label();
            this.XN1 = new System.Windows.Forms.Label();
            this.S1 = new System.Windows.Forms.TextBox();
            this.pcs1 = new System.Windows.Forms.Label();
            this.XN2 = new System.Windows.Forms.Label();
            this.pcs2 = new System.Windows.Forms.Label();
            this.S2 = new System.Windows.Forms.TextBox();
            this.XN3 = new System.Windows.Forms.Label();
            this.pcs3 = new System.Windows.Forms.Label();
            this.S3 = new System.Windows.Forms.TextBox();
            this.XN4 = new System.Windows.Forms.Label();
            this.pcs4 = new System.Windows.Forms.Label();
            this.S4 = new System.Windows.Forms.TextBox();
            this.XN5 = new System.Windows.Forms.Label();
            this.pcs5 = new System.Windows.Forms.Label();
            this.S5 = new System.Windows.Forms.TextBox();
            this.XN6 = new System.Windows.Forms.Label();
            this.pcs6 = new System.Windows.Forms.Label();
            this.S6 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.FromIncoming = new System.Windows.Forms.Label();
            this.BoxNum = new System.Windows.Forms.Label();
            this.BoxNumTextbox = new System.Windows.Forms.TextBox();
            this.Qty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BoxId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BoxNumtextBox2 = new System.Windows.Forms.TextBox();
            this.BoxId2 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.accident = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Review = new System.Windows.Forms.Button();
            this.Submit = new System.Windows.Forms.Button();
            this.Modify = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.shift = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.others = new System.Windows.Forms.TextBox();
            this.Seperate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // LineA
            // 
            this.LineA.AutoSize = true;
            this.LineA.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineA.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LineA.Location = new System.Drawing.Point(410, 29);
            this.LineA.Name = "LineA";
            this.LineA.Size = new System.Drawing.Size(96, 31);
            this.LineA.TabIndex = 0;
            this.LineA.Text = "Line A";
            // 
            // MO
            // 
            this.MO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MO.FormattingEnabled = true;
            this.MO.Location = new System.Drawing.Point(100, 87);
            this.MO.Name = "MO";
            this.MO.Size = new System.Drawing.Size(147, 33);
            this.MO.TabIndex = 1;
            // 
            // MOLabel
            // 
            this.MOLabel.AutoSize = true;
            this.MOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MOLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MOLabel.Location = new System.Drawing.Point(24, 87);
            this.MOLabel.Name = "MOLabel";
            this.MOLabel.Size = new System.Drawing.Size(75, 31);
            this.MOLabel.TabIndex = 0;
            this.MOLabel.Text = "MO#";
            // 
            // Clock
            // 
            this.Clock.AutoSize = true;
            this.Clock.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Clock.ForeColor = System.Drawing.SystemColors.Control;
            this.Clock.Location = new System.Drawing.Point(675, 35);
            this.Clock.Name = "Clock";
            this.Clock.Size = new System.Drawing.Size(76, 25);
            this.Clock.TabIndex = 2;
            this.Clock.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Seperate
            // 
            this.Seperate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Seperate.Controls.Add(this.FromStringer);
            this.Seperate.Location = new System.Drawing.Point(30, 133);
            this.Seperate.Name = "Seperate";
            this.Seperate.Size = new System.Drawing.Size(217, 33);
            this.Seperate.TabIndex = 3;
            this.Seperate.Paint += new System.Windows.Forms.PaintEventHandler(this.Seperate_Paint);
            // 
            // FromStringer
            // 
            this.FromStringer.AutoSize = true;
            this.FromStringer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromStringer.Location = new System.Drawing.Point(9, 7);
            this.FromStringer.Name = "FromStringer";
            this.FromStringer.Size = new System.Drawing.Size(189, 20);
            this.FromStringer.TabIndex = 0;
            this.FromStringer.Text = "Crack Cells From Stringer";
            // 
            // XN1
            // 
            this.XN1.AutoSize = true;
            this.XN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN1.Location = new System.Drawing.Point(27, 183);
            this.XN1.Name = "XN1";
            this.XN1.Size = new System.Drawing.Size(33, 16);
            this.XN1.TabIndex = 4;
            this.XN1.Text = "XN1";
            // 
            // S1
            // 
            this.S1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S1.Location = new System.Drawing.Point(75, 182);
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(120, 22);
            this.S1.TabIndex = 5;
            this.S1.Text = "0";
            // 
            // pcs1
            // 
            this.pcs1.AutoSize = true;
            this.pcs1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs1.Location = new System.Drawing.Point(212, 183);
            this.pcs1.Name = "pcs1";
            this.pcs1.Size = new System.Drawing.Size(35, 16);
            this.pcs1.TabIndex = 4;
            this.pcs1.Text = "PCS";
            // 
            // XN2
            // 
            this.XN2.AutoSize = true;
            this.XN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN2.Location = new System.Drawing.Point(27, 221);
            this.XN2.Name = "XN2";
            this.XN2.Size = new System.Drawing.Size(33, 16);
            this.XN2.TabIndex = 4;
            this.XN2.Text = "XN2";
            // 
            // pcs2
            // 
            this.pcs2.AutoSize = true;
            this.pcs2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs2.Location = new System.Drawing.Point(212, 222);
            this.pcs2.Name = "pcs2";
            this.pcs2.Size = new System.Drawing.Size(35, 16);
            this.pcs2.TabIndex = 4;
            this.pcs2.Text = "PCS";
            // 
            // S2
            // 
            this.S2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S2.Location = new System.Drawing.Point(75, 220);
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(120, 22);
            this.S2.TabIndex = 5;
            this.S2.Text = "0";
            // 
            // XN3
            // 
            this.XN3.AutoSize = true;
            this.XN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN3.Location = new System.Drawing.Point(27, 260);
            this.XN3.Name = "XN3";
            this.XN3.Size = new System.Drawing.Size(33, 16);
            this.XN3.TabIndex = 4;
            this.XN3.Text = "XN3";
            // 
            // pcs3
            // 
            this.pcs3.AutoSize = true;
            this.pcs3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs3.Location = new System.Drawing.Point(212, 261);
            this.pcs3.Name = "pcs3";
            this.pcs3.Size = new System.Drawing.Size(35, 16);
            this.pcs3.TabIndex = 4;
            this.pcs3.Text = "PCS";
            // 
            // S3
            // 
            this.S3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S3.Location = new System.Drawing.Point(75, 258);
            this.S3.Name = "S3";
            this.S3.Size = new System.Drawing.Size(120, 22);
            this.S3.TabIndex = 5;
            this.S3.Text = "0";
            // 
            // XN4
            // 
            this.XN4.AutoSize = true;
            this.XN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN4.Location = new System.Drawing.Point(27, 298);
            this.XN4.Name = "XN4";
            this.XN4.Size = new System.Drawing.Size(33, 16);
            this.XN4.TabIndex = 4;
            this.XN4.Text = "XN4";
            // 
            // pcs4
            // 
            this.pcs4.AutoSize = true;
            this.pcs4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs4.Location = new System.Drawing.Point(212, 300);
            this.pcs4.Name = "pcs4";
            this.pcs4.Size = new System.Drawing.Size(35, 16);
            this.pcs4.TabIndex = 4;
            this.pcs4.Text = "PCS";
            // 
            // S4
            // 
            this.S4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S4.Location = new System.Drawing.Point(75, 296);
            this.S4.Name = "S4";
            this.S4.Size = new System.Drawing.Size(120, 22);
            this.S4.TabIndex = 5;
            this.S4.Text = "0";
            // 
            // XN5
            // 
            this.XN5.AutoSize = true;
            this.XN5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN5.Location = new System.Drawing.Point(27, 337);
            this.XN5.Name = "XN5";
            this.XN5.Size = new System.Drawing.Size(33, 16);
            this.XN5.TabIndex = 4;
            this.XN5.Text = "XN5";
            // 
            // pcs5
            // 
            this.pcs5.AutoSize = true;
            this.pcs5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs5.Location = new System.Drawing.Point(212, 337);
            this.pcs5.Name = "pcs5";
            this.pcs5.Size = new System.Drawing.Size(35, 16);
            this.pcs5.TabIndex = 4;
            this.pcs5.Text = "PCS";
            // 
            // S5
            // 
            this.S5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S5.Location = new System.Drawing.Point(75, 334);
            this.S5.Name = "S5";
            this.S5.Size = new System.Drawing.Size(120, 22);
            this.S5.TabIndex = 5;
            this.S5.Text = "0";
            // 
            // XN6
            // 
            this.XN6.AutoSize = true;
            this.XN6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN6.Location = new System.Drawing.Point(27, 376);
            this.XN6.Name = "XN6";
            this.XN6.Size = new System.Drawing.Size(33, 16);
            this.XN6.TabIndex = 4;
            this.XN6.Text = "XN6";
            // 
            // pcs6
            // 
            this.pcs6.AutoSize = true;
            this.pcs6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs6.Location = new System.Drawing.Point(212, 376);
            this.pcs6.Name = "pcs6";
            this.pcs6.Size = new System.Drawing.Size(35, 16);
            this.pcs6.TabIndex = 4;
            this.pcs6.Text = "PCS";
            // 
            // S6
            // 
            this.S6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S6.Location = new System.Drawing.Point(75, 372);
            this.S6.Name = "S6";
            this.S6.Size = new System.Drawing.Size(120, 22);
            this.S6.TabIndex = 5;
            this.S6.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.FromIncoming);
            this.panel1.Location = new System.Drawing.Point(355, 133);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 33);
            this.panel1.TabIndex = 3;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Seperate_Paint);
            // 
            // FromIncoming
            // 
            this.FromIncoming.AutoSize = true;
            this.FromIncoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromIncoming.Location = new System.Drawing.Point(9, 7);
            this.FromIncoming.Name = "FromIncoming";
            this.FromIncoming.Size = new System.Drawing.Size(198, 20);
            this.FromIncoming.TabIndex = 0;
            this.FromIncoming.Text = "Crack Cells From Incoming";
            this.FromIncoming.Click += new System.EventHandler(this.FromIncoming_Click);
            // 
            // BoxNum
            // 
            this.BoxNum.AutoSize = true;
            this.BoxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNum.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BoxNum.Location = new System.Drawing.Point(352, 188);
            this.BoxNum.Name = "BoxNum";
            this.BoxNum.Size = new System.Drawing.Size(89, 16);
            this.BoxNum.TabIndex = 4;
            this.BoxNum.Text = "Box Number1";
            // 
            // BoxNumTextbox
            // 
            this.BoxNumTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNumTextbox.Location = new System.Drawing.Point(453, 185);
            this.BoxNumTextbox.Name = "BoxNumTextbox";
            this.BoxNumTextbox.Size = new System.Drawing.Size(120, 22);
            this.BoxNumTextbox.TabIndex = 5;
            // 
            // Qty
            // 
            this.Qty.AutoSize = true;
            this.Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Qty.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Qty.Location = new System.Drawing.Point(352, 225);
            this.Qty.Name = "Qty";
            this.Qty.Size = new System.Drawing.Size(28, 16);
            this.Qty.TabIndex = 4;
            this.Qty.Text = "Qty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(538, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "PCS";
            // 
            // BoxId
            // 
            this.BoxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxId.Location = new System.Drawing.Point(402, 222);
            this.BoxId.Name = "BoxId";
            this.BoxId.Size = new System.Drawing.Size(120, 22);
            this.BoxId.TabIndex = 5;
            this.BoxId.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(352, 308);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Box Number2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(352, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Qty";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(538, 345);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "PCS";
            // 
            // BoxNumtextBox2
            // 
            this.BoxNumtextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNumtextBox2.Location = new System.Drawing.Point(453, 305);
            this.BoxNumtextBox2.Name = "BoxNumtextBox2";
            this.BoxNumtextBox2.Size = new System.Drawing.Size(120, 22);
            this.BoxNumtextBox2.TabIndex = 5;
            // 
            // BoxId2
            // 
            this.BoxId2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxId2.Location = new System.Drawing.Point(402, 342);
            this.BoxId2.Name = "BoxId2";
            this.BoxId2.Size = new System.Drawing.Size(120, 22);
            this.BoxId2.TabIndex = 5;
            this.BoxId2.Text = "0";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(355, 271);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(218, 10);
            this.progressBar1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(680, 133);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(217, 33);
            this.panel2.TabIndex = 3;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Seperate_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Crack Cells From Accident";
            this.label5.Click += new System.EventHandler(this.FromIncoming_Click);
            // 
            // accident
            // 
            this.accident.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accident.Location = new System.Drawing.Point(680, 223);
            this.accident.Name = "accident";
            this.accident.Size = new System.Drawing.Size(120, 22);
            this.accident.TabIndex = 9;
            this.accident.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(816, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "PCS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(677, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Total Quantity";
            // 
            // Review
            // 
            this.Review.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Review.Location = new System.Drawing.Point(680, 85);
            this.Review.Name = "Review";
            this.Review.Size = new System.Drawing.Size(116, 33);
            this.Review.TabIndex = 10;
            this.Review.Text = "Review";
            this.Review.UseVisualStyleBackColor = true;
            this.Review.Click += new System.EventHandler(this.Review_Click);
            // 
            // Submit
            // 
            this.Submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Submit.Location = new System.Drawing.Point(75, 410);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(116, 33);
            this.Submit.TabIndex = 10;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // Modify
            // 
            this.Modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Modify.Location = new System.Drawing.Point(355, 410);
            this.Modify.Name = "Modify";
            this.Modify.Size = new System.Drawing.Size(116, 33);
            this.Modify.TabIndex = 10;
            this.Modify.Text = "Modify";
            this.Modify.UseVisualStyleBackColor = true;
            this.Modify.Click += new System.EventHandler(this.Modify_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(30, 465);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(867, 287);
            this.dataGridView1.TabIndex = 11;
            // 
            // shift
            // 
            this.shift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shift.FormattingEnabled = true;
            this.shift.Items.AddRange(new object[] {
            "D1",
            "N1",
            "D2",
            "N2"});
            this.shift.Location = new System.Drawing.Point(440, 85);
            this.shift.Name = "shift";
            this.shift.Size = new System.Drawing.Size(133, 33);
            this.shift.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(350, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 31);
            this.label8.TabIndex = 0;
            this.label8.Text = "Shift";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(677, 338);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "Total Quantity";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(680, 280);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(217, 33);
            this.panel3.TabIndex = 3;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.Seperate_Paint);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(181, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Crack Cells From Others";
            this.label10.Click += new System.EventHandler(this.FromIncoming_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(816, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 16);
            this.label11.TabIndex = 7;
            this.label11.Text = "PCS";
            // 
            // others
            // 
            this.others.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.others.Location = new System.Drawing.Point(680, 370);
            this.others.Name = "others";
            this.others.Size = new System.Drawing.Size(120, 22);
            this.others.TabIndex = 9;
            this.others.Text = "0";
            // 
            // FormLineACellLoss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(954, 738);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.S4);
            this.Controls.Add(this.Clock);
            this.Controls.Add(this.Review);
            this.Controls.Add(this.Modify);
            this.Controls.Add(this.BoxNumTextbox);
            this.Controls.Add(this.LineA);
            this.Controls.Add(this.BoxId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.MOLabel);
            this.Controls.Add(this.S1);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.BoxNumtextBox2);
            this.Controls.Add(this.shift);
            this.Controls.Add(this.MO);
            this.Controls.Add(this.XN4);
            this.Controls.Add(this.BoxId2);
            this.Controls.Add(this.Seperate);
            this.Controls.Add(this.pcs3);
            this.Controls.Add(this.others);
            this.Controls.Add(this.accident);
            this.Controls.Add(this.XN5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pcs4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.XN3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.XN6);
            this.Controls.Add(this.XN1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.S2);
            this.Controls.Add(this.BoxNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.S6);
            this.Controls.Add(this.pcs5);
            this.Controls.Add(this.pcs1);
            this.Controls.Add(this.Qty);
            this.Controls.Add(this.S5);
            this.Controls.Add(this.S3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pcs2);
            this.Controls.Add(this.XN2);
            this.Controls.Add(this.pcs6);
            this.Name = "FormLineACellLoss";
            this.Text = "FormLineACellLoss";
            this.Load += new System.EventHandler(this.FormLineACellLoss_Load);
            this.Seperate.ResumeLayout(false);
            this.Seperate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LineA;
        private System.Windows.Forms.ComboBox MO;
        private System.Windows.Forms.Label MOLabel;
        private System.Windows.Forms.Label Clock;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel Seperate;
        private System.Windows.Forms.Label FromStringer;
        private System.Windows.Forms.Label XN1;
        private System.Windows.Forms.TextBox S1;
        private System.Windows.Forms.Label pcs1;
        private System.Windows.Forms.Label XN2;
        private System.Windows.Forms.Label pcs2;
        private System.Windows.Forms.TextBox S2;
        private System.Windows.Forms.Label XN3;
        private System.Windows.Forms.Label pcs3;
        private System.Windows.Forms.TextBox S3;
        private System.Windows.Forms.Label XN4;
        private System.Windows.Forms.Label pcs4;
        private System.Windows.Forms.TextBox S4;
        private System.Windows.Forms.Label XN5;
        private System.Windows.Forms.Label pcs5;
        private System.Windows.Forms.TextBox S5;
        private System.Windows.Forms.Label XN6;
        private System.Windows.Forms.Label pcs6;
        private System.Windows.Forms.TextBox S6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label FromIncoming;
        private System.Windows.Forms.Label BoxNum;
        private System.Windows.Forms.TextBox BoxNumTextbox;
        private System.Windows.Forms.Label Qty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BoxId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BoxNumtextBox2;
        private System.Windows.Forms.TextBox BoxId2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox accident;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Review;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox shift;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox others;
    }
}