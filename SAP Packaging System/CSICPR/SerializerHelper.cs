﻿using System;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace CSICPR
{
    //对象序列化帮助类
    /// <summary>
    /// 对象序列化帮助类
    /// </summary>
    public class SerializerHelper
    {
        /// <summary>
        /// 对象到XML-----泛类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string BuildXmlByObject<T>(T obj)
        {
            if (Equals(obj, default(T)))
                return null;

            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            try
            {
                serializer.Serialize(stream, obj);
            }
            catch
            {
                return null;
            }
            stream.Position = 0;
            var xmlString = new StringBuilder();
            using (var sr = new StreamReader(stream, Encoding.UTF8))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    xmlString.AppendLine(line);
                }
            }
            return xmlString.ToString();

         

        }

        /// <summary>
        /// 对象到XML-----泛类型 指定xml字符串的encoding
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="obj">对象</param>
        /// <param name="encoding">编码</param>
        /// <returns></returns>
        public static string BuildXmlByObject<T>(T obj, Encoding encoding)
        {
            if (Equals(obj, default(T)))
                return null;

            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, encoding);
            var xmlns = new XmlSerializerNamespaces();
            xmlns.Add(String.Empty, String.Empty);
            try
            {
                serializer.Serialize(writer, obj, xmlns);
            }
            catch
            {
                return null;
            }

            stream.Position = 0;
            var xmlString = new StringBuilder();
            using (var sr = new StreamReader(stream, encoding))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    xmlString.AppendLine(line);
                }
            }
            return xmlString.ToString();
        }

        //XML到反序列化到对象----支持泛类型
        /// <summary>
        /// XML到反序列化到对象----支持泛类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static T BuildObjectByXmlString<T>(string xmlString)
        {
            if (string.IsNullOrEmpty(xmlString))
                return default(T);

            using (var stream = new MemoryStream())
            {
                using (var sw = new StreamWriter(stream, Encoding.UTF8))
                {
                    sw.Write(xmlString);
                    sw.Flush();
                    stream.Seek(0, SeekOrigin.Begin);
                    var serializer = new XmlSerializer(typeof(T));
                    try
                    {
                        return ((T)serializer.Deserialize(stream));
                    }
                    catch
                    {
                        //TODO:写日志
                        throw new Exception("Xml 文件格式不正确，请检查格式及数据是否正确！");
                    }
                }
            }
        }
    }
}