﻿namespace CSICPR
{
    partial class FormSAPTransferLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.plTransfer = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtMcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMitem = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lstView = new System.Windows.Forms.ListView();
            this.panel2.SuspendLayout();
            this.plTransfer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.plTransfer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(915, 58);
            this.panel2.TabIndex = 0;
            // 
            // plTransfer
            // 
            this.plTransfer.Controls.Add(this.btnReset);
            this.plTransfer.Controls.Add(this.txtYear);
            this.plTransfer.Controls.Add(this.btnSave);
            this.plTransfer.Controls.Add(this.txtMcode);
            this.plTransfer.Controls.Add(this.label3);
            this.plTransfer.Controls.Add(this.txtMitem);
            this.plTransfer.Controls.Add(this.label5);
            this.plTransfer.Controls.Add(this.label4);
            this.plTransfer.Location = new System.Drawing.Point(15, 14);
            this.plTransfer.Name = "plTransfer";
            this.plTransfer.Size = new System.Drawing.Size(884, 28);
            this.plTransfer.TabIndex = 50;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(796, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 56;
            this.btnReset.Text = "重置";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtYear
            // 
            this.txtYear.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtYear.Location = new System.Drawing.Point(555, 3);
            this.txtYear.MaxLength = 24;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(146, 21);
            this.txtYear.TabIndex = 55;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(716, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "下载";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtMcode
            // 
            this.txtMcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMcode.Location = new System.Drawing.Point(84, 2);
            this.txtMcode.MaxLength = 24;
            this.txtMcode.Name = "txtMcode";
            this.txtMcode.Size = new System.Drawing.Size(146, 21);
            this.txtMcode.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "凭  证  号";
            // 
            // txtMitem
            // 
            this.txtMitem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMitem.Location = new System.Drawing.Point(316, 3);
            this.txtMitem.MaxLength = 24;
            this.txtMitem.Name = "txtMitem";
            this.txtMitem.Size = new System.Drawing.Size(146, 21);
            this.txtMitem.TabIndex = 54;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(483, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "年       份";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(242, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "行   项  目";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 61);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lstView);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(915, 498);
            this.splitContainer1.SplitterDistance = 394;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 1;
            // 
            // lstView
            // 
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 0);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(915, 498);
            this.lstView.TabIndex = 2;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
            // 
            // FormSAPTransferLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 562);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel2);
            this.Name = "FormSAPTransferLoad";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "物料转储数据下载";
            this.Load += new System.EventHandler(this.FormSAPMASTERLoad_Load);
            this.panel2.ResumeLayout(false);
            this.plTransfer.ResumeLayout(false);
            this.plTransfer.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel plTransfer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.TextBox txtMitem;
        private System.Windows.Forms.TextBox txtMcode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.Button btnReset;
    }
}