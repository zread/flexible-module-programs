﻿namespace CSICPR
{
    partial class FormCancelStorge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        	System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.button1 = new System.Windows.Forms.Button();
        	this.btnCancel = new System.Windows.Forms.Button();
        	this.ddlQuery = new System.Windows.Forms.ComboBox();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.label14 = new System.Windows.Forms.Label();
        	this.PlCarton = new System.Windows.Forms.Panel();
        	this.txtCarton = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.plDate = new System.Windows.Forms.Panel();
        	this.label3 = new System.Windows.Forms.Label();
        	this.dtpPackingFrom = new System.Windows.Forms.DateTimePicker();
        	this.label4 = new System.Windows.Forms.Label();
        	this.dtpPackingTo = new System.Windows.Forms.DateTimePicker();
        	this.splitContainer1 = new System.Windows.Forms.SplitContainer();
        	this.chbSelected = new System.Windows.Forms.CheckBox();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        	this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CartonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CartonStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.SNQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.StorageDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.panel4 = new System.Windows.Forms.Panel();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.panel2.SuspendLayout();
        	this.PlCarton.SuspendLayout();
        	this.plDate.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
        	this.splitContainer1.Panel1.SuspendLayout();
        	this.splitContainer1.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.panel4.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.button1);
        	this.panel2.Controls.Add(this.btnCancel);
        	this.panel2.Controls.Add(this.ddlQuery);
        	this.panel2.Controls.Add(this.btnQuery);
        	this.panel2.Controls.Add(this.label14);
        	this.panel2.Controls.Add(this.PlCarton);
        	this.panel2.Controls.Add(this.plDate);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel2.Location = new System.Drawing.Point(3, 3);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(778, 101);
        	this.panel2.TabIndex = 0;
        	// 
        	// button1
        	// 
        	this.button1.Location = new System.Drawing.Point(488, 57);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(68, 24);
        	this.button1.TabIndex = 53;
        	this.button1.Text = "重置";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.button1_Click);
        	// 
        	// btnCancel
        	// 
        	this.btnCancel.Location = new System.Drawing.Point(568, 57);
        	this.btnCancel.Name = "btnCancel";
        	this.btnCancel.Size = new System.Drawing.Size(73, 24);
        	this.btnCancel.TabIndex = 52;
        	this.btnCancel.Text = "撤销入库";
        	this.btnCancel.UseVisualStyleBackColor = true;
        	this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
        	// 
        	// ddlQuery
        	// 
        	this.ddlQuery.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlQuery.FormattingEnabled = true;
        	this.ddlQuery.Location = new System.Drawing.Point(104, 15);
        	this.ddlQuery.Name = "ddlQuery";
        	this.ddlQuery.Size = new System.Drawing.Size(169, 21);
        	this.ddlQuery.TabIndex = 0;
        	this.ddlQuery.SelectedIndexChanged += new System.EventHandler(this.ddlQuery_SelectedIndexChanged);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(404, 57);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(68, 24);
        	this.btnQuery.TabIndex = 2;
        	this.btnQuery.Text = "查询";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// label14
        	// 
        	this.label14.AutoSize = true;
        	this.label14.Location = new System.Drawing.Point(28, 18);
        	this.label14.Name = "label14";
        	this.label14.Size = new System.Drawing.Size(64, 13);
        	this.label14.TabIndex = 5;
        	this.label14.Text = "查 询 条 件";
        	// 
        	// PlCarton
        	// 
        	this.PlCarton.Controls.Add(this.txtCarton);
        	this.PlCarton.Controls.Add(this.label1);
        	this.PlCarton.Location = new System.Drawing.Point(26, 56);
        	this.PlCarton.Name = "PlCarton";
        	this.PlCarton.Size = new System.Drawing.Size(329, 34);
        	this.PlCarton.TabIndex = 49;
        	// 
        	// txtCarton
        	// 
        	this.txtCarton.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtCarton.Location = new System.Drawing.Point(158, 4);
        	this.txtCarton.MaxLength = 24;
        	this.txtCarton.Name = "txtCarton";
        	this.txtCarton.Size = new System.Drawing.Size(168, 20);
        	this.txtCarton.TabIndex = 52;
        	this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(3, 8);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(64, 13);
        	this.label1.TabIndex = 6;
        	this.label1.Text = "内 部 托 号";
        	// 
        	// plDate
        	// 
        	this.plDate.Controls.Add(this.label3);
        	this.plDate.Controls.Add(this.dtpPackingFrom);
        	this.plDate.Controls.Add(this.label4);
        	this.plDate.Controls.Add(this.dtpPackingTo);
        	this.plDate.Location = new System.Drawing.Point(26, 56);
        	this.plDate.Name = "plDate";
        	this.plDate.Size = new System.Drawing.Size(249, 34);
        	this.plDate.TabIndex = 50;
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label3.Location = new System.Drawing.Point(1, 4);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(21, 14);
        	this.label3.TabIndex = 0;
        	this.label3.Text = "从";
        	// 
        	// dtpPackingFrom
        	// 
        	this.dtpPackingFrom.CustomFormat = "yyyy-MM-dd";
        	this.dtpPackingFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpPackingFrom.Location = new System.Drawing.Point(26, 1);
        	this.dtpPackingFrom.Name = "dtpPackingFrom";
        	this.dtpPackingFrom.Size = new System.Drawing.Size(97, 20);
        	this.dtpPackingFrom.TabIndex = 5;
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.label4.Location = new System.Drawing.Point(126, 4);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(21, 14);
        	this.label4.TabIndex = 6;
        	this.label4.Text = "到";
        	// 
        	// dtpPackingTo
        	// 
        	this.dtpPackingTo.CustomFormat = "yyyy-MM-dd";
        	this.dtpPackingTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dtpPackingTo.Location = new System.Drawing.Point(148, 1);
        	this.dtpPackingTo.Name = "dtpPackingTo";
        	this.dtpPackingTo.Size = new System.Drawing.Size(99, 20);
        	this.dtpPackingTo.TabIndex = 7;
        	// 
        	// splitContainer1
        	// 
        	this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
        	this.splitContainer1.IsSplitterFixed = true;
        	this.splitContainer1.Location = new System.Drawing.Point(3, 104);
        	this.splitContainer1.Name = "splitContainer1";
        	this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	// 
        	// splitContainer1.Panel1
        	// 
        	this.splitContainer1.Panel1.Controls.Add(this.chbSelected);
        	this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
        	this.splitContainer1.Panel1.Controls.Add(this.panel4);
        	this.splitContainer1.Panel2Collapsed = true;
        	this.splitContainer1.Size = new System.Drawing.Size(778, 502);
        	this.splitContainer1.SplitterDistance = 432;
        	this.splitContainer1.SplitterWidth = 2;
        	this.splitContainer1.TabIndex = 1;
        	// 
        	// chbSelected
        	// 
        	this.chbSelected.AutoSize = true;
        	this.chbSelected.Location = new System.Drawing.Point(21, 4);
        	this.chbSelected.Name = "chbSelected";
        	this.chbSelected.Size = new System.Drawing.Size(15, 14);
        	this.chbSelected.TabIndex = 4;
        	this.chbSelected.UseVisualStyleBackColor = true;
        	this.chbSelected.Click += new System.EventHandler(this.chbSelected_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowDrop = true;
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.AllowUserToResizeColumns = false;
        	this.dataGridView1.AllowUserToResizeRows = false;
        	dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
        	this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
        	this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        	dataGridViewCellStyle2.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        	dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        	dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        	dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        	this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        	this.dataGridView1.ColumnHeadersHeight = 20;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.Selected,
        	        	        	this.RowIndex,
        	        	        	this.CartonID,
        	        	        	this.CartonStatus,
        	        	        	this.CustomerCartonNo,
        	        	        	this.SNQTY,
        	        	        	this.StorageDate,
        	        	        	this.Column8});
        	dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
        	dataGridViewCellStyle3.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.PeachPuff;
        	dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
        	dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        	this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
        	this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dataGridView1.Location = new System.Drawing.Point(0, 0);
        	this.dataGridView1.Name = "dataGridView1";
        	dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        	dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
        	dataGridViewCellStyle4.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
        	dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        	dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        	dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        	this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
        	this.dataGridView1.RowHeadersVisible = false;
        	this.dataGridView1.RowTemplate.Height = 23;
        	this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        	this.dataGridView1.Size = new System.Drawing.Size(778, 422);
        	this.dataGridView1.TabIndex = 2;
        	// 
        	// Selected
        	// 
        	this.Selected.HeaderText = "";
        	this.Selected.IndeterminateValue = "";
        	this.Selected.Name = "Selected";
        	this.Selected.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        	this.Selected.Width = 50;
        	// 
        	// RowIndex
        	// 
        	this.RowIndex.HeaderText = "序号";
        	this.RowIndex.Name = "RowIndex";
        	this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	this.RowIndex.Width = 60;
        	// 
        	// CartonID
        	// 
        	this.CartonID.HeaderText = "内部托号";
        	this.CartonID.Name = "CartonID";
        	this.CartonID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// CartonStatus
        	// 
        	this.CartonStatus.HeaderText = "托号状态";
        	this.CartonStatus.Name = "CartonStatus";
        	this.CartonStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// CustomerCartonNo
        	// 
        	this.CustomerCartonNo.HeaderText = "客户托号";
        	this.CustomerCartonNo.Name = "CustomerCartonNo";
        	// 
        	// SNQTY
        	// 
        	this.SNQTY.HeaderText = "组件数量";
        	this.SNQTY.Name = "SNQTY";
        	this.SNQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// StorageDate
        	// 
        	this.StorageDate.HeaderText = "入库日期";
        	this.StorageDate.Name = "StorageDate";
        	this.StorageDate.Width = 150;
        	// 
        	// Column8
        	// 
        	this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
        	this.Column8.HeaderText = " ";
        	this.Column8.Name = "Column8";
        	this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        	// 
        	// panel4
        	// 
        	this.panel4.Controls.Add(this.lstView);
        	this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.panel4.Location = new System.Drawing.Point(0, 422);
        	this.panel4.Name = "panel4";
        	this.panel4.Size = new System.Drawing.Size(778, 80);
        	this.panel4.TabIndex = 3;
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(0, 0);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(778, 80);
        	this.lstView.TabIndex = 2;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// FormCancelStorge
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 609);
        	this.Controls.Add(this.splitContainer1);
        	this.Controls.Add(this.panel2);
        	this.Name = "FormCancelStorge";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "撤销入库";
        	this.Load += new System.EventHandler(this.FormCancelStorge_Load);
        	this.panel2.ResumeLayout(false);
        	this.panel2.PerformLayout();
        	this.PlCarton.ResumeLayout(false);
        	this.PlCarton.PerformLayout();
        	this.plDate.ResumeLayout(false);
        	this.plDate.PerformLayout();
        	this.splitContainer1.Panel1.ResumeLayout(false);
        	this.splitContainer1.Panel1.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
        	this.splitContainer1.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.panel4.ResumeLayout(false);
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox ddlQuery;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel plDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpPackingFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpPackingTo;
        private System.Windows.Forms.Panel PlCarton;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.CheckBox chbSelected;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn StorageDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Button button1;
    }
}