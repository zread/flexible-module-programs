﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace CSICPR
{
    public partial class FormNoPackge : Form
    {
        private string workshop = "";
        private string[] workshopSet = { "M02", "M03", "M07" };//"M01","M04",
        private string sqlConn = "";
        private string currentWorkShopDBConn = "";
        private int SchemeInterID = 0;

        public FormNoPackge(bool isMix = false, decimal line = 0)
        {
            InitializeComponent();
            currentWorkShopDBConn = FormCover.connectionBase;
        }

        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        public DataGridViewSelectedRowCollection GetMixCPT
        {
            get { return dgvData.SelectedRows; }
        }
        public string GetWorkshop
        {
            get { return cbWorkShop.Text; }
        }

        private void FormNoPackge_Load(object sender, EventArgs e)
        {
            workshop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");
            for (int i = 0; i < workshopSet.Length; i++)
            {
                if (!workshop.ToUpper().Equals(workshopSet[i]))
                    cbWorkShop.Items.Add(workshopSet[i]);
            }
            cbWorkShop.Enabled = false;
            lblScheme.Text = workshop + "车间标称功率方案：";
        }

        private void btMix_Click(object sender, EventArgs e)
        {
            dgvData.Rows.Clear();
            tbScheme.Text = "";
            cbWorkShop.Enabled = false;
            
        }

        private void tbBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                Thread.Sleep(50);

                string strTemp = tbBarCode.Text;
                string[] sDataSet = strTemp.Split('\n');
                for (int i = 0; i < sDataSet.Length; i++)
                {
                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    if (sDataSet[i] != null && sDataSet[i] != "")
                    {
                        if (tbScheme.Text.Length > 0)
                        {
                            if (string.IsNullOrEmpty(cbWorkShop.Text))
                            {
                                MessageBox.Show("请选择组件来源车间", "警告信息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            if (string.IsNullOrEmpty(sqlConn))
                                sqlConn = ToolsClass.getConfig(cbWorkShop.Text + "DB", false, "", "config.xml");
                            InsertData(sDataSet[i]);
                        }
                        else
                            SchemeInterID = GetSchemeInterID(sDataSet[i]);
                    }
                }
                tbBarCode.Clear();
                if (dgvData.Rows.Count > 0)
                    dgvData.FirstDisplayedScrollingRowIndex = dgvData.Rows.Count - 1;
            }
        }

        private void cbWorkShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbWorkShop.Text.Length > 0)
                sqlConn = ToolsClass.getConfig(cbWorkShop.Text+"DB", false, "", "config.xml");
        }

        /// <summary>
        /// 提取数据后插入当前车间数据库
        /// </summary>
        /// <param name="strSN"></param>
        private void InsertData(string strSN)
        {
            int interid = 0;
            insertProduct = "";
            insertElecParaTest = "";
            if (isPacked(strSN, workshop))
            {
                Product product = new Product();
                ElecParaTest test = new ElecParaTest();
                if (workshop == "M01")
                {
                }
                else
                {
                    #region M02-M03-M07
                    string sqlFromProduct = "select SN,modeltype,cellspec,celltype,cellcolor,process,boxid,ctmgrade,operatorid,art_no,pattern,processdatetime from product where interid=(select max(interid) from product where sn='{0}')";
                    string sqlFromTest = "select FNumber,Temp,VOC,ISC,Pmax,Vm,Im,FF,Eff from ElecParaTest where interid=(select max(interid) from ElecParaTest where fnumber='{0}')";
                    using (SqlDataReader readProduct = ToolsClass.GetDataReader(string.Format(sqlFromProduct, strSN), sqlConn))
                    {
                        if (readProduct.Read())
                        {
                            product.sn = readProduct.IsDBNull(0) ? "" : readProduct.GetString(0);
                            product.modeltype = readProduct.IsDBNull(1) ? "" : readProduct.GetString(1);
                            product.cellspec = readProduct.IsDBNull(2) ? "" : readProduct.GetString(2);
                            product.celltype = readProduct.IsDBNull(3) ? "" : readProduct.GetString(3);
                            product.cellcolor = readProduct.IsDBNull(4) ? "" : readProduct.GetString(4);
                            product.process = readProduct.IsDBNull(5) ? "" : readProduct.GetString(5);
                            product.boxid = readProduct.IsDBNull(6) ? "" : readProduct.GetString(6);
                            product.ctmgrade = readProduct.IsDBNull(7) ? "" : readProduct.GetString(7);
                            product.operatorid = readProduct.IsDBNull(8) ? "" : readProduct.GetString(8);
                            product.artno = readProduct.IsDBNull(9) ? "" : readProduct.GetString(9);
                            product.pattern = readProduct.IsDBNull(10) ? "" : readProduct.GetString(10);
                            product.processdatetime = readProduct.IsDBNull(11) ? "" : readProduct.GetSqlDateTime(11).ToString();
                        }
                    }
                    using (SqlDataReader readTest = ToolsClass.GetDataReader(string.Format(sqlFromTest, strSN), sqlConn))
                    {
                        if (readTest.Read())
                        {
                            test.sn = readTest.IsDBNull(0) ? "" : readTest.GetString(0);
                            test.temp = readTest.IsDBNull(1) ? 0.00 : readTest.GetDouble(1);
                            test.voc = readTest.IsDBNull(2) ? 0.00 : readTest.GetDouble(2);
                            test.isc = readTest.IsDBNull(3) ? 0.00 : readTest.GetDouble(3);
                            test.pmax = readTest.IsDBNull(4) ? 0.00 : readTest.GetDouble(4);
                            test.vmp = readTest.IsDBNull(5) ? 0.00 : readTest.GetDouble(5);
                            test.imp = readTest.IsDBNull(6) ? 0.00 : readTest.GetDouble(6);
                            test.ff = readTest.IsDBNull(7) ? 0.00 : readTest.GetDouble(7);
                            test.eff = readTest.IsDBNull(8) ? 0.00 : readTest.GetDouble(8);
                            test.schemeinterid = SchemeInterID;
                        }
                    }
                    if (string.IsNullOrEmpty(product.sn) || string.IsNullOrEmpty(test.sn))
                    {
                        dgvData.Rows.Insert(dgvData.Rows.Count, new object[] { "无此数据", strSN, "", "", "", "", "", "", "" });
                        return;
                    }

                    insertElecParaTest = "insert into ElecParaTest(FNumber,Temp,VOC,ISC,Pmax,Vm,Im,FF,Eff,SchemeInterID) values";
                    insertElecParaTest = insertElecParaTest + "(";
                    insertElecParaTest = insertElecParaTest + "'" + test.sn + "',";
                    insertElecParaTest = insertElecParaTest + test.temp + ",";
                    insertElecParaTest = insertElecParaTest + test.voc + ",";
                    insertElecParaTest = insertElecParaTest + test.isc + ",";
                    insertElecParaTest = insertElecParaTest + test.pmax + ",";
                    insertElecParaTest = insertElecParaTest + test.vmp + ",";
                    insertElecParaTest = insertElecParaTest + test.imp + ",";
                    insertElecParaTest = insertElecParaTest + test.ff + ",";
                    insertElecParaTest = insertElecParaTest + test.eff + ",";
                    insertElecParaTest = insertElecParaTest + test.schemeinterid;
                    insertElecParaTest = insertElecParaTest + ");" + " SELECT SCOPE_IDENTITY();";

                    interid = ToolsClass.ExecuteScalarGetInterID(insertElecParaTest, currentWorkShopDBConn);

                    insertProduct = "insert into product(SN,modeltype,cellspec,celltype,cellcolor,process,boxid,ctmgrade,operatorid,art_no,pattern,processdatetime,InterNewTempTableID) VALUES";
                    insertProduct = insertProduct + "(";
                    insertProduct = insertProduct + "'" + product.sn + "',";
                    insertProduct = insertProduct + "'" + product.modeltype + "',";
                    insertProduct = insertProduct + "'" + product.cellspec + "',";
                    insertProduct = insertProduct + "'" + product.celltype + "',";
                    insertProduct = insertProduct + "'" + product.cellcolor + "',";
                    insertProduct = insertProduct + "'" + product.process + "',";
                    insertProduct = insertProduct + "'" + product.boxid + "',";
                    insertProduct = insertProduct + "'" + product.ctmgrade + "',";
                    insertProduct = insertProduct + "'" + product.operatorid + "',";
                    insertProduct = insertProduct + "'" + product.artno + "',";
                    insertProduct = insertProduct + "'" + product.pattern + "',";
                    insertProduct = insertProduct + "'" + product.processdatetime + "',";
                    insertProduct = insertProduct + interid;
                    insertProduct = insertProduct + ")";

                    ToolsClass.ExecuteNonQuery(insertProduct, currentWorkShopDBConn);
                    if (!string.IsNullOrEmpty(product.sn) && !string.IsNullOrEmpty(test.sn))
                        dgvData.Rows.Insert(dgvData.Rows.Count, new object[] { "提取完成", product.sn, test.pmax, test.imp, test.vmp, product.modeltype, product.celltype, cbWorkShop.Text, tbScheme.Text });
                    //else
                    //    dgvData.Rows.Insert(dgvData.Rows.Count, new object[] { "无此数据", strSN, "", "", "", "", "", "","" });
                    #endregion
                }
            }
        }

        /// <summary>
        /// 获取当前车间待拼箱的组件运行方案--用于计算标称功率
        /// </summary>
        /// <param name="strSN"></param>
        private int GetSchemeInterID(string strSN)
        {
            int interid = 0;
            string strIDSql = "select [SchemeInterID] from [ElecParaTest] where [InterID] =(select max([InterID]) from [ElecParaTest] where [FNumber]='{0}')";
            strIDSql = string.Format(strIDSql, strSN);
            object obj = ToolsClass.ExecuteScalar(strIDSql);
            interid = obj == null ? 0 : int.Parse(obj.ToString());

            if (interid != 0)
            {
                string strSchemeSQL = "select SchemeID from scheme where interid=" + interid;
                tbScheme.Text = ToolsClass.ExecuteScalar(strSchemeSQL).ToString();
            }
            else
                MessageBox.Show("无法获取组件：" + strSN + " 的方案名称，请确认此组件是否是在" + workshop + "车间生产的！");

            return interid;
        }

        /// <summary>
        /// 验证拼箱的组件是否已经被打包
        /// </summary>
        /// <param name="strSN"></param>
        /// <returns></returns>
        private bool isPacked(string strSN,string work_shop)
        {
            string sql = "";
            bool rtn = true;
            string sBoxID = "";

            if (work_shop == "M01")
                sql = "select Carton from T_Module_Packing where ModuleSN='{0}'";
            else
                sql = "select BoxID from Product where SN='{0}'";

            sql = string.Format(sql, strSN);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql,sqlConn);
            while (sdr != null && sdr.Read())
            {
                sBoxID = sdr.GetString(0);
                if (!sBoxID.Trim().Equals(""))
                {
                    MessageBox.Show(strSN + " 已经打包在:" + sBoxID);
                    rtn = false;
                    break;
                }
            }
            sdr.Close(); sdr = null;
            return rtn;
        }

        private string insertProduct = "";
        private string insertElecParaTest = "";

        private void tbScheme_TextChanged(object sender, EventArgs e)
        {
            if (tbScheme.Text.Length > 0)
                cbWorkShop.Enabled = true;
        }
    }

    #region class Product and ElecParaTest
    class Product
    {
        public string sn = "";
        public string modeltype = "";
        public string cellspec = "";
        public string celltype = "";
        public string cellcolor = "";
        public string process = "";
        public string boxid = "";
        public string processdatetime = "";
        public string ctmgrade = "";
        public string operatorid = "";
        public string artno = "";
        public string pattern = "";
    }

    class ElecParaTest
    {
        public string sn = "";
        public double temp = 0.00;
        public double voc = 0.00;
        public double isc = 0.00;
        public double imp = 0.00;
        public double vmp = 0.00;
        public double pmax = 0.00;
        public double eff = 0.00;
        public double ff = 0.00;
        public int schemeinterid = 0;
        //public string Date = "";
        //public string Time = "";
    }
    #endregion

}
