﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;
using CSICPR.Properties;

namespace CSICPR
{
    public partial class FrmSapInventoryDataQuery : Form
    {
        public FrmSapInventoryDataQuery()
        {
            InitializeComponent();
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FrmSapInventoryDataQuery _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmSapInventoryDataQuery
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        private string _esbAddress = string.Empty;

        private List<SapInventoryData> _sapInventoryDatas = new List<SapInventoryData>();

        private void RebindGrid()
        {
            if (_sapInventoryDatas == null)
                _sapInventoryDatas = new List<SapInventoryData>();
            dgvData.DataSource = null;
            dgvData.DataSource = _sapInventoryDatas;

            lblModuleCntValue.Text = _sapInventoryDatas.Select(p => p.MODULE_SN).Distinct().Count().ToString();
            lblTestPowerSumValue.Text = _sapInventoryDatas.Select(p => p.TestPower).Sum().ToString();
        }

        private void FrmSapInventoryUploadResult_Load(object sender, EventArgs e)
        {
            cmbQueryType.SelectedIndex = 0;

            dgvData.AutoGenerateColumns = false;

            RebindGrid();

            _esbAddress = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString);



            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.cmbQueryType);

            # endregion
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            _sapInventoryDatas = new List<SapInventoryData>();
            RebindGrid();
            var url = _esbAddress;
            url = url + "QueryInventoryData?workshop=" + FormCover.CurrentFactory ;
            var selectedIndex = cmbQueryType.SelectedIndex;
            switch (selectedIndex)
            {
                case 0:
                    var orderNo = txtOrderNo.Text.Trim();
                    if (string.IsNullOrEmpty(orderNo))
                    {
                        ToolsClass.Log("btnQuery_Click::工单不能为空");
                        MessageBox.Show("工单号不能为空");
                        return;
                    }
                    url = url + "&orderno=" + orderNo;
                    break;
                case 1:
                    var jobNo = txtJobNo.Text.Trim();
                    if (string.IsNullOrEmpty(jobNo))
                    {
                        ToolsClass.Log("btnQuery_Click::出货柜号不能为空");
                        MessageBox.Show("出货柜号不能为空");
                        return;
                    }
                    url = url + "&jobno=" + jobNo;
                    break;
                case 2:
                    var cartonNos = txtCartonNos.Lines;
                    if (cartonNos.Length <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::托号不能为空");
                        MessageBox.Show("托号不能为空");
                        return;
                    }
                    var listCartonNos = new List<string>();
                    foreach (var cartonNo in cartonNos)
                    {
                        if (string.IsNullOrEmpty(cartonNo) || cartonNo.Trim().Length <= 0)
                            continue;
                        listCartonNos.Add(cartonNo);
                    }
                    if (listCartonNos.Count <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::托号不能为空");
                        MessageBox.Show("托号不能为空");
                        return;
                    }
                    url = url + "&cartonno=" + string.Join(",", listCartonNos.ToArray());
                    break;
                case 3:
                    var customerCartonNos = txtCustomerCartonNos.Lines;
                    if (customerCartonNos.Length <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::客户托号不能为空");
                        MessageBox.Show("客户托号不能为空");
                        return;
                    }
                    var listCustmerCartonNos = new List<string>();
                    foreach (var cartonNo in customerCartonNos)
                    {
                        if (string.IsNullOrEmpty(cartonNo) || cartonNo.Trim().Length <= 0)
                            continue;
                        listCustmerCartonNos.Add(cartonNo);
                    }
                    if (listCustmerCartonNos.Count <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::客户托号不能为空");
                        MessageBox.Show("客户托号不能为空");
                        return;
                    }
                    url = url + "&customercartonno=" + string.Join(",", listCustmerCartonNos.ToArray());
                    break;
                case 4:
                    var moduleSns = txtModuleSns.Lines;
                    if (moduleSns.Length <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::组件序列号不能为空");
                        MessageBox.Show("组件序列号不能为空");
                        return;
                    }
                    var listModuleSns = new List<string>();
                    foreach (var moduleSn in moduleSns)
                    {
                        if (string.IsNullOrEmpty(moduleSn) || moduleSn.Trim().Length <= 0)
                            continue;
                        listModuleSns.Add(moduleSn);
                    }
                    if (listModuleSns.Count <= 0)
                    {
                        ToolsClass.Log("btnQuery_Click::组件序列号不能为空");
                        MessageBox.Show("组件序列号不能为空");
                        return;
                    }
                    url = url + "&modulesn=" + string.Join(",", listModuleSns.ToArray());
                    break;
                case 5:
                    if (dptFrom.Value > dptTo.Value)
                    {
                        ToolsClass.Log("时间范围开始值不可大于结束值");
                        MessageBox.Show("时间范围开始值不可大于结束值");
                        return;
                    }
                    if (dptTo.Value.AddDays(-7) > dptFrom.Value)
                    {
                        ToolsClass.Log("查询的时间范围不能大于7天");
                        MessageBox.Show("查询的时间范围不能大于7天");
                        return;
                    }
                    var from = dptFrom.Value.ToString(dptFrom.CustomFormat);
                    url += "&uploaddatefrom=" + from;
                    var to = dptTo.Value.ToString(dptTo.CustomFormat);
                    url += "&uploaddateto=" + to;
                    break;
            }


            var req = WebRequest.Create(url);
            ToolsClass.Log("btnQuery_Click::" + url);
            req.Method = "GET";
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log("服务器反馈结果为空");
                    MessageBox.Show("服务器反馈结果为空");
                    return;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "连接错误";
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("btnQuery_Click::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<SOAP:BODY>", @"<SOAPBODY>");
                result = result.Replace(@"</SOAP:BODY>", @"</SOAPBODY>");
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log("服务器反馈结果与接口定义不符");
                    MessageBox.Show("服务器反馈结果与接口定义不符");
                    return;
                }
                var responseStatusTable = ds.Tables["RESPONSESTATUS"];
                if (responseStatusTable == null)
                {
                    ToolsClass.Log("服务器反馈结果与接口定义不符");
                    MessageBox.Show("服务器反馈结果与接口定义不符");
                    return;
                }
                var statusCode = responseStatusTable.Rows[0][0].ToString();
                if (statusCode == "000")
                {
                    var responseContentTable = ds.Tables["ITEM"];
                    if (responseContentTable == null || responseContentTable.Rows == null || responseContentTable.Rows.Count <= 0)
                    {
                        ToolsClass.Log("服务器反馈结果为空");
                        MessageBox.Show("服务器反馈结果为空");
                        return;
                    }
                    var sapInventoryDatas = new List<SapInventoryData>();
                    foreach (DataRow row in responseContentTable.Rows)
                    {
                        var sapInventoryData = new SapInventoryData
                        {
                            GUID = row["GUID"].ToString(),
                            CARTON_NO = row["CARTON_NO"].ToString(),
                            CUSTOMER_CARTON_NO = row["CUSTOMER_CARTON_NO"].ToString(),
                            JOB_NO = row["JOB_NO"].ToString(),
                            INNER_JOB_NO = row["INNER_JOB_NO"].ToString(),
                            FINISHED_ON = row["FINISHED_ON"].ToString(),
                            MODULE_COLOR = row["MODULE_COLOR"].ToString(),
                            MODULE_SN = row["MODULE_SN"].ToString(),
                            ORDER_NO = row["ORDER_NO"].ToString(),
                            SALES_ORDER_NO = row["SALES_ORDER_NO"].ToString(),
                            SALES_ITEM_NO = row["SALES_ITEM_NO"].ToString(),
                            ORDER_STATUS = row["ORDER_STATUS"].ToString(),
                            PRODUCT_CODE = row["PRODUCT_CODE"].ToString(),
                            UNIT = row["UNIT"].ToString(),
                            FACTORY = row["FACTORY"].ToString(),
                            WORKSHOP = row["WORKSHOP"].ToString(),
                            PACKING_LOCATION = row["PACKING_LOCATION"].ToString(),
                            PACKING_MODE = row["PACKING_MODE"].ToString(),
                            CELL_EFF = row["CELL_EFF"].ToString(),
                            TEST_POWER = row["TEST_POWER"].ToString(),
                            STD_POWER = row["STD_POWER"].ToString(),
                            MODULE_GRADE = row["MODULE_GRADE"].ToString(),
                            BYIM = row["BYIM"].ToString(),
                            TOLERANCE = row["TOLERANCE"].ToString(),
                            CELL_CODE = row["CELL_CODE"].ToString(),
                            CELL_BATCH = row["CELL_BATCH"].ToString(),
                            CELL_PRINT_MODE = row["CELL_PRINT_MODE"].ToString(),
                            GLASS_CODE = row["GLASS_CODE"].ToString(),
                            GLASS_BATCH = row["GLASS_BATCH"].ToString(),
                            EVA_CODE = row["EVA_CODE"].ToString(),
                            EVA_BATCH = row["EVA_BATCH"].ToString(),
                            TPT_CODE = row["TPT_CODE"].ToString(),
                            TPT_BATCH = row["TPT_BATCH"].ToString(),
                            CONBOX_CODE = row["CONBOX_CODE"].ToString(),
                            CONBOX_BATCH = row["CONBOX_BATCH"].ToString(),
                            CONBOX_TYPE = row["CONBOX_TYPE"].ToString(),
                            LONG_FRAME_CODE = row["LONG_FRAME_CODE"].ToString(),
                            LONG_FRAME_BATCH = row["LONG_FRAME_BATCH"].ToString(),
                            SHORT_FRAME_CODE = row["SHORT_FRAME_CODE"].ToString(),
                            SHORT_FRAME_BATCH = row["SHORT_FRAME_BATCH"].ToString(),
                            GLASS_THICKNESS = row["GLASS_THICKNESS"].ToString(),
                            IS_REWORK = row["IS_REWORK"].ToString(),
                            IS_BY_PRODUCTION = row["IS_BY_PRODUCTION"].ToString(),
                            BATCH_NO = row["BATCH_NO"].ToString(),
                            VOUCHER = row["VOUCHER"].ToString(),
                            CREATE_ON = row["CREATE_ON"].ToString()
                        };
                        sapInventoryDatas.Add(sapInventoryData);
                    }
                    _sapInventoryDatas = sapInventoryDatas;
                    RebindGrid();
                    return;
                }
                if (statusCode == "001")
                {
                    ToolsClass.Log("服务器反馈结果：参数异常");
                    MessageBox.Show("服务器反馈结果：参数异常");
                    return;
                }
                if (statusCode == "002")
                {
                    ToolsClass.Log("服务器反馈结果：系统异常");
                    MessageBox.Show("服务器反馈结果：系统异常");
                    return;
                }
                if (statusCode == "003")
                {
                    ToolsClass.Log("服务器反馈结果：正在处理中");
                    MessageBox.Show("服务器反馈结果：正在处理中");
                    return;
                }
                ToolsClass.Log("服务器反馈结果：StatusCode异常");
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer);
                MessageBox.Show(responseFromServer);
                return;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message);
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _sapInventoryDatas = new List<SapInventoryData>();
            cmbQueryType.SelectedIndex = 0;
            RebindGrid();
        }

        private void cmbQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOrderNo.Visible = false;
            txtJobNo.Visible = false;
            txtCartonNos.Visible = false;
            txtCustomerCartonNos.Visible = false;
            txtModuleSns.Visible = false;
            dptFrom.Visible = false;
            dptTo.Visible = false;
            pnlTop.Height = 70;
            btnQuery.Location = new Point(466, 38);
            btnReset.Location = new Point(550, 38);
            btnExport.Location = new Point(635, 38);
            var selectedIndex = cmbQueryType.SelectedIndex;
            switch (selectedIndex)
            {
                case 0:
                    txtOrderNo.Clear();
                    txtOrderNo.Visible = true;
                    txtOrderNo.Focus();
                    break;
                case 1:
                    txtJobNo.Clear();
                    txtJobNo.Visible = true;
                    txtJobNo.Focus();
                    break;
                case 2:
                    txtCartonNos.Clear();
                    txtCartonNos.Visible = true;
                    txtCartonNos.Focus();
                    pnlTop.Height = 126;
                    btnQuery.Location = new Point(466, 92);
                    btnReset.Location = new Point(550, 92);
                    btnExport.Location = new Point(635, 92);
                    break;
                case 3:
                    txtCustomerCartonNos.Clear();
                    txtCustomerCartonNos.Visible = true;
                    txtCustomerCartonNos.Focus();
                    pnlTop.Height = 126;
                    btnQuery.Location = new Point(466, 92);
                    btnReset.Location = new Point(550, 92);
                    btnExport.Location = new Point(635, 92);
                    break;
                case 4:
                    txtModuleSns.Clear();
                    txtModuleSns.Visible = true;
                    txtModuleSns.Focus();
                    pnlTop.Height = 126;
                    btnQuery.Location = new Point(466, 92);
                    btnReset.Location = new Point(550, 92);
                    btnExport.Location = new Point(635, 92);
                    break;
                case 5:
                    dptFrom.Visible = true;
                    dptTo.Visible = true;
                    break;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataToExcel(dgvData);
        }

        public void DataToExcel(DataGridView m_DataView)
        {
            SaveFileDialog kk = new SaveFileDialog(); 
            kk.Title = "保存EXECL文件"; 
            kk.Filter = "EXECL文件(*.xls)|*.xls|所有文件(*.*)|*.*"; 
            kk.FilterIndex = 1;
            if (kk.ShowDialog() == DialogResult.OK) 
            { 
                string FileName = kk.FileName + ".xls";
                if (File.Exists(FileName))
                    File.Delete(FileName);
                FileStream objFileStream; 
                StreamWriter objStreamWriter; 
                string strLine = ""; 
                objFileStream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write); 
                objStreamWriter = new StreamWriter(objFileStream, System.Text.Encoding.Unicode);
                for (int i = 0; i  < m_DataView.Columns.Count; i++) 
                { 
                    if (m_DataView.Columns[i].Visible == true) 
                    { 
                        strLine = strLine + m_DataView.Columns[i].HeaderText.ToString() + Convert.ToChar(9); 
                    } 
                } 
                objStreamWriter.WriteLine(strLine); 
                strLine = ""; 

                for (int i = 0; i  < m_DataView.Rows.Count; i++) 
                { 
                    if (m_DataView.Columns[0].Visible == true) 
                    { 
                        if (m_DataView.Rows[i].Cells[0].Value == null) 
                            strLine = strLine + " " + Convert.ToChar(9); 
                        else 
                            strLine = strLine + m_DataView.Rows[i].Cells[0].Value.ToString() + Convert.ToChar(9); 
                    } 
                    for (int j = 1; j  < m_DataView.Columns.Count; j++) 
                    { 
                        if (m_DataView.Columns[j].Visible == true) 
                        { 
                            if (m_DataView.Rows[i].Cells[j].Value == null) 
                                strLine = strLine + " " + Convert.ToChar(9); 
                            else 
                            { 
                                string rowstr = ""; 
                                rowstr = m_DataView.Rows[i].Cells[j].Value.ToString(); 
                                if (rowstr.IndexOf("\r\n") >  0) 
                                    rowstr = rowstr.Replace("\r\n", " "); 
                                if (rowstr.IndexOf("\t") >  0) 
                                    rowstr = rowstr.Replace("\t", " "); 
                                strLine = strLine + rowstr + Convert.ToChar(9); 
                            } 
                        } 
                    } 
                    objStreamWriter.WriteLine(strLine); 
                    strLine = ""; 
                } 
                objStreamWriter.Close(); 
                objFileStream.Close();
                MessageBox.Show(this,"保存EXCEL成功","提示",MessageBoxButtons.OK,MessageBoxIcon.Information); 
            }
        }
    }

    public class SapInventoryData
    {
        public string GUID { get; set; }
        public string CARTON_NO { get; set; }
        public string CUSTOMER_CARTON_NO { get; set; }
        public string JOB_NO { get; set; }
        public string INNER_JOB_NO { get; set; }
        public string FINISHED_ON { get; set; }
        public string MODULE_COLOR { get; set; }
        public string MODULE_SN { get; set; }
        public string ORDER_NO { get; set; }
        public string SALES_ORDER_NO { get; set; }
        public string SALES_ITEM_NO { get; set; }
        public string ORDER_STATUS { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string UNIT { get; set; }
        public string FACTORY { get; set; }
        public string WORKSHOP { get; set; }
        public string PACKING_LOCATION { get; set; }
        public string PACKING_MODE { get; set; }
        public string CELL_EFF { get; set; }
        public string TEST_POWER { get; set; }
        public string STD_POWER { get; set; }
        public string MODULE_GRADE { get; set; }
        public string BYIM { get; set; }
        public string TOLERANCE { get; set; }
        public string CELL_CODE { get; set; }
        public string CELL_BATCH { get; set; }
        public string CELL_PRINT_MODE { get; set; }
        public string GLASS_CODE { get; set; }
        public string GLASS_BATCH { get; set; }
        public string EVA_CODE { get; set; }
        public string EVA_BATCH { get; set; }
        public string TPT_CODE { get; set; }
        public string TPT_BATCH { get; set; }
        public string CONBOX_CODE { get; set; }
        public string CONBOX_BATCH { get; set; }
        public string CONBOX_TYPE { get; set; }
        public string LONG_FRAME_CODE { get; set; }
        public string LONG_FRAME_BATCH { get; set; }
        public string SHORT_FRAME_CODE { get; set; }
        public string SHORT_FRAME_BATCH { get; set; }
        public string GLASS_THICKNESS { get; set; }
        public string IS_REWORK { get; set; }
        public string IS_BY_PRODUCTION { get; set; }
        public string BATCH_NO { get; set; }
        public string VOUCHER { get; set; }
        public string CREATE_ON { get; set; }

        public decimal TestPower
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(TEST_POWER))
                        return 0;

                    return decimal.Parse(TEST_POWER);
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}