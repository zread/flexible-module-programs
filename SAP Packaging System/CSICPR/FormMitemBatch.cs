﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormMitemBatch : Form
    {     

        public string  Batch="";
        public string  MitemCode = "";
        private string _Batch="";
        private string _MitemType = "";
        private string _WO = "";
        private string _WOTYPE = "";
        int StorageCount = 0;
    
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }

        private void SetDataGridViewData(string MitemBatch,string WOQuerytype,string flag)
        {
            DataTable dt = ProductStorageDAL.GetBatchInfo(MitemBatch, WOQuerytype, _WO, _WOTYPE);
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["batchno"].Value = Convert.ToString(row["Batch"]);
                    this.dataGridView1.Rows[RowNo].Cells["MaterialCode"].Value = Convert.ToString(row["MaterialCode"]);
                    this.dataGridView1.Rows[RowNo].Cells["MaterialDescription"].Value = Convert.ToString(row["MaterialDescription"]);
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                if (!flag.Equals("Y"))
                {
                    MessageBox.Show("没有查询到数据");
                    this.txtBatch.SelectAll();
                    this.txtBatch.Focus();
                    return;
                }
            }
        }

        public FormMitemBatch()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 批次信息
        /// </summary>
        /// <param name="batch">批次号</param>
        /// <param name="MitemType">物料类型</param>
        /// <param name="wo">物料类型</param>
        /// <param name="wotype">工单类型</param>
        public FormMitemBatch(string batch,string MitemType,string wo,string wotype)
        {
            InitializeComponent();
            _Batch = batch;
            _MitemType = MitemType;
            _WO = wo;
            _WOTYPE = wotype;
        }
        private void FormCartonInfoQuery_Load(object sender, EventArgs e)
        {
            this.Text =_MitemType +":批次选择";

            this.PalBatch.Visible = true;
            this.txtBatch.Text = _Batch;
            this.txtBatch.SelectAll();
            this.txtBatch.Focus();

            SetDataGridViewData(_Batch, _MitemType,"Y");
            
        }
 
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData(this.txtBatch.Text.Trim().ToString(), _MitemType,"");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Batch = this.dataGridView1.CurrentRow.Cells["batchno"].Value.ToString();
            MitemCode = this.dataGridView1.CurrentRow.Cells["MaterialCode"].Value.ToString();
            this.Close();
        }

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }
        private void dataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Batch = this.dataGridView1.CurrentRow.Cells["batchno"].Value.ToString();
                MitemCode = this.dataGridView1.CurrentRow.Cells["MaterialCode"].Value.ToString();
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Batch = this.dataGridView1.Rows[e.RowIndex].Cells["batchno"].Value.ToString();
            MitemCode = this.dataGridView1.Rows[e.RowIndex].Cells["MaterialCode"].Value.ToString();
            this.Close();
        }
    }
}
