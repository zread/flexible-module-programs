﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;



namespace CSICPR
{
    public partial class FormCancelStorge : Form
    {
        public FormCancelStorge()
        {
            InitializeComponent();
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private List<CARTONPACKINGTRANSACTION> _ListCartonSucess = new List<CARTONPACKINGTRANSACTION>();
        private List<MODULEPACKINGTRANSACTION> _ListModuleSucess = new List<MODULEPACKINGTRANSACTION>();
        #region 共有变量
        private static FormCancelStorge theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormCancelStorge();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        string QueryType = "";
        private DateTime StorageDateFrom;
        private DateTime StorageDateTo;
        string carton = "";
        private DataTable dts;
        List<List<string>> CartonList = new List<List<string>>();
        
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        #endregion

        private void FormCancelStorge_Load(object sender, EventArgs e)
        {
            this.ddlQuery.Items.Insert(0, "入库日期");
            this.ddlQuery.Items.Insert(1, "内部托号");

            //默认查询方式
            this.ddlQuery.SelectedIndex = 1;
            this.plDate.Visible = false;
            this.PlCarton.Visible = true;

            this.lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlQuery);
            # endregion

        }

        #region 页面事件
        /// <summary>
        /// 查询条件选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlQuery_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlQuery.SelectedIndex == 0)
            {
                QueryType = "StorageDate";
                this.plDate.Visible = true;
                this.PlCarton.Visible = false;
                if (this.dataGridView1.Rows.Count > 0)
                    this.dataGridView1.Rows.Clear();
            }
            else if (this.ddlQuery.SelectedIndex == 1)
            {
                QueryType = "Cartom";
                this.plDate.Visible = false;
                this.PlCarton.Visible = true;
                if (this.dataGridView1.Rows.Count > 0)
                    this.dataGridView1.Rows.Clear();
            }
        }
        #endregion

        /// <summary>
        /// 是否全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            StorageDateFrom = Convert.ToDateTime(this.dtpPackingFrom.Value);
            StorageDateTo = Convert.ToDateTime(this.dtpPackingTo.Value);
            carton = Convert.ToString(this.txtCarton.Text.Trim());
            DataTable dt = ProductStorageDAL.GetCancelStorageInfo(QueryType, carton, StorageDateFrom, StorageDateTo);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string cartonStatus = "";
                    if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        cartonStatus = "已测试";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        cartonStatus = "已包装";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        cartonStatus = "已入库";
                    else
                        cartonStatus = "已入库";

                    if (checkrepeat(Convert.ToString(row["CartonID"])))
                        dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { false, this.dataGridView1.Rows.Count + 1, Convert.ToString(row["CartonID"]), cartonStatus, Convert.ToString(row["Cust_BoxID"]), Convert.ToString(row["SNQTY"]), Convert.ToString(row["StorageDate"]) });

                }
                this.txtCarton.Clear();
                this.txtCarton.Focus();
            }
            else
            {
                ToolsClass.Log("没有查到已入库的托号!", "ABNORMAL", lstView);
                if (QueryType.Equals("StorageDate"))
                    this.dtpPackingFrom.Focus();
                else
                {
                    this.txtCarton.Clear();
                    this.txtCarton.Focus();
                }
                return;
            }
        }

        private bool checkrepeat(string carton)
        {
            foreach (DataGridViewRow datarow in dataGridView1.Rows)
            {
                if (datarow.Cells[2].Value != null && datarow.Cells[2].Value.Equals(carton))
                {
                    ToolsClass.Log("托号：" + carton + " 已在列表第 " + (datarow.Index + 1) + " 行", "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 取消入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnQuery.Enabled = false;
                this.button1.Enabled = false;
                if (this.dataGridView1.Rows.Count < 1)
                {
                    this.btnQuery.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("没有要取消入库的数据,请确认", "ABNORMAL", lstView);
                    return;
                }

                if (CartonList.Count > 0)
                    CartonList.Clear();
                //if (_ListCartonSucess.Count > 0)
                //    _ListCartonSucess.Clear();

                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if ((bool)dgvRow.Cells[0].Value)
                    {
                        List<string> CartonListArray = new List<string>();

                        if ((!Convert.ToString(dgvRow.Cells["CartonID"].Value).Equals("")) &&
                            (!Convert.ToString(dgvRow.Cells["CustomerCartonNo"].Value).Equals("")))
                        {
                            CartonListArray.Add(Convert.ToString(dgvRow.Cells["CartonID"].Value));
                            CartonListArray.Add(Convert.ToString(dgvRow.Cells["CustomerCartonNo"].Value));
                        }

                        CartonList.Add(CartonListArray);
                    }
                }

                if (CartonList.Count < 1)
                {
                    this.btnQuery.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("没有选中要撤销的托号", "ABNORMAL", lstView);
                    return;
                }
                string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                string posttime = DT.DateTime().LongDateTime;
                for (int i=0; i < CartonList.Count; i++)
                {
                    //#region 撤消入库交易
                    //dts = new DataTable();
                    //dts = ProductStorageDAL.CancelStorageSucess(Convert.ToString(CartonList[i][0]));
                    //#endregion

                   string msg ="";
                   if (CancelSapStorage(Convert.ToString(CartonList[i][0]),Convert.ToString(CartonList[i][1])))
                    {
                        //#region 写入库交易记录
                        //if (_ListCartonSucess.Count > 0)
                        //{
                        //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "UnInvertory");
                        //}
                        //#endregion
                        
                        if (ProductStorageDAL.UpdateCancelStorageInfo(Convert.ToString(CartonList[i][0]), out msg,joinid,posttime))
                        {
                            ProductStorageDAL.SaveStorageInfoLog(_ListSapWo);
                            ToolsClass.Log("内部托号:" + Convert.ToString(Convert.ToString(CartonList[i][0])) + "撤销入库成功", "NORMAL", lstView);
                            foreach (DataGridViewRow row in this.dataGridView1.Rows)
                            {
                                if (Convert.ToString(row.Cells["CartonID"].Value).Equals(Convert.ToString(Convert.ToString(CartonList[i][0]))))
                                    this.dataGridView1.Rows.Remove(row);
                            }
                            /*
                            #region　拼撤消入库交易list
                            if (dts != null && dts.Rows.Count > 0)
                            {
                                CARTONPACKINGTRANSACTION cartonpacking = new CARTONPACKINGTRANSACTION();
                                cartonpacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                                cartonpacking.Orgnization = "CS";
                                cartonpacking.Carton = Convert.ToString(CartonList[i][0]);
                                cartonpacking.JobNo = "";
                                cartonpacking.Action = "UnInvertory";
                                cartonpacking.StdPowerLevel = dts.Rows[0]["StdPower"].ToString();
                                cartonpacking.ActionTxnID = joinid;
                                cartonpacking.ActionDate = posttime;
                                cartonpacking.ActionBy = FormCover.CurrUserName;
                                cartonpacking.ModuleQty = dts.Rows.Count.ToString();
                                cartonpacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
   
                                    _ListCartonSucess.Add(cartonpacking);

                                foreach (DataRow rows in dts.Rows)
                                {
                                    MODULEPACKINGTRANSACTION modulepacking = new MODULEPACKINGTRANSACTION();
                                    modulepacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                                    modulepacking.ModuleSn = Convert.ToString(rows["SN"]);
                                    modulepacking.CartonNoSysID = cartonpacking.SysID;
                                    modulepacking.ProductNo = "";
                                    modulepacking.ModuleType = Convert.ToString(rows["ModelType"]);
                                    modulepacking.ModuleStdPower = Convert.ToString(rows["StdPower"]);
                                    modulepacking.Pmax = Convert.ToString(rows["Pmax"]);
                                    modulepacking.ModuleStdPower = Convert.ToString(rows["StdPower"]);
                                    modulepacking.Workshop = FormCover.CurrentFactory.ToUpper().Trim();
                                    modulepacking.Temp = Convert.ToString(rows["TEMP"]);
                                    modulepacking.VOC = Convert.ToString(rows["VOC"]);
                                    modulepacking.ISC = Convert.ToString(rows["ISC"]);
                                    modulepacking.IMP = Convert.ToString(rows["IM"]);
                                    modulepacking.VMP = Convert.ToString(rows["VM"]);
                                    modulepacking.FF = Convert.ToString(rows["FF"]);
                                    modulepacking.EFF = Convert.ToString(rows["EFF"]);
                                    modulepacking.TestDate = Convert.ToString(rows["TESTTIME"]);
                                    modulepacking.WorkOrder = Convert.ToString(rows["WORKORDER"]);
                                    modulepacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                                    
                                        _ListModuleSucess.Add(modulepacking);
                                }

                            }
                            #endregion
                            */
                        }
                        else
                        {
                            ToolsClass.Log("托号:" + Convert.ToString(Convert.ToString(CartonList[i][0])) + " 在包装系统撤销入库失败,原因:" + msg + "", "ABNORMAL", lstView);
                        }
                    }
                    else
                    {
                        ToolsClass.Log("托号:" + Convert.ToString(Convert.ToString(CartonList[i][0])) + " 在SAP撤销入库失败,原因：" + msg + "", "ABNORMAL", lstView);
                    }
                }
                /*
                #region  写撤消入库交易
                string msg1 = "";
                if (!new CartonPackingTransactionDal().SaveStorage(_ListCartonSucess,_ListModuleSucess,out msg1))
                {
                    ToolsClass.Log("整托入库记录数据保存失败:" + msg1 + "", "ABNORMAL");
                    return;
                }
                #endregion
                 * */
            

                this.btnQuery.Enabled = true;
                this.button1.Enabled = true;
                this.chbSelected.Checked = false;
            }
            catch(Exception ex)
            {
                this.btnQuery.Enabled = true;
                this.button1.Enabled = true;
                ToolsClass.Log("撤销入库时发生了异常"+ex.Message+"", "ABNORMAL", lstView);
            }
        }

        private bool CancelSapStorage(string carton, string custcarton)
        {
            #region 生成XML,并上传SAP
            if (_ListSapWo.Count > 0)
                _ListSapWo.Clear();

            DateTime CurrentTime = DateTime.Now;
            string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期
            string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码
            DataTable newdt = ProductStorageDAL.GetCartonInfo(carton, "");
            if (newdt == null || newdt.Rows.Count == 0)
            {
                ToolsClass.Log("托号：" + carton + "在包装系统获取组件信息失败", "ABNORMAL", lstView);
                return false;
            }
            foreach (DataRow newdtrow in newdt.Rows)
            {
                SapWoModule SapWo = new SapWoModule();
                SapWo.ActionCode = "D";
                SapWo.SysId = Convert.ToString(newdtrow["SN"]);
                SapWo.ModuleSN = Convert.ToString(newdtrow["SN"]);
                //if (!(Convert.ToString(newdtrow["ReWorkOrder"]).Equals("")))
                //    SapWo.OrderNo = Convert.ToString(newdtrow["ReWorkOrder"]);
                //else
                SapWo.OrderNo = Convert.ToString(newdtrow["WorkOrder"]);

                SapWo.CartonNo = Convert.ToString(carton);
                SapWo.CustomerCartonNo = custcarton;
                SapWo.IsCancelPacking = "2";
                SapWo.Workshop = FormCover.CurrentFactory.Trim();
                SapWo.PostedOn = PostedOn;
                _ListSapWo.Add(SapWo);
            }

            DataTable dt = new DataTable();
            string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(_ListSapWo, Encoding.UTF8);
            SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");

            ToolsClass.Log("内部托号" + carton + " 撤销入库数据保存");
            ToolsClass.Log(SapStorageString);
            try
            {
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                string aa = service.packingDataMesToSap(SapStorageString);
                string bb;
                dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
            }
            catch (Exception ex)
            {
                ToolsClass.Log("连接SAP时,发生异常." + ex.Message + "", "ABNORMAL", lstView);
                return false;
            }
            #endregion

            if (_ListSapWo.Count != dt.Rows.Count)
            {
                ToolsClass.Log("上传SAP和SAP返回的数据总数不一致", "ABNORMAL", lstView);
                return false;
            }

            //处理SAP返回的结果
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                   
                    #region
                    string flag = "Y";
                    for (int mmm = 0; mmm < _ListSapWo.Count; mmm++)
                    {
                        DataRow[] rows = dt.Select("key = '" + _ListSapWo[mmm].ModuleSN + "'");
                        if (rows.Length > 0)
                        {
                            //上传失败
                            if (Convert.ToString(rows[0]["Result"]).Equals("0"))
                            {
                                flag = "N";
                                ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:" + Convert.ToString(rows[0]["Message"]) + "", "ABNORMAL", lstView);
                                break;
                            }
                        }
                        else //上传SAP失败
                        {
                            flag = "N";
                            ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:SAP返回数据丢失", "ABNORMAL", lstView);
                            break;
                        }
                    }
                    //上传成功
                    if (flag.Equals("Y"))
                    {
                        return true;
                    }
                    else
                        return false;
                    #endregion
                }
                else
                {
                    ToolsClass.Log("上传SAP保存,返回数据错误", "ABNORMAL", lstView);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ToolsClass.Log("处理SAP返回数据时发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }
        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().Equals("\r"))
            {
                btnQuery_Click(null, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
            this.txtCarton.SelectAll();
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }
    }
}
