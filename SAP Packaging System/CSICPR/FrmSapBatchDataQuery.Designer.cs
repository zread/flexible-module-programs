﻿namespace CSICPR
{
    partial class FrmSapBatchDataQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.txtModuleSns = new System.Windows.Forms.TextBox();
            this.txtCartonNos = new System.Windows.Forms.TextBox();
            this.txtJobNo = new System.Windows.Forms.TextBox();
            this.cmbQueryType = new System.Windows.Forms.ComboBox();
            this.lblQueryType = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.txtModuleSns);
            this.pnlTop.Controls.Add(this.txtCartonNos);
            this.pnlTop.Controls.Add(this.txtJobNo);
            this.pnlTop.Controls.Add(this.cmbQueryType);
            this.pnlTop.Controls.Add(this.lblQueryType);
            this.pnlTop.Controls.Add(this.btnExport);
            this.pnlTop.Controls.Add(this.btnReset);
            this.pnlTop.Controls.Add(this.btnQuery);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(3, 3);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(942, 70);
            this.pnlTop.TabIndex = 0;
            // 
            // txtModuleSns
            // 
            this.txtModuleSns.AcceptsReturn = true;
            this.txtModuleSns.Location = new System.Drawing.Point(150, 39);
            this.txtModuleSns.Multiline = true;
            this.txtModuleSns.Name = "txtModuleSns";
            this.txtModuleSns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtModuleSns.Size = new System.Drawing.Size(180, 75);
            this.txtModuleSns.TabIndex = 60;
            this.txtModuleSns.Visible = false;
            // 
            // txtCartonNos
            // 
            this.txtCartonNos.AcceptsReturn = true;
            this.txtCartonNos.Location = new System.Drawing.Point(150, 39);
            this.txtCartonNos.Multiline = true;
            this.txtCartonNos.Name = "txtCartonNos";
            this.txtCartonNos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCartonNos.Size = new System.Drawing.Size(180, 75);
            this.txtCartonNos.TabIndex = 58;
            this.txtCartonNos.Visible = false;
            // 
            // txtJobNo
            // 
            this.txtJobNo.Location = new System.Drawing.Point(150, 39);
            this.txtJobNo.Name = "txtJobNo";
            this.txtJobNo.Size = new System.Drawing.Size(180, 21);
            this.txtJobNo.TabIndex = 57;
            this.txtJobNo.Visible = false;
            // 
            // cmbQueryType
            // 
            this.cmbQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueryType.FormattingEnabled = true;
            this.cmbQueryType.Items.AddRange(new object[] {
            "出货柜号",
            "托号",
            "组件序列号"});
            this.cmbQueryType.Location = new System.Drawing.Point(15, 39);
            this.cmbQueryType.Name = "cmbQueryType";
            this.cmbQueryType.Size = new System.Drawing.Size(121, 20);
            this.cmbQueryType.TabIndex = 55;
            this.cmbQueryType.SelectedIndexChanged += new System.EventHandler(this.cmbQueryType_SelectedIndexChanged);
            // 
            // lblQueryType
            // 
            this.lblQueryType.AutoSize = true;
            this.lblQueryType.Location = new System.Drawing.Point(15, 13);
            this.lblQueryType.Name = "lblQueryType";
            this.lblQueryType.Size = new System.Drawing.Size(53, 12);
            this.lblQueryType.TabIndex = 54;
            this.lblQueryType.Text = "查询条件";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(635, 38);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(90, 22);
            this.btnExport.TabIndex = 53;
            this.btnExport.Text = "导出到Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(550, 38);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(68, 22);
            this.btnReset.TabIndex = 53;
            this.btnReset.Text = "重置";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(466, 38);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(68, 22);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(3, 73);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.RowTemplate.Height = 23;
            this.dgvData.Size = new System.Drawing.Size(942, 280);
            this.dgvData.TabIndex = 1;
            // 
            // FrmSapBatchDataQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 356);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.pnlTop);
            this.Name = "FrmSapBatchDataQuery";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SAP批次特性数据查询";
            this.Load += new System.EventHandler(this.FrmSapInventoryUploadResult_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ComboBox cmbQueryType;
        private System.Windows.Forms.Label lblQueryType;
        private System.Windows.Forms.TextBox txtJobNo;
        private System.Windows.Forms.TextBox txtModuleSns;
        private System.Windows.Forms.TextBox txtCartonNos;
        private System.Windows.Forms.Button btnExport;
    }
}