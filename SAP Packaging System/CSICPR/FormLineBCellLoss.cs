﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormLineBCellLoss : Form
    {
        public FormLineBCellLoss()
        {
            InitializeComponent();
            List<string> returnList = new List<string>();
            SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB;User ID = reader; Password=12345");
            string sql = "select top 4 ProductionOrder FROM[LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where ProductionOrder like 'CB%' order by ID desc";
            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    returnList.Add(sdr[0].ToString());

                }
                foreach (string value in returnList)
                {
                    MO.Items.Add(value);


                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        private static FormLineBCellLoss theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormLineBCellLoss();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                {
                    theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
        private void FormLineBCellLoss_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ClockB.Text = DateTime.Now.ToString("yyyy-MMM-dd HH:mm");
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            Submit.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(shift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[BrokenCells] values('{0}','{1}','{2}','{3}','{4}','{5}',null,null,null,null,null,null,'{6}','{7}','{8}','{9}','{10}','{11}',getdate(),null)";
                sql = string.Format(sql, MO.Text.ToString(), shift.Text.ToString(), Convert.ToInt32(S1.Text), Convert.ToInt32(S2.Text), Convert.ToInt32(S3.Text), Convert.ToInt32(S4.Text), BoxNumTextbox.Text.ToString(), Convert.ToInt32(BoxId.Text), BoxNumtextBox2.Text.ToString(), Convert.ToInt32(BoxId2.Text), Convert.ToInt32(accident.Text), Convert.ToInt32(others.Text));
                Executenonequery(sql);
            }
            string sqlselect = "select ID,MO,Shift,s1 as XN1,S2 as XN2,S3 as XN3,S4 as XN4 ,Box1, Box1Qty as Incoming1,Box2,Box2Qty as Incoming2,Accident,Others,CreatedDate,ModifiedDate FROM [CAPA01DB01].[dbo].[BrokenCells] where MO = '" + MO.Text + "' and shift = '" + shift.Text + "' and CreatedDate >= GETDATE()-0.1";
            DataTable dt = RetrieveData(sqlselect);
            if (dt.Rows.Count >= 1)
            {
                MessageBox.Show("Broken Cells information go into database.");
            }
            else
            {
                MessageBox.Show("Please check value type of all the field.");
            }
            dataGridView1.DataSource = dt;
            Submit.Enabled = true;

        }
        private void Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                Mycommand.ExecuteNonQuery();
                conn.Close();



            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void Review_Click(object sender, EventArgs e)
        {
            Review.Enabled = false;
            S1.Text = "0";
            S2.Text = "0";
            S3.Text = "0";
            S4.Text = "0";
            BoxNumTextbox.Text = "";
            BoxId.Text = "0";
            BoxNumtextBox2.Text = "";
            BoxId2.Text = "0";
            accident.Text = "0";
            others.Text = "0";

            string sqlselect = "select ID,MO,Shift,s1 as XN1,S2 as XN2,S3 as XN3,S4 as XN4 ,Box1, Box1Qty as Incoming1,Box2,Box2Qty as Incoming2,Accident,Others,CreatedDate,ModifiedDate FROM [CAPA01DB01].[dbo].[BrokenCells] where MO = '" + MO.Text + "' and shift = '" + shift.Text + "' and CreatedDate >= GETDATE()-1";
            DataTable dt = RetrieveData(sqlselect);
            if (dt.Rows.Count < 1)
            {
                MessageBox.Show("MO and Shift can not be Null. Otherwise, no data is found.");
            }
            else
            {
                dataGridView1.DataSource = dt;
            }
            Review.Enabled = true;
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            Modify.Enabled = false;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
            string sql = "Update  [CAPA01DB01].[dbo].[BrokenCells] set  shift = '" + selectedRow.Cells["shift"].Value.ToString() + "',Box1 = '" + selectedRow.Cells["box1"].Value.ToString() + "',Box2 = '" + selectedRow.Cells["Box2"].Value.ToString() + "',S1 = '" + selectedRow.Cells["XN1"].Value + "',S2 = '" + selectedRow.Cells["XN2"].Value + "',S3 = '" + selectedRow.Cells["XN3"].Value + "',S4 = '" + selectedRow.Cells["XN4"].Value + "',Box1Qty = '" + selectedRow.Cells["Incoming1"].Value + "',Box2Qty = '" + selectedRow.Cells["Incoming2"].Value + "',accident = '" + selectedRow.Cells["accident"].Value + "',others = '" + selectedRow.Cells["others"].Value + "', ModifiedDate = GETDATE()  where  ID = '" + selectedRow.Cells["ID"].Value + "'";
            Executenonequery(sql);
            MessageBox.Show("This row is Modified. Please review again.");
            Modify.Enabled = true;
        }
    }
}
