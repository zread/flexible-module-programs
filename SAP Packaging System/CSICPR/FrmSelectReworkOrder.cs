﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FrmSelectReworkOrder : Form
    {
        public string OrderNo = "";
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private void ClearDataGridViewData()
        {
            if (dataGridView1.Rows.Count > 0)
                dataGridView1.Rows.Clear();
        }

        private void SetDataGridViewData(string Factory)
        {
            if (txtWo.Text.Trim().Equals(""))
                return;

            var dt = ProductStorageDAL.GetReworkWorkOrder(txtWo.Text.Trim(), Factory);
            if (dt != null && dt.Rows.Count > 0)
            {
                var rowIndex = 1;
                var rowNo = 0;
                foreach (DataRow row in dt.Rows)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[rowNo].Cells["RowIndex"].Value = rowIndex.ToString(CultureInfo.InvariantCulture);
                    dataGridView1.Rows[rowNo].Cells["WONO"].Value = Convert.ToString(row["TWO_NO"]);
                    dataGridView1.Rows[rowNo].Cells["Mitem"].Value = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    dataGridView1.Rows[rowNo].Cells["Order"].Value = Convert.ToString(row["TWO_ORDER_NO"]);
                    rowNo++;
                    rowIndex++;
                }
            }
            else
            {
                txtWo.SelectAll();
                MessageBox.Show("No Record found");
            }
        }

        public FrmSelectReworkOrder()
        {
            InitializeComponent();
        }

        public FrmSelectReworkOrder(string orderNo)
        {
            InitializeComponent();
            OrderNo = orderNo;
            txtWo.Text = orderNo;
        }

        #region 窗体页面事件
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData(FormCover.PlanCode);
    
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null) 
                OrderNo = dataGridView1.CurrentRow.Cells["WONO"].Value.ToString();
            Close();
        }

        #endregion

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null, null);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (dataGridView1.CurrentRow != null) 
                    OrderNo = dataGridView1.CurrentRow.Cells["WONO"].Value.ToString();
                Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            OrderNo = dataGridView1.Rows[e.RowIndex].Cells["WONO"].Value.ToString();
            Close();
        }

        private void FrmSelectReworkOrder_Load(object sender, EventArgs e)
        {
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            btnQuery_Click(null, null);
        }
    }
}