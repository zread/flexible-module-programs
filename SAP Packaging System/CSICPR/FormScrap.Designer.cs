﻿namespace CSICPR
{
    partial class FormScrap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.Review = new System.Windows.Forms.Button();
        	this.Modify = new System.Windows.Forms.Button();
        	this.Scrap = new System.Windows.Forms.Button();
        	this.Qty = new System.Windows.Forms.TextBox();
        	this.pcs = new System.Windows.Forms.Label();
        	this.quantity = new System.Windows.Forms.Label();
        	this.Shiftlabel = new System.Windows.Forms.Label();
        	this.MOLabel = new System.Windows.Forms.Label();
        	this.MO = new System.Windows.Forms.ComboBox();
        	this.Material = new System.Windows.Forms.Label();
        	this.line = new System.Windows.Forms.Label();
        	this.lineicon = new System.Windows.Forms.Label();
        	this.Shift = new System.Windows.Forms.ComboBox();
        	this.Conversion = new System.Windows.Forms.Label();
        	this.QR = new System.Windows.Forms.Button();
        	this.WHR = new System.Windows.Forms.Button();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Location = new System.Drawing.Point(477, 182);
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.Size = new System.Drawing.Size(1087, 251);
        	this.dataGridView1.TabIndex = 46;
        	this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
        	// 
        	// Review
        	// 
        	this.Review.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Review.Location = new System.Drawing.Point(477, 144);
        	this.Review.Name = "Review";
        	this.Review.Size = new System.Drawing.Size(118, 32);
        	this.Review.TabIndex = 45;
        	this.Review.Text = "Review";
        	this.Review.UseVisualStyleBackColor = true;
        	this.Review.Click += new System.EventHandler(this.Review_Click_1);
        	// 
        	// Modify
        	// 
        	this.Modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Modify.Location = new System.Drawing.Point(477, 439);
        	this.Modify.Name = "Modify";
        	this.Modify.Size = new System.Drawing.Size(118, 32);
        	this.Modify.TabIndex = 43;
        	this.Modify.Text = "Modify";
        	this.Modify.UseVisualStyleBackColor = true;
        	this.Modify.Click += new System.EventHandler(this.Modify_Click_1);
        	// 
        	// Scrap
        	// 
        	this.Scrap.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Scrap.Location = new System.Drawing.Point(193, 377);
        	this.Scrap.Name = "Scrap";
        	this.Scrap.Size = new System.Drawing.Size(118, 32);
        	this.Scrap.TabIndex = 42;
        	this.Scrap.Text = "Scrap";
        	this.Scrap.UseVisualStyleBackColor = true;
        	this.Scrap.Click += new System.EventHandler(this.Scrap_Click);
        	// 
        	// Qty
        	// 
        	this.Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
        	this.Qty.Location = new System.Drawing.Point(152, 302);
        	this.Qty.Name = "Qty";
        	this.Qty.Size = new System.Drawing.Size(202, 31);
        	this.Qty.TabIndex = 40;
        	// 
        	// pcs
        	// 
        	this.pcs.AutoSize = true;
        	this.pcs.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.pcs.ForeColor = System.Drawing.SystemColors.Control;
        	this.pcs.Location = new System.Drawing.Point(357, 299);
        	this.pcs.Name = "pcs";
        	this.pcs.Size = new System.Drawing.Size(60, 31);
        	this.pcs.TabIndex = 41;
        	this.pcs.Text = "pcs";
        	// 
        	// quantity
        	// 
        	this.quantity.AutoSize = true;
        	this.quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.quantity.ForeColor = System.Drawing.SystemColors.Control;
        	this.quantity.Location = new System.Drawing.Point(78, 302);
        	this.quantity.Name = "quantity";
        	this.quantity.Size = new System.Drawing.Size(69, 31);
        	this.quantity.TabIndex = 39;
        	this.quantity.Text = "Qty.";
        	// 
        	// Shiftlabel
        	// 
        	this.Shiftlabel.AutoSize = true;
        	this.Shiftlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Shiftlabel.ForeColor = System.Drawing.SystemColors.Control;
        	this.Shiftlabel.Location = new System.Drawing.Point(73, 219);
        	this.Shiftlabel.Name = "Shiftlabel";
        	this.Shiftlabel.Size = new System.Drawing.Size(74, 31);
        	this.Shiftlabel.TabIndex = 36;
        	this.Shiftlabel.Text = "Shift";
        	// 
        	// MOLabel
        	// 
        	this.MOLabel.AutoSize = true;
        	this.MOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.MOLabel.ForeColor = System.Drawing.SystemColors.Control;
        	this.MOLabel.Location = new System.Drawing.Point(72, 142);
        	this.MOLabel.Name = "MOLabel";
        	this.MOLabel.Size = new System.Drawing.Size(75, 31);
        	this.MOLabel.TabIndex = 35;
        	this.MOLabel.Text = "MO#";
        	// 
        	// MO
        	// 
        	this.MO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.MO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.MO.FormattingEnabled = true;
        	this.MO.Location = new System.Drawing.Point(152, 145);
        	this.MO.Name = "MO";
        	this.MO.Size = new System.Drawing.Size(202, 33);
        	this.MO.TabIndex = 28;
        	// 
        	// Material
        	// 
        	this.Material.AutoSize = true;
        	this.Material.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Material.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.Material.Location = new System.Drawing.Point(115, 69);
        	this.Material.Name = "Material";
        	this.Material.Size = new System.Drawing.Size(149, 39);
        	this.Material.TabIndex = 32;
        	this.Material.Text = "Material";
        	// 
        	// line
        	// 
        	this.line.AutoSize = true;
        	this.line.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.line.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.line.Location = new System.Drawing.Point(588, 69);
        	this.line.Name = "line";
        	this.line.Size = new System.Drawing.Size(41, 39);
        	this.line.TabIndex = 34;
        	this.line.Text = "X";
        	// 
        	// lineicon
        	// 
        	this.lineicon.AutoSize = true;
        	this.lineicon.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.lineicon.ForeColor = System.Drawing.SystemColors.ControlLightLight;
        	this.lineicon.Location = new System.Drawing.Point(470, 69);
        	this.lineicon.Name = "lineicon";
        	this.lineicon.Size = new System.Drawing.Size(107, 39);
        	this.lineicon.TabIndex = 30;
        	this.lineicon.Text = "Line :";
        	// 
        	// Shift
        	// 
        	this.Shift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Shift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Shift.FormattingEnabled = true;
        	this.Shift.Items.AddRange(new object[] {
			"D1",
			"D2",
			"N1",
			"N2"});
        	this.Shift.Location = new System.Drawing.Point(152, 219);
        	this.Shift.Name = "Shift";
        	this.Shift.Size = new System.Drawing.Size(202, 33);
        	this.Shift.TabIndex = 28;
        	this.Shift.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Shift_keyDown);
        	// 
        	// Conversion
        	// 
        	this.Conversion.AutoSize = true;
        	this.Conversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.Conversion.ForeColor = System.Drawing.SystemColors.Control;
        	this.Conversion.Location = new System.Drawing.Point(12, 344);
        	this.Conversion.Name = "Conversion";
        	this.Conversion.Size = new System.Drawing.Size(162, 31);
        	this.Conversion.TabIndex = 39;
        	this.Conversion.Text = "Conversion";
        	// 
        	// QR
        	// 
        	this.QR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.QR.Location = new System.Drawing.Point(84, 439);
        	this.QR.Name = "QR";
        	this.QR.Size = new System.Drawing.Size(164, 32);
        	this.QR.TabIndex = 47;
        	this.QR.Text = "Quality Reject";
        	this.QR.UseVisualStyleBackColor = true;
        	this.QR.Visible = false;
        	this.QR.Click += new System.EventHandler(this.QR_Click);
        	// 
        	// WHR
        	// 
        	this.WHR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.WHR.Location = new System.Drawing.Point(271, 439);
        	this.WHR.Name = "WHR";
        	this.WHR.Size = new System.Drawing.Size(164, 32);
        	this.WHR.TabIndex = 48;
        	this.WHR.Text = "WH Return";
        	this.WHR.UseVisualStyleBackColor = true;
        	this.WHR.Visible = false;
        	this.WHR.Click += new System.EventHandler(this.WHR_Click);
        	// 
        	// FormScrap
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackColor = System.Drawing.SystemColors.ActiveBorder;
        	this.ClientSize = new System.Drawing.Size(1604, 737);
        	this.Controls.Add(this.WHR);
        	this.Controls.Add(this.QR);
        	this.Controls.Add(this.dataGridView1);
        	this.Controls.Add(this.Review);
        	this.Controls.Add(this.Modify);
        	this.Controls.Add(this.Scrap);
        	this.Controls.Add(this.Qty);
        	this.Controls.Add(this.pcs);
        	this.Controls.Add(this.Conversion);
        	this.Controls.Add(this.quantity);
        	this.Controls.Add(this.Shiftlabel);
        	this.Controls.Add(this.MOLabel);
        	this.Controls.Add(this.Shift);
        	this.Controls.Add(this.MO);
        	this.Controls.Add(this.Material);
        	this.Controls.Add(this.line);
        	this.Controls.Add(this.lineicon);
        	this.Name = "FormScrap";
        	this.Text = "FormScrap";
        	this.Load += new System.EventHandler(this.FormScrap_Load);
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Review;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.Button Scrap;
        private System.Windows.Forms.TextBox Qty;
        private System.Windows.Forms.Label pcs;
        private System.Windows.Forms.Label quantity;
        private System.Windows.Forms.Label Shiftlabel;
        private System.Windows.Forms.Label MOLabel;
        private System.Windows.Forms.ComboBox MO;
        private System.Windows.Forms.Label Material;
        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Label lineicon;
        private System.Windows.Forms.ComboBox Shift;
        private System.Windows.Forms.Label Conversion;
        private System.Windows.Forms.Button QR;
        private System.Windows.Forms.Button WHR;
    }
}