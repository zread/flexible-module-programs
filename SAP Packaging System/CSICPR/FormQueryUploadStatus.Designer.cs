﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 4/13/2016
 * Time: 5:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormQueryUploadStatus
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.TbCarton = new System.Windows.Forms.TextBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Carton_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Process_Flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BtQuery = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.TbCarton);
			this.panel1.Location = new System.Drawing.Point(1, 1);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(321, 502);
			this.panel1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(11, 31);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 21);
			this.label1.TabIndex = 1;
			this.label1.Text = "Carton No:";
			// 
			// TbCarton
			// 
			this.TbCarton.Location = new System.Drawing.Point(90, 28);
			this.TbCarton.Multiline = true;
			this.TbCarton.Name = "TbCarton";
			this.TbCarton.Size = new System.Drawing.Size(193, 436);
			this.TbCarton.TabIndex = 0;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
									this.Carton_No,
									this.Status,
									this.Process_Flag});
			this.dataGridView1.Location = new System.Drawing.Point(328, 1);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(347, 663);
			this.dataGridView1.TabIndex = 1;
			// 
			// Carton_No
			// 
			this.Carton_No.HeaderText = "Carton No.";
			this.Carton_No.Name = "Carton_No";
			this.Carton_No.ReadOnly = true;
			// 
			// Status
			// 
			this.Status.HeaderText = "Status";
			this.Status.Name = "Status";
			this.Status.ReadOnly = true;
			// 
			// Process_Flag
			// 
			this.Process_Flag.HeaderText = "Process Flag";
			this.Process_Flag.Name = "Process_Flag";
			this.Process_Flag.ReadOnly = true;
			// 
			// BtQuery
			// 
			this.BtQuery.Location = new System.Drawing.Point(37, 509);
			this.BtQuery.Name = "BtQuery";
			this.BtQuery.Size = new System.Drawing.Size(91, 27);
			this.BtQuery.TabIndex = 2;
			this.BtQuery.Text = "Query";
			this.BtQuery.UseVisualStyleBackColor = true;
			this.BtQuery.Click += new System.EventHandler(this.BtQueryClick);
			// 
			// FormQueryUploadStatus
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1273, 656);
			this.Controls.Add(this.BtQuery);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.panel1);
			this.Name = "FormQueryUploadStatus";
			this.Text = "FormQueryUploadStatus";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.DataGridViewTextBoxColumn Process_Flag;
		private System.Windows.Forms.DataGridViewTextBoxColumn Status;
		private System.Windows.Forms.DataGridViewTextBoxColumn Carton_No;
		private System.Windows.Forms.Button BtQuery;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.TextBox TbCarton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
	}
}
