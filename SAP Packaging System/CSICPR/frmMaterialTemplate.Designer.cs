﻿namespace CSICPR
{
    partial class frmMaterialTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMaterialTemplate));
        	this.PanelSelect = new System.Windows.Forms.Panel();
        	this.btnDelete = new System.Windows.Forms.Button();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.sysID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.btnSelect = new System.Windows.Forms.Button();
        	this.label1 = new System.Windows.Forms.Label();
        	this.txtTemplateNameSelect = new System.Windows.Forms.TextBox();
        	this.panelDisplay = new System.Windows.Forms.Panel();
        	this.Bt_Modify = new System.Windows.Forms.Button();
        	this.Cb_CabelLength = new System.Windows.Forms.ComboBox();
        	this.Cb_JboxType = new System.Windows.Forms.ComboBox();
        	this.label44 = new System.Windows.Forms.Label();
        	this.label43 = new System.Windows.Forms.Label();
        	this.label42 = new System.Windows.Forms.Label();
        	this.Cb_Lid = new System.Windows.Forms.ComboBox();
        	this.label41 = new System.Windows.Forms.Label();
        	this.Cb_Market = new System.Windows.Forms.ComboBox();
        	this.comboxTolerance = new System.Windows.Forms.ComboBox();
        	this.label34 = new System.Windows.Forms.Label();
        	this.label33 = new System.Windows.Forms.Label();
        	this.panelMaterialCode = new System.Windows.Forms.Panel();
        	this.label100 = new System.Windows.Forms.Label();
        	this.label106 = new System.Windows.Forms.Label();
        	this.label105 = new System.Windows.Forms.Label();
        	this.label104 = new System.Windows.Forms.Label();
        	this.label103 = new System.Windows.Forms.Label();
        	this.label102 = new System.Windows.Forms.Label();
        	this.label101 = new System.Windows.Forms.Label();
        	this.label15 = new System.Windows.Forms.Label();
        	this.label4 = new System.Windows.Forms.Label();
        	this.comboxCellCode = new System.Windows.Forms.ComboBox();
        	this.label9 = new System.Windows.Forms.Label();
        	this.comboxCellCodeDesc = new System.Windows.Forms.ComboBox();
        	this.label10 = new System.Windows.Forms.Label();
        	this.comboxCellSAPBatch = new System.Windows.Forms.ComboBox();
        	this.label13 = new System.Windows.Forms.Label();
        	this.comboxSFrameSAPBatch = new System.Windows.Forms.ComboBox();
        	this.comboxGlassCode = new System.Windows.Forms.ComboBox();
        	this.label27 = new System.Windows.Forms.Label();
        	this.label12 = new System.Windows.Forms.Label();
        	this.comboxSFrameCodeDesc = new System.Windows.Forms.ComboBox();
        	this.comboxGlassCodeDesc = new System.Windows.Forms.ComboBox();
        	this.label28 = new System.Windows.Forms.Label();
        	this.label11 = new System.Windows.Forms.Label();
        	this.comboxSFrameCode = new System.Windows.Forms.ComboBox();
        	this.comboxGlassSAPBatch = new System.Windows.Forms.ComboBox();
        	this.label29 = new System.Windows.Forms.Label();
        	this.label16 = new System.Windows.Forms.Label();
        	this.comboxLframeSAPBatch = new System.Windows.Forms.ComboBox();
        	this.comboxEVACode = new System.Windows.Forms.ComboBox();
        	this.label24 = new System.Windows.Forms.Label();
        	this.comboxEVACodeDesc = new System.Windows.Forms.ComboBox();
        	this.comboxLFrameCodeDesc = new System.Windows.Forms.ComboBox();
        	this.label14 = new System.Windows.Forms.Label();
        	this.label25 = new System.Windows.Forms.Label();
        	this.comboxEVASAPBatch = new System.Windows.Forms.ComboBox();
        	this.comboxLFrameCode = new System.Windows.Forms.ComboBox();
        	this.label19 = new System.Windows.Forms.Label();
        	this.label26 = new System.Windows.Forms.Label();
        	this.comboxTPTCode = new System.Windows.Forms.ComboBox();
        	this.comboxConboxSAPBatch = new System.Windows.Forms.ComboBox();
        	this.label18 = new System.Windows.Forms.Label();
        	this.label20 = new System.Windows.Forms.Label();
        	this.comboxTPTCodeDesc = new System.Windows.Forms.ComboBox();
        	this.comboxConboxCodeDesc = new System.Windows.Forms.ComboBox();
        	this.label17 = new System.Windows.Forms.Label();
        	this.label21 = new System.Windows.Forms.Label();
        	this.comboxTPTSAPBatch = new System.Windows.Forms.ComboBox();
        	this.comboxConboxCode = new System.Windows.Forms.ComboBox();
        	this.label22 = new System.Windows.Forms.Label();
        	this.label32 = new System.Windows.Forms.Label();
        	this.txtLastUpdateTime = new System.Windows.Forms.TextBox();
        	this.label31 = new System.Windows.Forms.Label();
        	this.txtLastUpdateUser = new System.Windows.Forms.TextBox();
        	this.lblTolerance = new System.Windows.Forms.Label();
        	this.btnFindMaterialCode = new System.Windows.Forms.Button();
        	this.label8 = new System.Windows.Forms.Label();
        	this.ddlGlassLength = new System.Windows.Forms.ComboBox();
        	this.ddlPackingPattern = new System.Windows.Forms.ComboBox();
        	this.ddlCellTransfer = new System.Windows.Forms.ComboBox();
        	this.label23 = new System.Windows.Forms.Label();
        	this.label6 = new System.Windows.Forms.Label();
        	this.ddlByIm = new System.Windows.Forms.ComboBox();
        	this.label5 = new System.Windows.Forms.Label();
        	this.txtWorkShop = new System.Windows.Forms.TextBox();
        	this.label7 = new System.Windows.Forms.Label();
        	this.PicBoxWO = new System.Windows.Forms.PictureBox();
        	this.ddlWoType = new System.Windows.Forms.ComboBox();
        	this.txtWoOrder = new System.Windows.Forms.TextBox();
        	this.LblWo = new System.Windows.Forms.Label();
        	this.lblWoType = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.txtTemplateDesc = new System.Windows.Forms.TextBox();
        	this.label2 = new System.Windows.Forms.Label();
        	this.txtTemplateName = new System.Windows.Forms.TextBox();
        	this.btnSave = new System.Windows.Forms.Button();
        	this.btnAdd = new System.Windows.Forms.Button();
        	this.btnUpdate = new System.Windows.Forms.Button();
        	this.btnCancel = new System.Windows.Forms.Button();
        	this.PanelSelect.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	this.panelDisplay.SuspendLayout();
        	this.panelMaterialCode.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// PanelSelect
        	// 
        	this.PanelSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.PanelSelect.Controls.Add(this.btnDelete);
        	this.PanelSelect.Controls.Add(this.dataGridView1);
        	this.PanelSelect.Controls.Add(this.btnSelect);
        	this.PanelSelect.Controls.Add(this.label1);
        	this.PanelSelect.Controls.Add(this.txtTemplateNameSelect);
        	this.PanelSelect.Location = new System.Drawing.Point(4, 12);
        	this.PanelSelect.Name = "PanelSelect";
        	this.PanelSelect.Size = new System.Drawing.Size(325, 471);
        	this.PanelSelect.TabIndex = 0;
        	this.PanelSelect.Tag = "";
        	// 
        	// btnDelete
        	// 
        	this.btnDelete.Enabled = false;
        	this.btnDelete.Location = new System.Drawing.Point(214, 436);
        	this.btnDelete.Name = "btnDelete";
        	this.btnDelete.Size = new System.Drawing.Size(72, 23);
        	this.btnDelete.TabIndex = 236;
        	this.btnDelete.Text = "删除";
        	this.btnDelete.UseVisualStyleBackColor = true;
        	this.btnDelete.Visible = false;
        	this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.name,
        	        	        	this.desc,
        	        	        	this.sysID});
        	this.dataGridView1.Location = new System.Drawing.Point(8, 48);
        	this.dataGridView1.MultiSelect = false;
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.ReadOnly = true;
        	this.dataGridView1.RowHeadersWidth = 20;
        	this.dataGridView1.RowTemplate.Height = 23;
        	this.dataGridView1.Size = new System.Drawing.Size(311, 368);
        	this.dataGridView1.TabIndex = 3;
        	this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
        	// 
        	// name
        	// 
        	this.name.HeaderText = "物料模板名称";
        	this.name.Name = "name";
        	this.name.ReadOnly = true;
        	this.name.Width = 120;
        	// 
        	// desc
        	// 
        	this.desc.HeaderText = "说明";
        	this.desc.Name = "desc";
        	this.desc.ReadOnly = true;
        	this.desc.Width = 120;
        	// 
        	// sysID
        	// 
        	this.sysID.HeaderText = "sysID";
        	this.sysID.Name = "sysID";
        	this.sysID.ReadOnly = true;
        	this.sysID.Visible = false;
        	this.sysID.Width = 30;
        	// 
        	// btnSelect
        	// 
        	this.btnSelect.Location = new System.Drawing.Point(247, 15);
        	this.btnSelect.Name = "btnSelect";
        	this.btnSelect.Size = new System.Drawing.Size(72, 23);
        	this.btnSelect.TabIndex = 2;
        	this.btnSelect.Text = "查询";
        	this.btnSelect.UseVisualStyleBackColor = true;
        	this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(5, 20);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(85, 13);
        	this.label1.TabIndex = 1;
        	this.label1.Text = "物料模板名称：";
        	// 
        	// txtTemplateNameSelect
        	// 
        	this.txtTemplateNameSelect.Location = new System.Drawing.Point(96, 16);
        	this.txtTemplateNameSelect.Name = "txtTemplateNameSelect";
        	this.txtTemplateNameSelect.Size = new System.Drawing.Size(130, 20);
        	this.txtTemplateNameSelect.TabIndex = 0;
        	// 
        	// panelDisplay
        	// 
        	this.panelDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.panelDisplay.Controls.Add(this.Bt_Modify);
        	this.panelDisplay.Controls.Add(this.Cb_CabelLength);
        	this.panelDisplay.Controls.Add(this.Cb_JboxType);
        	this.panelDisplay.Controls.Add(this.label44);
        	this.panelDisplay.Controls.Add(this.label43);
        	this.panelDisplay.Controls.Add(this.label42);
        	this.panelDisplay.Controls.Add(this.Cb_Lid);
        	this.panelDisplay.Controls.Add(this.label41);
        	this.panelDisplay.Controls.Add(this.Cb_Market);
        	this.panelDisplay.Controls.Add(this.comboxTolerance);
        	this.panelDisplay.Controls.Add(this.label34);
        	this.panelDisplay.Controls.Add(this.label33);
        	this.panelDisplay.Controls.Add(this.panelMaterialCode);
        	this.panelDisplay.Controls.Add(this.label32);
        	this.panelDisplay.Controls.Add(this.txtLastUpdateTime);
        	this.panelDisplay.Controls.Add(this.label31);
        	this.panelDisplay.Controls.Add(this.txtLastUpdateUser);
        	this.panelDisplay.Controls.Add(this.lblTolerance);
        	this.panelDisplay.Controls.Add(this.btnFindMaterialCode);
        	this.panelDisplay.Controls.Add(this.label8);
        	this.panelDisplay.Controls.Add(this.ddlGlassLength);
        	this.panelDisplay.Controls.Add(this.ddlPackingPattern);
        	this.panelDisplay.Controls.Add(this.ddlCellTransfer);
        	this.panelDisplay.Controls.Add(this.label23);
        	this.panelDisplay.Controls.Add(this.label6);
        	this.panelDisplay.Controls.Add(this.ddlByIm);
        	this.panelDisplay.Controls.Add(this.label5);
        	this.panelDisplay.Controls.Add(this.txtWorkShop);
        	this.panelDisplay.Controls.Add(this.label7);
        	this.panelDisplay.Controls.Add(this.PicBoxWO);
        	this.panelDisplay.Controls.Add(this.ddlWoType);
        	this.panelDisplay.Controls.Add(this.txtWoOrder);
        	this.panelDisplay.Controls.Add(this.LblWo);
        	this.panelDisplay.Controls.Add(this.lblWoType);
        	this.panelDisplay.Controls.Add(this.label3);
        	this.panelDisplay.Controls.Add(this.txtTemplateDesc);
        	this.panelDisplay.Controls.Add(this.label2);
        	this.panelDisplay.Controls.Add(this.txtTemplateName);
        	this.panelDisplay.Location = new System.Drawing.Point(335, 12);
        	this.panelDisplay.Name = "panelDisplay";
        	this.panelDisplay.Size = new System.Drawing.Size(793, 471);
        	this.panelDisplay.TabIndex = 1;
        	// 
        	// Bt_Modify
        	// 
        	this.Bt_Modify.Enabled = false;
        	this.Bt_Modify.Location = new System.Drawing.Point(704, 182);
        	this.Bt_Modify.Name = "Bt_Modify";
        	this.Bt_Modify.Size = new System.Drawing.Size(72, 23);
        	this.Bt_Modify.TabIndex = 257;
        	this.Bt_Modify.Text = "Modify";
        	this.Bt_Modify.UseVisualStyleBackColor = true;
        	this.Bt_Modify.Click += new System.EventHandler(this.Bt_ModifyClick);
        	// 
        	// Cb_CabelLength
        	// 
        	this.Cb_CabelLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Cb_CabelLength.FormattingEnabled = true;
        	this.Cb_CabelLength.Location = new System.Drawing.Point(562, 155);
        	this.Cb_CabelLength.Name = "Cb_CabelLength";
        	this.Cb_CabelLength.Size = new System.Drawing.Size(127, 21);
        	this.Cb_CabelLength.TabIndex = 256;
        	this.Cb_CabelLength.Tag = "电流分档";
        	// 
        	// Cb_JboxType
        	// 
        	this.Cb_JboxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Cb_JboxType.FormattingEnabled = true;
        	this.Cb_JboxType.Location = new System.Drawing.Point(562, 183);
        	this.Cb_JboxType.Name = "Cb_JboxType";
        	this.Cb_JboxType.Size = new System.Drawing.Size(127, 21);
        	this.Cb_JboxType.TabIndex = 255;
        	this.Cb_JboxType.Tag = "电流分档";
        	// 
        	// label44
        	// 
        	this.label44.AutoSize = true;
        	this.label44.Location = new System.Drawing.Point(486, 162);
        	this.label44.Name = "label44";
        	this.label44.Size = new System.Drawing.Size(70, 13);
        	this.label44.TabIndex = 254;
        	this.label44.Text = "CableLength:";
        	// 
        	// label43
        	// 
        	this.label43.AutoSize = true;
        	this.label43.Location = new System.Drawing.Point(485, 187);
        	this.label43.Name = "label43";
        	this.label43.Size = new System.Drawing.Size(57, 13);
        	this.label43.TabIndex = 251;
        	this.label43.Text = "JBoxType:";
        	// 
        	// label42
        	// 
        	this.label42.AutoSize = true;
        	this.label42.Location = new System.Drawing.Point(231, 165);
        	this.label42.Name = "label42";
        	this.label42.Size = new System.Drawing.Size(30, 13);
        	this.label42.TabIndex = 249;
        	this.label42.Text = "LID：";
        	// 
        	// Cb_Lid
        	// 
        	this.Cb_Lid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Cb_Lid.FormattingEnabled = true;
        	this.Cb_Lid.Items.AddRange(new object[] {
        	        	        	"LID",
        	        	        	"XLID"});
        	this.Cb_Lid.Location = new System.Drawing.Point(304, 161);
        	this.Cb_Lid.Name = "Cb_Lid";
        	this.Cb_Lid.Size = new System.Drawing.Size(127, 21);
        	this.Cb_Lid.TabIndex = 250;
        	this.Cb_Lid.Tag = "电流分档";
        	// 
        	// label41
        	// 
        	this.label41.AutoSize = true;
        	this.label41.Location = new System.Drawing.Point(7, 163);
        	this.label41.Name = "label41";
        	this.label41.Size = new System.Drawing.Size(43, 13);
        	this.label41.TabIndex = 247;
        	this.label41.Text = "Market:";
        	// 
        	// Cb_Market
        	// 
        	this.Cb_Market.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.Cb_Market.FormattingEnabled = true;
        	this.Cb_Market.Items.AddRange(new object[] {
        	        	        	"E",
        	        	        	"U",
        	        	        	"C"});
        	this.Cb_Market.Location = new System.Drawing.Point(80, 159);
        	this.Cb_Market.Name = "Cb_Market";
        	this.Cb_Market.Size = new System.Drawing.Size(127, 21);
        	this.Cb_Market.TabIndex = 248;
        	this.Cb_Market.Tag = "电流分档";
        	// 
        	// comboxTolerance
        	// 
        	this.comboxTolerance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxTolerance.FormattingEnabled = true;
        	this.comboxTolerance.Location = new System.Drawing.Point(304, 95);
        	this.comboxTolerance.Name = "comboxTolerance";
        	this.comboxTolerance.Size = new System.Drawing.Size(102, 21);
        	this.comboxTolerance.TabIndex = 246;
        	this.comboxTolerance.Tag = "Tolerance";
        	// 
        	// label34
        	// 
        	this.label34.AutoSize = true;
        	this.label34.BackColor = System.Drawing.Color.YellowGreen;
        	this.label34.ForeColor = System.Drawing.SystemColors.ControlText;
        	this.label34.Location = new System.Drawing.Point(393, 128);
        	this.label34.Name = "label34";
        	this.label34.Size = new System.Drawing.Size(58, 13);
        	this.label34.TabIndex = 245;
        	this.label34.Text = "例: 15.234";
        	// 
        	// label33
        	// 
        	this.label33.AutoSize = true;
        	this.label33.BackColor = System.Drawing.Color.YellowGreen;
        	this.label33.ForeColor = System.Drawing.SystemColors.ControlText;
        	this.label33.Location = new System.Drawing.Point(210, 192);
        	this.label33.Name = "label33";
        	this.label33.Size = new System.Drawing.Size(223, 13);
        	this.label33.TabIndex = 244;
        	this.label33.Text = "点击左面按钮后才可以配置以下物料代码";
        	this.label33.Visible = false;
        	// 
        	// panelMaterialCode
        	// 
        	this.panelMaterialCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.panelMaterialCode.Controls.Add(this.label100);
        	this.panelMaterialCode.Controls.Add(this.label106);
        	this.panelMaterialCode.Controls.Add(this.label105);
        	this.panelMaterialCode.Controls.Add(this.label104);
        	this.panelMaterialCode.Controls.Add(this.label103);
        	this.panelMaterialCode.Controls.Add(this.label102);
        	this.panelMaterialCode.Controls.Add(this.label101);
        	this.panelMaterialCode.Controls.Add(this.label15);
        	this.panelMaterialCode.Controls.Add(this.label4);
        	this.panelMaterialCode.Controls.Add(this.comboxCellCode);
        	this.panelMaterialCode.Controls.Add(this.label9);
        	this.panelMaterialCode.Controls.Add(this.comboxCellCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.label10);
        	this.panelMaterialCode.Controls.Add(this.comboxCellSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.label13);
        	this.panelMaterialCode.Controls.Add(this.comboxSFrameSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.comboxGlassCode);
        	this.panelMaterialCode.Controls.Add(this.label27);
        	this.panelMaterialCode.Controls.Add(this.label12);
        	this.panelMaterialCode.Controls.Add(this.comboxSFrameCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.comboxGlassCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.label28);
        	this.panelMaterialCode.Controls.Add(this.label11);
        	this.panelMaterialCode.Controls.Add(this.comboxSFrameCode);
        	this.panelMaterialCode.Controls.Add(this.comboxGlassSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.label29);
        	this.panelMaterialCode.Controls.Add(this.label16);
        	this.panelMaterialCode.Controls.Add(this.comboxLframeSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.comboxEVACode);
        	this.panelMaterialCode.Controls.Add(this.label24);
        	this.panelMaterialCode.Controls.Add(this.comboxEVACodeDesc);
        	this.panelMaterialCode.Controls.Add(this.comboxLFrameCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.label14);
        	this.panelMaterialCode.Controls.Add(this.label25);
        	this.panelMaterialCode.Controls.Add(this.comboxEVASAPBatch);
        	this.panelMaterialCode.Controls.Add(this.comboxLFrameCode);
        	this.panelMaterialCode.Controls.Add(this.label19);
        	this.panelMaterialCode.Controls.Add(this.label26);
        	this.panelMaterialCode.Controls.Add(this.comboxTPTCode);
        	this.panelMaterialCode.Controls.Add(this.comboxConboxSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.label18);
        	this.panelMaterialCode.Controls.Add(this.label20);
        	this.panelMaterialCode.Controls.Add(this.comboxTPTCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.comboxConboxCodeDesc);
        	this.panelMaterialCode.Controls.Add(this.label17);
        	this.panelMaterialCode.Controls.Add(this.label21);
        	this.panelMaterialCode.Controls.Add(this.comboxTPTSAPBatch);
        	this.panelMaterialCode.Controls.Add(this.comboxConboxCode);
        	this.panelMaterialCode.Controls.Add(this.label22);
        	this.panelMaterialCode.Location = new System.Drawing.Point(3, 216);
        	this.panelMaterialCode.Name = "panelMaterialCode";
        	this.panelMaterialCode.Size = new System.Drawing.Size(675, 205);
        	this.panelMaterialCode.TabIndex = 243;
        	// 
        	// label100
        	// 
        	this.label100.AutoSize = true;
        	this.label100.ForeColor = System.Drawing.Color.Red;
        	this.label100.Location = new System.Drawing.Point(481, 16);
        	this.label100.Name = "label100";
        	this.label100.Size = new System.Drawing.Size(73, 13);
        	this.label100.TabIndex = 236;
        	this.label100.Text = "MultiSelection";
        	this.label100.Visible = false;
        	// 
        	// label106
        	// 
        	this.label106.AutoSize = true;
        	this.label106.ForeColor = System.Drawing.Color.Red;
        	this.label106.Location = new System.Drawing.Point(481, 176);
        	this.label106.Name = "label106";
        	this.label106.Size = new System.Drawing.Size(73, 13);
        	this.label106.TabIndex = 252;
        	this.label106.Text = "MultiSelection";
        	this.label106.Visible = false;
        	// 
        	// label105
        	// 
        	this.label105.AutoSize = true;
        	this.label105.ForeColor = System.Drawing.Color.Red;
        	this.label105.Location = new System.Drawing.Point(476, 153);
        	this.label105.Name = "label105";
        	this.label105.Size = new System.Drawing.Size(73, 13);
        	this.label105.TabIndex = 251;
        	this.label105.Text = "MultiSelection";
        	this.label105.Visible = false;
        	// 
        	// label104
        	// 
        	this.label104.AutoSize = true;
        	this.label104.ForeColor = System.Drawing.Color.Red;
        	this.label104.Location = new System.Drawing.Point(476, 124);
        	this.label104.Name = "label104";
        	this.label104.Size = new System.Drawing.Size(73, 13);
        	this.label104.TabIndex = 250;
        	this.label104.Text = "MultiSelection";
        	this.label104.Visible = false;
        	// 
        	// label103
        	// 
        	this.label103.AutoSize = true;
        	this.label103.ForeColor = System.Drawing.Color.Red;
        	this.label103.Location = new System.Drawing.Point(477, 98);
        	this.label103.Name = "label103";
        	this.label103.Size = new System.Drawing.Size(73, 13);
        	this.label103.TabIndex = 249;
        	this.label103.Text = "MultiSelection";
        	this.label103.Visible = false;
        	// 
        	// label102
        	// 
        	this.label102.AutoSize = true;
        	this.label102.ForeColor = System.Drawing.Color.Red;
        	this.label102.Location = new System.Drawing.Point(481, 72);
        	this.label102.Name = "label102";
        	this.label102.Size = new System.Drawing.Size(73, 13);
        	this.label102.TabIndex = 248;
        	this.label102.Text = "MultiSelection";
        	this.label102.Visible = false;
        	// 
        	// label101
        	// 
        	this.label101.AutoSize = true;
        	this.label101.ForeColor = System.Drawing.Color.Red;
        	this.label101.Location = new System.Drawing.Point(481, 43);
        	this.label101.Name = "label101";
        	this.label101.Size = new System.Drawing.Size(73, 13);
        	this.label101.TabIndex = 247;
        	this.label101.Text = "MultiSelection";
        	this.label101.Visible = false;
        	// 
        	// label15
        	// 
        	this.label15.AutoSize = true;
        	this.label15.Location = new System.Drawing.Point(212, 67);
        	this.label15.Name = "label15";
        	this.label15.Size = new System.Drawing.Size(82, 13);
        	this.label15.TabIndex = 208;
        	this.label15.Text = "EVA代码描述：";
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(3, 16);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(78, 13);
        	this.label4.TabIndex = 194;
        	this.label4.Text = "Cell物料代码：";
        	// 
        	// comboxCellCode
        	// 
        	this.comboxCellCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxCellCode.FormattingEnabled = true;
        	this.comboxCellCode.Location = new System.Drawing.Point(101, 13);
        	this.comboxCellCode.Name = "comboxCellCode";
        	this.comboxCellCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxCellCode.TabIndex = 195;
        	this.comboxCellCode.Tag = "Cell物料代码";
        	this.comboxCellCode.SelectedIndexChanged += new System.EventHandler(this.comboxCellCode_SelectedIndexChanged);
        	// 
        	// label9
        	// 
        	this.label9.AutoSize = true;
        	this.label9.Location = new System.Drawing.Point(212, 13);
        	this.label9.Name = "label9";
        	this.label9.Size = new System.Drawing.Size(81, 13);
        	this.label9.TabIndex = 196;
        	this.label9.Text = "Cell 代码描述：";
        	// 
        	// comboxCellCodeDesc
        	// 
        	this.comboxCellCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxCellCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxCellCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxCellCodeDesc.Enabled = false;
        	this.comboxCellCodeDesc.FormattingEnabled = true;
        	this.comboxCellCodeDesc.Location = new System.Drawing.Point(305, 10);
        	this.comboxCellCodeDesc.Name = "comboxCellCodeDesc";
        	this.comboxCellCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxCellCodeDesc.TabIndex = 197;
        	// 
        	// label10
        	// 
        	this.label10.AutoSize = true;
        	this.label10.Location = new System.Drawing.Point(476, 16);
        	this.label10.Name = "label10";
        	this.label10.Size = new System.Drawing.Size(78, 13);
        	this.label10.TabIndex = 198;
        	this.label10.Text = "Cell SAP批次：";
        	// 
        	// comboxCellSAPBatch
        	// 
        	this.comboxCellSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxCellSAPBatch.FormattingEnabled = true;
        	this.comboxCellSAPBatch.Location = new System.Drawing.Point(570, 13);
        	this.comboxCellSAPBatch.Name = "comboxCellSAPBatch";
        	this.comboxCellSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxCellSAPBatch.TabIndex = 199;
        	this.comboxCellSAPBatch.Tag = "Cell SAP批次";
        	// 
        	// label13
        	// 
        	this.label13.AutoSize = true;
        	this.label13.Location = new System.Drawing.Point(3, 43);
        	this.label13.Name = "label13";
        	this.label13.Size = new System.Drawing.Size(85, 13);
        	this.label13.TabIndex = 200;
        	this.label13.Text = "玻璃物料代码：";
        	// 
        	// comboxSFrameSAPBatch
        	// 
        	this.comboxSFrameSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxSFrameSAPBatch.FormattingEnabled = true;
        	this.comboxSFrameSAPBatch.Location = new System.Drawing.Point(570, 172);
        	this.comboxSFrameSAPBatch.Name = "comboxSFrameSAPBatch";
        	this.comboxSFrameSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxSFrameSAPBatch.TabIndex = 235;
        	this.comboxSFrameSAPBatch.Tag = "短边框SAP批次";
        	// 
        	// comboxGlassCode
        	// 
        	this.comboxGlassCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxGlassCode.FormattingEnabled = true;
        	this.comboxGlassCode.Location = new System.Drawing.Point(101, 40);
        	this.comboxGlassCode.Name = "comboxGlassCode";
        	this.comboxGlassCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxGlassCode.TabIndex = 201;
        	this.comboxGlassCode.Tag = "玻璃物料代码";
        	this.comboxGlassCode.SelectedIndexChanged += new System.EventHandler(this.comboxGlassCode_SelectedIndexChanged);
        	// 
        	// label27
        	// 
        	this.label27.AutoSize = true;
        	this.label27.Location = new System.Drawing.Point(476, 176);
        	this.label27.Name = "label27";
        	this.label27.Size = new System.Drawing.Size(94, 13);
        	this.label27.TabIndex = 234;
        	this.label27.Text = "短边框SAP批次：";
        	// 
        	// label12
        	// 
        	this.label12.AutoSize = true;
        	this.label12.Location = new System.Drawing.Point(212, 40);
        	this.label12.Name = "label12";
        	this.label12.Size = new System.Drawing.Size(85, 13);
        	this.label12.TabIndex = 202;
        	this.label12.Text = "玻璃代码描述：";
        	// 
        	// comboxSFrameCodeDesc
        	// 
        	this.comboxSFrameCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxSFrameCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxSFrameCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxSFrameCodeDesc.Enabled = false;
        	this.comboxSFrameCodeDesc.FormattingEnabled = true;
        	this.comboxSFrameCodeDesc.Location = new System.Drawing.Point(305, 172);
        	this.comboxSFrameCodeDesc.Name = "comboxSFrameCodeDesc";
        	this.comboxSFrameCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxSFrameCodeDesc.TabIndex = 233;
        	// 
        	// comboxGlassCodeDesc
        	// 
        	this.comboxGlassCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxGlassCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxGlassCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxGlassCodeDesc.Enabled = false;
        	this.comboxGlassCodeDesc.FormattingEnabled = true;
        	this.comboxGlassCodeDesc.Location = new System.Drawing.Point(305, 37);
        	this.comboxGlassCodeDesc.Name = "comboxGlassCodeDesc";
        	this.comboxGlassCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxGlassCodeDesc.TabIndex = 203;
        	// 
        	// label28
        	// 
        	this.label28.AutoSize = true;
        	this.label28.Location = new System.Drawing.Point(212, 176);
        	this.label28.Name = "label28";
        	this.label28.Size = new System.Drawing.Size(97, 13);
        	this.label28.TabIndex = 232;
        	this.label28.Text = "短边框代码描述：";
        	// 
        	// label11
        	// 
        	this.label11.AutoSize = true;
        	this.label11.Location = new System.Drawing.Point(476, 43);
        	this.label11.Name = "label11";
        	this.label11.Size = new System.Drawing.Size(82, 13);
        	this.label11.TabIndex = 204;
        	this.label11.Text = "玻璃SAP批次：";
        	// 
        	// comboxSFrameCode
        	// 
        	this.comboxSFrameCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxSFrameCode.FormattingEnabled = true;
        	this.comboxSFrameCode.Location = new System.Drawing.Point(101, 176);
        	this.comboxSFrameCode.Name = "comboxSFrameCode";
        	this.comboxSFrameCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxSFrameCode.TabIndex = 231;
        	this.comboxSFrameCode.Tag = "短边框物料代码";
        	this.comboxSFrameCode.SelectedIndexChanged += new System.EventHandler(this.comboxSFrameCode_SelectedIndexChanged);
        	// 
        	// comboxGlassSAPBatch
        	// 
        	this.comboxGlassSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxGlassSAPBatch.FormattingEnabled = true;
        	this.comboxGlassSAPBatch.Location = new System.Drawing.Point(570, 40);
        	this.comboxGlassSAPBatch.Name = "comboxGlassSAPBatch";
        	this.comboxGlassSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxGlassSAPBatch.TabIndex = 205;
        	this.comboxGlassSAPBatch.Tag = "玻璃SAP批次";
        	// 
        	// label29
        	// 
        	this.label29.AutoSize = true;
        	this.label29.Location = new System.Drawing.Point(3, 178);
        	this.label29.Name = "label29";
        	this.label29.Size = new System.Drawing.Size(97, 13);
        	this.label29.TabIndex = 230;
        	this.label29.Text = "短边框物料代码：";
        	// 
        	// label16
        	// 
        	this.label16.AutoSize = true;
        	this.label16.Location = new System.Drawing.Point(3, 70);
        	this.label16.Name = "label16";
        	this.label16.Size = new System.Drawing.Size(82, 13);
        	this.label16.TabIndex = 206;
        	this.label16.Text = "EVA物料代码：";
        	// 
        	// comboxLframeSAPBatch
        	// 
        	this.comboxLframeSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxLframeSAPBatch.FormattingEnabled = true;
        	this.comboxLframeSAPBatch.Location = new System.Drawing.Point(570, 148);
        	this.comboxLframeSAPBatch.Name = "comboxLframeSAPBatch";
        	this.comboxLframeSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxLframeSAPBatch.TabIndex = 229;
        	this.comboxLframeSAPBatch.Tag = "长边框SAP批次";
        	// 
        	// comboxEVACode
        	// 
        	this.comboxEVACode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxEVACode.FlatStyle = System.Windows.Forms.FlatStyle.System;
        	this.comboxEVACode.FormattingEnabled = true;
        	this.comboxEVACode.Location = new System.Drawing.Point(101, 67);
        	this.comboxEVACode.Name = "comboxEVACode";
        	this.comboxEVACode.Size = new System.Drawing.Size(102, 21);
        	this.comboxEVACode.TabIndex = 207;
        	this.comboxEVACode.Tag = "EVA物料代码";
        	this.comboxEVACode.SelectedIndexChanged += new System.EventHandler(this.comboxEVACode_SelectedIndexChanged);
        	// 
        	// label24
        	// 
        	this.label24.AutoSize = true;
        	this.label24.Location = new System.Drawing.Point(476, 151);
        	this.label24.Name = "label24";
        	this.label24.Size = new System.Drawing.Size(94, 13);
        	this.label24.TabIndex = 228;
        	this.label24.Text = "长边框SAP批次：";
        	// 
        	// comboxEVACodeDesc
        	// 
        	this.comboxEVACodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxEVACodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxEVACodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxEVACodeDesc.Enabled = false;
        	this.comboxEVACodeDesc.FormattingEnabled = true;
        	this.comboxEVACodeDesc.Location = new System.Drawing.Point(305, 64);
        	this.comboxEVACodeDesc.Name = "comboxEVACodeDesc";
        	this.comboxEVACodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxEVACodeDesc.TabIndex = 209;
        	// 
        	// comboxLFrameCodeDesc
        	// 
        	this.comboxLFrameCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxLFrameCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxLFrameCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxLFrameCodeDesc.Enabled = false;
        	this.comboxLFrameCodeDesc.FormattingEnabled = true;
        	this.comboxLFrameCodeDesc.Location = new System.Drawing.Point(305, 145);
        	this.comboxLFrameCodeDesc.Name = "comboxLFrameCodeDesc";
        	this.comboxLFrameCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxLFrameCodeDesc.TabIndex = 227;
        	// 
        	// label14
        	// 
        	this.label14.AutoSize = true;
        	this.label14.Location = new System.Drawing.Point(476, 70);
        	this.label14.Name = "label14";
        	this.label14.Size = new System.Drawing.Size(82, 13);
        	this.label14.TabIndex = 210;
        	this.label14.Text = "EVA SAP批次：";
        	// 
        	// label25
        	// 
        	this.label25.AutoSize = true;
        	this.label25.Location = new System.Drawing.Point(212, 148);
        	this.label25.Name = "label25";
        	this.label25.Size = new System.Drawing.Size(97, 13);
        	this.label25.TabIndex = 226;
        	this.label25.Text = "长边框代码描述：";
        	// 
        	// comboxEVASAPBatch
        	// 
        	this.comboxEVASAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxEVASAPBatch.FormattingEnabled = true;
        	this.comboxEVASAPBatch.Location = new System.Drawing.Point(570, 67);
        	this.comboxEVASAPBatch.Name = "comboxEVASAPBatch";
        	this.comboxEVASAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxEVASAPBatch.TabIndex = 211;
        	this.comboxEVASAPBatch.Tag = "EVA SAP批次";
        	// 
        	// comboxLFrameCode
        	// 
        	this.comboxLFrameCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxLFrameCode.FormattingEnabled = true;
        	this.comboxLFrameCode.Location = new System.Drawing.Point(101, 148);
        	this.comboxLFrameCode.Name = "comboxLFrameCode";
        	this.comboxLFrameCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxLFrameCode.TabIndex = 225;
        	this.comboxLFrameCode.Tag = "长边框物料代码";
        	this.comboxLFrameCode.SelectedIndexChanged += new System.EventHandler(this.comboxLFrameCode_SelectedIndexChanged);
        	// 
        	// label19
        	// 
        	this.label19.AutoSize = true;
        	this.label19.Location = new System.Drawing.Point(3, 98);
        	this.label19.Name = "label19";
        	this.label19.Size = new System.Drawing.Size(85, 13);
        	this.label19.TabIndex = 212;
        	this.label19.Text = "背板物料代码：";
        	// 
        	// label26
        	// 
        	this.label26.AutoSize = true;
        	this.label26.Location = new System.Drawing.Point(3, 151);
        	this.label26.Name = "label26";
        	this.label26.Size = new System.Drawing.Size(97, 13);
        	this.label26.TabIndex = 224;
        	this.label26.Text = "长边框物料代码：";
        	// 
        	// comboxTPTCode
        	// 
        	this.comboxTPTCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxTPTCode.FormattingEnabled = true;
        	this.comboxTPTCode.Location = new System.Drawing.Point(101, 94);
        	this.comboxTPTCode.Name = "comboxTPTCode";
        	this.comboxTPTCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxTPTCode.TabIndex = 213;
        	this.comboxTPTCode.Tag = "背板物料代码";
        	this.comboxTPTCode.SelectedIndexChanged += new System.EventHandler(this.comboxTPTCode_SelectedIndexChanged);
        	// 
        	// comboxConboxSAPBatch
        	// 
        	this.comboxConboxSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxConboxSAPBatch.FormattingEnabled = true;
        	this.comboxConboxSAPBatch.Location = new System.Drawing.Point(570, 121);
        	this.comboxConboxSAPBatch.Name = "comboxConboxSAPBatch";
        	this.comboxConboxSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxConboxSAPBatch.TabIndex = 223;
        	this.comboxConboxSAPBatch.Tag = "接线盒SAP批次";
        	// 
        	// label18
        	// 
        	this.label18.AutoSize = true;
        	this.label18.Location = new System.Drawing.Point(212, 94);
        	this.label18.Name = "label18";
        	this.label18.Size = new System.Drawing.Size(85, 13);
        	this.label18.TabIndex = 214;
        	this.label18.Text = "背板代码描述：";
        	// 
        	// label20
        	// 
        	this.label20.AutoSize = true;
        	this.label20.Location = new System.Drawing.Point(476, 124);
        	this.label20.Name = "label20";
        	this.label20.Size = new System.Drawing.Size(94, 13);
        	this.label20.TabIndex = 222;
        	this.label20.Text = "接线盒SAP批次：";
        	// 
        	// comboxTPTCodeDesc
        	// 
        	this.comboxTPTCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxTPTCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxTPTCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxTPTCodeDesc.Enabled = false;
        	this.comboxTPTCodeDesc.FormattingEnabled = true;
        	this.comboxTPTCodeDesc.Location = new System.Drawing.Point(305, 91);
        	this.comboxTPTCodeDesc.Name = "comboxTPTCodeDesc";
        	this.comboxTPTCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxTPTCodeDesc.TabIndex = 215;
        	// 
        	// comboxConboxCodeDesc
        	// 
        	this.comboxConboxCodeDesc.BackColor = System.Drawing.SystemColors.MenuBar;
        	this.comboxConboxCodeDesc.Cursor = System.Windows.Forms.Cursors.Default;
        	this.comboxConboxCodeDesc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.comboxConboxCodeDesc.Enabled = false;
        	this.comboxConboxCodeDesc.FormattingEnabled = true;
        	this.comboxConboxCodeDesc.Location = new System.Drawing.Point(305, 118);
        	this.comboxConboxCodeDesc.Name = "comboxConboxCodeDesc";
        	this.comboxConboxCodeDesc.Size = new System.Drawing.Size(165, 21);
        	this.comboxConboxCodeDesc.TabIndex = 221;
        	// 
        	// label17
        	// 
        	this.label17.AutoSize = true;
        	this.label17.Location = new System.Drawing.Point(476, 98);
        	this.label17.Name = "label17";
        	this.label17.Size = new System.Drawing.Size(82, 13);
        	this.label17.TabIndex = 216;
        	this.label17.Text = "背板SAP批次：";
        	// 
        	// label21
        	// 
        	this.label21.AutoSize = true;
        	this.label21.Location = new System.Drawing.Point(212, 121);
        	this.label21.Name = "label21";
        	this.label21.Size = new System.Drawing.Size(97, 13);
        	this.label21.TabIndex = 220;
        	this.label21.Text = "接线盒代码描述：";
        	// 
        	// comboxTPTSAPBatch
        	// 
        	this.comboxTPTSAPBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxTPTSAPBatch.FormattingEnabled = true;
        	this.comboxTPTSAPBatch.Location = new System.Drawing.Point(570, 94);
        	this.comboxTPTSAPBatch.Name = "comboxTPTSAPBatch";
        	this.comboxTPTSAPBatch.Size = new System.Drawing.Size(102, 21);
        	this.comboxTPTSAPBatch.TabIndex = 217;
        	this.comboxTPTSAPBatch.Tag = "背板SAP批次";
        	// 
        	// comboxConboxCode
        	// 
        	this.comboxConboxCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboxConboxCode.FormattingEnabled = true;
        	this.comboxConboxCode.Location = new System.Drawing.Point(101, 121);
        	this.comboxConboxCode.Name = "comboxConboxCode";
        	this.comboxConboxCode.Size = new System.Drawing.Size(102, 21);
        	this.comboxConboxCode.TabIndex = 219;
        	this.comboxConboxCode.Tag = "接线盒物料代码";
        	this.comboxConboxCode.SelectedIndexChanged += new System.EventHandler(this.comboxConboxCode_SelectedIndexChanged);
        	// 
        	// label22
        	// 
        	this.label22.AutoSize = true;
        	this.label22.Location = new System.Drawing.Point(3, 124);
        	this.label22.Name = "label22";
        	this.label22.Size = new System.Drawing.Size(97, 13);
        	this.label22.TabIndex = 218;
        	this.label22.Text = "接线盒物料代码：";
        	// 
        	// label32
        	// 
        	this.label32.AutoSize = true;
        	this.label32.Location = new System.Drawing.Point(216, 430);
        	this.label32.Name = "label32";
        	this.label32.Size = new System.Drawing.Size(85, 13);
        	this.label32.TabIndex = 241;
        	this.label32.Text = "上次修改时间：";
        	// 
        	// txtLastUpdateTime
        	// 
        	this.txtLastUpdateTime.BackColor = System.Drawing.SystemColors.Menu;
        	this.txtLastUpdateTime.Enabled = false;
        	this.txtLastUpdateTime.Location = new System.Drawing.Point(309, 428);
        	this.txtLastUpdateTime.Name = "txtLastUpdateTime";
        	this.txtLastUpdateTime.Size = new System.Drawing.Size(165, 20);
        	this.txtLastUpdateTime.TabIndex = 242;
        	// 
        	// label31
        	// 
        	this.label31.AutoSize = true;
        	this.label31.Location = new System.Drawing.Point(481, 430);
        	this.label31.Name = "label31";
        	this.label31.Size = new System.Drawing.Size(85, 13);
        	this.label31.TabIndex = 239;
        	this.label31.Text = "上次修改人员：";
        	// 
        	// txtLastUpdateUser
        	// 
        	this.txtLastUpdateUser.BackColor = System.Drawing.SystemColors.Menu;
        	this.txtLastUpdateUser.Enabled = false;
        	this.txtLastUpdateUser.Location = new System.Drawing.Point(574, 428);
        	this.txtLastUpdateUser.Name = "txtLastUpdateUser";
        	this.txtLastUpdateUser.Size = new System.Drawing.Size(102, 20);
        	this.txtLastUpdateUser.TabIndex = 240;
        	// 
        	// lblTolerance
        	// 
        	this.lblTolerance.AutoSize = true;
        	this.lblTolerance.Location = new System.Drawing.Point(223, 103);
        	this.lblTolerance.Name = "lblTolerance";
        	this.lblTolerance.Size = new System.Drawing.Size(37, 13);
        	this.lblTolerance.TabIndex = 237;
        	this.lblTolerance.Tag = "公差";
        	this.lblTolerance.Text = "公差：";
        	// 
        	// btnFindMaterialCode
        	// 
        	this.btnFindMaterialCode.Location = new System.Drawing.Point(6, 187);
        	this.btnFindMaterialCode.Name = "btnFindMaterialCode";
        	this.btnFindMaterialCode.Size = new System.Drawing.Size(198, 23);
        	this.btnFindMaterialCode.TabIndex = 236;
        	this.btnFindMaterialCode.Text = "根据工单设定物料代码";
        	this.btnFindMaterialCode.UseVisualStyleBackColor = true;
        	this.btnFindMaterialCode.Visible = false;
        	this.btnFindMaterialCode.Click += new System.EventHandler(this.btnFindMaterialCode_Click);
        	// 
        	// label8
        	// 
        	this.label8.AutoSize = true;
        	this.label8.Location = new System.Drawing.Point(469, 103);
        	this.label8.Name = "label8";
        	this.label8.Size = new System.Drawing.Size(61, 13);
        	this.label8.TabIndex = 192;
        	this.label8.Text = "玻璃厚度：";
        	// 
        	// ddlGlassLength
        	// 
        	this.ddlGlassLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlGlassLength.FormattingEnabled = true;
        	this.ddlGlassLength.Location = new System.Drawing.Point(583, 99);
        	this.ddlGlassLength.Name = "ddlGlassLength";
        	this.ddlGlassLength.Size = new System.Drawing.Size(127, 21);
        	this.ddlGlassLength.TabIndex = 193;
        	this.ddlGlassLength.Tag = "玻璃厚度";
        	// 
        	// ddlPackingPattern
        	// 
        	this.ddlPackingPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlPackingPattern.FormattingEnabled = true;
        	this.ddlPackingPattern.Location = new System.Drawing.Point(584, 124);
        	this.ddlPackingPattern.Name = "ddlPackingPattern";
        	this.ddlPackingPattern.Size = new System.Drawing.Size(127, 21);
        	this.ddlPackingPattern.TabIndex = 191;
        	this.ddlPackingPattern.Tag = "包装方式";
        	// 
        	// ddlCellTransfer
        	// 
        	this.ddlCellTransfer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
        	this.ddlCellTransfer.FormattingEnabled = true;
        	this.ddlCellTransfer.Location = new System.Drawing.Point(304, 124);
        	this.ddlCellTransfer.Name = "ddlCellTransfer";
        	this.ddlCellTransfer.Size = new System.Drawing.Size(83, 21);
        	this.ddlCellTransfer.TabIndex = 185;
        	this.ddlCellTransfer.Tag = "Cell转换效率";
        	this.ddlCellTransfer.Leave += new System.EventHandler(this.ddlCellTransfer_Leave);
        	// 
        	// label23
        	// 
        	this.label23.AutoSize = true;
        	this.label23.Location = new System.Drawing.Point(223, 128);
        	this.label23.Name = "label23";
        	this.label23.Size = new System.Drawing.Size(78, 13);
        	this.label23.TabIndex = 184;
        	this.label23.Text = "Cell转换效率：";
        	// 
        	// label6
        	// 
        	this.label6.AutoSize = true;
        	this.label6.Location = new System.Drawing.Point(7, 128);
        	this.label6.Name = "label6";
        	this.label6.Size = new System.Drawing.Size(61, 13);
        	this.label6.TabIndex = 188;
        	this.label6.Text = "电流分档：";
        	// 
        	// ddlByIm
        	// 
        	this.ddlByIm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlByIm.FormattingEnabled = true;
        	this.ddlByIm.Location = new System.Drawing.Point(80, 124);
        	this.ddlByIm.Name = "ddlByIm";
        	this.ddlByIm.Size = new System.Drawing.Size(127, 21);
        	this.ddlByIm.TabIndex = 189;
        	this.ddlByIm.Tag = "电流分档";
        	// 
        	// label5
        	// 
        	this.label5.AutoSize = true;
        	this.label5.Location = new System.Drawing.Point(6, 430);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(61, 13);
        	this.label5.TabIndex = 186;
        	this.label5.Text = "当前车间：";
        	// 
        	// txtWorkShop
        	// 
        	this.txtWorkShop.BackColor = System.Drawing.SystemColors.Menu;
        	this.txtWorkShop.Enabled = false;
        	this.txtWorkShop.Location = new System.Drawing.Point(104, 428);
        	this.txtWorkShop.Name = "txtWorkShop";
        	this.txtWorkShop.Size = new System.Drawing.Size(102, 20);
        	this.txtWorkShop.TabIndex = 187;
        	// 
        	// label7
        	// 
        	this.label7.AutoSize = true;
        	this.label7.Location = new System.Drawing.Point(469, 128);
        	this.label7.Name = "label7";
        	this.label7.Size = new System.Drawing.Size(61, 13);
        	this.label7.TabIndex = 190;
        	this.label7.Text = "包装方式：";
        	// 
        	// PicBoxWO
        	// 
        	this.PicBoxWO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.BackgroundImage")));
        	this.PicBoxWO.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.InitialImage")));
        	this.PicBoxWO.Location = new System.Drawing.Point(195, 99);
        	this.PicBoxWO.Name = "PicBoxWO";
        	this.PicBoxWO.Size = new System.Drawing.Size(27, 20);
        	this.PicBoxWO.TabIndex = 181;
        	this.PicBoxWO.TabStop = false;
        	this.PicBoxWO.Click += new System.EventHandler(this.PicBoxWO_Click);
        	// 
        	// ddlWoType
        	// 
        	this.ddlWoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlWoType.FormattingEnabled = true;
        	this.ddlWoType.Location = new System.Drawing.Point(528, 12);
        	this.ddlWoType.Name = "ddlWoType";
        	this.ddlWoType.Size = new System.Drawing.Size(150, 21);
        	this.ddlWoType.TabIndex = 180;
        	this.ddlWoType.Visible = false;
        	// 
        	// txtWoOrder
        	// 
        	this.txtWoOrder.Enabled = false;
        	this.txtWoOrder.Location = new System.Drawing.Point(93, 98);
        	this.txtWoOrder.Name = "txtWoOrder";
        	this.txtWoOrder.Size = new System.Drawing.Size(102, 20);
        	this.txtWoOrder.TabIndex = 179;
        	this.txtWoOrder.Tag = "工单";
        	// 
        	// LblWo
        	// 
        	this.LblWo.AutoSize = true;
        	this.LblWo.Location = new System.Drawing.Point(7, 103);
        	this.LblWo.Name = "LblWo";
        	this.LblWo.Size = new System.Drawing.Size(37, 13);
        	this.LblWo.TabIndex = 178;
        	this.LblWo.Text = "工单：";
        	// 
        	// lblWoType
        	// 
        	this.lblWoType.AutoSize = true;
        	this.lblWoType.Location = new System.Drawing.Point(447, 14);
        	this.lblWoType.Name = "lblWoType";
        	this.lblWoType.Size = new System.Drawing.Size(58, 13);
        	this.lblWoType.TabIndex = 177;
        	this.lblWoType.Text = "工单类型:";
        	this.lblWoType.Visible = false;
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(7, 48);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(61, 13);
        	this.label3.TabIndex = 5;
        	this.label3.Text = "模板说明：";
        	// 
        	// txtTemplateDesc
        	// 
        	this.txtTemplateDesc.Location = new System.Drawing.Point(80, 40);
        	this.txtTemplateDesc.Multiline = true;
        	this.txtTemplateDesc.Name = "txtTemplateDesc";
        	this.txtTemplateDesc.Size = new System.Drawing.Size(596, 50);
        	this.txtTemplateDesc.TabIndex = 4;
        	this.txtTemplateDesc.Tag = "模板说明";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(7, 20);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(61, 13);
        	this.label2.TabIndex = 3;
        	this.label2.Text = "模板名称：";
        	// 
        	// txtTemplateName
        	// 
        	this.txtTemplateName.Location = new System.Drawing.Point(132, 15);
        	this.txtTemplateName.Name = "txtTemplateName";
        	this.txtTemplateName.Size = new System.Drawing.Size(211, 20);
        	this.txtTemplateName.TabIndex = 2;
        	this.txtTemplateName.Tag = "模板名称";
       
        	
        	// 
        	// btnSave
        	// 
        	this.btnSave.Location = new System.Drawing.Point(782, 486);
        	this.btnSave.Name = "btnSave";
        	this.btnSave.Size = new System.Drawing.Size(72, 23);
        	this.btnSave.TabIndex = 9;
        	this.btnSave.Text = "保存";
        	this.btnSave.UseVisualStyleBackColor = true;
        	this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        	// 
        	// btnAdd
        	// 
        	this.btnAdd.Location = new System.Drawing.Point(468, 486);
        	this.btnAdd.Name = "btnAdd";
        	this.btnAdd.Size = new System.Drawing.Size(72, 23);
        	this.btnAdd.TabIndex = 8;
        	this.btnAdd.Text = "新增";
        	this.btnAdd.UseVisualStyleBackColor = true;
        	this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
        	// 
        	// btnUpdate
        	// 
        	this.btnUpdate.Location = new System.Drawing.Point(559, 486);
        	this.btnUpdate.Name = "btnUpdate";
        	this.btnUpdate.Size = new System.Drawing.Size(72, 23);
        	this.btnUpdate.TabIndex = 11;
        	this.btnUpdate.Text = "更新";
        	this.btnUpdate.UseVisualStyleBackColor = true;
        	this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
        	// 
        	// btnCancel
        	// 
        	this.btnCancel.Enabled = false;
        	this.btnCancel.Location = new System.Drawing.Point(652, 486);
        	this.btnCancel.Name = "btnCancel";
        	this.btnCancel.Size = new System.Drawing.Size(72, 23);
        	this.btnCancel.TabIndex = 12;
        	this.btnCancel.Text = "取消";
        	this.btnCancel.UseVisualStyleBackColor = true;
        	this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
        	// 
        	// frmMaterialTemplate
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1124, 523);
        	this.Controls.Add(this.btnCancel);
        	this.Controls.Add(this.btnUpdate);
        	this.Controls.Add(this.panelDisplay);
        	this.Controls.Add(this.PanelSelect);
        	this.Controls.Add(this.btnAdd);
        	this.Controls.Add(this.btnSave);
        	this.Name = "frmMaterialTemplate";
        	this.Text = "物料模板维护";
        	this.Load += new System.EventHandler(this.frmMaterialTemplate_Load);
        	this.PanelSelect.ResumeLayout(false);
        	this.PanelSelect.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	this.panelDisplay.ResumeLayout(false);
        	this.panelDisplay.PerformLayout();
        	this.panelMaterialCode.ResumeLayout(false);
        	this.panelMaterialCode.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).EndInit();
        	this.ResumeLayout(false);
        }
        private System.Windows.Forms.Button Bt_Modify;
        private System.Windows.Forms.ComboBox Cb_JboxType;
        private System.Windows.Forms.ComboBox Cb_CabelLength;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox Cb_Market;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox Cb_Lid;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label100;

        #endregion

        private System.Windows.Forms.Panel PanelSelect;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTemplateNameSelect;
        private System.Windows.Forms.Panel panelDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTemplateDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTemplateName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.PictureBox PicBoxWO;
        private System.Windows.Forms.ComboBox ddlWoType;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Label LblWo;
        private System.Windows.Forms.Label lblWoType;
        private System.Windows.Forms.ComboBox ddlPackingPattern;
        private System.Windows.Forms.ComboBox ddlCellTransfer;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox ddlByIm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWorkShop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ddlGlassLength;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnFindMaterialCode;
        private System.Windows.Forms.ComboBox comboxSFrameSAPBatch;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox comboxSFrameCodeDesc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboxSFrameCode;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboxLframeSAPBatch;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboxLFrameCodeDesc;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboxLFrameCode;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboxConboxSAPBatch;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboxConboxCodeDesc;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboxConboxCode;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboxTPTSAPBatch;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboxTPTCodeDesc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboxTPTCode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboxEVASAPBatch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboxEVACodeDesc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboxEVACode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboxGlassSAPBatch;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboxGlassCodeDesc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboxGlassCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboxCellSAPBatch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboxCellCodeDesc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboxCellCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtLastUpdateTime;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtLastUpdateUser;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panelMaterialCode;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysID;
        private System.Windows.Forms.Label lblTolerance;
        private System.Windows.Forms.ComboBox comboxTolerance;
    }
}