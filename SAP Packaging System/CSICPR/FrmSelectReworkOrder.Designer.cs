﻿namespace CSICPR
{
    partial class FrmSelectReworkOrder    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnQuery = new System.Windows.Forms.Button();
            this.lblCartonQuery = new System.Windows.Forms.Label();
            this.PalWO = new System.Windows.Forms.Panel();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mitem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WONO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PalWO.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(285, 21);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 22);
            this.btnQuery.TabIndex = 168;
            this.btnQuery.Tag = "button1";
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // lblCartonQuery
            // 
            this.lblCartonQuery.AutoSize = true;
            this.lblCartonQuery.Location = new System.Drawing.Point(16, 10);
            this.lblCartonQuery.Name = "lblCartonQuery";
            this.lblCartonQuery.Size = new System.Drawing.Size(0, 12);
            this.lblCartonQuery.TabIndex = 170;
            // 
            // PalWO
            // 
            this.PalWO.Controls.Add(this.txtWo);
            this.PalWO.Controls.Add(this.label2);
            this.PalWO.Location = new System.Drawing.Point(13, 19);
            this.PalWO.Name = "PalWO";
            this.PalWO.Size = new System.Drawing.Size(259, 32);
            this.PalWO.TabIndex = 172;
            // 
            // txtWo
            // 
            this.txtWo.Location = new System.Drawing.Point(79, 3);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(148, 21);
            this.txtWo.TabIndex = 94;
            this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "重 工 工 单";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.lblCartonQuery);
            this.panel1.Controls.Add(this.PalWO);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(461, 65);
            this.panel1.TabIndex = 174;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(362, 21);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 22);
            this.btnSave.TabIndex = 174;
            this.btnSave.Tag = "button1";
            this.btnSave.Text = "确定";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowIndex,
            this.WONO,
            this.Mitem,
            this.Order});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 65);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(461, 328);
            this.dataGridView1.TabIndex = 175;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // Order
            // 
            this.Order.HeaderText = "订单";
            this.Order.Name = "Order";
            this.Order.ReadOnly = true;
            // 
            // Mitem
            // 
            this.Mitem.FillWeight = 150F;
            this.Mitem.HeaderText = "产品物料代码";
            this.Mitem.Name = "Mitem";
            this.Mitem.ReadOnly = true;
            this.Mitem.Width = 150;
            // 
            // WONO
            // 
            this.WONO.FillWeight = 120F;
            this.WONO.HeaderText = "工单";
            this.WONO.Name = "WONO";
            this.WONO.ReadOnly = true;
            // 
            // RowIndex
            // 
            this.RowIndex.FillWeight = 50F;
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            // 
            // FrmSelectReworkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 393);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSelectReworkOrder";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0002";
            this.Text = "重工工单选择";
            this.Load += new System.EventHandler(this.FrmSelectReworkOrder_Load);
            this.PalWO.ResumeLayout(false);
            this.PalWO.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalWO;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn WONO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mitem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order;
    }
}