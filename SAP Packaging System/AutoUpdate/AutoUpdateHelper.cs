﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Windows.Forms;

namespace AutoUpdate
{
    class AutoUpdateHelper
    {
        //结束进程 规则：进程名包含ApplicationName中去除.exe的部分
        /// <summary>
        /// 结束进程
        /// 规则：进程名包含ApplicationName中去除.exe的部分
        /// </summary>
        public static void KillProcess(string Appname)
        {
            Process proc = Process.GetProcesses()
                .FirstOrDefault<Process>(
                    x => x.ProcessName.Contains(Appname.Replace(".exe", "")));

            if (proc == null)
                return;

            proc.Kill();
            System.Threading.Thread.Sleep(1000);

            KillProcess(Appname);
        }

        /// <summary>
        /// Connet to remote share folder 
        /// </summary>
        /// <param name="remoteHost">remote server IP or machinename.domain name</param>
        /// <param name="shareName">share name</param>
        /// <param name="userName">user name of remote share access account</param>
        /// <param name="passWord">password of remote share access account</param>
        /// <returns>connect result</returns>        
        public static bool Delete(string remotePath)
        {
          
            bool Flag = false;
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                string dosLine = @"net use " + remotePath + " /d";
                proc.StandardInput.WriteLine(dosLine);
                proc.StandardInput.WriteLine("exit");
                while (!proc.HasExited)
                {
                    proc.WaitForExit(1000);
                }

                string errormsg = proc.StandardError.ReadToEnd();
                proc.StandardError.Close();
                if (String.IsNullOrEmpty(errormsg))
                {
                    Flag = true;
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                proc.Close();
                proc.Dispose();
            }
            return Flag;
        }
        /// <summary>
        /// Connet to remote share folder 
        /// </summary>
        /// <param name="remoteHost">remote server IP or machinename.domain name</param>
        /// <param name="shareName">share name</param>
        /// <param name="userName">user name of remote share access account</param>
        /// <param name="passWord">password of remote share access account</param>
        /// <returns>connect result</returns>        
        public static bool Connect(string remotePath, string userName, string passWord)
        {
            
            bool Flag = false;
            Process proc = new Process();
            try
            {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardInput = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                string dosLine = @"net use " + remotePath + " /User:" + userName + " " + passWord + " /PERSISTENT:YES";
                proc.StandardInput.WriteLine(dosLine);
                proc.StandardInput.WriteLine("exit");
                while (!proc.HasExited)
                {
                    proc.WaitForExit(1000);
                }

                string errormsg = proc.StandardError.ReadToEnd();
                proc.StandardError.Close();
                if (String.IsNullOrEmpty(errormsg))
                {
                    Flag = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                proc.Close();
                proc.Dispose();
            }
            return Flag;
        }

        /// <summary>
        /// 返回集中数据库的表T_AutoUpdateConfig定义的自动更新的信息
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAutoUpdateInfo()
        {
            string connString = getConnectionStringFromConfig();
            List<string> lst = new List<string>();

            SqlConnection conn = new SqlConnection(connString);
            string sql = @"SELECT ParaValue FROM [csimes_SapInterface].[dbo].[T_AutoUpdateConfig]
                            WHERE AppName = 'CSICPR'  AND ParaType='BasicCondition'";
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);

                // 创建DataAdapter和DataSet
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        lst.Add(Convert.ToString(row[0]));
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                conn = null;
            }

            return lst;
        }


        /// <summary>
        /// get packing parameters setting
        /// </summary>
        /// <param name="sNodeName"></param>
        /// <param name="isAttribute"></param>
        /// <param name="sAttributeName"></param>
        /// <param name="configFile"></param>
        /// <returns></returns>
        private static string getConfigFromXML(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "ParametersSetting.xml";
            string xmlFile = Application.StartupPath + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {
                MessageBox.Show("Please Inform Manager to Config Package Parameters Before Packing", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }

        /// <summary>
        /// 初始化参数
        /// </summary>
        private static string getConnectionStringFromConfig()
        {
            //从XML里读取
            string dbServer = getConfigFromXML("DataSource", false, "", "config.xml");
            string dbName = getConfigFromXML("DBName", false, "", "config.xml");
            string dbUser = getConfigFromXML("UserID", false, "", "" +"");
            string dbPwd = getConfigFromXML("Password", false, "", "config.xml");
            string connstring = string.Empty;


            if (dbServer.Equals("") || dbName.Equals("") || dbUser.Equals("") || dbPwd.Equals(""))
            {
                MessageBox.Show("连接数据库的DataSource,DBName,UserID,Password没有配置", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            connstring = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", dbServer, dbName, dbUser, dbPwd);
            return connstring;
        }

    }
}
