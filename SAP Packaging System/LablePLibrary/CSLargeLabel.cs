﻿using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;
using System.Windows.Forms;


namespace LablePLibrary
{
    public delegate void ProgressHandler();

    /// <summary>
    /// 常熟组件厂区Excel竖包装大标签
    /// </summary>
    public class CSLargeLabel
    {
        private FileInfo template;
        private Microsoft.Office.Interop.Excel.Application app;
        private object missing = Missing.Value;//用于调用带默认参数的方法
        public event ProgressHandler SavedEvent;
        private string sDirectory;
        private string sSourceFile;

        /// <summary>
        /// Initial
        /// </summary>
        /// <param name="sDirectory">Excel文件备份目录</param>
        /// <param name="sSourceFile">Excel模板文件</param>
        public CSLargeLabel(string sDirectory, string sSourceFile)
        {
        	
            this.app = new Microsoft.Office.Interop.Excel.Application();
         
            this.app.Visible = false;//设置Excel窗口不可见
            this.app.DisplayAlerts = false;//
            this.app.AlertBeforeOverwriting = false;//关闭修改之后是否保存
            if (sDirectory == "" || sDirectory == null)
            {
                //this.sDirectory = System.IO.Directory.GetDirectoryRoot(System.IO.Directory.GetCurrentDirectory()) + "竖插包装";
                this.sDirectory = System.IO.Directory.GetCurrentDirectory() + "Vertical_packaging";                   
                // this.sDirectory = @"C:\Users\Jacky.li\Documents\My Received Files\包装系统-CAMO-new\LablePLibrary\CSLargeLabel";
            }
            else
                this.sDirectory = sDirectory;
            
            this.sSourceFile = sSourceFile;
        }

        /// <summary>
        /// 写数据到Excel
        /// </summary>
        /// <param name="x">Excel行</param>
        /// <param name="y">Excel列</param>
        /// <param name="largeLabel">标签参数类</param>
        /// <param name="iSheet">第几个Excel sheet页</param>
        /// <returns>true:写入成功，false:写入失败</returns>
        public bool Write2Excel(int x, int y, LargeLabel largeLabel, int iSheet = 1)
        {
            bool isWriteOK = false;
            try
            {
            	//string filePath = CopyFile(sDirectory, sSourceFile, largeLabel.CartonNumber);
                //Workbook book = app.Workbooks.Open(filePath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);//打开拷贝的副本
                Workbook book = null;
				book = app.Workbooks.Open(sSourceFile, missing, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
				
                Worksheet sheet = book.Sheets[iSheet] as Worksheet;//得到Excel的第一个sheet

                #region
                //for (int i = 0; i < largeLabel.Arraylist.Count; i++)
                //{
                //    sheet.Cells[x + i, y] = largeLabel.Arraylist[i];
                //}
                #endregion
				
                for (int i = 0; i < largeLabel.ListInfo.Count;i++ )
                {                              	
                    sheet.Cells[x + i, y] = largeLabel.ListInfo[i][0];//组件标签                   
                    sheet.Cells[x + i, y + 2] = largeLabel.ListInfo[i][1];
                    sheet.Cells[x + i, y + 3] = largeLabel.ListInfo[i][2];
                    sheet.Cells[x + i, y + 4] = largeLabel.ListInfo[i][3];
                    sheet.Cells[x + i, y + 5] = largeLabel.ListInfo[i][4];
                    sheet.Cells[x + i, y + 6] = largeLabel.ListInfo[i][5];
                }
				x+=2;
                #region Modify by Gengao 2012-12-06
                if (largeLabel.IsShowPowerGrade.Trim().Equals("Y"))
                    sheet.Cells[x + 24, y] = largeLabel.Model.Insert(largeLabel.Model.IndexOf('-') + 1, largeLabel.Power) + largeLabel.PowerGrade;
                else
                    sheet.Cells[x + 24, y] = largeLabel.Model.Insert(largeLabel.Model.IndexOf('-') + 1, largeLabel.Power); 
                #endregion

                sheet.Cells[x + 24 + 1, y] = largeLabel.CellType;
                sheet.Cells[x + 24 + 2, y] = largeLabel.Size;
                sheet.Cells[x + 24 + 3, y] = largeLabel.Colour;
//                if (largeLabel.IsHavePattern)
//                {
//                    sheet.Cells[x + 24 + 4, y] = largeLabel.Pattern;
//                    sheet.Cells[x + 24 + 5, y] = largeLabel.CartonNumber;
//                }
//                else
                sheet.Cells[x + 24 + 4, y] = largeLabel.CartonNumber;                
                sheet.Cells[x+24+5,y] = largeLabel.Voltage;
                sheet.Cells[x+24+7,y] = largeLabel.NetWeight;
               	sheet.Cells[x+24+8,y] = largeLabel.GrossWeight;
               	sheet.Cells[x+24+11,y] = largeLabel.Volumn;
               	sheet.Cells[x+24+13,y] = largeLabel.FrameColor;
               	sheet.Cells[x+24+14,y] = largeLabel.Cable;
               	sheet.Cells[x+24+15,y] = largeLabel.Market;
               	sheet.Cells[x+24+16,y] = largeLabel.LID;
               	sheet.Cells[x+24+17,y] = largeLabel.BusBar;
               	sheet.Cells[x+24+18,y] = largeLabel.Quantity;
               	sheet.Cells[x+24+19,y] = largeLabel.CellVendor;
               	sheet.Cells[x+24+20,y] = largeLabel.Connector;
               	
                //app.Visible = true;
                //sheet.PrintPreview(false);
               
                sheet.PrintOut();                
               // book.Save();//保存Excel
                app.Visible = false;              
                app.Workbooks.Close();
                isWriteOK = true;
            }
            catch(Exception ex)
            {
                isWriteOK = false;
            }
            finally
            {
                this.app.Quit();//退出Excel，结束进程
                System.GC.Collect();//回收资源
            }
            return isWriteOK;
        }

        public void PrintLabelSixC( string Boxid )
        {
            try
            {
                //string filePath = CopyFile(sDirectory, sSourceFile, largeLabel.CartonNumber);
                //Workbook book = app.Workbooks.Open(filePath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);//打开拷贝的副本
                Workbook book = null;
                book = app.Workbooks.Open(sSourceFile, missing, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);

                Worksheet sheet = book.Sheets[2] as Worksheet;//得到Excel的第一个sheet
                sheet.Cells[52 + 22 + 6, 6] = Boxid;
                sheet.PrintOut();
                app.Visible = false;
                app.Workbooks.Close();
              
            }
            catch (Exception ex)
            {
               
            }
            finally
            {
                this.app.Quit();//退出Excel，结束进程
                System.GC.Collect();//回收资源
            }
        }
        public void PrintLabelSixC(LargeLabel largeLabel)
        {
            try
            {
                //string filePath = CopyFile(sDirectory, sSourceFile, largeLabel.CartonNumber);
                //Workbook book = app.Workbooks.Open(filePath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);//打开拷贝的副本
                Workbook book = null;
                book = app.Workbooks.Open(sSourceFile, missing, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);

                Worksheet sheet = book.Sheets[1] as Worksheet;//得到Excel的第一个sheet
                sheet.Cells[45, 6] = largeLabel.CartonNumber;
                int x = 46, y = 6;


                for (int i = 0; i < largeLabel.ListInfo.Count; i++)
                {
                    sheet.Cells[x + i, y] = largeLabel.ListInfo[i][0];//组件标签    
                }



                sheet.PrintOut();
                app.Visible = false;
                app.Workbooks.Close();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.app.Quit();//退出Excel，结束进程
                System.GC.Collect();//回收资源
            }
        }
        public bool Write2ExcelH(int x, int y, LargeLabelH largeLabel, int iSheet = 2)
        {
            bool isWriteOK = false;
            try
            {
            	//string filePath = CopyFile(sDirectory, sSourceFile, largeLabel.CartonNumber);
                //Workbook book = app.Workbooks.Open(filePath, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);//打开拷贝的副本
                Workbook book = null;
				book = app.Workbooks.Open(sSourceFile, missing, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
				
                Worksheet sheet = book.Sheets[iSheet] as Worksheet;//得到Excel的第一个sheet

                #region
                //for (int i = 0; i < largeLabel.Arraylist.Count; i++)
                //{
                //    sheet.Cells[x + i, y] = largeLabel.Arraylist[i];
                //}
                #endregion
				
                for (int i = 0; i < largeLabel.arraylist.Count;i++ )
                {                              	
                    sheet.Cells[x + i, y] = largeLabel.arraylist[i];//组件标签                 
                   
                }

                sheet.Cells[x+ 22,y] = largeLabel.packingDate;
                sheet.Cells[x + 22 + 1, y] = largeLabel.materialCode;
                sheet.Cells[x + 22 + 2, y] = largeLabel.model;
               	sheet.Cells[x + 22 + 3, y] = largeLabel.cellType;                	
                sheet.Cells[x + 22 + 4, y] = largeLabel.size;
                sheet.Cells[x + 22 + 5, y] = largeLabel.colour;
                sheet.Cells[x + 22 + 6, y] = largeLabel.cartonNumber;
                sheet.Cells[x + 22 + 7, y] = largeLabel.quantity;
               	sheet.Cells[x + 22 + 8, y] = largeLabel.grossweight;
               	sheet.Cells[x + 22 + 9, y] = largeLabel.netweight;
               	sheet.Cells[x + 22 + 10, y] = largeLabel.power;
               	sheet.Cells[x + 22 + 11, y] = largeLabel.cartonclass;
               	sheet.Cells[x + 22 + 12, y] = largeLabel.LID;
				sheet.Cells[x + 22 + 13, y] = largeLabel.JboxType;
				sheet.Cells[x + 22 + 14, y] = largeLabel.CableLength;
				sheet.Cells[x + 22 + 15, y] = largeLabel.ConnectorType;				
              	sheet.Cells[x + 22 + 16, y] = largeLabel.BusBar;	
                //app.Visible = true;
                //sheet.PrintPreview(false);
               
                sheet.PrintOut();                
               // book.Save();//保存Excel
                app.Visible = false;              
                app.Workbooks.Close();
                isWriteOK = true;
            }
            catch(Exception ex)
            {
                isWriteOK = false;
            }
            finally
            {
                this.app.Quit();//退出Excel，结束进程
                System.GC.Collect();//回收资源
            }
            return isWriteOK;
        }

        /// <summary>
        /// 复制Excel文件，放在备份目录下
        /// </summary>
        /// <param name="directory">目标目录</param>
        /// <param name="souceFileName">Excel源文件</param>
        /// <param name="sCartonNo">箱号</param>
        /// <returns>目标文件路径</returns>
        private string CopyFile(string directory,string souceFileName,string sCartonNo)
        {
            string destFileName = "";
            if (!System.IO.Directory.Exists(directory))
            {
                System.IO.Directory.CreateDirectory(directory);
            }
            destFileName = directory + "\\" + System.IO.Path.GetFileName(souceFileName).Insert(System.IO.Path.GetFileName(souceFileName).LastIndexOf('.'), "_" + sCartonNo);
            System.IO.File.Copy(souceFileName, destFileName);
            return destFileName;
        }

    }
}














